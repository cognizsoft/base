/*
* Title: Provider Invoice
* Descrpation :- This module realted to provider invoice
* Date :- July 01, 2019
* Author :- Ramesh Kumar & Cogniz Software Solution
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/provider-invoice-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT patient_procedure.application_id,DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") as procedure_date,patient_procedure.procedure_amt,patient_procedure.procedure_status, credit_applications.application_no, payment_plan.plan_number, payment_plan.pp_id, payment_plan.loan_amount as loan_amount, '
            + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name '
            + 'FROM patient_procedure '
            + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_procedure.pp_id '
            + 'INNER JOIN credit_applications ON credit_applications.application_id=patient_procedure.application_id AND (credit_applications.status=1 OR credit_applications.status=6) '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'WHERE patient_procedure.procedure_status>? AND payment_plan.plan_status=? AND patient_procedure.provider_id=? AND patient_procedure.pp_id NOT IN (SELECT provider_invoice_detial.pp_id FROM provider_invoice_detial WHERE provider_id=? AND status=0)'
            ,
            [
                0,
                1,
                req.query.id,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });

    app.post('/api/provider-selected-list', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/invoiceSql.js');
        dataSql.getSelectedApplication(con, req.body, function (result) {
            res.send(result);
        });
    })

    const pdf = require('html-pdf');

    app.post('/api/create-pdf', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/invoiceSql.js');
        dataSql.getSelectedApplication(con, req.body, function (result) {

            if (result.status == 1) {
                const pdfTemplate = require('./documents');

                const dir = './uploads/provider/' + req.body.provider_id + '/' + Math.random()
                //var fullUrl = req.protocol + '://' + req.get('host');
                //var logoimg = fullUrl + '/uploads/logo.png';
                var fullUrl = req.get('origin');
                var logoimg = fullUrl + '/logo.png';
                var filename = dir + 'rezultati.pdf';

                pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {

                    if (err) {

                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);

                    }
                })
            }
        });



    })
    app.get('/api/fetch-pdf', jwtMW, (req, res) => {
        con = require('../db');
        const path = require('path');
        res.sendFile(path.join(__dirname, '..', req.query.file));

    })
    app.post('/api/provider-submit-invoice', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/invoiceSql.js');
        dataSql.getSelectedApplication(con, req.body, function (result) {
            if (result.status == 1) {
                dataSql.setInvoice(con, result, req.body.current_user_id, req.body.provider_id, function (invoiceResult) {
                    res.send(invoiceResult);
                });
            } else {
                res.send(result);
            }
        });
    })

    app.get('/api/provider-submittedinvoice-list/', jwtMW, (req, res) => {
        con = require('../db');
        let cond = 1;

        if (req.query.status_id == 1) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id ="' + req.query.status_id + '" ';
        } else if (req.query.status_id == 2) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id ="' + req.query.status_id + '" ';
        } else if (req.query.status_id == 3) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id ="' + req.query.status_id + '" ';
        } else if (req.query.status_id == 4) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id ="' + req.query.status_id + '" ';
        } else if (req.query.status_id == 6) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id IN (1,3,4) ';
        } else if (req.query.status_id == 7) {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id IN (1,3) ';
        } else {
            cond = cond + ' AND provider_invoice.mdv_invoice_status_id IN (1,2,3,4,5) ';
        }

        con.query('SELECT provider_invoice.provider_invoice_id,provider_invoice.invoice_number,provider_invoice.provider_id,provider_invoice.total_amt, provider_invoice.total_hps_discount, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, provider_invoice.mdv_invoice_status_id, master_data_values.value as invoice_status, '
            + '(SELECT count(provider_invoice_id) FROM provider_invoice_detial WHERE provider_invoice_id=provider_invoice.provider_invoice_id AND status=0) as total_application '
            + 'FROM provider_invoice '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id AND md_id=?'
            + 'WHERE provider_invoice.provider_id=? AND ' + cond + ' ORDER BY provider_invoice.provider_invoice_id DESC'
            ,
            [
                'Provider Invoice Status',
                req.query.id,
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });
    app.post('/api/view-pdf', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/invoiceSql.js');
        dataSql.getInvoiceApplication(con, req.body, function (getApp) {

            if (getApp.status == 0) {
                var obj = {
                    "status": 0,
                    "message": "Invoice cancelled or application not available."
                }
                res.send(obj);
            }
            else if (getApp.status == 1 && Object.keys(getApp.pp_id).length > 0) {

                dataSql.getSelectedApplication(con, getApp, function (result) {

                    if (result.status == 1) {
                        const pdfTemplate = require('./documents');

                        const dir = './uploads/provider/' + req.body.provider_id + '/' + Math.random()
                        //var fullUrl = req.protocol + '://' + req.get('host');
                        //var logoimg = fullUrl + '/uploads/logo.png';
                        var fullUrl = req.get('origin');
                        var logoimg = fullUrl + '/logo.png';
                        var filename = dir + 'rezultati.pdf';
                        pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {

                            if (err) {
                                var obj = {
                                    "status": 0,
                                    "message": "Currently we are not able to generate pdf",
                                }
                                res.send(obj);
                            } else {
                                var obj = {
                                    "status": 1,
                                    "result": filename
                                }
                                res.send(obj);

                            }
                        })
                    }
                });
            } else if (getApp.pp_id.length == 0) {
                var obj = {
                    "status": 0,
                    "message": "Invoice cancelled or application not available."
                }
                res.send(obj);
            } else {
                res.send(getApp);
            }
        });




    })


    //view submitted invoice
    app.post('/api/view-submitted-invoice', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        var dataSql = require('./invoice/invoiceSql.js');
        dataSql.getInvoiceApplication(con, req.body, function (getApp) {
            
            if (getApp.status == 0) {
                var obj = {
                    "status": 0,
                    "message": "Invoice cancelled or application not available."
                }
                res.send(obj);
            }
            else if (getApp.status == 1 && Object.keys(getApp.pp_id).length > 0) {

                dataSql.getSelectedApplication(con, getApp, function (result) {

                    if (result.status == 1) {
                        var obj = {
                            "status": 1,
                            "message": "Submitted invoice detail view"
                        }
                        obj.selectedApplication = result;
                        obj.getInvoiceApplication = getApp
                        res.send(obj)
                    } else {
                        res.send("something wrong")
                    }
                });
            } else if (getApp.pp_id.length == 0) {
                var obj = {
                    "status": 0,
                    "message": "Invoice cancelled or application not available."
                }
                res.send(obj);
            } else {
                res.send(getApp);
            }
        });




    })

    app.get('/api/provider-refundinvoice-list/', jwtMW, (req, res) => {
        con = require('../db');

        let cond='';
        if (req.query.iou_flag != '' && req.query.iou_flag != 'all') {
            cond = cond + 'AND provider_refund.iou_flag ="' + req.query.iou_flag + '"';
        }

        con.query('SELECT payment_plan.plan_number,payment_plan.loan_amount,credit_applications.application_no,patient.f_name,patient.m_name,patient.l_name,patient.peimary_phone,patient.email, '
            + 'provider_refund.refund_id,provider_refund.invoice_number,provider_refund.refund_due,provider_refund.comments,master_data_values.value,provider_refund.iou_flag '
            + 'FROM provider_refund '
            + 'INNER JOIN payment_plan ON payment_plan.pp_id = provider_refund.pp_id '
            + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
            + 'INNER JOIN patient ON patient.patient_id = credit_applications.patient_id '
            + 'INNER JOIN provider_invoice ON provider_invoice.provider_invoice_id = provider_refund.provider_invoice_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=provider_refund.status AND md_id=? '

            + 'WHERE provider_invoice.provider_id=? ' +cond+ ' ORDER BY provider_refund.refund_id DESC'
            ,
            [
                'Provider Refund Invoice Status',
                req.query.id,
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var refundID = rows && [...new Set(rows.map(item => item.refund_id))];
                    
                    con.query('SELECT provider_refund_details.status,provider_refund_details.refund_id,provider_refund_details.payment_method, provider_refund_details.paid_amount, provider_refund_details.check_no, DATE_FORMAT(provider_refund_details.check_date, "%m/%d/%Y") as check_date, provider_refund_details.ach_bank_name, provider_refund_details.ach_routing_no, provider_refund_details.ach_account_no, master_data_values.value, '
                        + 'provider_refund_details.paid_amount, provider_invoice.invoice_number,DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y") as date_paid '
                        + 'FROM provider_refund_details '
                        + 'LEFT JOIN master_data_values ON master_data_values.status_id=provider_refund_details.payment_method AND md_id=? '
                        + 'LEFT JOIN provider_invoice ON provider_invoice.provider_invoice_id=provider_refund_details.provider_invoice_id '
                        + 'LEFT JOIN provider_refund ON provider_refund.refund_id=provider_refund_details.refund_id '
                        //+ 'LEFT JOIN payment_plan ON payment_plan.pp_id=provider_refund.pp_id '

                        + 'WHERE provider_refund_details.refund_id IN(?)'
                        , ['Payment Method', (refundID.length > 0) ? refundID : 0]
                        , function (error, details, fields) {
                            
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again111."
                                }
                                res.send(obj);
                            } else {
                                con.query('SELECT md_id, value, status_id as option_id '
                                    + 'FROM master_data_values '
                                    + 'WHERE md_id=? AND status=?'
                                    , ['Payment Method', 1]
                                    , function (error, closeInt, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": rows,
                                                "paymentMethod": closeInt,
                                                "invoiceDetails": details,
                                            }
                                            res.send(obj);
                                        }
                                    })
                            }
                        })


                }
            })
    });
    app.post('/api/refund-submitted-invoice', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let date = require('date-and-time');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

        con.query('SELECT pp_id FROM `provider_refund` WHERE refund_id =? ',
            [
                req.body.data.refund_invoice_id
            ]
            , function (error, rows, fields) {
                if (error) {
                    obj.status = 0;
                    obj.message = 'Please try again.';
                    res.send(obj);
                } else {
                    con.query('INSERT INTO `provider_refund_details` SET '
                        + 'refund_id=?, '
                        + 'pp_id=?, '
                        + 'paid_amount=?, '
                        + 'payment_method=?, '
                        + 'check_no=?, '
                        + 'check_date=?, '
                        + 'ach_bank_name=?, '
                        + 'ach_routing_no=?, '
                        + 'ach_account_no=?, '
                        + 'status=?, '
                        + 'date_created=?, '
                        + 'date_modified=?, '
                        + 'created_by=?, '
                        + 'modified_by=?',
                        [
                            req.body.data.refund_invoice_id,
                            rows[0].pp_id,
                            req.body.data.refund_amount,
                            req.body.data.provider_pay_method,
                            req.body.data.check_no,
                            req.body.data.check_date,
                            req.body.data.ach_bank_name,
                            req.body.data.ach_routing_no,
                            req.body.data.ach_account_no,
                            3,
                            current_date,
                            current_date,
                            req.body.current_user_id,
                            req.body.current_user_id
                        ]
                        , function (error, rows, fields) {
                            if (error) {
                                obj.status = 0;
                                obj.message = 'Please try again.';
                                res.send(obj);
                            } else {
                                con.query('UPDATE `provider_refund` SET '
                                    + 'status=?, '
                                    + 'date_modified=?, '
                                    + 'modified_by=? '
                                    + 'WHERE refund_id=?',
                                    [
                                        3,
                                        current_date,
                                        req.body.current_user_id,
                                        req.body.data.refund_invoice_id
                                    ]
                                    , function (error, rows, fields) {
                                        if (error) {
                                            obj.status = 0;
                                            obj.message = 'Please try again.';
                                            res.send(obj);
                                        } else {
                                            obj.status = 1;
                                            obj.message = 'Invoice successfully submitted.';
                                            res.send(obj);
                                        }
                                    })
                            }
                        })
                }
            })
    })

    app.get('/api/admin-refundinvoice-list/', jwtMW, (req, res) => {
        con = require('../db');
        
        let cond='';
        if (req.query.iou_flag != '' && req.query.iou_flag != 'all') {
            cond = cond + 'AND provider_refund.iou_flag ="' + req.query.iou_flag + '"';
        }

        con.query('SELECT payment_plan.plan_number,payment_plan.loan_amount,credit_applications.application_no,patient.f_name,patient.m_name,patient.l_name,patient.peimary_phone,patient.email, '
            + 'provider_refund.status,provider_refund.refund_id,provider_refund.invoice_number,provider_refund.refund_due,provider_refund.comments,master_data_values.value, provider.name,provider_refund.iou_flag '
            + 'FROM provider_refund '
            + 'INNER JOIN payment_plan ON payment_plan.pp_id = provider_refund.pp_id '
            + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
            + 'INNER JOIN patient ON patient.patient_id = credit_applications.patient_id '
            + 'INNER JOIN provider_invoice ON provider_invoice.provider_invoice_id = provider_refund.provider_invoice_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=provider_refund.status AND md_id=? '
            + 'INNER JOIN provider ON provider.provider_id = payment_plan.provider_id '
            + 'WHERE provider_refund.status != ? ' +cond+ ' ORDER BY provider_refund.refund_id DESC'
            ,
            [
                'Provider Refund Invoice Status', 1
            ]
            , function (error, rows, fields) {
                console.log(error)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var refundID = rows && [...new Set(rows.map(item => item.refund_id))];
                    con.query('SELECT provider_refund_details.status,provider_refund_details.refund_id,provider_refund_details.payment_method, provider_refund_details.paid_amount, provider_refund_details.check_no, DATE_FORMAT(provider_refund_details.check_date, "%m/%d/%Y") as check_date, provider_refund_details.ach_bank_name, provider_refund_details.ach_routing_no, provider_refund_details.ach_account_no, master_data_values.value, '
                        + 'provider_refund_details.paid_amount, provider_invoice.invoice_number,DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y") as date_paid '
                        + 'FROM provider_refund_details '
                        + 'LEFT JOIN master_data_values ON master_data_values.status_id=provider_refund_details.payment_method AND md_id=? '
                        + 'LEFT JOIN provider_invoice ON provider_invoice.provider_invoice_id=provider_refund_details.provider_invoice_id '
                        + 'LEFT JOIN provider_refund ON provider_refund.refund_id=provider_refund_details.refund_id '
                        //+ 'LEFT JOIN payment_plan ON payment_plan.pp_id=provider_refund.pp_id '
                        + 'WHERE provider_refund_details.refund_id IN(?)'
                        , ['Payment Method', (refundID.length > 0) ? refundID : 0]
                        , function (error, details, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                var obj = {
                                    "status": 1,
                                    "result": rows,
                                    "invoiceDetails": details,
                                }
                                res.send(obj);
                            }
                        })
                }
            })
    });

    app.post('/api/adminrefund-submitted-invoice', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let date = require('date-and-time');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

        con.query('SELECT SUM(paid_amount) as paid_amount, refund_due FROM `provider_refund_details` '
            + 'INNER JOIN provider_refund ON provider_refund.refund_id = provider_refund_details.refund_id '
            + 'WHERE provider_refund.refund_id=?',
            [
                req.body.data.refund_invoice_id,
            ], function (error, getDetails, fields) {
                if (error) {
                    obj.status = 0;
                    obj.message = 'Please try again.';
                    res.send(obj);
                } else {
                    con.query('UPDATE `provider_refund_details` SET '
                        + 'comments=?, '
                        + 'status=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE refund_id=? AND status=?',
                        [
                            req.body.data.note,
                            4,
                            current_date,
                            req.body.current_user_id,
                            req.body.data.refund_invoice_id,
                            3
                        ]
                        , function (error, rows, fields) {
                            if (error) {
                                obj.status = 0;
                                obj.message = 'Please try again.';
                                res.send(obj);
                            } else {
                                con.query('UPDATE `provider_refund` SET '
                                    + 'comments=?, '
                                    + 'status=?, '
                                    + 'date_modified=?, '
                                    + 'modified_by=? '
                                    + 'WHERE refund_id=?',
                                    [
                                        req.body.data.note,
                                        (getDetails[0].paid_amount == getDetails[0].refund_due)?4:5,
                                        current_date,
                                        req.body.current_user_id,
                                        req.body.data.refund_invoice_id
                                    ]
                                    , function (error, rows, fields) {
                                        if (error) {
                                            obj.status = 0;
                                            obj.message = 'Please try again.';
                                            res.send(obj);
                                        } else {
                                            obj.status = 1;
                                            obj.message = 'Invoice successfully submitted.';
                                            res.send(obj);
                                        }
                                    })
                            }
                        })
                }
            })
    })

}