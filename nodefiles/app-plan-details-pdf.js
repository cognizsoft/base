module.exports = (data, planid, logoimg) => {
   //return false;
   let date = require('date-and-time');
   let moment = require('moment');
   let now = new Date();
   let current_date = date.format(now, 'MM/DD/YYYY');

   let totalLateAmount = 0;
   let totalFinCharge = 0;
   var uniqueLateCount = 0;//data.application_plans && [...new Set(data.application_plans.map(item => item.late_count))];
   var groupArrays = data.invoiceDetails && data.invoiceDetails.map((dat, idx) => {
      var nextDetailsDetails = data.invoicePlan.filter(x => x.invoice_id == dat.invoice_id);
      nextDetailsDetails = data.invoicePlan.filter(x => x.invoice_id > dat.invoice_id && x.pp_id == nextDetailsDetails[0].pp_id);
      data.nextDeferred = 0;
      if (nextDetailsDetails.length > 0) {
         var checkNextInvoice = nextDetailsDetails.reduce(function (a, b) {
            return new Date(a.due_date) < new Date(b.due_date) ? a : b;
         });
         if (Object.keys(checkNextInvoice).length !== 0) {
            data.nextDeferred = 1;
         }
      }
      if (dat.on_fly == 1 && nextDetailsDetails.length > 0 && dat.invoice_status == 3 && dat.invoice_status != 5 && dat.invoice_status != 0) {
         dat.missed_flag = 1;
      } else if (data.invoiceDetails.length > (idx + 1) && dat.invoice_status != 1 && dat.invoice_status != 4 && dat.on_fly != 1 && dat.invoice_status != 5 && dat.invoice_status != 0) {
         dat.missed_flag = 1;
      } else {
         dat.missed_flag = 0;
      }

      //dat.fin_charge_received = (dat.fin_charge_received != null) ? (dat.finpct != null) ? (parseFloat(dat.fin_charge_received) - (parseFloat(dat.finpct) * parseFloat(dat.fin_charge_received)) / 100) : parseFloat(dat.fin_charge_received) : 0;
      //dat.late_fee_received = (dat.late_fee_received != null) ? (dat.latepct != null) ? (parseFloat(dat.late_fee_received) - (parseFloat(dat.latepct) * parseFloat(dat.late_fee_received)) / 100) : parseFloat(dat.late_fee_received) : 0;
      dat.fin_charge_received = (dat.invoice_status == 3) ? dat.fin_charge_due : dat.fin_charge_received;
      dat.late_fee_received = (dat.invoice_status == 3) ? dat.late_fee_due : dat.late_fee_received;

      //dat.previous_fin_charge = (dat.previous_fin_charge != null) ? (dat.finpct != null) ? (parseFloat(dat.previous_fin_charge) - (parseFloat(dat.finpct) * parseFloat(dat.previous_fin_charge)) / 100) : parseFloat(dat.previous_fin_charge) : 0;
      //dat.previous_late_fee = (dat.previous_late_fee != null) ? (dat.latepct != null) ? (parseFloat(dat.previous_late_fee) - (parseFloat(dat.latepct) * parseFloat(dat.previous_late_fee)) / 100) : parseFloat(dat.previous_late_fee) : 0;
      dat.previous_fin_charge = parseFloat(dat.previous_fin_charge);
      dat.previous_late_fee = parseFloat(dat.previous_late_fee);

      dat.total_due = dat.payment_amount;
      dat.paid_amount1 = (dat.invoice_status == 1 || dat.invoice_status == 4) ? (dat.paid_amount + dat.fin_charge_received + dat.late_fee_received + dat.additional_amount + dat.previous_fin_charge + dat.previous_late_fee) : 0;
      dat.total_due += (dat.late_fee_received) ? dat.late_fee_received : 0;
      dat.total_due += (dat.fin_charge_received) ? dat.fin_charge_received : 0;
      dat.total_due += (dat.previous_late_fee) ? dat.previous_late_fee : 0;
      dat.total_due += (dat.previous_fin_charge) ? dat.previous_fin_charge : 0;

      if (idx != 0) {

         dat.previous_blc += (dat.previous_fin_charge != null) ? (dat.finpct != null) ? (parseFloat(dat.previous_fin_charge) - (parseFloat(dat.finpct) * parseFloat(dat.previous_fin_charge)) / 100) : parseFloat(dat.previous_fin_charge) : 0;
         dat.previous_blc += (dat.previous_late_fee != null) ? (dat.latepct != null) ? (parseFloat(dat.previous_late_fee) - (parseFloat(dat.latepct) * parseFloat(dat.previous_late_fee)) / 100) : parseFloat(dat.previous_late_fee) : 0;
      }

      if (dat.invoice_status == 1 || dat.invoice_status == 4) {
         totalLateAmount += dat.late_fee_received + dat.previous_late_fee;
         totalFinCharge += dat.fin_charge_received + dat.previous_fin_charge;
      }
      dat.finalAmt = (dat.paid_amount1 <= dat.total_due) ? dat.total_due - dat.paid_amount1 : 0;
      if((dat.invoice_status == 2 || dat.invoice_status == 3) && data.invoiceDetails[data.invoiceDetails.length-1].due_date > dat.due_date ){
         uniqueLateCount++;
      }
      return dat;
   })

   var uniqueLoanAmount = 0;
   var uniqueRemainingAmount = 0;
   data.application_plans && data.application_plans.reduce(function (accumulator, currentValue, currentindex) {
      if (!accumulator[currentValue.pp_id]) {
         accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
         uniqueLoanAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.loan_amount : 0;
         uniqueRemainingAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.remaining_amount : 0;
         //uniqueLoanAmount += currentValue.loan_amount;
         //uniqueRemainingAmount += currentValue.remaining_amount;
      }
      return accumulator;
   }, []);


   
   var uniqueMissedCount = data.application_plans && [...new Set(data.application_plans.map(item => item.missed_count))];

   var UniqueReceived = data.amountDetails && data.amountDetails.reduce(function (accumulator, currentValue, currentindex) {
      if (currentindex == 0) {
         accumulator = 0;
      }
      accumulator += currentValue.amount_rcvd;
      return accumulator;
   }, []);

   var planDetails = data.application_plans && data.application_plans.reduce(function (accumulator, currentValue, currentindex) {
      if (!accumulator[currentValue.pp_id]) {
         // get recived amount
         var totalAmt = data.amountDetails && data.amountDetails.reduce(function (accumulator, currentplan, planindex) {
            if (planindex == 0) {
               accumulator = 0;
            }

            accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
            return accumulator;
         }, 0);
         accumulator[currentValue.pp_id] = { monthly_amount: currentValue.monthly_amount, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, emi: currentValue.installment_amt, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, status_name: currentValue.status_name };
      } else {
         //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
      }

      return accumulator;
   }, []);


   //PAYMENT PLAN DETAIL
   var pp_html = '';
   planDetails.forEach(function (row, idx) {
      pp_html = pp_html + '<tr>'
         + '<td>' + row.pp_id + '</td>'
         + '<td>$' + parseFloat(row.loan_amount).toFixed(2) + '</td>'
         + '<td>$' + (row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2) + '</td>'
         + '<td>' + row.payment_term_month + ' Month</td>'
         + '<td>' + parseFloat(row.discounted_interest_rate).toFixed(2) + '%</td>'
         + '<td>$' + parseFloat(row.monthly_amount).toFixed(2) + '</td>'
         + '<td>' + row.date_created + '</td>'
         + '<td>' + row.status_name + '</td>'
         + '</tr>';
   })

   //INVOICE HISTORY
   if (groupArrays) {
      var inv_html = '';
      groupArrays.forEach(function (row, idx) {
         inv_html = inv_html + '<tr>'
            + '<td>' + (idx + 1) + '</td>'
            + '<td>' + row.invoice_number + '</td>'
            + '<td>' + ((row.payment_date) ? row.payment_date : "-") + '</td>'
            + '<td>' + row.due_date + '</td>'
            + '<td>' + ((row.previous_blc > 0) ? "$" + parseFloat(row.previous_blc).toFixed(2) : "-") + '</td>'
            + '<td>$' + parseFloat(row.payment_amount).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(row.late_fee_received).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(row.fin_charge_received).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(row.total_due).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(row.total_due).toFixed(2) + '</td>'
            + '<td>' + ((row.additional_amount > 0) ? "$" + parseFloat(row.additional_amount).toFixed(2) : "-") + '</td>'
            + '<td>' + ((row.paid_amount1 > 0) ? "$" + parseFloat(row.paid_amount1).toFixed(2) : "-") + '</td>'
            + '<td>$' + parseFloat(row.finalAmt).toFixed(2) + '</td>'
            + '<td>' + ((row.missed_flag == 1) ?
               'Missed'
               :
               (row.invoice_status == 1) ?
                  row.invoice_status_name
                  :
                  (row.invoice_status == 3) ?
                     row.invoice_status_name
                     :
                     (row.invoice_status == 4) ?
                        row.invoice_status_name
                        :
                        row.invoice_status_name) + '</td>'
            + '</tr>';
      })


   }
   let co_signer = '';
   if (data.application_details[0].co_patient_id != null) {
      co_signer = co_signer + '<div class="info-bx">'
         + '<div class="customer-info co-customer-info">'
         + '<table cellpadding="0" cellspacing="0" class="blue-tble">'
         + '<tbody>'
         + '<tr>'
         + '<th colSpan="3">Co-signer Information</th>'
         + '</tr>'
         + '<tr>'
         + '<td><strong>Name:</strong> ' + data.application_details[0].co_first_name + ' ' + data.application_details[0].co_middle_name + ' ' + data.application_details[0].co_last_name + '</td>'
         + '<td><strong>Address:</strong> ' + data.application_details[0].co_address1 + ' ' + data.application_details[0].co_address2 + ' ' + data.application_details[0].co_City + ' ' + data.application_details[0].co_state_name + '</td>'
         + '<td><strong>Phone:</strong> ' + data.application_details[0].co_phone_no + '</td>'
         + '</tr>'
         + '</tbody>'
         + '</table>'
         + '</div>';
   }
   return `
<!doctype html>
<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <title>PDF Result Template</title>
      <style>
         .invoice-box {
         max-width: 1200px;
         margin: auto;
         padding: 0px;
         font-size: 8px;
         line-height: 15px;
         font-family: 'Arial', sans-serif;
         color: #303030;
         }
         .margin-top {
         margin-top: 50px;
         }
         .justify-center {
         text-align: center;
         }
         .invoice-box table {
         width: 100%;
         line-height: inherit;
         text-align: left;
      border:solid 1px #ddf1ff; border-collapse:collapse;}
      .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
      .customer-info{width:32%; float:left;}
      .customer-info tr td, .loan-info tr td, .payment-info tr td {border:none;}
      .loan-info{width:32%; float:left; margin-left:15px}
      .payment-info{width:32%; float:right;}
      .blue-tble td{background:#f1f9ff; padding:2px 5px; font-size:8px;}
      .blue-tble th{background:#0e5d97; color:#fff;padding:2px 5px; font-size:8px;}
      .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
      .info-bx:after{margin-bottom:20px;}
      .detail-table th{background:#0e5d97; color:#fff;padding:2px 2px; font-size:8px; line-height:10px;}
      .detail-table td{padding:2px 2px; font-size:8px;}
      .info-bx {display: flex}
      .text-center {text-align:center;}
      .co-customer-info{width:100%}
      </style>
   </head>
   <body>
      <div class="invoice-box">
      <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
     <h1 class="justify-center">Customer Payment Plan</h1>

     <div class="info-bx">

        <div class="customer-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">
              <tr>
                 <th colspan="2">Customer Information</th>
              </tr>
              <tr>
                 <td><strong>Application No: </strong> </td><td>${data.application_details[0].application_no}</td>
              </tr>
              <tr>
                 <td><strong>Name: </strong> </td><td>${data.application_details[0].f_name + ' ' + data.application_details[0].m_name + ' ' + data.application_details[0].l_name}</td>
              </tr>
              <tr>
                 <td><strong>Address: </strong> </td><td>${data.application_details[0].address1 + ', ' + data.application_details[0].City + ', ' + data.application_details[0].name + ', ' + data.application_details[0].zip_code}</td>
              </tr>
              </tr>
                 <td><strong>Phone: </strong> </td><td>${data.application_details[0].peimary_phone}</td>
              </tr>
           </table>
        </div>
     
        <div class="loan-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">

              <tr>
                 <th colspan="2">Loan Information</th>
              </tr>
              </tr>
                 <td><strong>Line Of Credit :</strong> </td><td>$${parseFloat(data.application_details[0].approve_amount).toFixed(2)}</td>
              </tr>
              </tr>
                 <td><strong>Available Balance :</strong> </td><td>$${parseFloat(data.application_details[0].remaining_amount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Principal Amount :</strong> </td><td>$${parseFloat(uniqueLoanAmount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Outstanding Principal :</strong> </td><td>$${parseFloat(uniqueRemainingAmount).toFixed(2)}</td>
              </tr>

           </table>
        </div>

        <div class="payment-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">

              <tr>
                 <th colspan="2">Payment Information</th>
              </tr>
              </tr>
                 <td><strong>Late/Missed Payments: </strong> </td><td>${uniqueLateCount}</td>
              </tr>
              
              <tr>
                 <td><strong>Late Fees: </strong> </td><td>$${parseFloat(totalLateAmount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Financial Charges: </strong> </td><td>$${parseFloat(totalFinCharge).toFixed(2)}</td>
              </tr>
              <tr>
                 <td>&nbsp;</td><td>&nbsp;</td>
              </tr>
           </table>
        </div>

     </div>
     ${co_signer}
     <div class="detail-bx">
        <h2 class="justify-center">Payment Plans</h2>
        <div class="detail-table">
          <table cellpadding="0" cellspacing="0">
             
                <tr>
                   <th>Plan ID</th>
                   <th>Principal Amt</th>
                   <th>Outstanding Principal Amt</th>
                   <th>Payment Term</th>
                   <th>APR(%)</th>
                   <th>Monthly Payment</th>
                   <th>Date Created</th>
                   <th>Status</th>
                </tr>
                ${pp_html}              
             
          </table>
     </div>
     
     <div class="detail-bx">
        <h2 class="justify-center">Invoice History</h2>
        <div class="detail-table">
           <table cellpadding="0" cellspacing="0">
              <tr>
                 <th>S.No.</th>
                 <th>Invoice No.</th>
                 <th>Payment Date</th>
                 <th>Due Date</th>
                 <th>Prev Bal</th>
                 <th>Monthly Payment</th>
                 <th>Late Fee</th>
                 <th>Fin Charge</th>
                 <th>Total Due</th>
                 <th>Min Due</th>
                 <th>Additional Amt</th>
                 <th>Paid</th>
                 <th>Balance</th>
                 <th>Status</th>
              </tr>
              ${inv_html}
           </table>
        </div>
     </div>
     
        
      </div>
   </body>
</html>
    `;
};