/*
* Title: masterValue
* Descrpation :- This module blong to admin reports
* Date :-  July 18, 2019
* Author :- Ramesh
*/
module.exports = function (app, jwtMW) {
	let date = require('date-and-time');

	// customr weekly monthly filter option
	app.post('/api/admin-customer-account-filter/', jwtMW, (req, res) => {
		console.log(req.body)
		con = require('../db');
		var dataSql = require('./support/customer-support.js');
		if(req.body.sort_by == 2) {
			dataSql.customerAccountFilter(con, req.body, function (result) {
				res.send(result);
			});
		} else {
			dataSql.providerAccountFilter(con, req.body, function (result) {
				res.send(result);
			});			

		}
	});

	app.get('/api/get-user-questions', jwtMW, (req, res) => {
	    con = require('../db');
	    var obj = {}
	    con.query('SELECT '
            /*+ 'credit_applications.application_id, '
            + 'credit_applications.patient_id, '*/

            + 'user.user_id, '

            + 'security_answers.id AS security_answers_id, '
            + 'security_answers.user_id, '
            + 'security_answers.security_questions_id, '
            + 'security_answers.answers, '

            + 'security_questions.name '

            + 'FROM user '

            /*+ 'INNER JOIN user '
            + 'ON credit_applications.patient_id = user.patient_id '*/

            + 'INNER JOIN security_answers '
            + 'ON security_answers.user_id = user.user_id '

            + 'INNER JOIN security_questions '
            + 'ON security_questions.id = security_answers.security_questions_id '

            + 'WHERE user.user_id = ? AND security_answers.status = ? AND security_answers.delete_flag = ?'
            , [req.query.user_id, 1, 0]
            , function(err, questions) {
                console.log(this.sql)
                if(err) {
                    obj.status = 0;
                    obj.result = "Something wrong please try again.";
                    res.send(obj);
                } else {
                    obj.status = 1;
                    obj.questions = questions;
                    res.send(obj);
                }
            })


	})

	app.post('/api/create-support-ticket/', jwtMW, (req, res) => {
	    con = require('../db');
	    var dataSql = require('./support/customer-support.js');
	    
	    if(req.body.submit == 1) {
	    	dataSql.createTicketDetail(con, req.body, function (result) {
				res.send(result);
			});
	    } else {

		    dataSql.createTicket(con, req.body, function (result) {
				res.send(result);
			});
	    
	    }
	})

	app.get('/api/get-all-tickets', jwtMW, (req, res) => {
        con = require('../db');
        var data = JSON.parse(req.query.data)
        
        var dataSql = require('./support/customer-support.js');

        if(data.filter_by == 2) {
        	//for provider
        	dataSql.getAllProviderTickets(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }

	        });
        } else if(data.filter_by == 1) {
        	//for customer at admin end
	        dataSql.getAllCustomerTickets(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }

	        });
        } else if(data.filter_by == 3) {
        	//for single ticket
        	dataSql.getSingleTicket(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }

	        });
        } else if(data.filter_by == 4) {
        	//for customer at customer end
	        dataSql.getCustomerTickets(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }

	        });
        } else if(data.filter_by == 5) {
        	//for customer at customer end
	        dataSql.getCosignerTickets(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }

	        });
        } else {
        	//for all tickets on customer support page
        	dataSql.getAllTickets(con, data, function (result) {
	            if (result.status == 1) {
	                res.send(result);
	            } else {
	                res.send(result);
	            }
	        });
        }
    });

}
