module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    var CryptoJS = require('crypto-js');
    var AES = require("crypto-js/aes");
    var SHA256 = require("crypto-js/sha256");
    let now = new Date();

    const encryption = (plainText) => {

        var SECRET = 'rmaeshCSS'

        var b64 = CryptoJS.AES.encrypt(plainText.toString(), SECRET).toString();

        var e64 = CryptoJS.enc.Base64.parse(b64);
        var eHex = e64.toString(CryptoJS.enc.Hex);


        return eHex;
    }


    var multer = require('multer');
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './uploads');
        },
        filename: function (req, file, cb) {
            cb(null, Date.now() + file.originalname);
        }
    });

    const fileFilter = (req, file, cb) => {
        cb(null, true);
    }

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * 5
        },
        fileFilter: fileFilter
    });

    var dataupload = upload.array('files');

    const capitalize = (str) => {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    app.get('/api/direct-creditapplication-states/', (req, res) => {
        con = require('../db');
        con.query('SELECT state_id,name FROM states WHERE status=? AND delete_flag=? AND serving=? AND country_id=?',
            [
                1,
                0,
                1,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })

    //////////

    app.get('/api/get-data-access-request/', (req, res) => {
        con = require('../db');
        con.query('SELECT * FROM access_request WHERE id=?',
            [
                req.query.access_request_id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    obj.access_request_row = rows;
                    res.send(obj);
                }
            })
    })

    /////////


    /*
    * customer signup
    */
    app.post('/api/customer-signup/', (req, res) => {
        con = require('../db');

        var rand_no = Math.floor((Math.random() * 1000000) + 1);
        let now = new Date();
        var obj = {};
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        con.query('INSERT INTO access_request (customer_type, provider_name, f_name, l_name, phone, email, access_token, date_created, date_modified) VALUES(?,?,?,?,?,?,?,?,?)',
            [

                (req.body.customer_type == 1) ? 'Provider' : 'Customer', req.body.provider_name, req.body.f_name, req.body.l_name, req.body.phone, req.body.email, rand_no, current_date, current_date

            ], function (err, result) {
                //console.log(result)
                if (err) {
                    obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    /*var obj = {
                    "status": 1,
                    "message": "Please verify your email"
                    }
    
                    res.send(obj);*/

                    var emialTemp = require('./emailplan.js');
                    /*var locals = { 
                        cutomer_type: (req.body.customer_type == 1) ? 'Provider' : 'Customer',
                        provider_name: req.body.provider_name,
                        f_name: req.body.f_name,
                        l_name: req.body.l_name,
                        phone: req.body.phone,
                        email: req.body.email,
                        token: rand_no,
                        current_date: current_date,
                        verify_link: encryption(result.insertId)
                    };
    
                    var toEmailAddress = req.body.email;
                    var template = 'register-verify-email';
                    emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                    var context = {
                        customer_type: (req.body.customer_type == 1) ? 'Provider' : 'Customer',
                        customer_name: req.body.f_name + ' ' + req.body.l_name,
                        customer_phone: req.body.phone,
                        customer_email: req.body.email,
                        provider_name: (req.body.provider_name) ? req.body.provider_name : '-',
                        access_token: rand_no,
                        verify_link: encryption(result.insertId)
                    };

                    //console.log(context)
                    var template = 'User Verify Email';
                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                        if (Result.status == 1) {

                            var emailOpt = {
                                toEmail: req.body.email,
                                subject: Result.result[0].template_subject
                            }

                            var html = Result.result[0].template_content;

                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                if (emailResult.status == 1) {
                                    obj.template = "Template email sent successfully!"
                                } else {
                                    obj.template = "Template email not sent successfully!"
                                }
                            });
                        } else {
                            obj.template = "Template details not found."
                        }

                    });

                    obj = {
                        "status": 1,
                        "message": "Email sent successfully"
                    }
                    obj.last_insert_id = result.insertId;
                    res.send(obj)


                }
            })
    });

    app.get('/api/verify-detail/', (req, res) => {
        con = require('../db');
        con.query('SELECT '
            + 'id, '
            + 'customer_type, '
            + 'access_token, '
            + 'access_flag, '
            + 'profile_created '
            + 'FROM access_request WHERE id = ?',
            [

                req.query.id

            ], function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong!"
                    }
                    res.send(obj)
                } else {

                    var obj = {
                        "status": 1,
                        "message": "Detail fetched successfully"
                    }
                    obj.customer_detail_verify = result;
                    res.send(obj)

                }
            })
    });

    ///////////

    app.post('/api/customer-flag-update/', (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

        con.query('UPDATE access_request SET access_flag=?, date_modified=? WHERE id=?',

            [

                1, current_date, req.body.id

            ],

            function (err, result) {
                if (err) {
                    var obj = {
                        "status": 1,
                        "message": "Something wrong!"
                    }
                    res.send(obj);
                } else {

                    con.query('SELECT customer_type FROM access_request WHERE id=? AND access_flag=?',

                        [

                            req.body.id, 1

                        ],

                        function (err, result2) {
                            if (err) {
                                var obj = {
                                    "status": 1,
                                    "message": "Something wrong!"
                                }
                                res.send(obj);
                            } else {
                                var obj = {
                                    "status": 1,
                                    "message": "Customer Verified"
                                }

                                obj.customer_type = result2[0].customer_type;
                                res.send(obj);
                            }
                        })


                }
            })
    });

    ///////////

    app.get('/api/direct-provider-type-master-data-value/', (req, res) => {
        con = require('../db');
        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Provider Type' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows,
                }
                res.send(obj);
            }
        })
    });

    //////////

    app.get('/api/direct-creditapplication-states/', (req, res) => {
        con = require('../db');
        con.query('SELECT state_id,name FROM states WHERE status=? AND delete_flag=? AND serving=? AND country_id=?',
            [
                1,
                0,
                1,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })

    /////////

    app.get('/api/direct-provider-country/', (req, res) => {
        con = require('../db');
        con.query("SELECT id, name FROM countries WHERE deleted_flag = 0 AND status = 1", function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows,
                }
                res.send(obj);
            }
        })
    });

    ////////

    app.get('/api/direct-provider-option/', (req, res) => {
        con = require('../db');
        var optionObj = {};

        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Bank Account Type' AND deleted_flag = 0 AND status = 1", ['Bank Account Type', 0, 1], function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {
                optionObj.bank_account_type = rows;
                con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Speciality', 0, 1], function (error, rows, fields) {
                    if (error) {
                        var obj = {
                            "status": 0,
                            "message": "Something wrong please try again."
                        }
                        res.send(obj);
                    } else {
                        optionObj.speciality = rows;
                        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Discount Type', 0, 1], function (error, rows, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                optionObj.discount_type = rows;
                                con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Fee Type', 0, 1], function (error, rows, fields) {
                                    if (error) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                    } else {
                                        optionObj.fee_type = rows;
                                        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Document Type', 0, 1], function (error, rows, fields) {
                                            if (error) {
                                                var obj = {
                                                    "status": 0,
                                                    "message": "Something wrong please try again."
                                                }
                                                res.send(obj);
                                            } else {
                                                optionObj.document_type = rows;
                                                con.query("SELECT user_type_role_id as mdv_id,value FROM user_type_role INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_role_id WHERE master_data_values.status=? AND master_data_values.deleted_flag=? AND mdv_type_id=?", [1, 0, 3], function (error, rows, fields) {
                                                    if (error) {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Something wrong please try again."
                                                        }
                                                        res.send(obj);
                                                    } else {
                                                        optionObj.user_role = rows;
                                                        con.query("SELECT user_type_role_id as mdv_id,value FROM user_type_role INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_role_id WHERE master_data_values.status=? AND master_data_values.deleted_flag=? AND mdv_type_id=?", [1, 0, 2], function (error, rows, fields) {
                                                            if (error) {
                                                                var obj = {
                                                                    "status": 0,
                                                                    "message": "Something wrong please try again."
                                                                }
                                                                res.send(obj);
                                                            } else {
                                                                optionObj.user_role = rows;
                                                                con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Payment Term Month' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
                                                                    if (error) {
                                                                        var obj = {
                                                                            "status": 0,
                                                                            "message": "Something wrong please try again."
                                                                        }
                                                                        res.send(obj);
                                                                    } else {
                                                                        optionObj.pvd_payment_term = rows;
                                                                        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Payment Term Month' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
                                                                            if (error) {
                                                                                var obj = {
                                                                                    "status": 0,
                                                                                    "message": "Something wrong please try again."
                                                                                }
                                                                                res.send(obj);
                                                                            } else {
                                                                                optionObj.pvd_loan_term_month = rows;
                                                                                con.query("SELECT profile_created FROM access_request WHERE id = ?", [req.query.access_request_id], function (error, rows, fields) {
                                                                                    if (error) console.log(error)
                                                                                    else {
                                                                                        optionObj.access_request_data = rows;
                                                                                        res.send(optionObj);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

    });

    ////////

    app.post('/api/direct-user-username-exist/', (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.user_id === undefined) {
            sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=?';
            var edit = 0;
        } else {
            sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=? AND user_id !=?';
            var edit = 1;
        }
        con.query(sql,
            [
                0,
                req.body.username,
                req.body.user_id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "exist": (rows.length > 0) ? 1 : 0,
                        "edit": edit
                    }
                    res.send(obj);

                }
            })
    });

    ///////

    app.post('/api/direct-provider-name-exist/', (req, res) => {
        con = require('../db');
        var sql = 'SELECT provider_id FROM provider WHERE deleted_flag=? AND name=?';

        con.query(sql,
            [
                0,
                req.body.provider_name,
            ]
            , function (error, rows, fields) {
                console.log(this.sql)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "exist": (rows.length > 0) ? 1 : 0
                    }
                    res.send(obj);

                }
            })
    });

    ////////

    app.post('/api/direct-ssn-exist/', (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.provider_id === undefined) {
            sql = 'SELECT tax_ssn_id FROM provider WHERE deleted_flag=? AND tax_ssn_id=?';
            var edit = 0;
        } else {
            sql = 'SELECT tax_ssn_id FROM provider WHERE deleted_flag=? AND tax_ssn_id=? AND provider_id !=?';
            var edit = 1;
        }
        con.query(sql,
            [
                0,
                req.body.ssn,
                req.body.provider_id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "exist": (rows.length > 0) ? 1 : 0,
                        "edit": edit
                    }
                    res.send(obj);

                }
            })
    });

    ///////

    app.get('/api/direct-provider-spl-procedure/', (req, res) => {
        con = require('../db');
        con.query('SELECT procedure_name, procedure_id FROM spec_procedure WHERE status=? AND deleted_flag=? AND spec_id=?',
            [
                1,
                0,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })

    ///////

    app.post('/api/direct-provider-insert/', dataupload, (req, res) => {
        con = require('../db');
        var addData = JSON.parse(req.body.addData);

        const md5 = require('md5');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = 0;
        let password = md5(addData.user_password);
        var obj = {};
        //console.log(current_user_id)
        console.log(JSON.parse(req.body.addData));
        //return false;
        // start Transaction
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
            }
            //PRIVIDER INSERT
            con.query('INSERT INTO provider SET '
                + 'provider_ac=((SELECT COALESCE(MAX(provider_ac),11111)+1 AS dt FROM provider as m)), '
                + 'name=?, '
                + 'provider_type=?,'
                + 'tax_ssn_id=?, '
                + 'phone=?, '
                + 'email=?, '
                + 'fax_no=?, '
                + 'website_url=?, '
                + 'primary_contact=?, '
                + 'primary_phone=?, '
                + 'member_flag=?, '
                + 'status=?, '
                + 'date_created=?, '
                + 'date_modified=?, '
                + 'created_by=?, '
                + 'modified_by=?',
                [
                    capitalize(addData.provider_name),
                    addData.provider_type,
                    addData.provider_taxid_ssn,
                    addData.provider_phone_no,
                    addData.provider_email.toLowerCase(),
                    addData.provider_fax_no,
                    addData.provider_website,
                    capitalize(addData.provider_primary_contact),
                    addData.provider_primary_contact_phone,
                    addData.provider_network,

                    addData.provider_status,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]
                , function (err, resultmain) {
                    if (err) {

                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong with provider table please try again.";
                        });
                    } else {
                        // INSERT USER
                        con.query('INSERT INTO user SET '
                            + 'f_name=?, '
                            + 'm_name=?, '
                            + 'l_name=?, '
                            + 'email_id=?, '
                            + 'user_type_role_id=?, '
                            + 'user_location=?, '
                            + 'client_id=?, '
                            + 'phone=?, '
                            + 'provider_primary_contact_flg=?, '
                            + 'username=?, '
                            + 'password=?, '
                            + 'status=?, '
                            + 'date_created=?, '
                            + 'date_modified=?, '
                            + 'created_by=?, '
                            + 'modified_by=?',
                            [

                                capitalize(addData.user_first_name),
                                capitalize(addData.user_middle_name),
                                capitalize(addData.user_last_name),
                                addData.user_email.toLowerCase(),
                                addData.user_role,
                                capitalize(addData.location[0].physical_location_name),
                                resultmain.insertId,
                                addData.user_phone_no,
                                addData.user_primary_contact_flag,
                                addData.username,
                                password,

                                addData.provider_status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                            ]
                            , function (err, resultuser) {
                                if (err) {

                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong with user table please try again.";
                                    });
                                } else {

                                    //LOCATION INSERT
                                    var arr = [];
                                    var cou = 1;
                                    addData.location.forEach(function (element, idx) {
                                        if (element.primary_address_flag == '') {
                                            primary_address_flag = 0;
                                        } else {
                                            primary_address_flag = element.primary_address_flag;
                                        }

                                        if (element.primary_billing_address_flag == '') {
                                            primary_billing_address_flag = 0;
                                        } else {
                                            primary_billing_address_flag = element.primary_billing_address_flag;
                                        }

                                        if (element.billing_address_flag == '') {
                                            billing_address_flag = 0;
                                        } else {
                                            billing_address_flag = element.billing_address_flag;
                                        }

                                        if (element.billing_address_flag == 0) {

                                            var as = cou;

                                            arr.push([
                                                resultmain.insertId,
                                                cou,
                                                null,
                                                capitalize(element.physical_address1),
                                                capitalize(element.physical_address2),
                                                element.physical_country,
                                                element.physical_state,
                                                element.physical_region,
                                                element.physical_county,
                                                capitalize(element.physical_city),
                                                element.physical_zip_code,
                                                capitalize(element.physical_location_name),
                                                element.physical_phone_no,
                                                primary_address_flag,
                                                0,
                                                billing_address_flag,

                                                addData.provider_status,
                                                current_date,
                                                current_date,
                                                current_user_id,
                                                current_user_id
                                            ]);
                                            cou++;
                                            arr.push([
                                                resultmain.insertId,
                                                cou,
                                                as,
                                                capitalize(element.billing_address1),
                                                capitalize(element.billing_address2),
                                                element.billing_country,
                                                element.billing_state,
                                                element.billing_region,
                                                element.billing_county,
                                                capitalize(element.billing_city),
                                                element.billing_zip_code,
                                                capitalize(element.billing_location_name),
                                                element.physical_phone_no,
                                                primary_address_flag,
                                                primary_billing_address_flag,
                                                billing_address_flag,

                                                addData.provider_status,
                                                current_date,
                                                current_date,
                                                current_user_id,
                                                current_user_id
                                            ]);
                                            cou++;
                                        } else {
                                            arr.push([
                                                resultmain.insertId,
                                                cou,
                                                cou,
                                                capitalize(element.physical_address1),
                                                capitalize(element.physical_address2),
                                                element.physical_country,
                                                element.physical_state,
                                                element.physical_region,
                                                element.physical_county,
                                                capitalize(element.physical_city),
                                                element.physical_zip_code,
                                                capitalize(element.physical_location_name),
                                                element.physical_phone_no,
                                                primary_address_flag,
                                                primary_billing_address_flag,
                                                billing_address_flag,

                                                addData.provider_status,
                                                current_date,
                                                current_date,
                                                current_user_id,
                                                current_user_id
                                            ]);
                                            cou++;
                                        }

                                    });

                                    con.query('INSERT INTO provider_location(provider_id, provider_location_id, parent_provider_location_id, address1, address2, country, state, region, county, city, zip_code, location_name, primary_phone, primary_address_flag, primary_billing_address_flag, billing_address_flag, status, date_created, date_modified, created_by, modified_by) VALUES ? '
                                        ,
                                        [
                                            arr
                                        ]
                                        , function (err, resultProviderLocation) {
                                            if (err) {

                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong with provider location table please try again.";
                                                });
                                            } else {

                                                //BANK INSERT
                                                addData.bank_details.forEach(function (element, idx) {

                                                    con.query('INSERT INTO provider_bank(provider_location_id, provider_id, bank_name, bank_address, bank_rounting_no, acct_no, name_on_acct, acct_type, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + capitalize(element.location_name) + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + capitalize(element.bank_name) + '", "' + capitalize(element.bank_address) + '", "' + element.rounting_no + '", "' + element.bank_accout + '", "' + capitalize(element.name_on_account) + '", ' + element.bank_account_type + ', ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                                        , function (err, resultProviderBank) {
                                                            if (err) {

                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong with provider location table please try again.";
                                                                });
                                                            }
                                                        });
                                                });
                                                //BANK INSERT END


                                                //SPECIALITY INSERT
                                                addData.speciality.forEach(function (element, idx) {

                                                    con.query('INSERT INTO provider_loc_spec(provider_location_provider_location_id, provider_location_provider_id, mdv_speciality_id, procedure_id, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + capitalize(element.spl_location_name) + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + element.specialization + '", "' + element.procedure + '", ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                                        , function (err, resultProviderLocationSpl) {
                                                            if (err) {

                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong with provider location table please try again.";
                                                                });
                                                            }
                                                        });
                                                });
                                                //SPECIALITY INSERT END

                                                //DOCTOR INSERT
                                                addData.speciality.forEach(function (element, idx) {

                                                    con.query('INSERT INTO doctors(f_name, l_name, location_id, provider_id, mdv_speciality_id, date_created, date_modified, created_by, modified_by) VALUES("' + capitalize(element.doctor_f_name) + '", "' + capitalize(element.doctor_l_name) + '", (SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + capitalize(element.spl_location_name) + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + element.specialization + '", "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                                        , function (err, resultDoctor) {

                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong with doctor table please try again.";
                                                                });
                                                            }
                                                        });

                                                });
                                                //DOCTOR INSERT END 

                                                //INSERT DISCOUNT RATE


                                                //INSERT DISCOUNT RATE END

                                                //INSERT DOC REPO  


                                                //INSERT DOC REPO END


                                                con.commit(function (err) {
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Something wrong please try again.";
                                                        });
                                                    }
                                                    obj.status = 1;
                                                    obj.message = "Application submitted for approval";
                                                    res.send(obj);
                                                });


                                            }

                                        });
                                    //LOCATION INSERT END

                                }

                            });
                        //USER INSERT END

                    }

                });
            //PROVIDER INSERT END
        });
    });


    ////////////////
    //Application//
    //////////////

    app.get('/api/direct-creditapplication-option/', (req, res) => {
        con = require('../db');
        var obj = {};
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
            }
            con.query('SELECT id,name FROM countries WHERE status=? AND deleted_flag=?',
                [
                    1,
                    0
                ]
                , function (error, countries) {
                    if (error) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.countries = countries;
                    }
                });
            con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Employment Type'
                ]
                , function (err, employment) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.employment = employment;
                    }

                });
            con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Bank Account Type'
                ]
                , function (err, bank_type) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.bank_type = bank_type;
                    }

                });

            con.query('SELECT profile_created FROM access_request WHERE id = ?',
                [
                    req.query.access_request_id
                ]
                , function (err, access_request_data) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.access_request_data = access_request_data;
                    }

                });

            // check provider id exists or not
            if (req.query.id !== undefined && req.query.id != '') {
                con.query('SELECT provider_id,provider_location_id,location_name FROM provider_location WHERE status=? AND provider_id=? AND (parent_provider_location_id IS NULL OR parent_provider_location_id=provider_location_id)',
                    [
                        1, req.query.id
                    ]
                    , function (err, location) {
                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                            });
                        } else {
                            obj.location = location;
                        }
                    });
            }
            con.query('SELECT status_id, value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Relationship'
                ]
                , function (err, relationship) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.relationship = relationship;
                    }

                });
            con.commit(function (err) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                    });
                }
                obj.status = 1;
                res.send(obj);
            });
        })
    });

    app.get('/api/direct-provider-creditapplication-states/', (req, res) => {
        con = require('../db');
        con.query('SELECT state_id,name FROM states WHERE status=? AND delete_flag=? AND serving=? AND country_id=?',
            [
                1,
                0,
                1,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })
    directAppliation = async (req, res, next) => {
        var obj = {};

        con = require('../db');
        // get experan login details
        var dataSql = require('./credit/creditSql.js');
        var experian = require('./credit/experianApi.js');
        var scroeCal = require('./credit/creditScore.js');
        req.body.current_user_id = 1;
        let expLogin;
        await dataSql.getExperianLoginDetail(con, (exResult) => {
            if (exResult.status == 0) {
                expLogin = 0;
                res.send(exResult);
            } else {
                expLogin = exResult;
            }
        });
        // stop script
        if (expLogin == 0) { return false }
        // Check experion login details.
        let expToken;
        await experian.experianLogin(req.body, expLogin.experian, function (login) {
            if (login.status == 0) {
                expToken = 0;
                res.send(login);
            } else {
                expToken = login;
            }
        })
        // stop script
        if (expToken == 0) { return false }

        // check custoemr exits or not
        await dataSql.existCustomer(con, req.body, 0, (exResult) => {
            req.body.patient_id = exResult;
        });
        // co-signer insert
        if (req.body.co_signer == 1) {
            await dataSql.existCoCustomer(con, req.body, 1, (exResult) => {
                req.body.co_patient_id = exResult;
            });
        }

        // check customer exists or not
        let cusCreditApp;
        let cusCreditAppCo;
        if (req.body.patient_id !== undefined && req.body.patient_id != '') {

            /**************** Check profile already runing or not*****************/
            let checkExists = 1;
            await dataSql.checkAppRuning(con, req.body, (exResult) => {
                if (exResult.status == 0) {
                    checkExists = 0;
                    res.send(exResult);
                }
            });
            if (checkExists == 0) { return false }
            //planRuning
            //res.send({ status: 0 })
            //return false;
            /**************** Check profile already runing or not*****************/

            /******* co-signer code area *****************/
            let cusAddCo;
            let checkCOInsert = 0;
            if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                await dataSql.updatePatitentCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id == '') {
                checkCOInsert = 1;
                req.body.co_patient_id = null;
                await dataSql.addpatientCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
                req.body.co_patient_id = (req.body.co_signer == 1) ? cusAddCo.mainPatientId : null;
            }
            /******* co-signer code area *****************/

            let cusAdd;
            // add customer basic details
            await dataSql.updatePatitentDetails(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAdd = 0;
                    res.send(exResult);
                } else {
                    cusAdd = exResult;
                }
            });
            // stop script
            if (cusAdd == 0) { return false }


            let cusUser;
            // add customer user details
            await dataSql.updatePatientUser(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusUser = 0;
                    res.send(exResult);
                } else {
                    cusUser = exResult;
                }
            });
            // stop script
            if (cusUser == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {

                let cusUserCo;
                // add customer user details
                await dataSql.updatePatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusUserCo;
                // add customer user details
                await dataSql.addPatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            let cusBank;
            // add customer bank details
            await dataSql.updatePatientBank(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusBank = 0;
                    res.send(exResult);
                } else {
                    cusBank = exResult;
                }
            });
            // stop script
            if (cusBank == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {
                let cusBankCo;
                // add customer user details
                await dataSql.updatePatientBank(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusBankCo;
                // add customer user details
                await dataSql.addPatientBank(con, req.body, 1, (exResult) => {

                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            let cusAddr;
            await dataSql.updatepatientAddress(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAddr = 0;
                    res.send(exResult);
                } else {
                    cusAddr = exResult;
                }
            });

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {
                let cusAddr;
                // add customer user details
                await dataSql.updatepatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusAddr;
                // add customer user details
                await dataSql.addpatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            }
            /******* co-signer code area *****************/

            // add credit application details
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            await dataSql.addCreditApp(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusCreditApp = 0;
                    res.send(exResult);
                } else {
                    cusCreditApp = exResult;
                }
            });
            // stop script
            if (cusCreditApp == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1) {
                req.body.co_signer_id = null;
                // add customer user details
                await dataSql.addCreditApp(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusCreditAppCo = 0;
                        res.send(exResult);
                    } else {
                        cusCreditAppCo = exResult;
                    }
                });
                // stop script
                if (cusCreditAppCo == 0) { return false }
            }
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            /******* co-signer code area *****************/

        } else {
            /******* co-signer code area *****************/
            let cusAddCo;
            let checkCOInsert = 0;
            if (req.body.co_signer == 1 && req.body.co_patient_id == '') {
                checkCOInsert = 1;
                await dataSql.addpatientCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
                req.body.co_patient_id = (req.body.co_signer == 1) ? cusAddCo.mainPatientId : null;
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                await dataSql.updatePatitentCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
            }

            /******* co-signer code area *****************/
            let cusAdd;
            // add customer basic details
            await dataSql.addpatientDetails(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAdd = 0;
                    res.send(exResult);
                } else {
                    cusAdd = exResult;
                }
            });
            // stop script
            if (cusAdd == 0) { return false }
            //res.send({status:0})
            //return false;
            req.body.patient_id = cusAdd.mainPatientId;
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusUserCo;
                // add customer user details
                await dataSql.addPatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusUserCo;
                // add customer user details
                await dataSql.updatePatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            let cusUser;
            // add customer user details
            await dataSql.addPatientUser(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusUser = 0;
                    res.send(exResult);
                } else {
                    cusUser = exResult;
                }
            });
            // stop script
            if (cusUser == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusBankCo;
                // add customer user details
                await dataSql.addPatientBank(con, req.body, 1, (exResult) => {

                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusBankCo;
                // add customer user details
                await dataSql.updatePatientBank(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            }
            /******* co-signer code area *****************/
            let cusBank;
            // add customer bank details
            await dataSql.addPatientBank(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusBank = 0;
                    res.send(exResult);
                } else {
                    cusBank = exResult;
                }
            });
            // stop script
            if (cusBank == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusAddr;
                // add customer user details
                await dataSql.addpatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusAddr;
                // add customer user details
                await dataSql.updatepatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            }
            /******* co-signer code area *****************/
            // add customer address details
            let cusAddr;
            await dataSql.addpatientAddress(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAddr = 0;
                    res.send(exResult);
                } else {
                    cusAddr = exResult;
                }
            });
            // stop script
            if (cusAddr == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1) {
                req.body.co_signer_id = null;
                // add customer user details
                await dataSql.addCreditApp(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusCreditAppCo = 0;
                        res.send(exResult);
                    } else {
                        cusCreditAppCo = exResult;
                    }
                });
                // stop script
                if (cusCreditAppCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            // add credit application details
            //let cusCreditApp;
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            await dataSql.addCreditApp(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusCreditApp = 0;
                    res.send(exResult);
                } else {
                    cusCreditApp = exResult;
                }
            });
            // stop script
            if (cusCreditApp == 0) { return false }
            /******************** */
        }
        /////////////////////////////////////
        ///////////////// REPATE CODE ///////
        /////////////////////////////////////
        /*********************
         * get and update co-signer details
         *********************/
        let expScoreDetailsCo = {};
        let expScoreCo;
        if (req.body.co_signer == 1) {

            await experian.experianApi(con, expLogin.experian[0].subscriberCode, expToken, req.body, 1, function (score) {
                if (score.status == 1) {
                    expScoreCo = score;
                } else if (score.status == 2) {
                    expScoreCo = score;
                } else {
                    expScoreCo = 0;
                    res.send(score);
                }
            })
            // stop script
            if (expScoreCo == 0) { return false }
            // get customer credit score according to experian data
            if (expScoreCo.status == 1) {
                await scroeCal.creditScore(con, expScoreCo, cusCreditApp.applicationID, req.body.employed, function (finalResult) {
                    if (finalResult.status == 0) {
                        expScoreDetailsCo = 0;
                        res.send(finalResult);
                    } else {
                        expScoreDetailsCo = finalResult;
                    }
                })
                // stop script
                if (expScoreDetailsCo == 0) { return false }
            } else {
                expScoreDetailsCo.applicationStatus = 2;
                expScoreDetailsCo.creditScroe = 0;
                expScoreDetailsCo.status = 1;
                expScoreDetailsCo.creditProfile = 2;
                expScoreDetailsCo.applicationId = cusCreditAppCo.applicationID;
                expScoreDetailsCo.message = 'Application approved for manual process.';
            }
            // add credit application details
            let cusCreditAppUpdateCo;
            await dataSql.applicationScore(con, cusCreditAppCo.applicationID, expScoreDetailsCo.applicationStatus, expScoreDetailsCo.creditScroe, req.body.current_user_id, expScoreDetailsCo, req.body.co_signer, function (done) {
                if (done.status == 0) {
                    cusCreditAppUpdateCo = 0;
                    res.send(done);
                } else {
                    cusCreditAppUpdateCo = done;
                }
            });
            // stop script
            if (cusCreditAppUpdateCo == 0) { return false }

            // add credit realtionship with co-signer
            let addCoRealtion;
            await dataSql.addCreditRelation(con, req.body, cusCreditApp.applicationID, function (done) {
                if (done.status == 0) {
                    addCoRealtion = 0;
                    res.send(done);
                } else {
                    addCoRealtion = done;
                }
            });
            // stop script
            if (addCoRealtion == 0) { return false }

        }
        /*********************
         * get and update co-signer details
         *********************/
        // get experion credit details
        let expScore;
        await experian.experianApi(con, expLogin.experian[0].subscriberCode, expToken, req.body, 0, function (score) {
            if (score.status == 1) {
                expScore = score;
            } else if (score.status == 2) {
                expScore = score;
            } else {
                expScore = 0;
                res.send(score);
            }
        })
        // stop script
        if (expScore == 0) { return false }
        let secDetails;

        await dataSql.addSecondaryDetails(con, req.body, cusCreditApp.applicationID, req.body.patient_id, function (sResult) {
            if (sResult.status == 0) {
                secDetails = 0;
                res.send(sResult);
            }
        })
        if (secDetails == 0) { return false }
        // get customer credit score according to experian data
        let expScoreDetails = {};
        if (expScore.status == 1) {
            await scroeCal.creditScore(con, expScore, cusCreditApp.applicationID, req.body.employed, function (finalResult) {
                if (finalResult.status == 0) {
                    expScoreDetails = 0;
                    res.send(finalResult);
                } else {
                    expScoreDetails = finalResult;
                }
            })
            // stop script
            if (expScoreDetails == 0) { return false }
        } else {
            expScoreDetails.applicationStatus = 2;
            expScoreDetails.creditScroe = 0;
            expScoreDetails.status = 1;
            expScoreDetails.creditProfile = 2;
            expScoreDetails.applicationId = cusCreditApp.applicationID;
            expScoreDetails.message = 'Application approved for manual process.';
        }


        if (req.body.co_signer == 1) {
            expScoreDetails.status = expScoreDetailsCo.status;
            expScoreDetails.message = expScoreDetailsCo.message;
            expScoreDetails.applicationStatus = expScoreDetailsCo.applicationStatus;
            //expScoreDetails.creditScroe = expScoreDetailsCo.creditScroe;
        }

        // add credit application details
        let cusCreditAppUpdate;
        await dataSql.applicationScore(con, cusCreditApp.applicationID, expScoreDetails.applicationStatus, expScoreDetails.creditScroe, req.body.current_user_id, expScoreDetailsCo, req.body.co_signer, function (done) {
            if (done.status == 0) {
                cusCreditAppUpdate = 0;
                res.send(done);
            } else {
                cusCreditAppUpdate = done;
            }
        });
        // stop script
        if (cusCreditAppUpdate == 0) { return false }
        // get application details
        //dataSql.getApplicationDetails(con, result.applicationId, function (result) {
        let cusAppDetails;
        await dataSql.getApplicationDetails(con, cusCreditApp.applicationID, function (getDetails) {
            if (getDetails.status == 0) {
                cusAppDetails = 0;
                res.send(getDetails);
            } else {
                cusAppDetails = getDetails;
            }
        });
        // stop script
        if (cusAppDetails == 0) { return false }
        /*
        * Create pdf document according to application status
        */
        const pdf = require('html-pdf');
        const dir = './uploads/provider/' + req.body.provider_id;
        var filename = dir + '/application-' + cusAppDetails.application.application_no + '.pdf';
        var fullUrl = req.get('origin');
        var logoimg = fullUrl + '/logo.png';
        // set pdf according to application status
        const path = require('path');
        var curentfile = {
            path: path.join(__dirname, '..', filename),
            customer_id: cusAppDetails.application.patient_ac,
        }
        let pdfCreateStatus;
        let templateEmail;
        if (expScoreDetails.applicationStatus == 1) {
            templateEmail = 'Application Approval';
            curentfile.filename = 'application-approval-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);


        } else if (expScoreDetails.applicationStatus == 2) {
            templateEmail = 'Application Manual';
            curentfile.filename = 'application-manual-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationManualStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);


        } else if (expScoreDetails.applicationStatus == 0) {
            templateEmail = 'Application Decline';
            curentfile.filename = 'application-declined-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationDeclinedStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);
            await dataSql.deleteBank(con, cusCreditApp.applicationID, (exResult) => {

            });

        }
        // stop script
        if (pdfCreateStatus == 0) { return false }
        var repoData = {
            application_id: cusAppDetails.application.application_id,
            current_user_id: req.body.current_user_id,
        }

        var oneDrive = require('./microsoft/oneDrive.js');
        //oneDrive.getOneDriveLogin(function (loginOne) {
        let oneDriveLogin;
        await oneDrive.getOneDriveLogin(function (loginOne) {
            if (loginOne.status == 0) {
                oneDriveLogin = 0;
                res.send(getDetails);
            } else {
                oneDriveLogin = loginOne;
            }
        });
        // stop script
        if (oneDriveLogin == 0) { return false }
        let driveuploadStatus;
        await oneDrive.getOneDriveUpload(oneDriveLogin.token, curentfile, function (one) {
            //res.send(one);
            if ((one.status == 1)) {
                curentfile.item_id = one.item_id;
                const fs = require('fs');
                fs.unlinkSync(curentfile.path)
            } else {
                driveuploadStatus = 0;
                curentfile.item_id = '';
                one.message = 'Currently can\'t upload on drive. Please try again'
                res.send(one);
            }
        })
        // stop script
        if (driveuploadStatus == 0) { return false }
        //dataSql.getApplicationDetails(con, result.applicationId, function (result) {
        let cusRepoDetails;
        await dataSql.documentApplcation(con, repoData, curentfile, function (result) {
            if (result.status == 0) {
                cusRepoDetails = 0;
                res.send(result);
            } else {
                cusRepoDetails = result;
            }
        });
        if (cusRepoDetails == 0) { return false }
        // Finaly get email templete for customer
        var emialTemp = require('./emailplan.js');
        let emailStatus;
        await emialTemp.customEmailTemplateDetails(templateEmail, function (emailResult) {
            if (emailResult.status == 0) {
                emailStatus = 0;
                res.send(emailResult);
            } else {
                emailStatus = emailResult;
            }
        })
        if (emailStatus == 0) { return false }

        var context = {
            customer_name: cusAppDetails.application.f_name + ' ' + cusAppDetails.application.m_name + ' ' + cusAppDetails.application.l_name,
            customer_address: cusAppDetails.application.address1 + ' ' + cusAppDetails.application.address2,
            customer_phone: cusAppDetails.application.phone_no,
            account_number: cusAppDetails.application.patient_ac,
            application_number: cusAppDetails.application.application_no,
            approved_amount: cusAppDetails.application.approve_amount,
            authorization_date: cusAppDetails.application.authorization_date,
            authorization_expiration: cusAppDetails.application.expiry_date
        };
        var emailOpt = {
            toEmail: cusAppDetails.application.email,
            subject: emailStatus.result[0].template_subject,
        }
        // save credit experion risk factor or customer details
        dataSql.applicationFactor(con, cusCreditApp.applicationID, expScore, req.body.current_user_id)
        if (req.body.co_signer == 1) {
            dataSql.applicationFactor(con, cusCreditAppCo.applicationID, expScoreCo, req.body.current_user_id)
        }
        var html = emailStatus.result[0].template_content;
        // finally send mail to customer
        emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {

            if (emailResult.status == 1) {
                var Eobj = {
                    "status": 1,
                    "message": "Email sent successfully"
                }
            } else {
                var Eobj = {
                    "status": 0,
                    "message": "Email not sent successfully!"
                }
            }
        });

        //console.log(cusCreditAppUpdate)
        res.send(expScoreDetails)
        //console.log(cusAppDetails)
        //res.send({ status: 0 })
        return false;
        //updatePatitentDetails

    }
    app.post('/api/direct-creditapplication-submit-provider/', directAppliation);
    app.post('/api/direct-creditapplication-submit-provider22/', (req, res) => {
        con = require('../db');
        var obj = {};
        //console.log('---------req.body---------')
        req.body.current_user_id = 1;
        req.body.provider_id = 2;
        //console.log(req.body)
        //return false;
        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        //dataSql.creditSql(con, req.body, function (result) {
        dataSql.getExperianLoginDetail(con, function (exResult) {
            if (exResult.status == 1) {
                var experian = require('./credit/experianApi.js');
                experian.experianLogin(req.body, exResult.experian, function (login) {
                    //check login success or not
                    if (login.status == 1) {
                        // get applcation score
                        //experian.experianApi(con, exResult.experian[0].subscriberCode, login, req.body, function (score) {
                        //if (score.status == 1) {
                        //console.log('login')
                        //console.log(login)
                        //save application if all details are correct
                        dataSql.DirectCreditSql(con, req.body, function (result) {
                            if (result.status == 1) {
                                //console.log('result')
                                //console.log(result)
                                experian.experianApi(con, exResult.experian[0].subscriberCode, login, req.body, function (score) {
                                    //console.log('score')
                                    //console.log(score)
                                    if (score.status == 1) {

                                        // check return scorss according to risk factor
                                        var scroeCal = require('./credit/creditScore.js');
                                        scroeCal.creditScore(con, score, result.applicationId, function (finalResult) {

                                            // save application factor
                                            dataSql.applicationScore(con, result.applicationId, finalResult.applicationStatus, finalResult.creditScroe, req.body.current_user_id, function (done) {
                                                dataSql.getApplicationDetails(con, result.applicationId, function (result) {
                                                    if (result.application.email != '') {
                                                        var emialTemp = require('./emailplan.js');

                                                        var context = {
                                                            customer_name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                                                            customer_address: result.application.address1 + ' ' + result.application.address2,
                                                            customer_phone: result.application.phone_no,
                                                            account_number: result.application.patient_ac,
                                                            application_number: result.application.application_no,
                                                            approved_amount: result.application.approve_amount,
                                                            authorization_date: result.application.authorization_date,
                                                            authorization_expiration: result.application.expiry_date
                                                        };
                                                        //var toEmailAddress = result.application.email;
                                                        if (finalResult.applicationStatus == 1) {
                                                            var template = 'Application Approval';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;
                                                                    //console.log(html)
                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 1,
                                                                                "message": "Email sent successfully"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        } else if (finalResult.applicationStatus == 0) {
                                                            var template = 'Application Decline';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;
                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 1,
                                                                                "message": "Email sent successfully"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        } else if (finalResult.applicationStatus == 2) {
                                                            var template = 'Application Manual';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;
                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email sent successfully!"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        }
                                                        // this code use for save document
                                                        const pdf = require('html-pdf');
                                                        const dir = './uploads/provider/' + req.body.provider_id;
                                                        var filename = dir + '/application-' + result.application.application_no + '.pdf';
                                                        //var fullUrl = req.protocol + '://' + req.get('host');
                                                        //var logoimg = fullUrl + '/uploads/logo.png';
                                                        var fullUrl = req.get('origin');
                                                        var logoimg = fullUrl + '/logo.png';

                                                        if (finalResult.applicationStatus == 1) {
                                                            const pdfTemplate = require('./credit/applicationStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-approval-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })


                                                                }
                                                            })
                                                        } else if (finalResult.applicationStatus == 0) {

                                                            const pdfTemplate = require('./credit/applicationDeclinedStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-declined-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        } else if (finalResult.applicationStatus == 2) {
                                                            //console.log('manual-----------))')
                                                            const pdfTemplate = require('./credit/applicationManualStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-manual-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }

                                                    }
                                                    //res.send(result);
                                                });
                                            })
                                            dataSql.applicationFactor(con, result.applicationId, score, req.body.current_user_id)

                                            res.send(finalResult);
                                        })

                                    } else if (score.status == 2) {
                                        /////////////\\\\\\\\
                                        obj.status = 1;
                                        obj.creditProfile = 2;
                                        obj.creditScroe = 0;
                                        obj.applicationId = result.applicationId;
                                        obj.applicationStatus = 2;
                                        obj.message = "Application approved for manual process.";
                                        //console.log('manual-------|||')
                                        dataSql.applicationScore(con, result.applicationId, 2, 0, req.body.current_user_id, function (done) {
                                            dataSql.getApplicationDetails(con, result.applicationId, function (result) {
                                                if (result.application.email != '') {
                                                    var emialTemp = require('./email.js');
                                                    var locals = {
                                                        name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                                                        address: result.application.address1 + ' ' + result.application.address2,
                                                        cityState: result.application.City + ', ' + result.application.name + ' - ' + result.application.zip_code,
                                                        phone: result.application.phone_no,
                                                        accountNumber: result.application.patient_ac,
                                                        applicationNumber: result.application.application_no,
                                                        amountApprove: result.application.approve_amount,
                                                        authDate: result.application.authorization_date,
                                                        expDate: result.application.expiry_date,
                                                    };
                                                    var toEmailAddress = result.application.email;
                                                    //if (finalResult.applicationStatus == 2) {
                                                    var template = 'applicationmanualemail';
                                                    emialTemp.emailTemplate(toEmailAddress, locals, template);
                                                    //}

                                                    // this code use for save document
                                                    const pdf = require('html-pdf');
                                                    const dir = './uploads/provider/' + req.body.provider_id;
                                                    var filename = dir + '/application-' + result.application.application_no + '.pdf';
                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    //if (finalResult.applicationStatus == 2) {

                                                    const pdfTemplate = require('./credit/applicationManualStatus');
                                                    pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                        } else {
                                                            var data = {
                                                                application_id: result.application.application_id,
                                                                current_user_id: req.body.current_user_id,
                                                            }
                                                            // onedrive
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'application-manual-' + result.application.application_no + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.application.patient_ac,
                                                            }
                                                            //console.log(curentfile)
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    console.log('drive login')
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //console.log(one);
                                                                        if ((one.status == 1)) {
                                                                            curentfile.item_id = one.item_id;
                                                                            const fs = require('fs');
                                                                            fs.unlinkSync(curentfile.path)
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                    //}

                                                }
                                                //res.send(result);
                                            });
                                        })
                                        //dataSql.applicationFactor(con, result.applicationId, score, req.body.current_user_id)

                                        res.send(obj);
                                        //return callback(obj);
                                        //////////\\\\\
                                    } else {
                                        res.send(score);
                                    }
                                })
                            } else {
                                res.send(result);
                            }
                        })

                        //} else {
                        //res.send(score);
                        //}
                        //})

                    } else {
                        res.send(login);
                    }
                })
            } else {
                res.send(exResult);
            }
        });
    });

}