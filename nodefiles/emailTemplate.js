/*
* Title: States
* Descrpation :- This module blong to question realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');

    app.get('/api/payment-reminder-email/', (req, res) => {
        console.log(req.query)
        con = require('../db');
        var emialTemp = require('./emailplan.js');
        
        emialTemp.customEmailTemplateDetails(req.query.template_name, function (Result) {
            console.log(Result)
            res.send(Result)
        });

        
        var context = {
            current_date: new Date(),
            provider_location: provider_location,
            customer_name: customer_name,
            principal_amount: principal_amount,
            interest_rate: interest_rate,
            customer_address: customer_address,
            customer_phone: customer_phone,
        };
        var toEmailAddress = 'amandeep@euclidesolutions.co.in';

        var html = '<div>Hi <strong>{{username}}</strong></div>';
        emialTemp.customEmailTemplate(toEmailAddress, context, html, function (emailResult) {
            console.log(emailResult)
        });

    })

    app.get('/api/email-template-variable/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT templates.template_id AS id, '
            + 'template_variable.variable '

            + 'FROM template_variable '

            + 'INNER JOIN master_template_variable '
            + 'ON master_template_variable.template_variable_id = template_variable.template_variable_id '

            + 'INNER JOIN templates '
            + 'ON templates.template_id = master_template_variable.template_id '

            + 'WHERE templates.template_id = ?',
            [req.query.temp_id]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })

    app.get('/api/email-template-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT template_id AS id, mdv_template_type AS template_type, template_name, template_subject, template_content, status FROM templates WHERE delete_flag = ? ORDER BY template_id DESC',
            [0]
            , function (error, rows, fields) {
                console.log(error)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT '
                        + 'value, '
                        + 'status_id '

                        + 'FROM master_data_values '
                        + 'WHERE md_id = ?'
                        ,
                        ['Template Type']
                        , function (err, tempType, fields) {
                            console.log(error)
                            if (err) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {

                                var obj = {
                                    "status": 1,
                                    "result": rows,
                                    "template_type": tempType
                                }
                                res.send(obj);

                            }
                        })

                }
            })
    })

    app.post('/api/email-template-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.id
         === undefined) {
          sql = 'SELECT template_name FROM templates WHERE template_name=?';
          var edit = 0;
        } else {
          sql = 'SELECT template_name FROM templates WHERE template_name=? AND template_id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            req.body.template_name,
            req.body.id
          ]
          , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {

                var obj = {
                    "status": 1,
                    "exist": (rows.length > 0) ? 1 : 0,
                    "edit": edit
                }
                res.send(obj);

            }
          })
    })

    app.post('/api/email-template-insert/', jwtMW, (req, res) => {
        con = require('../db');
        const Entities = require('html-entities').AllHtmlEntities;
        const entities = new Entities();

        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        var template_content = req.body.template_content.replace(/&nbsp;/g, ' ');
        con.query('INSERT INTO templates (mdv_template_type, template_name, template_subject, template_content, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
            [
                req.body.template_type,
                req.body.template_name,
                req.body.template_subject,
                entities.decode(template_content),
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_temp_id": result.insertId,
                        "template_type": req.body.template_type,
                        "template_name": req.body.template_name,
                        "template_subject": req.body.template_subject,
                        "template_content": req.body.template_content,
                        "temp_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/email-template-update/', jwtMW, (req, res) => {
        con = require('../db');
        const Entities = require('html-entities').AllHtmlEntities;
        const entities = new Entities();

        let now = new Date();
        //return false
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        var template_content = req.body.template_content.replace(/&nbsp;/g, ' ');
        //console.log(entities.decode(template_content))
        //return false
        con.query('UPDATE templates SET mdv_template_type=?, template_name=?, template_subject=?, template_content=?, status=?, date_modified=?, modified_by=? WHERE template_id=?',
            [
                req.body.template_type,
                req.body.template_name,
                req.body.template_subject,
                entities.decode(template_content),
                req.body.status,
                current_date,
                current_user_id,
                req.body.id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/email-template-delete/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE templates SET delete_flag=? WHERE template_id=?',
          [
            1,
            req.body.id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                "status": 0,
                "message": "Something wrong please try again."
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "id": req.body.id
              }
              res.send(obj);
    
            }
          })
      })

}
