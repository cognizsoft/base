module.exports = (data, planid, logoimg) => {
   //return false;
   let date = require('date-and-time');
   let moment = require('moment');
   let now = new Date();
   let current_date = date.format(now, 'MM/DD/YYYY');

   let totalLateAmount = 0;
   let totalFinCharge = 0;

   var groupArrays = data.invoiceDetails && data.invoiceDetails.map((dat, idx) => {

      if (moment(moment(dat.due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')) && dat.paid_flag != 1 && dat.paid_flag != 4) {
         dat.missed_flag = 1;
      } else {
         dat.missed_flag = 0;
      }
      dat.fin_charge_amt = (dat.fin_charge_amt != null) ? (dat.finpct != null) ? (parseFloat(dat.fin_charge_amt) - (parseFloat(dat.finpct) * parseFloat(dat.fin_charge_amt)) / 100) : parseFloat(dat.fin_charge_amt) : 0;
      dat.late_fee_received = (dat.late_fee_received != null) ? (dat.latepct != null) ? (parseFloat(dat.late_fee_received) - (parseFloat(dat.latepct) * parseFloat(dat.late_fee_received)) / 100) : parseFloat(dat.late_fee_received) : 0;

      dat.previous_fin_charge = (dat.previous_fin_charge != null) ? (dat.finpct != null) ? (parseFloat(dat.previous_fin_charge) - (parseFloat(dat.finpct) * parseFloat(dat.previous_fin_charge)) / 100) : parseFloat(dat.previous_fin_charge) : 0;
      dat.previous_late_fee = (dat.previous_late_fee != null) ? (dat.latepct != null) ? (parseFloat(dat.previous_late_fee) - (parseFloat(dat.latepct) * parseFloat(dat.previous_late_fee)) / 100) : parseFloat(dat.previous_late_fee) : 0;

      dat.total_due = dat.payment_amount;
      dat.paid_amount1 = (dat.paid_flag == 1 || dat.paid_flag == 4) ? (dat.paid_amount + dat.fin_charge_amt + dat.late_fee_received + dat.additional_amount + dat.previous_fin_charge + dat.previous_late_fee) : 0;
      dat.total_due += (dat.late_fee_received) ? dat.late_fee_received : 0;
      dat.total_due += (dat.fin_charge_amt) ? dat.fin_charge_amt : 0;
      dat.total_due += (dat.previous_late_fee) ? dat.previous_late_fee : 0;
      dat.total_due += (dat.previous_fin_charge) ? dat.previous_fin_charge : 0;

      if (idx != 0) {

         dat.previous_blc += (dat.previous_fin_charge != null) ? (dat.finpct != null) ? (parseFloat(dat.previous_fin_charge) - (parseFloat(dat.finpct) * parseFloat(dat.previous_fin_charge)) / 100) : parseFloat(dat.previous_fin_charge) : 0;
         dat.previous_blc += (dat.previous_late_fee != null) ? (dat.latepct != null) ? (parseFloat(dat.previous_late_fee) - (parseFloat(dat.latepct) * parseFloat(dat.previous_late_fee)) / 100) : parseFloat(dat.previous_late_fee) : 0;
      }
      if (dat.paid_flag == 1 || dat.paid_flag == 4) {
         totalLateAmount += dat.late_fee_received + dat.previous_late_fee;
         totalFinCharge += dat.fin_charge_amt + dat.previous_fin_charge;
      }
      dat.finalAmt = (dat.paid_amount1 <= dat.total_due) ? dat.total_due - dat.paid_amount1 : 0;
      return dat;
   })

   var uniqueLoanAmount = 0;
   data.application_plans && data.application_plans.reduce(function (accumulator, currentValue, currentindex) {
      if (!accumulator[currentValue.pp_id]) {
         accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
         uniqueLoanAmount += currentValue.loan_amount;
      }
      return accumulator;
   }, []);


   var uniqueLateCount = data.application_plans && [...new Set(data.application_plans.map(item => item.late_count))];
   var uniqueMissedCount = data.application_plans && [...new Set(data.application_plans.map(item => item.missed_count))];

   var UniqueReceived = data.amountDetails && data.amountDetails.reduce(function (accumulator, currentValue, currentindex) {
      if (currentindex == 0) {
         accumulator = 0;
      }
      accumulator += currentValue.amount_rcvd;
      return accumulator;
   }, []);

   var planDetails = data.application_plans && data.application_plans.reduce(function (accumulator, currentValue, currentindex) {
      if (!accumulator[currentValue.pp_id]) {
         // get recived amount
         var totalAmt = data.amountDetails && data.amountDetails.reduce(function (accumulator, currentplan, planindex) {
            if (planindex == 0) {
               accumulator = 0;
            }

            accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
            return accumulator;
         }, 0);
         accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.amount, emi: currentValue.installment_amt, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created };
      } else {
         //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
      }

      return accumulator;
   }, []);


   /////SINGLE PLAN PAYMENT HISTORY/////
   var action = 0;
   var totalPay = 0;
   var blacAmt = 0;

   var single_plan_pymnt = data.application_plans.filter(function (item) {
      return item.pp_id == planid;
   }).map(function ({ pp_id, pp_installment_id, amount, amount_rcvd, due_date, installment_amt, paid_flag, partial_paid, invoice_status, payment_date, paidOld }) {
      if (new Date() > new Date(due_date) || invoice_status == 1) {
         action = 0;
      } else if (action == 0) {
         action = 1;
         action = 1;
      } else {
         action = 2;
      }
      var current_installment = data.amountDetails && data.amountDetails.reduce(function (accumulator, currentplan, planindex) {
         if (planindex == 0) {
            accumulator['amountrcvd'] = 0;
            accumulator['paid_date'] = '-';
         }
         if (currentplan.pp_installment_id == pp_installment_id) {
            accumulator['amountrcvd'] += currentplan.amount_rcvd;
            accumulator['paid_date'] = currentplan.payment_date;
         }

         return accumulator;
      }, []);

      amount_rcvd = current_installment.amountrcvd;
      payment_date = current_installment.paid_date;
      totalPay += (amount_rcvd != null) ? amount_rcvd : 0;
      //blacAmt = amount.toFixed(2) - totalPay.toFixed(2);

      blacAmt = parseFloat(amount.toFixed(2)) - parseFloat(totalPay.toFixed(2));
      return { pp_id, pp_installment_id, amount, amount_rcvd, due_date, installment_amt, paid_flag, partial_paid, invoice_status, payment_date, action, totalPay, blacAmt, paidOld };
   });

   //console.log('single_plan_pymnt')
   //console.log(single_plan_pymnt)

   //PAYMENT PLAN DETAIL
   var pp_html = '';
   planDetails.forEach(function (row, idx) {
      pp_html = pp_html + '<tr class="' + ((row.pp_id == planid) ? 'selected-plan' : '') + '">'
         + '<td>' + row.pp_id + '</td>'
         + '<td>$' + parseFloat(row.loan_amount).toFixed(2) + '</td>'
         + '<td>$' + (row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2) + '</td>'
         + '<td>' + row.payment_term_month + ' Month</td>'
         + '<td>' + parseFloat(row.discounted_interest_rate).toFixed(2) + '%</td>'
         + '<td>$' + parseFloat(row.emi).toFixed(2) + '</td>'
         + '<td>' + row.date_created + '</td>'
         + '</tr>';
   })

   //PAYMENT HISTORY
   if (single_plan_pymnt) {

      var pym_history = '';
      single_plan_pymnt.forEach(function (row, idx) {
         pym_history = pym_history + '<tr>'
            + '<td>' + (idx + 1) + '</td>'
            + '<td>$' + ((row.installment_amt) ? parseFloat(row.installment_amt).toFixed(2) : '-') + '</td>'
            + '<td>' + row.due_date + '</td>'
            + '<td>' + ((row.amount_rcvd) ? '$' + parseFloat(row.amount_rcvd).toFixed(2) : '-') + '</td>'
            + '<td>' + ((row.payment_date) ? row.payment_date : '-') + '</td>'
            + '<td>' + ((row.totalPay > 0) ? '$' + parseFloat(row.totalPay).toFixed(2) : '-') + '</td>'
            + '<td>' + ((row.blacAmt) ? '$' + parseFloat(row.blacAmt).toFixed(2) : '-') + '</td>'
            + '<td>' + ((row.paid_flag == 1) ? 'Paid' : (row.partial_paid == 1) ? 'Partial Paid' : 'Unpaid') + '</td>'
            + '</tr>';
      })
   }
   let co_signer = '';
   if (data.application_details[0].co_patient_id != null) {
      co_signer = co_signer + '<div class="info-bx">'
         + '<div class="customer-info co-customer-info">'
         + '<table cellpadding="0" cellspacing="0" class="blue-tble">'
         + '<tbody>'
         + '<tr>'
         + '<th colSpan="3">Co-signer Information</th>'
         + '</tr>'
         + '<tr>'
         + '<td><strong>Name:</strong> ' + data.application_details[0].co_first_name + ' ' + data.application_details[0].co_middle_name + ' ' + data.application_details[0].co_last_name + '</td>'
         + '<td><strong>Address:</strong> ' + data.application_details[0].co_address1 + ' ' + data.application_details[0].co_address2 + ' ' + data.application_details[0].co_City + ' ' + data.application_details[0].co_state_name + '</td>'
         + '<td><strong>Phone:</strong> ' + data.application_details[0].co_phone_no + '</td>'
         + '</tr>'
         + '</tbody>'
         + '</table>'
         + '</div>';
   }
   return `
<!doctype html>
<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <title>PDF Result Template</title>
      <style>
        .selected-plan {
            background: #d7eeff;
        }
        
        .invoice-box {
         max-width: 1200px;
         margin: auto;
         padding: 0px;
         font-size: 8px;
         line-height: 15px;
         font-family: 'Arial', sans-serif;
         color: #303030;
         }
         .margin-top {
         margin-top: 50px;
         }
         .justify-center {
         text-align: center;
         }
         .invoice-box table {
         width: 100%;
         line-height: inherit;
         text-align: left;
      border:solid 1px #ddf1ff; border-collapse:collapse;}
      .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
      .customer-info{width:32%; float:left;}
      .customer-info tr td, .loan-info tr td, .payment-info tr td {border:none;}
      .loan-info{width:32%; float:left; margin-left:15px}
      .payment-info{width:32%; float:right;}
      .blue-tble td{background:#f1f9ff; padding:2px 5px; font-size:8px;}
      .blue-tble th{background:#0e5d97; color:#fff;padding:2px 5px; font-size:8px;}
      .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
      .info-bx:after{margin-bottom:20px;}
      .detail-table th{background:#0e5d97; color:#fff;padding:2px 2px; font-size:8px; line-height:10px;}
      .detail-table td{padding:2px 2px; font-size:8px;}
      .info-bx {display: flex}
      .text-center {text-align:center;}
      .white-space {white-space: nowrap;}
      .co-customer-info{width:100%}
      </style>
   </head>
   <body>
      <div class="invoice-box">
      <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
     <h1 class="justify-center">Customer Payment Plan</h1>

     <div class="info-bx">

        <div class="customer-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble" height="120px">
              <tr>
                 <th colspan="2">Customer Information</th>
              </tr>
              <tr>
                 <td class="white-space"><strong>Application No: </strong> </td><td>${data.application_details[0].application_no}</td>
              </tr>
              <tr>
                 <td><strong>Name: </strong> </td><td>${data.application_details[0].f_name + ' ' + data.application_details[0].m_name + ' ' + data.application_details[0].l_name}</td>
              </tr>
              <tr>
                 <td><strong>Address: </strong> </td><td>${data.application_details[0].address1 + ', ' + data.application_details[0].City + ', ' + data.application_details[0].name + ', ' + data.application_details[0].zip_code}</td>
              </tr>
              </tr>
                 <td><strong>Phone: </strong> </td><td>${data.application_details[0].peimary_phone}</td>
              </tr>
           </table>
        </div>
     
        <div class="loan-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble" height="120px">

              <tr>
                 <th colspan="2">Loan Information</th>
              </tr>
              </tr>
                 <td><strong>Line Of Credit :</strong> </td><td>$${parseFloat(data.application_details[0].approve_amount).toFixed(2)}</td>
              </tr>
              </tr>
                 <td><strong>Available Balance :</strong> </td><td>$${parseFloat(data.application_details[0].remaining_amount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Principal Amount :</strong> </td><td>$${parseFloat(uniqueLoanAmount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Outstanding Principal :</strong> </td><td>$${parseFloat(uniqueLoanAmount - UniqueReceived).toFixed(2)}</td>
              </tr>

           </table>
        </div>

        <div class="payment-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble" height="120px">

              <tr>
                 <th colspan="2">Payment Information</th>
              </tr>
              </tr>
                 <td><strong>Late Payments: </strong> </td><td>${uniqueLateCount.reduce((a, b) => a + b, 0)}</td>
              </tr>
              <tr>
                 <td><strong>Payments Missed: </strong> </td><td>${uniqueMissedCount.reduce((a, b) => a + b, 0)}</td>
              </tr>
              <tr>
                 <td><strong>Late Fees: </strong> </td><td>$${parseFloat(totalLateAmount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Financial Charges: </strong> </td><td>$${parseFloat(totalFinCharge).toFixed(2)}</td>
              </tr>
              
           </table>
        </div>

     </div>
     ${co_signer}
     <div class="detail-bx">
        <h2 class="justify-center">Payment Plans</h2>
        <div class="detail-table">
          <table cellpadding="0" cellspacing="0">
             
                <tr>
                   <th>Plan ID</th>
                   <th>Principal Amt</th>
                   <th>Outstanding Principal Amt</th>
                   <th>Payment Term</th>
                   <th>APR(%)</th>
                   <th>Monthly Payment</th>
                   <th>Date Created</th>
                </tr>
                ${pp_html}              
             
          </table>
     </div>
     
     <div class="detail-bx">
        <h2 class="justify-center">Payment Installments</h2>
        <div class="detail-table">
           <table cellpadding="0" cellspacing="0">
              <tr>
                 <th>S. No.</th>
                 <th>Amount Due</th>
                 <th>Due Date</th>
                 <th>Amount Paid</th>
                 <th>Payment Date</th>
                 <th>Total Paid</th>
                 <th>Balance</th>
                 <th>Status</th>
              </tr>
              ${pym_history}
           </table>
        </div>
     </div>
     
        
      </div>
   </body>
</html>
    `;
};