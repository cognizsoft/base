/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    //////////////////////////
    /////INSERT USER TYPE/////
    /////////////////////////

    app.post('/api/master-insert/', jwtMW, (req, res) => {
        con = require('../db');    
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO master_data (md_name, status, edit_allowed, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?)',
            [
                req.body.md_name,
                req.body.status,
                req.body.edit_allowed,
                current_user_id,
                current_user_id,
                current_date,
                current_date

            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_type_id": result.insertId,
                        "md_name": req.body.md_name,
                        "edit_allowed": req.body.edit_allowed,
                        "data_status": req.body.status
                    }
                    console.log(obj)
                    res.send(obj);

                }
            })
    })
    app.get('/api/master-list/', jwtMW, (req, res) => {
        con = require('../db');
        let result = {};
        con.query('select md_id, md_name, status, edit_allowed from master_data where deleted_flag = ? ORDER BY md_id DESC',
      [
        0
      ]
      , function (error, rows, fields) {
        if (error) {
            result.status = 0;
            result.message = "Something wrong please try again.";
          }
          else {
            result.status = 1;
            result.result = rows;
            res.send(result);
          }
      })
    });
    /*app.get('/api/master-list/', jwtMW, (req, res) => {
        let result = {};
        let parameterCount = [];
        let parameterQuery = [];
        console.log(req.query.filterList)
        //console.log(req.query.filterList['2'])
        req.query.page = (req.query.page === undefined) ? '0' : req.query.page;
        req.query.searchText = (req.query.searchText === undefined) ? '' : req.query.searchText;
        req.query.rowsPerPage = (req.query.rowsPerPage === undefined)? 10 : Number(req.query.rowsPerPage);
        parameterCount.push(0);
        if (req.query.searchText !== '') {
            parameterCount.push('%' + req.query.searchText + '%');
            parameterCount.push('%' + req.query.searchText + '%');
            if(req.query.searchText === 'active'){
                parameterCount.push('%1%');
            }else if(req.query.searchText === 'inactive'){
                parameterCount.push('%0%');
            }else{
                parameterCount.push('%' + req.query.searchText + '%');
            }
            if(req.query.searchText === 'yes'){
                parameterCount.push('%1%');
            }else if(req.query.searchText === 'no'){
                parameterCount.push('%0%');
            }else{
                parameterCount.push('%' + req.query.searchText + '%');
            }
            
        }
        let condationCount = ' deleted_flag = ? ';
        condationCount += (req.query.searchText !== '') ? 'AND (md_name LIKE ? OR md_id LIKE ? OR status LIKE ? OR edit_allowed LIKE ?) ' : '';

        con.query('select count(md_id) as total from master_data where' + condationCount,
            parameterCount,
            function (error, record, fields) {
                if (error) console.log(error)
                else {
                    result.status = 1;
                    result.total = record[0].total;
                }
            })
        parameterQuery.push(0);
        if (req.query.searchText !== '') {
            parameterQuery.push('%' + req.query.searchText + '%');
            parameterQuery.push('%' + req.query.searchText + '%');
            if(req.query.searchText === 'active'){
                parameterQuery.push('%1%');
            }else if(req.query.searchText === 'inactive'){
                parameterQuery.push('%0%');
            }else{
                parameterQuery.push('%' + req.query.searchText + '%');
            }
            if(req.query.searchText === 'yes'){
                parameterQuery.push('%1%');
            }else if(req.query.searchText === 'no'){
                parameterQuery.push('%0%');
            }else{
                parameterQuery.push('%' + req.query.searchText + '%');
            }
        }

        
        parameterQuery.push((req.query.page) * req.query.rowsPerPage);
        parameterQuery.push(req.query.rowsPerPage);

        let condation = ' deleted_flag = ? ';
        condation += (req.query.searchText !== '') ? 'AND (md_name LIKE ? OR md_id LIKE ? OR status LIKE ? OR edit_allowed LIKE ?) ' : '';
        condation += 'LIMIT ?, ?';
        console.log(parameterQuery)
        con.query('select md_id, md_name, status, edit_allowed from master_data where' + condation,
            parameterQuery,
            function (error, rows, fields) {
                if (error) console.log(error)
                else {
                    console.log(rows)
                    // get total records
                    result.result = rows;
                    res.send(result);
                }
            })

       

    })
    */
    app.post('/api/master-update/', jwtMW, (req, res) => {
        con = require('../db');

        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE master_data SET md_name=?, status=?, edit_allowed=?, modified_by=?, modified=? WHERE md_id=?',
            [
                req.body.md_name,
                req.body.status,
                req.body.edit_allowed,
                current_user_id,
                current_date,
                req.body.md_id,

            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/master-check-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.md_id === undefined) {
            sql = 'SELECT md_id FROM master_data WHERE md_name=?';
            var edit = 0;
        } else {
            sql = 'SELECT md_id FROM master_data WHERE md_name=? AND md_id !=?';
            var edit = 1;
        }
        con.query(sql,
            [
                req.body.value,
                req.body.md_id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 1,
                        "message": "Username or password mismatch."
                    }
                    res.send(obj);
                    
                } else {

                    var obj = {
                        "status": 1,
                        "exist": (rows.length > 0) ? 1 : 0 ,
                        "edit":edit
                    }
                    res.send(obj);

                }
            })
    })


}
