/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
  /*app.use(jwtMW)
  app.use(function (err, req, res, next) {
    console.log(err)
    if (err.name === 'UnauthorizedError') {
      var obj = {
         "status": 0,
         "message": "Invalid token."
      }
      res.status(401).send(obj);
    } else {
      next(err);
    }
  });*/

  app.get('/api/user-types/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('select mdv_id, value, description, status from master_data_values where deleted_flag = 0 AND md_id = "User Type" ORDER BY mdv_id DESC', function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      }
      else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////
  /////UPDATE USER TYPE/////
  /////////////////////////

  app.post('/api/user-types-update/', jwtMW, (req, res) => {
    con = require('../db');
    let user_type = req.body.value;
    let user_type_desc = req.body.description;
    let status = req.body.status;
    let mdv_id = req.body.mdv_id;
    let current_user_id = req.body.current_user_id;

    con.query('UPDATE master_data_values SET value="' + user_type + '", description="' + user_type_desc + '", status="' + status + '", modified=NOW(), modified_by="' + current_user_id + '" WHERE mdv_id="' + mdv_id + '"', function (err, result) {
      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1
        }
        res.send(obj);

      }
    })
  })

  //////////////////////////
  /////DELETE USER TYPE/////
  /////////////////////////

  app.post('/api/user-types-delete/', jwtMW, (req, res) => {
    con = require('../db');
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
    con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="' + current_user_id + '" WHERE type_id="' + type_id + '"', function (err, result) {
      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1
        }
        res.send(obj);

      }
    })
  })

  //////////////////////////
  /////INSERT USER TYPE/////
  /////////////////////////

  app.post('/api/user-types-insert/', jwtMW, (req, res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    con.query('INSERT INTO master_data_values (md_id, value, description, status, created_by, modified_by, created, modified) VALUES("User Type", "' + req.body.value + '", "' + req.body.user_type_desc + '", "' + req.body.status + '", "' + current_user_id + '", "' + current_user_id + '", NOW(), NOW())', function (err, result) {

      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "last_type_id": result.insertId,
          "user_type": req.body.value,
          "user_type_desc": req.body.user_type_desc,
          "user_type_status": req.body.status
        }
        res.send(obj);

      }
    })
  })

  //////////////////////////
  /////LAST INSERT ID USER TYPE/////
  /////////////////////////


  app.get('/api/user-types-last-insert-id/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT mdv_id FROM master_data_values WHERE deleted_flag = 0 AND md_id = "User Type" ORDER BY mdv_id DESC LIMIT 1', function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  app.post('/api/usertypes/submit', jwtMW, (req, res) => {
    con = require('../db');
    let user_type = req.body.usertype;
    let status = req.body.status;
    // insert new user type according to request
    con.query('INSERT INTO `user_types`(`user_type`, `status`) VALUES ("' + user_type + '",' + status + ')', function (error, rows, fields) {
      console.log(rows);
      console.log(error)
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        con.query('select type_id,user_type,status from user_types where delete_flag = 0', function (error, rows, fields) {
          if (error) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
              "status": 1,
              "result": rows,
            }
            res.send(obj);

          }
        })

      }
    })

  })

  app.post('/api/usertypes/getUserType', jwtMW, (req, res) => {
    con = require('../db');
    let user_type_id = req.body.user_type_id;
    // insert new user type according to request
    con.query('SELECT type_id,user_type,status FROM `user_types` WHERE type_id=' + user_type_id, function (error, rows, fields) {
      console.log(rows);
      console.log(error)
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);

      }
    })

  })

  app.post('/api/user-types-check-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.mdv_id === undefined) {
      sql = 'SELECT mdv_id FROM master_data_values WHERE md_id=? AND value=?';
      var edit = 0;
    } else {
      sql = 'SELECT mdv_id FROM master_data_values WHERE md_id=? AND value=? AND mdv_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        'User Type',
        req.body.value,
        req.body.mdv_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 1,
            "message": "Something wrong please try again."
          }
          res.send(obj);

        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })
  //other routes..
}
