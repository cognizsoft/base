/*
* Title: States
* Descrpation :- This module blong to question realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/question-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('select id, name, status from security_questions where delete_flag = ? ORDER BY id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })


    app.post('/api/question-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.id
         === undefined) {
          sql = 'SELECT name FROM security_questions WHERE name=?';
          var edit = 0;
        } else {
          sql = 'SELECT name FROM security_questions WHERE name=? AND id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            req.body.name,
            req.body.id
          ]
          , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {

                var obj = {
                    "status": 1,
                    "exist": (rows.length > 0) ? 1 : 0,
                    "edit": edit
                }
                res.send(obj);

            }
          })
    })


    app.post('/api/question-insert/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO security_questions (name, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?)',
            [
                req.body.name,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_question_id": result.insertId,
                        "name": req.body.name,
                        "question_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })
    app.post('/api/question-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE security_questions SET name=?, status=?, date_modified=?, modified_by=? WHERE id=?',
            [
                req.body.name,
                req.body.status,
                current_date,
                current_user_id,
                req.body.id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })
    app.post('/api/question-delete/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE security_questions SET delete_flag=? WHERE id=?',
          [
            1,
            req.body.id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                "status": 0,
                "message": "Something wrong please try again."
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "id": req.body.id
              }
              res.send(obj);
    
            }
          })
      })

}
