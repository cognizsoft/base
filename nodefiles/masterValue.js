/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('select md_name from master_data where deleted_flag = ? AND status = ? AND md_name NOT IN (?) ORDER BY md_name',
    [
      0,
      1,
      ['User Type','User Role','Interest Rate','Payment Term Month','Loan Amount']
    ]
    , function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/master-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('select mdv_id, md_id, value, description, status from master_data_values where deleted_flag = ? AND md_id NOT IN (?) ORDER BY mdv_id DESC',
    [
      0,
      ['User Type','User Role','Interest Rate','Payment Term Month','Loan Amount']
    ]
    , function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/master-value-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let master_value_name = req.body.name;
    let master_value = req.body.value;
    let master_value_desc = req.body.description;
    let master_value_status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE master_data_values SET md_id=?, value=?, description=?, status=?, modified=?, modified_by=? WHERE mdv_id=?',

         [
            
            req.body.md_id, req.body.value, req.body.description, req.body.status,current_date, current_user_id, req.body.mdv_id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })

  /////////////////////////////
  /////DELETE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/master-value-delete/', jwtMW,(req,res) => {
    con = require('../db');
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
     con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="'+current_user_id+'" WHERE type_id="'+type_id+'"', function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
                "status": 1
            }
            res.send(obj);
           
          }
     })
  })

  /////////////////////////////
  /////INSERT MASTER VALUE/////
  ////////////////////////////

  app.post('/api/master-value-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO master_data_values (md_id, value, description, status, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?,?)',
          [
            
              req.body.name, req.body.value, req.body.description, req.body.status,current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
               "status": 1,
               "last_value_id": result.insertId,
               "master_value_name": req.body.name,
               "master_value": req.body.value,
               "master_value_desc": req.body.description,
               "master_value_status": req.body.status
            }
            res.send(obj);
           
          }
     })
  })

  app.post('/api/mastervalue-check-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.mdv_id === undefined || req.body.mdv_id === "") {
        sql = 'SELECT mdv_id FROM master_data_values WHERE value=? AND md_id=?';
        var edit = 0;
    } else {
        sql = 'SELECT mdv_id FROM master_data_values WHERE value=? AND md_id=? AND mdv_id !=?';
        var edit = 1;
    }
    con.query(sql,
        [
            req.body.value,
            req.body.name,
            req.body.mdv_id
        ]
        , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 1,
                    "message": "Username or password mismatch."
                }
                res.send(obj);
                
            } else {

                var obj = {
                    "status": 1,
                    "exist": (rows.length > 0) ? 1 : 0 ,
                    "edit":edit
                }
                res.send(obj);

            }
        })
})
  

    //other routes..
}
