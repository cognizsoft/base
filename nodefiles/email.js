/*
* Title: email sent
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
var emailTemplate = function (toEmailAddress,locals,template) {
  var nodemailer = require('nodemailer');
  const Email = require('email-templates');
  const path = require('path');
  var transport = nodemailer.createTransport({
	/*host: 'ourhealthpartner.com',
    port: 587,
    secure: false,    //<<here
      auth: {
        user: "support@ourhealthpartner.com",
        pass: "Sp5cq12~"
      },*/
      tls: {
          rejectUnauthorized: false
      },
  });
  const email = new Email({
      message: {
          //from: 'info@ourhealthpartner.com'
          from: 'Health Partner Support <info@ourhealthpartner.com>'
      },
      send: true,
      juice: true,
        juiceResources: {
          preserveImportant: true,
          webResources: {
            relativeTo: path.join(__dirname, '..', 'emails/'+template+'/assets'),
            images: true
          }
        },
      transport
  });
  //var fullUrl = req.protocol + '://' + req.get('host');
  email.send({
      template: template,//'usersregister',
      message: {
          to: toEmailAddress
      },
      locals: locals,
      
      /*{
          name: 'Elon',
          url : fullUrl
      },*/

  })
  .then(console.log)
  .catch(console.error);
};

exports.emailTemplate = emailTemplate;

