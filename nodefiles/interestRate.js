/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function (app, jwtMW) {
  let date = require('date-and-time');




  /////////////////////////////////////////////////////////////////////
 /////////////////////////////START///////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.get('/api/interest_rate_list/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('select id, score, interest_rate as apr, risk_factor, status FROM mfs_apr WHERE deleted_flag = 0 ORDER BY id DESC', function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      }
      else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
})

app.post('/api/interest_rate_update/', jwtMW, (req, res) => {
    con = require('../db');
    console.log(req.body)
    //return false
    let score = req.body.score;
    let apr = req.body.apr;
    let risk_factor = req.body.risk_factor;
    let status = req.body.status;
    let id = req.body.id;
    let current_user_id = req.body.current_user_id;

    con.query('UPDATE mfs_apr SET score="' + score + '", interest_rate="' + apr + '", risk_factor="' + risk_factor + '", status="' + status + '", modified=NOW(), modified_by="' + current_user_id + '" WHERE id="' + id + '"', function (err, result) {
      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "message": "Interest Rate Updated Successfully."
        }
        res.send(obj);

      }
    })
})

app.post('/api/interest_rate_insert/', jwtMW, (req, res) => {
    con = require('../db');

    console.log('req.body')
    console.log(req.body)

    //return false;
    let current_user_id = req.body.current_user_id;
    con.query('INSERT INTO mfs_apr (score, interest_rate, risk_factor, status, created_by, modified_by, created, modified) VALUES("' + req.body.score + '", "' + req.body.apr + '", "' + req.body.risk_factor + '", "' + req.body.status + '", "' + current_user_id + '", "' + current_user_id + '", NOW(), NOW())', function (err, result) {

      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "last_mfs_apr_id": result.insertId,
          "score": req.body.score,
          "apr": req.body.apr,
          "risk_factor": req.body.risk_factor,
          "int_score_status": req.body.status
        }
        res.send(obj);

      }
    })
})

app.post('/api/interest_rate_check_exist/', jwtMW, (req, res) => {
    con = require('../db');
    console.log(req.body)
    let sql = '';
    if (req.body.id === undefined) {
      sql = 'SELECT id FROM mfs_apr WHERE score=?';
      var edit = 0;
    } else {
      sql = 'SELECT id FROM mfs_apr WHERE score=? AND id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        req.body.value,
        req.body.id
      ]
      , function (error, rows, fields) {
        console.log(error)
        if (error) {
          var obj = {
            "status": 1,
            "message": "Something wrong please try again."
          }
          res.send(obj);

        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
    })
})

  /////////////////////////////////////////////////////////////////////
 /////////////////////////////END///////////////////////////////////
/////////////////////////////////////////////////////////////////////


  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/interest-rate-master-data-value/', jwtMW, (req, res) => {
    con = require('../db');
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?',
        [
          'MFS Score',
          0,
          1
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.mfs_score = rows;
          }
        });
      con.query('SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?',
        [
          'Loan Amount',
          0,
          1
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.loan_amount = rows;
          }
        });
      con.query('SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?',
        [
          'Payment Term Month',
          0,
          1
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.payment_term_month = rows;
          }
        });
      con.query('SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?',
        [
          'Interest Rate',
          0,
          1
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.interest_rate = rows;
          }
        });
      con.commit(function (err) {
        if (err) {
          con.rollback(function () {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
          });
        } else {
          obj.status = 1;
        }
        res.send(obj);
      });
    });
  })


  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/interest-rate/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT interest_rate.id, '
    +'DATE_FORMAT(interest_rate.effective_date,"%Y-%m-%d") AS effective_date, '
    +'interest_rate.status, interest_rate.mdv_score_range AS score_range_id, '
    +'interest_rate.mdv_interest_rate as interest_rate_id, '
    +'interest_rate.mdv_loan_amount as loan_amount_id, '
    +'interest_rate.mdv_payment_term_month as payment_term_month_id, '
    +'rate.value AS rate_range, '
    +'loan.value AS loan_range, '
    +'term.value AS term_range, '
    +'score.value AS score_range '
    +'FROM interest_rate '
    +'INNER JOIN master_data_values as score ON interest_rate.mdv_score_range = score.mdv_id '
    +'INNER JOIN master_data_values as rate ON interest_rate.mdv_interest_rate = rate.mdv_id '
    +'INNER JOIN master_data_values as loan ON interest_rate.mdv_loan_amount = loan.mdv_id '
    +'INNER JOIN master_data_values as term ON interest_rate.mdv_payment_term_month = term.mdv_id '
    +'WHERE interest_rate.deleted_flag = 0 ORDER BY interest_rate.id DESC', function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/interest-rate-update/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let score_range_id = req.body.score_range_id;
    let interest_rate_id = req.body.interest_rate_id;
    let loan_amount_id = req.body.loan_amount_id;
    let payment_term_month_id = req.body.payment_term_month_id;
    let effective_date = req.body.effective_date;
    let score_status = req.body.status;
    let current_user_id = req.body.current_user_id;

    con.query('UPDATE interest_rate SET mdv_score_range=?, mdv_interest_rate=?, mdv_loan_amount=?, mdv_payment_term_month=?, effective_date=?, status=?, date_modified=?, modified_by=? WHERE id=?',

      [

        score_range_id, interest_rate_id, loan_amount_id, payment_term_month_id, effective_date, score_status, current_date, current_user_id, req.body.id

      ],

      function (err, result) {
        if (err) {
          var obj = {
              "status": 0,
              "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1
          }
          res.send(obj);

        }
      })
  })


  /////////////////////////////
  /////INSERT MASTER VALUE/////
  ////////////////////////////

  app.post('/api/interest-rate-insert/', jwtMW, (req, res) => {
    con = require('../db'); 
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.query('INSERT INTO interest_rate (mdv_score_range, mdv_interest_rate, mdv_loan_amount, mdv_payment_term_month, effective_date, status, created_by, modified_by, date_created, date_modified) VALUES(?,?,?,?,?,?,?,?,?,?)',
      [

        req.body.score_range, req.body.interest_rate, req.body.loan_amount, req.body.payment_term_month, req.body.effective_date, req.body.status, current_user_id, current_user_id, current_date, current_date

      ], function (err, result) {

        if (err) {
          var obj = {
              "status": 0,
              "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
              "status": 1,
              "last_value_id": result.insertId,
              "score_range": req.body.score_range,
              "rate_range": req.body.interest_rate,
              "loan_range": req.body.loan_amount,
              "term_range": req.body.payment_term_month,
              "effective_date": req.body.effective_date,
              "score_status": req.body.status
          }
          res.send(obj);

        }
      })
  })




  //other routes..
}
