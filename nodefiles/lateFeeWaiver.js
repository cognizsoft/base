/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  //////////////////////////////////////////////
  /////GET MASTER DATA VALUE WAIVER TYPE////////
  //////////////////////////////////////////////

  app.get('/api/late-fee-waiver-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, md_id, value FROM master_data_values WHERE md_id IN('Late Fee Waiver', 'Finance Charge Waiver') AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////////////////////////
  /////GET MASTER DATA VALUE REASON TYPE////////
  //////////////////////////////////////////////

  app.get('/api/late-fee-reason-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Waiver Reason Type' AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET LIST/////
  ////////////////////////////

  app.get('/api/late-fee-waiver/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT late_fee_waivers.id, late_fee_waivers.status, late_fee_waivers.mdv_waiver_type_id AS waiver_type_id, late_fee_waivers.reason_desc AS reason_type_id, master_data_values.md_id AS waiver_type, master_data_values.value AS waiver_value FROM late_fee_waivers INNER JOIN master_data_values ON late_fee_waivers.mdv_waiver_type_id = master_data_values.mdv_id WHERE late_fee_waivers.deleted_flag = 0 ORDER BY late_fee_waivers.id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/late-fee-waiver-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let waiver_type_id = req.body.waiver_type_id;
    let reason_type_id = req.body.reason_type_id;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE late_fee_waivers SET mdv_waiver_type_id=?, reason_desc=?, status=?, modified=?, modified_by=? WHERE id=?',

         [
            
            waiver_type_id, reason_type_id, status, current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
                "status": 1
            }
            res.send(obj);
           
          }
     })
  })

  /////////////////////////////
  /////INSERT Waiver/////
  ////////////////////////////

  app.post('/api/late-fee-waiver-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO late_fee_waivers (mdv_waiver_type_id, reason_desc, status, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?)',
          [
            
            req.body.waiver_type, req.body.reason_type, req.body.status, current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1,
           "last_value_id": result.insertId,
           "waiver_type": req.body.waiver_type,
           "reason_type": req.body.reason_type,
           "factor_status": req.body.status
          }
          res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
