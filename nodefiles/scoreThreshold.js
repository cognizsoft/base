/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/score-threshold-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Risk Factors' AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/score-threshold/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT id, name, value, description, DATE_FORMAT(effective_date,"%Y-%m-%d") AS effective_date, primary_flag as type, status FROM score_thresholds WHERE deleted_flag = 0 ORDER BY id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/score-threshold-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let name = req.body.name;
    let value = req.body.value;
    let description = req.body.description;
    let effective_date = req.body.effective_date;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE score_thresholds SET name=?, value=?, description=?, effective_date=?, primary_flag=?, status=?, modified=?, modified_by=? WHERE id=?',

         [
            
            req.body.name, req.body.value, req.body.description, req.body.effective_date, req.body.type, req.body.status, current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })


  ////////////////////
  ////CHECK EXIST/////
  ///////////////////

  app.post('/api/score-threshold-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.id
     === undefined) {
      sql = 'SELECT name FROM score_thresholds WHERE name=?';
      var edit = 0;
    } else {
      sql = 'SELECT name FROM score_thresholds WHERE name=? AND id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        req.body.name,
        req.body.id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
              "status": 0,
              "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })


  /////////////////////////////
  /////INSERT MASTER VALUE/////
  ////////////////////////////

  app.post('/api/score-threshold-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO score_thresholds (name, value, description, effective_date, primary_flag, status, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?,?,?,?)',
          [
            
            req.body.name, req.body.value, req.body.description, req.body.effective_date, req.body.type, req.body.status,current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
               "status": 1,
               "last_value_id": result.insertId,
               "name": req.body.name,
               "value": req.body.value,
               "description": req.body.description,
               "effective_date": req.body.effective_date,
               "st_status": req.body.status,
               "type": req.body.type
            }
            res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
