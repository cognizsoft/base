/*
* Title: States
* Descrpation :- This module blong to question realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');


    ///////////CUSTOMER/////////////
    app.get('/api/customer-answer-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT security_answers.id, security_answers.answers, security_answers.status, security_questions.id AS security_questions_id, security_questions.name, user.f_name, user.m_name, user.l_name, user.user_id '

            + 'FROM security_answers '
            + 'INNER JOIN security_questions ON security_questions.id=security_answers.security_questions_id '
            + 'INNER JOIN user ON user.user_id=security_answers.user_id '
            + 'WHERE security_answers.user_id = ? AND security_answers.delete_flag = 0 ORDER BY id DESC',
            [
            req.query.user_id,
            0
            ]
            , function (error, rows, fields) {
                //console.log(this.sql)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT id as security_questions_id,name '
                        + 'from security_questions '
                        + 'where status=? AND delete_flag = ? ORDER BY id DESC',
                        [1, 0]
                        , function (error, question, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                con.query('SELECT user_id,f_name,m_name,l_name '
                                    + 'from user '
                                    + 'where status=? AND delete_flag = ? ORDER BY user_id DESC',
                                    [1, 0]
                                    , function (error, users, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": rows,
                                                "question": question,
                                                "users": users,
                                            }
                                            res.send(obj);
                                        }
                                    })
                            }
                        })
                }
            })
    })

    app.post('/api/customer-answer-insert/', jwtMW, (req, res) => {
        console.log(req.body)
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO security_answers (user_id, security_questions_id, answers, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?)',
            [
                current_user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_answer_id": result.insertId,
                        "answers": req.body.answers,
                        "user_id": current_user_id,
                        "security_questions_id": req.body.security_questions_id,
                        "answer_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/customer-answer-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE security_answers SET user_id=?, security_questions_id=?, answers=?, status=?, date_modified=?, modified_by=? WHERE id=?',
            [
                req.body.user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_user_id,
                req.body.id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/customer-answer-delete/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE security_answers SET delete_flag=? WHERE id=?',
            [
                1,
                req.body.id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "id": req.body.id
                    }
                    res.send(obj);

                }
            })
    })

    ///////////PROVIDER/////////////
    app.get('/api/provider-answer-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT security_answers.id, security_answers.answers, security_answers.status, security_questions.id AS security_questions_id, security_questions.name, user.f_name, user.m_name, user.l_name, user.user_id '

            + 'FROM security_answers '
            + 'INNER JOIN security_questions ON security_questions.id=security_answers.security_questions_id '
            + 'INNER JOIN user ON user.user_id=security_answers.user_id '
            + 'WHERE security_answers.user_id = ? AND security_answers.delete_flag = 0 ORDER BY id DESC',
            [
            req.query.user_id,
            0
            ]
            , function (error, rows, fields) {
                //console.log(this.sql)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT id as security_questions_id,name '
                        + 'from security_questions '
                        + 'where status=? AND delete_flag = ? ORDER BY id DESC',
                        [1, 0]
                        , function (error, question, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                con.query('SELECT user_id,f_name,m_name,l_name '
                                    + 'from user '
                                    + 'where status=? AND delete_flag = ? ORDER BY user_id DESC',
                                    [1, 0]
                                    , function (error, users, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": rows,
                                                "question": question,
                                                "users": users,
                                            }
                                            res.send(obj);
                                        }
                                    })
                            }
                        })
                }
            })
    })

    app.post('/api/provider-answer-insert/', jwtMW, (req, res) => {
        console.log(req.body)
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO security_answers (user_id, security_questions_id, answers, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?)',
            [
                current_user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_answer_id": result.insertId,
                        "answers": req.body.answers,
                        "user_id": current_user_id,
                        "security_questions_id": req.body.security_questions_id,
                        "answer_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/provider-answer-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE security_answers SET user_id=?, security_questions_id=?, answers=?, status=?, date_modified=?, modified_by=? WHERE id=?',
            [
                req.body.user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_user_id,
                req.body.id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/provider-answer-delete/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE security_answers SET delete_flag=? WHERE id=?',
            [
                1,
                req.body.id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "id": req.body.id
                    }
                    res.send(obj);

                }
            })
    })

    ////////////ADMIN///////////////
    app.get('/api/answer-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT security_answers.id, security_answers.answers, security_answers.status, security_questions.id as security_questions_id, security_questions.name, user.username, user.f_name, user.m_name, user.l_name, user.user_id '
            + 'from security_answers '
            + 'INNER JOIN security_questions ON security_questions.id=security_answers.security_questions_id '
            + 'INNER JOIN user ON user.user_id=security_answers.user_id '
            + 'where security_answers.delete_flag = 0 ORDER BY id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT id as security_questions_id,name '
                        + 'from security_questions '
                        + 'where status=? AND delete_flag = ? ORDER BY id DESC',
                        [1, 0]
                        , function (error, question, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                con.query('SELECT user_id,f_name,m_name,l_name, user.username '
                                    + 'from user '
                                    + 'where status=? AND delete_flag = ? ORDER BY user_id DESC',
                                    [1, 0]
                                    , function (error, users, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": rows,
                                                "question": question,
                                                "users": users,
                                            }
                                            res.send(obj);
                                        }
                                    })
                            }
                        })
                }
            })
    })


    app.post('/api/answer-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        console.log(req.body.id)
        if (req.body.id === undefined) {
            sql = 'SELECT id FROM security_answers WHERE user_id = ? AND security_questions_id = ? AND delete_flag = ?';
            var edit = 0;
        } else {
            sql = 'SELECT id FROM security_answers WHERE user_id = ? AND security_questions_id = ? AND delete_flag = ? AND id != ?';
            var edit = 1;
        }
        con.query(sql,
            [
                req.body.user_id,
                req.body.question_id,                
                0,
                req.body.id
            ]
            , function (error, rows, fields) {
                console.log(this.sql)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "exist": (rows.length > 0) ? 1 : 0,
                        "edit": edit
                    }
                    res.send(obj);

                }
            })
    })


    app.post('/api/answer-insert/', jwtMW, (req, res) => {
        console.log(req.body)
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO security_answers (user_id, security_questions_id, answers, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?)',
            [
                req.body.user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_answer_id": result.insertId,
                        "answers": req.body.answers,
                        "user_id": req.body.user_id,
                        "security_questions_id": req.body.security_questions_id,
                        "answer_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })
    app.post('/api/answer-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE security_answers SET user_id=?, security_questions_id=?, answers=?, status=?, date_modified=?, modified_by=? WHERE id=?',
            [
                req.body.user_id,
                req.body.security_questions_id,
                req.body.answers,
                req.body.status,
                current_date,
                current_user_id,
                req.body.id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })
    app.post('/api/answer-delete/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE security_answers SET delete_flag=? WHERE id=?',
            [
                1,
                req.body.id
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {

                    var obj = {
                        "status": 1,
                        "id": req.body.id
                    }
                    res.send(obj);

                }
            })
    })

}
