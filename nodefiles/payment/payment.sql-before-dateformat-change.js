var getInvoicePlans = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'payment_plan.pp_id , '
            + 'payment_plan.interest_rate_id, '
            + 'payment_plan.discounted_interest_rate, '
            + 'payment_plan.application_id, '
            + 'payment_plan.amount, '

            + 'interest_rate.mdv_interest_rate, '
            + 'interest_rate.mdv_payment_term_month, '

            + 'credit_applications.patient_id, '
            + 'credit_applications.application_no, '
            + 'credit_applications.score, '
            + 'payment_plan.amount AS credit_amount, '

            + 'patient.f_name, '
            + 'patient.m_name, '
            + 'patient.l_name, '
            + 'patient.email, '
            + 'provider.name, '
            + 'patient.peimary_phone, '
            + 'patient.patient_ac, '
            + 'patient_address.address1, '
            + 'patient_address.address2, '
            + 'patient_address.City, '
            + 'patient_address.county, '
            + 'patient_address.region_id, '

            + '(SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice AS m) AS invoice_number, '

            + 'states.name AS region_name, '
            
            + 'pp_installments.installment_amt AS monthly_payment, '
            
            + 'DATE_FORMAT(payment_plan.date_created, "%d/%m/%Y") AS date_created, '
            + 'master_data_values.value AS interest_rate, '
            + 'master_data_values2.value AS payment_term, '
            + 'provider.member_flag '

            + 'FROM payment_plan '

            + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
            
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            
            + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id '
            /*+ 'INNER JOIN regions ON patient_address.region_id=regions.region_id '*/
            
            + 'INNER JOIN states ON patient_address.state_id=states.state_id '
            
            + 'INNER JOIN pp_installments ON payment_plan.pp_id=pp_installments.pp_id '
            
            + 'LEFT JOIN interest_rate ON interest_rate.id=payment_plan.interest_rate_id '
            
            + 'LEFT JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=interest_rate.mdv_payment_term_month '
            
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=interest_rate.mdv_interest_rate '
            
            + 'LEFT JOIN provider ON provider.provider_id=patient.provider_id '
            
            + 'WHERE '
            + 'payment_plan.application_id=?',
            [
                id
            ]
            , function (err, plan) {
                
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Payment plan not found.";
                        return callback(obj);
                    });
                } else {

                    con.query('SELECT '
                        + 'pp_installments.pp_id, '
                        + 'pp_installments.pp_installment_id, '
                        + 'pp_installments.installment_amt, '
                        + 'DATE_FORMAT(pp_installments.due_date, "%d/%m/%Y") AS due_date, '
                        + 'pp_installments.due_date AS comparison_date, '
                        + 'pp_installments.paid_flag, '
                        + 'pp_installments.missed_flag, '
                        + 'pp_installments.late_flag '

                        + 'FROM pp_installments '
                        + 'WHERE pp_installments.pp_id = ?'
                        ,
                        [
                            plan[0].pp_id
                        ]
                        , function (err, pymntPlanDetails) {

                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Payment Details not foundddd.";
                                    return callback(obj);
                                });
                            } else {


                                con.query('SELECT '
                                    + 'patient_invoice.invoice_id, '
                                    + 'patient_invoice.payment_amount, '
                                    + 'patient_invoice.additional_amount, '
                                    + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date, '
                                    + 'patient_invoice.additional_amount, '
                                    + 'patient_invoice.late_fee_received, '
                                    + 'patient_invoice.fin_charge_amt, '

                                    + 'patient.f_name, '
                                    + 'patient.m_name, '
                                    + 'patient.l_name, '
                                    + 'patient.peimary_phone, '

                                    + 'patient_address.address1, '
                                    + 'patient_address.address2, '
                                    + 'patient_address.City, '
                                    + 'patient_address.zip_code, '
                                    + 'states.name as state_name, '

                                    + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, '
                                    + 'pp_installments.paid_flag '

                                    + 'FROM patient_invoice '
                                    
                                    + 'INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id  '
                                    
                                    + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=pp_installment_invoice.pp_installment_id '
                                    
                                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
                                    
                                    + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
                                    
                                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                                    
                                    + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '

                                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                                    
                                    + 'WHERE credit_applications.application_id = ? AND patient_invoice.paid_flag = ? '
                                    
                                    + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC LIMIT 1'
                                    ,
                                    [
                                        id, 1
                                    ]
                                    , function (err, last_payment_info) {

                                        if (err) {
                                            /*con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Last Payment Details not found.";
                                                return callback(obj);
                                            });*/
                                            con.commit(function (err) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong please try again.";
                                                    });
                                                    return callback(obj);
                                                }
                                                obj.status = 1;
                                                obj.plan = plan;
                                                //obj.plan_details = planDetails;
                                                obj.payment_plan_details = pymntPlanDetails;
                                                obj.last_payment_info = ''
                                                return callback(obj);
                                            });
                                        } else {
                                            con.commit(function (err) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong please try again.";
                                                    });
                                                    return callback(obj);
                                                }
                                                obj.status = 1;
                                                obj.plan = plan;
                                                //obj.plan_details = planDetails;
                                                obj.payment_plan_details = pymntPlanDetails;
                                                obj.last_payment_info = last_payment_info
                                                return callback(obj);
                                            });
                                        }
                                    });
                            }
                        });

                }
            });
    });
    var obj = {};
}


var getAllPlansDetails = function (con, appid, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT credit_applications.application_no, '
            + 'credit_applications.patient_id, '
            + 'credit_applications.approve_amount, '
            + 'credit_applications.remaining_amount, '
            + 'credit_applications.expiry_date, '
            + 'patient.f_name, '
            + 'patient.m_name, '
            + 'patient.l_name, '
            + 'patient.peimary_phone, '
            + 'patient_address.address1, '
            + 'patient_address.City, '
            + 'patient_address.state_id '
            
            + 'FROM credit_applications '

            + 'INNER JOIN patient '
            + 'ON credit_applications.patient_id = patient.patient_id '

            + 'INNER JOIN patient_address '
            + 'ON patient_address.patient_id = patient.patient_id '

            + 'WHERE application_id = ?',
            [
            appid
            ]
            , function (err, app_details) {
                //console.log(this.sql)
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application not found.";
                        return callback(obj);
                    });
                } else {
                    obj.application_details = app_details;
                    con.query('SELECT payment_plan.pp_id, '
                        + 'payment_plan.interest_rate_id, '
                        + 'payment_plan.date_created, '
                        + 'payment_plan.amount, '
                        + 'payment_plan.loan_amount, '
                        + 'payment_plan.amount, '

                        + 'interest_rate.mdv_payment_term_month, '
                        + 'interest_rate.mdv_loan_amount, '
                        + 'interest_rate.mdv_interest_rate, '

                        + 'master_data_values.value AS payment_term_month, '
                        + 'master_data_values2.value AS interest_rate '

                        + 'FROM payment_plan '

                        + 'INNER JOIN interest_rate '
                        + 'ON payment_plan.interest_rate_id = interest_rate.id '

                        + 'INNER JOIN master_data_values '
                        + 'ON master_data_values.mdv_id = interest_rate.mdv_payment_term_month '

                        + 'INNER JOIN master_data_values AS master_data_values2 '
                        + 'ON master_data_values2.mdv_id = interest_rate.mdv_interest_rate '
                        
                        + 'WHERE payment_plan.application_id = ?',
                        [
                        appid
                        ]
                        , function (err, plans) {
                            //console.log('this.sql.plan')
                            //console.log(this.sql)
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Plan not found.";
                                    return callback(obj);
                                });
                            } else {
                                obj.application_plans = plans;
                                obj.plan_count = plans.length;
                                
                                
                                var innerSql = require('./inner.sql.js');
                                innerSql.AllPlansDetails(con, plans, function (result) {
                                    //console.log(result)
                                    if (result.status == 1) {
                                        obj.plan_count = 'No plan details';
                                        return callback(obj);
                                    } else {
                                        obj.pp_details = result;
                                        return callback(obj);
                                    }

                                });
                            }
                        });
                }
            });
});
var obj = {};
}


var getSingleInstallment = function (con, appId, planId, installmentId, callback) {
    var obj = {};
    con.query('SELECT '
        +'pp_installments.installment_amt, '
        +'pp_installments.pp_id, '
        +'pp_installments.pp_installment_id, '
        +'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
        +'pp_installments.paid_flag, '

        +'payment_plan.interest_rate_id, '
        +'payment_plan.discounted_interest_rate, '

        +'pp_installment_invoice.invoice_id, '
        
        +'interest_rate.mdv_interest_rate, '
        +'interest_rate.mdv_payment_term_month, '
        
        +'master_data_values.value AS interest_rate, '
        +'master_data_values2.value AS payment_term, '

        +'late_fee_waivers.mdv_waiver_type_id AS late_fee_reason_id, '
        +'late_fee_waivers2.mdv_waiver_type_id AS fin_charge_reason_id, '
        
        +'master_data_values3.value AS late_fee_pct, '
        +'master_data_values4.value AS fin_fee_pct, '

        /*+'patient_invoice.patient_payemnt_id, '*/
        +'patient_invoice.late_fee_received, '
        +'patient_invoice.late_fee_waivers_id, '
        +'patient_invoice.fin_charge_waiver_id, '
        +'patient_invoice.fin_charge_amt, '
        +'patient_invoice.additional_amount, '
        +'patient_invoice.payment_amount, '
        +'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS payment_date, '
        /*+'patient_invoice.additional_inst_amount, '*/
        +'patient_invoice.late_fee_waiver_comt, '
        +'patient_invoice.fin_charge_waiver_comt, '
        +'patient_invoice.comments, '
        +'patient_invoice.bank_name, '
        +'(SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice AS m) AS invoice_number, '
        +'patient_invoice.txn_ref_no '

        +'FROM pp_installments '
        +'INNER JOIN payment_plan '
        +'ON payment_plan.pp_id=pp_installments.pp_id '

        +'LEFT JOIN interest_rate '
        +'ON interest_rate.id=payment_plan.interest_rate_id '

        +'LEFT JOIN pp_installment_invoice '
        +'ON pp_installment_invoice.pp_id = pp_installments.pp_installment_id '
        +'AND pp_installment_invoice.pp_installment_id = pp_installments.pp_installment_id '

        +'LEFT JOIN patient_invoice '
        +'ON patient_invoice.invoice_id = pp_installment_invoice.invoice_id '

        +'LEFT JOIN late_fee_waivers '
        +'ON late_fee_waivers.id=patient_invoice.late_fee_waivers_id '
        
        +'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
        +'ON late_fee_waivers2.id=patient_invoice.fin_charge_waiver_id '

        +'LEFT JOIN master_data_values AS master_data_values3 '
        +'ON master_data_values3.mdv_id=late_fee_waivers.mdv_waiver_type_id '

        +'LEFT JOIN master_data_values AS master_data_values4 '
        +'ON master_data_values4.mdv_id=late_fee_waivers2.mdv_waiver_type_id '

        +'LEFT JOIN master_data_values AS master_data_values2 '
        +'ON master_data_values2.mdv_id=interest_rate.mdv_payment_term_month '

        +'LEFT JOIN master_data_values '
        +'ON master_data_values.mdv_id=interest_rate.mdv_interest_rate '

        +'WHERE payment_plan.application_id=? AND DATE_FORMAT(pp_installments.due_date, "%m") = ? AND DATE_FORMAT(pp_installments.due_date, "%Y") = ?'
        , [appId, planId, installmentId]
        , function (error,single_rows, fields){
            //console.log('this.sql.single.plan')
            //console.log(this.sql)
            if(error) console.log(error)
                else {
                    //console.log(single_rows)
                    obj.single_installment_payment = single_rows;
                    var arr_pp_ins_id = [];
                    single_rows.forEach(function(pp_ins_id){
                        if(pp_ins_id.pp_installment_id){
                            arr_pp_ins_id.push(pp_ins_id.pp_installment_id);
                        }
                    })
                    con.query('SELECT '
                        +'value AS late_fee '
                        +'FROM master_data_values '
                        +'WHERE md_id = ?'
                        ,['Late Fee']
                        ,function(err, late_fee_rows, fields) {
                          if(err) {
                            console.log(err)  
                        } else {
                            obj.payment_billing_late_fee = late_fee_rows
                            con.query('SELECT '
                                +'pp_installment_invoice.invoice_id, '

                                +'patient_invoice.payment_amount, '
                                +'patient_invoice.additional_amount, '
                                +'patient_invoice.late_fee_received, '
                                +'patient_invoice.late_fee_waiver_comt, '
                                +'patient_invoice.payment_date, '
                                +'patient_invoice.bank_name, '
                                +'patient_invoice.txn_ref_no, '
                                +'patient_invoice.late_fee_waivers_id, '
                                +'patient_invoice.fin_charge_amt, '
                                +'patient_invoice.fin_charge_waiver_id, '
                                +'patient_invoice.fin_charge_waiver_comt, '
                                +'patient_invoice.comments, '

                                +'late_fee_waivers.mdv_waiver_type_id AS fin_charge_reason_id, '
                                +'late_fee_waivers2.mdv_waiver_type_id AS late_fee_reason_id, '

                                +'master_data_values.value AS late_fee_pct, '
                                +'master_data_values2.value AS fin_fee_pct '

                                +'FROM pp_installment_invoice '
                                
                                +'INNER JOIN patient_invoice '
                                +'ON pp_installment_invoice.invoice_id = patient_invoice.invoice_id '

                                +'LEFT JOIN late_fee_waivers '
                                +'ON late_fee_waivers.id = patient_invoice.fin_charge_waiver_id '

                                +'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
                                +'ON late_fee_waivers2.id = patient_invoice.late_fee_waivers_id '

                                +'LEFT JOIN master_data_values '
                                +'ON master_data_values.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

                                +'LEFT JOIN master_data_values AS master_data_values2 '
                                +'ON master_data_values2.mdv_id = late_fee_waivers.mdv_waiver_type_id '

                                +'WHERE pp_installment_invoice.pp_installment_id IN(?) LIMIT 1'
                                ,[arr_pp_ins_id]
                                ,function(err, rows, fields) {
                                    //console.log(this.sql)
                                  if(err) {
                                    console.log(err)  
                                } else {
                                    obj.payment_invoice_detail = (rows.length>0) ? rows[0] : ''
                                    return callback(obj);
                                }

                            });
                        }

                    });
                }
            });
}

var getPaymentMasterFeeOption = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        +'mdv_id, '
        +'md_id AS waiver_type, '
        +'value AS waiver_value '
        +'FROM master_data_values '
        +'WHERE md_id = ? AND status = ?'
        , ['Late Fee Waiver', 1]
        , function (error,rows, fields){
            if(error) console.log(error)
                else {
                    optionObj.late_fee_waivers_option = rows;
                    con.query('SELECT '
                        +'mdv_id, '
                        +'md_id AS waiver_type, '
                        +'value AS waiver_value '
                        +'FROM master_data_values '
                        +'WHERE md_id = ? AND status = ?'
                        , ['Finance Charge Waiver', 1]
                        , function (error,rows, fields){
                            if(error) console.log(error)
                                else {
                                    optionObj.financial_charges_waiver_option = rows;
                                    con.query('SELECT '
                                        +'mdv_financial_charges_type, '
                                        +'charge '
                                        +'FROM financial_charges '
                                        +'WHERE deleted_flag = ? AND status = ?'
                                        , [0, 1]
                                        , function (error,rows, fields){
                                            if(error) console.log(error)
                                                else {
                                                    optionObj.financial_charges_option = rows;
                                                    con.query('SELECT '
                                                        +'mdv_id, '
                                                        +'value '
                                                        +'FROM master_data_values '
                                                        +'WHERE md_id = ? AND status = ?'
                                                        ,['Payment Method', 1]
                                                        ,function(err, rows, fields) {
                                                            if(err) {
                                                                console.log(err)
                                                            } else {
                                                                optionObj.payment_method_option = rows;
                                                                return callback(optionObj);
                                                            }
                                                        }

                                                        )
                                                }
                                            });
                                }
                            });
                }
            });

}

var getLateFeeWaiverType = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        +'late_fee_waivers.id, '
        +'late_fee_waivers.reason_desc, '
        +'master_data_values.md_id AS reason_type, '
        +'master_data_values.value AS max_waiver '
        +'FROM late_fee_waivers '
        +'INNER JOIN master_data_values '
        +'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '
        +'WHERE late_fee_waivers.mdv_waiver_type_id = ? AND late_fee_waivers.deleted_flag = ? AND late_fee_waivers.status = ?'
        , [id, 0, 1]
        , function (error,rows, fields){
        //console.log(this.sql)
        if(error) console.log(error)
            else {
                optionObj.late_fee_waiver_type_reason_option = rows;
                return callback(optionObj);
            }
        });

}

var getFinChargeWaiverType = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        +'late_fee_waivers.id, '
        +'late_fee_waivers.reason_desc, '
        +'master_data_values.value AS reason_type, '
        +'master_data_values.value AS max_waiver '
        +'FROM late_fee_waivers '
        +'INNER JOIN master_data_values '
        +'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '
        +'WHERE late_fee_waivers.mdv_waiver_type_id = ? AND late_fee_waivers.deleted_flag = ? AND late_fee_waivers.status = ?'
        , [id, 0, 1]
        , function (error,rows, fields){
        //console.log(this.sql)
        if(error) console.log(error)
            else {
                optionObj.fin_charge_waiver_type_reason_option = rows;
                return callback(optionObj);
            }
        });

}

var payInstallmentInsert = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    let sql;
    let data_sql;

    //console.log(data);
    console.log(data);
    console.log('---------data-------------');
    //return false;
    var lfeee;
    if(new Date(data.due_date)<new Date()) {
        //console.log('lfeee iffff')
        //console.log(data.late_fee_recieved)
        //console.log('---------')
        lfeee = data.late_fee_recieved;
        fchrg = data.fin_charge_amt;
    } else {
        //console.log('lfeee elseee')
        lfeee = 0;
        fchrg = 0;
    }
    if(!data.payment_installment_id) {

        sql = 'INSERT INTO patient_invoice (invoice_number, payment_amount, additional_amount, late_fee_received, late_fee_waiver_comt, payment_date, bank_name, txn_ref_no, late_fee_waivers_id, fin_charge_amt, fin_charge_waiver_comt, fin_charge_waiver_id, comments, created_by, modified_by, date_created, date_modified) VALUES(((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice as m)),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        data_sql = [data.payment_amount, data.additional_amount, lfeee, data.late_fee_comt, current_date, data.bank_name, data.txn_ref_no, data.late_fee_waiver_id, fchrg, data.finance_charge_commt, data.fin_charge_waiver_id, data.comments, data.current_user_id, data.current_user_id, current_date, current_date];

    } else {
        sql = 'UPDATE patient_invoice SET additional_amount=?, paid_flag=?, late_fee_received=?, late_fee_waiver_comt=?, payment_date=?, bank_name=?, txn_ref_no=?, late_fee_waivers_id=?, fin_charge_amt=?, fin_charge_waiver_id=?, fin_charge_waiver_comt=?, comments=?, modified_by=?, date_modified=? WHERE invoice_id=?';

        data_sql = [data.additional_amount, (data.payment_in == 1) ? 1 : (data.payment_in == 2) ? 2 : 0, lfeee, data.late_fee_comt, current_date, data.bank_name, data.txn_ref_no, data.late_fee_waiver_id, fchrg, data.fin_charge_waiver_id, data.finance_charge_commt, data.comments, data.current_user_id, current_date, data.payment_installment_id];
    }



    con.query(sql, data_sql
      , function (err,result){

          if(err) {
            console.log(err);
        } else {
            var obj = {
                "status": "payment detail added in patient_payment table"
            }
            
              ///////////////////////////////////////////////
             //INSERT IN pp_installment_invoice table data//
            ///////////////////////////////////////////////

            var arrppInsInv = [];

            data.allData.forEach(function(dat) {

                arrppInsInv.push([
                    result.insertId,
                    dat.pp_id, 
                    dat.pp_installment_id,
                    current_date,
                    current_date,
                    data.current_user_id,
                    data.current_user_id,
                    ]);
                
            });

            if(!data.payment_installment_id) {
                usql_pp = 'INSERT INTO pp_installment_invoice(invoice_id, pp_id, pp_installment_id, date_created, date_modified, created_by, modified_by) VALUES ?';
                data_usql_pp = [arrppInsInv];
            } else {
                usql_pp = 'UPDATE pp_installment_invoice SET invoice_id=?, date_modified=?, modified_by=? WHERE invoice_id=?';
                data_usql_pp = [data.payment_installment_id, current_date, data.current_user_id, data.payment_installment_id];
            }

            con.query(usql_pp, data_usql_pp
              , function (err, pp_installment_invoice){

                if(err) {
                    console.log(err);
                } else {
                    var obj = {
                     "status": "Inserted in pp_installment_invoice table",
                     "redirectReport": 1
                 }


                 /////////////////////////////////////
                //CHECK HIGH RISK RATE//////////////
                ///////////////////////////////////

                /*let max_payment_term = 0;
                var max_term_pp_id = '';
                var max_term_pp_installment_id = '';

                var allDataCount = data.allData.length;

                data.allData.forEach(function(dat) {

                    if( 0 === --allDataCount ) {
                    
                        if (parseInt(dat.payment_term) > max_payment_term) {
                            max_payment_term = parseInt(dat.payment_term);
                            max_term_pp_id = dat.pp_id;
                            max_term_pp_installment_id = dat.pp_installment_id;
                        }

                    }
                });*/

                con.query('SELECT '
                    +'pp_id '
                    
                    +'FROM payment_plan '
                    +'WHERE application_id = ?'
                    ,
                    [
                        data.app_id
                    ]
                    , function (err, high_risk_plan_ids) {
                        console.log('-------high_risk_plan_ids_sql-------')
                        console.log(this.sql)
                        if(err) {
                            console.log(err)
                        } else {
                            var obj = {
                             "status": "high_risk_plan_ids"
                            }
                /*        }
                });*/

                            var arr_plan_id = [];
                            var allDataCount = high_risk_plan_ids.length;

                            console.log('-------high_risk_plan_ids-------')
                            console.log(high_risk_plan_ids)

                            high_risk_plan_ids.forEach(function(plan_id){

                                //if( 0 === --allDataCount ) {
                                    if(plan_id.pp_id){
                                        arr_plan_id.push(plan_id.pp_id);
                                    }
                                //}

                            })

                            console.log('-------arr_plan_id-------')
                            console.log(arr_plan_id)
                                

                            //GET ALL INSTALLMENT BASED ON HIGH RISK RATE FOR UPDATE ADDITIONAL AMOUNT

                            con.query('SELECT '
                                +'pp_installments.pp_id, '
                                +'pp_installments.pp_installment_id, '
                                +'pp_installments.installment_amt, '
                                +'pp_installments.amount_paid, '
                                +'pp_installments.partial_paid, '

                                +'master_data_values.value '

                                +'FROM pp_installments '

                                +'INNER JOIN payment_plan '
                                +'ON payment_plan.pp_id = pp_installments.pp_id '

                                +'INNER JOIN interest_rate '
                                +'ON interest_rate.id = payment_plan.interest_rate_id '

                                +'INNER JOIN master_data_values '
                                +'ON master_data_values.mdv_id = interest_rate.mdv_payment_term_month '
                                
                                +'WHERE pp_installments.paid_flag != ? '
                                +'AND pp_installments.pp_id IN (?) '
                                
                                +'ORDER BY master_data_values.value ASC, '
                                +'pp_installments.pp_installment_id DESC'
                                ,
                                [
                                    1, arr_plan_id
                                ]
                                , function (err, high_risk_plan) {
                                    console.log('-------high_risk_plan_data-------')
                                    console.log(high_risk_plan)
                                    console.log(this.sql)
                                    if(err) {
                                        console.log(err)
                                    } else {
                                        var obj = {
                                         "status": "Update additional amount in pp_installments table",
                                         "redirectReport": 1
                                        }

                                        var arrAddAmt = [];

                                        var addAmtTot = parseFloat(data.additional_amount);

                                        var high_risk_plan_length = high_risk_plan.length;

                                        high_risk_plan.forEach(function(risk_plan) {
                                            
                                            if(addAmtTot > 0) {
                                                //console.log('addAmtTot')
                                                //console.log(addAmtTot)

                                              

                                                    /*if(risk_plan.amount_paid !== null && risk_plan.partial_paid == 1) {
                                                        addAmtTot = addAmtTot+risk_plan.amount_paid;
                                                    } */  

                                                    if(data.payment_in == 1) {
                                                        //console.log('ifffffff')
                                                        addAmtTot = parseFloat(addAmtTot+risk_plan.amount_paid).toFixed(2);
                                                        console.log('-------addAmtTot-------')
                                                        console.log(addAmtTot)
                                                        console.log(risk_plan.installment_amt)

                                                        var usqla = 'UPDATE pp_installments SET paid_flag=?, amount_paid=?, additional_amount=?, partial_paid=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';

                                                        var datau = [(addAmtTot >= risk_plan.installment_amt) ? 1 : 0, (addAmtTot >= risk_plan.installment_amt) ? risk_plan.installment_amt : addAmtTot, 0, (addAmtTot < risk_plan.installment_amt) ? 1 : 0, current_date, data.current_user_id, risk_plan.pp_id, risk_plan.pp_installment_id
                                                        ];

                                                    } else {
                                                       //console.log('elseeeee')

                                                       addAmtTot = addAmtTot+risk_plan.amount_paid;

                                                       var usqla = 'UPDATE pp_installments SET paid_flag=?, additional_amount=?, partial_paid=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';

                                                        var datau = [2, (addAmtTot >= risk_plan.installment_amt) ? risk_plan.installment_amt - risk_plan.amount_paid : addAmtTot, (addAmtTot < risk_plan.installment_amt) ? 1 : 0, current_date, data.current_user_id, risk_plan.pp_id, risk_plan.pp_installment_id
                                                        ]; 

                                                    }
                                                
                                                
                                                /*var usqla = 'UPDATE pp_installments SET paid_flag=?, amount_paid=?, partial_paid=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';

                                                var datau = [(data.payment_in == 2) ? 2 : ((addAmtTot >= risk_plan.installment_amt) ? 1 : 0), (addAmtTot >= risk_plan.installment_amt) ? risk_plan.installment_amt : addAmtTot, (addAmtTot < risk_plan.installment_amt) ? 1 : 0, current_date, data.current_user_id, risk_plan.pp_id, risk_plan.pp_installment_id
                                                ];*/


                                                con.query(usqla, datau
                                                  , function (err, update_additional_amt){
                                                    //console.log('this.sql.additional_amount')
                                                    //console.log(this.sql)
                                                    if(err) {
                                                        console.log(err);
                                                    } else {
                                                        //console.log('addAmtTot else')
                                                        //console.log(addAmtTot)
                                                        var obj = {
                                                         "status": "Additional amount updated",
                                                         "redirectReport": 1
                                                        }
                                                        if( 0 === --high_risk_plan_length ) {
                                                            callback(obj);
                                                        }
                                                    }

                                                });
                                                addAmtTot = (addAmtTot > risk_plan.installment_amt) ? (addAmtTot - risk_plan.installment_amt) : 0

                                                //}

                                            }
                                            

                                        });


                                        ////////////////////////////////////////
                                        //Flag Update in pp_installments table//
                                        ///////////////////////////////////////

                                         let usql;
                                         let data_sql;

                                         var arrUp = [];
                                         var allDlength = data.allData.length;
                                         data.allData.forEach(function(dat) {

                                            usql = 'UPDATE pp_installments SET paid_flag=?, missed_flag=?, late_flag=?, amount_paid=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';

                                            var dataa = [data.payment_in,0,(lfeee>0)?1:0,dat.installment_amt,current_date,data.current_user_id,dat.pp_id,dat.pp_installment_id
                                            ];

                                            con.query(usql, dataa
                                              , function (err, update_flag){
                                                if(err) {
                                                    console.log(err);
                                                } else {
                                                    var obj = {
                                                     "status": "paid flag updated",
                                                     "redirectReport": 1
                                                }
                                                /*if( 0 === --allDlength ) {
                                                    return callback(obj);
                                                }*/
                                             }

                                            });

                                            
                                        });

                                        return callback(obj);

                                    }
                                });
                             
                            

                        }
                });
                }

            });

            

        

            
            

        }
    })

}

exports.getPaymentMasterFeeOption = getPaymentMasterFeeOption;
exports.getLateFeeWaiverType = getLateFeeWaiverType;
exports.getFinChargeWaiverType = getFinChargeWaiverType;
exports.getSingleInstallment = getSingleInstallment;
exports.payInstallmentInsert = payInstallmentInsert;
exports.getAllPlansDetails = getAllPlansDetails;
exports.getInvoicePlans = getInvoicePlans;