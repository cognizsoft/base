const capitalize = (str) => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}

var getInvoicePlans = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }

        con.query('SELECT '
            + 'patient_invoice.invoice_number, '
            + 'patient_invoice.payment_amount, '
            + 'patient_invoice.previous_late_fee, '
            + 'patient_invoice.late_fee_received, '
            + 'patient_invoice.late_fee_due, '
            + 'patient_invoice.previous_fin_charge, '
            + 'patient_invoice.fin_charge_received, '
            + 'patient_invoice.fin_charge_due, '
            + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") AS due_date, '

            + 'payment_plan.pp_id, '
            + 'payment_plan.application_id, '
            + 'payment_plan.discounted_interest_rate, '
            + 'payment_plan.amount, payment_plan.loan_amount, '
            + 'payment_plan.term as term_month, '

            + 'credit_applications.patient_id, '

            + 'patient.patient_ac, '
            + 'patient.f_name, '
            + 'patient.m_name, '
            + 'patient.l_name, '
            + 'patient.email, '
            + 'patient.peimary_phone, '
            + 'patient.withdrawal_date, '

            + 'patient_address.address1, '
            + 'patient_address.City, '
            + 'patient_address.zip_code, '

            + 'states.name AS state, '

            //+ 'master_data_values.value AS term_month, '

            + 'patient_invoice_details.invoice_amount, '
            + 'patient_invoice_details.paid_flag '

            + 'FROM patient_invoice '

            + 'INNER JOIN patient_invoice_details '
            + 'ON patient_invoice_details.invoice_id = patient_invoice.invoice_id '

            + 'INNER JOIN payment_plan '
            + 'ON payment_plan.pp_id = patient_invoice_details.pp_id '

            + 'INNER JOIN credit_applications '
            + 'ON credit_applications.application_id = payment_plan.application_id '

            + 'INNER JOIN patient '
            + 'ON patient.patient_id = credit_applications.patient_id '

            + 'INNER JOIN patient_address '
            + 'ON patient_address.patient_id = credit_applications.patient_id '

            + 'INNER JOIN states '
            + 'ON states.state_id = patient_address.state_id '

            + 'WHERE patient_invoice.invoice_id = ?'
            , [id]
            , function (inv_err, invoice_dtl, fields) {

                if (inv_err) {
                    console.log(inv_err)
                } else {
                    obj.invoice_dtl = invoice_dtl;
                    con.query('SELECT '
                        + 'md_id, '
                        + 'value '

                        + 'FROM master_data_values '

                        + 'WHERE md_id IN ("Financial Charges", "Late Fee") AND status = 1'
                        , function (mdv_err, mdv_values, fields) {
                            if (mdv_err) {
                                console.log(mdv_err)
                            } else {
                                var lateFee = 0;
                                var finCharge = 0;
                                if (mdv_values.length > 0) {
                                    var late = mdv_values.filter(function (item) {
                                        return item.md_id == 'Late Fee';
                                    });
                                    lateFee = (late.length > 0) ? late[0].value : 0;
                                    var financial = mdv_values.filter(function (item) {
                                        return item.md_id == 'Financial Charges';
                                    });
                                    finCharge = (financial.length > 0) ? financial[0].value : 0;

                                }
                                obj.late_fee = lateFee;
                                obj.fin_charge = finCharge;
                                var appid = invoice_dtl[0].application_id;

                                con.query('SELECT '
                                    + 'payment_plan.pp_id, '
                                    + 'payment_plan.term, '
                                    + 'payment_plan.discounted_interest_rate, '
                                    + 'payment_plan.monthly_amount, '
                                    + 'payment_plan.loan_amount, '
                                    + 'payment_plan.remaining_amount '

                                    + 'FROM payment_plan '
                                    + 'INNER JOIN patient_invoice_details ON patient_invoice_details.pp_id=payment_plan.pp_id '

                                    + 'WHERE patient_invoice_details.invoice_id = ?'
                                    , [id]
                                    , function (mdv_err, plans, fields) {
                                        if (mdv_err) {
                                            console.log(mdv_err)
                                        } else {
                                            con.query('SELECT '
                                                + 'patient_invoice.invoice_id, '
                                                + 'patient_invoice.payment_amount, '
                                                + 'patient_invoice.additional_amount, '
                                                + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS payment_date, '
                                                + 'patient_invoice.additional_amount, '
                                                + 'patient_invoice.late_fee_received, '
                                                + 'patient_invoice.late_fee_due, '
                                                + 'patient_invoice.fin_charge_received, '
                                                + 'patient_invoice.fin_charge_due, '
                                                + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") AS due_date, '
                                                + 'patient_invoice.invoice_status, '

                                                + 'patient_invoice.previous_late_fee, '
                                                + 'patient_invoice.previous_fin_charge, '
                                                + 'patient_invoice.paid_amount, '

                                                + 'patient.f_name, '
                                                + 'patient.m_name, '
                                                + 'patient.l_name, '
                                                + 'patient.peimary_phone, '

                                                + 'patient_address.address1, '
                                                + 'patient_address.address2, '
                                                + 'patient_address.City, '
                                                + 'patient_address.zip_code, '

                                                + 'states.name as state_name, '

                                                + 'master_data_values.value AS mdv_late_percentage, '
                                                + 'master_data_values2.value AS mdv_fin_percentage '

                                                + 'FROM patient_invoice '

                                                + 'INNER JOIN credit_applications '
                                                + 'ON credit_applications.application_id = patient_invoice.application_id '

                                                + 'INNER JOIN patient '
                                                + 'ON patient.patient_id=credit_applications.patient_id '

                                                + 'INNER JOIN patient_address '
                                                + 'ON patient_address.patient_id=patient.patient_id '
                                                + 'AND patient_address.primary_address=1 '

                                                + 'INNER JOIN states '
                                                + 'ON states.state_id=patient_address.state_id '

                                                + 'LEFT JOIN late_fee_waivers '
                                                + 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

                                                + 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
                                                + 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '

                                                + 'LEFT JOIN master_data_values '
                                                + 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

                                                + 'LEFT JOIN master_data_values AS master_data_values2 '
                                                + 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

                                                + 'WHERE credit_applications.application_id = ? '
                                                + 'AND patient_invoice.invoice_status IN (?,?) '

                                                + 'GROUP BY patient_invoice.invoice_id '
                                                + 'ORDER BY patient_invoice.payment_date DESC LIMIT 1'
                                                ,
                                                [
                                                    appid, 1, 4
                                                ]
                                                , function (last_inv_err, invoice_last_pymt) {
                                                    if (err) {
                                                        console.log(last_inv_err)
                                                    } else {
                                                        con.commit(function (err) {
                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong please try again.";
                                                                });
                                                                return callback(obj);
                                                            }
                                                            obj.status = 1;
                                                            obj.plan = plans;
                                                            obj.amountDetailsInv = []
                                                            obj.plan_details = [];
                                                            obj.payment_plan_details = [];
                                                            obj.invoice_last_pymt = invoice_last_pymt;
                                                            return callback(obj);
                                                        });
                                                    }

                                                })
                                        }
                                    })
                            }
                        })

                }

            });


    });
    //var obj = {};
}



var getAllPlansDetails = function (con, appid, callback) {
    var obj = {};
    con.query('SELECT credit_applications.application_no, '
        + 'credit_applications.patient_id, '
        + 'credit_applications.approve_amount, '
        + '(credit_applications.remaining_amount+credit_applications.override_amount) as remaining_amount, '
        + 'credit_applications.expiry_date, '
        + 'patient.f_name, '
        + 'patient.m_name, '
        + 'patient.l_name, '
        + 'patient.peimary_phone, '
        + 'patient_address.address1, '
        + 'patient_address.City,patient_address.zip_code,patient.patient_ac, '
        + 'states.name, '
        + 'user.user_id '

        + 'FROM credit_applications '
        + 'INNER JOIN patient ON credit_applications.patient_id = patient.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN user ON credit_applications.patient_id = user.patient_id '
        + 'WHERE application_id = ?',
        [
            appid
        ]
        , function (err, app_details) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Application not found.";
                    return callback(obj);
                });
            } else {
                obj.application_details = app_details;
                con.query('SELECT payment_plan.pp_id, '
                    + 'payment_plan.term as payment_term_month, payment_plan.paid_flag, payment_plan.plan_number,'
                    + 'DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS date_created, '
                    + 'payment_plan.amount,payment_plan.discounted_interest_rate,payment_plan.monthly_amount,payment_plan.remaining_amount,payment_plan.installments_count, '
                    + 'payment_plan.loan_amount, payment_plan.late_count, payment_plan.missed_count,payment_plan.plan_updated, payment_plan.plan_status, payment_plan.note, payment_plan.created_by, '
                    + 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") AS procedure_date, master_data_values.value as status_name, '
                    + '(SELECT pp_id FROM provider_invoice_detial WHERE pp_id=patient_procedure.pp_id AND provider_invoice_id = 0) as invoice_exist, '
                    + '(SELECT mdv_invoice_status_id FROM provider_invoice INNER JOIN provider_invoice_detial ON provider_invoice_detial.provider_invoice_id=provider_invoice.provider_invoice_id WHERE provider_invoice_detial.pp_id=patient_procedure.pp_id AND provider_invoice_detial.status = 0) as provider_invoice_status, '
                    + '(SELECT DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id = patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id ORDER BY patient_invoice.invoice_id DESC LIMIT 1) as last_plan_due, '
                    + 'customer_refund.refund_id,customer_refund.refund_amt,customer_refund.payment_method,customer_refund.check_no,DATE_FORMAT(customer_refund.check_date, "%m/%d/%Y") as check_date,customer_refund.ach_bank_name,customer_refund.ach_routing_no,customer_refund.ach_account_no,customer_refund.comments as refund_comment,refund_methods.value as refundmethod, '
                    + 'provider_refund.refund_due as pro_refund_amt,provider_refund.iou_flag,provider_refund.payment_method as pro_payment_method,provider_refund.check_no as pro_provider_refund,DATE_FORMAT(provider_refund.check_date, "%m/%d/%Y") as pro_check_date,provider_refund.ach_bank_name as pro_ach_bank_name,provider_refund.ach_routing_no as pro_ach_routing_no,provider_refund.ach_account_no as pro_ach_account_no,provider_refund.comments as pro_refund_comment,provider_refund_methods.value as pro_refundmethod '

                    + 'FROM payment_plan '
                    + 'INNER JOIN master_data_values on master_data_values.status_id = payment_plan.plan_status '
                    + 'LEFT JOIN patient_procedure on patient_procedure.pp_id = payment_plan.pp_id '
                    + 'LEFT JOIN customer_refund on customer_refund.pp_id = payment_plan.pp_id '
                    + 'LEFT JOIN master_data_values as refund_methods on refund_methods.status_id = customer_refund.payment_method AND refund_methods.md_id="Payment Method" '
                    + 'LEFT JOIN provider_refund on provider_refund.pp_id = payment_plan.pp_id '
                    + 'LEFT JOIN master_data_values as provider_refund_methods on provider_refund_methods.status_id = provider_refund.payment_method AND provider_refund_methods.md_id="Payment Method" '

                    + 'WHERE payment_plan.application_id = ? AND master_data_values.md_id=?',
                    [
                        appid, 'Customer Plan Status'
                    ]
                    , function (err, plansDetails) {
                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Plan not found1";
                                return callback(obj);
                            });
                        } else {
                            obj.application_plans = plansDetails;
                            obj.amountDetails = [];
                            con.query('SELECT patient_invoice.invoice_number,patient_invoice.invoice_id,patient_invoice.payment_amount,patient_invoice.previous_blc,patient_invoice.previous_late_fee,patient_invoice.previous_fin_charge,'
                                + 'patient_invoice.additional_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_received,patient_invoice.fin_charge_due,patient_invoice.invoice_status,'
                                + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.paid_amount, patient_invoice.paid_amount, '
                                + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date, patient_invoice.on_fly, '
                                + 'invoicestatus.value as invoice_status_name '
                                //+ '(SELECT previous_blc FROM patient_invoice WHERE DATE_FORMAT(due_date, "%m/%d/%Y") = ?) as previous_month_blc '
                                + 'FROM patient_invoice '
                                + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '
                                + 'WHERE patient_invoice.application_id=? AND invoicestatus.md_id=? GROUP BY invoice_id'
                                , [appid, 'Customer Invoice Status'], function (err, invoiceDetails) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Plan not found.2";
                                            return callback(obj);
                                        });
                                    } else {
                                        obj.invoiceDetails = invoiceDetails;
                                        con.query('SELECT patient_invoice_details.invoice_id,patient_invoice_details.pp_id, DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date '
                                            + 'FROM patient_invoice_details '
                                            + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                            + 'WHERE patient_invoice_details.application_id=?'
                                            , [appid], function (err, invoicePlan) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Plan not found.3";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.invoicePlan = invoicePlan;
                                                    con.query('SELECT '
                                                        + 'credit_applications.application_id, '
                                                        + 'credit_applications.patient_id, '

                                                        + 'user.user_id, '

                                                        + 'security_answers.id AS security_answers_id, '
                                                        + 'security_answers.user_id, '
                                                        + 'security_answers.security_questions_id, '
                                                        + 'security_answers.answers, '

                                                        + 'security_questions.name '

                                                        + 'FROM credit_applications '

                                                        + 'INNER JOIN user '
                                                        + 'ON credit_applications.patient_id = user.patient_id '

                                                        + 'INNER JOIN security_answers '
                                                        + 'ON security_answers.user_id = user.user_id '

                                                        + 'INNER JOIN security_questions '
                                                        + 'ON security_questions.id = security_answers.security_questions_id '

                                                        + 'WHERE credit_applications.application_id = ? AND security_answers.status = ? AND security_answers.delete_flag = ?'
                                                        ,
                                                        [
                                                            appid, 1, 0
                                                        ]
                                                        , function (err, user_questions) {
                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Questions not found.";
                                                                    return callback(obj);
                                                                });
                                                            } else {
                                                                con.commit(function (err) {
                                                                    if (err) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Plan not found.";
                                                                            return callback(obj);
                                                                        });
                                                                    }
                                                                    obj.status = 1;
                                                                    obj.user_questions = user_questions;
                                                                    return callback(obj);
                                                                });
                                                            }
                                                        });
                                                }

                                            })


                                    }
                                })




                        }
                    })
            }
        })



    return false;

}

var getSingleInstallment = function (con, invoiceId, callback) {
    var obj = {};
    con.query('SELECT '
        + 'patient_invoice.invoice_id, '
        + 'patient_invoice.invoice_number, '
        + 'patient_invoice.payment_amount, '
        + 'patient_invoice.additional_amount, '
        + 'patient_invoice.paid_amount, '
        + 'patient_invoice.previous_blc, '
        + 'patient_invoice.previous_late_fee, '
        + 'patient_invoice.previous_fin_charge, '
        + 'patient_invoice.late_fee_received, '
        + 'patient_invoice.late_fee_due, '

        + 'patient_invoice.late_fee_waiver_comt, '
        + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") AS due_date, '

        + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS payment_date, '

        + 'patient_invoice.late_fee_waivers_id, '
        + 'patient_invoice.fin_charge_received, '
        + 'patient_invoice.fin_charge_due, '

        /*+'patient_invoice.patient_payemnt_id, '*/
        + 'patient_invoice.fin_charge_waiver_id, '
        + 'patient_invoice.fin_charge_waiver_comt, '
        + 'patient_invoice.comments, '
        + 'patient_invoice.invoice_status as paid_flag '

        + 'FROM patient_invoice '
        + 'WHERE patient_invoice.invoice_id = ?'
        , [invoiceId]
        , function (error, single_rows, fields) {
            if (error) console.log(error)
            else {
                if (single_rows[0].previous_late_fee == null) {
                    single_rows[0].previous_late_fee = 0
                }
                if (single_rows[0].previous_fin_charge == null) {
                    single_rows[0].previous_fin_charge = 0
                }

                obj.single_installment_payment = single_rows;
                //obj.single_installment_payment_edit = [];
                con.query('SELECT '
                    + 'patient_invoice.late_fee_waivers_id, '
                    + 'patient_invoice.fin_charge_waiver_id, '

                    + 'late_fee_waivers.mdv_waiver_type_id AS mdv_fin_id, '
                    + 'late_fee_waivers2.mdv_waiver_type_id AS mdv_late_id, '

                    + 'master_data_values.value AS mdv_fin_percentage, '
                    + 'master_data_values2.value AS mdv_late_percentage '

                    + 'FROM patient_invoice '

                    + 'LEFT JOIN late_fee_waivers '
                    + 'ON late_fee_waivers.id = patient_invoice.fin_charge_waiver_id '

                    + 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
                    + 'ON late_fee_waivers2.id = patient_invoice.late_fee_waivers_id '

                    + 'LEFT JOIN master_data_values '
                    + 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

                    + 'LEFT JOIN master_data_values AS master_data_values2 '
                    + 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

                    + 'WHERE patient_invoice.invoice_id = ?'
                    , [invoiceId]
                    , function (err, single_rows_edit, fields) {

                        if (err) {
                            console.log(error)
                        } else {
                            obj.single_installment_payment_edit = single_rows_edit;

                            con.query('SELECT '
                                + 'pp_id, '
                                + 'pp_installment_id, '
                                + 'invoice_id '

                                + 'FROM invoice_installments '

                                + 'WHERE invoice_id = ?'
                                , [invoiceId]
                                , function (error, invoice_rows, fields) {
                                    if (error) {
                                        console.log(error)
                                    } else {
                                        obj.single_invoice_rows = invoice_rows;
                                        con.query('SELECT '
                                            + 'application_id '

                                            + 'FROM patient_invoice '
                                            + 'WHERE invoice_id=?'
                                            , [invoiceId]
                                            , function (error, invoice_rows_appid, fields) {
                                                if (error) {
                                                    console.log(error)
                                                } else {
                                                    obj.invoice_rows_appid = invoice_rows_appid;
                                                    con.query('SELECT '
                                                        + 'amount_paid, payment_method, bank_name, ach_bank_name, payment_id, check_no, DATE_FORMAT(check_date, "%m/%d/%Y") AS check_date, ach_routing_no, ach_account_no, status '
                                                        + 'FROM patient_invoice_payment '
                                                        + 'WHERE invoice_id=? ORDER BY payment_id DESC LIMIT 1'
                                                        , [invoiceId]
                                                        , function (error, paymentDetails, fields) {
                                                            if (error) {
                                                                console.log(error)
                                                            } else {
                                                                if (paymentDetails.length > 0) {
                                                                    obj.single_installment_payment[0].amount_paid = paymentDetails[0].amount_paid;
                                                                    obj.single_installment_payment[0].payment_method = paymentDetails[0].payment_method;
                                                                    obj.single_installment_payment[0].bank_name = paymentDetails[0].bank_name;
                                                                    obj.single_installment_payment[0].ach_bank_name = paymentDetails[0].ach_bank_name;
                                                                    obj.single_installment_payment[0].payment_id = paymentDetails[0].payment_id;
                                                                    obj.single_installment_payment[0].check_no = paymentDetails[0].check_no;
                                                                    obj.single_installment_payment[0].check_date = paymentDetails[0].check_date;
                                                                    obj.single_installment_payment[0].ach_routing_no = paymentDetails[0].ach_routing_no;
                                                                    obj.single_installment_payment[0].ach_account_no = paymentDetails[0].ach_account_no;
                                                                    obj.single_installment_payment[0].last_invoice_status = paymentDetails[0].status;
                                                                } else {
                                                                    obj.single_installment_payment[0].amount_paid = null;
                                                                    obj.single_installment_payment[0].payment_method = null;
                                                                    obj.single_installment_payment[0].bank_name = null;
                                                                    obj.single_installment_payment[0].ach_bank_name = null;
                                                                    obj.single_installment_payment[0].payment_id = null;
                                                                    obj.single_installment_payment[0].check_no = null;
                                                                    obj.single_installment_payment[0].check_date = null;
                                                                    obj.single_installment_payment[0].ach_routing_no = null;
                                                                    obj.single_installment_payment[0].ach_account_no = null;
                                                                    obj.single_installment_payment[0].last_invoice_status = null;
                                                                }
                                                                con.query('SELECT '
                                                                    + 'payment_id, status as prv_invoice_status, amount_paid, '
                                                                    + 'patient_invoice_payment.amount_paid as total_recived, payment_method, ach_bank_name, ach_routing_no, ach_account_no, comments, check_no, DATE_FORMAT(check_date, "%m/%d/%Y") as check_date, DATE_FORMAT(date_modified, "%m/%d/%Y") as payment_date '
                                                                    + 'FROM patient_invoice_payment '
                                                                    //+ 'WHERE (status=1 OR status=4) AND invoice_id=? ORDER BY payment_id DESC LIMIT 1'
                                                                    + 'WHERE (status=1 OR status=4) AND invoice_id=? ORDER BY payment_id DESC'
                                                                    , [invoiceId]
                                                                    , function (error, paymentPaidDetails, fields) {
                                                                        console.log(this.sql)
                                                                        if (error) {
                                                                            console.log(error)
                                                                        } else {
                                                                            if (paymentPaidDetails.length > 0) {
                                                                                //obj.single_installment_payment[0].prv_invoice_status = paymentPaidDetails[0].status;
                                                                                //obj.single_installment_payment[0].total_recived = (paymentPaidDetails[0].total_recived != null)?paymentPaidDetails[0].total_recived:0;
                                                                                obj.single_installment_payment[0].prv_invoice_details = paymentPaidDetails;
                                                                            } else {
                                                                                obj.single_installment_payment[0].prv_invoice_details = null;
                                                                                //obj.single_installment_payment[0].prv_invoice_status = null;
                                                                                //obj.single_installment_payment[0].total_recived = 0;
                                                                            }

                                                                            return callback(obj);
                                                                        }
                                                                    })


                                                            }
                                                        })

                                                }
                                            });
                                        //return callback(obj);  
                                    }
                                });

                        }
                    })
            }
        });
}


var getPaymentMasterFeeOption = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        + 'mdv_id, '
        + 'md_id AS waiver_type, '
        + 'value AS waiver_value '
        + 'FROM master_data_values '
        + 'WHERE md_id = ? AND status = ?'
        , ['Late Fee Waiver', 1]
        , function (error, rows, fields) {
            if (error) console.log(error)
            else {
                optionObj.late_fee_waivers_option = rows;
                con.query('SELECT '
                    + 'mdv_id, '
                    + 'md_id AS waiver_type, '
                    + 'value AS waiver_value '
                    + 'FROM master_data_values '
                    + 'WHERE md_id = ? AND status = ?'
                    , ['Finance Charge Waiver', 1]
                    , function (error, rows, fields) {
                        if (error) console.log(error)
                        else {
                            optionObj.financial_charges_waiver_option = rows;
                            con.query('SELECT '
                                + 'mdv_financial_charges_type, '
                                + 'charge '
                                + 'FROM financial_charges '
                                + 'WHERE deleted_flag = ? AND status = ?'
                                , [0, 1]
                                , function (error, rows, fields) {
                                    if (error) console.log(error)
                                    else {
                                        optionObj.financial_charges_option = rows;
                                        con.query('SELECT '
                                            + 'mdv_id, '
                                            + 'status_id, '
                                            + 'value '
                                            + 'FROM master_data_values '
                                            + 'WHERE md_id = ? AND status = ?'
                                            , ['Payment Method', 1]
                                            , function (err, rows, fields) {
                                                if (err) {
                                                    console.log(err)
                                                } else {
                                                    optionObj.payment_method_option = rows;
                                                    return callback(optionObj);
                                                }
                                            }

                                        )
                                    }
                                });
                        }
                    });
            }
        });

}

var getLateFeeWaiverType = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        + 'late_fee_waivers.id, '
        + 'late_fee_waivers.reason_desc, '
        + 'master_data_values.md_id AS reason_type, '
        + 'master_data_values.value AS max_waiver '
        + 'FROM late_fee_waivers '
        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '
        + 'WHERE late_fee_waivers.mdv_waiver_type_id = ? AND late_fee_waivers.deleted_flag = ? AND late_fee_waivers.status = ?'
        , [id, 0, 1]
        , function (error, rows, fields) {
            if (error) console.log(error)
            else {
                optionObj.late_fee_waiver_type_reason_option = rows;
                return callback(optionObj);
            }
        });

}

var getFinChargeWaiverType = function (con, id, callback) {
    var optionObj = {};
    con.query('SELECT '
        + 'late_fee_waivers.id, '
        + 'late_fee_waivers.reason_desc, '
        + 'master_data_values.value AS reason_type, '
        + 'master_data_values.value AS max_waiver '
        + 'FROM late_fee_waivers '
        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '
        + 'WHERE late_fee_waivers.mdv_waiver_type_id = ? AND late_fee_waivers.deleted_flag = ? AND late_fee_waivers.status = ?'
        , [id, 0, 1]
        , function (error, rows, fields) {
            if (error) console.log(error)
            else {
                optionObj.fin_charge_waiver_type_reason_option = rows;
                return callback(optionObj);
            }
        });

}

// this function use for update remanning amount
var updateRemaning = function (con, invoice_id, pp_id, amount, current_user_id, interest, principal_amount, invoice_status, installments_count, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {}
    //check install ment count with invoice status
    installments_count = (invoice_status == 5) ? installments_count + 1 : installments_count;
    /*
    * Update remaning amount according to particuler plan
    * Also update plan status if remianing amount completly paid
    */
    con.query('UPDATE payment_plan SET remaining_amount=?, installments_count=?, plan_status=?, date_modified=?,modified_by=? WHERE pp_id=?'
        ,
        [amount, installments_count, (amount > 0) ? 1 : 8, current_date, current_user_id, pp_id]
        , function (ppErr) {
            if (ppErr) {
                obj.status = 0;
                return callback(obj);
            } else {
                /*
                * Update principal and interest amount according to customer paid amount 
                * Also update status according to principal paid
                */
                con.query('UPDATE patient_invoice_details SET interest=?, principal_amount=?, paid_flag=?, date_modified=?,modified_by=? WHERE pp_id=? AND invoice_id=?'
                    ,
                    [interest, principal_amount, (principal_amount > 0) ? 2 : 1, current_date, current_user_id, pp_id, invoice_id]
                    , function (ppIErr) {
                        if (ppIErr) {
                            obj.status = 0;
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            return callback(obj);
                        }
                    })

            }
        })
}

var updatePaymentDetails = function (con, data, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    if (data.payment_id === null || data.last_invoice_status == 4) {
        con.query('INSERT INTO patient_invoice_payment (invoice_id, amount_paid, payment_method, ach_bank_name, ach_routing_no, ach_account_no, check_no, check_date, comments, status, created_by, modified_by, date_created, date_modified) '
            + 'VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [
                data.invoice_id,
                data.additional_amount,
                data.payment_method,
                capitalize(data.ach_bank_name),
                data.ach_routing_no,
                data.ach_account_no,
                (data.check_no != '') ? data.check_no : null,
                (data.check_date != '') ? data.check_date : null,
                (data.comments != null) ? capitalize(data.comments) : '',
                data.newStatus,
                data.current_user_id,
                data.current_user_id,
                current_date,
                current_date
            ],
            function (paymentErr, getInvoice) {
                console.log(paymentErr)
                if (paymentErr) {
                    var obj = {
                        status: 0,
                    }
                    callback(obj);
                } else {
                    var obj = {
                        status: 1,
                    }
                    callback(obj);
                }
            })
    } else {
        con.query('UPDATE patient_invoice_payment SET '
            + 'amount_paid=?, '
            + 'payment_method=?, '
            + 'ach_bank_name=?, '
            + 'ach_routing_no=?, '
            + 'ach_account_no=?, '
            + 'check_no=?, '
            + 'check_date=?, '
            + 'comments=?, '
            + 'status=?, '
            + 'date_modified=?, '
            + 'modified_by=? '
            + 'WHERE payment_id=?'
            ,
            [
                data.additional_amount,
                data.payment_method,
                capitalize(data.ach_bank_name),
                data.ach_routing_no,
                data.ach_account_no,
                (data.check_no != '') ? data.check_no : null,
                (data.check_date != '') ? data.check_date : null,
                capitalize(data.comments),
                data.newStatus,
                current_date,
                data.current_user_id,
                data.payment_id
            ],
            function (paymentErr, getInvoice) {
                if (paymentErr) {
                    var obj = {
                        status: 0,
                    }
                    callback(obj);
                } else {
                    var obj = {
                        status: 1,
                    }
                    callback(obj);
                }
            })
    }
}
// add invoice payment according to admin and customer
var payInstallmentInsert = function (con, data, callback) {
    //console.log(data)
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    /*
    * Check late Fee or fincial charge avaiable or not
    */
    data.late_fee_recieved = (isNaN(data.late_fee_recieved)) ? 0 : data.late_fee_recieved;
    data.fin_charge_received = (isNaN(data.fin_charge_received)) ? 0 : data.fin_charge_received;
    var obj = {};
    /*
    * Sum all amount
    */
    var totalInvoiceAmt = parseFloat(data.payment_amount) + parseFloat(data.late_fee_recieved) + parseFloat(data.fin_charge_received);
    var addAmt = 0;
    if (data.prv_invoice_status == 4) {
        data.additional_amount = parseFloat(data.additional_amount) + parseFloat(data.total_recived);
    }
    /*
    * Check additional amount  paid or not
    */
    if (totalInvoiceAmt < parseFloat(data.additional_amount)) {
        addAmt = parseFloat(data.additional_amount) - totalInvoiceAmt;
        addAmt = parseFloat(parseFloat(addAmt).toFixed(2));
        data.additional_amount = parseFloat(data.additional_amount) - addAmt;
        data.additional_amount = parseFloat(data.additional_amount.toFixed(2))
    }


    if (data.payment_in == 1) {
        var mainAmt = data.additional_amount - (parseFloat(data.late_fee_recieved) + parseFloat(data.fin_charge_received));
    } else {
        var mainAmt = data.additional_amount - (parseFloat(data.late_fee_recieved) + parseFloat(data.fin_charge_received));
    }
    mainAmt = mainAmt.toFixed(2)
    totalInvoiceAmt = totalInvoiceAmt.toFixed(2)
    mainAmt = (mainAmt > 0) ? mainAmt : 0




    /*
    * Get current invoice details
    */
    con.query('SELECT patient_invoice_details.invoice_amount,patient_invoice_details.principal_amount,patient_invoice_details.interest,patient_invoice.payment_amount,payment_plan.loan_amount,payment_plan.pp_id,payment_plan.discounted_interest_rate,payment_plan.term,payment_plan.loan_type,payment_plan.monthly_amount,payment_plan.last_month_amount,payment_plan.remaining_amount,payment_plan.installments_count,patient_invoice.invoice_status,payment_plan.installments_count, '
        + '(SELECT additional_amount FROM patient_invoice WHERE application_id=patient_invoice.application_id AND invoice_status=1 ORDER BY invoice_id DESC LIMIT 1) as Addition_amt '
        + 'FROM payment_plan '
        + 'INNER JOIN patient_invoice ON patient_invoice.application_id = payment_plan.application_id '
        + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id = patient_invoice.invoice_id AND patient_invoice_details.pp_id=payment_plan.pp_id '
        + 'LEFT JOIN patient_procedure ON patient_procedure.pp_id = payment_plan.pp_id '
        //+ 'WHERE patient_invoice.invoice_id=? AND payment_plan.remaining_amount > ? AND payment_plan.plan_status = ? AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <= patient_invoice.due_date ORDER BY payment_plan.discounted_interest_rate ASC',
        + 'WHERE patient_invoice.invoice_id=? AND payment_plan.remaining_amount > ? AND payment_plan.plan_status = ? ORDER BY payment_plan.discounted_interest_rate ASC',
        [data.invoice_id, 0, 1],
        function (detailsErr, getDetails) {
            //console.log(getDetails);
            /*
            * Check error found or not
            */
            if (detailsErr || getDetails.length == 0) {
                obj.status = 0;
                obj.redirectReport = 0;
                obj.message = "Currently we are not receiving payments. Please contact with HPS";
                return callback(obj);
            } else {
                /*
                * Update main invoice according to custoemr payment
                */
                data.newStatus = (totalInvoiceAmt > parseFloat(data.additional_amount) && data.payment_in == 1) ? 4 : data.payment_in;
                con.query('UPDATE patient_invoice SET '
                    + 'paid_amount=?, '
                    + 'additional_amount=?, '
                    + 'late_fee_waiver_comt=?, '
                    + 'payment_date=?, '
                    + 'late_fee_received=?, '
                    + 'fin_charge_received=?, '
                    + 'late_fee_waivers_id=?, '
                    + 'fin_charge_waiver_id=?, '
                    + 'fin_charge_waiver_comt=?, '
                    + 'comments=?, '
                    + 'invoice_status=?, '
                    + 'date_modified=?, '
                    + 'modified_by=? '
                    + ' WHERE invoice_id=?'
                    ,
                    [
                        mainAmt,
                        addAmt,
                        data.late_fee_comt,
                        current_date,
                        data.late_fee_recieved,
                        data.fin_charge_received,
                        data.late_fee_waiver_id,
                        data.fin_charge_waiver_id,
                        data.finance_charge_commt,
                        (data.comments != null) ? capitalize(data.comments) : '',
                        data.newStatus,
                        current_date,
                        data.current_user_id,
                        data.invoice_id
                    ]
                    ,
                    function (invoiceErr, getInvoice) {
                        console.log(invoiceErr)
                        console.log('----------------')
                        /*
                        * Check any error found or not
                        */
                        if (invoiceErr) {
                            obj.status = 0;
                            obj.redirectReport = 0;
                            obj.message = "payment detail added in patient_payment table";
                            return callback(obj);
                        } else {

                            if (data.prv_invoice_status == 4) {
                                mainAmt = mainAmt - parseFloat(data.total_recived);
                                data.additional_amount = parseFloat(data.additional_amount) + addAmt - parseFloat(data.total_recived);
                                mainAmt = mainAmt + (parseFloat(data.late_fee_recieved) + parseFloat(data.fin_charge_received));
                            }
                            updatePaymentDetails(con, data, function (updatePayment) {
                                console.log(updatePayment)
                                if (updatePayment.status == 1) {
                                    /*
                                    * Check customer amount confirm or under process
                                    */
                                    if (data.payment_in == 2) {
                                        obj.status = 1;
                                        obj.redirectReport = 1;
                                        obj.message = "Invoice created successfully.";
                                        return callback(obj);
                                    }
                                    /*
                                    * Count invoice installment and run one by one
                                    */
                                    var currentInt = getDetails.length;
                                    for (let intData of getDetails) {
                                        mainAmt = parseFloat(mainAmt).toFixed(2)
                                        addAmt = parseFloat(addAmt).toFixed(2)
                                        //var InterestRate = intData.discounted_interest_rate / 100;
                                        /*
                                        * Check paid amount according to invoice amount
                                        */
                                        if (mainAmt >= intData.invoice_amount && mainAmt > 0) {
                                            mainAmt = mainAmt - intData.interest;
                                            /*
                                            * Check principal amount
                                            */
                                            if (intData.remaining_amount > intData.principal_amount) {
                                                remaningAmount = intData.remaining_amount - intData.principal_amount;
                                                //check additional amount grater then ot equal remaning amount
                                                if (addAmt > 0 && addAmt <= remaningAmount) {
                                                    remaningAmount = remaningAmount - addAmt;
                                                    addAmt = 0;
                                                } else if (addAmt > 0) {
                                                    addAmt = addAmt - remaningAmount;
                                                    remaningAmount = 0;
                                                }
                                                mainAmt = mainAmt - intData.principal_amount;
                                                intData.principal_amount = 0;

                                            } else {
                                                mainAmt = mainAmt - intData.principal_amount;
                                                remaningAmount = intData.remaining_amount - intData.principal_amount;
                                                intData.principal_amount = 0;

                                            }
                                            /*
                                            * Send request for update details
                                            */
                                            intData.interest = 0;
                                            updateRemaning(con, data.invoice_id, intData.pp_id, remaningAmount, data.current_user_id, intData.interest, intData.principal_amount, intData.invoice_status, intData.installments_count, function (updateResponse) {
                                                /*
                                                * Return final statement
                                                */
                                                if (updateResponse.status == 1) {

                                                    if (0 === --currentInt) {
                                                        obj.status = 1;
                                                        obj.redirectReport = 1;
                                                        obj.message = "Invoice created successfully.";
                                                        return callback(obj);
                                                    }
                                                } else {
                                                    if (0 === --currentInt) {
                                                        obj.status = 1;
                                                        obj.redirectReport = 1;
                                                        obj.message = "Invoice created successfully.";
                                                        return callback(obj);
                                                    }
                                                }
                                            })
                                        } else if (mainAmt > 0) {
                                            // remove interset rate first
                                            if (mainAmt >= intData.interest) {
                                                mainAmt = mainAmt - intData.interest;
                                                intData.interest = 0;
                                            } else {
                                                intData.interest = intData.interest - mainAmt;
                                                mainAmt = 0;
                                            }
                                            if (mainAmt > 0) {
                                                if (mainAmt > intData.principal_amount) {
                                                    mainAmt = mainAmt - intData.principal_amount;
                                                    var remaningAmount = intData.remaining_amount - intData.principal_amount;
                                                    intData.principal_amount = 0;
                                                } else {
                                                    intData.principal_amount = intData.principal_amount - mainAmt;
                                                    var remaningAmount = intData.remaining_amount - mainAmt;
                                                    mainAmt = 0;
                                                }
                                            } else {
                                                var remaningAmount = intData.remaining_amount
                                            }

                                            /*
                                            * Send request for update details
                                            */
                                            updateRemaning(con, data.invoice_id, intData.pp_id, remaningAmount, data.current_user_id, intData.interest, intData.principal_amount, intData.invoice_status, intData.installments_count, function (updateResponse) {
                                                /*
                                                * Return final statement
                                                */
                                                if (updateResponse.status == 1) {

                                                    if (0 === --currentInt) {
                                                        obj.status = 1;
                                                        obj.redirectReport = 1;
                                                        obj.message = "Invoice created successfully.";
                                                        return callback(obj);
                                                    }
                                                } else {
                                                    if (0 === --currentInt) {
                                                        obj.status = 1;
                                                        obj.redirectReport = 1;
                                                        obj.message = "Invoice created successfully.";
                                                        return callback(obj);
                                                    }
                                                }
                                            })

                                        } else {
                                            /*
                                            * Return final statement
                                            */
                                            if (0 === --currentInt) {
                                                obj.status = 1;
                                                obj.redirectReport = 1;
                                                obj.message = "Invoice created successfully.";
                                                return callback(obj);
                                            }
                                        }
                                    }
                                } else {
                                    obj.status = 0;
                                    obj.redirectReport = 0;
                                    obj.message = "payment detail added in patient_payment table222";
                                    return callback(obj);
                                }
                            })

                        }
                    });
            }
        })

}


var customerInvoice = function (con, invoiceId, callback) {
    var obj = {};
    con.query('SELECT '
        + 'invoice_number, patient_id, payment_amount, paid_amount, additional_amount, late_fee_received, late_fee_due, DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date, '
        + 'DATE_FORMAT(due_date, "%m/%d/%Y") as due_date, previous_late_fee, previous_fin_charge, '
        + 'fin_charge_received, fin_charge_due, invoice_status '


        + 'FROM patient_invoice '
        + 'WHERE invoice_id = ?'
        , [invoiceId]
        , function (error, rows, fields) {
            console.log(this.sql)
            if (error || rows.length == 0) {
                obj.status = 0;
                obj.message = "Payment Receipt not found.";
                return callback(obj);
            } else {
                con.query('SELECT patient.patient_id, patient.f_name, patient.m_name, patient.l_name, patient.gender, patient.email, patient.peimary_phone, patient.patient_ac, '
                    + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name '
                    + 'FROM credit_applications '
                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                    + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                    + 'WHERE credit_applications.patient_id=?',
                    [
                        rows[0].patient_id
                    ]
                    , function (error, customer, fields) {

                        if (error) {
                            obj.status = 0;
                            obj.message = "Payment Receipt not found.";
                            return callback(obj);
                        } else {
                            con.query('SELECT patient_invoice_details.invoice_id,(patient_invoice_details.installment_interest+patient_invoice_details.installment_principal_amount) as invoice_amount,patient_invoice_details.interest,patient_invoice_details.principal_amount,patient_invoice_details.paid_flag,payment_plan.plan_number '
                                + 'FROM patient_invoice_details '
                                + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
                                + 'WHERE patient_invoice_details.invoice_id=?',
                                [
                                    invoiceId
                                ]
                                , function (intErr, installmentDetails, fields) {
                                    if (intErr) {
                                        obj.status = 1;
                                        obj.result = (rows.length > 0) ? rows[0] : '';
                                        obj.customer = (customer.length > 0) ? customer[0] : '';
                                        return callback(obj);
                                    } else {
                                        con.query('SELECT patient_invoice_payment.amount_paid, patient_invoice_payment.payment_method, patient_invoice_payment.check_no, DATE_FORMAT(patient_invoice_payment.check_date, "%m/%d/%Y") as check_date , patient_invoice_payment.bank_name, patient_invoice_payment.ach_bank_name, patient_invoice_payment.ach_routing_no, patient_invoice_payment.ach_account_no, patient_invoice_payment.comments, DATE_FORMAT(patient_invoice_payment.date_modified, "%m/%d/%Y") as payment_date, '
                                            + 'payType.value '
                                            + 'FROM patient_invoice_payment '
                                            + 'INNER JOIN master_data_values AS payType on payType.status_id=patient_invoice_payment.payment_method AND payType.md_id=? '
                                            + 'WHERE patient_invoice_payment.invoice_id=?',
                                            [
                                                'Payment Method', invoiceId
                                            ]
                                            , function (intErr, invoicePaymentDetails, fields) {
                                                if (intErr) {
                                                    obj.status = 1;
                                                    obj.result = (rows.length > 0) ? rows[0] : '';
                                                    obj.customer = (customer.length > 0) ? customer[0] : '';
                                                    obj.installmentDetails = (installmentDetails.length > 0) ? installmentDetails : ''
                                                    return callback(obj);;
                                                } else {
                                                    obj.status = 1;
                                                    obj.result = (rows.length > 0) ? rows[0] : '';
                                                    obj.customer = (customer.length > 0) ? customer[0] : '';
                                                    obj.installmentDetails = (installmentDetails.length > 0) ? installmentDetails : '';
                                                    obj.invoicePaymentDetails = (invoicePaymentDetails.length > 0) ? invoicePaymentDetails : ''
                                                    return callback(obj);;
                                                }
                                            })

                                    }
                                })

                        }
                    })

            }
        });

}

var getPreinvoiceDetails = function (con, appid, newDate, onFlay, callback) {
    con.query('SELECT '
        + 'patient_payment_pause.fin_charge_waived,patient_payment_pause.late_fee_waived,patient_payment_pause.interest_waived '
        + 'FROM patient_payment_pause '
        + 'INNER JOIN credit_applications on credit_applications.patient_id = patient_payment_pause.patient_id '
        + 'WHERE credit_applications.application_id= ? AND patient_payment_pause.status = ? AND patient_payment_pause.start_date <= ? AND patient_payment_pause.end_date >= ? ',
        [appid, 1, newDate, newDate]
        , function (pauseError, pauseResult, fields) {
            //console.log(pauseResult)
            if (pauseResult.length > 0) {
                if (onFlay == 1) {
                    var obj = {}
                    obj.status = 1;
                    obj.oldLateFee = 0;
                    obj.oldFinCharge = 0;
                    obj.due_date = null;
                    obj.prevBlc = 0;
                    obj.child = {}
                    obj.lateApply = 0;
                    obj.invoiceStatus = 5;
                    obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                    obj.late_fee_waived = pauseResult[0].late_fee_waived;
                    return callback(obj);
                }
                var moment = require('moment');
                var lastDate = moment(newDate, "YYYY-MM-DD").subtract((1), 'months').format('YYYY-MM-DD');

                con.query('SELECT '
                    + 'application_id,invoice_id,previous_late_fee,late_fee_received,late_fee_due,payment_amount,paid_amount,previous_blc, '
                    + 'previous_fin_charge,fin_charge_received,fin_charge_due, invoice_status, '
                    + 'DATE_FORMAT(due_date, "%m/%d/%Y") as due_date '
                    + 'FROM patient_invoice '
                    //+ 'WHERE application_id=? ORDER BY invoice_id DESC LIMIT 1',
                    + 'WHERE invoice_status!=? AND invoice_status!=? AND application_id=? AND YEAR(due_date) = YEAR(?) AND MONTH(due_date)=MONTH(?) ',
                    [0, 6, appid, lastDate, lastDate]
                    , function (mainError, mainResult, fields) {
                        if (mainError || mainResult.length == 0) {
                            var obj = {}
                            obj.status = 1;
                            obj.oldLateFee = 0;
                            obj.oldFinCharge = 0;
                            obj.due_date = null;
                            obj.prevBlc = 0;
                            obj.child = {}
                            obj.lateApply = 0;
                            obj.invoiceStatus = 5;
                            obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                            obj.late_fee_waived = pauseResult[0].late_fee_waived;
                            return callback(obj);
                        } else {

                            var uInvoiceID = mainResult && [...new Set(mainResult.map(item => item.invoice_id))];

                            con.query('SELECT '
                                + 'patient_invoice_details.pp_id,patient_invoice_details.interest,patient_invoice_details.principal_amount,patient_invoice.invoice_status,patient_invoice.fin_charge_waived,patient_invoice.late_fee_waived, patient_invoice.due_date '
                                + 'FROM patient_invoice_details '
                                + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                + 'WHERE patient_invoice_details.invoice_id IN(?)',
                                [uInvoiceID]
                                , function (error, result, fields) {
                                    if (error || result.length == 0) {
                                        var obj = {}
                                        obj.status = 1;
                                        obj.oldLateFee = 0;
                                        obj.oldFinCharge = 0;
                                        obj.due_date = null;
                                        obj.prevBlc = 0;
                                        obj.lateApply = 0;
                                        obj.application_id = mainResult[0].application_id;
                                        obj.child = {}
                                        obj.invoiceStatus = 5;
                                        obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                                        obj.late_fee_waived = pauseResult[0].late_fee_waived;
                                        return callback(obj);
                                    } else {
                                        var obj = {}
                                        obj.status = 1;
                                        obj.oldLateFee = 0;
                                        obj.oldFinCharge = 0;
                                        obj.due_date = null;
                                        obj.prevBlc = 0;
                                        obj.lateApply = 1;
                                        obj.child = {}
                                        obj.invoiceStatus = 5;
                                        obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                                        obj.late_fee_waived = pauseResult[0].late_fee_waived;
                                        var total_payment_amount = 0;
                                        var total_paid_amount = 0;

                                        mainResult.map((data, idx) => {
                                            if (data.invoice_status == 0) {
                                                obj.oldLateFee += data.previous_late_fee;
                                                obj.oldLateFee += data.late_fee_due;
                                                obj.oldFinCharge += data.previous_fin_charge;
                                                obj.oldFinCharge += data.fin_charge_due;
                                                var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                obj.prevBlc += payment_amount - paid_amount;
                                                total_payment_amount += payment_amount;
                                                total_paid_amount += paid_amount;
                                            } else if (data.invoice_status == 5) {
                                                obj.oldLateFee += data.previous_late_fee;
                                                obj.oldLateFee += data.late_fee_due;
                                                obj.oldFinCharge += data.previous_fin_charge;
                                                obj.oldFinCharge += data.fin_charge_due;
                                                obj.prevBlc += data.previous_blc;
                                            } else {
                                                if (data.invoice_status == 1 || data.invoice_status == 4) {
                                                    obj.oldLateFee += 0;
                                                    obj.oldFinCharge += 0;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                } else {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                }

                                            }


                                        })
                                        var due_date = moment(mainResult[0].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                        due_date = new Date(moment(due_date, "MM/DD/YYYY").format('YYYY'), moment(due_date, "MM/DD/YYYY").format('MM'), 0);
                                        due_date = moment(due_date).format('MM/DD/YYYY');
                                        obj.due_date = due_date;

                                        obj.latePCT = (total_paid_amount == 0) ? 100 : total_paid_amount / total_payment_amount * 100;
                                        obj.application_id = mainResult[0].application_id;
                                        obj.child = result;

                                        return callback(obj);
                                    }
                                })
                        }
                    });
            } else {
                if (onFlay == 1) {
                    var obj = {}
                    obj.status = 1;
                    obj.oldLateFee = 0;
                    obj.oldFinCharge = 0;
                    obj.due_date = null;
                    obj.prevBlc = 0;
                    obj.child = {}
                    obj.lateApply = 0;
                    obj.invoiceStatus = 3;
                    obj.fin_charge_waived = 0;
                    obj.late_fee_waived = 0;
                    return callback(obj);
                }
                var moment = require('moment');

                var lastDate = moment(newDate, "YYYY-MM-DD").subtract((1), 'months').format('YYYY-MM-DD');

                con.query('SELECT '
                    + 'application_id,invoice_id,previous_late_fee,late_fee_received,late_fee_due,payment_amount,paid_amount,previous_blc, '
                    + 'previous_fin_charge,fin_charge_received,fin_charge_due, invoice_status, '
                    + 'DATE_FORMAT(due_date, "%m/%d/%Y") as due_date '
                    + 'FROM patient_invoice '
                    //+ 'WHERE application_id=? ORDER BY invoice_id DESC LIMIT 1',
                    + 'WHERE invoice_status!=? AND invoice_status!=? AND application_id=? AND YEAR(due_date) = YEAR(?) AND MONTH(due_date)=MONTH(?) ',
                    [0, 6, appid, lastDate, lastDate]
                    , function (mainError, mainResult, fields) {
                        if (mainError || mainResult.length == 0) {
                            var obj = {}
                            obj.status = 1;
                            obj.oldLateFee = 0;
                            obj.oldFinCharge = 0;
                            obj.due_date = null;
                            obj.prevBlc = 0;
                            obj.child = {}
                            obj.lateApply = 0;
                            obj.invoiceStatus = 3;
                            obj.fin_charge_waived = 0;
                            obj.late_fee_waived = 0;
                            return callback(obj);
                        } else {

                            var uInvoiceID = mainResult && [...new Set(mainResult.map(item => item.invoice_id))];

                            con.query('SELECT '
                                + 'patient_invoice_details.pp_id,patient_invoice_details.interest,patient_invoice_details.principal_amount,patient_invoice.invoice_status,patient_invoice.fin_charge_waived,patient_invoice.late_fee_waived, patient_invoice.due_date '
                                + 'FROM patient_invoice_details '
                                + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                + 'WHERE patient_invoice_details.invoice_id IN(?)',
                                [uInvoiceID]
                                , function (error, result, fields) {
                                    if (error || result.length == 0) {
                                        var obj = {}
                                        obj.status = 1;
                                        obj.oldLateFee = 0;
                                        obj.oldFinCharge = 0;
                                        obj.due_date = null;
                                        obj.prevBlc = 0;
                                        obj.lateApply = 0;
                                        obj.application_id = mainResult[0].application_id;
                                        obj.child = {}
                                        obj.invoiceStatus = 3;
                                        obj.fin_charge_waived = 0;
                                        obj.late_fee_waived = 0;
                                        return callback(obj);
                                    } else {
                                        var obj = {}
                                        obj.status = 1;
                                        obj.oldLateFee = 0;
                                        obj.oldFinCharge = 0;
                                        obj.due_date = null;
                                        obj.prevBlc = 0;
                                        obj.lateApply = 1;
                                        obj.child = {}
                                        obj.invoiceStatus = 3;
                                        obj.fin_charge_waived = 0;
                                        obj.late_fee_waived = 0;
                                        var total_payment_amount = 0;
                                        var total_paid_amount = 0;

                                        mainResult.map((data, idx) => {
                                            if (data.invoice_status == 0) {
                                                obj.oldLateFee += data.previous_late_fee;
                                                obj.oldLateFee += data.late_fee_due;
                                                obj.oldFinCharge += data.previous_fin_charge;
                                                obj.oldFinCharge += data.fin_charge_due;
                                                var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                obj.prevBlc += payment_amount - paid_amount;
                                                total_payment_amount += payment_amount;
                                                total_paid_amount += paid_amount;
                                            } else {
                                                if (data.invoice_status == 1 || data.invoice_status == 4) {
                                                    obj.oldLateFee += 0;
                                                    obj.oldFinCharge += 0;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                } else if (data.invoice_status == 5) {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    obj.prevBlc += data.previous_blc;
                                                } else {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                }

                                            }


                                        })
                                        var due_date = moment(mainResult[0].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                        due_date = new Date(moment(due_date, "MM/DD/YYYY").format('YYYY'), moment(due_date, "MM/DD/YYYY").format('MM'), 0);
                                        due_date = moment(due_date).format('MM/DD/YYYY');
                                        obj.due_date = due_date;

                                        obj.latePCT = (total_paid_amount == 0) ? 100 : total_paid_amount / total_payment_amount * 100;
                                        obj.application_id = mainResult[0].application_id;
                                        obj.child = result;

                                        return callback(obj);
                                    }
                                })
                        }
                    });
            }
        })

}
/**
 * Create customer invoice on Flay according to provider action
 * @param {*} con 
 * @param {*} data 
 * @param {*} callback 
 */
var createInvoiceOnFlayAdmin = function (con, data, callback) {
    let moment = require('moment');
    var reqDate = moment(data.date).set("date", 15).format('YYYY-MM-DD');
    var obj = {};
    /*
    * Get customer installemt according to request
    */
    con.query('SELECT payment_plan.installments_count,payment_plan.pp_id,DATE_FORMAT(payment_plan.date_created, "%Y-%m-%d") as date_created,credit_applications.application_id,credit_applications.patient_id,payment_plan.term AS payment_term_month,payment_plan.discounted_interest_rate,payment_plan.interest_rate, '
        + 'payment_plan.monthly_amount,payment_plan.last_month_amount,payment_plan.remaining_amount,payment_plan.installments_count,payment_plan.loan_type,payment_plan.loan_amount '
        + 'FROM payment_plan '
        + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
        + 'WHERE payment_plan.pp_id = ? AND payment_plan.plan_status = ?  AND payment_plan.application_id=? AND payment_plan.remaining_amount > ? '
        + 'ORDER by credit_applications.patient_id ',
        [data.pp_id, 1, data.appid, 0]
        , function (error, result, fields) {

            //var dateValid = moment(moment().format('YYYY') + '-' + moment().format('MM') + '-15').format('YYYY-MM-DD');
            //result = result.filter((s, idx) => new Date(s.date_created) <= new Date(dateValid));
            /*
            * Check any error 
            */
            if (error || result.length == 0) {
                obj.status = 0;
                obj.message = "We can't create the invoice right now. Please try again.";
                return callback(obj);
            } else {
                /*
                * get last invoice details
                */
                getPreinvoiceDetails(con, data.appid, data.date, 1, function (invoiceDetails) {
                    if (invoiceDetails.status == 1) {
                        /*
                        * get some master value for invoice
                        */
                        con.query('SELECT '
                            + 'value,md_id '
                            + 'FROM master_data_values '
                            + 'WHERE status = ? AND (md_id = ? OR md_id = ? OR md_id = ?)'
                            , [1, 'Late Fee', 'Financial Charges', 'Partial Pay PCT']
                            , function (errLate, late_fee_rows, fields) {
                                if (errLate) {
                                    obj.status = 0;
                                    obj.message = "We can't create the invoice right now. Please try again.";
                                    return callback(obj);
                                } else {
                                    /*
                                    * Get details according to result
                                    */
                                    var lateFee = 0;
                                    var finCharge = 0;
                                    var PartialPayPCT = 0;
                                    if (late_fee_rows.length > 0) {
                                        var late = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Late Fee';
                                        });
                                        lateFee = (late.length > 0) ? late[0].value : 0;
                                        var financial = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Financial Charges';
                                        });
                                        finCharge = (financial.length > 0) ? financial[0].value : 0;
                                        var PartialPay = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Partial Pay PCT';
                                        });
                                        PartialPayPCT = (PartialPay.length > 0) ? PartialPay[0].value : 0;
                                    }
                                    var checkLate = 0;
                                    var lateFeePatient = 0;
                                    var finChargePatient = 0;

                                    var newAmt = 0;
                                    var newAmount = 0;
                                    /*
                                    * Marge record according to final result
                                    */
                                    var allDetails = result && result.reduce(function (accumulator, currentValue, currentindex) {
                                        var installmentFin = 0;
                                        /*
                                        * Get and merge last invoice details
                                        */
                                        var oldAmount = (invoiceDetails.child.length > 0) ? invoiceDetails.child.filter((s, idx) => s.pp_id == currentValue.pp_id) : [];
                                        // check previous record found or not

                                        if (oldAmount.length > 0) {
                                            result[currentindex].principal_amount = oldAmount[0].principal_amount;
                                            result[currentindex].currentInterest = oldAmount[0].interest;
                                            result[currentindex].palnAmount = oldAmount[0].interest + oldAmount[0].principal_amount;
                                            var previousPlanBlc = oldAmount[0].interest + oldAmount[0].principal_amount;
                                        } else {
                                            result[currentindex].principal_amount = 0;
                                            result[currentindex].currentInterest = 0;
                                            result[currentindex].palnAmount = 0;
                                            previousPlanBlc = null;
                                        }
                                        result[currentindex].oldLateFee = invoiceDetails.oldLateFee;
                                        result[currentindex].oldFinCharge = invoiceDetails.oldFinCharge;
                                        result[currentindex].due_date = invoiceDetails.due_date;
                                        result[currentindex].prevBlc = invoiceDetails.prevBlc;

                                        result[currentindex].fin_charge = 0;

                                        if (!accumulator[currentValue.patient_id]) {
                                            lateFeePatient = 0;
                                            finChargePatient = 0;
                                            checkLate = 0;
                                            newAmt = 0;
                                            newAmount = 0;
                                            installmentFin = 0;
                                            // check monthly amount with previous balance amount

                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            result[currentindex].currentInterest += newAmount;
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            // check due date
                                            if ((new Date() > new Date(currentValue.due_date) && currentValue.due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;
                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }
                                                    result[currentindex].fin_charge = installmentFin;
                                                }
                                            }
                                            accumulator[currentValue.patient_id] = {
                                                patient_id: currentValue.patient_id,
                                                totalAmt: newAmt,
                                                finCharge: 0,//finChargePatient,
                                                lateFee: 0,//lateFeePatient,
                                                //oldBlc: currentValue.oldBlc,
                                                oldLateFee: 0,//(currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee,
                                                oldFinCharge: 0,//(currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge,
                                                prevBlc: currentValue.prevBlc,
                                                invoiceStatus: invoiceDetails.invoiceStatus,
                                                invoiceFinStatus: invoiceDetails.fin_charge_waived,
                                                invoiceLateStatus: invoiceDetails.late_fee_waived,
                                            };
                                        } else {
                                            var newAmt = 0;
                                            // check monthly amount with previous balance amount
                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            result[currentindex].currentInterest += newAmount;
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            if ((new Date() > new Date(currentValue.due_date) && currentValue.due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;
                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }

                                                    result[currentindex].fin_charge = installmentFin;
                                                }


                                            }
                                            prevBlc = 0;
                                            accumulator[currentValue.patient_id].totalAmt += newAmt;
                                            accumulator[currentValue.patient_id].finCharge = 0;//finChargePatient;
                                            accumulator[currentValue.patient_id].lateFee = 0;//lateFeePatient;
                                            //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
                                            accumulator[currentValue.patient_id].oldLateFee = 0;//(currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee;
                                            accumulator[currentValue.patient_id].oldFinCharge = 0;//(currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge;
                                            //accumulator[currentValue.patient_id].prevBlc += prevBlc;
                                        }
                                        return accumulator;

                                    }, []);
                                    var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
                                        return allDetails[patient_id];
                                    })
                                    //console.log(patientGroup)
                                    //console.log(result)
                                    //obj.status = 1;
                                    //obj.message = "Invoice created successfully.";
                                    //return callback(obj);
                                    /*
                                    * Send request for insrest details
                                    */
                                    var minLenght = patientGroup.length;
                                    patientGroup && patientGroup.map((dataArr, idx) => {
                                        dataArr.totalAmt = dataArr.totalAmt + dataArr.prevBlc;
                                        mainTableDate(con, dataArr.patient_id, dataArr.totalAmt, data.date, data.appid, dataArr.prevBlc, dataArr.finCharge, dataArr.lateFee, dataArr.oldLateFee, dataArr.oldFinCharge, data.current_user_id, result, 1, dataArr.invoiceStatus, dataArr.invoiceFinStatus, dataArr.invoiceLateStatus, function (mainResponce) {

                                            if (0 === --minLenght) {
                                                obj.status = 1;
                                                obj.message = "Invoice created successfully.";
                                                return callback(obj);
                                            }
                                        });
                                    })
                                }
                            })
                    } else {
                        obj.status = 0;
                        obj.message = "We can't create the invoice right now. Please try again.";
                        return callback(obj);
                    }
                })
            }
        });

}

var createInvoiceOnFlay = function (con, data, callback) {
    let moment = require('moment');
    var reqDate = moment(data.date).set("date", 15).format('YYYY-MM-DD');
    var obj = {};
    /*
    * Get customer installemt according to request
    */
    con.query('SELECT payment_plan.installments_count,payment_plan.pp_id,DATE_FORMAT(payment_plan.date_created, "%Y-%m-%d") as date_created,credit_applications.application_id,credit_applications.patient_id,payment_plan.term AS payment_term_month,payment_plan.discounted_interest_rate,payment_plan.interest_rate, '
        + 'payment_plan.monthly_amount,payment_plan.last_month_amount,payment_plan.remaining_amount,payment_plan.installments_count,payment_plan.loan_type,payment_plan.loan_amount '
        + 'FROM payment_plan '
        + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
        + 'INNER JOIN patient_procedure ON patient_procedure.PP_id = payment_plan.pp_id '
        + 'WHERE payment_plan.pp_id = ? AND payment_plan.plan_status = ?  AND payment_plan.application_id=? AND payment_plan.remaining_amount > ? AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <=? '
        + 'ORDER by credit_applications.patient_id ',
        [data.pp_id, 1, data.appid, 0, reqDate]
        , function (error, result, fields) {

            //var dateValid = moment(moment().format('YYYY') + '-' + moment().format('MM') + '-15').format('YYYY-MM-DD');
            //result = result.filter((s, idx) => new Date(s.date_created) <= new Date(dateValid));
            /*
            * Check any error 
            */
            if (error || result.length == 0) {
                obj.status = 0;
                obj.message = "We can't create the invoice right now. Please try again.";
                return callback(obj);
            } else {
                /*
                * get last invoice details
                */
                getPreinvoiceDetails(con, data.appid, data.date, 1, function (invoiceDetails) {
                    if (invoiceDetails.status == 1) {
                        /*
                        * get some master value for invoice
                        */
                        con.query('SELECT '
                            + 'value,md_id '
                            + 'FROM master_data_values '
                            + 'WHERE status = ? AND (md_id = ? OR md_id = ? OR md_id = ?)'
                            , [1, 'Late Fee', 'Financial Charges', 'Partial Pay PCT']
                            , function (errLate, late_fee_rows, fields) {
                                if (errLate) {
                                    obj.status = 0;
                                    obj.message = "We can't create the invoice right now. Please try again.";
                                    return callback(obj);
                                } else {
                                    /*
                                    * Get details according to result
                                    */
                                    var lateFee = 0;
                                    var finCharge = 0;
                                    var PartialPayPCT = 0;
                                    if (late_fee_rows.length > 0) {
                                        var late = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Late Fee';
                                        });
                                        lateFee = (late.length > 0) ? late[0].value : 0;
                                        var financial = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Financial Charges';
                                        });
                                        finCharge = (financial.length > 0) ? financial[0].value : 0;
                                        var PartialPay = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Partial Pay PCT';
                                        });
                                        PartialPayPCT = (PartialPay.length > 0) ? PartialPay[0].value : 0;
                                    }
                                    var checkLate = 0;
                                    var lateFeePatient = 0;
                                    var finChargePatient = 0;

                                    var newAmt = 0;
                                    var newAmount = 0;
                                    /*
                                    * Marge record according to final result
                                    */
                                    var allDetails = result && result.reduce(function (accumulator, currentValue, currentindex) {
                                        var installmentFin = 0;
                                        /*
                                        * Get and merge last invoice details
                                        */
                                        var oldAmount = (invoiceDetails.child.length > 0) ? invoiceDetails.child.filter((s, idx) => s.pp_id == currentValue.pp_id) : [];
                                        // check previous record found or not

                                        if (oldAmount.length > 0) {
                                            result[currentindex].principal_amount = oldAmount[0].principal_amount;
                                            result[currentindex].currentInterest = oldAmount[0].interest;
                                            result[currentindex].palnAmount = oldAmount[0].interest + oldAmount[0].principal_amount;
                                            var previousPlanBlc = oldAmount[0].interest + oldAmount[0].principal_amount;
                                        } else {
                                            result[currentindex].principal_amount = 0;
                                            result[currentindex].currentInterest = 0;
                                            result[currentindex].palnAmount = 0;
                                            previousPlanBlc = null;
                                        }
                                        result[currentindex].oldLateFee = invoiceDetails.oldLateFee;
                                        result[currentindex].oldFinCharge = invoiceDetails.oldFinCharge;
                                        result[currentindex].due_date = invoiceDetails.due_date;
                                        result[currentindex].prevBlc = invoiceDetails.prevBlc;

                                        result[currentindex].fin_charge = 0;

                                        if (!accumulator[currentValue.patient_id]) {
                                            lateFeePatient = 0;
                                            finChargePatient = 0;
                                            checkLate = 0;
                                            newAmt = 0;
                                            newAmount = 0;
                                            installmentFin = 0;
                                            // check monthly amount with previous balance amount

                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            result[currentindex].currentInterest += newAmount;
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            // check due date
                                            if ((new Date() > new Date(currentValue.due_date) && currentValue.due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;
                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }
                                                    result[currentindex].fin_charge = installmentFin;
                                                }
                                            }
                                            accumulator[currentValue.patient_id] = {
                                                patient_id: currentValue.patient_id,
                                                totalAmt: newAmt,
                                                finCharge: finChargePatient,
                                                lateFee: lateFeePatient,
                                                //oldBlc: currentValue.oldBlc,
                                                oldLateFee: (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee,
                                                oldFinCharge: (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge,
                                                prevBlc: currentValue.prevBlc,
                                                invoiceStatus: invoiceDetails.invoiceStatus,
                                                invoiceFinStatus: invoiceDetails.fin_charge_waived,
                                                invoiceLateStatus: invoiceDetails.late_fee_waived,
                                            };
                                        } else {
                                            var newAmt = 0;
                                            // check monthly amount with previous balance amount
                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            result[currentindex].currentInterest += newAmount;
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            if ((new Date() > new Date(currentValue.due_date) && currentValue.due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;
                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }

                                                    result[currentindex].fin_charge = installmentFin;
                                                }


                                            }
                                            prevBlc = 0;
                                            accumulator[currentValue.patient_id].totalAmt += newAmt;
                                            accumulator[currentValue.patient_id].finCharge = finChargePatient;
                                            accumulator[currentValue.patient_id].lateFee = lateFeePatient;
                                            //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
                                            accumulator[currentValue.patient_id].oldLateFee = (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee;
                                            accumulator[currentValue.patient_id].oldFinCharge = (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge;
                                            //accumulator[currentValue.patient_id].prevBlc += prevBlc;
                                        }
                                        return accumulator;

                                    }, []);
                                    var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
                                        return allDetails[patient_id];
                                    })
                                    //console.log(patientGroup)
                                    //console.log(result)
                                    //obj.status = 1;
                                    //obj.message = "Invoice created successfully.";
                                    //return callback(obj);
                                    /*
                                    * Send request for insrest details
                                    */
                                    var minLenght = patientGroup.length;
                                    patientGroup && patientGroup.map((dataArr, idx) => {
                                        dataArr.totalAmt = dataArr.totalAmt + dataArr.prevBlc;
                                        mainTableDate(con, dataArr.patient_id, dataArr.totalAmt, data.date, data.appid, dataArr.prevBlc, dataArr.finCharge, dataArr.lateFee, dataArr.oldLateFee, dataArr.oldFinCharge, data.current_user_id, result, 1, dataArr.invoiceStatus, dataArr.invoiceFinStatus, dataArr.invoiceLateStatus, function (mainResponce) {

                                            if (0 === --minLenght) {
                                                obj.status = 1;
                                                obj.message = "Invoice created successfully.";
                                                return callback(obj);
                                            }
                                        });
                                    })
                                }
                            })
                    } else {
                        obj.status = 0;
                        obj.message = "We can't create the invoice right now. Please try again.";
                        return callback(obj);
                    }
                })
            }
        });

}

var createInvoice = function (con, data, callback) {
    let moment = require('moment');
    var reqDate = moment(data.date).set("date", 15).format('YYYY-MM-DD');
    var obj = {};
    //console.log(data)
    /*
    * Get customer installemt according to request
    */
    con.query('SELECT payment_plan.installments_count,payment_plan.pp_id,DATE_FORMAT(payment_plan.date_created, "%Y-%m-%d") as date_created,credit_applications.application_id,credit_applications.patient_id,payment_plan.term AS payment_term_month,payment_plan.discounted_interest_rate,payment_plan.interest_rate, '
        + 'payment_plan.monthly_amount,payment_plan.last_month_amount,payment_plan.remaining_amount,payment_plan.installments_count,payment_plan.loan_type,payment_plan.loan_amount '
        + 'FROM payment_plan '
        + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
        + 'LEFT JOIN patient_procedure ON patient_procedure.PP_id = payment_plan.pp_id AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <=? '
        + 'WHERE payment_plan.plan_status = ?  AND payment_plan.application_id=? AND payment_plan.remaining_amount > ? '
        + 'AND payment_plan.pp_id NOT IN (SELECT patient_invoice_details.pp_id FROM patient_invoice_details INNER JOIN patient_invoice ON patient_invoice.invoice_id = patient_invoice_details.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id AND patient_invoice.due_date=?) '
        + 'ORDER by credit_applications.patient_id ',
        [reqDate, 1, data.appid, 0, data.date]
        , function (error, result, fields) {

            //var dateValid = moment(moment().format('YYYY') + '-' + moment().format('MM') + '-15').format('YYYY-MM-DD');
            //result = result.filter((s, idx) => new Date(s.date_created) <= new Date(dateValid));
            /*
            * Check any error 
            */
            if (error || result.length == 0) {
                obj.status = 0;
                obj.message = "We can't create the invoice right now. Please try again.";
                return callback(obj);
            } else {
                /*
                * get last invoice details
                */

                getPreinvoiceDetails(con, data.appid, data.date, 0, function (invoiceDetails) {
                    console.log(invoiceDetails)
                    //obj.status = 0;
                    //obj.message = "We can't create the invoice right now. Please try again.";
                    //return callback(obj);
                    //return false;
                    if (invoiceDetails.status == 1) {
                        /*
                        * get some master value for invoice
                        */
                        con.query('SELECT '
                            + 'value,md_id '
                            + 'FROM master_data_values '
                            + 'WHERE status = ? AND (md_id = ? OR md_id = ? OR md_id = ?)'
                            , [1, 'Late Fee', 'Financial Charges', 'Partial Pay PCT']
                            , function (errLate, late_fee_rows, fields) {
                                if (errLate) {
                                    obj.status = 0;
                                    obj.message = "We can't create the invoice right now. Please try again.";
                                    return callback(obj);
                                } else {
                                    /*
                                    * Get details according to result
                                    */
                                    var lateFee = 0;
                                    var finCharge = 0;
                                    var PartialPayPCT = 0;
                                    if (late_fee_rows.length > 0) {
                                        var late = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Late Fee';
                                        });
                                        lateFee = (late.length > 0) ? late[0].value : 0;
                                        var financial = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Financial Charges';
                                        });
                                        finCharge = (financial.length > 0) ? financial[0].value : 0;
                                        var PartialPay = late_fee_rows.filter(function (item) {
                                            return item.md_id == 'Partial Pay PCT';
                                        });
                                        PartialPayPCT = (PartialPay.length > 0) ? PartialPay[0].value : 0;
                                    }
                                    var checkLate = 0;
                                    var lateFeePatient = 0;
                                    var finChargePatient = 0;

                                    var newAmt = 0;
                                    var newAmount = 0;
                                    /*
                                    * Marge record according to final result
                                    */
                                    var allDetails = result && result.reduce(function (accumulator, currentValue, currentindex) {
                                        var installmentFin = 0;
                                        /*
                                        * Get and merge last invoice details
                                        */
                                        var oldAmount = (invoiceDetails.child.length > 0) ? invoiceDetails.child.filter((s, idx) => s.pp_id == currentValue.pp_id) : [];
                                        // check previous record found or not
                                        if (oldAmount.length > 0) {
                                            //result[currentindex].principal_amount = (oldAmount[0].invoice_status != 5) ? oldAmount[0].principal_amount : 0;
                                            result[currentindex].principal_amount = oldAmount[0].principal_amount;
                                            result[currentindex].currentInterest = oldAmount[0].interest;
                                            result[currentindex].palnAmount = oldAmount[0].interest + oldAmount[0].principal_amount;
                                            //var previousPlanBlc = (oldAmount[0].invoice_status != 5) ? oldAmount[0].interest + oldAmount[0].principal_amount : oldAmount[0].interest;
                                            var previousPlanBlc = (oldAmount[0].invoice_status != 5) ? oldAmount[0].interest + oldAmount[0].principal_amount : oldAmount[0].interest;
                                            //var lastInvoiceStatus = oldAmount[0].invoice_status;
                                        } else {
                                            result[currentindex].principal_amount = 0;
                                            result[currentindex].currentInterest = 0;
                                            result[currentindex].palnAmount = 0;
                                            var previousPlanBlc = null;
                                            //var lastInvoiceStatus = oldAmount[0].invoice_status;
                                        }
                                        if (oldAmount[0].invoice_status == 5) {
                                            //invoiceDetails.prevBlc += oldAmount[0].interest;
                                        }
                                        result[currentindex].oldLateFee = invoiceDetails.oldLateFee;
                                        result[currentindex].oldFinCharge = invoiceDetails.oldFinCharge;
                                        result[currentindex].due_date = invoiceDetails.due_date;
                                        result[currentindex].prevBlc = invoiceDetails.prevBlc;

                                        result[currentindex].fin_charge = 0;

                                        if (!accumulator[currentValue.patient_id]) {
                                            lateFeePatient = 0;
                                            finChargePatient = 0;
                                            checkLate = 0;
                                            newAmt = 0;
                                            newAmount = 0;
                                            installmentFin = 0;
                                            // check monthly amount with previous balance amount

                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            if (oldAmount[0].invoice_status == 5) {
                                                var lastAmt = currentValue.monthly_amount - newAmount;
                                                lastAmt = parseFloat(lastAmt.toFixed(2))
                                                result[currentindex].principal_amount = result[currentindex].principal_amount - lastAmt;
                                                result[currentindex].currentInterest += newAmount;
                                                newAmount = result[currentindex].currentInterest;
                                            } else {
                                                result[currentindex].currentInterest += newAmount;
                                            }
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            // check due date
                                            // check deferred invoice
                                            if (oldAmount[0].invoice_status == 5) {

                                                //check fincal chage apply or not
                                                if (oldAmount[0].fin_charge_waived == 0) {
                                                    //DeferredAmount = oldAmount[0].principal_amount;
                                                    installmentFin = parseFloat(((previousPlanBlc + oldAmount[0].principal_amount) * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                }
                                                // check late fee apply or not
                                                if (oldAmount[0].late_fee_waived == 0) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                }
                                                //} else if ((new Date() > new Date(oldAmount[0].due_date) && oldAmount[0].due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                            } else if (oldAmount[0].invoice_status == 3) {

                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;

                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }
                                                    result[currentindex].fin_charge = installmentFin;
                                                }
                                            }
                                            accumulator[currentValue.patient_id] = {
                                                patient_id: currentValue.patient_id,
                                                totalAmt: newAmt,
                                                finCharge: finChargePatient,
                                                lateFee: lateFeePatient,
                                                //oldBlc: currentValue.oldBlc,
                                                oldLateFee: (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee,
                                                oldFinCharge: (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge,
                                                prevBlc: currentValue.prevBlc,
                                                invoiceStatus: invoiceDetails.invoiceStatus,
                                                invoiceFinStatus: invoiceDetails.fin_charge_waived,
                                                invoiceLateStatus: invoiceDetails.late_fee_waived,
                                            };
                                        } else {
                                            var newAmt = 0;
                                            // check monthly amount with previous balance amount
                                            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                currentValue.installments_count = currentValue.payment_term_month - 1;
                                                currentValue.monthly_amount = currentValue.last_month_amount;
                                            }
                                            var InterestRate = currentValue.discounted_interest_rate / 100;
                                            var newAmount = currentValue.remaining_amount * InterestRate / 12;
                                            newAmount = parseFloat(newAmount.toFixed(2))
                                            //result[currentindex].currentInterest += newAmount;
                                            if (oldAmount[0].invoice_status == 5) {
                                                var lastAmt = currentValue.monthly_amount - newAmount;
                                                lastAmt = parseFloat(lastAmt.toFixed(2))
                                                result[currentindex].principal_amount = result[currentindex].principal_amount - lastAmt;
                                                result[currentindex].currentInterest += newAmount;
                                                newAmount = result[currentindex].currentInterest;
                                            } else {
                                                result[currentindex].currentInterest += newAmount;
                                            }
                                            newAmount = currentValue.monthly_amount - newAmount;
                                            if (currentValue.remaining_amount >= newAmount) {
                                                result[currentindex].principal_amount += newAmount;
                                                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                                                result[currentindex].palnAmount += currentValue.monthly_amount;
                                                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                                                    var variation = currentValue.remaining_amount - result[currentindex].principal_amount;
                                                    result[currentindex].principal_amount += variation;
                                                    newAmt += variation;
                                                }
                                            } else {
                                                result[currentindex].principal_amount += currentValue.remaining_amount;
                                                result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                                                newAmt = parseFloat(newAmt.toFixed(2))
                                            }
                                            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
                                            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
                                            /*
                                            * Check and apply late fee or fincial charge
                                            */
                                            if (oldAmount[0].invoice_status == 5) {
                                                //check fincal chage apply or not
                                                if (oldAmount[0].fin_charge_waived == 0) {
                                                    //DeferredAmount = oldAmount[0].principal_amount;
                                                    installmentFin = parseFloat(((previousPlanBlc + oldAmount[0].principal_amount) * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                }
                                                // check late fee apply or not
                                                if (oldAmount[0].late_fee_waived == 0) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                }
                                                //} else if ((new Date() > new Date(oldAmount[0].due_date) && oldAmount[0].due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
                                            } else if (oldAmount[0].invoice_status == 3) {
                                                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                                                checkLate = 1;
                                                lateFeePatient = lateFee;
                                                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                finChargePatient += installmentFin;
                                                result[currentindex].fin_charge = installmentFin;
                                            } else {
                                                if (checkLate == 1 || (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) <= PartialPayPCT)) {
                                                    lateFeePatient = lateFee;
                                                    checkLate = 1;
                                                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                    finChargePatient += installmentFin;
                                                    result[currentindex].fin_charge = installmentFin;
                                                } else {
                                                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                                    if (invoiceDetails.lateApply == 1 && parseFloat(invoiceDetails.latePCT) < 100) {
                                                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                                                        finChargePatient += installmentFin;
                                                    } else {
                                                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                                    }

                                                    result[currentindex].fin_charge = installmentFin;
                                                }


                                            }
                                            prevBlc = 0;
                                            accumulator[currentValue.patient_id].totalAmt += newAmt;
                                            accumulator[currentValue.patient_id].finCharge = finChargePatient;
                                            accumulator[currentValue.patient_id].lateFee = lateFeePatient;
                                            //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
                                            accumulator[currentValue.patient_id].oldLateFee = (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee;
                                            accumulator[currentValue.patient_id].oldFinCharge = (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge;
                                            accumulator[currentValue.patient_id].prevBlc = currentValue.prevBlc;
                                        }
                                        return accumulator;

                                    }, []);
                                    var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
                                        return allDetails[patient_id];
                                    })
                                    //console.log(patientGroup)
                                    //console.log(result)
                                    //obj.status = 1;
                                    //obj.message = "Invoice created successfully.";
                                    //return callback(obj);
                                    /*
                                    * Send request for insrest details
                                    */
                                    var minLenght = patientGroup.length;
                                    patientGroup && patientGroup.map((dataArr, idx) => {
                                        dataArr.totalAmt = dataArr.totalAmt + dataArr.prevBlc;
                                        mainTableDate(con, dataArr.patient_id, dataArr.totalAmt, data.date, data.appid, dataArr.prevBlc, dataArr.finCharge, dataArr.lateFee, dataArr.oldLateFee, dataArr.oldFinCharge, data.current_user_id, result, 0, dataArr.invoiceStatus, dataArr.invoiceFinStatus, dataArr.invoiceLateStatus, function (mainResponce) {

                                            if (0 === --minLenght) {
                                                obj.status = 1;
                                                obj.message = "Invoice created successfully.";
                                                return callback(obj);
                                            }
                                        });
                                    })
                                }
                            })
                    } else {
                        obj.status = 0;
                        obj.message = "We can't create the invoice right now. Please try again.";
                        return callback(obj);
                    }
                })
            }
        });

}


var mainTableDate = function (con, key, value, due_date, appid, prevBlc, finCharge, lateFee, oldLateFee, oldFinCharge, current_user_id, result, on_fly, invoiceStatus, invoiceFinStatus, invoiceLateStatus, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('INSERT INTO patient_invoice (invoice_number, application_id, patient_id, payment_amount, previous_blc,previous_late_fee, previous_fin_charge, late_fee_due, fin_charge_due, due_date, invoice_status, on_fly, fin_charge_waived, late_fee_waived, created_by, modified_by, date_created, date_modified) '
        + 'VALUES(((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice as m)),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [appid, key, value, prevBlc, oldLateFee, oldFinCharge, lateFee, finCharge, due_date, invoiceStatus, on_fly, invoiceFinStatus, invoiceLateStatus, current_user_id, current_user_id, current_date, current_date]
        , function (error, resultMain, fields) {

            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                var resultFilter = result.filter((r) => r.patient_id == key);
                childLenght = resultFilter.length;
                resultFilter.forEach(function (element, idx) {

                    childTableDate(con, resultMain.insertId, element, current_user_id, invoiceStatus, function (childResponce) {
                        if (0 === --childLenght) {
                            obj.status = 1;
                            return callback(obj);
                        }
                    });
                })
            }
        });

}

var childTableDate = function (con, insertId, element, current_user_id, invoiceStatus, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    //element.monthly_amount = element.monthly_amount + element.currentInterest + element.principal_amount;
    con.query('INSERT INTO patient_invoice_details (invoice_id, application_id, pp_id, invoice_amount, installment_interest, installment_principal_amount, interest, principal_amount, fin_charge, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [insertId, element.application_id, element.pp_id, element.palnAmount, element.currentInterest, element.principal_amount, element.currentInterest, element.principal_amount, element.fin_charge, current_date, current_date, current_user_id, current_user_id]
        , function (err, resultPPIns) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                var count = (invoiceStatus == 5) ? element.installments_count : element.installments_count + 1;
                con.query('UPDATE payment_plan SET installments_count=? WHERE pp_id=?',
                    [count, element.pp_id]
                    , function (errUpdate, resultPPIns) {
                        if (errUpdate) {
                            obj.status = 0;
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            return callback(obj);
                        }
                    })
            }
        })

}

var getCurrentPlanDetails = function (con, id, callback) {
    con.query('SELECT payment_plan.pp_id,payment_plan.plan_number, '
        + 'payment_plan.term as payment_term_month, payment_plan.loan_type, payment_plan.interest_rate, payment_plan.discounted_interest_rate,'
        + 'payment_plan.provider_id,payment_plan.doctor_id,  '
        + 'payment_plan.amount,payment_plan.discounted_interest_rate,payment_plan.monthly_amount,payment_plan.remaining_amount, '
        + 'payment_plan.loan_amount, payment_plan.late_count, payment_plan.missed_count,payment_plan.plan_updated,doctors.f_name as docter_fname,doctors.l_name as docter_l_name, '
        + 'credit_applications.application_id, credit_applications.application_no,credit_applications.approve_amount,credit_applications.remaining_amount,credit_applications.patient_id,'
        + 'patient.f_name,patient.l_name,patient.bank_name,patient.bank_address,patient.rounting_no,patient.account_number,patient.account_name,patient.account_type, patient.email, '
        + 'patient_procedure.provider_location_id as provider_location,patient_procedure.procedure_amt as procedure_amount, '
        + 'patient.patient_ac,patient.peimary_phone, '
        + 'patient_address.address1, '
        + 'patient_address.address2, '
        + 'patient_address.City, '
        + 'patient_address.zip_code, '
        + 'states.name as state_name '

        + 'FROM payment_plan '
        + 'INNER JOIN doctors ON doctors.id=payment_plan.doctor_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_procedure ON patient_procedure.pp_id=payment_plan.pp_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id = patient.patient_id AND patient_address.primary_address=1 '
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'WHERE payment_plan.pp_id = ?',
        [
            id
        ]
        , function (err, plansDetails) {
            if (err) {
                var obj = {
                    status: 0,
                    message: 'Current plan not found.'
                }
                callback(obj)
            } else {
                var obj = {
                    status: 1,
                    data: plansDetails
                }
                callback(obj)
            }
        });
}

var cancelPlan = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    //data.plan_id
    let current_user_id = data.current_user_id;
    /*
    * Update remaning amount according to particuler plan
    * Also update plan status if remianing amount completly paid
    */
    con.query('UPDATE payment_plan SET plan_status=?, note=?, date_modified=?,modified_by=? WHERE pp_id=?'
        ,
        [0, capitalize(data.data.note), current_date, current_user_id, data.data.plan_id]
        , function (ppErr) {
            if (ppErr) {
                var obj = {
                    status: 0
                }
                return callback(obj)
            } else {
                con.query('UPDATE patient_procedure SET procedure_status=?, date_modified=?,modified_by=? WHERE pp_id=?'
                    ,
                    [0, current_date, current_user_id, data.data.plan_id]
                    , function (ppErr) {
                        if (ppErr) {
                            var obj = {
                                status: 0
                            }
                            return callback(obj)
                        } else {
                            var obj = {
                                status: 1
                            }
                            return callback(obj)
                        }
                    })
            }
        })
}

var getOptionToClose = function (con, id, callback) {
    var obj = {};
    con.query('SELECT '
        + 'value as name, status_id as option_id '
        + 'FROM master_data_values '
        + 'WHERE md_id = ? AND status=?'
        , ['Option To Close Plan', 1]
        , function (error, rows, fields) {

            if (error || rows.length == 0) {
                obj.status = 0;
                obj.message = "Option not found.";
                return callback(obj);
            } else {
                con.query('SELECT '
                    + 'invoice_id '
                    + 'FROM patient_invoice_details '
                    + 'WHERE pp_id = ?'
                    , [id]
                    , function (error, invoice, fields) {
                        if (error || rows.length == 0) {
                            obj.status = 0;
                            obj.message = "Option not found.";
                            return callback(obj);
                        } else {
                            con.query('SELECT '
                                + 'payment_plan.provider_id,payment_plan.plan_status, patient.patient_id,patient.patient_ac,patient.f_name as first_name,'
                                + 'patient.m_name as middle_name,patient.l_name as last_name,patient.alias_name,'
                                + 'patient.bank_name,patient.bank_address,patient.rounting_no,patient.account_number,patient.account_name,patient.account_type,'
                                + 'patient.malling_address,patient.peimary_phone, master_data_values.value as plan_status_name, '
                                + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name,'
                                + 'credit_applications.application_id, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date, credit_applications.application_no, credit_applications.approve_amount, (credit_applications.remaining_amount + credit_applications.override_amount) AS remaining_amount '
                                + 'FROM payment_plan '
                                + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
                                + 'INNER JOIN patient ON credit_applications.patient_id=patient.patient_id '

                                + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                                /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                                + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                                + 'INNER JOIN master_data_values on master_data_values.status_id = payment_plan.plan_status '
                                + 'WHERE payment_plan.pp_id = ? AND master_data_values.md_id=? ORDER BY credit_applications.application_id DESC',
                                [id, 'Customer Plan Status']
                                , function (err, applicationDetails) {
                                    console.log(error)
                                    console.log(this.sql)
                                    if (error || rows.length == 0) {
                                        obj.status = 0;
                                        obj.message = "Option not found.";
                                        return callback(obj);
                                    } else {
                                        con.query('SELECT '
                                            + 'provider.provider_id,provider.name,provider.primary_phone, provider.email, '
                                            + 'provider_location.address1, provider_location.address2, provider_location.city, provider_location.zip_code, states.name as state_name,countries.name as country_name, '
                                            + 'provider.name,provider.provider_ac '
                                            + 'FROM provider '
                                            + 'INNER JOIN master_data_values ON provider.provider_type=master_data_values.mdv_id '
                                            + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
                                            + 'INNER JOIN states ON states.state_id=provider_location.state '
                                            + 'INNER JOIN countries ON countries.id=states.country_id '
                                            + 'WHERE master_data_values.status_id=?'
                                            ,
                                            [
                                                1
                                            ]
                                            , function (providerError, providerDetails, fields) {

                                                if (providerError || rows.providerDetails == 0) {
                                                    obj.status = 0;
                                                    obj.message = "Option not found.";
                                                    return callback(obj);
                                                } else {
                                                    obj.status = 1;
                                                    obj.result = rows;
                                                    obj.invoiceExits = (invoice.length > 0) ? 1 : 0;
                                                    obj.customerDetails = (applicationDetails.length > 0) ? applicationDetails[0] : '';
                                                    obj.providerDetails = (providerDetails.length > 0) ? providerDetails[0] : '';
                                                    return callback(obj);
                                                }
                                            })
                                    }
                                })

                        }
                    })
            }
        });
}

var closePlan = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('SELECT payment_plan.loan_amount,credit_applications.remaining_amount,credit_applications.application_id FROM payment_plan '
        + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
        + 'WHERE pp_id = ?'
        , [data.data.pp_id]
        , function (error, result, fields) {
            if (error || result.length == 0) {
                obj.status = 0;
                obj.message = "Please try again.";
                return callback(obj);
            } else {
                var totalAmt = result[0].loan_amount + result[0].remaining_amount;
                con.query('UPDATE `credit_applications` SET '
                    + 'remaining_amount=?, date_modified=?,modified_by=? '
                    + 'WHERE application_id = ?'
                    , [totalAmt, current_date, data.current_user_id, result[0].application_id]
                    , function (error, rows, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.message = "Please try again.";
                            return callback(obj);
                        } else {
                            con.query('UPDATE `payment_plan` SET '
                                + 'plan_status=?, note=?, date_modified=?,modified_by=? '
                                + 'WHERE pp_id = ?'
                                , [0, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
                                , function (error, rows, fields) {

                                    if (error || rows.length == 0) {
                                        obj.status = 0;
                                        obj.message = "Please try again.";
                                        return callback(obj);
                                    } else {
                                        con.query('UPDATE `patient_procedure` SET '
                                            + 'procedure_status=?, date_modified=?,modified_by=? '
                                            + 'WHERE pp_id = ?'
                                            , [0, current_date, data.current_user_id, data.data.pp_id]
                                            , function (error, rows, fields) {
                                                if (error || rows.length == 0) {
                                                    obj.status = 0;
                                                    obj.message = "Please try again.";
                                                    return callback(obj);
                                                } else {
                                                    obj.status = 1;
                                                    obj.message = "Plan has been closed successfully.";
                                                    return callback(obj);
                                                }
                                            })
                                    }
                                });
                        }
                    })
            }
        })
    return false;
    con.query('UPDATE `payment_plan` SET '
        + 'plan_status=?, note=?, date_modified=?,modified_by=? '
        + 'WHERE pp_id = ?'
        , [0, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
        , function (error, rows, fields) {

            if (error || rows.length == 0) {
                obj.status = 0;
                obj.message = "Please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Plan has been closed successfully.";
                return callback(obj);
            }
        });

}

var pauseTrigger = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    let packages = [];
    let packagesPlan = [];
    con.query('SELECT patient_invoice.invoice_id,patient_invoice_details.pp_id,payment_plan.installments_count FROM patient_invoice INNER JOIN credit_applications on credit_applications.application_id=patient_invoice.application_id INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id INNER JOIN payment_plan on payment_plan.pp_id=patient_invoice_details.pp_id WHERE patient_invoice.invoice_status NOT IN(0,1,2,4) AND credit_applications.patient_id=? AND patient_invoice.due_date BETWEEN ? AND ? ORDER BY patient_invoice.invoice_id DESC LIMIT 2 '
        , [data.patient_id, data.start_date, data.end_date]
        , function (error, result, fields) {

            if (error) {
                obj.status = 0;
                obj.message = "Please try again.";
                return callback(obj);
            } else {
                result.map((datas, idx) => {
                    // packages[tracking_index] = {"module_data": module_data, "package_data": package};
                    packages.push(datas.invoice_id);
                    datas.installments_count = datas.installments_count - 1;
                    con.query('UPDATE `payment_plan` SET '
                        + 'installments_count=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE pp_id=? '
                        , [datas.installments_count, current_date, data.current_user_id, datas.pp_id]
                        , function (error, result, fields) {
                        })

                })
                if (packages.length > 0) {
                    con.query('UPDATE `patient_invoice` SET '
                        + 'invoice_status=?, '
                        + 'fin_charge_waived=?, '
                        + 'late_fee_waived=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE invoice_id IN(?) '
                        , [5, data.fin_charge_waived, data.late_fee_waived, current_date, data.current_user_id, packages]
                        , function (error, result, fields) {
                            if (error) {
                                obj.status = 0;
                                obj.message = "Please try again.";
                                return callback(obj);
                            } else {
                                obj.status = 1;
                                return callback(obj);
                            }
                        })

                } else {
                    obj.status = 1;
                    return callback(obj);
                }

            }
        })

}

var pauseTriggerUpdate = function (con, data, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    let packages = [];
    con.query('SELECT DATE_FORMAT(start_date, "%Y-%m-%d") as start_date, DATE_FORMAT(end_date, "%Y-%m-%d") as end_date FROM patient_payment_pause WHERE id=? '
        , [data.id]
        , function (error, result, fields) {
            //con.query('SELECT patient_invoice.invoice_id,patient_invoice_details.pp_id,payment_plan.installments_count FROM patient_invoice INNER JOIN credit_applications on credit_applications.application_id=patient_invoice.application_id INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id INNER JOIN payment_plan on payment_plan.pp_id=patient_invoice_details.pp_id WHERE patient_invoice.due_date>=? AND patient_invoice.due_date <= ? AND patient_invoice.invoice_id=(SELECT patient_invoice.invoice_id FROM patient_invoice INNER JOIN credit_applications ON credit_applications.application_id=patient_invoice.application_id WHERE patient_invoice.invoice_status=5 AND credit_applications.patient_id=? ORDER BY patient_invoice.invoice_id DESC LIMIT 2) '
            con.query('SELECT patient_invoice.invoice_id,patient_invoice_details.pp_id,payment_plan.installments_count FROM patient_invoice INNER JOIN credit_applications on credit_applications.application_id=patient_invoice.application_id INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id INNER JOIN payment_plan on payment_plan.pp_id=patient_invoice_details.pp_id WHERE patient_invoice.due_date>=? AND patient_invoice.due_date <= ? AND credit_applications.patient_id=? ORDER BY patient_invoice.invoice_id DESC LIMIT 2 '
                , [result[0].start_date, result[0].end_date, data.patient_id]
                , function (error, result, fields) {

                    if (error) {
                        obj.status = 1;
                        return callback(obj);
                    } else {
                        result.map((datas, idx) => {
                            // packages[tracking_index] = {"module_data": module_data, "package_data": package};
                            packages.push(datas.invoice_id);
                            datas.installments_count = datas.installments_count + 1;
                            con.query('UPDATE `payment_plan` SET '
                                + 'installments_count=?, '
                                + 'date_modified=?, '
                                + 'modified_by=? '
                                + 'WHERE pp_id=? '
                                , [datas.installments_count, current_date, data.current_user_id, datas.pp_id]
                                , function (error, result, fields) {
                                })
                        })
                        if (packages.length > 0) {
                            con.query('UPDATE `patient_invoice` SET '
                                + 'invoice_status=?, '
                                + 'fin_charge_waived=?, '
                                + 'late_fee_waived=?, '
                                + 'date_modified=?, '
                                + 'modified_by=? '
                                + 'WHERE invoice_id IN(?) '
                                , [3, null, null, current_date, data.current_user_id, packages]
                                , function (error, result, fields) {

                                    if (error) {
                                        obj.status = 0;
                                        obj.message = "Please try again.";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        return callback(obj);
                                    }
                                })
                        } else {
                            obj.status = 1;
                            return callback(obj);
                        }

                    }
                })

        })

}

var getclosePlanDetails = function (con, id, callback) {
    var obj = {};
    con.query('SELECT md_id, value, status_id as option_id '
        + 'FROM master_data_values '
        + 'WHERE (md_id=? OR md_id=? OR md_id=?) AND status=?'
        , ['Interest Rate on Default', 'Term on Default', 'Payment Method', 1]
        , function (error, closeInt, fields) {
            if (error) {
                obj.status = 0;
                obj.message = "Something wrong. Please try again.";
                callback(obj)
            } else {
                con.query('SELECT term,loan_type,interest_rate,discounted_interest_rate,monthly_amount,last_month_amount,plan_number,amount,remaining_amount,loan_amount,installments_count '
                    + 'FROM payment_plan '
                    + 'WHERE pp_id=?'
                    , [id]
                    , function (error, result, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.message = "Something wrong. Please try again.";
                            callback(obj)
                        } else {
                            con.query('SELECT patient_invoice_details.installment_interest,installment_principal_amount,interest,principal_amount,fin_charge, '
                                + 'patient_invoice.late_fee_received, patient_invoice.invoice_status '
                                + 'FROM patient_invoice_details '
                                + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '
                                + 'WHERE pp_id=?'
                                , [id]
                                , function (error, invoiceResult, fields) {
                                    if (error) {
                                        obj.status = 0;
                                        obj.message = "Something wrong. Please try again.";
                                        callback(obj)
                                    } else {
                                        con.query('SELECT SUM(amount_paid) customer_paid '
                                            + 'FROM patient_invoice_payment '
                                            + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id = patient_invoice_payment.invoice_id '
                                            + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id = patient_invoice.invoice_id '
                                            + 'WHERE patient_invoice_details.pp_id=? AND (patient_invoice.invoice_status = ? OR patient_invoice.invoice_status = ?)'
                                            , [id, 1, 4]
                                            , function (error, customerAmount, fields) {
                                                if (error) {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong. Please try again.";
                                                    callback(obj)
                                                } else {
                                                    con.query('SELECT provider_invoice.check_amount,provider_invoice.mdv_invoice_status_id as invoice_status '
                                                        + 'FROM provider_invoice '
                                                        + 'INNER JOIN provider_invoice_detial ON provider_invoice_detial.provider_invoice_id = provider_invoice.provider_invoice_id '
                                                        + 'WHERE provider_invoice_detial.pp_id=? AND provider_invoice_detial.status=?'
                                                        , [id, 0]
                                                        , function (error, providerInvoiceDetails, fields) {
                                                            if (error) {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong. Please try again.";
                                                                callback(obj)
                                                            } else {
                                                                con.query('SELECT refund_due,iou_flag,invoice_number '
                                                                    + 'FROM provider_refund '
                                                                    + 'WHERE pp_id=?'
                                                                    , [id]
                                                                    , function (error, providerRefundDetails, fields) {
                                                                        if (error) {
                                                                            obj.status = 0;
                                                                            obj.message = "Something wrong. Please try again.";
                                                                            callback(obj)
                                                                        } else {
                                                                            var intDef = closeInt.filter(function (item) {
                                                                                return item.md_id == 'Interest Rate on Default';
                                                                            });
                                                                            intDef = (intDef.length > 0) ? intDef[0].value : 0;
                                                                            var termDef = closeInt.filter(function (item) {
                                                                                return item.md_id == 'Term on Default';
                                                                            });
                                                                            termDef = (termDef.length > 0) ? termDef[0].value : 0;
                                                                            var paymentMethod = closeInt.filter(function (item) {
                                                                                return item.md_id == 'Payment Method';
                                                                            });
                                                                            paymentMethod = (paymentMethod.length > 0) ? paymentMethod : '';


                                                                            obj.status = 1;
                                                                            obj.planDetails = result[0];
                                                                            obj.invoiceDetails = invoiceResult;
                                                                            obj.closeInt = intDef;
                                                                            obj.closeTerm = termDef;
                                                                            obj.paymentMethod = paymentMethod;
                                                                            obj.customerPaid = (customerAmount[0].customer_paid != null) ? customerAmount[0].customer_paid : 0;
                                                                            obj.providerInvoiceDetails = (providerInvoiceDetails.length > 0) ? providerInvoiceDetails[0] : '';
                                                                            obj.providerRefundDetails = (providerRefundDetails.length > 0) ? providerRefundDetails[0] : ''
                                                                            callback(obj)
                                                                        }
                                                                    })
                                                            }
                                                        })
                                                }
                                            })


                                    }
                                })
                        }
                    })
            }
        })
}

var payfullPlan = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};

    con.query('UPDATE `payment_plan` SET '
        + 'remaining_amount=?, adjusted_amount=?, revised_interest_amount=? , plan_status=?, note=?, date_modified=?,modified_by=? '
        + 'WHERE pp_id = ?'
        , [0, data.data.adjusted_amount, data.data.revised_int, 7, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
        , function (error, rows, fields) {

            if (error || rows.length == 0) {
                obj.status = 0;
                obj.message = "Please try again.";
                return callback(obj);
            } else {
                con.query('SELECT invoice_id FROM patient_invoice_details WHERE pp_id=? ORDER BY details_id DESC LIMIT 1 '
                    , [data.data.pp_id]
                    , function (error, invoiceID, fields) {

                        if (error || rows.length == 0) {
                            obj.status = 0;
                            obj.message = "Please try again.";
                            return callback(obj);
                        } else {
                            con.query('UPDATE `patient_invoice` SET '
                                + 'invoice_status=?, comments=?, date_modified=?,modified_by=? '
                                + 'WHERE invoice_id=?'
                                , [0, capitalize(data.data.note), current_date, data.current_user_id, invoiceID[0].invoice_id]
                                , function (error, rows, fields) {
                                    if (error || rows.length == 0) {
                                        obj.status = 0;
                                        obj.message = "Please try again.";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.message = "Plan has been closed successfully.";
                                        return callback(obj);
                                    }
                                })
                        }
                    })
            }
        });

}


var newSettementPlan = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};

    con.query('UPDATE `payment_plan` SET '
        + 'remaining_amount=?, plan_status=?, note=?, date_modified=?,modified_by=? '
        + 'WHERE pp_id = ?'
        , [0, 7, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
        , function (error, rows, fields) {
            if (error || rows.length == 0) {
                obj.status = 0;
                obj.message = "Please try again.";
                return callback(obj);
            } else {
                con.query('SELECT patient_invoice.invoice_id, patient_invoice.invoice_status FROM patient_invoice_details INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id WHERE pp_id=? ORDER BY details_id DESC LIMIT 1 '
                    , [data.data.pp_id]
                    , function (error, invoiceID, fields) {

                        if (error || rows.length == 0) {
                            obj.status = 0;
                            obj.message = "Please try again.";
                            return callback(obj);
                        } else {
                            if (invoiceID[0].invoice_status != 1 && invoiceID[0].invoice_status != 4) {
                                con.query('UPDATE `patient_invoice` SET '
                                    + 'invoice_status=?, comments=?, date_modified=?,modified_by=? '
                                    + 'WHERE invoice_id=?'
                                    , [0, capitalize(data.data.note), current_date, data.current_user_id, invoiceID[0].invoice_id]
                                    , function (error, rows, fields) {
                                        if (error || rows.length == 0) {
                                            obj.status = 0;
                                            obj.message = "Please try again.";
                                            return callback(obj);
                                        } else {
                                            obj.status = 1;
                                            obj.message = "Plan has been closed successfully.";
                                            return callback(obj);
                                        }
                                    })
                            } else {
                                obj.status = 1;
                                obj.message = "Plan has been closed successfully.";
                                return callback(obj);
                            }

                        }
                    })
            }
        });

}

var providerConfirmation = function (con, data, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};

    con.query('UPDATE `payment_plan` SET '
        + 'plan_status=?, note=?, date_modified=?,modified_by=? '
        + 'WHERE pp_id = ?'
        , [9, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.message = 'Please try again.';
                console.log(data)
                callback(obj);
            } else {
                obj.status = 1;
                obj.message = 'Provider confirmation cancellation sent';
                console.log(data)
                callback(obj);
            }
        })
}

var confirmedCancellation = function (con, data, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('UPDATE `payment_plan` SET '
        + 'plan_status=?, date_modified=?,modified_by=? '
        + 'WHERE pp_id = ?'
        , [(data.provider_confirm == 1) ? 10 : 1, current_date, data.current_user_id, data.pp_id]
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.message = 'Please try again.';
                console.log(data)
                callback(obj);
            } else {
                if (data.provider_confirm == 1 && data.refund_type >= 0) {
                    con.query('INSERT INTO `provider_refund` SET '
                        + 'pp_id=?, '
                        + 'provider_id=?, '
                        + 'invoice_number=((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM provider_refund as p)), '
                        + 'iou_flag=?, '
                        + 'status=?, '
                        + 'date_created=?, '
                        + 'date_modified=?, '
                        + 'created_by=?, '
                        + 'modified_by=?',
                        [
                            data.pp_id,
                            data.provider_id,
                            data.refund_type,
                            1,
                            current_date,
                            current_date,
                            data.current_user_id,
                            data.current_user_id
                        ]
                        , function (error, rows, fields) {
                            if (error) {
                                obj.status = 0;
                                obj.message = 'Please try again.';
                                //console.log(data)
                                callback(obj);
                            } else {
                                obj.status = 1;
                                obj.message = 'Plan cancellation confirmed';
                                console.log(data)
                                callback(obj);
                            }
                        })
                } else {
                    obj.status = 1;
                    obj.message = 'Plan cancellation confirmed';
                    console.log(data)
                    callback(obj);
                }
            }
        })
}

var refundCustomerInvoice = function (con, data, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT patient_invoice.invoice_id FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=? '
            , [data.data.pp_id]
            , function (error, result, fields) {
                if (error) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        return callback(obj);
                    });
                } else {
                    let packages = [];
                    result.map((datas, idx) => {
                        // packages[tracking_index] = {"module_data": module_data, "package_data": package};
                        packages.push(datas.invoice_id);
                    })
                    if (packages.length > 0) {
                        con.query('UPDATE `patient_invoice` SET '
                            + 'invoice_status=?, date_modified=?,modified_by=? '
                            + 'WHERE invoice_id IN(?) AND (invoice_status=? OR invoice_status=?)'
                            , [6, current_date, data.current_user_id, packages, 1, 4]
                            , function (error, rows, fields) {
                                if (error) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        return callback(obj);
                                    });
                                } else {
                                    con.query('UPDATE `patient_invoice` SET '
                                        + 'invoice_status=?, date_modified=?,modified_by=? '
                                        + 'WHERE invoice_id IN(?) AND (invoice_status=? OR invoice_status=? OR invoice_status=?)'
                                        , [0, current_date, data.current_user_id, packages, 2, 3, 5]
                                        , function (error, rows, fields) {
                                            if (error) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    return callback(obj);
                                                });
                                            } else {

                                                con.query('INSERT INTO `customer_refund`(`pp_id`, `refund_amt`, `payment_method`, `check_no`, `check_date`, `ach_bank_name`, `ach_routing_no`, `ach_account_no`, `comments`, `status`, `date_created`, `date_modified`, `created_by`, `modified_by`) VALUES '
                                                    + '(?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                                                    ,
                                                    [
                                                        data.data.pp_id,
                                                        data.data.customer_refund,
                                                        data.data.customer_pay_method,
                                                        (data.data.check_no) ? data.data.check_no : null,
                                                        (data.data.check_date) ? data.data.check_date : null,
                                                        (data.data.ach_bank_name) ? data.data.ach_bank_name : null,
                                                        (data.data.ach_routing_no) ? data.data.ach_routing_no : null,
                                                        (data.data.ach_account_no) ? data.data.ach_account_no : null,
                                                        capitalize(data.data.note),
                                                        1,
                                                        current_date,
                                                        current_date,
                                                        data.current_user_id,
                                                        data.current_user_id
                                                    ]
                                                    , function (error, rows, fields) {
                                                        if (error) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.";
                                                                return callback(obj);
                                                            });
                                                        } else {

                                                            con.query('UPDATE `payment_plan` SET '
                                                                + 'plan_status=?, note=?, date_modified=?,modified_by=? '
                                                                + 'WHERE pp_id = ?'
                                                                , [0, capitalize(data.data.note), current_date, data.current_user_id, data.data.pp_id]
                                                                , function (error, rows, fields) {
                                                                    if (error) {
                                                                        obj.status = 0;
                                                                        obj.message = "Something wrong please try again.";
                                                                        callback(obj);
                                                                    } else {
                                                                        con.commit(function (err) {
                                                                            if (err) {
                                                                                con.rollback(function () {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Something wrong please try again.";
                                                                                    return callback(obj);
                                                                                });
                                                                            } else {
                                                                                obj.status = 1;
                                                                                obj.message = "Plan has been closed successfully.";
                                                                                return callback(obj);
                                                                            }
                                                                        })
                                                                    }
                                                                })

                                                        }
                                                    })
                                            }
                                        })
                                }
                            })
                    } else {
                        con.commit(function (err) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    return callback(obj);
                                });
                            } else {
                                obj.status = 1;
                                obj.message = "Plan has been closed successfully.";
                                return callback(obj);
                            }
                        });
                    }
                }
            })
        return false;

    })
}

var refundProviderInvoice = function (con, data, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT provider_invoice.provider_invoice_id,provider_invoice.mdv_invoice_status_id as status FROM provider_invoice INNER JOIN provider_invoice_detial ON provider_invoice_detial.provider_invoice_id=provider_invoice.provider_invoice_id WHERE provider_invoice_detial.pp_id=? AND provider_invoice_detial.status=?'
            , [data.data.pp_id, 0]
            , function (error, result, fields) {
                if (error) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        return callback(obj);
                    });
                } else {
                    if (result.length == 0) {
                        con.query('UPDATE `patient_procedure` SET '
                            + 'procedure_status=?, date_modified=?,modified_by=? '
                            + 'WHERE pp_id = ?'
                            , [0, current_date, data.current_user_id, data.data.pp_id]
                            , function (error, rows, fields) {
                                if (error) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        return callback(obj);
                                    });
                                } else {
                                    con.commit(function (err) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            obj.status = 1;
                                            obj.message = "Plan has been closed successfully.";
                                            return callback(obj);
                                        }
                                    })
                                }
                            })
                    } else if (result[0].status == 4) {
                        con.query('UPDATE `provider_refund` SET '
                            + 'provider_invoice_id=?, '
                            + 'refund_due=?, '
                            + 'comments=?, '
                            + 'status=?, '
                            + 'date_created=?, '
                            + 'modified_by=? '
                            + 'WHERE pp_id=?'
                            ,
                            [
                                result[0].provider_invoice_id,
                                data.data.provider_refund,
                                capitalize(data.data.note),
                                2,
                                current_date,
                                data.current_user_id,
                                data.data.pp_id,
                            ]
                            , function (error, rows, fields) {
                                if (error) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        return callback(obj);
                                    });
                                } else {

                                    con.commit(function (err) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            obj.status = 1;
                                            obj.message = "Plan has been closed successfully.";
                                            return callback(obj);
                                        }
                                    })

                                }
                            })
                    } else {
                        var entryDetails = {
                            current_user_id: data.current_user_id,
                            invoice_id: result[0].provider_invoice_id,
                            comment: data.data.note,
                            pp_id: data.data.pp_id,
                            provider_id: data.data.customerDetails.provider_id,
                        }
                        var dataSql = require('../invoice/admininvoiceSql.js');
                        dataSql.deleteAdminInvoiceapplication(con, entryDetails, function (delresult) {
                            if (delresult.status == 1) {
                                con.query('UPDATE `patient_procedure` SET '
                                    + 'procedure_status=?, date_modified=?,modified_by=? '
                                    + 'WHERE pp_id = ?'
                                    , [0, current_date, data.current_user_id, data.data.pp_id]
                                    , function (error, rows, fields) {
                                        if (error) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            con.commit(function (err) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong please try again.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.status = 1;
                                                    obj.message = "Plan has been closed successfully.";
                                                    return callback(obj);
                                                }
                                            })
                                        }
                                    })
                            } else {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    return callback(obj);
                                });
                            }
                        })
                    }
                }
            })
    })

}

exports.getPaymentMasterFeeOption = getPaymentMasterFeeOption;
exports.getLateFeeWaiverType = getLateFeeWaiverType;
exports.getFinChargeWaiverType = getFinChargeWaiverType;
exports.getSingleInstallment = getSingleInstallment;
exports.payInstallmentInsert = payInstallmentInsert;
exports.getAllPlansDetails = getAllPlansDetails;
exports.getInvoicePlans = getInvoicePlans;
exports.customerInvoice = customerInvoice;
exports.createInvoice = createInvoice;
exports.getCurrentPlanDetails = getCurrentPlanDetails;
exports.cancelPlan = cancelPlan;
exports.createInvoiceOnFlay = createInvoiceOnFlay;
exports.getOptionToClose = getOptionToClose;
exports.closePlan = closePlan;
exports.pauseTrigger = pauseTrigger;
exports.pauseTriggerUpdate = pauseTriggerUpdate;
exports.getclosePlanDetails = getclosePlanDetails;
exports.payfullPlan = payfullPlan;
exports.newSettementPlan = newSettementPlan;
exports.createInvoiceOnFlayAdmin = createInvoiceOnFlayAdmin;
exports.providerConfirmation = providerConfirmation;
exports.confirmedCancellation = confirmedCancellation;
exports.refundCustomerInvoice = refundCustomerInvoice;
exports.refundProviderInvoice = refundProviderInvoice;