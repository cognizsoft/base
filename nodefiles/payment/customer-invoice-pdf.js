module.exports = (locals, loan_plan_info_arr, logoimg) => {
   let moment = require('moment');
   //console.log('loan_plan_info_arr')
   ///console.log(loan_plan_info_arr)
   //return false;
   var html = '';
   loan_plan_info_arr.forEach(function (val, idx) {
      html = html + '<tr>'
         + '<td>' + val[0].pp_id + '</td>'
         + '<td>' + '$'+parseFloat(val[0].loan_amount).toFixed(2) + '</td>'
         + '<td>$' + val[0].term+ ' Month' + '</td>'
         + '<td>$' + parseFloat(val[0].discounted_interest_rate).toFixed(2)+'%' + '</td>'
         + '<td>$' + parseFloat(val[0].monthly_amount).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body{font-size: 14px; line-height: 20px; font-family: 'Arial', sans-serif; color: #303030;}
			.invoice-box {max-width: 1200px; margin: auto; padding: 2rem; border: 1px solid #eee;}
			.logo{display:inline-block;}
			.logo img{max-width:320px;}	
			.invoicedetail{display:inline-block;}
			.invoicedetail h2{margin:0; color:#145b88;}	
			.client-info{background:#eff7fb; border:1px solid #cfdbe1; border-radius:4px; padding:2rem; margin-top:1.4rem;display:table;width:100%;box-sizing:border-box;}
			.client-infoclient-info-text{display:block; line-height:1.7; position:relative;}
			.clearfix:after{display:table; content:""; clear:both;}
			.client-info-text .client-name{font-size:18px; font-weight:700;}	
			.inline-box{display:inline-block;}
			.align-right{float:right;}
			.text-right{text-align:right;}
			.text-left{text-align:left;}
			.invoice-info{display:flex; flex-direction: row; justify-content:space-between; flex-wrap: nowrap; padding-top:1.2rem;}
			.invoice-bx{border:solid 1px #cfdbe1; border-radius:4px; width:100%;}
			.invoice-bx h2{color:#fff; background-color:#085993; margin:0; padding:0.9rem 1.3rem; border-radius:4px 4px 0 0;}
			table{width:100%;}
			.invoice-bx table{padding: 0.8rem 1.3rem;}
			.invoice-bx table td, .invoice-bx table th{padding:0.25rem; text-align: left !important;}
			.mrgn-auto{margin:0px auto;}
			.invoice-bx table tr td:last-child{text-align:right;}
			.border-top{border-top:1px solid #cfdbe1;}
			.section-title{ margin-top: 50px;border-top: 3px dashed #cfdbe1; color:#085993; text-align:center; padding-top:2rem; padding-bottom:1.4rem; margin-bottom:1.4rem;}
			
			.gross-info h3{color:#fff; background-color:#085186; padding:0.8rem 1.5rem; border-radius:4px 4px 0 0; border-bottom:1px solid #126dae; margin:0; text-align:center; font-size:1.2rem;}
			.gross-info table{padding:0.8rem 0.8rem 1.5rem;}
			.gross-info table.table-bg{color:#fff;}
			.gross-info table tr td{width:50%; font-size:1rem; padding:0.3rem 0.5rem;}
			.gross-info table tr td:first-child{text-align:right;}
			.pymnt-voucher-bx{ width: 100%; display:table;}
			.company-info{align-items: center; display:flex; padding-right:1.5rem;}
			.gross-info-bx{background-color:#085993; border-radius:4px;}
			.table-wt-bg td{font-size:1.2rem;}
			.invoice-bx-container{width:48%;float:left;}
			.invoice-bx-container.invoice-bx-container-expln-amt-due{margin-right: 10px}
			.invoice-bx-container.invoice-bx-container-last-pymnt{margin-left: 10px}
			.loan-info.invoice-bx {margin-top: 23px;}	
			.vocher-total {
			    font-size: 18px;
			    margin-top: 20px;
			    text-align: center;
			    border: 1px solid #085993;
			    padding: 13px;
			    border-radius: 3px;
			    height: 40px;
			}
			.credit-card-details {
				width:58% !important;
				margin-right: 15px;
				float:left;
				display:inline-block;
			}
			.break-before {
    			page-break-before: always;
    		}
			.vocher-total-payment-box {
				width:39%;
				float:right;
			}
			.bg-grey {background:#f9f9f9;border: 1px solid;
			    margin-right: 20px;
			    border-radius: 3px;
			    width:100% !important;}
			.credit-card-details ul {list-style:none;text-align: center;}
			.credit-card-details ul li {display: inline-block; margin-right: 20px;}
			.credit-card-details ul li:last-child {margin-right: 0px;}
			.credit-card-details ul li span {
			    content: '';
			    width: 15px;
			    height: 15px;
			    border: 2px solid;
			    position: relative;
			    display: inline-block;
			    margin-right: 10px;
			    vertical-align: middle;
			}
			.credit-card-details .gross-info-bx {
			    border: 1px solid !important;
			}
			.credit-card-details table {
				padding: 0;
				border-collapse: collapse;
			}
			.logo-container {
			    border-bottom: 1px solid !important;
			}
			.logo-heading {
			  text-transform: uppercase;
			    font-weight: 600;
			    font-size: 10px;
			    text-align: center;
			    margin: 0
			}
			.field-container td {
			    color: #000 !important;
			    text-align: left !important;
			    font-size: 12px !important;
			    border: 1px solid #d7eeff !important;
			    padding-bottom: 20px !important;
			    width: 50%;
			    text-transform: uppercase;
			    font-weight: 600 !important;
			}
			.credit-card-heading {
				text-align: center;
				padding: 5px;
				margin: 0;
				text-transform: uppercase;
			    font-weight: 600 !important;
			    border-bottom: 1px solid;
			}
			span.payable-amount, .additional-credit-amount {
			    font-size: 20px !important;
			    width: 100%;
			    float: left;
			    margin-top: 5px;
			}
			.address_box p {
			    margin-bottom:0;
			    padding-left: 10px;
			    padding-right:10px;
			}
			.gross-info-bx table td {
			    font-size: 13px !important;
			    padding: 4px 0px !important;
			}
			.address-top p {
			    margin: 0;
			}
			.address-top {
			    margin-top: 10px;
			}
        </style>
     </head>
    <body>
      	<div class="invoice-box">

      		<h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>

			<div class="header-row">
				<div class="logo">
					<p>5720 Creedmoor Road, Suite 103</p>
					<p>Raleigh, NC 27612</p>
				</div>
				
				<div class="invoicedetail align-right text-right">
					<h2>Invoice: ${locals.invoice_number}</h2>
					<p><strong>Date:</strong> ${locals.invoice_date}</p>
				</div>

				<div class="client-info">
					<div class="inline-box client-info-text address_box">
						<p>${locals.full_name}</p>
						<div class="address-top">
							<p>${locals.address}</p>
							<p>${locals.address_city} ${locals.address_zip}</p>
						</div>
						<p class="address-phone">${locals.phone}</p>
					</div>
					<div class="inline-box client-info-text align-right text-right">
						<table>
							<tr>
							<td><strong>Invoice Date :</strong> </td>
							<td  class="text-left">${locals.invoice_date}</td>
							</tr>
							<tr>
							<td><strong>Invoice Number :</strong> </td>
							<td  class="text-left">${locals.invoice_number}</td>
							</tr>
							<tr>
							<td><strong>Account Number :</strong></td>
							<td class="text-left">${locals.ac_no}</td>
							</tr>
							<tr>
							<td><strong>Payment Due Date :</strong> </td>
							<td  class="text-left">${locals.pymnt_due_date}</td>
							</tr>
							<tr>
							<td><strong>Amount Due :</strong> </td>
							<td  class="text-left">$${locals.amount_due}</td>
							</tr>
							<tr>
							<td><strong>Late Fee after(${locals.pymnt_due_date}) :</strong> </td>
							<td  class="text-left">$${locals.master_late_fee}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="clearfix">
				<div class="loan-info invoice-bx">
					<h2>Plans</h2>
					<table>
					<tr>
					<th>Plan ID</th>
					<th>Principal Amount</th>
					<th>Payment Term</th>
					<th>APR(%)</th>
					<th>Monthly Payment</th>
					</tr>
					${html}
					</table>
				</div>
			</div>

			<div class="invoice-info clearfix">
				<div class="invoice-bx-container invoice-bx-container-expln-amt-due">
					<div class="loan-info invoice-bx mrgn-auto">
						<h2>Current Month</h2>
						<table>
							<tr>
							<td><strong>Regular Monthly Payment :</strong></td>
							<td>$${locals.regu_mnthly_pymnt}</td>
							</tr>
							<tr>
							<td><strong>Late Fee ${locals.late_fee_after_text}:</strong></td>
							<td>$${locals.late_fee}</td>
							</tr>
							<tr>
							<td><strong>Previous Late Fee:</strong></td>
							<td>$${locals.prev_late_fee}</td>
							</tr>
							<tr>
							<td><strong>Finance Charge :</strong></td>
							<td>$${locals.fin_charge}</td>
							</tr>
							<tr>
							<td><strong>Previous Finance Charge :</strong></td>
							<td>$${locals.prev_fin_charge}</td>
							</tr>
							<tr>
							<td><strong>Total Amount Due :</strong></td>
							<td>$${locals.total_amt_due}</td>
							</tr>
						</table>
					</div>
				</div>

			
				<div class="invoice-bx-container invoice-bx-container-last-pymnt">
					<div class="loan-info invoice-bx">
						<h2>Last Payment</h2>
						<table>
						<tr>
						<td><strong>Regular Monthly Payment :</strong></td>
						<td>$${locals.last_reg_mnthly_pymnt}</td>
						</tr>
						<tr>
						<td><strong>Payment Date :</strong></td>
						<td>${locals.last_payment_date}</td>
						</tr>
						<tr>
						<td><strong>Paid Amount :</strong></td>
						<td>$${locals.last_paid_amount}</td>
						</tr>
						<tr>
						<td><strong>Late Fee :</strong></td>
						<td>$${locals.last_late_fee}</td>
						</tr>
						<tr>
						<td><strong>Finance Charge :</strong></td>
						<td>$${locals.last_fin_ch}</td>
						</tr>
						<tr>
						<td class="border-top"><strong>Total Amount Due :</strong></td>
						<td class="border-top"><strong>$${locals.last_total_amount}</strong></td>
						</tr>
						</table>
					</div>
				</div>
			</div>
		
			<h2 class="section-title break-before">PAYMENT VOUCHER</h2>
			<div class="pymnt-voucher-bx">

				<div class="gross-info credit-card-details">
					<div class="gross-info-bxx bg-greyx">
						<div class="add-card bg-grey">
							<p class="credit-card-heading mb-0 p-5">If Paying By Credit Card Please Fill Out Below</p>  
							<div class="logo-container">
								<p class="logo-heading">Check Card Using for Payment</p>
								<ul class="mb-0 p-10">  
								<li><span class="check"></span>Visa Card</li>
								<li><span class="check"></span>Master Card</li>
								<li><span class="check"></span>American Express</li>
								<li><span class="check"></span>Discover</li>
								</ul>
							</div>
							<div class="field-container">
								<table>
									<tbody>
										<tr>
										<td>Card Number</td>
										<td>3 Digit Security Code</td>
										</tr>
										<tr>
										<td class="pt-5">Signature</td>
										<td class="pt-5">Exp. Date</td>
										</tr>
										<tr>
										<td>Name on Card</td>
										<td>Zip Code</td>
										</tr>
										<tr>
										<td>Statement Date</td>
										<td>Account Number</td>
										</tr>
										<tr>
										<td>3% Additional Charge
										<span class="additional-credit-amount">$${locals.three_per_charge}</span>
										</td>
										<td>Pay This Amount
										<span class="payable-amount">$${locals.total_amt_due}</span>
										</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="gross-info vocher-total-payment-box">
					<div class="gross-info-bx">
						<h3>Amount Due</h3>
						<table class="table-bg">
							<tr>
							<td>Invoice Date : </td>
							<td>${locals.invoice_date}</td>
							</tr>
							<tr>
							<td>Invoice Number : </td>
							<td>${locals.invoice_number}</td>
							</tr>
							<tr>
							<td>Account Number :</td>
							<td>${locals.ac_no}</td>
							</tr>
							<tr>
							<td>Payment Due Date :</td>
							<td>${locals.pymnt_due_date}</td>
							</tr>
							<tr>
							<td>Amount Due :</td>
							<td>$${locals.amount_due}</td>
							</tr>
							<tr>
							<td>Late Fee after(${locals.pymnt_due_date}):</td>
							<td>$${locals.late_fee}</td>
							</tr>
							<tr>
							<td>Previous Late Fee:</td>
							<td>$${locals.prev_late_fee}</td>
							</tr>
							<tr>
							<td>Finance Charge :</td>
							<td>$${locals.fin_charge}</td>
							</tr>
							<tr>
							<td>Previous Finance Charge :</td>
							<td>$${locals.prev_fin_charge}</td>
							</tr>
						</table>
					</div>

					<div class="vocher-total"><strong>Total Amount Enclosed</strong></div>
				</div>

			</div>

		</div>
    </body>
  </html>
      `;
};