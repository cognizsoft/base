var AllPlansDetails = function (con, plans, callback) {
	var plan_count = plans.length;
    var pp_details = [];
    var obj = {};
    //console.log('plans......')
    //console.log(plans)
    plans.forEach(function(data) {
        con.query('SELECT pp_installments.pp_id, '
            + 'pp_installments.pp_installment_id, '
            + 'pp_installments.installment_amt, '
            + 'pp_installments.installment_amt AS installment_amount, '
            + 'pp_installments.amount_paid, '
            + 'pp_installments.additional_amount AS additional_amt, '
            + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
            + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS comparison_date, '
            + 'pp_installments.missed_flag, '
            + 'pp_installments.late_flag, '
            + 'pp_installments.paid_flag, '
            + 'pp_installments.partial_paid, '
            + '(SELECT SUM(installment_amt) FROM pp_installments WHERE paid_flag=1 AND pp_id='+data.pp_id+') AS installment_total, '
            + '(SELECT SUM(amount_paid) FROM pp_installments WHERE partial_paid=1 AND pp_id='+data.pp_id+') AS partial_total, '

            + 'pp_installment_invoice.invoice_id, '

            + 'patient_invoice.payment_amount, '
            + 'patient_invoice.additional_amount, '
            + 'patient_invoice.late_fee_received, '
            + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS payment_date, '
            + 'patient_invoice.fin_charge_amt '

            + 'FROM pp_installments '

            + 'LEFT JOIN pp_installment_invoice '
            + 'ON pp_installment_invoice.pp_id = pp_installments.pp_id '
            + 'AND pp_installment_invoice.pp_installment_id = pp_installments.pp_installment_id '

            + 'LEFT JOIN patient_invoice '
            + 'ON patient_invoice.invoice_id = pp_installment_invoice.invoice_id '
            
            + 'WHERE pp_installments.pp_id = ?'
            ,
            [
                data.pp_id
            ]
            , function (err, plan_details) {
                //console.log('this.sql')
                //console.log(this.sql)

                if (err) {
                    
                    obj.status = 0;
                    obj.message = "Plan details not foundAA.";
                    callback(obj);
                   
                } else {
                    pp_details.push(plan_details);

                    if( 0 === --plan_count ) {
						callback(pp_details); 
					}
                }
            });
    });

    //obj.pp_details = pp_details;
    //return callback(pp_details);
   
    
}



exports.AllPlansDetails = AllPlansDetails;