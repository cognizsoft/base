const capitalize = (str) => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    return splitStr.join(' '); 
}

// this function use for save all data in table.
var updateCustomerProfile = function (con, data, callback) {
    const md5 = require('md5');
    const aes256 = require('aes256');
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    let Orgpassword = data.password;
    let password = md5(data.password);
    var ase256key = 'ramesh';
    data.provider_id = (data.provider_id !== undefined) ? data.provider_id : null;

    //console.log('data-------')
    //console.log(data.password)
    //return false;
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again. 1";
            return callback(obj);
        }

        existCustomer(con, data, function (result_id) {
            data.patient_id = result_id;
            //if (data.patient_id !== undefined && data.patient_id != '') {
            con.query('UPDATE patient SET '
                + 'employment_status=?, '
                + 'employment_type=?, '
                + 'annual_income=?, '
                + 'employer_since=?, '
                + 'employer_name=?, '
                + 'employer_phone=?, '
                + 'employer_email=?, '
                + 'bank_name=?, '
                + 'bank_address=?, '
                + 'rounting_no=?, '
                + 'account_number=?, '
                + 'account_name=?, '
                + 'account_type=?, '
                + 'withdrawal_date=?, '
                + 'peimary_phone=?, '
                + 'alternative_phone=?, '
                + 'email=?, '
                + 'secondary_email=?, '
                + 'status=?, '
                + 'date_modified=?, '
                + 'modified_by=? WHERE patient_id=?',
                [
                    (data.employed == 1) ? 1 : 0,
                    data.employment_type,
                    data.annual_income,
                    data.employed_since,
                    capitalize(data.employer_name),
                    data.employer_phone,
                    (data.employer_email) ? data.employer_email.toLowerCase() : '', 
                    capitalize(data.bank_name),
                    capitalize(data.bank_address),
                    data.rounting_no,
                    data.bank_ac,
                    capitalize(data.account_name),
                    data.account_type,
                    data.withdrawal_date,
                    data.phone_1,
                    data.phone_2,
                    data.email.toLowerCase(),
                    (data.secondary_email) ? data.secondary_email.toLowerCase() : '',
                    data.status,
                    current_date,
                    current_user_id,
                    data.patient_id
                ]
                , function (err, resultmain) {
                    if (err) {
                        //console.log(err)
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again. 2";
                            return callback(obj);
                        });
                    } else {
                        console.log(Orgpassword)
                        if(Orgpassword == '') {
                            var sql = 'UPDATE user SET phone=?, email_id=?, date_modified=?, modified_by=? WHERE patient_id=?';
                            var dataa = [data.phone_1,data.email.toLowerCase(),current_date,current_user_id,data.patient_id];
                        } else {
                            var sql = 'UPDATE user SET phone=?, password=?, email_id=?, date_modified=?, modified_by=? WHERE patient_id=?';
                            var dataa = [data.phone_1,password,data.email.toLowerCase(),current_date,current_user_id,data.patient_id];
                        }
                        con.query(sql, dataa
                            , function (err, resultuser) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again. 3";
                                        return callback(obj);
                                    });
                                } else {
                                    con.query('UPDATE `patient_address` SET status=? WHERE patient_id=?',
                                        [
                                            0,
                                            data.patient_id
                                        ]
                                        , function (err, resultuser) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again. 4";
                                                    return callback(obj);
                                                });
                                            } else {
                                                var values = [];
                                                var billing = data.location.filter(x => x.billing_address == 1);
                                                if (billing.length < 1) {
                                                    values.push([
                                                        data.patient_id,
                                                        data.billing_state,
                                                        capitalize(data.billing_address1),
                                                        capitalize(data.billing_address2),
                                                        capitalize(data.billing_city),
                                                        data.billing_zip_code,
                                                        data.billing_how_long,
                                                        0,
                                                        1,
                                                        0,
                                                        data.billing_phone_no,
                                                        data.status,
                                                        current_date,
                                                        current_date,
                                                        current_user_id,
                                                        current_user_id
                                                    ]);
                                                }

                                                data.location.forEach(function (element, idx) {
                                                    values.push([
                                                        data.patient_id,
                                                        element.state,
                                                        capitalize(element.address1),
                                                        capitalize(element.address2),
                                                        capitalize(element.city),
                                                        element.zip_code,
                                                        element.how_long,
                                                        element.primary_address,
                                                        element.billing_address,
                                                        (element.billing_address) ? 1 : 0,
                                                        element.phone_no,
                                                        data.status,
                                                        current_date,
                                                        current_date,
                                                        current_user_id,
                                                        current_user_id
                                                    ]);
                                                });
                                                //console.log(values)
                                                con.query('INSERT INTO patient_address ('
                                                    + 'patient_id, '
                                                    + 'state_id, '
                                                    + 'address1, '
                                                    + 'address2, '
                                                    + 'City, '
                                                    + 'zip_code, '
                                                    + 'address_time_period, '
                                                    + 'primary_address, '
                                                    + 'billing_address, '
                                                    + 'same_billing_flag, '
                                                    + 'phone_no, '
                                                    + 'status, '
                                                    + 'date_created, '
                                                    + 'date_modified, '
                                                    + 'created_by, '
                                                    + 'modified_by) VALUES ?',
                                                    [
                                                        values
                                                    ]
                                                    , function (err, resultuser) {
                                                        //console.log(this.sql)
                                                        //console.log(err)
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again. 5";
                                                                return callback(obj);
                                                            });
                                                        } else {

                                                            con.commit(function (err) {
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Something wrong please try again. 6";
                                                                        return callback(obj);
                                                                    });
                                                                } else {

                                                                    obj.status = 1;
                                                                    obj.message = "Customer detail updated";
                                                                    return callback(obj);

                                                                }

                                                            });

                                                        }
                                                    });
                                            }

                                        });
                                }
                            });
                    }
                });

        });

    });
}

var existCustomer = function (con, data, callback) {
    if (data.patient_id === undefined) {
        con.query('SELECT patient_id,dob,ssn FROM patient WHERE f_name=? AND l_name=? AND AES_DECRYPT(patient.dob, "ramesh_cogniz")=? AND AES_DECRYPT(patient.ssn, "ramesh_cogniz")=?',
            [
                data.first_name,
                data.last_name,
                data.dob,
                data.ssn,
            ]
            , function (err, existCustoemr) {
                var patient_id = (existCustoemr.length > 0) ? existCustoemr[0].patient_id : '';
                return callback(patient_id)
            });
    } else {
        return callback(data.patient_id)
    }
}

var editCustomerProfileDetails = function (con, customer_id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'patient.patient_id, '
            + 'patient.profile_flag, '
            + 'patient.patient_ac, '
            + 'patient.f_name as first_name, '
            + 'patient.m_name as middle_name, '
            + 'patient.l_name as last_name, '
            + 'patient.alias_name, '
            + 'patient.other_f_name as alias_first_name, '
            + 'patient.other_m_name as alias_middle_name, '
            + 'patient.other_l_name as alias_last_name, '
            + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob, '
            + 'AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn, '
            + 'patient.gender, '
            + 'patient.secondary_email, '
            + 'patient.status, '
            + 'patient.malling_address, '
            + 'patient.peimary_phone, '
            + 'patient.alternative_phone, '
            + 'patient.employment_status as employed, '
            + 'patient.annual_income as annual_income, '
            + 'patient.employer_name as employer_name, '
            + 'patient.employer_since as employed_since, '
            + 'patient.employer_phone as employer_phone, '
            + 'patient.employer_email as employer_email, '
            + 'patient.employment_type as employment_type, '
            + 'patient.bank_name as bank_name, '
            + 'patient.account_number as bank_ac, '
            + 'patient.bank_address as bank_address, '
            + 'patient.rounting_no as rounting_no, '
            + 'patient.account_name as account_name, '
            + 'patient.account_type as account_type, '
            + 'patient.withdrawal_date, '
            + 'patient.alternative_phone as phone_2, '

            + 'user.username, '
            + 'user.password, '
            + 'user.email_id as email, '
            + 'user.phone as phone_1 '

            + 'FROM patient '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'WHERE '
            + 'patient.patient_id=?',
            [
                customer_id
            ]
            , function (err, planDetails) {
                if (err || planDetails.length == 0) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Customer not found.";
                        return callback(obj);
                    });
                } else {
                    //var encrypted = aes256.encrypt(ase256key, plaintext);
                    //planDetails[0].ssn = aes256.decrypt(ase256key, planDetails[0].ssn);
                    //planDetails[0].dob = aes256.decrypt(ase256key, planDetails[0].dob);
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    //let date = require('date-and-time');
                    //let now = new Date(planDetails[0].dob);
                    //planDetails[0].dob = date.format(now, 'DD/MM/YYYY');
                    planDetails[0].dob = planDetails[0].dob.toString();
                    planDetails[0].freeze_override = 0;
                    planDetails[0].freeze_code = '';
                    planDetails[0].amount = '';
                    planDetails[0].procedure_status = 1;

                    con.query('SELECT '
                        + 'patient_address.address1 as address1,patient_address.address2 as address2,'
                        + 'patient_address.City as city,patient_address.county as county,'
                        /*+ 'patient_address.region_id as region,'*/
                        + 'patient_address.address_time_period as how_long,'
                        + 'patient_address.primary_address as primary_address,'
                        + 'patient_address.billing_address as billing_address,'
                        + 'patient_address.same_billing_flag as same_billing_flag,'
                        + 'patient_address.phone_no as phone_no,'
                        + 'patient_address.zip_code as zip_code,'
                        + 'states.state_id as state,'
                        + 'countries.id as country '
                        + 'FROM patient_address '
                        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                        + 'INNER JOIN countries ON countries.id=states.country_id '
                        + 'WHERE '
                        + 'patient_address.status = 1 AND patient_id=?',
                        [
                            planDetails[0].patient_id
                        ]
                        , function (err, planaddressDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Application not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    } else {
                                        planDetails = (planDetails) ? planDetails[0] : '';
                                        //console.log(planDetails.password)
                                        planDetails.password = '';
                                        planDetails.confirm_password = '';
                                        var filterData = planaddressDetails.filter(x => x.same_billing_flag == 0 && x.billing_address == 1);

                                        if (filterData.length > 0) {
                                            planDetails.billing_address1 = filterData[0].address1;
                                            planDetails.billing_address2 = filterData[0].address2;
                                            planDetails.billing_country = filterData[0].country;
                                            planDetails.billing_state = filterData[0].state;
                                            planDetails.billing_region = filterData[0].region;
                                            planDetails.billing_county = filterData[0].county;
                                            planDetails.billing_city = filterData[0].city;
                                            planDetails.billing_zip_code = filterData[0].zip_code;
                                            planDetails.billing_how_long = filterData[0].how_long;
                                            planDetails.billing_phone_no = filterData[0].phone_no;
                                            planaddressDetails.splice(planaddressDetails.findIndex(e => e.same_billing_flag == 0 && e.billing_address == 1), 1);
                                        } else {
                                            planDetails.billing_address1 = "";
                                            planDetails.billing_address2 = "";
                                            planDetails.billing_country = "";
                                            planDetails.billing_state = "";
                                            planDetails.billing_region = "";
                                            planDetails.billing_county = "";
                                            planDetails.billing_city = "";
                                            planDetails.billing_zip_code = "";
                                            planDetails.billing_how_long = "";
                                            planDetails.billing_phone_no = "";
                                            planDetails.sameBilling = false;
                                        }
                                        obj.status = 1;
                                        obj.customerAddress = planaddressDetails;
                                        obj.customerDetails = planDetails
                                        obj.message = "Customer detail success";
                                    }
                                    return callback(obj);
                                });
                            }
                        });
                }
            });



    });
}

var getCustomerActivePlansDetails = function (con, id, callback) {
    //console.log('123232323244')
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'credit_applications.application_id '
            + 'FROM credit_applications '
            + 'INNER JOIN user '
            + 'ON credit_applications.patient_id=user.patient_id '
            + 'WHERE user.user_id=? AND (credit_applications.status = 1 OR credit_applications.status = 6)'
            , [id]
            , function (err, app_id) {
                if (err || app_id.length == 0) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application id not found.";
                        return callback(obj);
                    });
                } else {
                    var appid = app_id[0].application_id;

                    con.query('SELECT credit_applications.application_no, '
                        + 'credit_applications.patient_id, '
                        + 'credit_applications.approve_amount, '
                        + '(credit_applications.remaining_amount+credit_applications.override_amount) as remaining_amount, '
                        + 'credit_applications.expiry_date, '
                        + 'patient.f_name, '
                        + 'patient.m_name, '
                        + 'patient.l_name, '
                        + 'patient.peimary_phone, '
                        + 'patient_address.address1, '
                        + 'patient_address.City,patient_address.zip_code, '
                        + 'states.name '
                        + 'FROM credit_applications '
                        + 'INNER JOIN patient ON credit_applications.patient_id = patient.patient_id '
                        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                        + 'WHERE application_id = ?',
                        [
                            appid
                        ]
                        , function (err, app_details) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Application not found.";
                                    return callback(obj);
                                });
                            } else {
                                obj.application_details = app_details;
                                con.query('SELECT payment_plan.pp_id, '
                                    + 'payment_plan.term as payment_term_month, payment_plan.paid_flag, payment_plan.plan_number,'
                                    + 'DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS date_created, '
                                    + 'payment_plan.amount,payment_plan.discounted_interest_rate,payment_plan.monthly_amount,payment_plan.remaining_amount,payment_plan.installments_count, '
                                    + 'payment_plan.loan_amount, payment_plan.late_count, payment_plan.missed_count,payment_plan.plan_updated, payment_plan.plan_status, payment_plan.note, '
                                    + 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") AS procedure_date, master_data_values.value as status_name, '
                                    + '(SELECT pp_id FROM provider_invoice_detial WHERE pp_id=patient_procedure.pp_id) as invoice_exist, '
                                    + '(SELECT mdv_invoice_status_id FROM provider_invoice INNER JOIN provider_invoice_detial ON provider_invoice_detial.provider_invoice_id=provider_invoice.provider_invoice_id WHERE provider_invoice_detial.pp_id=patient_procedure.pp_id) as provider_invoice_status, '
                                    + '(SELECT DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id = patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=patient_procedure.pp_id ORDER BY patient_invoice.invoice_id DESC LIMIT 1) as last_plan_due '
                                    + 'FROM payment_plan '
                                    + 'LEFT JOIN patient_procedure on patient_procedure.pp_id = payment_plan.pp_id '
                                    + 'INNER JOIN master_data_values on master_data_values.status_id = payment_plan.plan_status '
                                    + 'WHERE payment_plan.application_id = ? AND master_data_values.md_id=?',
                                    [
                                        appid,'Customer Plan Status'
                                    ]
                                    , function (err, plansDetails) {

                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Plan not found.2";
                                                return callback(obj);
                                            });
                                        } else {
                                            obj.application_plans = plansDetails;
                                            obj.amountDetails = [];
                                            con.query('SELECT patient_invoice.invoice_number,patient_invoice.invoice_id,patient_invoice.payment_amount,patient_invoice.previous_blc,patient_invoice.previous_late_fee,patient_invoice.previous_fin_charge,'
                                + 'patient_invoice.additional_amount,patient_invoice.late_fee_received,patient_invoice.fin_charge_amt,patient_invoice.invoice_status,'
                                + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.paid_amount, patient_invoice.paid_amount, '
                                + 'DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date, patient_invoice.on_fly, '
                                + 'master_data_values.value as latepct,master2.value as finpct, invoicestatus.value as invoice_status_name '
                                //+ '(SELECT previous_blc FROM patient_invoice WHERE DATE_FORMAT(due_date, "%m/%d/%Y") = ?) as previous_month_blc '
                                + 'FROM patient_invoice '
                                + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '
                                + 'LEFT JOIN late_fee_waivers ON late_fee_waivers.id=patient_invoice.late_fee_waivers_id '
                                + 'LEFT JOIN master_data_values ON late_fee_waivers.mdv_waiver_type_id=master_data_values.mdv_id '
                                + 'LEFT JOIN late_fee_waivers as late2 ON late2.id=patient_invoice.fin_charge_waiver_id '
                                + 'LEFT JOIN master_data_values as master2 ON late2.mdv_waiver_type_id=master2.mdv_id '
                                + 'WHERE patient_invoice.application_id=? AND invoicestatus.md_id=? GROUP BY invoice_id'
                                                , [appid,'Customer Invoice Status'], function (err, invoiceDetails) {

                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Plan not found.";
                                                            return callback(obj);
                                                        });
                                                    } else {
                                                        con.query('SELECT patient_invoice_details.invoice_id,patient_invoice_details.pp_id, DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date '
                                                            + 'FROM patient_invoice_details '
                                                            + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                                            + 'WHERE patient_invoice_details.application_id=?'
                                                            , [appid], function (err, invoicePlan) {
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Plan not found.";
                                                                        return callback(obj);
                                                                    });
                                                                } else {
                                                                    obj.invoicePlan = invoicePlan;
                                                                    con.commit(function (err) {
                                                                        if (err) {
                                                                            con.rollback(function () {
                                                                                obj.status = 0;
                                                                                obj.message = "Plan not found.";
                                                                                return callback(obj);
                                                                            });
                                                                        }
                                                                        obj.status = 1;
                                                                        obj.invoiceDetails = invoiceDetails;
                                                                        return callback(obj);
                                                                    });
                                                                }
                                                            })

                                                    }
                                                })




                                        }
                                    })
                            }
                        })

                }
            });
    });
    var obj = {};
}

var getActivePlanDetails = function (con, id, callback) {
    //console.log(id)
    //return false;
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'credit_applications.application_id '
            + 'FROM credit_applications '
            + 'INNER JOIN user '
            + 'ON credit_applications.patient_id=user.patient_id '
            + 'WHERE user.user_id=?'
            , [id]
            , function (err, app_id) {
                //console.log(app_id[0].application_id)
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application id not found.";
                        return callback(obj);
                    });
                } else {

                    con.query('SELECT payment_plan.pp_id,payment_plan.interest_rate_id,'
                        + 'payment_plan.application_id,payment_plan.amount,'
                        + 'interest_rate.mdv_interest_rate,interest_rate.mdv_payment_term_month,'
                        + 'credit_applications.application_no,credit_applications.score,credit_applications.amount AS credit_amount,'
                        + 'patient.f_name,patient.m_name,patient.l_name,provider.name,patient.peimary_phone,patient.patient_ac,'
                        + 'patient_address.address1, patient_address.address2, patient_address.City, patient_address.county, patient_address.region_id,'
                        /*+ 'regions.name AS region_name,'*/
                        + 'pp_installments.installment_amt AS monthly_payment,'
                        + 'DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS date_created,master_data_values.value AS interest_rate,master_data_values2.value AS payment_term, provider.member_flag '
                        + 'FROM payment_plan '
                        + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
                        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                        + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id '
                        /*+ 'INNER JOIN regions ON patient_address.region_id=regions.region_id '*/
                        + 'INNER JOIN pp_installments ON payment_plan.pp_id=pp_installments.pp_id '
                        + 'LEFT JOIN interest_rate ON interest_rate.id=payment_plan.interest_rate_id '
                        + 'LEFT JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=interest_rate.mdv_payment_term_month '
                        + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=interest_rate.mdv_interest_rate '
                        + 'LEFT JOIN provider ON provider.provider_id=patient.provider_id '
                        + 'WHERE '
                        + 'credit_applications.application_id=?',
                        [
                            app_id[0].application_id
                        ]
                        , function (err, plan) {
                            console.log(this.sql)
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Payment plan not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.query('SELECT patient_payemnt_id,pp_id,'
                                    + 'payment_amount,late_fee_received,DATE_FORMAT(payment_date, "%m/%d/%Y") AS payment_date,bank_name,'
                                    + 'txn_ref_no '
                                    + 'FROM patient_payment WHERE '
                                    + 'pp_id=?',
                                    [
                                        plan[0].pp_id
                                    ]
                                    , function (err, planDetails) {

                                        //console.log(this.sql)
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Payment plan not found.";
                                                return callback(obj);
                                            });
                                        } else {

                                            con.query('SELECT '
                                                + 'pp_id FROM payment_plan '
                                                + 'WHERE application_id = ?'
                                                ,
                                                [
                                                    app_id[0].application_id
                                                ]
                                                , function (err, pymntPlanId) {

                                                    //console.log(this.sql)
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Payment Details not foundddd.";
                                                            return callback(obj);
                                                        });
                                                    } else {
                                                        con.query('SELECT '
                                                            + 'patient_payment.payment_amount, '
                                                            + 'patient_payment.additional_amount, '
                                                            + 'patient_payment.late_fee_received, '
                                                            + 'DATE_FORMAT(patient_payment.payment_date, "%m/%d/%Y") AS payment_date, '
                                                            + 'patient_payment.bank_name, '
                                                            + 'patient_payment.txn_ref_no, '
                                                            + 'patient_payment.fin_charge_amt, '

                                                            + 'pp_installments.pp_id, '
                                                            + 'pp_installments.pp_installment_id, '
                                                            + 'pp_installments.installment_amt, '
                                                            + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
                                                            + 'pp_installments.due_date AS comparison_date, '
                                                            + 'pp_installments.paid_flag, '
                                                            + 'pp_installments.missed_flag, '
                                                            + 'pp_installments.late_flag '

                                                            + 'FROM pp_installments '
                                                            + 'LEFT JOIN patient_payment '

                                                            + 'ON patient_payment.pp_id=pp_installments.pp_id '
                                                            + 'AND patient_payment.pp_installment_id=pp_installments.pp_installment_id '


                                                            + 'WHERE pp_installments.pp_id = ? '
                                                            + 'ORDER BY pp_installments.pp_installment_id'
                                                            ,
                                                            [
                                                                pymntPlanId[0].pp_id
                                                            ]
                                                            , function (err, pymntPlanDetails) {
                                                                //console.log(this.sql)
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Payment Details not foundddd.";
                                                                        return callback(obj);
                                                                    });
                                                                } else {
                                                                    con.commit(function (err) {
                                                                        if (err) {
                                                                            con.rollback(function () {
                                                                                obj.status = 0;
                                                                                obj.message = "Something wrong please try again.";
                                                                            });
                                                                            return callback(obj);
                                                                        }
                                                                        obj.status = 1;
                                                                        obj.plan = plan;
                                                                        obj.plan_details = planDetails;
                                                                        obj.payment_plan_details = pymntPlanDetails;
                                                                        return callback(obj);
                                                                    });
                                                                }
                                                            });
                                                    }
                                                });
                                        }
                                    });
                            }
                        });
                }
            });
    });
    var obj = {};
}

var payCustomerInstallmentInsert = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    let sql;
    let data_sql;
    //console.log(data.payment_installment_id);
    //return false;
    var lfeee;
    if (new Date(data.due_date) < new Date()) {
        lfeee = data.late_fee_received;
    } else {
        lfeee = null;
    }
    if (!data.payment_installment_id) {

        sql = 'INSERT INTO patient_payment (pp_id, pp_installment_id, payment_amount, additional_amount, late_fee_received, payment_date, bank_name, txn_ref_no, fin_charge_amt, created_by, modified_by, date_created, date_modified) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)';

        data_sql = [data.pp_id, data.pp_installment_id, data.payment_amount, data.additional_amount, lfeee, current_date, capitalize(data.bank_name), data.txn_ref_no, data.fin_charge_amt, data.current_user_id, data.current_user_id, current_date, current_date];

    } else {
        sql = 'UPDATE patient_payment SET additional_amount=?, late_fee_received=?, payment_date=?, bank_name=?, txn_ref_no=?, fin_charge_amt=?, modified_by=?, date_modified=? WHERE patient_payemnt_id=?';

        data_sql = [data.additional_amount, lfeee, current_date, capitalize(data.bank_name), data.txn_ref_no, data.fin_charge_amt, data.current_user_id, current_date, data.payment_installment_id];
    }



    con.query(sql, data_sql
        , function (err, result) {

            if (err) {
                console.log(err);
            } else {
                var obj = {
                    "status": "payment detail added in patient_payment table"
                }
                var Lfee;
                con.query('SELECT value FROM master_data_values WHERE md_id = ?',
                    [
                        'Late Fee'
                    ]
                    , function (err, lateFee) {

                        if (err) {
                            console.log(err)
                        } else {
                            Lfee = lateFee[0].value;

                            var late_fee_flag;
                            if (data.late_fee_recieved > lateFee[0].value || data.late_fee_recieved < lateFee[0].value) {
                                late_fee_flag = 1;
                            } else {
                                late_fee_flag = 0;
                            }

                            let usql;
                            let data_sql;
                            if (!data.payment_installment_id) {
                                usql = 'UPDATE pp_installments SET paid_flag=?, missed_flag=?, late_flag=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';
                                data_usql = [data.payment_in, 0, late_fee_flag, current_date, data.current_user_id, data.pp_id, data.pp_installment_id];
                            } else {
                                usql = 'UPDATE pp_installments SET paid_flag=?, date_modified=?, modified_by=? WHERE pp_id=? AND pp_installment_id=?';
                                data_usql = [data.payment_in, current_date, data.current_user_id, data.pp_id, data.pp_installment_id];
                            }

                            con.query(usql, data_usql
                                , function (err, result) {
                                    console.log(this.sql)
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        var obj = {
                                            "status": "paid flag updated",
                                            "redirectReport": 1
                                        }
                                        //GET ALL INSTALLMENT FOR UPDATE ADDITIONAL AMOUNT

                                        //if(data.payment_in === 1) {

                                        con.query('SELECT '
                                            + 'patient_payment.payment_amount, '
                                            + 'patient_payment.additional_inst_amount, '
                                            + 'patient_payment.patient_payemnt_id, '
                                            + 'patient_payment.additional_amount, '
                                            + 'patient_payment.late_fee_received, '
                                            + 'DATE_FORMAT(patient_payment.payment_date, "%m/%d/%Y") AS payment_date, '
                                            + 'patient_payment.bank_name, '
                                            + 'patient_payment.txn_ref_no, '
                                            + 'patient_payment.fin_charge_amt, '

                                            + 'pp_installments.pp_id, '
                                            + 'pp_installments.pp_installment_id, '
                                            + 'pp_installments.installment_amt, '
                                            + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
                                            + 'pp_installments.due_date AS comparison_date, '
                                            + 'pp_installments.paid_flag, '
                                            + 'pp_installments.missed_flag, '
                                            + 'pp_installments.late_flag '

                                            + 'FROM pp_installments '
                                            + 'LEFT JOIN patient_payment '

                                            + 'ON patient_payment.pp_id=pp_installments.pp_id '
                                            + 'AND patient_payment.pp_installment_id=pp_installments.pp_installment_id '


                                            + 'WHERE pp_installments.pp_id = ? '
                                            + 'ORDER BY pp_installments.pp_installment_id'
                                            ,
                                            [
                                                data.pp_id
                                            ]
                                            , function (err, pymntPlanDetailsOnSinglePage) {
                                                //console.log(this.sql)
                                                if (err) {
                                                    console.log(err)
                                                    obj.status = 0;
                                                    obj.message = "Payment Details not foundddd.";
                                                    return callback(obj);

                                                } else {
                                                    var InstAmt = pymntPlanDetailsOnSinglePage[0].installment_amt;
                                                    pymntPlanDetailsOnSinglePage.reverse();
                                                    var addiAmt = (pymntPlanDetailsOnSinglePage) ? pymntPlanDetailsOnSinglePage.filter(x => x.additional_amount !== 0 && x.additional_amount !== null) : '';

                                                    var addiAmtTotal = 0;
                                                    if (addiAmt) {
                                                        for (var i = 0; i < addiAmt.length; i++) {
                                                            addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
                                                        }
                                                    }

                                                    var arrData = [];
                                                    var arrDataUp = [];
                                                    //console.log(pymntPlanDetailsOnSinglePage)
                                                    for (var k = 0; k < pymntPlanDetailsOnSinglePage.length; k++) {
                                                        //planD.map(function(plan, idx) {
                                                        //console.log(addiAmtTotal);
                                                        if (addiAmtTotal > pymntPlanDetailsOnSinglePage[k].installment_amt) {
                                                            addiAmtTotal = addiAmtTotal - pymntPlanDetailsOnSinglePage[k].installment_amt;
                                                            //console.log('if');
                                                            pymntPlanDetailsOnSinglePage[k].installment_amt = 0;
                                                            //if(pymntPlanDetailsOnSinglePage[k].installment_amt === 0) {
                                                            var addCmt = "additional amount comment";
                                                            if (!pymntPlanDetailsOnSinglePage[k].patient_payemnt_id) {

                                                                arrData.push([
                                                                    pymntPlanDetailsOnSinglePage[k].pp_id,
                                                                    pymntPlanDetailsOnSinglePage[k].pp_installment_id,
                                                                    pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                    InstAmt + pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                    addCmt
                                                                ]);

                                                            } else {
                                                                if (pymntPlanDetailsOnSinglePage[k].additional_inst_amount !== InstAmt) {
                                                                    arrDataUp.push({
                                                                        installment_amt: pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                        additional_amount: pymntPlanDetailsOnSinglePage[k].additional_amount,
                                                                        additional_inst_amount: InstAmt + pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                        comments: addCmt,
                                                                        patient_payemnt_id: pymntPlanDetailsOnSinglePage[k].patient_payemnt_id,
                                                                        pp_id: pymntPlanDetailsOnSinglePage[k].pp_id,
                                                                        pp_installment_id: pymntPlanDetailsOnSinglePage[k].pp_installment_id
                                                                    });
                                                                }
                                                            }
                                                            //}
                                                        } else {
                                                            pymntPlanDetailsOnSinglePage[k].installment_amt = pymntPlanDetailsOnSinglePage[k].installment_amt - addiAmtTotal;
                                                            if (addiAmtTotal !== 0) {
                                                                var addCmt = "additional amount comment";
                                                                if (!pymntPlanDetailsOnSinglePage[k].patient_payemnt_id) {

                                                                    arrData.push([
                                                                        pymntPlanDetailsOnSinglePage[k].pp_id,
                                                                        pymntPlanDetailsOnSinglePage[k].pp_installment_id,
                                                                        0,
                                                                        InstAmt - pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                        addCmt
                                                                    ]);

                                                                } else {
                                                                    if (pymntPlanDetailsOnSinglePage[k].additional_inst_amount !== InstAmt) {
                                                                        arrDataUp.push({
                                                                            installment_amt: 0,
                                                                            additional_amount: pymntPlanDetailsOnSinglePage[k].additional_amount,
                                                                            additional_inst_amount: InstAmt - pymntPlanDetailsOnSinglePage[k].installment_amt,
                                                                            comments: addCmt,
                                                                            patient_payemnt_id: pymntPlanDetailsOnSinglePage[k].patient_payemnt_id,
                                                                            pp_id: pymntPlanDetailsOnSinglePage[k].pp_id,
                                                                            pp_installment_id: pymntPlanDetailsOnSinglePage[k].pp_installment_id
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                            addiAmtTotal = 0;
                                                        }

                                                        //});
                                                    }


                                                    arrData.reverse();
                                                    arrDataUp.reverse();
                                                    //console.log('------------------add')
                                                    //console.log(arrData)
                                                    //console.log('------------------update')
                                                    //console.log(arrDataUp)
                                                    //return false
                                                    ///////////////////////////////
                                                    //////INSERT ADDITIONAL///////
                                                    /////////////////////////////

                                                    /*var paidByAddi = pymntPlanDetailsOnSinglePage.filter(x => x.installment_amt !== InstAmt).reverse();
                                                    for(a=0; a<paidByAddi.length; a++) {
                                                        if(paidByAddi[a].patient_payemnt_id) {
    
                                                            var asql = 'UPDATE patient_payment SET  ';
                                                            var data_asql = '';
    
                                                        } 
                                                    }*/
                                                    if (arrData.length) {

                                                        var asql = 'INSERT INTO patient_payment (pp_id, pp_installment_id, payment_amount, additional_inst_amount, comments) VALUES ?';
                                                        var data_asql = [arrData];

                                                        con.query(asql, data_asql

                                                            , function (err, pymntPlanInsertAddi) {
                                                                //console.log(this.sql)
                                                                if (err) {
                                                                    console.log(err)
                                                                    obj.status = 0;
                                                                    obj.message = "Additional payment not inserted.";
                                                                    //return callback(obj);

                                                                } else {
                                                                    //obj.status = 1;
                                                                    obj.status = 1;
                                                                    obj.payment_plan_details_on_single_page = pymntPlanDetailsOnSinglePage;
                                                                    //return callback(obj);
                                                                }
                                                            });

                                                    }


                                                    if (arrDataUp.length) {
                                                        //var data_asqlu =  [arrDataUp.installment_amt, arrDataUp.additional_inst_amount];
                                                        //var asqlu = '';
                                                        arrDataUp.forEach(function (data) {
                                                            var asqlu = 'UPDATE patient_payment SET payment_amount=?, additional_amount=?, additional_inst_amount=?, comments=? WHERE patient_payemnt_id=? AND pp_id=? AND pp_installment_id=?';
                                                            //})

                                                            con.query(asqlu, [data.installment_amt, data.additional_amount, data.additional_inst_amount, data.comments, data.patient_payemnt_id, data.pp_id, data.pp_installment_id]

                                                                , function (err, pymntPlanUpdateAddi) {
                                                                    //console.log(this.sql)
                                                                    if (err) {
                                                                        console.log(err)
                                                                        obj.status = 0;
                                                                        obj.message = "Additional payment not updated.";
                                                                        //return callback(obj);

                                                                    } else {
                                                                        //obj.status = 1;
                                                                        obj.status = 1;
                                                                        obj.payment_plan_details_on_single_page = pymntPlanDetailsOnSinglePage;
                                                                    }
                                                                });
                                                        })
                                                    }
                                                    //pymntPlanDetailsOnSinglePage.reverse();
                                                    //console.log(paidByAddi);
                                                    return callback(obj);

                                                }
                                            });

                                        // }

                                        //////////////
                                        //return callback(obj);
                                    }
                                });
                        }
                    });
                //return false;
                //console.log('dfdf'+Lfee);


            }
        })

}

exports.getActivePlanDetails = getActivePlanDetails;
exports.payCustomerInstallmentInsert = payCustomerInstallmentInsert;
exports.getCustomerActivePlansDetails = getCustomerActivePlansDetails;
exports.editCustomerProfileDetails = editCustomerProfileDetails;
exports.updateCustomerProfile = updateCustomerProfile;
exports.existCustomer = existCustomer;
