/*
* Title: masterValue
* Descrpation :- This module blong to admin reports
* Date :-  July 18, 2019
* Author :- Ramesh
*/
module.exports = function (app, jwtMW) {
	let date = require('date-and-time');
	let moment = require('moment');
	// provider account payable
	app.post('/api/customer-credit-charges', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.customerCreditChargeFilter(con, req.body, function (result) {
			res.send(result);
		});
	});

	// download pdf reports
	app.post('/api/customer-credit-charges-pdf', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.customerCreditChargeFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/customerCreditChargesDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'customer-credit-chagrges.pdf';
				pdf.create(pdfTemplate(req.body, result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	// download xls reports
	app.post('/api/customer-credit-charges-xls', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.customerCreditChargeFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Credit Charges'); //creating worksheet

				let dataFilter = req.body;
				let fileName = '';
				if (dataFilter.filter_type == 1) {
					moment.updateLocale('en', {
						week: {
							dow: 1,
							doy: 1
						}
					});

					let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
					let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
					fileName = 'Weekly Credit Charges Report (' + startOfWeek + ' - ' + endOfWeek + ')';
				} else if (dataFilter.filter_type == 2) {
					let monthName = moment().month(dataFilter.month - 1).format("MMM");
					fileName = 'Monthly Credit Charges Report (' + monthName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 3) {
					let queterName = ''
					if (dataFilter.quarter == 1) {
						queterName = 'Q1';
					} else if (dataFilter.quarter == 2) {
						queterName = 'Q2';
					} else if (dataFilter.quarter == 3) {
						queterName = 'Q3';
					} else if (dataFilter.quarter == 4) {
						queterName = 'Q4';
					}
					fileName = 'Quarterly Credit Charges Report (' + queterName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 4) {
					fileName = 'Yearly Credit Charges Report (' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 5) {
					let currentDate = moment().format('MM/DD/YYYY');
					fileName = 'Year to Date Credit Charges Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
				} else if (dataFilter.filter_type == 6) {
					let loanStart = moment(dataFilter.loan_start_date).format('MM/DD/YYYY');
					let loanEnd = moment(dataFilter.loan_end_date).format('MM/DD/YYYY');
					fileName = 'By Date Credit Charges Report (' + loanStart + '-' + loanEnd + ')';
				}

				worksheet.mergeCells('A1:F3');

				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Invoice No";
				worksheet.getCell('B5').value = "Inv Due Date";
				worksheet.getCell('C5').value = "Customer Name";
				worksheet.getCell('D5').value = "Invoice Amt";
				worksheet.getCell('E5').value = "CC Charge Amt";
				worksheet.getCell('F5').value = "Total Amt Rcvd";
				
				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'id', width: 10 },
					{ key: 'name', width: 30 },
					{ key: 'location', width: 30 },
					{ key: 'phone', width: 30 },
					{ key: 'dob', width: 20, numFmt: '#.##' },
					{ key: 'HPS Discount', width: 20, },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {


					let refundAmt = (element.iou_flag != 1) ? (element.refund_recived != null) ? parseFloat(element.refund_recived) : 0 : parseFloat(element.refund_due)

					worksheet.getCell('A' + count).value = element.invoice_number;
					worksheet.getCell('B' + count).value = element.due_date;
					worksheet.getCell('C' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('D' + count).value = element.invoice_amt;
					worksheet.getCell('E' + count).value = element.credit_charge_amt;
					worksheet.getCell('F' + count).value = element.total_paid;

					worksheet.getCell('D' + count).numFmt = '0.00';
					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('F' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				worksheet.getCell('A' + count).font = {
					bold: true
				};
				// total for actual amount
				const sumRange = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0.00';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumRange})` };
				worksheet.getCell('E' + count).font = {
					bold: true
				};
				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'provider-payable.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});

			}
		});
	});

	// provider account payable
	app.post('/api/admin-provider-payable-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerAccountPayableFilter(con, req.body, function (result) {
			res.send(result);
		});
	});

	// download pdf reports
	app.post('/api/provider-payable-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerAccountPayableFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/providerPayableDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'provider-invoice.pdf';
				pdf.create(pdfTemplate(req.body, result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	// download xls reports
	app.post('/api/provider-payable-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerAccountPayableFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Provider Invoice'); //creating worksheet

				let dataFilter = req.body;
				let fileName = '';
				if (dataFilter.filter_type == 1) {
					moment.updateLocale('en', {
						week: {
							dow: 1,
							doy: 1
						}
					});

					let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
					let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
					fileName = 'Weekly Accounts Payable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
				} else if (dataFilter.filter_type == 2) {
					let monthName = moment().month(dataFilter.month - 1).format("MMM");
					fileName = 'Monthly Accounts Payable Report (' + monthName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 3) {
					let queterName = ''
					if (dataFilter.quarter == 1) {
						queterName = 'Q1';
					} else if (dataFilter.quarter == 2) {
						queterName = 'Q2';
					} else if (dataFilter.quarter == 3) {
						queterName = 'Q3';
					} else if (dataFilter.quarter == 4) {
						queterName = 'Q4';
					}
					fileName = 'Quarterly Accounts Payable Report (' + queterName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 4) {
					fileName = 'Yearly Accounts Payable Report (' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 5) {
					let currentDate = moment().format('MM/DD/YYYY');
					fileName = 'Year to Date Accounts Payable Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
				} else if (dataFilter.filter_type == 6) {
					let loanStart = moment(dataFilter.loan_start_date).format('MM/DD/YYYY');
					let loanEnd = moment(dataFilter.loan_end_date).format('MM/DD/YYYY');
					fileName = 'By Date Accounts Payable Report (' + loanStart + '-' + loanEnd + ')';
				}

				worksheet.mergeCells('A1:K3');
				console.log(fileName)
				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Loan No";
				worksheet.getCell('B5').value = "Provider Name";
				worksheet.getCell('C5').value = "Customer Name";
				worksheet.getCell('D5').value = "Loan Date";
				worksheet.getCell('E5').value = "Total Amt Invoiced";
				worksheet.getCell('F5').value = "Total Discount Amt";
				worksheet.getCell('G5').value = "Total Amt payable";
				worksheet.getCell('H5').value = "Total Amt paid";
				worksheet.getCell('I5').value = "Balance Amt Payable";
				worksheet.getCell('J5').value = "Refund Amt";
				worksheet.getCell('K5').value = "Status";

				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'Loan No', width: 10 },
					{ key: 'Provider Name', width: 15 },
					{ key: 'Customer Name', width: 15 },
					{ key: 'Loan Date', width: 15 },
					{ key: 'Total Amt Invoiced', width: 20, numFmt: '#.##' },
					{ key: 'Total Discount Amt', width: 20, },
					{ key: 'Total Amt payable', width: 20 },
					{ key: 'Total Amt paid', width: 15 },
					{ key: 'Balance Amt Payable', width: 20 },
					{ key: 'Refund Amt', width: 15 },
					{ key: 'Status', width: 15 },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {


					element.procedure_amt = (element.procedure_amt) ? element.procedure_amt : 0;
					element.hps_discount_amt = (element.hps_discount_amt) ? element.hps_discount_amt : 0;
					let refundAmt = (element.iou_flag != 1) ? (element.refund_recived != null) ? parseFloat(element.refund_recived) : 0 : parseFloat(element.refund_due)
					let paySingle = (element.procedure_amt) ? (parseFloat(element.procedure_amt) - parseFloat(element.hps_discount_amt)) : 0;
					let checkAmt = (element.mdv_invoice_status_id == 4) ? (parseFloat(element.procedure_amt) - parseFloat(element.hps_discount_amt)) : 0;
					let invStatus = (element.inv_status != null) ? element.inv_status : '-';
					let deuAmt = (element.mdv_invoice_status_id != 4) ? (parseFloat(element.procedure_amt) - parseFloat(element.hps_discount_amt)) : 0;

					worksheet.getCell('A' + count).value = element.plan_number;
					worksheet.getCell('B' + count).value = element.name;
					worksheet.getCell('C' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('D' + count).value = element.procedure_date;
					worksheet.getCell('E' + count).value = element.procedure_amt;
					worksheet.getCell('F' + count).value = element.hps_discount_amt;
					worksheet.getCell('G' + count).value = paySingle;
					worksheet.getCell('H' + count).value = checkAmt;
					worksheet.getCell('I' + count).value = deuAmt;
					worksheet.getCell('J' + count).value = refundAmt;
					worksheet.getCell('K' + count).value = invStatus;

					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('F' + count).numFmt = '0.00';
					worksheet.getCell('G' + count).numFmt = '0.00';
					worksheet.getCell('H' + count).numFmt = '0.00';
					worksheet.getCell('I' + count).numFmt = '0.00';
					worksheet.getCell('J' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				worksheet.getCell('A' + count).font = {
					bold: true
				};
				// total for actual amount
				const sumRange = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0.00';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumRange})` };
				worksheet.getCell('E' + count).font = {
					bold: true
				};
				// total for hps discount amount
				const sumRangeDiscount = `${'F2'}:${'F' + (count - 1)}`;
				worksheet.getCell('F' + count).numFmt = '0.00';
				worksheet.getCell('F' + count).value = { formula: `SUM(${sumRangeDiscount})` };
				worksheet.getCell('F' + count).font = {
					bold: true
				};
				// total for checked amount
				const sumRangeConfirm = `${'G2'}:${'G' + (count - 1)}`;
				worksheet.getCell('G' + count).numFmt = '0.00';
				worksheet.getCell('G' + count).value = { formula: `SUM(${sumRangeConfirm})` };
				worksheet.getCell('G' + count).font = {
					bold: true
				};
				// total for checked amount
				const sumRefundAmt = `${'H2'}:${'H' + (count - 1)}`;
				worksheet.getCell('H' + count).numFmt = '0.00';
				worksheet.getCell('H' + count).value = { formula: `SUM(${sumRefundAmt})` };
				worksheet.getCell('H' + count).font = {
					bold: true
				};

				const sumRefundAmt1 = `${'I2'}:${'I' + (count - 1)}`;
				worksheet.getCell('I' + count).numFmt = '0.00';
				worksheet.getCell('I' + count).value = { formula: `SUM(${sumRefundAmt1})` };
				worksheet.getCell('I' + count).font = {
					bold: true
				};

				const sumRefundAmt2 = `${'J2'}:${'J' + (count - 1)}`;
				worksheet.getCell('J' + count).numFmt = '0.00';
				worksheet.getCell('J' + count).value = { formula: `SUM(${sumRefundAmt2})` };
				worksheet.getCell('J' + count).font = {
					bold: true
				};

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'provider-payable.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});

			}
		});
	});

	app.post('/api/customer-support-report-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./support/supportReports.js');
		dataSql.customerSupportReportFilterData(con, req.body, function (result) {
			if (result.status == 1) {


				//Grouping//
				var tickArr = result.result.reduce((acc, obj) => {
					const key1 = obj['date_created'];
					//const key2 = obj['provider_id'];

					const key = key1

					var opn = 0
					var clo = 0
					var pen = 0
					var hol = 0
					var neW = 0
					if (obj['status'] == 0) {
						++clo
					}
					if (obj['status'] == 1 || obj['status'] == 2) {
						++pen
					}
					if (obj['status'] == 2) {
						++hol
					}
					if (obj['commented_by'] !== 1) {
						++neW
					}

					if (!acc[key]) {
						acc[key] = {
							ticket_opened: clo + pen,
							ticket_closed: clo,
							ticket_pending: pen,
							ticket_hold: hol,
							ticket_new: neW,
							provider_id: obj['customer_id'],
							//provider_name:obj['name'],
							date_created: obj['date_created'],
						};
					} else {
						acc[key].ticket_opened += clo + pen;
						acc[key].ticket_closed += clo;
						acc[key].ticket_pending += pen;
						acc[key].ticket_hold += hol;
						acc[key].ticket_new += neW;
						provider_id: obj['customer_id'];
						//provider_name:obj['name'];
						date_created: obj['date_created'];
					}

					return acc;
				}, []);

				var ticket_array = new Array();
				for (var items in tickArr) {
					ticket_array.push(tickArr[items]);
				}

				//End Grouping//

				const jsonData = ticket_array;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Customer Support Report'); //creating worksheet
				//  WorkSheet Header
				worksheet.columns = [
					{ header: 'Month', key: 'Month', width: 30 },
					{ header: 'New Cases', key: 'New Cases', width: 10 },
					{ header: 'Opened', key: 'Opened', width: 20 },
					{ header: 'Closed', key: 'Closed', width: 30 },
					{ header: 'Hold', key: 'Hold', width: 30 },
					{ header: 'Pending', key: 'Pending', width: 30 },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 2;
				jsonData.forEach(function (element, idx) {
					var appstatus = '';

					worksheet.getCell('A' + count).value = element.date_created;
					worksheet.getCell('B' + count).value = element.ticket_new;
					worksheet.getCell('C' + count).value = element.ticket_opened;
					worksheet.getCell('D' + count).value = element.ticket_closed;
					worksheet.getCell('E' + count).value = element.ticket_hold;
					worksheet.getCell('F' + count).value = element.ticket_pending;

					worksheet.getCell('B' + count).numFmt = '0';
					worksheet.getCell('C' + count).numFmt = '0';
					worksheet.getCell('D' + count).numFmt = '0';
					worksheet.getCell('E' + count).numFmt = '0';
					worksheet.getCell('F' + count).numFmt = '0';

					count++;
				});
				// set header details
				['A1', 'B1', 'C1', 'D1', 'E1', 'F1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':A' + count);
				worksheet.getCell('A' + count).value = 'Total';
				// total sumNew
				const sumNew = `${'B2'}:${'B' + (count - 1)}`;
				worksheet.getCell('B' + count).numFmt = '0';
				worksheet.getCell('B' + count).value = { formula: `SUM(${sumNew})` };
				// total sumOpened
				const sumOpened = `${'C2'}:${'C' + (count - 1)}`;
				worksheet.getCell('C' + count).numFmt = '0';
				worksheet.getCell('C' + count).value = { formula: `SUM(${sumOpened})` };
				// total sumResolved
				const sumResolved = `${'D2'}:${'D' + (count - 1)}`;
				worksheet.getCell('D' + count).numFmt = '0';
				worksheet.getCell('D' + count).value = { formula: `SUM(${sumResolved})` };
				// total sumHold
				const sumHold = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumHold})` };
				// total sumPending
				const sumPending = `${'F2'}:${'F' + (count - 1)}`;
				worksheet.getCell('F' + count).numFmt = '0';
				worksheet.getCell('F' + count).value = { formula: `SUM(${sumPending})` };
				// total sumFinCharge

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'customer-support-report.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});

	app.post('/api/customer-support-report-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./support/supportReports.js');
		dataSql.customerSupportReportFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./support/customerSupportReportPdf');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'custmer-support-report.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	app.post('/api/customer-support-report-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./support/supportReports.js');
		dataSql.customerSupportReportFilterData(con, req.body, function (result) {
			res.send(result);
		});
	});

	app.get('/api/customer-support-report/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./support/supportReports.js');
		dataSql.customerSupportReportData(con, function (result) {
			res.send(result);
		});
	})

	app.post('/api/provider-support-report-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./support/supportReports.js');
		dataSql.supportReportFilterData(con, req.body, function (result) {
			if (result.status == 1) {


				//Grouping//
				var tickArr = result.result.reduce((acc, obj) => {
					const key1 = obj['date_created'];
					const key2 = obj['provider_id'];

					const key = key1 + '-' + key2

					var opn = 0
					var clo = 0
					var pen = 0
					var hol = 0
					if (obj['status'] == 0) {
						++clo
					}
					if (obj['status'] == 1 || obj['status'] == 2) {
						++pen
					}
					if (obj['status'] == 2) {
						++hol
					}

					if (!acc[key]) {
						acc[key] = {
							ticket_opened: clo + pen,
							ticket_closed: clo,
							ticket_pending: pen,
							ticket_hold: hol,
							provider_id: obj['provider_id'],
							provider_name: obj['name'],
							date_created: obj['date_created'],
						};
					} else {
						acc[key].ticket_opened += clo + pen;
						acc[key].ticket_closed += clo;
						acc[key].ticket_pending += pen;
						acc[key].ticket_hold += hol;
						provider_id: obj['provider_id'];
						provider_name: obj['name'];
						date_created: obj['date_created'];
					}

					return acc;
				}, []);

				var ticket_array = new Array();
				for (var items in tickArr) {
					ticket_array.push(tickArr[items]);
				}

				//End Grouping//

				const jsonData = ticket_array;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Provider Support Report'); //creating worksheet
				//  WorkSheet Header
				worksheet.columns = [
					{ header: 'Month', key: 'Month', width: 30 },
					{ header: 'Provider', key: 'Provider', width: 10 },
					{ header: 'Opened', key: 'Opened', width: 20 },
					{ header: 'Closed', key: 'Closed', width: 30 },
					{ header: 'Hold', key: 'Hold', width: 30 },
					{ header: 'Pending', key: 'Pending', width: 30 },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 2;
				jsonData.forEach(function (element, idx) {
					var appstatus = '';

					worksheet.getCell('A' + count).value = element.date_created;
					worksheet.getCell('B' + count).value = element.provider_name;
					worksheet.getCell('C' + count).value = element.ticket_opened;
					worksheet.getCell('D' + count).value = element.ticket_closed;
					worksheet.getCell('E' + count).value = element.ticket_hold;
					worksheet.getCell('F' + count).value = element.ticket_pending;

					worksheet.getCell('C' + count).numFmt = '0';
					worksheet.getCell('D' + count).numFmt = '0';
					worksheet.getCell('E' + count).numFmt = '0';
					worksheet.getCell('F' + count).numFmt = '0';

					count++;
				});
				// set header details
				['A1', 'B1', 'C1', 'D1', 'E1', 'F1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':B' + count);
				worksheet.getCell('A' + count).value = 'Total';
				// total sumOpened
				const sumOpened = `${'C2'}:${'C' + (count - 1)}`;
				worksheet.getCell('C' + count).numFmt = '0';
				worksheet.getCell('C' + count).value = { formula: `SUM(${sumOpened})` };
				// total sumResolved
				const sumResolved = `${'D2'}:${'D' + (count - 1)}`;
				worksheet.getCell('D' + count).numFmt = '0';
				worksheet.getCell('D' + count).value = { formula: `SUM(${sumResolved})` };
				// total sumHold
				const sumHold = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumHold})` };
				// total sumPending
				const sumPending = `${'F2'}:${'F' + (count - 1)}`;
				worksheet.getCell('F' + count).numFmt = '0';
				worksheet.getCell('F' + count).value = { formula: `SUM(${sumPending})` };
				// total sumFinCharge

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'provider-support-report.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});

	app.post('/api/provider-support-report-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./support/supportReports.js');
		dataSql.supportReportFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./support/providerSupportReportPdf');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'provider-support-report.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	app.post('/api/provider-support-report-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./support/supportReports.js');
		dataSql.supportReportFilterData(con, req.body, function (result) {
			res.send(result);
		});
	});
	// loan rport start here
	app.get('/api/provider-support-report/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./support/supportReports.js');
		dataSql.supportReportData(con, function (result) {
			res.send(result);
		});
	})

	app.post('/api/admin-account-receivables-report-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/accountReceivables.js');
		dataSql.accountReceivablesFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Accounts Receivable'); //creating worksheet
				
				let dataFilter = req.body;
				let fileName = '';
				if (dataFilter.filter_type == 1) {
					moment.updateLocale('en', {
						week: {
							dow: 1,
							doy: 1
						}
					});

					let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
					let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
					fileName = 'Weekly Accounts Receivable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
				} else if (dataFilter.filter_type == 2) {
					let monthName = moment().month(dataFilter.month - 1).format("MMM");
					fileName = 'Monthly Accounts Receivable Report (' + monthName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 3) {
					let queterName = ''
					if (dataFilter.quarter == 1) {
						queterName = 'Q1';
					} else if (dataFilter.quarter == 2) {
						queterName = 'Q2';
					} else if (dataFilter.quarter == 3) {
						queterName = 'Q3';
					} else if (dataFilter.quarter == 4) {
						queterName = 'Q4';
					}
					fileName = 'Quarterly Accounts Receivable Report (' + queterName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 4) {
					fileName = 'Yearly Accounts Receivable Report (' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 5) {
					let currentDate = moment().format('MM/DD/YYYY');
					fileName = 'Year to Date Accounts Receivable Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
				} else if (dataFilter.filter_type == 6) {
					let loanStart = moment(dataFilter.start_date).format('MM/DD/YYYY');
					let loanEnd = moment(dataFilter.end_date).format('MM/DD/YYYY');
					fileName = 'By Date Accounts Receivable Report (' + loanStart + '-' + loanEnd + ')';
				}

				worksheet.mergeCells('A1:M3');
				console.log(fileName)
				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Loan No";
				worksheet.getCell('B5').value = "Provider Name";
				worksheet.getCell('C5').value = "Customer Name";
				worksheet.getCell('D5').value = "Loan Date";
				worksheet.getCell('E5').value = "Prin Amt";
				worksheet.getCell('F5').value = "Prin Due";
				worksheet.getCell('G5').value = "Prin Bal";
				worksheet.getCell('H5').value = "Int Due";
				worksheet.getCell('I5').value = "Late Fee";
				worksheet.getCell('J5').value = "Fin Charge";
				worksheet.getCell('K5').value = "Total Due";
				worksheet.getCell('L5').value = "Amt Rcvd";
				worksheet.getCell('M5').value = "Refund Amt";

				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'Loan No', width: 10 },
					{ key: 'Loan No', width: 10 },
					{ key: 'Customer Name', width: 30 },
					{ key: 'Loan Date', width: 30 },
					{ key: 'Prin Amt', width: 20 },
					{ key: 'Prin Due', width: 30 },
					{ key: 'Prin Bal', width: 30 },
					{ key: 'Int Due', width: 30 },
					{ key: 'Late Fee', width: 20, },
					{ key: 'Fin Charge', width: 20 },
					{ key: 'Total Due', width: 15 },
					{ key: 'Amt Rcvd', width: 15 },
					{ key: 'Refund Amt', width: 15 },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {
					var appstatus = '';

					element.main_amount = (element.main_amount) ? element.main_amount : 0;
					element.remaining_amount = (element.remaining_amount) ? element.remaining_amount : 0;
					element.interest_due = (element.interest_due) ? element.interest_due : 0;
					element.late_fee = (element.late_fee) ? element.late_fee : 0;
					element.fin_fee = (element.fin_fee) ? element.fin_fee : 0;
					element.inv_interest = (element.inv_interest) ? element.inv_interest : 0;
					element.refund_amt = (element.refund_amt) ? element.refund_amt : 0

					var prin_bal;
					if ((element.main_amount - element.remaining_amount) < element.main_amount && (element.main_amount - element.remaining_amount) !== 0) {
						prin_bal = element.main_amount - element.remaining_amount;
					} else if ((element.main_amount - element.remaining_amount) == 0) {
						prin_bal = element.main_amount;
					} else {
						prin_bal = 0;
					}

					worksheet.getCell('A' + count).value = element.plan_number;
					worksheet.getCell('B' + count).value = element.name;
					worksheet.getCell('C' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('D' + count).value = element.loan_date;
					worksheet.getCell('E' + count).value = element.main_amount;
					worksheet.getCell('F' + count).value = element.remaining_amount;
					worksheet.getCell('G' + count).value = prin_bal;
					worksheet.getCell('H' + count).value = element.interest_due;
					worksheet.getCell('I' + count).value = element.late_fee;
					worksheet.getCell('J' + count).value = element.fin_fee;
					worksheet.getCell('K' + count).value = element.remaining_amount + element.interest_due;
					worksheet.getCell('L' + count).value = (element.main_amount - element.remaining_amount) + element.inv_interest + element.late_fee + element.fin_fee;
					worksheet.getCell('M' + count).value = element.refund_amt;

					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('F' + count).numFmt = '0.00';
					worksheet.getCell('G' + count).numFmt = '0.00';
					worksheet.getCell('H' + count).numFmt = '0.00';
					worksheet.getCell('I' + count).numFmt = '0.00';
					worksheet.getCell('J' + count).numFmt = '0.00';
					worksheet.getCell('K' + count).numFmt = '0.00';
					worksheet.getCell('L' + count).numFmt = '0.00';
					worksheet.getCell('M' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				worksheet.getCell('A' + count).font = {
					bold: true
				};
				// total sumPrinAmt
				const sumPrinAmt = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0.00';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumPrinAmt})` };
				worksheet.getCell('E' + count).font = {
					bold: true
				};
				// total sumPrinDue
				const sumPrinDue = `${'F2'}:${'F' + (count - 1)}`;
				worksheet.getCell('F' + count).numFmt = '0.00';
				worksheet.getCell('F' + count).value = { formula: `SUM(${sumPrinDue})` };
				worksheet.getCell('F' + count).font = {
					bold: true
				};
				// total sumPrinBal
				const sumPrinBal = `${'G2'}:${'G' + (count - 1)}`;
				worksheet.getCell('G' + count).numFmt = '0.00';
				worksheet.getCell('G' + count).value = { formula: `SUM(${sumPrinBal})` };
				worksheet.getCell('G' + count).font = {
					bold: true
				};
				// total sumIntDue
				const sumIntDue = `${'H2'}:${'H' + (count - 1)}`;
				worksheet.getCell('H' + count).numFmt = '0.00';
				worksheet.getCell('H' + count).value = { formula: `SUM(${sumIntDue})` };
				worksheet.getCell('H' + count).font = {
					bold: true
				};
				// total sumFinCharge
				const sumLateCharge = `${'I2'}:${'I' + (count - 1)}`;
				worksheet.getCell('I' + count).numFmt = '0.00';
				worksheet.getCell('I' + count).value = { formula: `SUM(${sumLateCharge})` };
				worksheet.getCell('I' + count).font = {
					bold: true
				};
				// total sumLateCharge
				const sumFinCharge = `${'J2'}:${'J' + (count - 1)}`;
				worksheet.getCell('J' + count).numFmt = '0.00';
				worksheet.getCell('J' + count).value = { formula: `SUM(${sumFinCharge})` };
				worksheet.getCell('J' + count).font = {
					bold: true
				};
				// total sumTotalDue
				const sumTotalDue = `${'K2'}:${'K' + (count - 1)}`;
				worksheet.getCell('K' + count).numFmt = '0.00';
				worksheet.getCell('K' + count).value = { formula: `SUM(${sumTotalDue})` };
				worksheet.getCell('K' + count).font = {
					bold: true
				};
				// total sumAmtRcvd
				const sumAmtRcvd = `${'L2'}:${'L' + (count - 1)}`;
				worksheet.getCell('L' + count).numFmt = '0.00';
				worksheet.getCell('L' + count).value = { formula: `SUM(${sumAmtRcvd})` };
				worksheet.getCell('L' + count).font = {
					bold: true
				};
				// total sumAmtRefund
				const sumAmtRefund = `${'M2'}:${'M' + (count - 1)}`;
				worksheet.getCell('M' + count).numFmt = '0.00';
				worksheet.getCell('M' + count).value = { formula: `SUM(${sumAmtRefund})` };
				worksheet.getCell('M' + count).font = {
					bold: true
				};

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'account-receivables.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});

	app.post('/api/admin-account-receivables-report-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/accountReceivables.js');
		dataSql.accountReceivablesFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/accountReceivablesPdf');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'account-receivables.pdf';
				pdf.create(pdfTemplate(req.body, result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});
	// filter option
	app.post('/api/admin-account-receivables-report-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/accountReceivables.js');
		dataSql.accountReceivablesFilterData(con, req.body, function (result) {
			res.send(result);
		});
	});
	// loan rport start here
	app.get('/api/admin-account-receivables-report/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/accountReceivables.js');
		dataSql.accountReceivablesData(con, function (result) {
			res.send(result);
		});
	})

	// loan rport start here
	app.get('/api/admin-report-list/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/loanReports.js');
		dataSql.loanReportsData(con, function (result) {
			res.send(result);
		});
	})

	// filter option
	app.post('/api/loan-report-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/loanReports.js');
		dataSql.loanReportsFilterData(con, req.body, function (result) {
			res.send(result);
		});
	});
	// download reports
	app.post('/api/loan-report-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/loanReports.js');
		dataSql.loanReportsFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/loanDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'loan-report.pdf';
				pdf.create(pdfTemplate(req.body, result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});
	// download xls for loan report
	// download xls reports
	app.post('/api/loan-reposrt-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/loanReports.js');
		dataSql.loanReportsFilterData(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Loan Reports'); //creating worksheet

				let dataFilter = req.body;
				let fileName = '';
				if (dataFilter.filter_type == 1) {
					moment.updateLocale('en', {
						week: {
							dow: 1,
							doy: 1
						}
					});

					let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
					let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
					fileName = 'Weekly Loan Report (' + startOfWeek + ' - ' + endOfWeek + ')';
				} else if (dataFilter.filter_type == 2) {
					let monthName = moment().month(dataFilter.month - 1).format("MMM");
					fileName = 'Monthly Loan Report (' + monthName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 3) {
					let queterName = ''
					if (dataFilter.quarter == 1) {
						queterName = 'Q1';
					} else if (dataFilter.quarter == 2) {
						queterName = 'Q2';
					} else if (dataFilter.quarter == 3) {
						queterName = 'Q3';
					} else if (dataFilter.quarter == 4) {
						queterName = 'Q4';
					}
					fileName = 'Quarterly Loan Report (' + queterName + '-' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 4) {
					fileName = 'Yearly Loan Report (' + dataFilter.year + ')';
				} else if (dataFilter.filter_type == 5) {
					let currentDate = moment().format('MM/DD/YYYY');
					fileName = 'Year to Date Loan Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
				} else if (dataFilter.filter_type == 6) {
					let loanStart = moment(dataFilter.loan_start_date).format('MM/DD/YYYY');
					let loanEnd = moment(dataFilter.loan_end_date).format('MM/DD/YYYY');
					fileName = 'By Date Loan Report (' + loanStart + '-' + loanEnd + ')';
				}

				worksheet.mergeCells('A1:O3');

				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Loan No";
				worksheet.getCell('B5').value = "Provider Name";
				worksheet.getCell('C5').value = "Customer Name";
				worksheet.getCell('D5').value = "Loan Date";
				worksheet.getCell('E5').value = "Prin. Amt";
				worksheet.getCell('F5').value = "Maturity Date";
				worksheet.getCell('G5').value = "Maturity Amt";
				worksheet.getCell('H5').value = "Int. Amt";
				worksheet.getCell('I5').value = "Late Fee";
				worksheet.getCell('J5').value = "Fin Charge";
				worksheet.getCell('K5').value = "Prin. Bal.";
				worksheet.getCell('L5').value = "Prin. Recvd";
				worksheet.getCell('M5').value = "Interest recvd";
				worksheet.getCell('N5').value = "Total Amt Recvd";
				worksheet.getCell('O5').value = "Status";

				worksheet.columns = [
					{ key: 'Loan No', width: 10 },
					{ key: 'Loan No', width: 15 },
					{ key: 'Customer Name', width: 15 },
					{ key: 'Loan Date', width: 15 },
					{ key: 'Principal Amt', width: 15, numFmt: '#.##' },
					{ key: 'Maturity Date', width: 15 },
					{ key: 'Late Fee', width: 10, },
					{ key: 'Late Fee', width: 10, },
					{ key: 'Late Fee', width: 10, },
					{ key: 'Fin Charge', width: 10 },
					{ key: 'Principal Balance', width: 15 },
					{ key: 'Principal Paid', width: 15 },
					{ key: 'Interest recvd', width: 15 },
					{ key: 'Total Paid', width: 10 },
					{ key: 'Status', width: 12 },
				];
				//  WorkSheet Header
				/*worksheet.columns = [
					{ header: 'Loan No', key: 'Loan No', width: 10 },
					{ header: 'Provider Name', key: 'Loan No', width: 10 },
					{ header: 'Customer Name', key: 'Customer Name', width: 30 },
					{ header: 'Loan Date', key: 'Loan Date', width: 30 },
					{ header: 'Prin. Amt', key: 'Principal Amt', width: 20, numFmt: '#.##' },
					{ header: 'Maturity Date', key: 'Maturity Date', width: 30 },
					{ header: 'Maturity Amt', key: 'Late Fee', width: 20, },
					{ header: 'Int. Amt', key: 'Late Fee', width: 20, },
					{ header: 'Late Fee', key: 'Late Fee', width: 20, },
					{ header: 'Fin Charge', key: 'Fin Charge', width: 20 },
					{ header: 'Prin. Bal.', key: 'Principal Balance', width: 15 },
					{ header: 'Prin. Recvd', key: 'Principal Paid', width: 15 },
					{ header: 'Interest recvd', key: 'Interest recvd', width: 15 },
					{ header: 'Total Amt Recvd', key: 'Total Paid', width: 15 },
					{ header: 'Status', key: 'Status', width: 15 },
				];*/

				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {
					var appstatus = '';

					element.loan_amount = (element.loan_amount) ? element.loan_amount : 0;
					element.total_late_fee = (element.total_late_fee) ? element.total_late_fee : 0;
					element.total_finance_charge = (element.total_finance_charge) ? element.total_finance_charge : 0;
					element.total_payment_amt = (element.total_payment_amt) ? element.total_payment_amt : 0;
					element.total_payment_amt = (element.total_payment_amt) ? element.total_payment_amt : 0;

					let FinCharge = (element.fin_fee) ? element.fin_fee : 0;
					let lateFee = (element.late_fee) ? element.late_fee : 0
					let prinRecd = (element.principal_paid) ? element.principal_paid : 0;
					let AddAmt = (element.total_additional_amt) ? element.total_additional_amt : 0;
					let totalAmt = (element.total_payment_amt) ? element.total_payment_amt : 0;

					var blc = element.loan_amount - element.total_payment_amt;
					worksheet.getCell('A' + count).value = element.plan_number;
					worksheet.getCell('B' + count).value = element.name;
					worksheet.getCell('C' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('D' + count).value = element.loan_date;
					worksheet.getCell('E' + count).value = element.main_amount;
					worksheet.getCell('F' + count).value = element.maturity_date;
					worksheet.getCell('G' + count).value = element.amount;
					worksheet.getCell('H' + count).value = (element.amount - element.main_amount);
					worksheet.getCell('I' + count).value = element.late_fee;
					worksheet.getCell('J' + count).value = element.fin_fee;
					worksheet.getCell('K' + count).value = element.remaining_amount;
					worksheet.getCell('L' + count).value = element.principal_paid + element.total_additional_amt;
					worksheet.getCell('M' + count).value = ((totalAmt + AddAmt) - (prinRecd + AddAmt + FinCharge + lateFee));
					worksheet.getCell('N' + count).value = element.total_payment_amt + element.total_additional_amt;
					worksheet.getCell('O' + count).value = element.p_status;

					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('G' + count).numFmt = '0.00';
					worksheet.getCell('H' + count).numFmt = '0.00';
					worksheet.getCell('I' + count).numFmt = '0.00';
					worksheet.getCell('J' + count).numFmt = '0.00';
					worksheet.getCell('K' + count).numFmt = '0.00';
					worksheet.getCell('L' + count).numFmt = '0.00';
					worksheet.getCell('M' + count).numFmt = '0.00';
					worksheet.getCell('N' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5', 'L5', 'M5', 'N5', 'O5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				if (result.result.length > 0) {
					// merge A and B column
					worksheet.mergeCells('A' + count + ':D' + count);
					worksheet.getCell('A' + count).value = 'Total';
					worksheet.getCell('A' + count).font = {
						bold: true
					};
					// total for actual amount
					const sumRangeLoan = `${'E2'}:${'E' + (count - 1)}`;
					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('E' + count).value = { formula: `SUM(${sumRangeLoan})` };
					worksheet.getCell('E' + count).font = {
						bold: true
					};
					// total for hps discount amount
					const sumRangeLateFee = `${'G2'}:${'G' + (count - 1)}`;
					worksheet.getCell('G' + count).numFmt = '0.00';
					worksheet.getCell('G' + count).value = { formula: `SUM(${sumRangeLateFee})` };
					worksheet.getCell('G' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeFianceCharge = `${'H2'}:${'H' + (count - 1)}`;
					worksheet.getCell('H' + count).numFmt = '0.00';
					worksheet.getCell('H' + count).value = { formula: `SUM(${sumRangeFianceCharge})` };
					worksheet.getCell('H' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeBlanceAmt = `${'I2'}:${'I' + (count - 1)}`;
					worksheet.getCell('I' + count).numFmt = '0.00';
					worksheet.getCell('I' + count).value = { formula: `SUM(${sumRangeBlanceAmt})` };
					worksheet.getCell('I' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangePrinBlance = `${'J2'}:${'J' + (count - 1)}`;
					worksheet.getCell('J' + count).numFmt = '0.00';
					worksheet.getCell('J' + count).value = { formula: `SUM(${sumRangePrinBlance})` };
					worksheet.getCell('J' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeTotalBlc = `${'K2'}:${'K' + (count - 1)}`;
					worksheet.getCell('K' + count).numFmt = '0.00';
					worksheet.getCell('K' + count).value = { formula: `SUM(${sumRangeTotalBlc})` };
					worksheet.getCell('K' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeTotalPrin = `${'L2'}:${'L' + (count - 1)}`;
					worksheet.getCell('L' + count).numFmt = '0.00';
					worksheet.getCell('L' + count).value = { formula: `SUM(${sumRangeTotalPrin})` };
					worksheet.getCell('L' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeTotalInt = `${'M2'}:${'M' + (count - 1)}`;
					worksheet.getCell('M' + count).numFmt = '0.00';
					worksheet.getCell('M' + count).value = { formula: `SUM(${sumRangeTotalInt})` };
					worksheet.getCell('M' + count).font = {
						bold: true
					};
					// total for checked amount
					const sumRangeTotalPaid = `${'N2'}:${'N' + (count - 1)}`;
					worksheet.getCell('N' + count).numFmt = '0.00';
					worksheet.getCell('N' + count).value = { formula: `SUM(${sumRangeTotalPaid})` };
					worksheet.getCell('N' + count).font = {
						bold: true
					};
				}


				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'loan-report.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});
	// fatch downloadable file
	app.get('/api/fetch-loan-pdf', jwtMW, (req, res) => {
		con = require('../db');
		const path = require('path');
		if (req.query.file !== undefined) {
			res.sendFile(path.join(__dirname, '..', req.query.file));
		}
	})
	// loan report end herer

	app.get('/api/admin-provider-invoice-list/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerInvoiceReports(con, req.query, function (result) {
			res.send(result);
		});
	})

	// filter option
	app.post('/api/admin-provider-invoice-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerInvoiceReportsFilter(con, req.body, function (result) {
			res.send(result);
		});
	});


	// download pdf single invoice reports
	app.post('/api/provider-single-invoice-pdf/', jwtMW, (req, res) => {

		//return false;
		con = require('../db');
		const pdf = require('html-pdf');

		////////

		var dataSql = require('./invoice/invoiceSql.js');
		dataSql.getInvoiceApplication(con, req.body, function (getApp) {


			if (getApp.status == 0) {
				var obj = {
					"status": 0,
					"message": "Invoice cancelled or application not available."
				}
				res.send(obj);
			}
			else if (getApp.status == 1 && Object.keys(getApp.pp_id).length > 0) {

				dataSql.getSelectedApplication(con, getApp, function (result) {

                    /*if (result.status == 1) {
                        var obj = {
                            "status": 1,
                            "message": "Submitted invoice detail view"
                        }
                        obj.selectedApplication = result;
                        obj.getInvoiceApplication = getApp
                        res.send(obj)
                    } else {
                        res.send("something wrong")
                    }*/
                    /*console.log('---------------9-9-9--')
                    console.log(result)
                    console.log('---------------9-9-9-->>>>')
                    console.log(getApp)*/
					//return false
					if (result.status == 1) {
						const pdfTemplate = require('./reports/providerSingleInvoiceReport');

						const dir = './uploads/report/'
						//var fullUrl = req.protocol + '://' + req.get('host');
						//var logoimg = fullUrl + '/uploads/logo.png';

						if (req.body.check !== undefined && req.body.check == 1) {
							result.provider.invoice_number = result.provider.invoice_number - 1;
						}
						var fullUrl = req.get('origin');
						var logoimg = fullUrl + '/logo.png';
						var filename = dir + 'provider-single-invoice.pdf';
						pdf.create(pdfTemplate(getApp, result, logoimg), {}).toFile(filename, (err) => {
							if (err) {
								var obj = {
									"status": 0,
									"message": "Currently we are not able to generate pdf",
								}
								res.send(obj);
							} else {
								var obj = {
									"status": 1,
									"result": filename
								}
								res.send(obj);
							}
						})
					}

				});
			} else if (getApp.pp_id.length == 0) {
				var obj = {
					"status": 0,
					"message": "Invoice cancelled or application not available."
				}
				res.send(obj);
			} else {
				res.send(getApp);
			}
		});



		////////


		/*var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerInvoiceReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/providerInvoiceDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'provider-invoice.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});*/
	});

	// download pdf reports
	app.post('/api/provider-invoice-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerInvoiceReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/providerInvoiceDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'provider-invoice.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	// download xls reports
	app.post('/api/provider-invoice-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/providerInvoiceReports.js');
		dataSql.providerInvoiceReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Provider Invoice'); //creating worksheet

				worksheet.mergeCells('A1:L3');

				worksheet.getCell('A1').value = 'Provider Invoices Report';
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Invoice ID";
				worksheet.getCell('B5').value = "Provider Name";
				worksheet.getCell('C5').value = "Provider Location";
				worksheet.getCell('D5').value = "Provider Phone";
				worksheet.getCell('E5').value = "Total $ Amount";
				worksheet.getCell('F5').value = "HPS Discount";
				worksheet.getCell('G5').value = "Confirm $ Amount";
				worksheet.getCell('H5').value = "Invoice Date";
				worksheet.getCell('I5').value = "Paid Date";
				worksheet.getCell('J5').value = "Bank Name";
				worksheet.getCell('K5').value = "Txn No";
				worksheet.getCell('L5').value = "Status";

				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'id', width: 10 },
					{ key: 'name', width: 15 },
					{ key: 'location', width: 15 },
					{ key: 'phone', width: 15 },
					{ key: 'dob', width: 15, numFmt: '#.##' },
					{ key: 'HPS Discount', width: 15, },
					{ key: 'Confirm $ Amount', width: 20 },
					{ key: 'Invoice Date', width: 15 },
					{ key: 'Paid Date', width: 15 },
					{ key: 'Bank Name', width: 15 },
					{ key: 'Txn No', width: 15, },
					{ key: 'Status', width: 15, },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {


					element.total_amt = (element.total_amt) ? element.total_amt : 0;
					element.total_hps_discount = (element.total_hps_discount) ? element.total_hps_discount : 0;
					element.check_amount = (element.check_amount) ? + element.check_amount : '-';
					element.date_paid = (element.date_paid) ? element.date_paid : '-';
					element.bank_name = (element.bank_name) ? element.bank_name : '-';
					element.txn_no = (element.txn_no) ? element.txn_no : '-';

					worksheet.getCell('A' + count).value = element.invoice_number;
					worksheet.getCell('B' + count).value = element.name;
					worksheet.getCell('C' + count).value = element.location_name;
					worksheet.getCell('D' + count).value = element.phone;
					worksheet.getCell('E' + count).value = element.total_amt;
					worksheet.getCell('F' + count).value = element.total_hps_discount;
					worksheet.getCell('G' + count).value = element.check_amount;
					worksheet.getCell('H' + count).value = element.date_created;
					worksheet.getCell('I' + count).value = element.date_paid;
					worksheet.getCell('J' + count).value = element.bank_name;
					worksheet.getCell('K' + count).value = element.txn_no;
					worksheet.getCell('L' + count).value = element.value;

					worksheet.getCell('E' + count).numFmt = '0.00';
					worksheet.getCell('F' + count).numFmt = '0.00';
					worksheet.getCell('G' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5', 'L5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				worksheet.getCell('A' + count).font = {
					bold: true
				};
				// total for actual amount
				const sumRange = `${'E2'}:${'E' + (count - 1)}`;
				worksheet.getCell('E' + count).numFmt = '0.00';
				worksheet.getCell('E' + count).value = { formula: `SUM(${sumRange})` };
				worksheet.getCell('E' + count).font = {
					bold: true
				};
				// total for hps discount amount
				const sumRangeDiscount = `${'F2'}:${'F' + (count - 1)}`;
				worksheet.getCell('F' + count).numFmt = '0.00';
				worksheet.getCell('F' + count).value = { formula: `SUM(${sumRangeDiscount})` };
				worksheet.getCell('F' + count).font = {
					bold: true
				};
				// total for checked amount
				const sumRangeConfirm = `${'G2'}:${'G' + (count - 1)}`;
				worksheet.getCell('G' + count).numFmt = '0.00';
				worksheet.getCell('G' + count).value = { formula: `SUM(${sumRangeConfirm})` };
				worksheet.getCell('G' + count).font = {
					bold: true
				};

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'provider-invoice.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
				/*const pdfTemplate = require('./reports/providerInvoiceDocuments');

				const dir = './uploads/report/'

				var filename = dir + 'provider-invoice.pdf';
				pdf.create(pdfTemplate(result), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})*/
			}
		});
	});

	// customer week month invoice report
	app.get('/api/admin-customer-weekmonth-list/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerInvoiceReports(con, function (result) {
			res.send(result);
		});
	})

	// customr weekly monthly filter option
	app.post('/api/admin-customer-weekmonth-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
			res.send(result);
		});
	});

	// download pdf reports for custoemr weekly & monthly invoice
	app.post('/api/customer-weeklt-monthly-invoice-pdf/', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/customerInvoiceDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'customer-invoice.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	// download xlsx reports for custoemr weekly & monthly invoice
	app.post('/api/customer-weeklt-monthly-invoice-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Customer Invoices'); //creating worksheet
				//  WorkSheet Header
				worksheet.columns = [
					{ header: 'Invoice ID', key: 'Invoice ID', width: 10 },
					{ header: 'Customer Name', key: 'Customer Name', width: 30 },
					{ header: 'Address', key: 'Address', width: 30 },
					{ header: 'City', key: 'City', width: 30 },
					{ header: 'State', key: 'State', width: 20, numFmt: '#.##' },
					{ header: 'Zip', key: 'Zip', width: 20, },
					{ header: 'Phone', key: 'Phone', width: 20 },
					{ header: 'Invoice date', key: 'Invoice date', width: 15 },
					{ header: 'Invoice due date', key: 'Invoice due date', width: 15 },
					{ header: 'Invoice Amt', key: 'Invoice amt', width: 15 },
					{ header: 'Addtl Amt', key: 'Invoice amt', width: 15 },
					{ header: 'Total Paid', key: 'Total Paid', width: 15 },
					{ header: 'Invoice status', key: 'Invoice status', width: 20, },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 2;
				jsonData.forEach(function (element, idx) {
					element.payment_date = (element.payment_date) ? element.payment_date : '-';



					element.payment_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.payment_amount)).toFixed(2)
					element.payment_amount = parseFloat(element.payment_amount);

					element.paid_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.paid_amount)).toFixed(2)
					element.paid_amount = parseFloat(element.paid_amount);

					element.additional_amount = (element.additional_amount) ? element.additional_amount : 0;
					element.paid_amount = (element.paid_amount) ? element.paid_amount : 0;

					worksheet.getCell('A' + count).value = element.invoice_number;
					worksheet.getCell('B' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('C' + count).value = element.address1;
					worksheet.getCell('D' + count).value = element.City;
					worksheet.getCell('E' + count).value = element.state_name;
					worksheet.getCell('F' + count).value = element.zip_code;
					worksheet.getCell('G' + count).value = element.peimary_phone;
					worksheet.getCell('H' + count).value = element.payment_date;
					worksheet.getCell('I' + count).value = element.due_date;
					worksheet.getCell('J' + count).value = element.payment_amount;
					worksheet.getCell('K' + count).value = element.additional_amount;
					worksheet.getCell('L' + count).value = (element.invoice_status == 1 || element.invoice_status == 4) ? element.additional_amount + element.paid_amount : 0;
					worksheet.getCell('M' + count).value = element.invoice_status_name;

					worksheet.getCell('J' + count).numFmt = '0.00';
					worksheet.getCell('K' + count).numFmt = '0.00';
					worksheet.getCell('L' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				// total for actual amount
				const sumRange = `${'J2'}:${'J' + (count - 1)}`;
				worksheet.getCell('J' + count).numFmt = '0.00';
				worksheet.getCell('J' + count).value = { formula: `SUM(${sumRange})` };
				// total for additional amount
				const sumRangeAdd = `${'K2'}:${'K' + (count - 1)}`;
				worksheet.getCell('K' + count).numFmt = '0.00';
				worksheet.getCell('K' + count).value = { formula: `SUM(${sumRangeAdd})` };
				// total for total paid amount
				const sumRangeAddTot = `${'L2'}:${'L' + (count - 1)}`;
				worksheet.getCell('L' + count).numFmt = '0.00';
				worksheet.getCell('L' + count).value = { formula: `SUM(${sumRangeAddTot})` };


				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'customer-invoice.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});

	// print invoice.
	app.post('/api/invoice-print/', jwtMW, (req, res) => {

		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/customerInvoiceReports.js');
		//dataSql.customerMultipleInvoice(con, req.body, function (result) {
		//if (result.status == 1) {
		//var inviceID = result.result.reduce(function (accumulator, currentValue) {
		//accumulator.push(currentValue.invoice_id);
		//return accumulator
		//}, []);
		dataSql.getPrintDetails(con, req.body, function (invoiceDetails) {
			

			if (invoiceDetails.status == 1) {
				invoiceDetails.resultmain = invoiceDetails.resultmain.map(function (element) {

					var newchild = invoiceDetails.childResult.filter(function (number) {
						return element.invoice_id == number.invoice_id;
					});
					//invoiceDetails.resultmain.child = newchild[0];
					var o = Object.assign({}, element);
					const uniqueID = [...new Set(newchild.map(item => item.application_id))];
					// get total amount
					var totalAmt = invoiceDetails.planDetails.reduce(function (accumulator, currentValue, currentindex) {
						if (currentindex == 0) {
							accumulator = 0;
						}

						accumulator += (currentValue.application_id == uniqueID) ? currentValue.amount : 0;
						return accumulator;
					}, 0);
					// get recived amount
					var recivedAmt = invoiceDetails.amountDetails.reduce(function (accumulator, currentValue, currentindex) {
						if (currentindex == 0) {
							accumulator = 0;
						}
						accumulator += (currentValue.application_id == uniqueID) ? currentValue.amount_rcvd : 0;
						return accumulator;
					}, 0);
					// get last payment details
					var lastDetails = invoiceDetails.LastPayment.filter(function (number) {
						return uniqueID == number.application_id;
					});

					o.child = newchild;
					o.totalAmt = totalAmt;
					o.recivedAmt = recivedAmt;
					o.lastDetails = lastDetails;
					o.MissCount = invoiceDetails.MissCount;
					return o;
				})

				const pdfTemplate = require('./reports/invoicePrintDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';

				var config = {

					"footer": {
						"height": "290px",
					},


				}
				var filename = dir + 'invoicePrint.pdf';
				pdf.create(pdfTemplate(invoiceDetails.resultmain, invoiceDetails.lateFee, invoiceDetails.LastPayment, invoiceDetails.planDetails, invoiceDetails.amountDetails, logoimg), config).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
				//res.send(invoiceDetails);
			} else {
				res.send(invoiceDetails);
			}
		})

		//}
		//});
	});

	// customr withderawal reports
	app.post('/api/admin-customer-withdrawal-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerWithdrawalReportsFilter(con, req.body, function (result) {
			res.send(result);
		});
	});

	app.post('/api/customer-withdrawal-pdf', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerWithdrawalReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/customerInvoiceWithdrawalDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'customer-invoice.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	app.post('/api/customer-withdrawal-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerWithdrawalReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Customer Invoices'); //creating worksheet

				let dataFilter = req.body;
				let fileName = 'Withdrawal Day Report';


				worksheet.mergeCells('A1:L3');
				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Invoice ID";
				worksheet.getCell('B5').value = "Customer Name";
				worksheet.getCell('C5').value = "Address";
				worksheet.getCell('D5').value = "City";
				worksheet.getCell('E5').value = "State";
				worksheet.getCell('F5').value = "Zip";
				worksheet.getCell('G5').value = "Phone";
				worksheet.getCell('H5').value = "Invoice date";
				worksheet.getCell('I5').value = "Invoice due date";
				worksheet.getCell('J5').value = "Invoice $ amt";
				worksheet.getCell('K5').value = "Withdrawal Day";
				worksheet.getCell('L5').value = "Invoice status";


				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'Invoice ID', width: 10 },
					{ key: 'Customer Name', width: 30 },
					{ key: 'Address', width: 30 },
					{ key: 'City', width: 30 },
					{ key: 'State', width: 20, numFmt: '#.##' },
					{ key: 'Zip', width: 20, },
					{ key: 'Phone', width: 20 },
					{ key: 'Invoice date', width: 15 },
					{ key: 'Invoice due date', width: 15 },
					{ key: 'Invoice amt', width: 15 },
					{ key: 'Withdrawal Day', width: 15 },
					/*{ header: 'Additional $ amt', key: 'Invoice amt', width: 15 },
					{ header: 'Total Paid', key: 'Total Paid', width: 15 },*/
					{ key: 'Invoice status', width: 20, },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {
					element.payment_date = (element.payment_date) ? element.payment_date : '-';


					if (element.invoice_status == 3) {
						element.payment_amount = (parseFloat(element.previous_late_fee) + parseFloat(element.previous_fin_charge) + parseFloat(element.late_fee_due) + parseFloat(element.fin_charge_due) + parseFloat(element.payment_amount)).toFixed(2)
					} else {
						element.payment_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.payment_amount)).toFixed(2)
						element.paid_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.paid_amount)).toFixed(2)
					}
					//element.payment_amount = (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(element.payment_amount)).toFixed(2)
					element.payment_amount = parseFloat(element.payment_amount);

					//element.paid_amount = (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(element.paid_amount)).toFixed(2)
					element.paid_amount = parseFloat(element.paid_amount);


					worksheet.getCell('A' + count).value = element.invoice_number;
					worksheet.getCell('B' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
					worksheet.getCell('C' + count).value = element.address1;
					worksheet.getCell('D' + count).value = element.City;
					worksheet.getCell('E' + count).value = element.state_name;
					worksheet.getCell('F' + count).value = element.zip_code;
					worksheet.getCell('G' + count).value = element.peimary_phone;
					worksheet.getCell('H' + count).value = element.payment_date;
					worksheet.getCell('I' + count).value = element.due_date;
					worksheet.getCell('J' + count).value = element.payment_amount;
					worksheet.getCell('K' + count).value = element.withdrawal_date;
					//worksheet.getCell('K' + count).value = element.additional_amount;
					//worksheet.getCell('L' + count).value = element.additional_amount+element.paid_amount;
					worksheet.getCell('L' + count).value = element.invoice_status_name;

					worksheet.getCell('J' + count).numFmt = '0.00';
					//worksheet.getCell('K' + count).numFmt = '0.00';
					//worksheet.getCell('L' + count).numFmt = '0.00';

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5', 'L5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});
				// merge A and B column
				worksheet.mergeCells('A' + count + ':D' + count);
				worksheet.getCell('A' + count).value = 'Total';
				// total for actual amount
				const sumRange = `${'J2'}:${'J' + (count - 1)}`;
				worksheet.getCell('J' + count).numFmt = '0.00';
				worksheet.getCell('J' + count).value = { formula: `SUM(${sumRange})` };
				worksheet.getCell('J' + count).font = {
					bold: true
				};
				// total for additional amount
				/*const sumRangeAdd = `${'K2'}:${'K' + (count - 1)}`;
				worksheet.getCell('K' + count).numFmt = '0.00';
				worksheet.getCell('K' + count).value = { formula: `SUM(${sumRangeAdd})` };
				// total for total paid amount
				const sumRangeAddTot = `${'L2'}:${'L' + (count - 1)}`;
				worksheet.getCell('L' + count).numFmt = '0.00';
				worksheet.getCell('L' + count).value = { formula: `SUM(${sumRangeAddTot})` };*/


				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'customer-invoice.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});

	// customr withderawal reports
	app.post('/api/admin-procedure-date-filter/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerProcedureReportsFilter(con, req.body, function (result) {
			res.send(result);
		});
	});
	app.post('/api/procedure-date-pdf', jwtMW, (req, res) => {
		con = require('../db');
		const pdf = require('html-pdf');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerProcedureReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const pdfTemplate = require('./reports/customerInvoiceProcedureDateDocuments');

				const dir = './uploads/report/'
				//var fullUrl = req.protocol + '://' + req.get('host');
				//var logoimg = fullUrl + '/uploads/logo.png';
				var fullUrl = req.get('origin');
				var logoimg = fullUrl + '/logo.png';
				var filename = dir + 'customer-invoice.pdf';
				pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
					if (err) {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					} else {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}
				})
			}
		});
	});

	app.post('/api/procedure-date-xls/', jwtMW, (req, res) => {
		con = require('../db');
		const excel = require('exceljs');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerProcedureReportsFilter(con, req.body, function (result) {
			if (result.status == 1) {
				const jsonData = result.result;

				let workbook = new excel.Workbook(); //creating workbook
				let worksheet = workbook.addWorksheet('Procedure Date Reports'); //creating worksheet


				let fileName = 'Provider Procedure Date Report';

				worksheet.mergeCells('A1:M3');
				console.log(fileName)
				worksheet.getCell('A1').value = fileName;
				//worksheet.getCell('A1').alignment = { horizontal: 'center' };

				worksheet.getCell('A1').alignment = {
					vertical: 'middle', horizontal: 'center'
				};
				//worksheet.getRow('A1').font = { size: 22 };
				worksheet.getCell('A1').font = {
					size: 16,
					bold: true
				};
				['A1'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'ABA286' },
						bgColor: { argb: 'ABA286' },
					};


				});

				worksheet.getCell('A5').value = "Provider A/C";
				worksheet.getCell('B5').value = "Plan No.";
				worksheet.getCell('C5').value = "Plan Amount";
				worksheet.getCell('D5').value = "Customer Name";
				worksheet.getCell('E5').value = "Provider Name";
				worksheet.getCell('F5').value = "Address";
				worksheet.getCell('G5').value = "City";
				worksheet.getCell('H5').value = "State";
				worksheet.getCell('I5').value = "Zip";
				worksheet.getCell('J5').value = "Phone";
				worksheet.getCell('K5').value = "Procedure date";
				worksheet.getCell('L5').value = "Procedure Status";
				worksheet.getCell('M5').value = "Email";


				//  WorkSheet Header
				worksheet.columns = [
					{ key: 'Provider A/C', width: 15 },
					{ key: 'Plan No.', width: 10 },
					{ key: 'Plan Amount', width: 10 },
					{ key: 'Customer Name', width: 15 },
					{ key: 'Provider Name', width: 15 },
					{ key: 'Address', width: 15 },
					{ key: 'City', width: 10 },
					{ key: 'State', width: 10, numFmt: '#.##' },
					{ key: 'Zip', width: 10, },
					{ key: 'Phone', width: 15 },
					{ key: 'Procedure date', width: 15 },
					{ key: 'Procedure Status', width: 15 },
					{ key: 'Email', width: 15 },
				];

				// Add Array Rows
				//worksheet.addRows(jsonData);
				var rowValues = [];
				var count = 6;
				jsonData.forEach(function (element, idx) {
					element.procedure_date = (element.procedure_date) ? element.procedure_date : '-';



					worksheet.getCell('A' + count).value = element.provider_ac;
					worksheet.getCell('B' + count).value = element.plan_number;
					worksheet.getCell('C' + count).value = element.amount;
					worksheet.getCell('D' + count).value = element.customer_f_name + '' + element.customer_m_name + ' ' + element.customer_l_name;
					worksheet.getCell('E' + count).value = element.name;
					worksheet.getCell('F' + count).value = element.address1;
					worksheet.getCell('G' + count).value = element.city;
					worksheet.getCell('H' + count).value = element.state_name;
					worksheet.getCell('I' + count).value = element.zip_code;
					worksheet.getCell('J' + count).value = element.phone;
					worksheet.getCell('K' + count).value = element.procedure_date;
					worksheet.getCell('L' + count).value = element.plan_status;
					worksheet.getCell('M' + count).value = element.email;

					count++;
				});
				// set header details
				['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5', 'L5', 'M5'].map(key => {
					worksheet.getCell(key).fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'aba2a2' },
						bgColor: { argb: 'aba2a2' }
					};
				});

				// Write to File
				const dir = './uploads/report/'

				var filename = dir + 'customer-invoice.xlsx';
				workbook.xlsx.writeFile(filename)
					.then(function () {
						var obj = {
							"status": 1,
							"result": filename
						}
						res.send(obj);
					}).catch(function () {
						var obj = {
							"status": 0,
							"message": "Currently we are not able to generate pdf",
						}
						res.send(obj);
					});
			}
		});
	});
	app.post('/api/admin-procedure-date-email/', jwtMW, (req, res) => {
		con = require('../db');
		var obj = {};
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerProcedureReportsFilter(con, req.body, function (result) {
			//console.log(result)
			var emialTemp = require('./emailplan.js');

			var distinctArr = Array.from(new Set(result.result.map(x => x.provider_id)))
				.map(provider_id => {
					return {
						provider_id: provider_id,
						name: result.result.find(x => x.provider_id === provider_id).name,
						email: result.result.find(x => x.provider_id === provider_id).email
					}
				})
			console.log(distinctArr)
			/*result.result = result.result.reduce((accumulator, currentValue, currentindex) => {
				if (!accumulator[currentValue.provider_id]) {
					accumulator[currentValue.provider_id] = {
						provider_id: currentValue.provider_id,
						name: currentValue.name,
						email: currentValue.email
					};
				}
				return accumulator;
			}, []);*/
			//let context = {};

			//result.result.splice(0, 8);
			/*var objectName = {
			    provider_id: 10,
			    name: 'Aman',
			    email: 'mehrasaab22@gmail.com'
			}
			result.result.push(objectName)*/
			let totalProvider = distinctArr.length;
			//console.log(result.result)
			distinctArr.map((data, idx) => {
				var context = {
					provider_name: data.name
				}
				var template = 'Procedure Date Report';

				emialTemp.customEmailTemplateDetails(template, function (Result) {

					if (Result.status == 1) {

						var emailOpt = {
							toEmail: data.email,
							subject: Result.result[0].template_subject
						}

						var html = Result.result[0].template_content;

						emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
							if (emailResult.status == 1) {
								obj.template = "Template email sent successfully!"
								//res.send(obj);
							} else {
								obj.template = "Template email not sent successfully!"
								//res.send(obj);
							}
						});
					} else {
						obj.template = "Template details not found."
						//res.send(obj);
					}

				});

				//emialTemp.emailTemplate(data.email, locals, template);
				if (0 === --totalProvider) {
					result.message = 'Email sent successfully';
					res.send(result);
				}
			})
		});
	});

	app.post('/api/admin-procedure-single-email/', jwtMW, (req, res) => {
		con = require('../db');
		var dataSql = require('./reports/customerInvoiceReports.js');
		dataSql.customerProcedureReportsFilter(con, req.body, function (result) {
			let totalProvider = result.result.length;
			console.log(totalProvider)
			var emialTemp = require('./emailplan.js');
			let context = {};

			result.result.map((data, idx) => {
				context.provider_name = data.name;
				var template = 'Procedure Date Report';

				emialTemp.customEmailTemplateDetails(template, function (Result) {

					if (Result.status == 1) {

						var emailOpt = {
							toEmail: data.email,
							subject: Result.result[0].template_subject
						}

						var html = Result.result[0].template_content;

						emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
							if (emailResult.status == 1) {
								obj.template = "Template email sent successfully!"
							} else {
								obj.template = "Template email not sent successfully!"
							}
						});
					} else {
						obj.template = "Template details not found."
					}

				});

				//emialTemp.emailTemplate(data.email, locals, template);
				if (0 === --totalProvider) {
					result.message = 'Email sent successfully';
					res.send(result);
				}
			})
		});
	});

	//customer reminder invoice 
	app.post('/api/admin-reminder-invoice-email/', jwtMW, (req, res) => {
		con = require('../db');
		//console.log(req.body)
		var obj = {};
		con.query('SELECT '
			+ 'patient_invoice.invoice_id, '
			+ 'patient_invoice.patient_id, '
			+ 'patient_invoice.invoice_number, '
			+ 'patient_invoice.payment_amount, '
			+ 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") AS due_date, '

			+ 'patient_provider.provider_id, '

			+ 'provider.name AS provider_name, '

			+ 'patient.f_name, '
			+ 'patient.l_name, '
			+ 'patient.email '

			+ 'FROM patient_invoice '

			+ 'LEFT JOIN patient '
			+ 'ON patient_invoice.patient_id = patient.patient_id '

			+ 'INNER JOIN patient_provider '
			+ 'ON patient_invoice.patient_id = patient_provider.patient_id '

			+ 'INNER JOIN provider '
			+ 'ON provider.provider_id = patient_provider.provider_id '

			+ 'WHERE patient_invoice.invoice_id IN (?) '

			+ 'GROUP BY patient_invoice.patient_id'
			, [req.body]
			, function (error, rows, fields) {
				//console.log(error)
				if (error) {
					obj.status = 0;
					obj.result = "Something wrong please try again.";
					res.send(obj)
				} else {
					var emialTemp = require('./emailplan.js');
					let totalProvider = rows.length;
					console.log(rows)
					rows.map((data, idx) => {
						var context = {
							first_name: data.f_name,
							last_name: data.l_name,
							invoice_no: data.invoice_number,
							invoice_amount: data.payment_amount,
							due_date: data.due_date,
							provider_name: data.provider_name
						}
						var template = 'Payment Reminder';

						emialTemp.customEmailTemplateDetails(template, function (Result) {

							if (Result.status == 1) {

								var emailOpt = {
									toEmail: data.email,
									subject: Result.result[0].template_subject
								}

								var html = Result.result[0].template_content;

								emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
									if (emailResult.status == 1) {
										var template = "Template email sent successfully!"
									} else {
										var template = "Template email not sent successfully!"
									}
								});
							} else {
								var template = "Template details not found."
							}

						});

						//emialTemp.emailTemplate(data.email, locals, template);
						if (0 === --totalProvider) {
							obj.message = 'Email sent successfully';
							res.send(obj);
						}
					})
				}
			});
	});
}
