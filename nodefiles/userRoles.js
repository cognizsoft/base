/*
* Title: userTypes
* Descrpation :- This module blong to user Role all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
  let date = require('date-and-time');
  app.get('/api/user-roles/', jwtMW, (req, res) => {
    con = require('../db');
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('SELECT user_type_role.user_type_role_id, master_data_values.mdv_id, master_data_values.value, master_data_values.description, master_data_values.status, userType.mdv_id as type_id , userType.value as userType FROM master_data_values INNER JOIN user_type_role ON user_type_role.mdv_role_id=master_data_values.mdv_id INNER JOIN master_data_values as userType ON userType.mdv_id=user_type_role.mdv_type_id where master_data_values.deleted_flag = ? AND master_data_values.md_id = ? ORDER BY user_type_role.user_type_role_id DESC',
        [
          0,
          'User Role'
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
            [
              1, 0, 'User Type'
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                var obj = {
                  "status": 1,
                  "result": rows,
                  "userType": result
                }
                res.send(obj);
              });
            });

        })
    })
  })

  //////////////////////////
  /////UPDATE USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-update/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('UPDATE master_data_values SET value=?,description=?,status=?,modified=?, modified_by=? WHERE mdv_id=?',
        [
          req.body.value,
          req.body.description,
          req.body.status,
          current_date,
          current_user_id,
          req.body.mdv_id
        ]
        , function (err, resultmain) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again1.";
            });
          }
          con.query('UPDATE user_type_role SET mdv_type_id=?,mdv_role_id=?, status=?, date_modified=?, modified_by=? WHERE user_type_role_id=?',
            [
              req.body.type_id,
              req.body.mdv_id,
              req.body.status,
              current_date,
              current_user_id,
              req.body.user_type_role_id,
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again22.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 1;
                    obj.message = "Something wrong please try again33.";
                  });
                }
                obj.status = 1;
                res.send(obj);
              });
            });
        });
    });




    //console.log(req.body);
    /*let user_role = req.body.value;
    let user_role_desc = req.body.description;
    let status = req.body.status;
    let mdv_id = req.body.mdv_id;
    let current_user_id2 = req.body.current_user_id;

    con.query('UPDATE master_data_values SET value="' + user_role + '", description="' + user_role_desc + '", status="' + status + '", modified=NOW(), modified_by="' + current_user_id + '" WHERE mdv_id="' + mdv_id + '"', function (err, result) {
      if (err) {
        console.log(err);
      } else {
        var obj = {
          "status": 1
        }
        res.send(obj);

      }
    })*/
  })

  //////////////////////////
  /////DELETE USER TYPE/////
  /////////////////////////

  /* app.post('/api/user-roles-delete/', jwtMW,(req,res) => {
     //console.log(req.body);
     let type_id = req.body.type_id;
     let current_user_id = req.body.current_user_id;
      con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="'+current_user_id+'" WHERE type_id="'+type_id+'"', function (err,result){
           if(err) {
             console.log(err);
           } else {
             var obj = {
            "status": 1
           }
           res.send(obj);
            
           }
      })
   })*/

  //////////////////////////
  /////INSERT USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-insert/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    var obj = {};
    // start Transaction
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('INSERT INTO master_data_values SET md_id=?,value=?,description=?,status=?,created=?,modified=?,created_by=?,modified_by=?',
        [
          "User Role",
          req.body.value,
          req.body.user_role_desc,
          req.body.status,
          current_date,
          current_date,
          current_user_id,
          current_user_id
        ]
        , function (err, resultmain) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('INSERT INTO user_type_role SET mdv_type_id=?, mdv_role_id=?, status=?, date_created=?, date_modified=?, created_by=?, modified_by=?',
            [
              req.body.mdv_type_id,
              resultmain.insertId,
              req.body.status,
              current_date,
              current_date,
              current_user_id,
              current_user_id
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                obj = {
                  "status": 1,
                  "last_role_id": resultmain.insertId,
                  "user_type_role_id": result.insertId,
                  "userType": req.body.mdv_type_id,
                  "user_role": req.body.value,
                  "user_role_desc": req.body.user_role_desc,
                  "user_role_status": req.body.status
                }
                res.send(obj);
              });
            });
        });
    });


    /*con.query('INSERT INTO master_data_values (md_id, value, description, status, created_by, modified_by, created, modified) VALUES("User Role", "' + req.body.value + '", "' + req.body.user_role_desc + '", "' + req.body.status + '", "' + current_user_id + '", "' + current_user_id + '", NOW(), NOW())', function (err, result) {

      if (err) {
        console.log(err);
      } else {
        var obj = {
          "status": 1,
          "last_role_id": result.insertId,
          "user_role": req.body.value,
          "user_role_desc": req.body.user_role_desc,
          "user_role_status": req.body.status
        }
        res.send(obj);

      }
    })*/
  })

  app.post('/api/user-roles-check-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.mdv_id === undefined || req.body.mdv_id === "") {
      sql = 'SELECT master_data_values.mdv_id FROM master_data_values INNER JOIN user_type_role ON user_type_role.mdv_role_id=master_data_values.mdv_id WHERE master_data_values.value=? AND master_data_values.md_id=? AND user_type_role.mdv_type_id=?';
      var edit = 0;
    } else {
      sql = 'SELECT master_data_values.mdv_id FROM master_data_values INNER JOIN user_type_role ON user_type_role.mdv_role_id=master_data_values.mdv_id WHERE master_data_values.value=? AND master_data_values.md_id=? AND user_type_role.mdv_type_id=? AND user_type_role.user_type_role_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        req.body.value,
        "User Role",
        req.body.name,
        req.body.mdv_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 1,
            "message": "Username or password mismatch."
          }
          res.send(obj);

        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })
  //////////////////////////
  /////LAST INSERT ID USER TYPE/////
  /////////////////////////

  /*
   app.get('/api/user-types-last-insert-id/', jwtMW,(req,res) => {
 
     con.query('SELECT mdv_id FROM master_data_values WHERE deleted_flag = 0 AND md_id = "User Type" ORDER BY mdv_id DESC LIMIT 1', function (error,rows, fields){
       if(error) console.log(error)
       else {
         //console.log(rows)
         var obj = {
            "status": 1,
            "result": rows,
         }
         res.send(obj);
       }
     })
   })
 
   app.post('/api/usertypes/submit', jwtMW, (req,res) => {
     console.log(req.body)
     let user_type = req.body.usertype;
     let status    = req.body.status;
     // insert new user type according to request
     con.query('INSERT INTO `user_types`(`user_type`, `status`) VALUES ("'+user_type+'",'+status+')', function(error,rows,fields){
     console.log(rows);
     console.log(error)  
       if(error){
         var obj = {
           "status": 0,
           "message": "Something wrong please try again."
        }
        res.send(obj);
       }else{
         con.query('select type_id,user_type,status from user_types where delete_flag = 0', function (error,rows, fields){
           if(error) console.log(error)
           else {
             var obj = {
               "status": 1,
               "result": rows,
             }
             res.send(obj);
             
           }
         })
         
       }
     })
     
   })
 
   app.post('/api/usertypes/getUserType', jwtMW, (req,res) => {
     console.log(req.body)
     let user_type_id = req.body.user_type_id;
     // insert new user type according to request
     con.query('SELECT type_id,user_type,status FROM `user_types` WHERE type_id='+user_type_id, function(error,rows,fields){
     console.log(rows);
     console.log(error)  
       if(error){
         var obj = {
           "status": 0,
           "message": "Something wrong please try again."
        }
        res.send(obj);
       }else{
         var obj = {
           "status": 1,
           "result": rows,
         }
         res.send(obj);
         
       }
     })
     
   })
   
 */
  //other routes..
}
