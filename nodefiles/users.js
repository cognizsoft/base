/* 
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
  const md5 = require('md5');
  const jwt = require('jsonwebtoken');
  let date = require('date-and-time');


  app.post('/api/users/login', (req, res) => {
    con = require('../db');
    let username = req.body.username;
    let password = req.body.password;
    password = md5(password);
    con.query('SELECT user.user_id,user.lock_account,user.lock_expires,user.lock_count, user.username, master_data_values.value as role,user.user_type_role_id, user_type_role.mdv_type_id, user.client_id,user.patient_id '
      + 'FROM user '
      + 'INNER JOIN user_type_role on user_type_role.user_type_role_id=user.user_type_role_id '
      + 'INNER JOIN master_data_values on master_data_values.mdv_id=user_type_role.mdv_type_id '
      + 'WHERE user.delete_flag = ? AND user.status=? AND user.username=?',
      [
        0,
        1,
        username.trim(),
      ]
      , function (error, erows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          if (erows.length > 0) {
            if (erows[0].lock_account == 0 || (Date.now() / 1000) >= erows[0].lock_expires) {
              con.query('SELECT user.user_id, user.username, user.agree_flag, master_data_values.value as role,user.user_type_role_id, user_type_role.mdv_type_id, user.client_id,user.patient_id,user.co_signer_id, roles.value as role_name,user.f_name,user.m_name,user.l_name '
                + 'FROM user '
                + 'INNER JOIN user_type_role on user_type_role.user_type_role_id=user.user_type_role_id '
                + 'INNER JOIN master_data_values on master_data_values.mdv_id=user_type_role.mdv_type_id '
                + 'INNER JOIN master_data_values as roles on roles.mdv_id=user_type_role.mdv_role_id '
                + 'WHERE user.delete_flag = ? AND user.status=? AND user.username=? AND user.password=?',
                [
                  0,
                  1,
                  username.trim(),
                  password.trim(),
                ]
                , function (error, rows, fields) {
                  
                  if (error) {
                    var obj = {
                      "status": 0,
                      "message": "Something wrong please try again."
                    }
                    res.send(obj);
                  } else {
                    if (rows.length > 0) {
                      let token = jwt.sign({ username: rows[0].username }, 'ramesh', { expiresIn: 3600 }); // Signing the token
                      /*
                      * This sql use for get user permission
                      */
                      /*con.query('select system_module.sys_mod_id as   description,permission.create_flag,permission.edit_flag,permission.view_flag,permission.delete_flag '
                        + 'from permission '
                        + 'INNER JOIN system_module ON system_module.sys_mod_id=permission.sys_mod_id '
                        + 'INNER JOIN user ON user.user_type_role_id=permission.user_type_role_id '
                        + 'where user.user_id=' + rows[0].user_id, function (error, permission, fields) {*/
                      con.query('SELECT system_module.sys_mod_id AS description, '

                        + '(SELECT permission.create_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS create_flag, '

                        + '(SELECT permission.edit_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS edit_flag,'

                        + '(SELECT permission.view_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS view_flag,'

                        + '(SELECT permission.delete_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS delete_flag '

                        + 'FROM system_module'
                        ,
                        [
                          rows[0].user_type_role_id,
                          rows[0].user_type_role_id,
                          rows[0].user_type_role_id,
                          rows[0].user_type_role_id
                        ]
                        , function (error, permission, fields) {
                          if (error) {
                            var obj = {
                              "user": {
                                "status": 1,
                                "username": rows[0].username,
                                "name" : rows[0].f_name+' '+rows[0].m_name+' '+rows[0].l_name,
                                "current_user_id": rows[0].user_id,
                                "provider_id": rows[0].client_id,
                                "patient_id": rows[0].patient_id,
                                "co_signer_id": rows[0].co_signer_id,
                                "mdv_type_id": rows[0].mdv_type_id,
                                "agree_flag": rows[0].agree_flag,
                                "timezone": Intl.DateTimeFormat().resolvedOptions().timeZone,
                                token,
                                "expiresAt": Math.floor(Date.now() / 1000) + (3600)
                              },
                              "role": rows[0].role,
                              "user_permission": '',
                              "role_name" : rows[0].role_name,
                            }
                            res.send(obj);
                          } else {
                            var obj = {
                              "user": {
                                "status": 1,
                                "username": rows[0].username,
                                "name" : rows[0].f_name+' '+rows[0].m_name+' '+rows[0].l_name,
                                "current_user_id": rows[0].user_id,
                                "provider_id": rows[0].client_id,
                                "patient_id": rows[0].patient_id,
                                "co_signer_id": rows[0].co_signer_id,
                                "mdv_type_id": rows[0].mdv_type_id,
                                "agree_flag": rows[0].agree_flag,
                                "timezone": Intl.DateTimeFormat().resolvedOptions().timeZone,
                                token,
                                "expiresAt": Math.floor(Date.now() / 1000) + (3600)
                              },
                              "role": rows[0].role,
                              "user_permission": permission,
                              "role_name" : rows[0].role_name,
                            }
                            res.send(obj);
                          }
                          // update account lockout details 
                          con.query('UPDATE `user` SET '
                            + 'lock_count=?, '
                            + 'lock_account=?, '
                            + 'lock_expires=? '
                            + 'WHERE user_id=?',
                            [
                              0,
                              0,
                              '',
                              rows[0].user_id,
                            ]
                            , function (error, rows, fields) {
                            })
                        })
                    }
                    else {
                      // get mater data
                      con.query('SELECT value as account_attempts, (SELECT value FROM master_data_values WHERE md_id=?) as lockout_duration '
                        + 'FROM master_data_values '
                        + 'WHERE md_id=?',
                        [
                          'Account Lockout Duration',
                          'Account Attempts',
                        ]
                        , function (error, masterData, fields) {
                          if (error) {
                            var obj = {
                              "status": 0,
                              "message": "Something wrong please try again."
                            }
                            res.send(obj);
                          } else {
                            // update user table according to attempts
                            var attempts = (erows[0].lock_count == 0) ? (masterData[0].account_attempts - 1) : (erows[0].lock_count - 1);
                            var islock = (attempts == 0) ? 1 : 0;
                            con.query('UPDATE `user` SET '
                              + 'lock_count=?, '
                              + 'lock_account=?, '
                              + 'lock_expires=? '
                              + 'WHERE user_id=?',
                              [
                                attempts,
                                islock,
                                ((Date.now() / 1000) + (3600 * masterData[0].lockout_duration)),
                                erows[0].user_id,
                              ]
                              , function (error, rows, fields) {
                                var obj = {
                                  "status": 0,
                                  "message": (attempts == 0) ? "Account has been locked please try again after " + masterData[0].lockout_duration + " hours" : attempts + " login attempt remaining."
                                }
                                res.send(obj);
                              })
                          }
                        })

                    }
                  }
                })
            } else {
              var obj = {
                "status": 0,
                "message": "Your account has been locked. Please contact your administrator."
              }
              res.send(obj);
            }
          } else {
            var obj = {
              "status": 0,
              "message": "Username or Password is wrong."
            }
            res.send(obj);
          }
        }
      })
  })

  app.post('/api/users/login123', (req, res) => {
    con = require('../db');
    let username = req.body.username;
    let password = req.body.password;
    password = md5(password);

    con.query('SELECT user.user_id, user.username, master_data_values.value as role,user.user_type_role_id, user_type_role.mdv_type_id, user.client_id,user.patient_id '
      + 'FROM user '
      + 'INNER JOIN user_type_role on user_type_role.user_type_role_id=user.user_type_role_id '
      + 'INNER JOIN master_data_values on master_data_values.mdv_id=user_type_role.mdv_type_id '
      + 'WHERE user.delete_flag = ? AND user.status=? AND user.username=? AND user.password=?',
      [
        0,
        1,
        username.trim(),
        password.trim(),
      ]
      , function (error, rows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          if (rows.length > 0) {
            let token = jwt.sign({ username: rows[0].username }, 'ramesh', { expiresIn: 3600 }); // Signing the token
            /*
            * This sql use for get user permission
            */
            /*con.query('select system_module.sys_mod_id as   description,permission.create_flag,permission.edit_flag,permission.view_flag,permission.delete_flag '
              + 'from permission '
              + 'INNER JOIN system_module ON system_module.sys_mod_id=permission.sys_mod_id '
              + 'INNER JOIN user ON user.user_type_role_id=permission.user_type_role_id '
              + 'where user.user_id=' + rows[0].user_id, function (error, permission, fields) {*/
            con.query('SELECT system_module.sys_mod_id AS description, '

              + '(SELECT permission.create_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS create_flag, '

              + '(SELECT permission.edit_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS edit_flag,'

              + '(SELECT permission.view_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS view_flag,'

              + '(SELECT permission.delete_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS delete_flag '

              + 'FROM system_module'
              ,
              [
                rows[0].user_type_role_id,
                rows[0].user_type_role_id,
                rows[0].user_type_role_id,
                rows[0].user_type_role_id
              ]
              , function (error, permission, fields) {
                if (error) {
                  var obj = {
                    "user": {
                      "status": 1,
                      "username": rows[0].username,
                      "current_user_id": rows[0].user_id,
                      "provider_id": rows[0].client_id,
                      "patient_id": rows[0].patient_id,
                      "mdv_type_id": rows[0].mdv_type_id,
                      token,
                      "expiresAt": Math.floor(Date.now() / 1000) + (3600)
                    },
                    "role": rows[0].role,
                    "user_permission": '',
                  }
                  res.send(obj);
                } else {
                  var obj = {
                    "user": {
                      "status": 1,
                      "username": rows[0].username,
                      "current_user_id": rows[0].user_id,
                      "provider_id": rows[0].client_id,
                      "patient_id": rows[0].patient_id,
                      "mdv_type_id": rows[0].mdv_type_id,
                      token,
                      "expiresAt": Math.floor(Date.now() / 1000) + (3600)
                    },
                    "role": rows[0].role,
                    "user_permission": permission,
                  }
                  res.send(obj);
                }
              })
          }
          else {
            //var myData = ['status'=>0, 'message'=>'Username and password not match'];
            var obj = {
              "status": 0,
              "message": "Username or password mismatch."
            }
            res.send(obj);
          }
        }
      })
  })

  app.get('/api/user-role', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT user_type_role_id as mdv_id,value FROM user_type_role INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_role_id WHERE master_data_values.status=? AND master_data_values.deleted_flag=? AND mdv_type_id=?',
      [
        1,
        0,
        req.query.id
      ]
      , function (error, rows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Username or password mismatch."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1,
            "result": rows
          }
          res.send(obj);
        }
      })
  })
  app.get('/api/user-list/', jwtMW, (req, res) => {
    con = require('../db');
    let result = {};
    con.query('SELECT user.user_id, user.username, user.email_id, user.f_name, user.m_name, user.l_name,user.phone, user.status, user.lock_account, master_data_values.value as user_type, user_type_role.mdv_type_id as mdv_user_type_id, master_data_values_role.value as user_role, user_type_role.user_type_role_id as mdv_role_id, provider.provider_id, provider.name, provider_location.location_name '
      + 'FROM user '
      + 'INNER JOIN user_type_role ON user_type_role.user_type_role_id=user.user_type_role_id '
      + 'INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_type_id '
      + 'INNER JOIN master_data_values as master_data_values_role ON master_data_values_role.mdv_id=user_type_role.mdv_role_id '
      + 'LEFT JOIN provider ON provider.provider_id=user.client_id '
      + 'LEFT JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
      + 'WHERE user.delete_flag = 0 ORDER BY user.user_id DESC',
      [
        0
      ]
      , function (error, rows, fields) {
        if (error) {
          result.status = 0;
          result.message = "Something wrong please try again.";
        }
        else {
          // get user type
          con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
            [
              1, 0, 'User Type'
            ]
            , function (error, type, fields) {
              if (error) {
                result.status = 0;
                result.message = "Something wrong please try again.";
              }
              else {
                con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                  [
                    1, 0, 'User Role'
                  ]
                  , function (error, role, fields) {
                    if (error) {
                      result.status = 0;
                      result.message = "Something wrong please try again.";
                    }
                    else {
                      con.query('SELECT provider_id,name FROM provider WHERE status=? AND deleted_flag = ?',
                        [
                          1, 0
                        ]
                        , function (error, provider, fields) {
                          if (error) {
                            result.status = 0;
                            result.message = "Something wrong please try again.";
                          }
                          else {
                            con.query('SELECT security_answers.id AS security_answers_id, '
                              + 'security_answers.user_id, '
                              + 'security_answers.security_questions_id, '
                              + 'security_answers.answers, '

                              + 'security_questions.name '

                              + 'FROM security_answers '

                              + 'INNER JOIN security_questions '
                              + 'ON security_questions.id = security_answers.security_questions_id '

                              + 'WHERE security_answers.status = ? AND security_answers.delete_flag = ?'
                              ,
                              [
                                1, 0
                              ]
                              , function (error, user_questions, fields) {
                                if (error) {
                                  result.status = 0;
                                  result.message = "Something wrong please try again.";
                                } else {
                                  result.status = 1;
                                  result.userRole = role;
                                  result.userType = type;
                                  result.result = rows;
                                  result.provider = provider;
                                  result.user_questions = user_questions;
                                  res.send(result);
                                }
                              })

                          }
                        })
                    }
                  })
              }
            })

        }
      })
  })

  app.post('/api/user-username-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=?';
      var edit = 0;
    } else {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=? AND user_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-eamil-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=?';
      var edit = 0;
    } else {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=? AND user_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-delete/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('UPDATE user SET delete_flag=? WHERE user_id=?',
      [
        1,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "user_id": req.body.user_id
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-unlock/', jwtMW, (req, res) => {
    //return false;
    con = require('../db');
    con.query('UPDATE user SET lock_account=?, lock_expires=?, lock_count=? WHERE user_id=?',
      [
        0,
        '',
        0,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "user_id": req.body.user_id
          }
          res.send(obj);

        }
      })
  })

  app.get('/api/lock-user-details', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT master_data_values.value as user_type, user.co_signer_id '
      + 'FROM user '
      + 'INNER JOIN user_type_role ON user_type_role.user_type_role_id=user.user_type_role_id '
      + 'INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_type_id '
      + 'INNER JOIN master_data_values as master_data_values_role ON master_data_values_role.mdv_id=user_type_role.mdv_role_id '

      + 'WHERE user.delete_flag = 0 AND user.user_id = ? ORDER BY user.user_id DESC',
      [
        req.query.user_id
      ]
      , function (error, user, fields) {
        if (error || user.lenght == 0) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          if (user[0].user_type == 'Provider') {
            con.query('SELECT user.patient_id, user.f_name, user.m_name, user.l_name, '
              + 'tax_ssn_id as ssn '
              + 'FROM user '
              + 'INNER JOIN provider ON user.client_id = provider.provider_id '
              + 'WHERE user.user_id = ?'
              ,
              [
                req.query.user_id
              ]
              , function (error, rows, fields) {
                rows[0].dob = ''
                if (error) {
                  var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                  }
                  res.send(obj);
                } else {
                  var obj = {
                    "status": 1,
                    "result": rows
                  }
                  res.send(obj);
                }
              })
          } else if (user[0].user_type == 'Customers' && user[0].co_signer_id != null) {
            con.query('SELECT '
              + 'user.co_signer_id as patient_id, '
              + 'user.f_name, '
              + 'user.m_name, '
              + 'user.l_name, '
              + 'co_signer.dob AS dd, '
              + 'co_signer.ssn AS ss, '

              + 'AES_DECRYPT(co_signer.dob, "ramesh_cogniz") as dob, '
              + 'AES_DECRYPT(co_signer.ssn, "ramesh_cogniz") as ssn '

              + 'FROM user '

              + 'INNER JOIN co_signer '
              + 'ON user.co_signer_id = co_signer.co_signer_id '

              + 'WHERE user.user_id = ?'
              ,
              [
                req.query.user_id
              ]
              , function (error, rows, fields) {
                console.log(error)
                rows[0].dob = rows[0].dob.toString()
                var dobSplit = (rows[0].dob).split('-')

                rows[0].dob = dobSplit[1] + '/' + dobSplit[2] + '/' + dobSplit[0]


                rows[0].ssn = rows[0].ssn.toString()
                if (error) {
                  var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                  }
                  res.send(obj);
                } else {
                  var obj = {
                    "status": 1,
                    "result": rows
                  }
                  res.send(obj);
                }
              })
          } else if (user[0].user_type == 'Customers' && user[0].co_signer_id == null) {
            con.query('SELECT '
              + 'user.patient_id, '
              + 'user.f_name, '
              + 'user.m_name, '
              + 'user.l_name, '
              + 'patient.dob AS dd, '
              + 'patient.ssn AS ss, '

              + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob, '
              + 'AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn '

              + 'FROM user '

              + 'INNER JOIN patient '
              + 'ON user.patient_id = patient.patient_id '

              + 'WHERE user.user_id = ?'
              ,
              [
                req.query.user_id
              ]
              , function (error, rows, fields) {
                rows[0].dob = rows[0].dob.toString()
                var dobSplit = (rows[0].dob).split('-')

                rows[0].dob = dobSplit[1] + '/' + dobSplit[2] + '/' + dobSplit[0]


                rows[0].ssn = rows[0].ssn.toString()
                if (error) {
                  var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                  }
                  res.send(obj);
                } else {
                  var obj = {
                    "status": 1,
                    "result": rows
                  }
                  res.send(obj);
                }
              })
          } else {
            con.query('SELECT '
              + 'user.patient_id, '
              + 'user.f_name, '
              + 'user.m_name, '
              + 'user.l_name '
              + 'FROM user '
              + 'WHERE user.user_id = ?'
              ,
              [
                req.query.user_id
              ]
              , function (error, rows, fields) {
                rows[0].dob = '';
                rows[0].ssn = '';
                if (error) {
                  var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                  }
                  res.send(obj);
                } else {
                  var obj = {
                    "status": 1,
                    "result": rows
                  }
                  res.send(obj);
                }
              })
          }
        }
      })
  })
  //other routes..
  app.post('/api/user-insert/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);

    var obj = {}
    con.query('INSERT INTO user SET user_type_role_id=?,client_id=?,username=?,password=?,email_id=?,f_name=?,m_name=?,l_name=?,phone=?,status=?,date_created=?,date_modified=?,created_by=?,modified_by=?',
      [
        req.body.mdv_role_id,
        (req.body.client_id) ? req.body.client_id : null,
        req.body.username,
        password,
        req.body.email_id,
        req.body.f_name,
        req.body.m_name,
        req.body.l_name,
        req.body.phone,
        req.body.status,
        current_date,
        current_date,
        current_user_id,
        current_user_id
      ]
      , function (error, result, fields) {

        if (error) {
          obj.status = 0;
          obj.message = "Something wrong please try again.";
          res.send(obj);
        } else {
          var emialTemp = require('./email.js');
          var locals = {
            name: req.body.f_name + ' ' + req.body.m_name + ' ' + req.body.l_name,
            username: req.body.username,
            password: req.body.password
          };
          var toEmailAddress = req.body.email_id;
          var template = 'usersregister';
          emialTemp.emailTemplate(toEmailAddress, locals, template);
          obj.status = 1;
          obj.user_id = result.insertId;
          obj.mdv_user_type_id = req.body.mdv_user_type_id;
          obj.mdv_role_id = req.body.mdv_role_id;
          obj.username = req.body.username;
          obj.email_id = req.body.email_id;
          obj.userstatus = req.body.status;
          obj.f_name = req.body.f_name;
          obj.m_name = req.body.m_name;
          obj.l_name = req.body.l_name;
          obj.phone = req.body.phone;
          obj.provider_id = req.body.client_id;
          res.send(obj);

        }
      });
  });

  app.post('/api/user-update/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    var obj = {};
    con.query('UPDATE user SET user_type_role_id=?,client_id=?,username=?,email_id=?,f_name=?,m_name=?,l_name=?,phone=?,status=?,date_modified=?,modified_by=? WHERE user_id=?',
      [
        req.body.mdv_role_id,
        (req.body.provider_id) ? req.body.provider_id : null,
        req.body.username,
        req.body.email_id,
        req.body.f_name,
        req.body.m_name,
        req.body.l_name,
        req.body.phone,
        req.body.status,
        current_date,
        current_user_id,
        req.body.user_id
      ]
      , function (error, result, fields) {

        if (error) {
          obj.status = 0;
          obj.message = "Something wrong please try again.";
          res.send(obj);
        } else {

          obj.status = 1;
          res.send(obj);

        }
      });
  });

  app.post('/api/user-password/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);
    con.query('UPDATE user SET password=?,date_modified=?,modified_by=? WHERE user_id=?',
      [
        password,
        current_date,
        current_user_id,
        req.body.password_id,
      ]
      , function (err, result) {
        if (err) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1
          }
          res.send(obj);

        }
      })
  })

  /*
  * Provider
  */
  app.get('/api/user-list-provider/', jwtMW, (req, res) => {
    con = require('../db');
    let result = {};
    con.query('SELECT user.user_id, user.username, user.email_id, user.f_name, user.m_name, user.l_name,user.phone, user.status, master_data_values.value as user_type, user_type_role.mdv_type_id as mdv_user_type_id, master_data_values_role.value as user_role, user_type_role.user_type_role_id as mdv_role_id, provider.provider_id, provider.name, provider_location.location_name '
      + 'FROM user '
      + 'INNER JOIN user_type_role ON user_type_role.user_type_role_id=user.user_type_role_id '
      + 'INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_type_id '
      + 'INNER JOIN master_data_values as master_data_values_role ON master_data_values_role.mdv_id=user_type_role.mdv_role_id '
      + 'LEFT JOIN provider ON provider.provider_id=user.client_id '
      + 'LEFT JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
      + 'WHERE user.delete_flag = ? AND client_id = ? ORDER BY user.user_id DESC',
      [
        0,
        req.query.id
      ]
      , function (error, rows, fields) {
        if (error) {
          result.status = 0;
          result.message = "Something wrong please try again.";
        }
        else {
          // get user type
          con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
            [
              1, 0, 'User Type'
            ]
            , function (error, type, fields) {
              if (error) {
                result.status = 0;
                result.message = "Something wrong please try again.";
              }
              else {
                con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                  [
                    1, 0, 'User Role'
                  ]
                  , function (error, role, fields) {
                    if (error) {
                      result.status = 0;
                      result.message = "Something wrong please try again.";
                    }
                    else {
                      con.query('SELECT provider_id,name FROM provider WHERE status=? AND deleted_flag = ?',
                        [
                          1, 0
                        ]
                        , function (error, provider, fields) {
                          if (error) {
                            result.status = 0;
                            result.message = "Something wrong please try again.";
                          }
                          else {
                            result.status = 1;
                            result.userRole = role;
                            result.userType = type;
                            result.result = rows;
                            result.provider = provider;
                            res.send(result);
                          }
                        })
                    }
                  })
              }
            })

        }
      })
  })
  app.post('/api/user-insert-provider/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);

    var obj = {}
    con.query('INSERT INTO user SET user_type_role_id=?,client_id=?,username=?,password=?,email_id=?,f_name=?,m_name=?,l_name=?,phone=?,status=?,date_created=?,date_modified=?,created_by=?,modified_by=?',
      [
        req.body.mdv_role_id,
        (req.body.client_id) ? req.body.client_id : null,
        req.body.username,
        password,
        req.body.email_id,
        req.body.f_name,
        req.body.m_name,
        req.body.l_name,
        req.body.phone,
        req.body.status,
        current_date,
        current_date,
        current_user_id,
        current_user_id
      ]
      , function (error, result, fields) {

        if (error) {
          obj.status = 0;
          obj.message = "Something wrong please try again.";
          res.send(obj);
        } else {
          var emialTemp = require('./email.js');
          var locals = {
            name: req.body.f_name + ' ' + req.body.m_name + ' ' + req.body.l_name,
            username: req.body.username,
            password: req.body.password
          };
          var toEmailAddress = req.body.email_id;
          var template = 'usersregister';
          emialTemp.emailTemplate(toEmailAddress, locals, template);
          obj.status = 1;
          obj.user_id = result.insertId;
          obj.mdv_user_type_id = req.body.mdv_user_type_id;
          obj.mdv_role_id = req.body.mdv_role_id;
          obj.username = req.body.username;
          obj.email_id = req.body.email_id;
          obj.userstatus = req.body.status;
          obj.f_name = req.body.f_name;
          obj.m_name = req.body.m_name;
          obj.l_name = req.body.l_name;
          obj.phone = req.body.phone;
          obj.provider_id = req.body.client_id;
          res.send(obj);

        }
      });
  });
  app.post('/api/user-update-provider/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    var obj = {};
    con.query('UPDATE user SET user_type_role_id=?,username=?,email_id=?,f_name=?,m_name=?,l_name=?,phone=?,status=?,date_modified=?,modified_by=? WHERE user_id=?',
      [
        req.body.mdv_role_id,
        req.body.username,
        req.body.email_id,
        req.body.f_name,
        req.body.m_name,
        req.body.l_name,
        req.body.phone,
        req.body.status,
        current_date,
        current_user_id,
        req.body.user_id
      ]
      , function (error, result, fields) {

        if (error) {
          obj.status = 0;
          obj.message = "Something wrong please try again.";
          res.send(obj);
        } else {

          obj.status = 1;
          res.send(obj);

        }
      });
  });

  /*
  * user-forget-password
  */

  app.post('/api/user-forget-password', (req, res) => {
    con = require('../db');
    const crypto = require('crypto');
    con.query('SELECT user_id,f_name,m_name,l_name '
      + 'FROM user '
      + 'WHERE email_id = ?',
      [
        req.body.email
      ]
      , function (error, rows) {
        if (Object.keys(rows).length === 0) {
          var obj = {
            "status": 0,
            "message": "Email address not found."
          }
          res.send(obj);
        } else {
          var toeken = crypto.randomBytes(20).toString('hex');
          con.query('UPDATE user SET pass_toekn=?, pass_expires=? WHERE user_id=?',
            [
              toeken,
              Date.now() + 360000,
              rows[0].user_id
            ]
            , function (error, update, fields) {
              if (error) {
                var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
                }
                res.send(obj);
              } else {
                var emialTemp = require('./email.js');
                var locals = {
                  name: rows[0].f_name + ' ' + rows[0].f_name + ' ' + rows[0].l_name,
                  link: req.body.base + 'application/reset?check=' + toeken,
                  email: req.body.email,
                };
                var toEmailAddress = req.body.email;
                var template = 'userspassword';
                emialTemp.emailTemplate(toEmailAddress, locals, template);

                var obj = {
                  "status": 1,
                  "message": "Recovery mail sent."
                }
                res.send(obj);
              }
            })
        }
      })
  })

  /*
  * Reset password
  */
  app.post('/api/user-check-token', (req, res) => {
    con = require('../db');
    const crypto = require('crypto');
    con.query('SELECT user_id '
      + 'FROM user '
      + 'WHERE pass_toekn = ? AND pass_expires >= ?',
      //+ 'WHERE pass_toekn = ?',
      [
        req.body.token,
        Date.now()
      ]
      , function (error, rows) {

        if (Object.keys(rows).length === 0) {
          var obj = {
            "status": 0,
            "message": "Password reset link is invalid or has expired."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1,
            "user_id": rows[0].user_id
          }
          res.send(obj);
        }
      })
  })

  /*
  * Update password
  */
  app.post('/api/user-update-password/', (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);

    con.query('UPDATE user SET password=?,date_modified=? WHERE user_id=?',
      [
        password,
        current_date,
        req.body.user_id,
      ]
      , function (err, result) {
        if (err) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1,
            "message": "Your password has been changed successfully."
          }
          res.send(obj);

        }
      })
  })

  /*
  * Update agree flag
  */
  app.post('/api/user-update-agree-flag/', jwtMW, (req, res) => {
    con = require('../db');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    var obj = {};
    // provider agrement
    var fullUrl = req.get('origin');
    var logoimg = fullUrl + '/logo.png';
    const pdf = require('html-pdf');
    const pdfTemplate = require('./provider/provider-agreement');
    const dir = './uploads/provider/' + req.body.providerID;
    var filename = dir + '/provider-agreement-' + req.body.providerID + '.pdf';
    pdf.create(pdfTemplate(req.body, logoimg), {}).toFile(filename, (err) => {
      if (err) {
        var obj = {
          "status": 0,
          "message": "Currently we are not able to print payment plan",
        }
        res.send(obj);
      } else {
        var oneDrive = require('./microsoft/oneDrive.js');
        oneDrive.getOneDriveLogin(function (loginOne) {
          if (loginOne.status == 1) {
            const path = require('path');
            var curentfile = {
              filename: 'provider-agreement-' + req.body.providerID + '.pdf',
              path: path.join(__dirname, '..', filename),
              provider_id: req.body.providerID,
            }
            oneDrive.getOneDriveProviderUpload(loginOne.token, curentfile, function (one) {
              if ((one.status == 1)) {
                var item_id = one.item_id;
                const fs = require('fs');
                fs.unlinkSync(curentfile.path)
              } else {
                var item_id = '';
              }
              con.query('INSERT INTO doc_repo(provider_id, name, file_path, item_id, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
                [req.body.providerID, curentfile.filename, filename, item_id, 1, current_date, current_date, current_user_id, current_user_id]
                , function (err, resultProviderLocationSpl) {
                  if (err) {
                    var obj = {
                      "status": 0,
                      "message": "Currently we are not able to print payment plan",
                    }
                    res.send(obj);
                  } else {
                    con.query('UPDATE user SET agree_flag=?, date_modified=? WHERE user_id=?',
                      [

                        req.body.flag,
                        current_date,
                        current_user_id

                      ]
                      , function (error, agreement_flag, fields) {
                        if (error) {
                          var obj = {
                            "status": 0,
                            "message": "Currently we are not able to print payment plan",
                          }
                          res.send(obj);
                        } else {

                          var obj = {
                            "status": 1,
                          }
                          res.send(obj);
                        }
                      });
                  }
                })
            })
          } else {
            var obj = {
              "status": 0,
              "message": "Currently we are not able to print payment plan",
            }
            res.send(obj);
          }
        })

      }
    })
    //return false;

  });

}
