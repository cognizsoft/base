/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/risk-factor-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Risk Factors' AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/risk-factor/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT risk_factor_weight.id, risk_factor_weight.weight_type, risk_factor_weight.weight, DATE_FORMAT(risk_factor_weight.effective_date,"%Y-%m-%d") AS effective_date, risk_factor_weight.status, risk_factor_weight.mdv_factor_type_id AS factor_type_id, master_data_values.value AS factor_type FROM risk_factor_weight INNER JOIN master_data_values ON risk_factor_weight.mdv_factor_type_id = master_data_values.mdv_id WHERE risk_factor_weight.deleted_flag = 0 ORDER BY risk_factor_weight.id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/risk-factor-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let factor_type_id = req.body.factor_type_id;
    let weight = req.body.weight;
    let weight_type = req.body.weight_type;
    let effective_date = req.body.effective_date;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE risk_factor_weight SET mdv_factor_type_id=?, weight=?, weight_type=?, effective_date=?, status=?, modified=?, modified_by=? WHERE id=?',

         [
            
            req.body.factor_type_id, req.body.weight, req.body.weight_type, req.body.effective_date, req.body.status,current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })

  /////////////////////////////
  /////DELETE MASTER VALUE/////
  ////////////////////////////

  app.post('/api/master-value-delete/', jwtMW,(req,res) => {
    con = require('../db');
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
     con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="'+current_user_id+'" WHERE type_id="'+type_id+'"', function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
                "status": 1
            }
            res.send(obj);
           
          }
     })
  })

  /////////////////////////////
  /////INSERT MASTER VALUE/////
  ////////////////////////////

  app.post('/api/risk-factor-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO risk_factor_weight (mdv_factor_type_id, weight, weight_type, effective_date, status, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?,?,?)',
          [
            
            req.body.risk_factor_type, req.body.risk_factor_weight, req.body.risk_factor_weight_type, req.body.effective_date, req.body.status,current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
               "status": 1,
               "last_value_id": result.insertId,
               "risk_factor_type": req.body.risk_factor_type,
               "risk_factor_weight": req.body.risk_factor_weight,
               "risk_factor_weight_type": req.body.risk_factor_weight_type,
               "effective_date": req.body.effective_date,
               "factor_status": req.body.status
            }
            res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
