/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {

  app.get('/api/loan-amount-list/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('select mdv_id, value AS loan_amount, description AS loan_amount_desc, risk_factor, status FROM master_data_values WHERE deleted_flag = 0 AND md_id = "Loan Amount" ORDER BY mdv_id DESC', function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      }
      else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////
  /////UPDATE USER TYPE/////
  /////////////////////////

  app.post('/api/loan-amount-update/', jwtMW, (req, res) => {
    con = require('../db');
    console.log(req.body)
    //return false
    let loan_amount = req.body.loan_amount;
    let risk_factor = req.body.risk_factor;
    let loan_amount_desc = req.body.loan_amount_desc;
    let status = req.body.status;
    let mdv_id = req.body.mdv_id;
    let current_user_id = req.body.current_user_id;

    con.query('UPDATE master_data_values SET value="' + loan_amount + '", description="' + loan_amount_desc + '", risk_factor="' + risk_factor + '", status="' + status + '", modified=NOW(), modified_by="' + current_user_id + '" WHERE mdv_id="' + mdv_id + '"', function (err, result) {
      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "message": "Term Month Updated Successfully."
        }
        res.send(obj);

      }
    })
  })

  //////////////////////////
  /////DELETE USER TYPE/////
  /////////////////////////

  /*app.post('/api/user-types-delete/', jwtMW, (req, res) => {
    con = require('../db');
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
    con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="' + current_user_id + '" WHERE type_id="' + type_id + '"', function (err, result) {
      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1
        }
        res.send(obj);

      }
    })
  })*/

  //////////////////////////
  /////INSERT USER TYPE/////
  /////////////////////////

  app.post('/api/loan-amount-insert/', jwtMW, (req, res) => {
    con = require('../db');

    console.log('req.body')
    console.log(req.body)

    //return false;
    let current_user_id = req.body.current_user_id;
    con.query('INSERT INTO master_data_values (md_id, value, description, status, risk_factor, created_by, modified_by, created, modified) VALUES("Loan Amount", "' + req.body.loan_amount + '", "' + req.body.loan_amount_desc + '", "' + req.body.status + '", "' + req.body.risk_factor + '", "' + current_user_id + '", "' + current_user_id + '", NOW(), NOW())', function (err, result) {

      if (err) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "last_type_id": result.insertId,
          "loan_amount": req.body.loan_amount,
          "risk_factor": req.body.risk_factor,
          "loan_amount_desc": req.body.loan_amount_desc,
          "loan_amount_status": req.body.status
        }
        res.send(obj);

      }
    })
  })

  //////////////////////////
  /////LAST INSERT ID USER TYPE/////
  /////////////////////////

  app.post('/api/loan-amount-check-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.mdv_id === undefined) {
      sql = 'SELECT mdv_id FROM master_data_values WHERE md_id=? AND value=?';
      var edit = 0;
    } else {
      sql = 'SELECT mdv_id FROM master_data_values WHERE md_id=? AND value=? AND mdv_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        'Loan Amount',
        req.body.value,
        req.body.mdv_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 1,
            "message": "Something wrong please try again."
          }
          res.send(obj);

        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })
  //other routes..
}
