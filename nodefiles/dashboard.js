/*
* Title: dashboard
* Descrpation :- This module blong to HPS and PRovider dashboard
* Date :-  Sep 03 2019
* Author :- Cognizsoft & Ramesh KUmar
con = require('../db');
*/
module.exports = function (app, jwtMW) {
    /*
    * This api cal for get credit application 
    */
    app.get('/api/provider-dashboard/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT credit_applications.status FROM credit_applications '
            + 'INNER JOIN patient_provider ON patient_provider.patient_id=credit_applications.patient_id '
            + 'WHERE patient_provider.provider_id = ?'
            ,
            [req.query.provider_id]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    let cond = 1;

                    cond = cond + ' AND YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW())';
                    con.query('SELECT mdv_invoice_status_id,(total_amt-total_hps_discount) as total_amt,check_amount FROM provider_invoice '
                        + 'WHERE provider_id = ? AND ' + cond
                        ,
                        [req.query.provider_id]
                        , function (error, invoiceStatus, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                let cond = 1;

                                cond = cond + ' AND YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW())';
                                con.query('SELECT '
                                    + 'refund_id, '
                                    + 'provider_invoice_id, '
                                    + 'refund_due, '
                                    + 'iou_flag '

                                    + 'FROM provider_refund WHERE provider_id = ? AND ' + cond
                                    , [req.query.provider_id]
                                    , function (error, refundStatus, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            con.query('SELECT '
                                                + 'cs_ticket.ticket_id, '
                                                //+ 'cs_ticket.ticket_number, '
                                                + 'cs_ticket.ticket_source AS ticket_source_id, '
                                                + 'cs_ticket.ac_ticket_flag, '
                                                + 'cs_ticket.app_ticket_flag, '
                                                + 'cs_ticket.plan_ticket_flag, '
                                                + 'cs_ticket.invoice_ticket_flag, '
                                                + 'cs_ticket.subject, '
                                                + 'cs_ticket.provider_id, '
                                                + 'cs_ticket.customer_id, '
                                                + 'cs_ticket.app_id, '
                                                + 'cs_ticket.pp_id, '
                                                + 'cs_ticket.cust_inv_id, '
                                                + 'cs_ticket.provider_inv_id, '
                                                + 'cs_ticket.status AS status_id, '

                                                + 'cs_ticket_details.caller_name, '
                                                + 'cs_ticket_details.incoming_call_flag, '
                                                //+ 'cs_ticket_details.phone_no, '
                                                + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
                                                + 'cs_ticket_details.description, '
                                                + 'cs_ticket_details.follow_up_flag, '
                                                + 'cs_ticket_details.follow_up_sch, '

                                                + 'credit_applications.application_no, '
                                                + 'payment_plan.plan_number, '
                                                + 'patient_invoice.invoice_number, '
                                                + 'provider_invoice.invoice_number AS provider_invoice_number, '

                                                + 'master_data_values.value AS ticket_source, '
                                                + 'master_data_values2.value AS status, '
                                                + '(SELECT follow_up_flag FROM cs_ticket_details WHERE ticket_id = cs_ticket.ticket_id ORDER BY cs_tkt_dtl_id DESC LIMIT 1) as follow_up_admin '
                                                + 'FROM cs_ticket '

                                                + 'LEFT JOIN cs_ticket_details '
                                                + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

                                                + 'LEFT JOIN credit_applications '
                                                + 'ON credit_applications.application_id = cs_ticket.app_id '

                                                + 'LEFT JOIN payment_plan '
                                                + 'ON payment_plan.pp_id = cs_ticket.pp_Id '

                                                + 'LEFT JOIN patient_invoice '
                                                + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

                                                + 'LEFT JOIN provider_invoice '
                                                + 'ON provider_invoice.provider_invoice_id = cs_ticket.provider_inv_id '

                                                + 'LEFT JOIN master_data_values '
                                                + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

                                                + 'LEFT JOIN master_data_values AS master_data_values2 '
                                                + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

                                                + 'WHERE cs_ticket.provider_id=? AND cs_ticket.status IN (?) '
                                                + 'GROUP BY cs_ticket.ticket_id '
                                                + 'ORDER BY cs_ticket.ticket_id'
                                                ,
                                                [
                                                    req.query.provider_id, [0, 1, 2]
                                                ]
                                                , function (error, tickets) {
                                                    if (error) {
                                                        var obj = {
                                                            "status": 1,
                                                            "application": rows,
                                                            "invoiceStatus": invoiceStatus,
                                                            "customerInvoice": '',
                                                            "refundStatus": refundStatus,
                                                            "customerSupport": ''
                                                        }
                                                        res.send(obj);
                                                    } else {
                                                        var obj = {
                                                            "status": 1,
                                                            "application": rows,
                                                            "invoiceStatus": invoiceStatus,
                                                            "customerInvoice": '',
                                                            "refundStatus": refundStatus,
                                                            "customerSupport": tickets,
                                                        }
                                                        res.send(obj);
                                                    }
                                                })
                                        }
                                    })

                            }
                        })

                }
            })
    })

    /*
    * This api cal for get credit application for hps
    */

    app.get('/api/dashboard/', jwtMW, (req, res) => {
        con = require('../db');

        con.query('SELECT credit_applications.status FROM credit_applications'
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    let cond = 1;

                    cond = cond + ' AND YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW())';
                    con.query('SELECT '
                        + 'mdv_invoice_status_id, '
                        + '(total_amt-total_hps_discount) AS total_amt, '
                        + 'check_amount '

                        + 'FROM provider_invoice WHERE ' + cond
                        , function (error, invoiceStatus, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {

                                let cond = 1;

                                cond = cond + ' AND YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW())';
                                con.query('SELECT '
                                    + 'refund_id, '
                                    + 'provider_invoice_id, '
                                    + 'refund_due, '
                                    + 'iou_flag '

                                    + 'FROM provider_refund WHERE ' + cond
                                    , function (error, refundStatus, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            let cond = 1;

                                            cond = cond + ' AND YEAR(due_date) = YEAR(NOW()) AND MONTH(due_date)=MONTH(NOW())';

                                            con.query('SELECT invoice_status as paid_flag FROM `patient_invoice` WHERE ' + cond
                                                , function (error, customerInvoice, fields) {
                                                    if (error) {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Something wrong please try again."
                                                        }
                                                        res.send(obj);
                                                    } else {
                                                        con.query('SELECT '
                                                            + 'cs_ticket.ticket_id, '
                                                            //+ 'cs_ticket.ticket_number, '
                                                            + 'cs_ticket.ticket_source AS ticket_source_id, '
                                                            + 'cs_ticket.ac_ticket_flag, '
                                                            + 'cs_ticket.app_ticket_flag, '
                                                            + 'cs_ticket.plan_ticket_flag, '
                                                            + 'cs_ticket.invoice_ticket_flag, '
                                                            + 'cs_ticket.subject, '
                                                            + 'cs_ticket.provider_id, '
                                                            + 'cs_ticket.customer_id, '
                                                            + 'cs_ticket.app_id, '
                                                            + 'cs_ticket.pp_id, '
                                                            + 'cs_ticket.cust_inv_id, '
                                                            + 'cs_ticket.provider_inv_id, '
                                                            + 'cs_ticket.status AS status_id, '

                                                            + 'cs_ticket_details.caller_name, '
                                                            + 'cs_ticket_details.incoming_call_flag, '
                                                            //+ 'cs_ticket_details.phone_no, '
                                                            + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
                                                            + 'cs_ticket_details.description, '
                                                            + 'cs_ticket_details.follow_up_flag, '
                                                            + 'cs_ticket_details.follow_up_sch, '

                                                            + 'credit_applications.application_no, '
                                                            + 'payment_plan.plan_number, '
                                                            + 'patient_invoice.invoice_number, '
                                                            + 'provider_invoice.invoice_number AS provider_invoice_number, '

                                                            + 'master_data_values.value AS ticket_source, '
                                                            + 'master_data_values2.value AS status, '
                                                            + '(SELECT follow_up_flag FROM cs_ticket_details WHERE ticket_id = cs_ticket.ticket_id ORDER BY cs_tkt_dtl_id DESC LIMIT 1) as follow_up_admin '
                                                            + 'FROM cs_ticket '

                                                            + 'LEFT JOIN cs_ticket_details '
                                                            + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

                                                            + 'LEFT JOIN credit_applications '
                                                            + 'ON credit_applications.application_id = cs_ticket.app_id '

                                                            + 'LEFT JOIN payment_plan '
                                                            + 'ON payment_plan.pp_id = cs_ticket.pp_Id '

                                                            + 'LEFT JOIN patient_invoice '
                                                            + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

                                                            + 'LEFT JOIN provider_invoice '
                                                            + 'ON provider_invoice.provider_invoice_id = cs_ticket.provider_inv_id '

                                                            + 'LEFT JOIN master_data_values '
                                                            + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

                                                            + 'LEFT JOIN master_data_values AS master_data_values2 '
                                                            + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

                                                            + 'WHERE cs_ticket.status IN (?) '
                                                            + 'GROUP BY cs_ticket.ticket_id '
                                                            + 'ORDER BY cs_ticket.ticket_id DESC'
                                                            ,
                                                            [
                                                                [0, 1, 2]
                                                            ]
                                                            , function (error, tickets) {
                                                                if (error) {
                                                                    var obj = {
                                                                        "status": 1,
                                                                        "application": rows,
                                                                        "invoiceStatus": invoiceStatus,
                                                                        "customerInvoice": customerInvoice,
                                                                        "refundStatus": refundStatus,
                                                                        "customerSupport": ''
                                                                    }
                                                                    res.send(obj);
                                                                } else {
                                                                    var obj = {
                                                                        "status": 1,
                                                                        "application": rows,
                                                                        "invoiceStatus": invoiceStatus,
                                                                        "customerInvoice": customerInvoice,
                                                                        "refundStatus": refundStatus,
                                                                        "customerSupport": tickets,
                                                                    }
                                                                    res.send(obj);
                                                                }
                                                            })

                                                    }
                                                })
                                        }
                                    })

                            }
                        })

                }
            })
    })
}