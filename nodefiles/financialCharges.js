/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/financial-charges-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Financial Charges' AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/financial-charges/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT financial_charges.id, financial_charges.charge, financial_charges.status, financial_charges.mdv_financial_charges_type AS financial_charges_type_id, master_data_values.value AS financial_charges_type FROM financial_charges INNER JOIN master_data_values ON financial_charges.mdv_financial_charges_type = master_data_values.mdv_id WHERE financial_charges.deleted_flag = 0 ORDER BY financial_charges.id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE PAYBACK/////
  ////////////////////////////

  app.post('/api/financial-charges-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let payback_id = req.body.payback_id;
    let min_volume = req.body.min_volume;
    let max_volume = req.body.max_volume;
    let rate = req.body.rate;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE financial_charges SET mdv_financial_charges_type=?, charge=?, status=?, date_modified=?, modified_by=? WHERE id=?',

         [
            
            req.body.financial_charges_type_id, req.body.charge, req.body.status, current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })


  /////////////////////////////
  /////INSERT PAYBACK/////
  ////////////////////////////

  app.post('/api/financial-charges-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO financial_charges (mdv_financial_charges_type, charge, status, created_by, modified_by, date_created, date_modified) VALUES(?,?,?,?,?,?,?)',
          [
            
            req.body.financial_charges_type, req.body.charge, req.body.status, current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
               "status": 1,
               "last_value_id": result.insertId,
               "financial_charges_type": req.body.financial_charges_type,
               "charge": req.body.charge,
               "fc_status": req.body.status
            }
            res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
