/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/payback-master-data-value/', jwtMW,(req,res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Payback Type' AND deleted_flag = 0 AND status = 1", function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/payback/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT payback.id, payback.min_volume, payback.max_volume, payback.rate, payback.status, payback.mdv_discount_type_id AS payback_id, master_data_values.value AS payback_type FROM payback INNER JOIN master_data_values ON payback.mdv_discount_type_id = master_data_values.mdv_id WHERE payback.deleted_flag = 0 ORDER BY payback.id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE PAYBACK/////
  ////////////////////////////

  app.post('/api/payback-update/', jwtMW,(req,res) => {
    con = require('../db');
    
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let payback_id = req.body.payback_id;
    let min_volume = req.body.min_volume;
    let max_volume = req.body.max_volume;
    let rate = req.body.rate;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE payback SET mdv_discount_type_id=?, min_volume=?, max_volume=?, rate=?, status=?, modified=?, modified_by=? WHERE id=?',

         [
            
            req.body.payback_id, req.body.min_volume, req.body.max_volume, req.body.rate, req.body.status, current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
              "status": 1
            }
            res.send(obj);
           
          }
     })
  })


  /////////////////////////////
  /////INSERT PAYBACK/////
  ////////////////////////////

  app.post('/api/payback-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO payback (mdv_discount_type_id, min_volume, max_volume, rate, status, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?,?,?)',
          [
            
            req.body.payback_type, req.body.min_volume, req.body.max_volume, req.body.rate, req.body.status, current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
               "status": 1,
               "last_value_id": result.insertId,
               "payback_type": req.body.payback_type,
               "min_volume": req.body.min_volume,
               "max_volume": req.body.max_volume,
               "rate": req.body.rate,
               "payback_status": req.body.status
            }
            res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
