var customerInvoiceReports = function (con, callback) {
    var obj = {}
    con.query('SELECT '
        + 'pp_installments.pp_installment_id,pp_installments.installment_amt,DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, pp_installments.paid_flag,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,regions.name,states.name as state_name '
        + 'FROM pp_installments '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        + 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
        + 'INNER JOIN states ON states.state_id=regions.state_id '
        + 'WHERE WEEKOFYEAR(pp_installments.due_date)=WEEKOFYEAR(NOW()) '
        + 'ORDER BY pp_installments.date_modified DESC'
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

var customerInvoiceReportsFilter = function (con, data, callback) {
    let cond = 'invoicestatus.md_id="Customer Invoice Status"';
    if (data.invoice_due_start_date != '') {
        cond = cond + ' AND patient_invoice.due_date >="' + data.invoice_due_start_date + '"';
    }
    if (data.invoice_due_end_date != '') {
        cond = cond + ' AND patient_invoice.due_date <="' + data.invoice_due_end_date + '"';
    }

    if (data.invoice_start_date != '') {
        cond = cond + ' AND patient_invoice.payment_date >="' + data.invoice_start_date + '"';
    }
    if (data.invoice_end_date != '') {
        cond = cond + ' AND patient_invoice.payment_date <="' + data.invoice_end_date + '"';
    }
    if (data.invoice_number != '') {
        cond = cond + ' AND patient_invoice.invoice_number ="' + data.invoice_number + '"';
    }

    /*if (data.short_by == 1) {
        cond = cond + ' AND WEEKOFYEAR(patient_invoice.due_date)=WEEKOFYEAR(NOW())';
    } else {
        cond = cond + ' AND YEAR(patient_invoice.due_date) = YEAR(NOW()) AND MONTH(patient_invoice.due_date)=MONTH(NOW())';
    }*/

    if (data.status != '' && data.status != 'all') {
        cond = cond + ' AND patient_invoice.invoice_status ="' + data.status + '"';
    }


    if (data.customer_name != '') {
        cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
    }

    //cond = cond + ' ORDER BY patient_invoice.due_date, patient_invoice.payment_date DESC'; 

    var obj = {}
    con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number, '
        + 'patient_invoice.payment_amount,patient_invoice.paid_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_due,patient_invoice.fin_charge_received,patient_invoice.previous_fin_charge,patient_invoice.previous_late_fee,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,'
        //+ ',(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)+IFNULL(patient_invoice.previous_fin_charge,0)+IFNULL(patient_invoice.previous_late_fee,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'invoicestatus.value as invoice_status_name, '
        + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.invoice_status '
        + 'FROM patient_invoice '

        + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '

        + 'WHERE ' + cond
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.due_date DESC'
        , function (error, rows, fields) {
            
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                con.query('SELECT '
                    + 'value, '
                    + 'status_id '

                    + 'FROM master_data_values '
                    + 'WHERE md_id = ?'
                    , ['Customer Invoice Status']
                    , function (err, invoice_status) {
                        if (err) {
                            obj.status = 0;
                            obj.result = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            obj.result = rows;
                            obj.inv_status = invoice_status;
                            return callback(obj);
                        }
                    })
            }
        })

}

var getPrintDetails = function (con, data, callback) {
    //console.log(data);

    var obj = {}

    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT application_id,invoice_id,invoice_number	,payment_amount,additional_amount,late_fee_received,late_fee_due,DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date, DATE_FORMAT(due_date, "%m/%d/%Y") as due_date, '
            + 'payment_method, bank_name, txn_ref_no, ach_bank_name, ach_routing_no	,ach_account_no, fin_charge_received, fin_charge_due,invoice_status, previous_late_fee, previous_fin_charge '
            + 'FROM patient_invoice '
            + 'WHERE invoice_id IN (?)',
            [
                data
            ]
            , function (err, resultmain) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.888";
                        return callback(obj);
                    });
                } else {

                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.66";
                            return callback(obj);
                        });
                    } else {
                        con.query('SELECT patient_invoice_details.invoice_id, payment_plan.pp_id,payment_plan.monthly_amount as installment_amt, patient_invoice_details.invoice_amount,  '
                            + 'payment_plan.plan_number,payment_plan.discounted_interest_rate,payment_plan.amount,payment_plan.term as term_month,payment_plan.loan_amount,payment_plan.remaining_amount, '
                            + 'credit_applications.application_id,provider.name as provider_name,patient.patient_id,patient.patient_ac, patient.f_name, patient.m_name, patient.l_name, patient.gender, patient.email, patient.peimary_phone, patient.status, '
                            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
                            + 'payment_plan.missed_count as missed_payment, '
                            + 'payment_plan.late_count as late_payment, co_signer.f_name as co_f_name, co_signer.m_name as co_m_name, co_signer.l_name as co_l_name, co_signer.peimary_phone as co_peimary_phone, credit_app_cosigner.primary_bill_pay_flag, '
                            + 'co_patient_address.address1 as co_address1,co_patient_address.address2 as co_address2,co_patient_address.City as co_city,co_patient_address.zip_code as co_zip_code,co_states.name as co_state_name '
                            + 'FROM patient_invoice_details '
                            + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
                            + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
                            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.billing_address=1 AND patient_address.status=1 '
                            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                            //+ 'INNER JOIN interest_rate ON interest_rate.id=payment_plan.interest_rate_id '
                            //+ 'INNER JOIN master_data_values ON payment_plan.term_id=master_data_values.mdv_id '
                            //+ 'INNER JOIN patient_provider ON patient_provider.patient_id=patient.patient_id '
                            + 'INNER JOIN provider ON provider.provider_id=payment_plan.provider_id '
                            + 'LEFT JOIN credit_app_cosigner ON credit_app_cosigner.application_id=credit_applications.application_id '
                            + 'LEFT JOIN co_signer ON co_signer.co_signer_id = credit_app_cosigner.co_signer_id '
                            + 'LEFT JOIN patient_address as co_patient_address ON co_patient_address.co_signer_id=co_signer.co_signer_id AND co_patient_address.billing_address=1 AND co_patient_address.status=1 '
                            + 'LEFT JOIN states as co_states ON co_states.state_id=co_patient_address.state_id '
                            + 'WHERE invoice_id IN (?)',
                            [
                                data
                            ]
                            , function (child, childResult) {
                                const uniqueID = [...new Set(childResult.map(item => item.application_id))];

                                if (child) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again111111111.";
                                        return callback(obj);
                                    });
                                } else {
                                    con.query('SELECT '
                                        + 'value,md_id '
                                        + 'FROM master_data_values '
                                        + 'WHERE status = ? AND (md_id = ? OR md_id = ? OR md_id = ?)'
                                        , [1, 'Late Fee', 'Financial Charges', 'Credit Charge']
                                        , function (lateErr, late_fee_rows, fields) {

                                            if (lateErr) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.6666666";
                                                    return callback(obj);
                                                });
                                            } else {
                                                con.query('SELECT '
                                                    + 'patient_invoice.application_id,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS last_date,DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") AS due_date, patient_invoice.invoice_status, '
                                                    + 'patient_invoice.paid_amount,patient_invoice.additional_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_received,patient_invoice.fin_charge_due,patient_invoice.previous_late_fee,patient_invoice.previous_fin_charge '

                                                    + 'FROM patient_invoice '
                                                    //+ 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id '
                                                    //+ 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '

                                                    //+ 'WHERE payment_plan.application_id IN(?) AND (patient_invoice.invoice_status=? OR patient_invoice.invoice_status=?) ORDER BY patient_invoice.invoice_id DESC'
                                                    //, [uniqueID, 1, 4]
                                                    + 'WHERE patient_invoice.application_id IN(?) ORDER BY patient_invoice.invoice_id DESC'
                                                    , [uniqueID]
                                                    , function (missError, LastPayment, fields) {
                                                        if (missError) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.55555555";
                                                                return callback(obj);
                                                            });
                                                        } else {
                                                            con.query('SELECT pp_id,application_id,amount FROM payment_plan WHERE payment_plan.application_id IN(?)'
                                                                , [uniqueID]
                                                                , function (missPlan, planDetails, fields) {

                                                                    if (missPlan) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Something wrong please try again.4444444";
                                                                            return callback(obj);
                                                                        });
                                                                    } else {
                                                                        const uniquePlanID = [...new Set(planDetails.map(item => item.pp_id))];

                                                                        con.query('SELECT invoice_installments.pp_id,invoice_installments.amount_rcvd,payment_plan.application_id '
                                                                            + 'FROM invoice_installments '
                                                                            + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=invoice_installments.pp_installment_id '
                                                                            + 'INNER JOIN payment_plan ON pp_installments.pp_id=payment_plan.pp_id '
                                                                            + 'WHERE invoice_installments.pp_id IN(?) AND (pp_installments.paid_flag=0 OR pp_installments.partial_paid=0)'
                                                                            , [uniquePlanID, 1, 1]
                                                                            , function (missAmt, amountDetails, fields) {

                                                                                if (missAmt) {
                                                                                    con.rollback(function () {
                                                                                        obj.status = 0;
                                                                                        obj.message = "Something wrong please try again222.";
                                                                                        return callback(obj);
                                                                                    });
                                                                                } else {
                                                                                    con.query('SELECT count(invoice_id) as total_miss '
                                                                                        + 'FROM patient_invoice '
                                                                                        + 'WHERE application_id = ? AND DATE_FORMAT(due_date, "%m/%d/%Y") < ? AND invoice_status IN (2,3)',
                                                                                        [
                                                                                            resultmain[0].application_id,
                                                                                            resultmain[0].due_date
                                                                                        ]
                                                                                        , function (errMiss, resultmiss) {
                                                                                            if (errMiss) {
                                                                                                con.rollback(function () {
                                                                                                    obj.status = 0;
                                                                                                    obj.message = "Something wrong please try again.";
                                                                                                    return callback(obj);
                                                                                                });
                                                                                            } else {
                                                                                                con.commit(function (err) {
                                                                                                    obj.status = 1;
                                                                                                    obj.childResult = childResult;
                                                                                                    obj.resultmain = resultmain;
                                                                                                    obj.lateFee = (Object.keys(late_fee_rows).length > 0) ? late_fee_rows : '';
                                                                                                    obj.LastPayment = LastPayment;
                                                                                                    obj.planDetails = planDetails;
                                                                                                    obj.amountDetails = amountDetails;
                                                                                                    obj.MissCount = (resultmiss.length > 0)?resultmiss[0].total_miss : 0;
                                                                                                    return callback(obj);
                                                                                                })
                                                                                            }
                                                                                        })
                                                                                }
                                                                            })
                                                                    }
                                                                })

                                                        }
                                                    })
                                            }
                                        });

                                }
                            })
                    }

                }
            });
    })

    /*con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number ,(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, patient_invoice.paid_flag '
        + 'FROM patient_invoice '

        + 'INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=pp_installment_invoice.pp_installment_id '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'WHERE ' + cond
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC'
        , function (error, rows, fields) {
            //console.log(this.sql)
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })*/

}

var customerWithdrawalReportsFilter = function (con, data, callback) {
    let cond = 'invoicestatus.md_id="Customer Invoice Status"';
    /*if (data.invoice_due_start_date != '') {
        cond = cond + ' AND patient_invoice.due_date >="' + data.invoice_due_start_date + '"';
    }
    if (data.invoice_due_end_date != '') {
        cond = cond + ' AND patient_invoice.due_date <="' + data.invoice_due_end_date + '"';
    }*/

    if (data.invoice_start_date != '') {
        //cond = cond + ' AND patient_invoice.payment_date >="' + data.invoice_start_date + '"';
        cond = cond + ' AND patient.withdrawal_date>=DATE_FORMAT("' + data.invoice_start_date + '", "%e")';
        cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") >=DATE_FORMAT("' + data.invoice_start_date + '", "%m/%Y")';

    } else {
        cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") >=DATE_FORMAT(now(), "%m/%Y")';
    }
    if (data.invoice_end_date != '') {
        //cond = cond + ' AND patient_invoice.payment_date <="' + data.invoice_end_date + '"';
        cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") <=DATE_FORMAT("' + data.invoice_end_date + '", "%m/%Y")';
        cond = cond + ' AND patient.withdrawal_date<=DATE_FORMAT("' + data.invoice_end_date + '", "%e")';
    } else {
        cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") <=DATE_FORMAT(now(), "%m/%Y")';
    }

    /*if (data.withdrawal_day != '') {
        cond = cond + ' AND patient.withdrawal_date=' + data.withdrawal_day;
    }

    if (data.status != '') {
        cond = cond + ' AND patient_invoice.paid_flag ="' + data.status + '"';
    }


    if (data.customer_name != '') {
        cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
    }*/

    var obj = {}
    con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number, '
        + 'patient_invoice.payment_amount,patient_invoice.paid_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_received,patient_invoice.fin_charge_due	,patient_invoice.previous_fin_charge,patient_invoice.previous_late_fee,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,'
        //+ ',(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)+IFNULL(patient_invoice.previous_fin_charge,0)+IFNULL(patient_invoice.previous_late_fee,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone,patient.withdrawal_date, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'invoicestatus.value as invoice_status_name, '
        + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.invoice_status '
        + 'FROM patient_invoice '

        + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '

        + 'WHERE ' + cond
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC'
        , function (error, rows, fields) {

            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

var customerProcedureReportsFilter = function (con, data, callback) {
    let cond = ' master_data_values.md_id="Customer Plan Status" AND patient_procedure.procedure_status>0 AND patient_procedure.pp_id NOT IN (SELECT provider_invoice_detial.pp_id FROM provider_invoice_detial WHERE status=0)';
    if (data.id !== undefined && data.id != '') {
        cond = cond + ' AND patient_procedure.pp_id=' + data.id;
    }
    if (data.invoice_start_date !== undefined && data.invoice_start_date != '') {
        cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") >=DATE_FORMAT("' + data.invoice_start_date + '", "%m/%d/%Y")';

    } else {
        //cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") >=DATE_FORMAT(now(), "%m/%Y")';
    }
    if (data.invoice_end_date !== undefined && data.invoice_end_date != '') {
        cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") <=DATE_FORMAT("' + data.invoice_end_date + '", "%m/%d/%Y")';
    } else {
        //cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") <=DATE_FORMAT(now(), "%m/%Y")';
    }
    if (data.provider_name !== undefined && data.provider_name != '') {
        cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"'
        //cond = cond + ' AND provider.provider_name <= '+data.invoice_end_date;
    } else {
        //cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%m/%Y") <=DATE_FORMAT(now(), "%m/%Y")';
    }

    var obj = {}
    con.query('SELECT '
        + 'provider.provider_id,provider.name,provider.phone,provider.email,provider.provider_ac, '
        //+ 'provider_location.address1, '
        //+ 'provider_location.address2, '
        //+ 'provider_location.city, '
        //+ 'provider_location.zip_code, 
        //+ 'states.name as state_name, '

        + 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") as procedure_date,patient_procedure.pp_id, '

        + 'payment_plan.amount, '
        + 'payment_plan.loan_amount, '
        + 'payment_plan.plan_number, '

        + 'credit_applications.patient_id, '

        + 'patient.f_name AS customer_f_name, '
        + 'patient.m_name AS customer_m_name, '
        + 'patient.l_name AS customer_l_name, '

        + 'patient_address.address1, '
        + 'patient_address.City AS city, '
        + 'patient_address.state_id, '
        + 'patient_address.zip_code, '

        + 'state2.name AS state_name, '

        + 'master_data_values.value AS plan_status '
        + 'FROM patient_procedure '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_procedure.pp_id  '

        + 'INNER JOIN credit_applications '
        + 'ON credit_applications.application_id=payment_plan.application_id '

        + 'INNER JOIN patient '
        + 'ON patient.patient_id=credit_applications.patient_id '

        + 'INNER JOIN patient_address '
        + 'ON patient_address.patient_id=patient.patient_id '

        + 'INNER JOIN states AS state2 '
        + 'ON state2.state_id=patient_address.state_id '

        + 'INNER JOIN master_data_values ON master_data_values.status_id=payment_plan.plan_status  '

        + 'INNER JOIN provider ON provider.provider_id=patient_procedure.provider_id  '
        //+ 'INNER JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
        //+ 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'WHERE ' + cond
        + ' ORDER BY patient_procedure.procedure_date DESC'
        , function (error, rows, fields) {
            
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

exports.customerInvoiceReports = customerInvoiceReports;
exports.customerInvoiceReportsFilter = customerInvoiceReportsFilter;
exports.getPrintDetails = getPrintDetails;
exports.customerWithdrawalReportsFilter = customerWithdrawalReportsFilter;
exports.customerProcedureReportsFilter = customerProcedureReportsFilter;