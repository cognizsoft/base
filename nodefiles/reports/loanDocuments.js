module.exports = (dataFilter, data, logoimg) => {

   let moment = require('moment');
   let fileName = '';
   if (dataFilter.filter_type == 1) {
      moment.updateLocale('en', {
         week: {
            dow: 1,
            doy: 1
         }
      });

      let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
      let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
      fileName = 'Weelky Loan Report (' + startOfWeek + ' - ' + endOfWeek + ')';
   } else if (dataFilter.filter_type == 2) {
      let monthName = moment().month(dataFilter.month-1).format("MMM");
      fileName = 'Monthly Loan Report (' + monthName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 3) {
      let queterName = ''
      if(dataFilter.quarter == 1){
         queterName = 'Q1';
      } else if(dataFilter.quarter == 2){
         queterName = 'Q2';
      } else if(dataFilter.quarter == 3){
         queterName = 'Q3';
      } else if(dataFilter.quarter == 4){
         queterName = 'Q4';
      }
      fileName = 'Quarterly Loan Report (' + queterName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 4) {
      fileName = 'Yearly Loan Report (' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 5) {
      let currentDate = moment().format('MM/DD/YYYY');
      fileName = 'Year to Date Loan Report (01/01/' + dataFilter.year + '-'+ currentDate +')';
   } else if (dataFilter.filter_type == 6) {
      let loanStart = moment(dataFilter.loan_start_date).format('MM/DD/YYYY');
      let loanEnd = moment(dataFilter.loan_end_date).format('MM/DD/YYYY');
      fileName = 'By Date Loan Report (' + loanStart + '-'+ loanEnd +')';
   }
   var html = '';
   let total_loan_amount = 0.00;
   let total_late_fee = 0.00;
   let total_finance_charge = 0.00;
   let total_payment_amt2 = 0.00;
   let total_blc_amt = 0.00;
   let total_principal_paid = 0.00;

   let mrtAmt = 0.00;
   let intAmt = 0.00;
   let intRecvd = 0.00;
   data.result.forEach(function (element, idx) {
      element.main_amount = (element.main_amount) ? element.main_amount : 0;
      element.late_fee = (element.late_fee) ? element.late_fee : 0;
      element.fin_fee = (element.fin_fee) ? element.fin_fee : 0;
      element.total_payment_amt = (element.total_payment_amt) ? element.total_payment_amt : 0;
      element.total_payment_amt = (element.total_payment_amt) ? element.total_payment_amt : 0;

      total_loan_amount = total_loan_amount + element.main_amount;
      total_late_fee = total_late_fee + element.late_fee;
      total_finance_charge = total_finance_charge + element.fin_fee;
      total_payment_amt2 = total_payment_amt2 + element.total_payment_amt + element.total_additional_amt;
      //total_blc_amt = total_blc_amt + element.total_payment_amt;
      var blc = element.remaining_amount;
      //console.log(element.loan_amount)
      //console.log(element.total_payment_amt)

      total_principal_paid = total_principal_paid + (element.principal_paid + element.total_additional_amt);

      total_blc_amt = total_blc_amt + blc;

      let FinCharge = (element.fin_fee) ? element.fin_fee : 0;
      let lateFee = (element.late_fee) ? element.late_fee : 0
      let prinRecd = (element.principal_paid) ? element.principal_paid : 0;
      let AddAmt = (element.total_additional_amt) ? element.total_additional_amt : 0;
      let totalAmt = (element.total_payment_amt) ? element.total_payment_amt : 0;


      mrtAmt = mrtAmt + element.amount;
      intAmt = intAmt + (element.amount - element.main_amount);
      intRecvd = intRecvd + ((totalAmt + AddAmt) - (prinRecd + AddAmt + FinCharge + lateFee));
      html = html + '<tr>'
         + '<td>' + element.plan_number + '</td>'
         + '<td>' + element.name + '</td>'
         + '<td>' + element.f_name + ' ' + element.m_name + ' ' + element.l_name + '</td>'
         + '<td>' + element.loan_date + '</td>'
         + '<td>$' + element.main_amount.toFixed(2) + '</td>'
         + '<td>' + element.maturity_date + '</td>'
         + '<td>$' + element.amount.toFixed(2) + '</td>'
         + '<td>$' + (element.amount - element.main_amount).toFixed(2) + '</td>'
         + '<td>$' + element.late_fee.toFixed(2) + '</td>'
         + '<td>$' + element.fin_fee.toFixed(2) + '</td>'
         + '<td>$' + element.remaining_amount.toFixed(2) + '</td>'
         + '<td>$' + (element.principal_paid + element.total_additional_amt).toFixed(2) + '</td>'
         + '<td>$' + ((totalAmt + AddAmt) - (prinRecd + AddAmt + FinCharge + lateFee)).toFixed(2) + '</td>'
         + '<td>$' + (element.total_payment_amt + element.total_additional_amt).toFixed(2) + '</td>'
         + '<td>' + element.p_status + '</td>'
         + '</tr>';
   });
   if (data.result.length > 0) {
      html = html + '<tr>'
         + '<td><b>Total</b></td>'
         + '<td></td>'
         + '<td></td>'
         + '<td></td>'
         + '<td><b>$' + total_loan_amount.toFixed(2) + '</b></td>'
         + '<td></td>'
         + '<td><b>$' + mrtAmt.toFixed(2) + '</b></td>'
         + '<td><b>$' + intAmt.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_late_fee.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_finance_charge.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_blc_amt.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_principal_paid.toFixed(2) + '</b></td>'
         + '<td><b>$' + intRecvd.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_payment_amt2.toFixed(2) + '</b></td>'
         + '<td></td>'
         + '</tr>';
   }else{
      html = html + '<tr>'
         + '<td colspan="15">Record not found.</td>'
         + '</tr>';
   }

   return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>Loan Reports</title>
       <style>
            html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 12px;
          line-height: 16px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
       .detail-table td{padding:5px 2px; font-size:10px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">${fileName}</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>Loan No</th>
                  <th>Provider Name</th>
                  <th>Customer Name</th>
                  <th>Loan Date</th>
                  <th>Prin. Amt</th>
                  <th>Maturity Date</th>
                  <th>Maturity Amt</th>
                  <th>Int. Amt</th>                  
                  <th>Late Fee</th>
                  <th>Fin Charge</th>
                  <th>Prin. Bal.</th>
                  <th>Prin. Recvd</th>
                  <th>Interest recvd</th>
                  <th>Total Amt Recvd</th>
                  <th>Status</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};