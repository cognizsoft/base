var customerInvoiceReports = function (con, callback) {
    var obj = {}
    con.query('SELECT '
        + 'pp_installments.pp_installment_id,pp_installments.installment_amt,DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, pp_installments.paid_flag,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,regions.name,states.name as state_name '
        + 'FROM pp_installments '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        + 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
        + 'INNER JOIN states ON states.state_id=regions.state_id '
        + 'WHERE WEEKOFYEAR(pp_installments.due_date)=WEEKOFYEAR(NOW()) '
        + 'ORDER BY pp_installments.date_modified DESC'
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

var customerInvoiceReportsFilter = function (con, data, callback) {
    let cond = 'invoicestatus.md_id="Customer Invoice Status"';
    if (data.invoice_due_start_date != '') {
        cond = cond + ' AND patient_invoice.due_date >="' + data.invoice_due_start_date + '"';
    }
    if (data.invoice_due_end_date != '') {
        cond = cond + ' AND patient_invoice.due_date <="' + data.invoice_due_end_date + '"';
    }

    if (data.invoice_start_date != '') {
        cond = cond + ' AND patient_invoice.payment_date >="' + data.invoice_start_date + '"';
    }
    if (data.invoice_end_date != '') {
        cond = cond + ' AND patient_invoice.payment_date <="' + data.invoice_end_date + '"';
    }

    /*if (data.short_by == 1) {
        cond = cond + ' AND WEEKOFYEAR(patient_invoice.due_date)=WEEKOFYEAR(NOW())';
    } else {
        cond = cond + ' AND YEAR(patient_invoice.due_date) = YEAR(NOW()) AND MONTH(patient_invoice.due_date)=MONTH(NOW())';
    }*/

    if (data.status != '') {
        cond = cond + ' AND patient_invoice.invoice_status ="' + data.status + '"';
    }


    if (data.customer_name != '') {
        cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
    }

    var obj = {}
    con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number, '
        + 'patient_invoice.payment_amount,patient_invoice.paid_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_due,patient_invoice.fin_charge_received,patient_invoice.previous_fin_charge,patient_invoice.previous_late_fee,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,'
        //+ ',(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)+IFNULL(patient_invoice.previous_fin_charge,0)+IFNULL(patient_invoice.previous_late_fee,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'invoicestatus.value as invoice_status_name, '
        + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.invoice_status '
        + 'FROM patient_invoice '

        + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN user ON user.patient_id = patient.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '

        
        + 'WHERE ' + cond
        + ' AND user.user_id = ?'
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC'
        , [data.current_user_id]
        , function (error, rows, fields) {
            //console.log(this.sql)
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

var getPrintDetails = function (con, data, callback) {
    //console.log(data);

    var obj = {}

    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT invoice_id,invoice_number	,payment_amount,additional_amount,late_fee_received,DATE_FORMAT(payment_date, "%m/%d/%Y") as payment_date, '
            + 'payment_method, bank_name, txn_ref_no, ach_bank_name, ach_routing_no	,ach_account_no, fin_charge_amt, paid_flag, previous_late_fee, previous_fin_charge '
            + 'FROM patient_invoice '
            + 'WHERE invoice_id IN (?)',
            [
                data
            ]
            , function (err, resultmain) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        return callback(obj);
                    });
                } else {

                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            return callback(obj);
                        });
                    } else {
                        con.query('SELECT invoice_installments.invoice_id, pp_installments.pp_id,pp_installments.installment_amt,  DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, '
                            + 'payment_plan.plan_number,payment_plan.discounted_interest_rate,payment_plan.amount,master_data_values.value as term_month, '
                            + 'credit_applications.application_id,provider.name as provider_name,patient.patient_id,patient.patient_ac, patient.f_name, patient.m_name, patient.l_name, patient.gender, patient.email, patient.peimary_phone, patient.status, '
                            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
                            + '(SELECT SUM(missed_flag) FROM pp_installments WHERE pp_id=payment_plan.pp_id) as missed_payment, '
                            + '(SELECT SUM(late_flag) FROM pp_installments WHERE pp_id=payment_plan.pp_id) as late_payment '
                            + 'FROM invoice_installments '
                            + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=invoice_installments.pp_installment_id '
                            + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
                            + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
                            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                            + 'INNER JOIN interest_rate ON interest_rate.id=payment_plan.interest_rate_id '
                            + 'INNER JOIN master_data_values ON interest_rate.mdv_payment_term_month=master_data_values.mdv_id '
                            + 'INNER JOIN patient_provider ON patient_provider.patient_id=patient.patient_id '
                            + 'INNER JOIN provider ON provider.provider_id=patient_provider.provider_id '
                            + 'WHERE invoice_id IN (?)',
                            [
                                data
                            ]
                            , function (child, childResult) {

                                const uniqueID = [...new Set(childResult.map(item => item.application_id))];

                                if (child) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        return callback(obj);
                                    });
                                } else {
                                    con.query('SELECT '
                                        + 'value,md_id '
                                        + 'FROM master_data_values '
                                        + 'WHERE status = ? AND (md_id = ? OR md_id = ?)'
                                        , [1, 'Late Fee', 'Financial Charges']
                                        , function (lateErr, late_fee_rows, fields) {

                                            if (lateErr) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    return callback(obj);
                                                });
                                            } else {
                                                con.query('SELECT '
                                                    + 'payment_plan.application_id,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") AS last_date, '
                                                    + 'patient_invoice.paid_amount,patient_invoice.additional_amount,patient_invoice.late_fee_received,patient_invoice.fin_charge_amt,patient_invoice.previous_late_fee,patient_invoice.previous_fin_charge,'
                                                    + 'lateFee.value as latepct,finChrg.value as finpct '
                                                    + 'FROM patient_invoice '
                                                    + 'INNER JOIN invoice_installments ON invoice_installments.invoice_id=patient_invoice.invoice_id '
                                                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=invoice_installments.pp_id '
                                                    + 'LEFT JOIN late_fee_waivers ON late_fee_waivers.id=patient_invoice.late_fee_waivers_id '
                                                    + 'LEFT JOIN master_data_values as lateFee ON late_fee_waivers.mdv_waiver_type_id=lateFee.mdv_id '
                                                    + 'LEFT JOIN late_fee_waivers as late2 ON late2.id=patient_invoice.fin_charge_waiver_id '
                                                    + 'LEFT JOIN master_data_values as finChrg ON late2.mdv_waiver_type_id=finChrg.mdv_id '
                                                    + 'WHERE payment_plan.application_id IN(?) AND patient_invoice.paid_flag=?'
                                                    , [uniqueID, 1]
                                                    , function (missError, LastPayment, fields) {

                                                        if (missError) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.";
                                                                return callback(obj);
                                                            });
                                                        } else {
                                                            con.query('SELECT pp_id,application_id,amount FROM payment_plan WHERE payment_plan.application_id IN(?)'
                                                                , [uniqueID]
                                                                , function (missPlan, planDetails, fields) {

                                                                    if (missPlan) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Something wrong please try again.";
                                                                            return callback(obj);
                                                                        });
                                                                    } else {
                                                                        const uniquePlanID = [...new Set(planDetails.map(item => item.pp_id))];

                                                                        con.query('SELECT invoice_installments.pp_id,invoice_installments.amount_rcvd,payment_plan.application_id '
                                                                            + 'FROM invoice_installments '
                                                                            + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=invoice_installments.pp_installment_id '
                                                                            + 'INNER JOIN payment_plan ON pp_installments.pp_id=payment_plan.pp_id '
                                                                            + 'WHERE invoice_installments.pp_id IN(?) AND (pp_installments.paid_flag=0 OR pp_installments.partial_paid=0)'
                                                                            , [uniquePlanID, 1, 1]
                                                                            , function (missAmt, amountDetails, fields) {

                                                                                if (missAmt) {
                                                                                    con.rollback(function () {
                                                                                        obj.status = 0;
                                                                                        obj.message = "Something wrong please try again.";
                                                                                        return callback(obj);
                                                                                    });
                                                                                } else {

                                                                                    con.commit(function (err) {
                                                                                        obj.status = 1;
                                                                                        obj.childResult = childResult;
                                                                                        obj.resultmain = resultmain;
                                                                                        obj.lateFee = (Object.keys(late_fee_rows).length > 0) ? late_fee_rows : '';
                                                                                        obj.LastPayment = LastPayment;
                                                                                        obj.planDetails = planDetails;
                                                                                        obj.amountDetails = amountDetails;
                                                                                        return callback(obj);
                                                                                    })
                                                                                }
                                                                            })
                                                                    }
                                                                })

                                                        }
                                                    })
                                            }
                                        });

                                }
                            })
                    }

                }
            });
    })

    /*con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number ,(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") as due_date, patient_invoice.paid_flag '
        + 'FROM patient_invoice '

        + 'INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN pp_installments ON pp_installments.pp_installment_id=pp_installment_invoice.pp_installment_id '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'WHERE ' + cond
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC'
        , function (error, rows, fields) {
            //console.log(this.sql)
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })*/

}

var cosignerInvoiceReportsFilter = function (con, data, callback) {
    let cond = 'invoicestatus.md_id="Customer Invoice Status"';
    if (data.invoice_due_start_date != '') {
        cond = cond + ' AND patient_invoice.due_date >="' + data.invoice_due_start_date + '"';
    }
    if (data.invoice_due_end_date != '') {
        cond = cond + ' AND patient_invoice.due_date <="' + data.invoice_due_end_date + '"';
    }

    if (data.invoice_start_date != '') {
        cond = cond + ' AND patient_invoice.payment_date >="' + data.invoice_start_date + '"';
    }
    if (data.invoice_end_date != '') {
        cond = cond + ' AND patient_invoice.payment_date <="' + data.invoice_end_date + '"';
    }

    /*if (data.short_by == 1) {
        cond = cond + ' AND WEEKOFYEAR(patient_invoice.due_date)=WEEKOFYEAR(NOW())';
    } else {
        cond = cond + ' AND YEAR(patient_invoice.due_date) = YEAR(NOW()) AND MONTH(patient_invoice.due_date)=MONTH(NOW())';
    }*/

    if (data.status != '') {
        cond = cond + ' AND patient_invoice.invoice_status ="' + data.status + '"';
    }


    if (data.customer_name != '') {
        cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
    }

    var obj = {}
    con.query('SELECT '
        + 'patient_invoice.invoice_id,patient_invoice.invoice_number, '
        + 'patient_invoice.payment_amount,patient_invoice.paid_amount,patient_invoice.late_fee_received,patient_invoice.late_fee_due,patient_invoice.fin_charge_due,patient_invoice.fin_charge_received,patient_invoice.previous_fin_charge,patient_invoice.previous_late_fee,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,'
        //+ ',(IFNULL(patient_invoice.payment_amount,0)+IFNULL(patient_invoice.late_fee_received,0)+IFNULL(patient_invoice.fin_charge_amt,0)+IFNULL(patient_invoice.previous_fin_charge,0)+IFNULL(patient_invoice.previous_late_fee,0)) as payment_amount,patient_invoice.additional_amount,DATE_FORMAT(patient_invoice.payment_date, "%m/%d/%Y") as payment_date,  '
        + 'patient.f_name, patient.m_name, patient.l_name, patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id, '
        + 'invoicestatus.value as invoice_status_name, '
        + 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date, patient_invoice.invoice_status '
        + 'FROM patient_invoice '

        + 'INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id  '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_invoice_details.pp_id '
        + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values as invoicestatus on invoicestatus.status_id = patient_invoice.invoice_status '
        + 'INNER JOIN credit_app_cosigner ON credit_app_cosigner.application_id=credit_applications.application_id '
        + 'INNER JOIN user ON user.co_signer_id = credit_app_cosigner.co_signer_id '
        + 'WHERE ' + cond
        + ' AND user.user_id = ?'
        + ' GROUP BY patient_invoice.invoice_id ORDER BY patient_invoice.payment_date DESC'
        , [data.current_user_id]
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = rows;
                return callback(obj);
            }
        })

}

exports.customerInvoiceReports = customerInvoiceReports;
exports.customerInvoiceReportsFilter = customerInvoiceReportsFilter;
exports.getPrintDetails = getPrintDetails;
exports.cosignerInvoiceReportsFilter = cosignerInvoiceReportsFilter;