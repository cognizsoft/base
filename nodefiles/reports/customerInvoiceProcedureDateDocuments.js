module.exports = (data, logoimg) => {
   var html = '';
   
   data.result.forEach(function (element, idx) {
      
      html = html + '<tr>'
         + '<td>' + element.provider_ac + '</td>'
         + '<td>' + element.name + '</td>'
         + '<td>' + element.plan_number + '</td>'
         + '<td>' + element.amount + '</td>'
         + '<td>' + element.customer_f_name+''+element.customer_m_name+' '+element.customer_l_name + '</td>'
         + '<td>' + element.address1 + '</td>'
         + '<td>' + element.city + '</td>'
         + '<td>' + element.state_name + '</td>'
         + '<td>' + element.zip_code + '</td>'
         + '<td>' + element.phone + '</td>'
         + '<td>' + element.procedure_date + '</td>'
         + '<td>' + element.plan_status + '</td>'
         + '<td>' + element.email + '</td>';
   });
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Current Weekly and Monthly Invoice Invoice Reports</title>
        <style>
            html{zoom: 0.7;}
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0px;
           font-size: 12px;
           line-height: 16px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           .margin-top {
           margin-top: 50px;
           }
           .justify-center {
           text-align: center;
           
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .provider-info{width:48%; float:left}
        .invoice-info{width:48%; float:right;}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
        .detail-table td{padding:5px 2px; font-size:10px;}
           
           
        </style>
     </head>
     <body>
        <div class="invoice-box">
        <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
       <h2 class="justify-center">Provider Procedure Date Report</h2>
       <div class="detail-bx">
          <div class="detail-table">
             <table cellpadding="0" cellspacing="0">
                <tr>
                   <th>Provider A/C</th>
                   <th>Provider Name</th>
                   <th>Plan Number</th>
                   <th>Plan Amount</th>
                   <th>Customer Name</th>
                   <th>Address</th>
                   <th>City</th>
                   <th>State</th>
                   <th>Zip</th>
                   <th>Phone</th>
                   <th>Procedure date</th>
                   <th>Procedure Status</th>
                   <th>Email</th>
                </tr>
                ${html}
             </table>
          </div>
       </div>
       
          
        </div>
     </body>
  </html>
      `;
};