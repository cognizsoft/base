module.exports = (dataFilter, data, logoimg) => {

   let moment = require('moment');
   let fileName = '';
   if (dataFilter.filter_type == 1) {
      moment.updateLocale('en', {
         week: {
            dow: 1,
            doy: 1
         }
      });

      let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
      let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
      fileName = 'Weekly Accounts Payable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
   } else if (dataFilter.filter_type == 2) {
      let monthName = moment().month(dataFilter.month - 1).format("MMM");
      fileName = 'Monthly Accounts Payable Report (' + monthName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 3) {
      let queterName = ''
      if (dataFilter.quarter == 1) {
         queterName = 'Q1';
      } else if (dataFilter.quarter == 2) {
         queterName = 'Q2';
      } else if (dataFilter.quarter == 3) {
         queterName = 'Q3';
      } else if (dataFilter.quarter == 4) {
         queterName = 'Q4';
      }
      fileName = 'Quarterly Accounts Payable Report (' + queterName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 4) {
      fileName = 'Yearly Accounts Payable Report (' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 5) {
      let currentDate = moment().format('MM/DD/YYYY');
      fileName = 'Year to Date Accounts Payable Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
   } else if (dataFilter.filter_type == 6) {
      let loanStart = moment(dataFilter.loan_start_date).format('MM/DD/YYYY');
      let loanEnd = moment(dataFilter.loan_end_date).format('MM/DD/YYYY');
      fileName = 'By Date Accounts Payable Report (' + loanStart + '-' + loanEnd + ')';
   }

   var html = '';
   let total_amt = 0.00;
   let total_hps_discount = 0.00;
   let check_amount = 0.00;
   let refund_amount = 0.00;
   let totalPayAmt = 0;
   let totalDue = 0;
   data.result.forEach(function (element, idx) {
      element.total_amt = (element.total_amt) ? element.total_amt : 0;
      element.total_hps_discount = (element.total_hps_discount) ? element.total_hps_discount : 0;
      element.check_amount = (element.check_amount) ? element.check_amount : 0;
      element.date_paid = (element.date_paid) ? element.date_paid : '-';
      element.bank_name = (element.bank_name) ? element.bank_name : '-';
      element.txn_no = (element.txn_no) ? element.txn_no : '-';

      total_amt = total_amt + element.procedure_amt;
      total_hps_discount = total_hps_discount + element.hps_discount_amt;


      let deuAmt = (element.mdv_invoice_status_id != 4) ? (parseFloat(element.procedure_amt) - parseFloat(element.hps_discount_amt)) : 0;
      
      let refundAmt = (element.iou_flag != 1) ? (element.refund_recived != null) ? parseFloat(element.refund_recived) : 0 : parseFloat(element.refund_due)
      refund_amount = refund_amount + refundAmt;
      let paySingle = (element.procedure_amt) ? (parseFloat(element.procedure_amt) - parseFloat(element.hps_discount_amt)) : 0;
      let checkAmt = (element.check_amount != null) ? (element.check_amount) : 0;
      let invStatus = (element.inv_status != null)?element.inv_status:'-';

      totalDue = totalDue + deuAmt;
      check_amount = check_amount + checkAmt;
      totalPayAmt = totalPayAmt + paySingle;
      html = html + '<tr>'
         + '<td>' + element.plan_number + '</td>'
         + '<td>' + element.name + '</td>'
         + '<td>' + element.f_name + ' ' + element.m_name + ' ' + element.l_name + '</td>'
         + '<td>' + element.procedure_date + '</td>'
         + '<td>$' + element.procedure_amt.toFixed(2) + '</td>'
         + '<td>$' + element.hps_discount_amt.toFixed(2) + '</td>'
         + '<td>$' + parseFloat(paySingle).toFixed(2) + '</td>'
         + '<td>$' + parseFloat(checkAmt).toFixed(2) + '</td>'
         + '<td>$' + deuAmt.toFixed(2) + '</td>'
         + '<td>$' + refundAmt.toFixed(2) + '</td>'
         + '<td>' + invStatus + '</td>'
         + '</tr>';
   });
   if (data.result.length > 0) {
      html = html + '<tr>'
         + '<td><b>Total</b></td>'
         + '<td></td>'
         + '<td></td>'
         + '<td></td>'
         + '<td><b>$' + total_amt.toFixed(2) + '</b></td>'
         + '<td><b>$' + total_hps_discount.toFixed(2) + '</b></td>'
         + '<td><b>$' + totalPayAmt.toFixed(2) + '</b></td>'
         + '<td><b>$' + check_amount + '</b></td>'
         + '<td><b>$' + totalDue + '</b></td>'
         + '<td><b>$' + refund_amount + '</b></td>'
         + '<td></td>'
         + '</tr>';
   }
   return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>Provider Invoice Reports</title>
       <style>
          html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 10px;
          line-height: 24px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:8px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:8px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:7px; line-height:22px;}
       .detail-table td{padding:5px 2px; font-size:7px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">${fileName}</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>Loan No</th>
                  <th>Provider Name</th>
                  <th>Customer Name</th>
                  <th>Loan Date</th>
                  <th>Total Amt Invoiced</th>
                  <th>Total Discount Amt</th>
                  <th>Total Amt payable</th>
                  <th>Total Amt paid</th>
                  <th>Balance Amt Payable</th>
                  <th>Refund Amt</th>
                  <th>Status</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};