
var loanReportsData = function (con, callback) {
	var obj = {};
	con.query('SELECT '
		+ 'payment_plan.pp_id,payment_plan.plan_number,payment_plan.provider_id,payment_plan.application_id,payment_plan.amount,payment_plan.paid_flag,payment_plan.remaining_amount,payment_plan.plan_status, '
		+ 'payment_plan.amount as loan_amount,DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") As loan_date, '
		+ 'payment_plan.loan_amount as main_amount,credit_applications.application_no, '
		+ 'patient.f_name,patient.m_name,patient.l_name,provider.name, '
		+ 'master_data_values.value AS p_status, '

		+ '(SELECT DATE_FORMAT(due_date, "%m/%d/%Y") FROM pp_installments WHERE payment_plan.pp_id = pp_installments.pp_id ORDER BY pp_installment_id DESC LIMIT 1) as maturity_date, '

		+ '(SELECT SUM(fin_charge) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_finance_charge, '

		//+ '(SELECT SUM(payment_amount) FROM patient_invoice INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = pp_installment_invoice.pp_id) as total_payment_amt, '
		//+ '(SELECT SUM(invoice_amount-(interest+principal_amount)) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
		+ '(SELECT SUM(paid_amount+late_fee_received+fin_charge_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '

		+ '(SELECT SUM(additional_amount) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_additional_amt, '

		+ '(SELECT SUM(late_fee_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_late_fee, '

		+ '(SELECT SUM(installment_principal_amount-principal_amount) FROM patient_invoice_details WHERE pp_id = payment_plan.pp_id) as principal_paid '

		+ 'FROM payment_plan '
		+ 'INNER JOIN patient_procedure ON patient_procedure.pp_id=payment_plan.pp_id '
		+ 'INNER JOIN credit_applications  ON credit_applications.application_id=payment_plan.application_id '
		+ 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
		+ 'INNER JOIN provider ON provider.provider_id=payment_plan.provider_id '
		+ 'INNER JOIN master_data_values ON master_data_values.status_id=payment_plan.plan_status WHERE master_data_values.md_id = "Customer Plan Status" '

		, function (error, rows, fields) {
			if (error) {
				obj.status = 0;
				obj.message = "Something wrong. Please try again";
				return callback(obj);
			}
			else {
				con.query('SELECT '
					+ 'value, status_id '

					+ 'FROM master_data_values '
					+ 'WHERE md_id = "Customer Plan Status" AND status = 1'
					, function (err, plan_status) {
						if (err) {
							obj.status = 0;
							obj.message = "Something wrong. Please try again";
							return callback(obj);
						} else {
							con.query('SELECT '
								+ 'patient_invoice_details.pp_id, '

								+ 'patient_invoice.invoice_number, '
								+ 'patient_invoice.late_fee_received, '
								+ 'patient_invoice.late_fee_waivers_id, '
								+ 'patient_invoice_details.fin_charge AS fin_charge_amt, '
								+ 'patient_invoice.fin_charge_waiver_id, '

								+ 'late_fee_waivers.mdv_waiver_type_id, '
								+ 'late_fee_waivers2.mdv_waiver_type_id, '

								+ 'master_data_values.value AS late_fee_per, '
								+ 'master_data_values2.value AS fin_charge_per '

								+ 'FROM patient_invoice '

								+ 'INNER JOIN patient_invoice_details '
								+ 'ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '

								+ 'LEFT JOIN late_fee_waivers '
								+ 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

								+ 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
								+ 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '
								+ 'LEFT JOIN master_data_values '
								+ 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

								+ 'LEFT JOIN master_data_values AS master_data_values2 '
								+ 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

								+ 'WHERE patient_invoice.invoice_status IN (1,4)'
								, function (finErr, late_fee_fin_charge) {
									if (err) {
										obj.status = 0;
										obj.message = "Something wrong. Please try again";
										return callback(obj);
									} else {
										var arrData = []
										late_fee_fin_charge.forEach(function (chr) {
											if (chr.late_fee_waivers_id) {
												//var lateFee = (chr.late_fee_received - (chr.late_fee_received*chr.late_fee_per)/100)
												var lateFee = chr.late_fee_received;// - (chr.late_fee_received*chr.late_fee_per)/100)
											} else {
												var lateFee = chr.late_fee_received
											}
											if (chr.fin_charge_waiver_id) {
												var finFee = (chr.fin_charge_amt - (chr.fin_charge_amt * chr.fin_charge_per) / 100)
												//var finFee = chr.fin_charge_amt;// - (chr.fin_charge_amt*chr.fin_charge_per)/100)
											} else {
												var finFee = chr.fin_charge_amt
											}

											var objData = {
												"late_fee": lateFee,
												"fin_fee": finFee,
												"pp_id": chr.pp_id
											}

											arrData.push(objData)
										})

										var result = arrData.reduce(function (acc, val) {
											var o = acc.filter(function (obj) {
												return obj.pp_id == val.pp_id;
											}).pop() || { pp_id: val.pp_id, late_fee: 0, fin_fee: 0 };

											o.late_fee += val.late_fee;
											o.fin_fee += val.fin_fee;
											acc.push(o);
											return acc;
										}, []);

										var unique = Object.values(result.reduce((acc, cur) => Object.assign(acc, { [cur.pp_id]: cur }), {}))


										var blnk = {
											"late_fee": 0,
											"fin_fee": 0
										}

										var temp = rows.map(itm => ({
											...unique.find((item) => (item.pp_id === itm.pp_id) && item),
											...itm
										}));

										var finalRows = []
										temp.forEach(function (tm) {
											if (tm.late_fee == undefined) {
												tm.late_fee = 0
												tm.fin_fee = 0
												finalRows.push(tm)
											} else {
												finalRows.push(tm)
											}
										})

										con.query('SELECT value FROM master_data_values WHERE md_id=?'
											,
											['Year Start']
											, function (error, yearStart, fields) {
												if (error) {
													obj.status = 1;
													obj.result = finalRows;
													obj.plan_status = plan_status;
													obj.charge = late_fee_fin_charge;
													obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
													return callback(obj);
												} else {
													obj.status = 1;
													obj.result = finalRows;
													obj.plan_status = plan_status;
													obj.charge = late_fee_fin_charge;
													obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
													return callback(obj);
												}
											})
									}
								})
						}
					})

			}
		})

}

var loanReportsFilterData = function (con, data, callback) {
	var obj = {};
	let cond = ' payment_plan.plan_status NOT IN(0,2,3,4)';
	if (data.filter_type != '' || data.customer_name != '' || data.status != '' || data.maturity_start_date != '' || data.maturity_end_date != '') {
		if (data.filter_type == 1) {
			cond = cond + ' AND WEEKOFYEAR(patient_procedure.procedure_date)=WEEKOFYEAR(NOW())'
		} else if (data.filter_type == 2) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND MONTH(patient_procedure.procedure_date)=' + data.month;
		} else if (data.filter_type == 3) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND QUARTER(patient_procedure.procedure_date)=' + data.quarter;
		} else if (data.filter_type == 4) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year;
		} else if (data.filter_type == 5) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = YEAR(CURDATE()) AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
		} else if (data.filter_type == 6) {
			if (data.loan_start_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") >="' + data.loan_start_date + '"';
			}
			if (data.loan_end_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <="' + data.loan_end_date + '"';
			}
		}

		if (data.customer_name != '') {
			//cond = cond + ' AND DATE_FORMAT(credit_applications.date_created, "%Y-%m-%d") >="' + data.loan_start_date + '"';
			cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
		}


		if (data.status != '') {
			cond = cond + ' AND payment_plan.plan_status ="' + data.status + '"';
		}
		/*if (data.status != '') {
			if (data.status == -1) {
				cond = cond + ' AND credit_applications.status = 1 AND credit_applications.plan_flag = 0';
			} else if (data.status == 1) {
				cond = cond + ' AND credit_applications.status = 1 AND credit_applications.plan_flag = 1';
			} else {
				cond = cond + ' AND credit_applications.status ="' + data.status + '"';
			}
		}*/
		if (data.maturity_start_date != '' || data.maturity_end_date != '') {
			con.query('SELECT DATE_FORMAT(max(due_date), "%Y-%m-%d") as due_date, pp_id FROM pp_installments GROUP by pp_id'
				, function (error, rows, fields) {
					if (error) {
						obj.status = 0;
						obj.message = "Something wrong. Please try again";
						return callback(obj);
					} else {
						var pp_id = rows.reduce(function (accumulator, currentValue) {
							if (data.maturity_start_date != '' && data.maturity_end_date != '') {
								if (currentValue.due_date <= data.maturity_end_date && currentValue.due_date >= data.maturity_start_date) {
									accumulator.push(currentValue.pp_id);
								}
							} else if (data.maturity_start_date != '') {
								if (currentValue.due_date >= data.maturity_start_date) {
									accumulator.push(currentValue.pp_id);
								}
							} else if (data.maturity_end_date != '') {
								if (currentValue.due_date <= data.maturity_end_date) {
									accumulator.push(currentValue.pp_id);
								}
							}
							return accumulator
						}, []);
						if (pp_id.length == 0) {
							obj.status = 1;
							obj.result = [];
							return callback(obj);
						} else {
							cond = cond + ' AND payment_plan.pp_id IN (' + pp_id + ')';
							con.query('SELECT '
								+ 'payment_plan.pp_id,payment_plan.plan_number,payment_plan.provider_id,payment_plan.application_id,payment_plan.amount,payment_plan.paid_flag,payment_plan.remaining_amount,payment_plan.plan_status, '
								+ 'payment_plan.amount as loan_amount,DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") As loan_date, '
								+ 'payment_plan.loan_amount as main_amount,credit_applications.application_no, '
								+ 'patient.f_name,patient.m_name,patient.l_name,provider.name, '
								+ 'master_data_values.value AS p_status, '
								+ '(SELECT DATE_FORMAT(due_date, "%m/%d/%Y") FROM pp_installments WHERE payment_plan.pp_id = pp_installments.pp_id ORDER BY pp_installment_id DESC LIMIT 1) as maturity_date, '

								+ '(SELECT SUM(fin_charge) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_finance_charge, '

								//+ '(SELECT SUM(payment_amount) FROM patient_invoice INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = pp_installment_invoice.pp_id) as total_payment_amt, '
								//+ '(SELECT SUM(invoice_amount-(interest+principal_amount)) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
								+ '(SELECT SUM(paid_amount+late_fee_received+fin_charge_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
								+ '(SELECT SUM(additional_amount) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_additional_amt, '

								+ '(SELECT SUM(late_fee_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_late_fee, '

								+ '(SELECT SUM(installment_principal_amount-principal_amount) FROM patient_invoice_details WHERE pp_id = payment_plan.pp_id) as principal_paid '

								+ 'FROM payment_plan '
								+ 'INNER JOIN patient_procedure ON patient_procedure.pp_id=payment_plan.pp_id '
								+ 'INNER JOIN credit_applications  ON credit_applications.application_id=payment_plan.application_id '
								+ 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
								+ 'INNER JOIN provider ON provider.provider_id=payment_plan.provider_id '
								+ 'INNER JOIN master_data_values ON master_data_values.status_id=payment_plan.plan_status '
								+ 'WHERE master_data_values.md_id = "Customer Plan Status" AND ' + cond

								, function (error, rows, fields) {
									if (error) {
										obj.status = 0;
										obj.message = "Something wrong. Please try again";
										return callback(obj);
									}
									else {
										con.query('SELECT '
											+ 'value, status_id '

											+ 'FROM master_data_values '
											+ 'WHERE md_id = "Customer Plan Status" AND status = 1'
											, function (err, plan_status) {
												if (err) {
													obj.status = 0;
													obj.message = "Something wrong. Please try again";
													return callback(obj);
												} else {
													con.query('SELECT '
														+ 'patient_invoice_details.pp_id, '

														+ 'patient_invoice.invoice_number, '
														+ 'patient_invoice.late_fee_received, '
														+ 'patient_invoice.late_fee_waivers_id, '
														+ 'patient_invoice_details.fin_charge AS fin_charge_amt, '
														+ 'patient_invoice.fin_charge_waiver_id, '

														+ 'late_fee_waivers.mdv_waiver_type_id, '
														+ 'late_fee_waivers2.mdv_waiver_type_id, '

														+ 'master_data_values.value AS late_fee_per, '
														+ 'master_data_values2.value AS fin_charge_per '

														+ 'FROM patient_invoice '

														+ 'INNER JOIN patient_invoice_details '
														+ 'ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '

														+ 'LEFT JOIN late_fee_waivers '
														+ 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

														+ 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
														+ 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '
														+ 'LEFT JOIN master_data_values '
														+ 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

														+ 'LEFT JOIN master_data_values AS master_data_values2 '
														+ 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

														+ 'WHERE patient_invoice.invoice_status IN (1,4)'
														, function (finErr, late_fee_fin_charge) {
															if (err) {
																obj.status = 0;
																obj.message = "Something wrong. Please try again";
																return callback(obj);
															} else {
																var arrData = []
																late_fee_fin_charge.forEach(function (chr) {
																	if (chr.late_fee_waivers_id) {
																		var lateFee = (chr.late_fee_received - (chr.late_fee_received * chr.late_fee_per) / 100)
																	} else {
																		var lateFee = chr.late_fee_received
																	}

																	if (chr.fin_charge_waiver_id) {
																		var finFee = (chr.fin_charge_amt - (chr.fin_charge_amt * chr.fin_charge_per) / 100)
																	} else {
																		var finFee = chr.fin_charge_amt
																	}

																	var objData = {
																		"late_fee": lateFee,
																		"fin_fee": finFee,
																		"pp_id": chr.pp_id
																	}

																	arrData.push(objData)
																})

																var result = arrData.reduce(function (acc, val) {
																	var o = acc.filter(function (obj) {
																		return obj.pp_id == val.pp_id;
																	}).pop() || { pp_id: val.pp_id, late_fee: 0, fin_fee: 0 };

																	o.late_fee += val.late_fee;
																	o.fin_fee += val.fin_fee;
																	acc.push(o);
																	return acc;
																}, []);

																var unique = Object.values(result.reduce((acc, cur) => Object.assign(acc, { [cur.pp_id]: cur }), {}))


																var blnk = {
																	"late_fee": 0,
																	"fin_fee": 0
																}

																var temp = rows.map(itm => ({
																	...unique.find((item) => (item.pp_id === itm.pp_id) && item),
																	...itm
																}));

																var finalRows = []
																temp.forEach(function (tm) {
																	if (tm.late_fee == undefined) {
																		tm.late_fee = 0
																		tm.fin_fee = 0
																		finalRows.push(tm)
																	} else {
																		finalRows.push(tm)
																	}
																})
																con.query('SELECT value FROM master_data_values WHERE md_id=?'
																	,
																	['Year Start']
																	, function (error, yearStart, fields) {
																		if (error) {
																			obj.status = 1;
																			obj.result = finalRows;
																			obj.plan_status = plan_status;
																			obj.charge = late_fee_fin_charge;
																			obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
																			return callback(obj);
																		} else {
																			obj.status = 1;
																			obj.result = finalRows;
																			obj.plan_status = plan_status;
																			obj.charge = late_fee_fin_charge;
																			obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
																			return callback(obj);
																		}
																	})
															}
														})
												}
											})
									}
								})
						}
					}
				})
		} else {
			con.query('SELECT '
				+ 'payment_plan.pp_id,payment_plan.plan_number,payment_plan.provider_id,payment_plan.application_id,payment_plan.amount,payment_plan.paid_flag,payment_plan.remaining_amount,payment_plan.plan_status, '
				+ 'payment_plan.amount as loan_amount,DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") As loan_date, '
				+ 'payment_plan.loan_amount as main_amount,credit_applications.application_no, '
				+ 'patient.f_name,patient.m_name,patient.l_name,provider.name, '
				+ 'master_data_values.value AS p_status, '
				+ '(SELECT DATE_FORMAT(due_date, "%m/%d/%Y") FROM pp_installments WHERE payment_plan.pp_id = pp_installments.pp_id ORDER BY pp_installment_id DESC LIMIT 1) as maturity_date, '

				+ '(SELECT SUM(fin_charge) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_finance_charge, '

				//+ '(SELECT SUM(payment_amount) FROM patient_invoice INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = pp_installment_invoice.pp_id) as total_payment_amt, '
				//+ '(SELECT SUM(invoice_amount-(interest+principal_amount)) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
				+ '(SELECT SUM(paid_amount+late_fee_received+fin_charge_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
				+ '(SELECT SUM(additional_amount) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_additional_amt, '

				+ '(SELECT SUM(late_fee_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) as total_late_fee, '

				+ '(SELECT SUM(installment_principal_amount-principal_amount) FROM patient_invoice_details WHERE pp_id = payment_plan.pp_id) as principal_paid '

				+ 'FROM payment_plan '
				+ 'INNER JOIN patient_procedure ON patient_procedure.pp_id=payment_plan.pp_id '
				+ 'INNER JOIN credit_applications  ON credit_applications.application_id=payment_plan.application_id '
				+ 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
				+ 'INNER JOIN provider ON provider.provider_id=payment_plan.provider_id '
				+ 'INNER JOIN master_data_values ON master_data_values.status_id=payment_plan.plan_status '
				+ 'WHERE master_data_values.md_id = "Customer Plan Status" AND ' + cond

				, function (error, rows, fields) {

					if (error) {
						obj.status = 0;
						obj.message = "Something wrong. Please try again";
						return callback(obj);
					}
					else {
						con.query('SELECT '
							+ 'value, status_id '

							+ 'FROM master_data_values '
							+ 'WHERE md_id = "Customer Plan Status" AND status = 1'
							, function (err, plan_status) {
								if (err) {
									obj.status = 0;
									obj.message = "Something wrong. Please try again";
									return callback(obj);
								} else {
									con.query('SELECT '
										+ 'patient_invoice_details.pp_id, '

										+ 'patient_invoice.invoice_number, '
										+ 'patient_invoice.late_fee_received, '
										+ 'patient_invoice.late_fee_waivers_id, '
										+ 'patient_invoice_details.fin_charge AS fin_charge_amt, '
										+ 'patient_invoice.fin_charge_waiver_id, '

										+ 'late_fee_waivers.mdv_waiver_type_id, '
										+ 'late_fee_waivers2.mdv_waiver_type_id, '

										+ 'master_data_values.value AS late_fee_per, '
										+ 'master_data_values2.value AS fin_charge_per '

										+ 'FROM patient_invoice '

										+ 'INNER JOIN patient_invoice_details '
										+ 'ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '

										+ 'LEFT JOIN late_fee_waivers '
										+ 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

										+ 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
										+ 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '
										+ 'LEFT JOIN master_data_values '
										+ 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

										+ 'LEFT JOIN master_data_values AS master_data_values2 '
										+ 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

										+ 'WHERE patient_invoice.invoice_status IN (1,4)'
										, function (finErr, late_fee_fin_charge) {
											if (err) {
												obj.status = 0;
												obj.message = "Something wrong. Please try again";
												return callback(obj);
											} else {
												var arrData = []
												late_fee_fin_charge.forEach(function (chr) {
													if (chr.late_fee_waivers_id) {
														var lateFee = (chr.late_fee_received - (chr.late_fee_received * chr.late_fee_per) / 100)
													} else {
														var lateFee = chr.late_fee_received
													}

													if (chr.fin_charge_waiver_id) {
														var finFee = (chr.fin_charge_amt - (chr.fin_charge_amt * chr.fin_charge_per) / 100)
													} else {
														var finFee = chr.fin_charge_amt
													}

													var objData = {
														"late_fee": lateFee,
														"fin_fee": finFee,
														"pp_id": chr.pp_id
													}

													arrData.push(objData)
												})

												var result = arrData.reduce(function (acc, val) {
													var o = acc.filter(function (obj) {
														return obj.pp_id == val.pp_id;
													}).pop() || { pp_id: val.pp_id, late_fee: 0, fin_fee: 0 };

													o.late_fee += val.late_fee;
													o.fin_fee += val.fin_fee;
													acc.push(o);
													return acc;
												}, []);

												var unique = Object.values(result.reduce((acc, cur) => Object.assign(acc, { [cur.pp_id]: cur }), {}))


												var blnk = {
													"late_fee": 0,
													"fin_fee": 0
												}

												var temp = rows.map(itm => ({
													...unique.find((item) => (item.pp_id === itm.pp_id) && item),
													...itm
												}));

												var finalRows = []
												temp.forEach(function (tm) {
													if (tm.late_fee == undefined) {
														tm.late_fee = 0
														tm.fin_fee = 0
														finalRows.push(tm)
													} else {
														finalRows.push(tm)
													}
												})
												con.query('SELECT value FROM master_data_values WHERE md_id=?'
													,
													['Year Start']
													, function (error, yearStart, fields) {
														if (error) {
															obj.status = 1;
															obj.result = finalRows;
															obj.plan_status = plan_status;
															obj.charge = late_fee_fin_charge;
															obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
															return callback(obj);
														} else {
															obj.status = 1;
															obj.result = finalRows;
															obj.plan_status = plan_status;
															obj.charge = late_fee_fin_charge;
															obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
															return callback(obj);
														}
													})

											}
										})
								}
							})
					}
				})

		}
	} else {
		con.query('SELECT '
			+ 'value, status_id '

			+ 'FROM master_data_values '
			+ 'WHERE md_id = "Customer Plan Status" AND status = 1'
			, function (err, plan_status) {
				if (err) {
					obj.status = 0;
					obj.message = "Something wrong. Please try again";
					return callback(obj);
				} else {
					con.query('SELECT value FROM master_data_values WHERE md_id=?'
						,
						['Year Start']
						, function (error, yearStart, fields) {
							if (error) {
								obj.status = 1;
								obj.result = '';
								obj.plan_status = plan_status;
								obj.charge = '';
								obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
								return callback(obj);
							} else {
								obj.status = 1;
								obj.result = '';
								obj.plan_status = plan_status;
								obj.charge = '';
								obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : ''
								return callback(obj);
							}
						})
				}
			})
	}


}

exports.loanReportsData = loanReportsData;
exports.loanReportsFilterData = loanReportsFilterData;