module.exports = (data,logoimg) => {
   var html = '';
   let total_amt = 0.00;
   let total_hps_discount = 0.00;
   let check_amount = 0.00;
   
   data.result.forEach(function (element, idx) {
      element.total_amt = (element.total_amt) ? element.total_amt : 0;
      element.total_hps_discount = (element.total_hps_discount) ? element.total_hps_discount : 0;
      element.check_amount = (element.check_amount) ? element.check_amount : 0;
      element.date_paid = (element.date_paid) ? element.date_paid : '-';
      element.bank_name = (element.bank_name) ? element.bank_name : '-';
      element.txn_no = (element.txn_no) ? element.txn_no : '-';

      total_amt = total_amt + element.total_amt;
      total_hps_discount = total_hps_discount + element.total_hps_discount;
      check_amount = check_amount + element.check_amount;
      
      element.check_amount = (element.check_amount != 0) ? element.check_amount.toFixed(2) : '-';

      let iouAdj = data.iouAmt.filter(function (el) {
         return el.provider_invoice_id == element.provider_invoice_id;
      });
      let totalIOU = iouAdj.reduce(function (accumulator, currentValue, currentindex) {
         return accumulator += currentValue.iou_paid_amount;
      }, 0);
      totalIOU = (totalIOU)?totalIOU:'-';
      let amtDue = (element.value != 'Paid')?(parseFloat(element.total_amt) - parseFloat(element.total_hps_discount)):0;
      html = html + '<tr>'
         + '<td>' + element.invoice_number + '</td>'
         + '<td>' + element.name + '</td>'
         + '<td>' + element.location_name + '</td>'
         + '<td>' + element.phone + '</td>'
         + '<td>$' + element.total_amt.toFixed(2) + '</td>'
         + '<td>$' + element.total_hps_discount.toFixed(2) + '</td>'
         + '<td>' + (amtDue).toFixed(2) + '</td>'
         + '<td>' + element.check_amount + '</td>'
         + '<td>' + totalIOU + '</td>'
         + '<td>' + element.date_created + '</td>'
         + '<td>' + element.date_paid + '</td>'
         + '<td>' + element.bank_name + '</td>'
         + '<td>' + element.txn_no + '</td>'
         + '<td>' + element.value + '</td>'
         + '</tr>';
   });
   if(data.result.length){
   html = html + '<tr>'
      + '<td><b>Total</b></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td><b>$' + total_amt.toFixed(2) + '</b></td>'
      + '<td><b>$' + total_hps_discount.toFixed(2) + '</b></td>'
      + '<td></td>'
      + '<td><b>$' + check_amount.toFixed(2) + '</b></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '</tr>';
   }
   return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>Provider Invoice Reports</title>
       <style>
          html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 10px;
          line-height: 24px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:8px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:8px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:7px; line-height:22px;}
       .detail-table td{padding:5px 2px; font-size:7px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">Provider Invoice Reports</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>Invoice ID</th>
                  <th>Provider Name</th>
                  <th>Provider Location</th>
                  <th>Provider Phone</th>
                  <th>Total Amt</th>
                  <th>HPS Discount</th>
                  <th>Amount Due</th>
                  <th>Amount Paid</th>
                  <th>IOU Adj</th>
                  <th>Invoice Date</th>
                  <th>Paid Date</th>
                  <th>Bank Name</th>
                  <th>Txn No</th>
                  <th>Status</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};