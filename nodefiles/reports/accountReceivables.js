var accountReceivablesData = function (con, callback) {
	var obj = {};
	con.query('SELECT '
		+ 'payment_plan.pp_id, '
		+ 'payment_plan.plan_number, '
		+ 'payment_plan.provider_id, '
		+ 'payment_plan.application_id, '
		+ 'payment_plan.amount, '
		+ 'payment_plan.paid_flag, '
		+ 'payment_plan.discounted_interest_rate, '
		+ 'payment_plan.term, '
		+ 'payment_plan.remaining_amount, '
		+ 'payment_plan.plan_status, '
		+ 'payment_plan.amount AS loan_amount, '
		+ 'DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS plan_created, '
		+ 'payment_plan.loan_amount AS main_amount, '

		+ 'credit_applications.application_no, '

		+ 'patient.f_name, '
		+ 'patient.m_name, '
		+ 'patient.l_name, '
		+ 'provider.name, '

		+ 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") AS loan_date, '

		+ 'master_data_values.value AS p_status, '

		//+ '(SELECT DATE_FORMAT(due_date, "%m/%d/%Y") FROM pp_installments WHERE payment_plan.pp_id = pp_installments.pp_id ORDER BY pp_installment_id DESC LIMIT 1) AS maturity_date, '

		//+ '(SELECT SUM(fin_charge) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_finance_charge, '

		//+ '(SELECT SUM(payment_amount) FROM patient_invoice INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = pp_installment_invoice.pp_id) as total_payment_amt, '
		//+ '(SELECT SUM(invoice_amount-(interest+principal_amount)) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
		//+ '(SELECT SUM(paid_amount+late_fee_received+fin_charge_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) AS total_payment_amt, '

		//+ '(SELECT SUM(additional_amount) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_additional_amt, '

		//+ '(SELECT SUM(late_fee_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_late_fee, '

		//+ '(SELECT SUM(installment_principal_amount-principal_amount) FROM patient_invoice_details WHERE pp_id = payment_plan.pp_id) AS principal_paid, '

		+ '(SELECT SUM(installment_interest)-SUM(interest) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS inv_interest, '

		+ '(SELECT refund_amt FROM customer_refund WHERE payment_plan.pp_id = customer_refund.pp_id ORDER BY customer_refund.pp_id DESC LIMIT 1) AS refund_amt '
		
		+ 'FROM payment_plan '
		+ 'INNER JOIN credit_applications  '
		+ 'ON credit_applications.application_id=payment_plan.application_id '

		+ 'INNER JOIN patient '
		+ 'ON patient.patient_id=credit_applications.patient_id '
        
        + 'INNER JOIN provider '
        + 'ON provider.provider_id=payment_plan.provider_id '

        + 'INNER JOIN patient_procedure '
        + 'ON patient_procedure.pp_id=payment_plan.pp_id '
        
        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.status_id=payment_plan.plan_status '
        + 'WHERE master_data_values.md_id = "Customer Plan Status" AND payment_plan.plan_status IN (1, 7, 8, 11) '

		, function (error, rows, fields) {
			//console.log(rows[0])
			//console.log(this.sql)
			if (error) {
				obj.status = 0;
				obj.message = "Something wrong. Please try again";
				return callback(obj);
			}
			else {

				rows.forEach(function(row, key) {
					//console.log(key)
					row.inv_interest = (row.inv_interest !== null) ? row.inv_interest : 0
					let perMonthInt = (row.discounted_interest_rate / 12) / 100;

					let perMonth = parseFloat(row.main_amount) * (perMonthInt * Math.pow((1 + perMonthInt), row.term)) / (Math.pow((1 + perMonthInt), row.term) - 1);
					

					let totalMonth = parseFloat(perMonth).toFixed(2)*row.term

					let totalMonthInt = parseFloat(totalMonth).toFixed(2) - parseFloat(row.main_amount).toFixed(2)
					var interest_due
					if(totalMonthInt<=row.inv_interest) {									rows[key].interest_due = 0
					} else {
						rows[key].interest_due = (parseFloat(totalMonthInt).toFixed(2) - parseFloat(row.inv_interest).toFixed(2))
					}

				})

				con.query('SELECT '
					+ 'patient_invoice_details.pp_id, '

					+ 'patient_invoice.invoice_number, '
					+ 'patient_invoice.late_fee_received, '
					+ 'patient_invoice.late_fee_waivers_id, '
					+ 'patient_invoice_details.fin_charge AS fin_charge_amt, '
					+ 'patient_invoice.fin_charge_waiver_id, '

					+ 'late_fee_waivers.mdv_waiver_type_id, '
					+ 'late_fee_waivers2.mdv_waiver_type_id, '

					+ 'master_data_values.value AS late_fee_per, '
					+ 'master_data_values2.value AS fin_charge_per '

					+ 'FROM patient_invoice '

					+ 'INNER JOIN patient_invoice_details '
					+ 'ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '

					+ 'LEFT JOIN late_fee_waivers '
					+ 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

					+ 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
					+ 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '								
					+ 'LEFT JOIN master_data_values '
					+ 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

					+ 'LEFT JOIN master_data_values AS master_data_values2 '
					+ 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

					+ 'WHERE patient_invoice.invoice_status IN (1,4)'
					, function (finErr, late_fee_fin_charge) {
						//console.log(this.sql)
						//console.log(late_fee_fin_charge)
						if(finErr) {
							obj.status = 0;
							obj.message = "Something wrong. Please try again";
							return callback(obj);
						} else {
							var arrData = []
							late_fee_fin_charge.forEach(function(chr) {
								if(chr.late_fee_waivers_id) {
                                	//var lateFee = (chr.late_fee_received - (chr.late_fee_received*chr.late_fee_per)/100)
                                	var lateFee = chr.late_fee_received;// - (chr.late_fee_received*chr.late_fee_per)/100)
                                } else {
                                	var lateFee = chr.late_fee_received
                                }
                                if(chr.fin_charge_waiver_id) {
                                	var finFee = (chr.fin_charge_amt - (chr.fin_charge_amt*chr.fin_charge_per)/100)
                                	//var finFee = chr.fin_charge_amt;// - (chr.fin_charge_amt*chr.fin_charge_per)/100)
                                } else {
                                	var finFee = chr.fin_charge_amt
                                }

								var objData = {
									"late_fee": lateFee,
									"fin_fee": finFee,
									"pp_id": chr.pp_id
								}

								arrData.push(objData)
							})

							var result = arrData.reduce(function(acc, val){
							    var o = acc.filter(function(obj){
							        return obj.pp_id==val.pp_id;
							    }).pop() || {pp_id:val.pp_id, late_fee:0, fin_fee:0};
							    
							    o.late_fee += val.late_fee;
							    o.fin_fee += val.fin_fee;
							    acc.push(o);
							    return acc;
							},[]);

							var unique = Object.values(result.reduce((acc,cur)=>Object.assign(acc,{[cur.pp_id]:cur}),{}))


							var blnk = {
								"late_fee": 0,
								"fin_fee": 0
							}

							var temp = rows.map(itm => ({
						        ...unique.find((item) => (item.pp_id === itm.pp_id) && item),
						        ...itm
						    }));
							
							var finalRows = []
							temp.forEach(function(tm) {
								if(tm.late_fee == undefined) {
									tm.late_fee = 0
									tm.fin_fee = 0
									finalRows.push(tm)
								} else {
									finalRows.push(tm)
								}
							})
							
							con.query('SELECT '
								+ 'value '

								+ 'FROM master_data_values '
								+ 'WHERE md_id = "Year Start" AND status = 1'
								, function (err, year_start) {
									if(err) {
										obj.status = 0;
										obj.message = "Something wrong. Please try again";
										return callback(obj);
									} else {
										obj.status = 1;
										obj.result = finalRows;
										obj.year_start = year_start[0].value;
										obj.charge = late_fee_fin_charge;
										return callback(obj);
									}
							})
						}
				})
						
				
			}
		})
	
}

var accountReceivablesFilterData = function (con, data, callback) {
	var obj = {};

	if (data.filter_type != '') {
		let cond = 1;
		if (data.filter_type == 1) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				cond = cond + ' AND WEEKOFYEAR(patient_procedure.procedure_date)=WEEKOFYEAR(NOW())'
			} else {
				cond = cond + ' AND WEEKOFYEAR(patient_procedure.procedure_date)=WEEKOFYEAR(NOW())'
			}
		} else if (data.filter_type == 2) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND MONTH(patient_procedure.procedure_date)=' + data.month;
			} else {
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND MONTH(patient_procedure.procedure_date)=' + data.month;
			}
		} else if (data.filter_type == 3) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND QUARTER(patient_procedure.procedure_date)=' + data.quarter;
			} else {
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND QUARTER(patient_procedure.procedure_date)=' + data.quarter;
			}
		} else if (data.filter_type == 4) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year;
			} else {
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year;
			}
		} else if (data.filter_type == 5) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = YEAR(CURDATE()) AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
			} else {
				cond = cond + ' AND YEAR(patient_procedure.procedure_date) = YEAR(CURDATE()) AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
			}
		} else if (data.filter_type == 6) {
			if (data.provider_name != '') {
				cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
				if (data.start_date != '') {
					cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") >="' + data.start_date + '"';
				}
				if (data.end_date != '') {
					cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <="' + data.end_date + '"';
				}
			} else {
				if (data.start_date != '') {
					cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") >="' + data.start_date + '"';
				}
				if (data.end_date != '') {
					cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <="' + data.end_date + '"';
				}
			}
		} else if (data.provider_name != '') {
			cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
		}

		/*if (data.status != '') {
			cond = cond + ' AND payment_plan.plan_status ="' + data.status + '"';
		}*/
		con.query('SELECT '
			+ 'payment_plan.pp_id, '
			+ 'payment_plan.plan_number, '
			+ 'payment_plan.provider_id, '
			+ 'payment_plan.application_id, '
			+ 'payment_plan.amount, '
			+ 'payment_plan.paid_flag, '
			+ 'payment_plan.discounted_interest_rate, '
			+ 'payment_plan.term, '
			+ 'payment_plan.remaining_amount, '
			+ 'payment_plan.plan_status, '
			+ 'payment_plan.amount AS loan_amount, '
			+ 'DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS plan_created, '
			+ 'payment_plan.loan_amount AS main_amount, '
			+ 'credit_applications.application_no, '

			+ 'patient.f_name, '
			+ 'patient.m_name, '
			+ 'patient.l_name, '

			+ 'provider.name, '

			+ 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") AS loan_date, '

			+ 'master_data_values.value AS p_status, '

			//+ '(SELECT DATE_FORMAT(due_date, "%m/%d/%Y") FROM pp_installments WHERE payment_plan.pp_id = pp_installments.pp_id ORDER BY pp_installment_id DESC LIMIT 1) AS maturity_date, '

			//+ '(SELECT SUM(fin_charge) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_finance_charge, '

			//+ '(SELECT SUM(payment_amount) FROM patient_invoice INNER JOIN pp_installment_invoice ON pp_installment_invoice.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = pp_installment_invoice.pp_id) as total_payment_amt, '
			//+ '(SELECT SUM(invoice_amount-(interest+principal_amount)) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) as total_payment_amt, '
			//+ '(SELECT SUM(paid_amount+late_fee_received+fin_charge_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id) AS total_payment_amt, '

			//+ '(SELECT SUM(additional_amount) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_additional_amt, '

			//+ '(SELECT SUM(late_fee_received) FROM patient_invoice INNER JOIN patient_invoice_details ON patient_invoice_details.invoice_id=patient_invoice.invoice_id WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS total_late_fee, '

			//+ '(SELECT SUM(installment_principal_amount-principal_amount) FROM patient_invoice_details WHERE pp_id = payment_plan.pp_id) AS principal_paid, '

			+ '(SELECT SUM(installment_interest)-SUM(interest) FROM patient_invoice_details WHERE payment_plan.pp_id = patient_invoice_details.pp_id) AS inv_interest, '
			
			+ '(SELECT refund_amt FROM customer_refund WHERE payment_plan.pp_id = customer_refund.pp_id ORDER BY customer_refund.pp_id DESC LIMIT 1) AS refund_amt '

			+ 'FROM payment_plan '
			+ 'INNER JOIN credit_applications  '
			+ 'ON credit_applications.application_id=payment_plan.application_id '

			+ 'INNER JOIN patient '
			+ 'ON patient.patient_id=credit_applications.patient_id '

			+ 'INNER JOIN provider '
			+ 'ON provider.provider_id=payment_plan.provider_id '

			+ 'INNER JOIN patient_procedure '
			+ 'ON patient_procedure.pp_id=payment_plan.pp_id '

			+ 'INNER JOIN master_data_values '
			+ 'ON master_data_values.status_id=payment_plan.plan_status '
			+ 'WHERE master_data_values.md_id = "Customer Plan Status" AND payment_plan.plan_status IN (1, 7, 8, 11) AND ' + cond

			, function (error, rows, fields) {
				console.log(this.sql)
				if (error) {
					obj.status = 0;
					obj.message = "Something wrong. Please try again";
					return callback(obj);
				}
				else {

					rows.forEach(function (row, key) {
						//console.log(key)
						row.inv_interest = (row.inv_interest !== null) ? row.inv_interest : 0
						let perMonthInt = (row.discounted_interest_rate / 12) / 100;

						let perMonth = parseFloat(row.main_amount) * (perMonthInt * Math.pow((1 + perMonthInt), row.term)) / (Math.pow((1 + perMonthInt), row.term) - 1);


						let totalMonth = parseFloat(perMonth).toFixed(2) * row.term
						//console.log(totalMonth)
						let totalMonthInt = parseFloat(totalMonth).toFixed(2) - parseFloat(row.main_amount).toFixed(2)
						var interest_due
						if (totalMonthInt <= row.inv_interest) {
							rows[key].interest_due = 0
						} else {
							rows[key].interest_due = (parseFloat(totalMonthInt).toFixed(2) - parseFloat(row.inv_interest).toFixed(2))
						}

					})

					con.query('SELECT '
						+ 'patient_invoice_details.pp_id, '

						+ 'patient_invoice.invoice_number, '
						+ 'patient_invoice.late_fee_received, '
						+ 'patient_invoice.late_fee_waivers_id, '
						+ 'patient_invoice_details.fin_charge AS fin_charge_amt, '
						+ 'patient_invoice.fin_charge_waiver_id, '

						+ 'late_fee_waivers.mdv_waiver_type_id, '
						+ 'late_fee_waivers2.mdv_waiver_type_id, '

						+ 'master_data_values.value AS late_fee_per, '
						+ 'master_data_values2.value AS fin_charge_per '

						+ 'FROM patient_invoice '

						+ 'INNER JOIN patient_invoice_details '
						+ 'ON patient_invoice.invoice_id = patient_invoice_details.invoice_id '

						+ 'LEFT JOIN late_fee_waivers '
						+ 'ON late_fee_waivers.id = patient_invoice.late_fee_waivers_id '

						+ 'LEFT JOIN late_fee_waivers AS late_fee_waivers2 '
						+ 'ON late_fee_waivers2.id = patient_invoice.fin_charge_waiver_id '
						+ 'LEFT JOIN master_data_values '
						+ 'ON master_data_values.mdv_id = late_fee_waivers.mdv_waiver_type_id '

						+ 'LEFT JOIN master_data_values AS master_data_values2 '
						+ 'ON master_data_values2.mdv_id = late_fee_waivers2.mdv_waiver_type_id '

						+ 'WHERE patient_invoice.invoice_status IN (1,4)'
						, function (finErr, late_fee_fin_charge) {
							//console.log(this.sql)
							//console.log(late_fee_fin_charge)
							if (finErr) {
								obj.status = 0;
								obj.message = "Something wrong. Please try again";
								return callback(obj);
							} else {
								var arrData = []
								late_fee_fin_charge.forEach(function (chr) {
									if (chr.late_fee_waivers_id) {
										//var lateFee = (chr.late_fee_received - (chr.late_fee_received*chr.late_fee_per)/100)
										var lateFee = chr.late_fee_received;// - (chr.late_fee_received*chr.late_fee_per)/100)
									} else {
										var lateFee = chr.late_fee_received
									}
									if (chr.fin_charge_waiver_id) {
										var finFee = (chr.fin_charge_amt - (chr.fin_charge_amt * chr.fin_charge_per) / 100)
										//var finFee = chr.fin_charge_amt;// - (chr.fin_charge_amt*chr.fin_charge_per)/100)
									} else {
										var finFee = chr.fin_charge_amt
									}

									var objData = {
										"late_fee": lateFee,
										"fin_fee": finFee,
										"pp_id": chr.pp_id
									}

									arrData.push(objData)
								})

								var result = arrData.reduce(function (acc, val) {
									var o = acc.filter(function (obj) {
										return obj.pp_id == val.pp_id;
									}).pop() || { pp_id: val.pp_id, late_fee: 0, fin_fee: 0 };

									o.late_fee += val.late_fee;
									o.fin_fee += val.fin_fee;
									acc.push(o);
									return acc;
								}, []);

								var unique = Object.values(result.reduce((acc, cur) => Object.assign(acc, { [cur.pp_id]: cur }), {}))


								var blnk = {
									"late_fee": 0,
									"fin_fee": 0
								}

								var temp = rows.map(itm => ({
									...unique.find((item) => (item.pp_id === itm.pp_id) && item),
									...itm
								}));

								var finalRows = []
								temp.forEach(function (tm) {
									if (tm.late_fee == undefined) {
										tm.late_fee = 0
										tm.fin_fee = 0
										finalRows.push(tm)
									} else {
										finalRows.push(tm)
									}
								})


								con.query('SELECT '
									+ 'value '

									+ 'FROM master_data_values '
									+ 'WHERE md_id = "Year Start" AND status = 1'
									, function (err, year_start) {
										if (err) {
											obj.status = 0;
											obj.message = "Something wrong. Please try again";
											return callback(obj);
										} else {
											obj.status = 1;
											obj.result = finalRows;
											obj.year_start = year_start[0].value;
											obj.charge = late_fee_fin_charge;
											return callback(obj);
										}
									})
							}
						})

				}
			})
	} else {
		con.query('SELECT '
			+ 'value '
			+ 'FROM master_data_values '
			+ 'WHERE md_id = "Year Start" AND status = 1'
			, function (err, year_start) {
				if (err) {
					obj.status = 0;
					obj.message = "Something wrong. Please try again";
					return callback(obj);
				} else {
					obj.status = 1;
					obj.result = '';
					obj.year_start = year_start[0].value;
					obj.charge = '';
					return callback(obj);
				}
			})
	}



}

exports.accountReceivablesData = accountReceivablesData;
exports.accountReceivablesFilterData = accountReceivablesFilterData;