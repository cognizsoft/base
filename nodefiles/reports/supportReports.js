
var supportReportData = function (con, callback) {
	var obj = {};

	con.query('SELECT '
		+ 'cs_ticket.ticket_id, '
		+ 'cs_ticket.provider_id, '
        + 'DATE_FORMAT(cs_ticket.date_created, "%b-%y") AS date_created, '
        //+ 'COUNT(if(cs_ticket.status = 1, cs_ticket.status, NULL)) AS ticket_pending, '
        //+ 'COUNT(if(cs_ticket.status = 0, cs_ticket.status, NULL)) AS ticket_resolved, '
        //+ '(COUNT(if(cs_ticket.status = 1, cs_ticket.status, NULL)) + COUNT(if(cs_ticket.status = 0, cs_ticket.status, NULL))) AS ticket_opened, '
        + 'cs_ticket.status, '

        + 'provider.name '

        + 'FROM cs_ticket '

        + 'INNER JOIN provider '
        + 'ON provider.provider_id=cs_ticket.provider_id '

        + 'WHERE cs_ticket.provider_id IS NOT NULL '
        //+ 'GROUP BY DATE_FORMAT(cs_ticket.date_created, "%b-%y"), cs_ticket.provider_id '
        + 'ORDER BY DATE_FORMAT(cs_ticket.date_created, "%b-%y") DESC'
        , function (error, tickets) {
            //console.log(this.sql)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
	            obj.result = tickets;
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Plan not found.";
                            return callback(obj);
                        });
                    }
                    obj.status = 1;
                    
                    return callback(obj);
                });
            }
        })
	
}

var supportReportFilterData = function (con, data, callback) {
	console.log(data)
	//return false;
    var obj = {};
	let cond = 1;
	if (data.provider_name != '' && data.provider_name != undefined) {
		cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
	}
	if (data.start_date != '') {
		cond = cond + ' AND DATE_FORMAT(cs_ticket.date_created, "%Y-%b") >="' + data.start_date + '"';
	}
	if (data.end_date != '') {
		cond = cond + ' AND DATE_FORMAT(cs_ticket.date_created, "%Y-%b") <="' + data.end_date + '"';
	}

	con.query('SELECT '
		+ 'cs_ticket.ticket_id, '
		+ 'cs_ticket.provider_id, '
        + 'DATE_FORMAT(cs_ticket.date_created, "%b-%y") AS date_created, '
        + 'cs_ticket.status, '

        + 'provider.name '

        + 'FROM cs_ticket '

        + 'INNER JOIN provider '
        + 'ON provider.provider_id=cs_ticket.provider_id '

        + 'WHERE cs_ticket.provider_id IS NOT NULL AND ' + cond
        + ' ORDER BY DATE_FORMAT(cs_ticket.date_created, "%b-%y") DESC'
        , function (error, tickets) {
        	console.log(this.sql)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
	            obj.result = tickets;
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Plan not found.";
                            return callback(obj);
                        });
                    }
                    obj.status = 1;
                    
                    return callback(obj);
                });
            }
        })

}

//Customer//
var customerSupportReportData = function (con, callback) {
	var obj = {};

	con.query('SELECT '
		+ 'cs_ticket.ticket_id, '
		+ 'cs_ticket.customer_id, '
        + 'DATE_FORMAT(cs_ticket.date_created, "%b-%y") AS date_created, '
        + 'cs_ticket.status, '

        + 'cs_ticket_details.commented_by, '

        + 'patient.f_name, '
        + 'patient.m_name, '
        + 'patient.l_name '

        + 'FROM cs_ticket '

        + 'INNER JOIN patient '
        + 'ON patient.patient_id=cs_ticket.customer_id '

        + 'LEFT JOIN cs_ticket_details '
        + 'ON cs_ticket_details.ticket_id=cs_ticket.ticket_id AND cs_ticket_details.commented_by = 1 '

        + 'WHERE cs_ticket.customer_id IS NOT NULL '
        + 'GROUP BY cs_ticket_details.ticket_id '
        + 'ORDER BY DATE_FORMAT(cs_ticket.date_created, "%b-%y") DESC'
        , function (error, tickets) {
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
	            obj.result = tickets;
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Plan not found.";
                            return callback(obj);
                        });
                    }
                    obj.status = 1;
                    
                    return callback(obj);
                });
            }
        })
	
}

var customerSupportReportFilterData = function (con, data, callback) {
	console.log(data)
	//return false;
    var obj = {};
	let cond = 1;
	if (data.customer_name != '' && data.customer_name != undefined) {
		cond = cond + ' AND CONCAT_WS(" ",IF(LENGTH(patient.f_name),patient.f_name,NULL),IF(LENGTH(patient.m_name),patient.m_name,NULL),IF(LENGTH(patient.l_name),patient.l_name,NULL)) LIKE "%' + data.customer_name + '%"';
	}
	if (data.start_date != '') {
		cond = cond + ' AND DATE_FORMAT(cs_ticket.date_created, "%Y-%b") >="' + data.start_date + '"';
	}
	if (data.end_date != '') {
		cond = cond + ' AND DATE_FORMAT(cs_ticket.date_created, "%Y-%b") <="' + data.end_date + '"';
	}

	con.query('SELECT '
		+ 'cs_ticket.ticket_id, '
		+ 'cs_ticket.customer_id, '
        + 'DATE_FORMAT(cs_ticket.date_created, "%b-%y") AS date_created, '
        + 'cs_ticket.status, '

        + 'cs_ticket_details.commented_by, '

        + 'patient.f_name, '
        + 'patient.m_name, '
        + 'patient.l_name '

        + 'FROM cs_ticket '

        + 'INNER JOIN patient '
        + 'ON patient.patient_id=cs_ticket.customer_id '

        + 'LEFT JOIN cs_ticket_details '
        + 'ON cs_ticket_details.ticket_id=cs_ticket.ticket_id AND cs_ticket_details.commented_by = 1 '

        + 'WHERE cs_ticket.customer_id IS NOT NULL AND ' + cond
        + 'GROUP BY cs_ticket_details.ticket_id '
        + 'ORDER BY DATE_FORMAT(cs_ticket.date_created, "%b-%y") DESC'
        , function (error, tickets) {
        	console.log(this.sql)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
	            obj.result = tickets;
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Plan not found.";
                            return callback(obj);
                        });
                    }
                    obj.status = 1;
                    
                    return callback(obj);
                });
            }
        })

}

exports.supportReportData = supportReportData;
exports.supportReportFilterData = supportReportFilterData;
exports.customerSupportReportData = customerSupportReportData;
exports.customerSupportReportFilterData = customerSupportReportFilterData;