
var providerInvoiceReports = function (con, data, callback) {
	con.query('SELECT status_id,value FROM master_data_values WHERE md_id=?'
		,
		['Provider Invoice Status']
		, function (error, statusname, fields) {
			if (error) {
				var obj = {
					"status": 0,
					"message": "Something wrong. Please try again",
				}
				return callback(obj);
			}
			else {
				con.query('SELECT provider_id,name FROM provider WHERE status=? AND deleted_flag=?'
					,
					[1, 0]
					, function (error, provider, fields) {
						if (error) {
							var obj = {
								"status": 0,
								"message": "Something wrong. Please try again",
							}
							return callback(obj);
						}
						else {
							/*con.query('SELECT '
								+ 'provider_invoice.provider_invoice_id,provider_invoice.invoice_number,provider_invoice.total_amt, provider_invoice.total_hps_discount, provider_invoice.check_amount, provider_invoice.bank_name,provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y") as date_paid, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, '
								+ 'provider.provider_id, provider.name, master_data_values.value, provider.phone, provider_location.location_name '
								+ 'FROM provider_invoice '
								+ 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
								+ 'INNER JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
								+ 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id AND md_id=?'
								+ 'ORDER BY provider_invoice.date_modified DESC'
								,*/
							let cond = 1;

							//cond = cond + ' AND YEAR(date_created) = YEAR(NOW()) AND MONTH(date_created)=MONTH(NOW())';
							if (data.status_id == 0) {
								//cond = cond + ' AND YEAR(provider_invoice.date_created) = YEAR(NOW()) AND MONTH(provider_invoice.date_created)=MONTH(NOW())';
							} else if (data.status_id == 3) {
								cond = cond + ' AND YEAR(provider_invoice.date_created) = YEAR(NOW()) AND MONTH(provider_invoice.date_created)=MONTH(NOW()) AND provider_invoice.mdv_invoice_status_id IN (1,3)';
							} else if (data.status_id == 1) {
								cond = cond + ' AND YEAR(provider_invoice.date_created) = YEAR(NOW()) AND MONTH(provider_invoice.date_created)=MONTH(NOW()) AND provider_invoice.mdv_invoice_status_id IN (1,3,4)';
							} else {
								cond = cond + ' AND YEAR(provider_invoice.date_created) = YEAR(NOW()) AND MONTH(provider_invoice.date_created)=MONTH(NOW()) AND provider_invoice.mdv_invoice_status_id ="' + data.status_id + '"';
							}

							con.query('SELECT '
								+ 'provider_invoice.provider_invoice_id,provider_invoice.invoice_number,provider_invoice.total_amt, provider_invoice.total_hps_discount, provider_invoice.check_amount, provider_invoice.bank_name,provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y") as date_paid, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, '
								+ 'provider.provider_id,provider.name, master_data_values.value, provider.phone, provider_location.location_name  '
								+ 'FROM provider_invoice '
								+ 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
								+ 'INNER JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag = 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
								+ 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id  AND md_id=? '
								+ 'WHERE ' + cond + ' ORDER BY provider_invoice.date_modified DESC'
								,
								['Provider Invoice Status']
								, function (error, rows, fields) {

									if (error) {

										var obj = {
											"status": 0,
											"message": "Something wrong. Please try again",
										}
										return callback(obj);
									}
									else {
										var providerInvID = rows && [...new Set(rows.map(item => item.provider_invoice_id))];
										con.query('SELECT provider_refund_details.provider_invoice_id, '
											+ 'paid_amount as iou_paid_amount '
											+ 'FROM provider_refund '
											+ 'INNER JOIN provider_refund_details ON provider_refund_details.refund_id = provider_refund.refund_id '
											+ 'WHERE provider_refund_details.provider_invoice_id IN(?) AND provider_refund.iou_flag=? AND provider_refund.status=? '
											,
											[
												providerInvID, 1, 7
											]
											, function (iouError, iouAmt, fields) {
												if (iouError) {
													var obj = {
														"status": 1,
														"result": rows,
														"statusname": statusname,
														"provider": provider,
													}
													return callback(obj);
												} else {
													var obj = {
														"status": 1,
														"result": rows,
														"statusname": statusname,
														"provider": provider,
														"iouAmt": iouAmt,
													}
													return callback(obj);
												}
											})
									}
								});
						}
					});
			}
		});
}

var providerInvoiceReportsFilter = function (con, data, callback) {

	let cond = 1;
	if (data.paid_start_date != '') {
		cond = cond + ' AND DATE_FORMAT(provider_invoice.date_created, "%Y-%m-%d") >="' + data.paid_start_date + '"';
	}
	if (data.paid_end_date != '') {
		cond = cond + ' AND DATE_FORMAT(provider_invoice.date_created, "%Y-%m-%d") <="' + data.paid_end_date + '"';
	}
	if (data.status != '') {
		cond = cond + ' AND provider_invoice.mdv_invoice_status_id ="' + data.status + '"';
	}
	if (data.provider_name != '') {
		cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
	}
	if (data.provider_phone != '') {
		cond = cond + ' AND provider.phone LIKE "%' + data.provider_phone + '%"';
	}
	con.query('SELECT '
		+ 'provider_invoice.provider_invoice_id,provider_invoice.invoice_number,provider_invoice.total_amt, provider_invoice.total_hps_discount, provider_invoice.check_amount, provider_invoice.bank_name,provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y") as date_paid, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, '
		+ 'provider.provider_id,provider.name, master_data_values.value, provider.phone, provider_location.location_name  '
		+ 'FROM provider_invoice '
		+ 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
		+ 'INNER JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag = 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
		+ 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id  AND md_id=? '
		+ 'WHERE ' + cond + ' ORDER BY provider_invoice.date_modified DESC'
		,
		['Provider Invoice Status']
		, function (error, rows, fields) {

			if (error) {
				var obj = {
					"status": 0,
					"message": "Something wrong. Please try again",
				}
				return callback(obj);
			}
			else {
				var providerInvID = rows && [...new Set(rows.map(item => item.provider_invoice_id))];
				con.query('SELECT provider_refund_details.provider_invoice_id, '
					+ 'paid_amount as iou_paid_amount '
					+ 'FROM provider_refund '
					+ 'INNER JOIN provider_refund_details ON provider_refund_details.refund_id = provider_refund.refund_id '
					+ 'WHERE provider_refund_details.provider_invoice_id IN(?) AND provider_refund.iou_flag=? AND provider_refund.status=? '
					,
					[
						providerInvID, 1, 7
					]
					, function (iouError, iouAmt, fields) {
						if (iouError) {
							var obj = {
								"status": 1,
								"result": rows,
							}
							return callback(obj);
						} else {
							var obj = {
								"status": 1,
								"result": rows,
								"iouAmt": iouAmt,
							}
							return callback(obj);
						}
					})

			}
		});
}

var providerAccountPayableFilter = function (con, data, callback) {

	if (data.filter_type != '') {
		let cond = 1;
		// current week
		if (data.filter_type == 1) {
			cond = cond + ' AND WEEKOFYEAR(patient_procedure.procedure_date)=WEEKOFYEAR(NOW())'
		} else if (data.filter_type == 2) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND MONTH(patient_procedure.procedure_date)=' + data.month;
		} else if (data.filter_type == 3) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year + ' AND QUARTER(patient_procedure.procedure_date)=' + data.quarter;
		} else if (data.filter_type == 4) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = ' + data.year;
		} else if (data.filter_type == 5) {
			cond = cond + ' AND YEAR(patient_procedure.procedure_date) = YEAR(CURDATE()) AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
		} else if (data.filter_type == 6) {
			if (data.start_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") >="' + data.start_date + '"';
			}
			if (data.end_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_procedure.procedure_date, "%Y-%m-%d") <="' + data.end_date + '"';
			}
		}

		//if (data.provider_name != '') {
		//	cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
		//}
		con.query('SELECT '
			+ 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") as procedure_date,patient_procedure.hps_discount_amt,patient_procedure.procedure_amt, payment_plan.plan_number, '
			+ 'provider.provider_id,provider.name,master_data_values.value as inv_status,provider_invoice.check_amount,provider_invoice.mdv_invoice_status_id,patient.f_name,patient.m_name,patient.l_name, provider_refund.iou_flag, provider_refund.refund_due, '
			+ '(SELECT SUM(paid_amount) FROM provider_refund_details WHERE provider_refund.refund_id = provider_refund_details.refund_id AND provider_refund_details.status=4) as refund_recived '
			+ 'FROM patient_procedure '
			+ 'INNER JOIN provider ON provider.provider_id=patient_procedure.provider_id '
			+ 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_procedure.pp_id '
			+ 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
			+ 'INNER JOIN patient ON patient.patient_id = credit_applications.patient_id '
			+ 'LEFT JOIN provider_invoice_detial ON provider_invoice_detial.pp_id = patient_procedure.pp_id '
			+ 'LEFT JOIN provider_invoice ON provider_invoice.provider_invoice_id = provider_invoice_detial.provider_invoice_id '
			+ 'LEFT JOIN master_data_values ON master_data_values.status_id = provider_invoice.mdv_invoice_status_id AND master_data_values.md_id="Provider Invoice Status" '
			+ 'LEFT JOIN provider_refund ON provider_refund.pp_id = patient_procedure.pp_id '
			+ 'WHERE ' + cond + ' ORDER BY provider_invoice.date_modified DESC'

			, function (error, rows, fields) {
				console.log(error)
				if (error) {
					var obj = {
						"status": 0,
						"message": "Something wrong. Please try again",
					}
					return callback(obj);
				}
				else {
					con.query('SELECT value FROM master_data_values WHERE md_id=?'
						,
						['Year Start']
						, function (error, yearStart, fields) {
							if (error) {
								var obj = {
									"status": 1,
									"result": rows,
								}
								return callback(obj);
							} else {
								var obj = {
									"status": 1,
									"result": rows,
									"yearStart": (yearStart.length > 0) ? yearStart[0].value : ''
								}
								return callback(obj);
							}
						})
				}
			});
	} else {
		con.query('SELECT value FROM master_data_values WHERE md_id=?'
			,
			['Year Start']
			, function (error, yearStart, fields) {
				if (error) {
					var obj = {
						"status": 1,
						"result": '',
					}
					return callback(obj);
				} else {
					var obj = {
						"status": 1,
						"result": '',
						"yearStart": (yearStart.length > 0) ? yearStart[0].value : ''
					}
					return callback(obj);
				}
			})
	}


}

var customerCreditChargeFilter = function (con, data, callback) {

	if (data.filter_type != '') {
		let cond = ' (patient_invoice.invoice_status=1 OR patient_invoice.invoice_status=4) AND patient_invoice.invoice_id IN(SELECT invoice_id FROM patient_invoice_payment WHERE payment_method = 2)';
		// current week
		if (data.filter_type == 1) {
			cond = cond + ' AND WEEKOFYEAR(patient_invoice.due_date)=WEEKOFYEAR(NOW())'
		} else if (data.filter_type == 2) {
			cond = cond + ' AND YEAR(patient_invoice.due_date) = ' + data.year + ' AND MONTH(patient_invoice.due_date)=' + data.month;
		} else if (data.filter_type == 3) {
			cond = cond + ' AND YEAR(patient_invoice.due_date) = ' + data.year + ' AND QUARTER(patient_invoice.due_date)=' + data.quarter;
		} else if (data.filter_type == 4) {
			cond = cond + ' AND YEAR(patient_invoice.due_date) = ' + data.year;
		} else if (data.filter_type == 5) {
			cond = cond + ' AND YEAR(patient_invoice.due_date) = YEAR(CURDATE()) AND DATE_FORMAT(patient_invoice.due_date, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
		} else if (data.filter_type == 6) {
			if (data.start_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%Y-%m-%d") >="' + data.start_date + '"';
			}
			if (data.end_date != '') {
				cond = cond + ' AND DATE_FORMAT(patient_invoice.due_date, "%Y-%m-%d") <="' + data.end_date + '"';
			}
		}

		//if (data.provider_name != '') {
		//	cond = cond + ' AND provider.name LIKE "%' + data.provider_name + '%"';
		//}
		con.query('SELECT '
			+ 'DATE_FORMAT(patient_invoice.due_date, "%m/%d/%Y") as due_date,patient.f_name,patient.m_name,patient.l_name, '
			+ '(patient_invoice.previous_late_fee + patient_invoice.previous_fin_charge + patient_invoice.late_fee_due + patient_invoice.fin_charge_due + patient_invoice.payment_amount) as invoice_amt, patient_invoice.credit_charge_amt, '
			+ '(patient_invoice.late_fee_received + patient_invoice.fin_charge_received + patient_invoice.paid_amount) as total_paid,patient_invoice.invoice_number '
			//+ '(SELECT SUM(amount_paid) FROM patient_invoice_payment WHERE patient_invoice_payment.invoice_id = patient_invoice.invoice_id AND patient_invoice_payment.payment_method=2) as amount_recived '
			+ 'FROM patient_invoice '
			+ 'INNER JOIN credit_applications ON credit_applications.application_id = patient_invoice.application_id '
			+ 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
			+ 'WHERE ' + cond + ' ORDER BY patient_invoice.date_modified DESC'

			, function (error, rows, fields) {
				if (error) {
					var obj = {
						"status": 0,
						"message": "Something wrong. Please try again",
					}
					return callback(obj);
				}
				else {
					con.query('SELECT value FROM master_data_values WHERE md_id=?'
						,
						['Year Start']
						, function (error, yearStart, fields) {
							if (error) {
								var obj = {
									"status": 1,
									"result": rows,
								}
								return callback(obj);
							} else {
								var obj = {
									"status": 1,
									"result": rows,
									"yearStart": (yearStart.length > 0) ? yearStart[0].value : ''
								}
								return callback(obj);
							}
						})
				}
			});
	} else {
		con.query('SELECT value FROM master_data_values WHERE md_id=?'
			,
			['Year Start']
			, function (error, yearStart, fields) {
				if (error) {
					var obj = {
						"status": 1,
						"result": '',
					}
					return callback(obj);
				} else {
					var obj = {
						"status": 1,
						"result": '',
						"yearStart": (yearStart.length > 0) ? yearStart[0].value : ''
					}
					return callback(obj);
				}
			})
	}

}

exports.providerInvoiceReports = providerInvoiceReports;
exports.providerInvoiceReportsFilter = providerInvoiceReportsFilter;
exports.providerAccountPayableFilter = providerAccountPayableFilter;
exports.customerCreditChargeFilter = customerCreditChargeFilter;