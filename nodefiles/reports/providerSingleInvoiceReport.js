module.exports = (data1, data2, logoimg) => {
   var html = '';
   let total_amt = 0.00;
   let total_hps_discount = 0.00;
   let check_amount = 0.00;

	var amount = data2.result.reduce(function (accumulator, currentValue, currentindex) {
        if (currentindex == 0) {
            accumulator['total_amount'] = currentValue.loan_amount;
            accumulator['discount_amount'] = currentValue.hps_discount_amt;
        } else {
            accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
            accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
        }
        return accumulator
    }, []);


    var html = '';
	data2.result.forEach(function (element, idx) {
	   html = html+'<tr>'
	   +  '<td>'+(idx+1)+'</td>'
	   + '<td>'+element.plan_number+'</td>' 
	   + '<td>'+element.patient_ac+'</td>'
	   + '<td>'+element.application_no+'</td>'
	   + '<td>'+element.f_name+'</td>'
	   + '<td>'+element.m_name+'</td>'
	   + '<td>'+element.l_name+'</td>'
	   + '<td>'+element.dob+'</td>'
	   + '<td>'+element.peimary_phone+'</td>'
	   + '<td>'+element.City+'</td>'
	   + '<td>'+element.name+'</td>'
	   + '<td>$'+parseFloat(element.procedure_amt).toFixed(2)+'</td>'
	   + '<td>$'+parseFloat(element.loan_amount).toFixed(2)+'</td>'
	   + '</tr>';
	});
	var html2 ='';
	if(data2.txn_no != null){
	html2 = html2+'<h2 class="justify-center">Payment Details</h2>'
	        +'<div class="payment-detail">'
     	    +'<table cellpadding="0" cellspacing="0" class="blue-tble" width="100%">'
     		+'<tr>'
     		+'<td><strong>Check Number : </strong>'+data2.txn_no+'</td>'
     		+'<td><strong>Check Amount : </strong>$'+data2.check_amount+'</td>'
     		+'</tr>'
     		+'<tr>'
			+'<td><strong>Bank Name : </strong>'+data2.bank_name+'</td>'
			+'<td><strong>Paid Date : </strong>'+data2.date_paid+'</td>'
     		+'</tr>'
     		+'<tr>'
     		+'<td><strong>Note : </strong>'+data2.comment+'</td>'
     		+'<td></td>'
     		+'</tr>'
        	+'</table>'
            +'</div>'
	}
   return `
   <!doctype html>
<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <title>PDF Result Template</title>
      <style>
         .invoice-box {
         max-width: 1200px;
         margin: auto;
         padding: 0px;
         font-size: 8px;
         line-height: 15px;
         font-family: 'Arial', sans-serif;
         color: #303030;
         }
         .margin-top {
         margin-top: 50px;
         }
         .justify-center {
         text-align: center;
         }
         .invoice-box table {
         width: 100%
         line-height: inherit;
         text-align: left;
      border:solid 1px #ddf1ff; border-collapse:collapse;}
      .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
      .provider-info{width:40%; float:left}
      .invoice-info{width:58%; float:right;}
      .blue-tble td{background:#f1f9ff; padding:4px 15px; font-size:8px;}
      .blue-tble th{background:#0e5d97; color:#fff;padding:4px 15px; font-size:8px;}
      .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
      .info-bx:after{margin-bottom:20px;}
      .detail-table th{background:#0e5d97; color:#fff;padding:2px 2px; font-size:8px; line-height:10px;}
      .detail-table td{padding:2px 2px; font-size:8px;}
      .payment-detail{background:#f1f9ff;}
      .payment-detail table td {padding:4px 15px;}
      .pro-phone-padding{padding-bottom:20px;}
      </style>
      
   </head>
   <body>
      <div class="invoice-box">
      <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
     <h1 class="justify-center">Provider Invoice</h1>
     <div class="info-bx">
        <div class="provider-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble" width="100%" height="150px">
              <tr>
                 <th>Provider Information</th>
              </tr>
              <tr>
                 <td><strong>Name :</strong> ${data2.provider.name}</td>
              </tr>
              <tr>
                 <td><strong>Address :</strong> ${data2.provider.address1+', '+data2.provider.city+', '+data2.provider.state_name+', '+data2.provider.zip_code}</td>
              </tr>
              </tr>
                 <td class="pro-phone-padding"><strong>Phone :</strong> ${data2.provider.primary_phone}</td>
              </tr>
           </table>
        </div>
     
        <div class="invoice-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble" width="100%" height="150px">
              <tr>
                 <th>Invoice Information</th>
                 <th></th>
              </tr>
              </tr>
                 <td><strong>Invoice Number :</strong> ${data1.invoice_number}</td>
                 <td><strong>Discount Amount :</strong> $${parseFloat(amount.discount_amount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>Invoice Date :</strong> ${data2.date_created}</td>
                 <td><strong>Total Amount :</strong> $${parseFloat(parseFloat(amount.total_amount).toFixed(2)-parseFloat(amount.discount_amount).toFixed(2)).toFixed(2)}</td>
              </tr>
              <tr>
                 <td><strong>No of accounts included :</strong> ${data2.result.length}</td>
                 <td><strong>Status :</strong> ${data2.invoice_status}</td>
              </tr>
              <tr>
                 <td><strong>Invoice Amount: </strong> $${parseFloat(amount.total_amount).toFixed(2)}</td>
                 <td></td>
              </tr>
              
           </table>
        </div>
     </div>

     
     ${html2}
     
     <div class="detail-bx">
        <h2 class="justify-center">Invoice Detail</h2>
        <div class="detail-table">
           <table cellpadding="0" cellspacing="0">
              <tr>
                 <th>SN#</th>
                 <th>Loan No</th>
                 <th>A/C Number</th>
                 <th>Application No</th>
                 <th>First Name</th>
                 <th>Middle Name</th>
                 <th>Last Name</th>
                 <th>DOB</th>
                 <th>Phone</th>
                 <th>City</th>
                 <th>State</th>
                 <th>Procedure Amount</th>
                 <th>Loan Amount</th>
              </tr>
              ${html}
           </table>
        </div>
     </div>
     
        
      </div>
   </body>
</html>
`;
};