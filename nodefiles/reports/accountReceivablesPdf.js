module.exports = (dataFilter,data,logoimg) => {
    let moment = require('moment');
   let fileName = '';
   if (dataFilter.filter_type == 1) {
      moment.updateLocale('en', {
         week: {
            dow: 1,
            doy: 1
         }
      });

      let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
      let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
      fileName = 'Weekly Accounts Receivable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
   } else if (dataFilter.filter_type == 2) {
      let monthName = moment().month(dataFilter.month - 1).format("MMM");
      fileName = 'Monthly Accounts Receivable Report (' + monthName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 3) {
      let queterName = ''
      if (dataFilter.quarter == 1) {
         queterName = 'Q1';
      } else if (dataFilter.quarter == 2) {
         queterName = 'Q2';
      } else if (dataFilter.quarter == 3) {
         queterName = 'Q3';
      } else if (dataFilter.quarter == 4) {
         queterName = 'Q4';
      }
      fileName = 'Quarterly Accounts Receivable Report (' + queterName + '-' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 4) {
      fileName = 'Yearly Accounts Receivable Report (' + dataFilter.year + ')';
   } else if (dataFilter.filter_type == 5) {
      let currentDate = moment().format('MM/DD/YYYY');
      fileName = 'Year to Date Accounts Receivable Report (01/01/' + dataFilter.year + '-' + currentDate + ')';
   } else if (dataFilter.filter_type == 6) {
      let loanStart = moment(dataFilter.start_date).format('MM/DD/YYYY');
      let loanEnd = moment(dataFilter.end_date).format('MM/DD/YYYY');
      fileName = 'By Date Accounts Receivable Report (' + loanStart + '-' + loanEnd + ')';
   }
   
    var html = '';
    let total_prin_amt = 0.00;
    let total_prin_due = 0.00;
    let total_prin_bal = 0.00;
    let total_int_due = 0.00;
    let total_late_fee = 0.00;
    let total_fin_charge = 0.00;
    let total_due = 0.00;
    let total_amt_rcvd = 0.00;

    data.result.forEach(function (element, idx) {
        element.main_amount = (element.main_amount) ? element.main_amount : 0;
        element.late_fee = (element.late_fee) ? element.late_fee : 0;
        element.fin_fee = (element.fin_fee) ? element.fin_fee : 0;
        element.remaining_amount = (element.remaining_amount) ? element.remaining_amount : 0;
        element.interest_due = (element.interest_due) ? element.interest_due : 0;
        element.inv_interest = (element.inv_interest) ? element.inv_interest : 0;
        element.refund_amt = (element.refund_amt) ? element.refund_amt : 0
        var prin_bal;
        if((element.main_amount - element.remaining_amount) < element.main_amount && (element.main_amount - element.remaining_amount) !== 0) {
           prin_bal = element.main_amount - element.remaining_amount;
        } else if((element.main_amount - element.remaining_amount) == 0) {
           prin_bal = element.main_amount;
        } else {
           prin_bal = 0;
        }

        total_prin_amt = total_prin_amt + element.main_amount;
        total_prin_due = total_prin_due + element.remaining_amount;
        total_prin_bal = total_prin_bal + prin_bal;
        total_int_due = total_int_due + element.interest_due;
        total_late_fee = total_late_fee + element.late_fee;
        total_fin_charge = total_fin_charge + element.fin_fee;
        total_due = total_due + element.remaining_amount + element.interest_due;
        total_amt_rcvd = total_amt_rcvd + ((element.main_amount - element.remaining_amount) + element.inv_interest + element.late_fee + element.fin_fee);

        html = html + '<tr>'
            + '<td>' + element.plan_number + '</td>'
            + '<td>' + element.name + '</td>'
            + '<td>' + element.f_name + ' ' + element.m_name + ' ' + element.l_name + '</td>'
            + '<td>' + element.loan_date + '</td>'
            + '<td>$' + element.main_amount.toFixed(2) + '</td>'
            + '<td>$' + element.remaining_amount.toFixed(2) + '</td>'
            + '<td>$' + prin_bal.toFixed(2) + '</td>'
            + '<td>$' + element.interest_due.toFixed(2) + '</td>'
            + '<td>$' + element.late_fee.toFixed(2) + '</td>'
            + '<td>$' + element.fin_fee.toFixed(2) + '</td>'
            + '<td>$' + (element.remaining_amount + element.interest_due).toFixed(2) + '</td>'
            + '<td>$' + ((element.main_amount - element.remaining_amount) + element.inv_interest + element.late_fee + element.fin_fee).toFixed(2) + '</td>'
            + '<td>$' + element.refund_amt.toFixed(2) + '</td>'
            + '</tr>';
    });
    html = html + '<tr>'
        + '<td>Total</td>'
        + '<td></td>'
        + '<td></td>'
        + '<td></td>'
        + '<td>$' + total_prin_amt.toFixed(2) + '</td>'
        + '<td>$' + total_prin_due.toFixed(2) + '</td>'
        + '<td>$' + total_prin_bal.toFixed(2) + '</td>'
        + '<td>$' + total_int_due.toFixed(2) + '</td>'
        + '<td>$' + total_late_fee.toFixed(2) + '</td>'
        + '<td>$' + total_fin_charge.toFixed(2) + '</td>'
        + '<td>$' + total_due.toFixed(2) + '</td>'
        + '<td>$' + total_amt_rcvd.toFixed(2) + '</td>'
        + '</tr>';
    return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>${fileName}</title>
       <style>
            html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 12px;
          line-height: 16px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
       .detail-table td{padding:5px 2px; font-size:10px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">${fileName}</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>Loan No</th>
                  <th>Provider Name</th>
                  <th>Customer Name</th>
                  <th>Loan Date</th>
                  <th>Prin Amt</th>
                  <th>Prin Due</th>
                  <th>Prin Bal</th>
                  <th>Int Due</th>
                  <th>Late Fee</th>
                  <th>Fin Charge</th>
                  <th>Total Due</th>
                  <th>Amt Rcvd</th>
                  <th>Refund Amt</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};