module.exports = (data, logoimg) => {
   var html = '';
   let total_installment_amt = 0.00;
   let total_additional_amt = 0.00;
   let total_paid_amt = 0.00;
   data.result.forEach(function (element, idx) {
      if (element.invoice_status == 3) {
         element.payment_amount = (parseFloat(element.previous_late_fee) + parseFloat(element.previous_fin_charge) + parseFloat(element.late_fee_due) + parseFloat(element.fin_charge_due) + parseFloat(element.payment_amount)).toFixed(2)
      } else {
         element.payment_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.payment_amount)).toFixed(2)
         element.paid_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.paid_amount)).toFixed(2)   
      }
      //element.payment_amount = (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(element.payment_amount)).toFixed(2)
      element.payment_amount = parseFloat(element.payment_amount);

      //element.paid_amount = (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(element.paid_amount)).toFixed(2)
      element.paid_amount = parseFloat(element.paid_amount);

      total_installment_amt = total_installment_amt + element.payment_amount;

      total_additional_amt = total_additional_amt + element.additional_amount;

      total_paid_amt = total_paid_amt + element.paid_amount + element.additional_amount;

      element.payment_date = (element.payment_date)?element.payment_date:'-';
      
      if(element.withdrawal_date) {
          var withdrawal_date = element.withdrawal_date
      } else {
          var withdrawal_date = "-"
      }
      
      html = html + '<tr>'
         + '<td>' + element.invoice_number + '</td>'
         + '<td>' + element.f_name + ' ' + element.m_name + ' ' + element.l_name + '</td>'
         + '<td>' + element.address1 + '</td>'
         + '<td>' + element.City + '</td>'
         + '<td>' + element.state_name + '</td>'
         + '<td>' + element.zip_code + '</td>'
         + '<td>' + element.peimary_phone + '</td>'
         + '<td>' + element.payment_date + '</td>'
         + '<td>' + element.due_date + '</td>'
         + '<td>$' + parseFloat(element.payment_amount).toFixed(2) + '</td>'
         + '<td>' + withdrawal_date + '</td>'
         + '<td>' + element.invoice_status_name + '</td>'
         + '</tr>';
   });
   if(data.result.length){
      html = html + '<tr>'
      + '<td><b>Total</b></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td></td>'
      + '<td><b>$' + total_installment_amt.toFixed(2) + '</b></td>'
      + '<td></td>'
      + '<td></td>'
      + '</tr>';
   }
   
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Current Weekly and Monthly Invoice Invoice Reports</title>
        <style>
            html{zoom: 0.7;}
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0px;
           font-size: 12px;
           line-height: 16px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           .margin-top {
           margin-top: 50px;
           }
           .justify-center {
           text-align: center;
           
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .provider-info{width:48%; float:left}
        .invoice-info{width:48%; float:right;}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
        .detail-table td{padding:5px 2px; font-size:10px;}
           
           
        </style>
     </head>
     <body>
        <div class="invoice-box">
        <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
       <h2 class="justify-center">Withdrawal Day Report</h2>
       <div class="detail-bx">
          <div class="detail-table">
             <table cellpadding="0" cellspacing="0">
                <tr>
                   <th>Invoice ID</th>
                   <th>Customer Name</th>
                   <th>Address</th>
                   <th>City</th>
                   <th>State</th>
                   <th>Zip</th>
                   <th>Phone</th>
                   <th>Invoice date</th>
                   <th>Invoice due date</th>
                   <th>Invoice amt</th>
                   <th>Withdrawal Day</th>
                   <th>Invoice status</th>
                </tr>
                ${html}
             </table>
          </div>
       </div>
       
          
        </div>
     </body>
  </html>
      `;
};