module.exports = (data, lateFee, LastPayment, planDetails, amountDetails, logoimg) => {
    
    let date = require('date-and-time');
    let now = new Date();
    let currentDate = date.format(now, 'MM/DD/YYYY');
    var html = '';

    data.forEach(function (element, idx) {



        var LastPaymentDet = element.lastDetails.filter(function (number) {
            return (1 == number.invoice_status || 4 == number.invoice_status);
        });
        
        if (LastPaymentDet.length > 0) {
            var LastPayment = LastPaymentDet[0];
            var privaceAmt = LastPayment.paid_amount;
            privaceAmt += LastPayment.additional_amount;

            privaceAmt += LastPayment.late_fee_received;
            privaceAmt += LastPayment.fin_charge_received;
            var last_date = LastPayment.last_date;
        } else {
            var privaceAmt = 0;
            var last_date = '';
        }
        //var blacAmt = element.totalAmt - element.recivedAmt;



        var lateFreeApply = 0;
        var finCharge = 0;
        var creditCharge = 0;
        if (lateFee.length > 0) {
            var late = lateFee.filter(function (item) {
                return item.md_id == 'Late Fee';
            });
            lateFreeApply = (late.length > 0) ? parseFloat(late[0].value) : 0;
            var financial = lateFee.filter(function (item) {
                return item.md_id == 'Financial Charges';
            });
            finCharge = (financial.length > 0) ? parseFloat(financial[0].value) : 0;
            var creditChargePct = lateFee.filter(function (item) {
                return item.md_id == 'Credit Charge';
            });
            creditCharge = (creditChargePct.length > 0) ? parseFloat(creditChargePct[0].value) : 0;
        }

        var preNewLate = 0;
        var preNewLateStr = '';
        var preNewFin = 0;
        var preNewFinStr = '';
        /*if (element.previous_late_fee != null) {
            preNewLate = element.late_fee_received + element.previous_late_fee;
            //preNewLateStr = ' ($'+parseFloat(element.previous_late_fee).toFixed(2)+'+$'+parseFloat(element.late_fee_received).toFixed(2)+') ';
        } else {
            preNewLate = element.late_fee_received;
        }
        if (element.previous_fin_charge != null) {
            preNewFin = element.fin_charge_received + element.previous_fin_charge;
            //preNewFinStr = ' ($'+parseFloat(element.previous_fin_charge).toFixed(2)+'+$'+parseFloat(element.fin_charge_amt).toFixed(2)+') ';
        } else {
            preNewFin = element.fin_charge_received;
        }*/
        if (element.invoice_status == 3) {
            preNewFin = element.fin_charge_due + element.previous_fin_charge;
            preNewLate = element.late_fee_due + element.previous_late_fee;
        } else {
            preNewFin = element.fin_charge_received;
            preNewLate = element.late_fee_received;
        }
        element.payment_amount += preNewLate;
        element.payment_amount += preNewFin;
        var planHtml = '';
        var installmentTotal = 0;
        var missedPayment = 0;
        var latePayment = 0;
        var nextFinCharge = 0;
        var ACPrincipal = 0;
        var ACOutstanding = 0;
        element.child.forEach(function (ele, idx) {
            missedPayment = missedPayment + parseInt(ele.missed_payment);
            latePayment = latePayment + parseInt(ele.late_payment);
            installmentTotal = installmentTotal + parseFloat(ele.invoice_amount);
            nextFinCharge += parseFloat((ele.invoice_amount * finCharge / 100).toFixed(2));
            ACPrincipal += ele.loan_amount;
            ACOutstanding += ele.remaining_amount;
            planHtml = planHtml + `<tr>
            <td>${ele.plan_number}</td>
            <td>${ele.provider_name}</td>
            <td>$${parseFloat(ele.invoice_amount).toFixed(2)}</td>
            <td>$${parseFloat(ele.loan_amount).toFixed(2)}</td>
            <td>${(ele.remaining_amount > 0) ? '$' + parseFloat(ele.remaining_amount).toFixed(2) : '-'}</td>
            </tr>`
        })
        var checkIn=0;
        
        var dueDetail = element.lastDetails.filter(function (number) {
            
            if(new Date(number.due_date) <= new Date(element.due_date) && checkIn == 0){
                checkIn = ((number.invoice_status == 1 || number.invoice_status == 4) && new Date(number.due_date) < new Date(element.due_date))?1:0;
                if(checkIn == 0){
                    return (new Date(number.due_date) <= new Date(element.due_date) && (number.late_fee_due > 0 || number.fin_charge_due) > 0);
                }
            }
            
        });
        
        dueDetail.forEach(function (ele, idx) {
            let date = new Date(ele.due_date);  // 2009-11-10
            let month = date.toLocaleString('default', { month: 'long' });
            installmentTotal = installmentTotal + parseFloat(ele.late_fee_due);
            if(idx==0){
                planHtml += `<tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>`    
            }
            planHtml = planHtml + `<tr>
            <td>${(idx == 0) ? 'Late Fee' : ''}</td>
            <td>${month}</td>
            <td>$${parseFloat(ele.late_fee_due).toFixed(2)}</td>
            <td></td>
            <td></td>
            </tr>`
        })

        dueDetail.forEach(function (ele, idx) {
            let date = new Date(ele.due_date);  // 2009-11-10
            let month = date.toLocaleString('default', { month: 'long' });
            installmentTotal = installmentTotal + parseFloat(ele.fin_charge_due);
            if(idx==0){
                planHtml += `<tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                </tr>`    
            }
            planHtml = planHtml + `<tr>
            <td>${(idx == 0) ? 'Financial Charge' : ''}</td>
            <td>${month}</td>
            <td>$${parseFloat(ele.fin_charge_due).toFixed(2)}</td>
            <td></td>
            <td></td>
            </tr>`
        })

        var totalAmount = parseFloat(element.payment_amount) + lateFreeApply + nextFinCharge;

        if (new Date(element.child[0].due_date) < new Date()) {
            var crditAmount = totalAmount;
        } else {
            var crditAmount = parseFloat(element.payment_amount);
        }
        var coHtml = '';
        var customerAddress = '';
        if(element.child[0].primary_bill_pay_flag == 1){

            var YesNo = (element.child[0].primary_bill_pay_flag) ? 'Yes' : 'No'
            coHtml = `<div class="inner-mid"><ul class="">
            <li><strong>Co-signer Name: </strong>${element.child[0].co_f_name + ' ' + element.child[0].co_m_name + ' ' + element.child[0].co_l_name}</li>
            <li><strong>Co-signer Phone: </strong>${element.child[0].co_peimary_phone}</li>
            <li><strong>Primary Payer is Co-signer: </strong>${YesNo}</li></ul></div>`

            customerAddress = `<div class="bottom-address">
                <h3>${element.child[0].co_f_name + ' ' + element.child[0].co_m_name + ' ' + element.child[0].co_l_name}</h3>
                <div>${element.child[0].co_address1 + ' ' + element.child[0].co_address2}</div>
                <div>${element.child[0].co_city + ', ' + element.child[0].co_state_name + '-' + element.child[0].co_zip_code}</div>
            </div>`

        }else{
            coHtml = `<div class="inner-mid" style="border:0px"> &nbsp;</div>`
            customerAddress = `<div class="bottom-address">
                <h3>${element.child[0].f_name + ' ' + element.child[0].m_name + ' ' + element.child[0].l_name}</h3>
                <div>${element.child[0].address1 + ' ' + element.child[0].address2}</div>
                <div>${element.child[0].City + ', ' + element.child[0].state_name + '-' + element.child[0].zip_code}</div>
            </div>`
        }
        html = html + `
      <div class="invoice-box" style="height: 730px; position: relative;">
     <div class="header-row">
	 <div class="outer-box">
     <div class="inner-left"><img title="Health Partner" src="${logoimg}" width="200"/>
     <br><span>5720 Creedmoor Road, Suite 103</span><br><span>Raleigh, NC 27612</span>
     </div>
     
     ${coHtml}
     <div class="inner-right">
		<ul class="">
			<li><strong>Account no:</strong>${element.child[0].patient_ac}</li>
			<li><strong>Customer name: </strong>${element.child[0].f_name + ' ' + element.child[0].m_name + ' ' + element.child[0].l_name}</li>
			<li><strong>Address: </strong>${element.child[0].address1 + ' ' + element.child[0].address2} <br>
            ${element.child[0].City + ', ' + element.child[0].state_name + '-' + element.child[0].zip_code}<br>
            ${element.child[0].peimary_phone}
			</li>
            <li><strong>A/C Principal Amount : </strong>${(ACPrincipal > 0) ? '$' + parseFloat(ACPrincipal).toFixed(2) : '-'}</li>
            <li><strong>A/C Outstanding Principal : </strong>${(ACOutstanding > 0) ? '$' + parseFloat(ACOutstanding).toFixed(2) : '-'}</li>
            <li><strong>Late/Missed Payments: </strong>${element.MissCount}</li>
            
		</ul>
     </div>
	 </div>
	 <div class="clear"></div>
	 <div class="outer-box">
		<div class="pay-left">
			<ul class="">
				 <li><strong>Invoice no: </strong>${element.invoice_number}</li>
				 <li><strong>Invoice Date: </strong>${currentDate}</li>
				 <li><strong>Due Date: </strong>${element.due_date}</li>
				 <li><strong>Invoice Amt: </strong>$${parseFloat(element.payment_amount).toFixed(2)}</li>
			 </ul>
		</div>
		<div class="pay-mid">
			<ul class="">
				<li><strong>Pay this amount: </strong>$${parseFloat(element.payment_amount).toFixed(2)}</li>
				<li><strong>Late fee after due date: </strong>$${parseFloat(lateFreeApply).toFixed(2)}</li>
				<li><strong>Finance charge after due date: </strong>$${parseFloat(nextFinCharge).toFixed(2)}</li>
				<li><strong>Total due after due date: </strong>$${parseFloat(totalAmount).toFixed(2)}</li>
			</ul>
		</div>
		<div class="pay-right">
			<ul class="">
				<li><strong>Previous payment: </strong>${(privaceAmt > 0) ? '$' + parseFloat(privaceAmt).toFixed(2) : ''}</li>
				<li><strong>Previous payment date: </strong>${last_date}</li>
			</ul>
		</div>
	 </div>
     <div class="clear"></div>
     <div class="outer-box">
		<div class="plan-details">
		<h2>Invoice detail</h2>
		<table>
		<thead>
		<tr>
		<th>Plan#</th>
		<th>Provider</th>
		<th>Monthly Payment</th>
		<th>Principal Amount</th>
		<th>Outstanding Principal Amount</th>
		</tr>
		</thead>
		<tbody>`
        html = html + planHtml;
        html = html + `</tbody>
     <tfoot>
         <tr>
             <td colspan="2">Total Due :- </td>
             <td>$${parseFloat(installmentTotal).toFixed(2)}</td>
             <td></td>
             <td></td>
         </tr>
     </tfoot>
     </table>
     </div>
  </div>
  <div class="clear"></div>`
        let noteMsg = `<div class="note"><div>Note :</div>`
        noteMsg = noteMsg + `<div>1. Please ignore if you have already mailed the payment. Thank you.</div>`
        noteMsg = noteMsg + `<div>2. If paid via credit card or debit card, a 3% processing fee will apply.</div>`
        noteMsg = noteMsg + `<div>3. If you have provided your bank details, we will draw your payment on the due date. Please contact us if you would like to use an alternative method of payment.
Please call us at (919) 600-5526 or email info@ourhealthpartner.com if you have any questions.</div></div>`
        if (idx == 0) {
            html = html + noteMsg + `<div class="outer-box" id="pageFooter-first">`
        }/*else if(idx== (data.length-1)){
    //html = html + `<div class="outer-box" id="pageFooter-last">`
  }*/else {
            html = html + noteMsg + `<div class="outer-box" id="pageFooter-${idx + 1}">`
        }

        html = html + `<hr/>
  <div class="payment-voucher">
     <h2>PAYMENT VOUCHER</h2>
     <div class="voucher-left">
         <div class="top-address">
             <h3>Health Partner</h3>
             <div>5720 Creedmoor Rd,</div>
             <div>Suite 103,</div>
             <div>Raleigh, NC 27612</div>
         </div>
         ${customerAddress}
     </div>
     <div class="voucher-right">
         <div class="card-details">
             <div class="card-top">
                 <div>A/C no:${element.child[0].patient_ac}</div>
                 <div>Amt Due:$${parseFloat(crditAmount).toFixed(2)}</div>
                 <div>Date Due:${element.due_date}</div>
             </div>
             <div class="clear-inner"></div>
             <div class="card-mid">
                 <div><span class="check"></span>Visa</div>
                 <div><span class="check"></span>Master</div>
                 <div><span class="check"></span>Amex</div>
                 <div><span class="check"></span>Debit/Credit</div>
             </div>
             <div class="clear-inner"></div>
             <div class="card-mid2">
                 <div class="first-col1"><div>Card number</div><div class="textbox"></div></div>
                 <div class="first-col2"><div>Exp Date</div><div class="textbox"></div></div>
                 <div class="first-col3"><div>CVV #</div> <div class="textbox"></div></div>
             </div>
             <div class="clear-inner"></div>
             <div class="card-mid2">
                 <div  class="first-col11"><div>Name on card</div><div class="textbox"></div></div>
                 <div  class="first-col22"><div>Billing Zip code</div><div class="textbox"></div></div>
                 <div  class="first-col33"><div>Amount Paid</div><div class="textbox"></div></div>
                 
             </div>
             <div class="clear-inner"></div>
            <div class="credit-fee">Note: ${creditCharge}% Processing charges will apply.</div>
         </div>
         <div class="hps-address-right">
             <h3>Health Partner</h3>
             <div>5720 Creedmoor Rd,</div>
             <div>Suite 103,</div>
             <div>Raleigh, NC 27612</div>
         </div>
         
     </div>
     <div class="clear"></div>
  </div>
  </div>
  
  
  
  
  </div>
  </div>
  <div class="page-break"></div>
      `
    });
    return `
    <!doctype html>
    <html>
       <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          
          <title>Current Weekly and Monthly Invoice Invoice Reports</title>
          
          <style>
          html{zoom: 0.7;}
          body{font-size: 12px; line-height: 16px; font-family: 'Arial', sans-serif; color: #303030;}
          .invoice-box {margin: auto; padding: 40px 5px 5px 5px; }
          
          .outer-box {width:100%}
          .inner-left {width:34%; float:left}
          .inner-mid {width:32%; float:left; border:1px solid; margin-right:2%; margin-top:12%}
          .inner-right{width:30.5%; float:left; border:1px solid}
          .clear {clear: both; padding: 10px;}
          .clear-inner{clear: both;}
          .pay-left{float:left; width:32%; border:1px solid; min-height: 85px;}
          .pay-mid{float:left; width:33%; border:1px solid; margin: 0 1.4%; min-height: 85px;}
          .pay-right{float:left; width:31%; border:1px solid; min-height: 85px;}
          ul {list-style: none; margin: 0; padding: 0;}
          ul li {padding: 3px 5px;}
          .plan-details{border: 1px solid; text-align: center;}
          table{width: 100%;}
          table thead tr th{border-bottom:1px solid}
          table tfoot tr td{border-top:1px solid}
          table tr th, table tr td {text-align:left}
          hr {border: 1px solid #000;}
          .payment-voucher{text-align: center; margin: auto;}
          .voucher-right{width:54%; float:left; margin-left:2%}
          .voucher-left{width:44%; float:left}
          .top-address{border: 1px solid; text-align:left; padding:10px; margin: 15px 25px 70px 35px; width: 60%;}
          .bottom-address{border: 1px solid;text-align:left; padding:10px; width: 70%; margin: 0 15px 0 35px}
          .card-details{border: 1px solid;padding-bottom:5px}
          .hps-address-right{border: 1px solid;text-align:left; padding:10px; margin:30px 45px 45px 85px; width:50%}
          .card-top{text-align:left}
		  .card-mid{text-align:left}
          .card-top div{float:left; width:31%; padding:5px}
          .card-mid div{float:left; width:23%; padding:4px;}
          .card-mid div .check{border: 1px solid; padding: 0px 5px; margin-right: 2px;}
          /*.card-mid2 div{float:left; width:31%; padding:0 5px;}*/
          .card-mid2 .first-col1{float:left; width:65%; padding:0 5px;}
          .card-mid2 .first-col2{float:left; width:17%; padding:0 5px;}
          .card-mid2 .first-col3{float:left; width:9%; padding:0 5px;}
          .card-mid2 .first-col11{float:left; width:53%; padding:0 5px;}
          .card-mid2 .first-col22{float:left; width:20%; padding:0 5px;}
          .card-mid2 .first-col33{float:left; width:18%; padding:0 5px;}
          .card-mid2 div div{width: 100%; text-align: left;}
          .card-mid2 div .textbox {border: 1px solid; padding: 10px 0%; }
          h3{margin:1px}
          /*width 852
          hight 372*/
          .page-break    { display: block; page-break-before: always; }
          .note{bottom:0px;position: absolute}
          .credit-fee{text-align: left; padding-left: 5px;}
          </style>
       </head>
       <body>
     ${html}
     </body>
  </html>
      `;
};