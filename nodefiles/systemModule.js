/*
* Title: States
* Descrpation :- This module blong to question realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/system-module-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT sys_mod_id, description, status FROM system_module ORDER BY sys_mod_id DESC', function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })

    app.post('/api/system-module-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.sys_mod_id
         === undefined) {
          sql = 'SELECT description FROM system_module WHERE description=?';
          var edit = 0;
        } else {
          sql = 'SELECT description FROM system_module WHERE description=? AND sys_mod_id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            req.body.description,
            req.body.sys_mod_id
          ]
          , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {

              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0,
                "edit": edit
              }
              res.send(obj);

            }
          })
    })

    app.post('/api/system-module-insert/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO system_module (description, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?)',
            [
                req.body.description,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_system_module_id": result.insertId,
                        "description": req.body.description,
                        "system_module_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })
    app.post('/api/system-module-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE system_module SET description=?, date_modified=?, modified_by=? WHERE sys_mod_id=?',
            [
                req.body.description,
                current_date,
                current_user_id,
                req.body.sys_mod_id
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })

}
