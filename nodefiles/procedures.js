/*
* Title: procedures
* Descrpation :- This module blong to proceduresList in application
* Date :-  June 26, 2019
* Author :- Ramesh kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    /////////////////////////////
    /////GET MASTER DATA////////
    ////////////////////////////

    app.get('/api/procedures-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT master_data_values.value,spec_procedure.procedure_id,spec_procedure.spec_id,spec_procedure.procedure_name,spec_procedure.procedure_code,spec_procedure.status '
            + 'FROM spec_procedure '
            + 'INNER JOIN master_data_values ON master_data_values.mdv_id=spec_procedure.spec_id '
            + 'WHERE spec_procedure.deleted_flag = ? ORDER BY spec_procedure.procedure_id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                }
                else {
                    con.query('SELECT mdv_id,value '
                        + 'FROM master_data_values '
                        + 'WHERE deleted_flag = ? AND status = ?  AND md_id=?',
                        [0, 1, 'Speciality']
                        , function (error, spec) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                var obj = {
                                    "status": 1,
                                    "result": rows,
                                    "speciality": spec
                                }
                                res.send(obj);
                            }
                        })
                }
            })
    });

    // insert data
    app.post('/api/procedures-insert/', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;


        var obj = {}
        con.query('INSERT INTO spec_procedure SET '
            + 'spec_id=?, '
            + 'procedure_name=?,'
            + 'procedure_code=?,'
            + 'status=?,'
            + 'date_created=?,'
            + 'date_modified=?,'
            + 'created_by=?,'
            + 'modified_by=?',
            [
                req.body.speciality_type,
                req.body.name,
                req.body.code,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (error, result, fields) {
                if (error) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    res.send(obj);
                } else {

                    obj.status = 1;
                    obj.procedure_id = result.insertId;
                    obj.speciality_type = req.body.speciality_type;
                    obj.name = req.body.name;
                    obj.code = req.body.code;
                    obj.data_status = req.body.status;
                    res.send(obj);

                }
            });
    });

    // update data
    app.post('/api/procedures-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        var obj = {};
        con.query('UPDATE spec_procedure SET '
            + 'spec_id=?, '
            + 'procedure_name=?,'
            + 'procedure_code=?,'
            + 'status=?,'
            + 'date_modified=?,'
            + 'modified_by=? '
            + 'WHERE procedure_id =? ',
            [
                req.body.spec_id,
                req.body.procedure_name,
                req.body.procedure_code,
                req.body.status,
                current_date,
                current_user_id,
                req.body.procedure_id
            ]
            , function (error) {
                if (error) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    res.send(obj);
                } else {
                    obj.status = 1;
                    res.send(obj);
                }
            });
    });
}