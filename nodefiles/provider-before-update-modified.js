/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function (app, jwtMW) {
  let date = require('date-and-time');

  var multer = require('multer');
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads');
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname);
    }
  });

  const fileFilter = (req, file, cb) => {
    cb(null, true);
  }

  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });
  //var upload = multer({ dest: 'uploads/' })
  var dataupload = upload.array('files');

  app.post('/api/ssn-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    if (req.body.provider_id === undefined) {
      sql = 'SELECT tax_ssn_id FROM provider WHERE deleted_flag=? AND tax_ssn_id=?';
      var edit = 0;
    } else {
      sql = 'SELECT tax_ssn_id FROM provider WHERE deleted_flag=? AND tax_ssn_id=? AND provider_id !=?';
      var edit = 1;
    }
    con.query(sql,
      [
        0,
        req.body.ssn,
        req.body.provider_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/provider-delete/', jwtMW, (req, res) => {
    con = require('../db');

    con.query('UPDATE provider SET deleted_flag=? WHERE provider_id=?',
      [
        1,
        req.body.provider_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          con.query('UPDATE user SET delete_flag=? WHERE client_id=?',
            [
              1,
              req.body.provider_id
            ]
            , function (err, rows, fields) {
              if (err) {
                var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
                }
              } else {
                var obj = {
                  "status": 1,
                  "provider_id": req.body.provider_id
                }
                res.send(obj);
              }

            });

        }
      })
  })

  /////////////////////////////
  /////GET PROVIDER DETAILS////////
  ////////////////////////////


  app.get('/api/provider-details/', jwtMW, (req, res) => {
    con = require('../db');
    var providerObj = {};

    con.query('SELECT '
      + 'provider.provider_id AS provider_id, '
      + 'provider.name AS provider_name, '
      + 'provider.phone AS provider_phone_no, '
      + 'provider.email AS provider_email, '
      + 'provider.fax_no AS provider_fax_no, '
      + 'provider.website_url AS provider_website, '
      + 'provider.primary_contact AS provider_primary_contact, '
      + 'provider.tax_ssn_id AS provider_taxid_ssn, '
      + 'provider.member_flag AS provider_network, '
      + 'provider.primary_phone AS provider_primary_contact_phone, '
      + 'provider.status AS provider_status, '
      + 'provider.provider_type AS provider_type_id, '
      + 'master_data_values.value AS provider_type '
      + 'FROM provider INNER JOIN master_data_values '
      + 'ON provider.provider_type = master_data_values.mdv_id '

      + 'WHERE provider.status=? '
      + 'AND provider.deleted_flag=? '
      + 'AND provider.provider_id=?'
      , [1, 0, req.query.id]
      , function (error, rows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          providerObj.provider_detail = rows;
          con.query('SELECT '
            + 'provider_location.location_name AS physical_location_name, '
            + 'provider_location.address1 AS physical_address1, '
            + 'provider_location.address2 AS physical_address2, '
            + 'provider_location.county AS physical_county, '
            + 'provider_location.city AS physical_city, '
            + 'provider_location.zip_code AS physical_zip_code, '
            + 'provider_location.primary_phone AS physical_phone_no, '
            + 'provider_location.billing_address_flag, '
            + 'provider_location.primary_address_flag, '
            + 'countries.id AS physical_country_id, '
            + 'states.state_id AS physical_state_id, '

            + 'provider_location2.location_name AS billing_location_name, '
            + 'provider_location2.address1 AS billing_address1, '
            + 'provider_location2.address2 AS billing_address2, '
            + 'provider_location2.county AS billing_county, '
            + 'provider_location2.city AS billing_city, '
            + 'provider_location2.zip_code AS billing_zip_code, '
            + 'provider_location2.primary_phone AS billing_phone_no, '
            + 'provider_location2.billing_address_flag, '
            + 'provider_location2.primary_address_flag, '
            + 'provider_location2.primary_billing_address_flag, '
            + 'countries2.id AS billing_country_id, '
            + 'states2.state_id AS billing_state_id, '


            + 'states.name AS physical_state, '
            + 'countries.name AS physical_country, '


            + 'states2.name AS billing_state, '
            + 'countries2.name AS billing_country '

            + 'FROM provider_location '


            + 'INNER JOIN states '
            + 'ON provider_location.state = states.state_id '

            + 'INNER JOIN countries '
            + 'ON states.country_id = countries.id '

            + 'LEFT JOIN provider_location AS provider_location2 '
            + 'ON provider_location.provider_location_id = provider_location2.parent_provider_location_id '
            + 'AND provider_location.provider_id = provider_location2.provider_id '



            + 'LEFT JOIN states AS states2 '
            + 'ON provider_location2.state  = states2.state_id '

            + 'LEFT JOIN countries AS countries2 '
            + 'ON states2.country_id = countries2.id '

            + 'WHERE provider_location.provider_id = ? '
            + 'AND provider_location.status = ? '

            + 'AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
            + 'ORDER BY provider_location.provider_location_id ASC'

            , [req.query.id, 1]
            , function (error, rows, fields) {
              if (error) {
                var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
                }
                res.send(obj);
              } else {
                providerObj.provider_location = rows;
                con.query('SELECT '
                  + 'provider_bank.bank_name, '
                  + 'provider_bank.acct_no AS bank_accout, '
                  + 'provider_bank.bank_rounting_no AS rounting_no, '
                  + 'provider_bank.name_on_acct AS name_on_account, '
                  + 'provider_bank.bank_address, '
                  + 'provider_bank.acct_type AS bank_account_type_id, '
                  + 'provider_location.location_name, '
                  + 'master_data_values.value AS bank_account_type '
                  + 'FROM provider_bank '
                  + 'INNER JOIN provider_location '
                  + 'ON provider_bank.provider_id = provider_location.provider_id '
                  + 'AND provider_bank.provider_location_id = provider_location.provider_location_id '
                  + 'INNER JOIN master_data_values '
                  + 'ON provider_bank.acct_type = master_data_values.mdv_id '
                  + 'WHERE provider_bank.provider_id = ? '
                  + 'AND provider_bank.status = ? '
                  + 'ORDER BY provider_bank.provider_location_id ASC'
                  , [req.query.id, 1]
                  , function (error, rows, fields) {
                    if (error) {
                      var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                      }
                      res.send(obj);
                    } else {
                      providerObj.provider_bank = rows;
                      con.query('SELECT '
                        + 'provider_loc_spec.provider_location_provider_location_id, '
                        + 'provider_loc_spec.procedure_id AS procedure_id, '
                        + 'provider_loc_spec.mdv_speciality_id AS specialization, '
                        + 'provider_location.location_name AS spl_location_name '



                        + 'FROM provider_loc_spec '

                        + 'INNER JOIN provider_location '
                        + 'ON provider_loc_spec.provider_location_provider_location_id = provider_location.provider_location_id '
                        + 'AND provider_loc_spec.provider_location_provider_id = provider_location.provider_id '



                        + 'WHERE provider_loc_spec.provider_location_provider_id = ? '

                        + 'AND provider_loc_spec.status = ? '
                        + 'ORDER BY provider_loc_spec.provider_location_provider_location_id ASC'
                        , [req.query.id, 1]
                        , function (error, rows, fields) {
                          if (error) {
                            var obj = {
                              "status": 0,
                              "message": "Something wrong please try again."
                            }
                            res.send(obj);
                          } else {
                            providerObj.provider_specility = rows;
                            /*con.query('SELECT '
                              + 'doctors.f_name ,'
                              + 'doctors.l_name, '
                              + 'doctors.location_id, '
                              + 'doctors.id, '

                              + 'master_data_values.value AS speciality, '
                              + 'spec_procedure.procedure_name '

                              + 'FROM doctors '

                              + 'INNER JOIN master_data_values '
                              + 'ON master_data_values.mdv_id=doctors.mdv_speciality_id '

                              + 'INNER JOIN spec_procedure '
                              + 'ON spec_procedure.spec_id=doctors.mdv_speciality_id '

                              + 'INNER JOIN provider_loc_spec '
                              + 'ON provider_loc_spec.provider_location_provider_location_id=doctors.location_id '
                              + 'AND provider_loc_spec.provider_location_provider_id=doctors.provider_id '
                              + 'AND provider_loc_spec.mdv_speciality_id=doctors.mdv_speciality_id '

                              + 'WHERE doctors.provider_id = ? AND doctors.status =  ? '

                              + 'GROUP BY doctors.id'
                              , [req.query.id, 1]
                              , function (error, doc_rows, fields) {*/
                            con.query('SELECT '
                              + 'doctors.f_name ,'
                              + 'doctors.l_name, '
                              + 'doctors.location_id, '
                              + 'doctors.id, '

                              + 'master_data_values.value AS speciality, '
                              + '(SELECT spec_procedure.procedure_name FROM provider_loc_spec INNER JOIN spec_procedure ON spec_procedure.spec_id=provider_loc_spec.mdv_speciality_id '
                              + 'WHERE provider_loc_spec.provider_location_provider_location_id=doctors.location_id AND provider_loc_spec.provider_location_provider_id=doctors.provider_id AND '
                              + 'provider_loc_spec.mdv_speciality_id=doctors.mdv_speciality_id LIMIT 1) as procedure_name   '

                              + 'FROM doctors '

                              + 'INNER JOIN master_data_values '
                              + 'ON master_data_values.mdv_id=doctors.mdv_speciality_id '

                              //+ 'INNER JOIN spec_procedure '
                              //+ 'ON spec_procedure.spec_id=doctors.mdv_speciality_id '

                              //+ 'INNER JOIN provider_loc_spec '
                              //+ 'ON provider_loc_spec.provider_location_provider_location_id=doctors.location_id '
                              //+ 'AND provider_loc_spec.provider_location_provider_id=doctors.provider_id '
                              //+ 'AND provider_loc_spec.mdv_speciality_id=doctors.mdv_speciality_id '

                              + 'WHERE doctors.provider_id = ? AND doctors.status =  ? '

                              + 'GROUP BY doctors.id'
                              , [req.query.id, 1]
                              , function (error, doc_rows, fields) {
                                if (error) {
                                  var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                  }
                                  res.send(obj);
                                } else {
                                  providerObj.provider_specility_doctors = doc_rows;
                                  con.query('SELECT '
                                    + 'user.f_name AS user_first_name, '
                                    + 'user.m_name AS user_middle_name, '
                                    + 'user.l_name AS user_last_name, '
                                    + 'user.email_id AS user_email, '
                                    + 'user.phone AS user_phone_no, '
                                    + 'user.user_location, '
                                    + 'user.provider_primary_contact_flg AS user_primary_contact_flag, '
                                    + 'user.username, '
                                    + 'user_type_role.mdv_role_id AS ref_role_id, '
                                    + 'master_data_values.value AS user_role, '
                                    + 'user_type_role.user_type_role_id AS user_role_id '
                                    + 'FROM user '
                                    + 'INNER JOIN user_type_role '
                                    + 'ON user.user_type_role_id = user_type_role.user_type_role_id '

                                    + 'INNER JOIN master_data_values '
                                    + 'ON user_type_role.mdv_role_id = master_data_values.mdv_id '
                                    + 'WHERE user.client_id = ? '
                                    + 'AND user.status = ?'
                                    , [req.query.id, 1]
                                    , function (error, rows, fields) {
                                      if (error) {
                                        var obj = {
                                          "status": 0,
                                          "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                      } else {
                                        providerObj.provider_user = rows;
                                        con.query('SELECT '
                                          + 'discount_fee_ra.mdv_discount_rate_id AS discount_type_id, '
                                          + 'discount_fee_ra.discount_rate, '
                                          + 'discount_fee_ra.reduced_interest_pct AS redu_discount_rate, '
                                          + 'discount_fee_ra.loan_term_month, '
                                          + 'master_data_values2.value AS loan_term_month_value, '
                                          + 'DATE_FORMAT(discount_fee_ra.start_date, "%m/%d/%Y") AS discount_from_date, '
                                          + 'DATE_FORMAT(discount_fee_ra.end_date, "%m/%d/%Y") AS discount_to_date, '
                                          + 'master_data_values.value AS discount_type, '
                                          + 'CASE WHEN master_data_values.value = "Promotional" THEN "true" ELSE "false" END AS discount_type_filter '

                                          + 'FROM discount_fee_ra '
                                          + 'INNER JOIN provider_discount_fee '
                                          + 'ON provider_discount_fee.df_ra_id = discount_fee_ra.df_ra_id '

                                          + 'INNER JOIN master_data_values '
                                          + 'ON discount_fee_ra.mdv_discount_rate_id = master_data_values.mdv_id '

                                          + 'LEFT JOIN master_data_values AS master_data_values2 '
                                          + 'ON discount_fee_ra.loan_term_month = master_data_values2.mdv_id '

                                          + 'WHERE provider_discount_fee.provider_id = ? '
                                          + 'AND provider_discount_fee.status = ? '
                                          + 'AND discount_fee_ra.discount_rate IS NOT NULL '
                                          + 'ORDER BY discount_fee_ra.df_ra_id ASC'

                                          , [req.query.id, 1]
                                          , function (error, rows, fields) {
                                            if (error) {
                                              var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                              }
                                              res.send(obj);
                                            } else {
                                              providerObj.provider_additional_discount = rows;
                                              con.query('SELECT '
                                                + 'discount_fee_ra.mdv_discount_rate_id AS additional_fee_type_id, '
                                                + 'discount_fee_ra.fee_amount AS additional_conv_fee, '
                                                + 'DATE_FORMAT(discount_fee_ra.start_date, "%m/%d/%Y") AS additional_from_date, '
                                                + 'DATE_FORMAT(discount_fee_ra.end_date, "%m/%d/%Y") AS additional_to_date, '
                                                + 'master_data_values.value AS additional_fee_type '

                                                + 'FROM discount_fee_ra '
                                                + 'INNER JOIN provider_discount_fee '
                                                + 'ON provider_discount_fee.df_ra_id = discount_fee_ra.df_ra_id '

                                                + 'INNER JOIN master_data_values '
                                                + 'ON discount_fee_ra.mdv_discount_rate_id = master_data_values.mdv_id '

                                                + 'WHERE provider_discount_fee.provider_id = ?'
                                                + 'AND provider_discount_fee.status = ? '
                                                + 'AND discount_fee_ra.fee_amount IS NOT NULL '
                                                + 'ORDER BY discount_fee_ra.df_ra_id ASC'

                                                , [req.query.id, 1]
                                                , function (error, rows, fields) {
                                                  if (error) {
                                                    var obj = {
                                                      "status": 0,
                                                      "message": "Something wrong please try again."
                                                    }
                                                    res.send(obj);
                                                  } else {
                                                    providerObj.provider_additional_fee = rows;
                                                    con.query('SELECT '
                                                      + 'doc_repo.name AS document_name, '
                                                      + 'doc_repo.file_path AS document_upload, '
                                                      + 'doc_repo.file_type AS document_type, doc_repo.item_id, '
                                                      + 'master_data_values.value AS file_type '

                                                      + 'FROM doc_repo '
                                                      + 'INNER JOIN master_data_values '
                                                      + 'ON master_data_values.mdv_id = doc_repo.file_type '

                                                      + 'WHERE provider_id = ? AND doc_repo.status=?'
                                                      , [req.query.id, 1]
                                                      , function (error, rows, fields) {
                                                        if (error) {
                                                          var obj = {
                                                            "status": 0,
                                                            "message": "Something wrong please try again."
                                                          }
                                                          res.send(obj);
                                                        } else {
                                                          con.query('SELECT '
                                                            + 'payment_term_mdv_id '

                                                            + 'FROM provider_term '
                                                            + 'WHERE provider_id = ? AND status = ?'
                                                            , [req.query.id, 1]
                                                            , function (err, provider_term, fields) {
                                                              if (err) {

                                                              } else {

                                                                ///for file upload on onedrive
                                                                providerObj.provider_document = rows;
                                                                var oneDrive = require('./microsoft/oneDrive.js');
                                                                let totalPlan = providerObj.provider_document.length;
                                                                oneDrive.getOneDriveLogin(function (loginOne) {

                                                                  if (loginOne.status == 1 && totalPlan > 0) {
                                                                    providerObj.provider_document.map((data, idx) => {
                                                                      //getOneDriveFiles
                                                                      oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                                                        if (one.status == 1) {
                                                                          data.document_upload = one.file
                                                                        }
                                                                        if (0 === --totalPlan) {
                                                                          //res.send(result);
                                                                          providerObj.provider_term = provider_term;
                                                                          providerObj.status = 1;
                                                                          res.send(providerObj);
                                                                        }
                                                                      })
                                                                    })
                                                                  } else {
                                                                    providerObj.provider_term = provider_term;
                                                                    providerObj.status = 1;
                                                                    res.send(providerObj);
                                                                  }
                                                                })

                                                                ///end file uploading


                                                              }
                                                            });
                                                        }
                                                      });
                                                  }
                                                });
                                            }
                                          });
                                      }
                                    });
                                }
                                //}
                                //}
                              });
                          }
                        });
                    }
                  });
              }
            });
        }
      });

  })


  /////////////////////////////
  /////GET PROVIDER LIST////////
  ////////////////////////////

  app.get('/api/provider-list/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT '
      + 'provider.provider_id, '
      + 'provider.name, '
      + 'provider.member_flag, '
      + 'provider.phone, '
      + 'provider.primary_contact, '
      + 'provider.email, '
      + 'provider.status, '
      + 'provider_location.address1, provider_location.address2, provider_location.city, provider_location.zip_code, states.name as state_name, '
      + 'master_data_values.value AS provider_type '

      + 'FROM provider '
      + 'INNER JOIN master_data_values ON provider.provider_type = master_data_values.mdv_id '
      + 'INNER JOIN provider_location ON provider_location.provider_id = provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
      + 'INNER JOIN states ON states.state_id=provider_location.state '


      + 'WHERE provider.deleted_flag = 0 '

      + 'ORDER BY provider.provider_id DESC'

      , function (error, rows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1,
            "result": rows,
          }
          res.send(obj);
        }
      })
  })

  /////////////////////////////
  /////GET MASTER DATA////////
  ////////////////////////////

  app.get('/api/provider-type-master-data-value/', jwtMW, (req, res) => {
    con = require('../db');
    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Provider Type' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  ///Provider Country List

  app.get('/api/provider-country/', jwtMW, (req, res) => {
    con = require('../db');
    con.query("SELECT id, name FROM countries WHERE deleted_flag = 0 AND status = 1", function (error, rows, fields) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////
  ////PROVIDER OPTION///////
  /////////////////////////

  app.get('/api/provider-option/', jwtMW, (req, res) => {
    con = require('../db');
    var optionObj = {};

    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Bank Account Type' AND deleted_flag = 0 AND status = 1", ['Bank Account Type', 0, 1], function (error, rows, fields) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        optionObj.bank_account_type = rows;
        con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Speciality', 0, 1], function (error, rows, fields) {
          if (error) {
            var obj = {
              "status": 0,
              "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            optionObj.speciality = rows;
            con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Discount Type', 0, 1], function (error, rows, fields) {
              if (error) {
                var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
                }
                res.send(obj);
              } else {
                optionObj.discount_type = rows;
                con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Fee Type', 0, 1], function (error, rows, fields) {
                  if (error) {
                    var obj = {
                      "status": 0,
                      "message": "Something wrong please try again."
                    }
                    res.send(obj);
                  } else {
                    optionObj.fee_type = rows;
                    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = ? AND deleted_flag = ? AND status = ?", ['Document Type', 0, 1], function (error, rows, fields) {
                      if (error) {
                        var obj = {
                          "status": 0,
                          "message": "Something wrong please try again."
                        }
                        res.send(obj);
                      } else {
                        optionObj.document_type = rows;
                        con.query("SELECT user_type_role_id as mdv_id,value FROM user_type_role INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_role_id WHERE master_data_values.status=? AND master_data_values.deleted_flag=? AND mdv_type_id=?", [1, 0, 3], function (error, rows, fields) {
                          if (error) {
                            var obj = {
                              "status": 0,
                              "message": "Something wrong please try again."
                            }
                            res.send(obj);
                          } else {
                            optionObj.user_role = rows;
                            con.query("SELECT user_type_role_id as mdv_id,value FROM user_type_role INNER JOIN master_data_values ON master_data_values.mdv_id=user_type_role.mdv_role_id WHERE master_data_values.status=? AND master_data_values.deleted_flag=? AND mdv_type_id=?", [1, 0, 2], function (error, rows, fields) {
                              if (error) {
                                var obj = {
                                  "status": 0,
                                  "message": "Something wrong please try again."
                                }
                                res.send(obj);
                              } else {
                                optionObj.user_role = rows;
                                con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Payment Term Month' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
                                  if (error) {
                                    var obj = {
                                      "status": 0,
                                      "message": "Something wrong please try again."
                                    }
                                    res.send(obj);
                                  } else {
                                    optionObj.pvd_payment_term = rows;
                                    con.query("SELECT mdv_id, value FROM master_data_values WHERE md_id = 'Payment Term Month' AND deleted_flag = 0 AND status = 1", function (error, rows, fields) {
                                      if (error) {
                                        var obj = {
                                          "status": 0,
                                          "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                      } else {
                                        optionObj.pvd_loan_term_month = rows;
                                        res.send(optionObj);
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });

  })

  ///////////////////////////
  ///////INSERT PROVIDER/////
  ///////////////////////////

  app.post('/api/provider-insert/', dataupload, jwtMW, (req, res) => {

    con = require('../db');
    var addData = JSON.parse(req.body.addData);

    const md5 = require('md5');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(addData.user_password);
    var obj = {};

    //return false;
    // start Transaction
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      //PRIVIDER INSERT
      con.query('INSERT INTO provider SET '
        + 'provider_ac=((SELECT COALESCE(MAX(provider_ac),11111)+1 AS dt FROM provider as m)), '
        + 'name=?, '
        + 'provider_type=?,'
        + 'tax_ssn_id=?, '
        + 'phone=?, '
        + 'email=?, '
        + 'fax_no=?, '
        + 'website_url=?, '
        + 'primary_contact=?, '
        + 'primary_phone=?, '
        + 'member_flag=?, '
        + 'status=?, '
        + 'date_created=?, '
        + 'date_modified=?, '
        + 'created_by=?, '
        + 'modified_by=?',
        [
          addData.provider_name,
          addData.provider_type,
          addData.provider_taxid_ssn,
          addData.provider_phone_no,
          addData.provider_email,
          addData.provider_fax_no,
          addData.provider_website,
          addData.provider_primary_contact,
          addData.provider_primary_contact_phone,
          addData.provider_network,

          addData.provider_status,
          current_date,
          current_date,
          current_user_id,
          current_user_id
        ]
        , function (err, resultmain) {
          if (err) {

            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong with provider table please try again.";
            });
          } else {
            // INSERT USER
            con.query('INSERT INTO user SET '
              + 'f_name=?, '
              + 'm_name=?, '
              + 'l_name=?, '
              + 'email_id=?, '
              + 'user_type_role_id=?, '
              + 'user_location=?, '
              + 'client_id=?, '
              + 'phone=?, '
              + 'provider_primary_contact_flg=?, '
              + 'username=?, '
              + 'password=?, '
              + 'status=?, '
              + 'date_created=?, '
              + 'date_modified=?, '
              + 'created_by=?, '
              + 'modified_by=?',
              [

                addData.user_first_name,
                addData.user_middle_name,
                addData.user_last_name,
                addData.user_email,
                addData.user_role,
                addData.location[0].physical_location_name,
                resultmain.insertId,
                addData.user_phone_no,
                addData.user_primary_contact_flag,
                addData.username,
                password,

                addData.provider_status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
              ]
              , function (err, resultuser) {
                if (err) {

                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong with user table please try again.";
                  });
                } else {

                  //LOCATION INSERT
                  var arr = [];
                  var cou = 1;
                  addData.location.forEach(function (element, idx) {
                    if (element.primary_address_flag == '') {
                      primary_address_flag = 0;
                    } else {
                      primary_address_flag = element.primary_address_flag;
                    }

                    if (element.primary_billing_address_flag == '') {
                      primary_billing_address_flag = 0;
                    } else {
                      primary_billing_address_flag = element.primary_billing_address_flag;
                    }

                    if (element.billing_address_flag == '') {
                      billing_address_flag = 0;
                    } else {
                      billing_address_flag = element.billing_address_flag;
                    }

                    if (element.billing_address_flag == 0) {

                      var as = cou;

                      arr.push([
                        resultmain.insertId,
                        cou,
                        null,
                        element.physical_address1,
                        element.physical_address2,
                        element.physical_country,
                        element.physical_state,
                        element.physical_region,
                        element.physical_county,
                        element.physical_city,
                        element.physical_zip_code,
                        element.physical_location_name,
                        element.physical_phone_no,
                        primary_address_flag,
                        0,
                        billing_address_flag,

                        addData.provider_status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                      ]);
                      cou++;
                      arr.push([
                        resultmain.insertId,
                        cou,
                        as,
                        element.billing_address1,
                        element.billing_address2,
                        element.billing_country,
                        element.billing_state,
                        element.billing_region,
                        element.billing_county,
                        element.billing_city,
                        element.billing_zip_code,
                        element.billing_location_name,
                        element.physical_phone_no,
                        primary_address_flag,
                        primary_billing_address_flag,
                        billing_address_flag,

                        addData.provider_status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                      ]);
                      cou++;
                    } else {
                      arr.push([
                        resultmain.insertId,
                        cou,
                        cou,
                        element.physical_address1,
                        element.physical_address2,
                        element.physical_country,
                        element.physical_state,
                        element.physical_region,
                        element.physical_county,
                        element.physical_city,
                        element.physical_zip_code,
                        element.physical_location_name,
                        element.physical_phone_no,
                        primary_address_flag,
                        primary_billing_address_flag,
                        billing_address_flag,

                        addData.provider_status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                      ]);
                      cou++;
                    }

                  });

                  con.query('INSERT INTO provider_location(provider_id, provider_location_id, parent_provider_location_id, address1, address2, country, state, region, county, city, zip_code, location_name, primary_phone, primary_address_flag, primary_billing_address_flag, billing_address_flag, status, date_created, date_modified, created_by, modified_by) VALUES ? '
                    ,
                    [
                      arr
                    ]
                    , function (err, resultProviderLocation) {
                      if (err) {

                        con.rollback(function () {
                          obj.status = 0;
                          obj.message = "Something wrong with provider location table please try again.";
                        });
                      } else {

                        //BANK INSERT
                        addData.bank_details.forEach(function (element, idx) {

                          con.query('INSERT INTO provider_bank(provider_location_id, provider_id, bank_name, bank_address, bank_rounting_no, acct_no, name_on_acct, acct_type, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + element.location_name + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + element.bank_name + '", "' + element.bank_address + '", "' + element.rounting_no + '", "' + element.bank_accout + '", "' + element.name_on_account + '", ' + element.bank_account_type + ', ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                            , function (err, resultProviderBank) {
                              if (err) {

                                con.rollback(function () {
                                  obj.status = 0;
                                  obj.message = "Something wrong with provider location table please try again.";
                                });
                              }
                            });
                        });
                        //BANK INSERT END


                        //SPECIALITY INSERT
                        addData.speciality.forEach(function (element, idx) {

                          con.query('INSERT INTO provider_loc_spec(provider_location_provider_location_id, provider_location_provider_id, mdv_speciality_id, procedure_id, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + element.spl_location_name + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + element.specialization + '", "' + element.procedure + '", ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                            , function (err, resultProviderLocationSpl) {
                              if (err) {

                                con.rollback(function () {
                                  obj.status = 0;
                                  obj.message = "Something wrong with provider location table please try again.";
                                });
                              }
                            });
                        });
                        //SPECIALITY INSERT END

                        //DOCTOR INSERT
                        addData.speciality.forEach(function (element, idx) {

                          con.query('INSERT INTO doctors(f_name, l_name, location_id, provider_id, mdv_speciality_id, date_created, date_modified, created_by, modified_by) VALUES("' + element.doctor_f_name + '", "' + element.doctor_l_name + '", (SELECT provider_location_id FROM provider_location WHERE provider_id = ' + resultmain.insertId + ' AND location_name = "' + element.spl_location_name + '" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + resultmain.insertId + ', "' + element.specialization + '", "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                            , function (err, resultDoctor) {

                              if (err) {
                                con.rollback(function () {
                                  obj.status = 0;
                                  obj.message = "Something wrong with doctor table please try again.";
                                });
                              }
                            });

                        });
                        //DOCTOR INSERT END 

                        //INSERT DISCOUNT RATE
                        var arrD = addData.discount_rate;
                        var arrF = addData.additional_fee;
                        var arrDF = arrD.concat(arrF);
                        // console.log('------------discount-------------')
                        // console.log(arrD)
                        // console.log(arrF)
                        arrDF.forEach(function (element, idx) {
                          var arrDiscountFee = [];
                          if (element.discount_rate) {
                            element.additional_conv_fee = null
                            if (element.discount_rate !== 0) {
                              var discount_rate = element.discount_rate;
                            } else {
                              var discount_rate = null
                            }

                            if (element.redu_discount_rate !== 0) {
                              var redu_discount_rate = element.redu_discount_rate
                            } else {
                              var redu_discount_rate = null
                            }
                            arrDiscountFee.push([
                              element.discount_type,
                              discount_rate,
                              redu_discount_rate,
                              element.loan_term_month,
                              element.additional_conv_fee,
                              (element.discount_from_date != '') ? element.discount_from_date : null,
                              (element.discount_to_date != '') ? element.discount_to_date : null,


                              addData.provider_status,
                              current_date,
                              current_date,
                              current_user_id,
                              current_user_id
                            ]);
                          } else {
                            element.discount_rate = null
                            element.redu_discount_rate = null
                            element.loan_term_month = null
                            if(element.additional_fee_type && element.additional_conv_fee) {

                              arrDiscountFee.push([
                                element.additional_fee_type,
                                element.discount_rate,
                                element.redu_discount_rate,
                                element.loan_term_month,
                                element.additional_conv_fee,
                                (element.additional_from_date) ? element.additional_from_date : null,
                                (element.additional_to_date) ? element.additional_to_date : null,

                                addData.provider_status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                              ]);

                            }
                          }

                          con.query('INSERT INTO discount_fee_ra(mdv_discount_rate_id, discount_rate, reduced_interest_pct, loan_term_month, fee_amount, start_date, end_date, status, date_created, date_modified, created_by, modified_by) VALUES ?',

                            [arrDiscountFee],

                            function (err, resultDiscountFeeRate) {
                              if (err) {
                                con.rollback(function () {
                                  obj.status = 0;
                                  obj.message = "Something wrong with provider location table please try again.";
                                });
                              } else {
                                //INSERT DISCOUNT FEE
                                con.query('INSERT INTO provider_discount_fee(df_ra_id, provider_id, status, date_created, date_modified, created_by, modified_by) VALUES (?,?,?,?,?,?,?)',

                                  [resultDiscountFeeRate.insertId, resultmain.insertId, addData.provider_status, current_date, current_date, current_user_id, current_user_id],

                                  function (err, resultProviderDiscountFee) {
                                    if (err) {
                                      con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong with provider location table please try again.";
                                      });
                                    } else {
                                      obj.status = 1;
                                      obj.message = "Data added in provider discount fee";
                                    }
                                  });
                                //INSERT DISCOUNT FEE END
                              }
                            });

                        });

                        //INSERT DISCOUNT RATE END

                        //INSERT DOC REPO  
                        var oneDrive = require('./microsoft/oneDrive.js');
                        oneDrive.getOneDriveLogin(function (loginOne) {
                          if (loginOne.status == 1) {
                            addData.documents.forEach(function (element, idx) {
                              var file_path = '';
                              if (req.files.length !== 0) {
                                var file_path = req.files[idx].path;
                                var filename = req.files[idx].filename;
                                const path = require('path');
                                var curentfile = {
                                  filename: filename,
                                  path: path.join(__dirname, '..', file_path),
                                  provider_id: resultmain.insertId,
                                }
                                oneDrive.getOneDriveProviderUpload(loginOne.token, curentfile, function (one) {
                                  if ((one.status == 1)) {
                                    var item_id = one.item_id;
                                    const fs = require('fs');
                                    fs.unlinkSync(curentfile.path)
                                  } else {
                                    var item_id = '';
                                  }
                                  con.query('INSERT INTO doc_repo(provider_id, name, file_type, file_path, item_id, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?,?)',
                                    [resultmain.insertId, element.document_name, element.document_type, file_path, item_id, addData.provider_status, current_date, current_date, current_user_id, current_user_id]
                                    , function (err, resultProviderLocationSpl) {
                                      if (err) {
                                        con.rollback(function () {
                                          obj.status = 0;
                                          obj.message = "Something wrong with provider location table please try again.";
                                        });
                                      }
                                    });
                                })
                              } else {
                                file_path = null;
                                con.query('INSERT INTO doc_repo(provider_id, name, file_type, file_path, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
                                  [resultmain.insertId, element.document_name, element.document_type, file_path, addData.provider_status, current_date, current_date, current_user_id, current_user_id]
                                  , function (err, resultProviderLocationSpl) {
                                    if (err) {
                                      con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong with provider location table please try again.";
                                      });
                                    }
                                  });
                              }


                            });
                          } else {
                            addData.documents.forEach(function (element, idx) {
                              var file_path = '';
                              if (req.files.length !== 0) {
                                var file_path = req.files[idx].path;
                              } else {
                                file_path = null;
                              }
                              con.query('INSERT INTO doc_repo(provider_id, name, file_type, file_path, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
                                [resultmain.insertId, element.document_name, element.document_type, file_path, addData.provider_status, current_date, current_date, current_user_id, current_user_id]
                                , function (err, resultProviderLocationSpl) {
                                  if (err) {
                                    con.rollback(function () {
                                      obj.status = 0;
                                      obj.message = "Something wrong with provider location table please try again.";
                                    });
                                  }
                                });

                            });
                          }
                        })

                        //INSERT DOC REPO END


                        con.commit(function (err) {
                          if (err) {
                            con.rollback(function () {
                              obj.status = 0;
                              obj.message = "Something wrong please try again.";
                            });
                          }
                          obj.status = 1;
                          obj.message = "Application submitted for approval";
                          res.send(obj);
                        });


                      }

                    });
                  //LOCATION INSERT END

                }

              });
            //USER INSERT END

          }

        });
      //PROVIDER INSERT END
    });
  })





  //////////////////////
  ///Provider Update////
  //////////////////////

  app.post('/api/provider-update/', dataupload, jwtMW, (req, res) => {
    con = require('../db');
    var addData = JSON.parse(req.body.addData);
    const md5 = require('md5');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(addData.user_password);
    var obj = {};
    
    // start Transaction
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      //PRIVIDER UPDATE
      con.query('UPDATE provider SET '
        + 'name=?, '
        + 'provider_type=?,'
        + 'tax_ssn_id=?, '
        + 'phone=?, '
        + 'email=?, '
        + 'fax_no=?, '
        + 'website_url=?, '
        + 'primary_contact=?, '
        + 'primary_phone=?, '
        + 'member_flag=?, '
        + 'status=?, '
        + 'date_created=?, '
        + 'date_modified=?, '
        + 'created_by=?, '
        + 'modified_by=? '
        + 'WHERE provider_id=?',
        [
          addData.provider_name,
          addData.provider_type,
          addData.provider_taxid_ssn,
          addData.provider_phone_no,
          addData.provider_email,
          addData.provider_fax_no,
          addData.provider_website,
          addData.provider_primary_contact,
          addData.provider_primary_contact_phone,
          addData.provider_network,

          addData.provider_status,
          current_date,
          current_date,
          current_user_id,
          current_user_id,
          addData.provider_id
        ]
        , function (err, resultmain) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong with provider table please try again.";
            });
          } else {
            // UPDATE USER
            con.query('UPDATE user SET '
              + 'f_name=?, '
              + 'm_name=?, '
              + 'l_name=?, '
              + 'email_id=?, '
              + 'user_type_role_id=?, '
              + 'user_location=?, '
              + 'phone=?, '
              + 'provider_primary_contact_flg=?, '
              + 'status=?, '
              + 'date_created=?, '
              + 'date_modified=?, '
              + 'created_by=?, '
              + 'modified_by=? '
              + 'WHERE client_id=?',
              [

                addData.user_first_name,
                addData.user_middle_name,
                addData.user_last_name,
                addData.user_email,
                addData.user_role,
                addData.location[0].physical_location_name,
                addData.user_phone_no,
                addData.user_primary_contact_flag,

                addData.provider_status,
                current_date,
                current_date,
                current_user_id,
                current_user_id,
                addData.provider_id
              ]
              , function (err, resultuser) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong with user table please try again.";
                  });
                } else {
                  //UPDATE LOCATION STATUS
                  con.query('UPDATE provider_location SET status=? WHERE provider_id=?',
                    [0, addData.provider_id],
                    function (err, updateLocationStatus) {
                      if (err) {
                        con.rollback(function () {
                          obj.status = 0;
                          obj.message = "Something wrong with user table please try again.";
                        });
                      } else {

                        //LOCATION UPDATE
                        //GET MAX LOC ID
                        var max_loc_id;
                        con.query("SELECT MAX(provider_location_id) AS max_loc_id FROM provider_location WHERE provider_id=?",
                          [addData.provider_id],
                          function (error, rows, fields) {
                            if (error) {
                              con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong with user table please try again.";
                              });
                            } else {

                              max_loc_id = rows[0].max_loc_id;

                              var arr = [];
                              var cou = max_loc_id + 1;

                              addData.location.forEach(function (element, idx) {
                                if (element.primary_address_flag == '') {
                                  primary_address_flag = 0;
                                } else {
                                  primary_address_flag = element.primary_address_flag;
                                }

                                if (element.primary_billing_address_flag == '') {
                                  primary_billing_address_flag = 0;
                                } else {
                                  primary_billing_address_flag = element.primary_billing_address_flag;
                                }

                                if (element.billing_address_flag == '' || element.billing_address_flag == undefined) {
                                  billing_address_flag = 0;
                                } else {
                                  billing_address_flag = element.billing_address_flag;
                                }
                                if (element.billing_address_flag == 0 || element.billing_address_flag == undefined) {

                                  var as = cou;

                                  arr.push([
                                    addData.provider_id,
                                    cou,
                                    null,
                                    element.physical_address1,
                                    element.physical_address2,
                                    element.physical_country_id,
                                    element.physical_state_id,
                                    element.physical_city,
                                    element.physical_zip_code,
                                    element.physical_location_name,
                                    element.physical_phone_no,
                                    primary_address_flag,
                                    0,
                                    billing_address_flag,

                                    addData.provider_status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                  ]);
                                  cou++;
                                  arr.push([
                                    addData.provider_id,
                                    cou,
                                    as,
                                    element.billing_address1,
                                    element.billing_address2,
                                    element.billing_country_id,
                                    element.billing_state_id,
                                    element.billing_city,
                                    element.billing_zip_code,
                                    element.billing_location_name,
                                    element.physical_phone_no,
                                    primary_address_flag,
                                    primary_billing_address_flag,
                                    billing_address_flag,

                                    addData.provider_status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                  ]);
                                  cou++;
                                } else {
                                  arr.push([
                                    addData.provider_id,
                                    cou,
                                    cou,
                                    element.physical_address1,
                                    element.physical_address2,
                                    element.physical_country_id,
                                    element.physical_state_id,
                                    element.physical_city,
                                    element.physical_zip_code,
                                    element.physical_location_name,
                                    element.physical_phone_no,
                                    primary_address_flag,
                                    primary_billing_address_flag,
                                    billing_address_flag,

                                    addData.provider_status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                  ]);
                                  cou++;
                                }

                              });
                              con.query('INSERT INTO provider_location(provider_id, provider_location_id, parent_provider_location_id, address1, address2, country, state, city, zip_code, location_name, primary_phone, primary_address_flag, primary_billing_address_flag, billing_address_flag, status, date_created, date_modified, created_by, modified_by) VALUES ? '
                                ,
                                [
                                  arr
                                ]
                                , function (err, resultProviderLocation) {
                                  if (err) {
                                    con.rollback(function () {
                                      obj.status = 0;
                                      obj.message = "Something wrong with provider location table please try again.";
                                    });
                                  } else {

                                    //BANK STATUS UPDATE

                                    con.query('UPDATE provider_bank SET status=? WHERE provider_id=?',
                                      [0, addData.provider_id],
                                      function (err, updateBankStatus) {
                                        if (err) {
                                          con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong with user table please try again.";
                                          });
                                        } else {
                                          //BANK UPDATE
                                          addData.bank_details.forEach(function (element, idx) {

                                            con.query('INSERT INTO provider_bank(provider_location_id, provider_id, bank_name, bank_address, bank_rounting_no, acct_no, name_on_acct, acct_type, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + addData.provider_id + ' AND location_name = "' + element.location_name + '" AND status = "1" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + addData.provider_id + ', "' + element.bank_name + '", "' + element.bank_address + '", "' + element.rounting_no + '", "' + element.bank_accout + '", "' + element.name_on_account + '", ' + element.bank_account_type_id + ', ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                              , function (err, resultProviderBank) {
                                                console.log('this.sql>>>>>>>>>>>>>>>>>>>>')
                                                console.log(this.sql)
                                                if (err) {
                                                  con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong with provider location table please try again.";
                                                  });
                                                }

                                              });

                                          });
                                          //BANK UPDATE END
                                        }
                                      });

                                    //BANK STATUS UPDATE END

                                    //SPECILITY STATUS UPDATE
                                    con.query('UPDATE provider_loc_spec SET status=? WHERE provider_location_provider_id=?',
                                      [0, addData.provider_id],
                                      function (err, updateSpecialityStatus) {
                                        if (err) {
                                          con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong with user table please try again.";
                                          });
                                        } else {
                                          //SPECIALITY UPDATE

                                          addData.speciality.forEach(function (element, idx) {

                                            con.query('INSERT INTO provider_loc_spec(provider_location_provider_location_id, provider_location_provider_id, mdv_speciality_id, procedure_id, status, date_created, date_modified, created_by, modified_by) VALUES((SELECT provider_location_id FROM provider_location WHERE provider_id = ' + addData.provider_id + ' AND location_name = "' + element.spl_location_name + '" AND status = "1" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + addData.provider_id + ', "' + element.specialization + '", "' + element.procedure_id + '", ' + addData.provider_status + ', "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                              , function (err, resultProviderLocationSpl) {
                                                if (err) {
                                                  con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong with provider location table please try again.";
                                                  });
                                                }

                                              });

                                          });

                                          //UPDATE SPECIALITY END
                                        }
                                      });
                                    //SPECILITY STATUS UPDATE END

                                    //DOCTOR STATUS UPDATE
                                    con.query('UPDATE doctors SET status=? WHERE provider_id=?',
                                      [0, addData.provider_id],
                                      function (err, updateDoctorStatus) {
                                        if (err) {
                                          con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong with user table please try again.";
                                          });
                                        } else {
                                          //DOCTOR UPDATE

                                          addData.speciality.forEach(function (element, idx) {

                                            con.query('INSERT INTO doctors(f_name, l_name, location_id, provider_id, mdv_speciality_id, date_created, date_modified, created_by, modified_by) VALUES("' + element.doctor_f_name + '", "' + element.doctor_l_name + '", (SELECT provider_location_id FROM provider_location WHERE provider_id = ' + addData.provider_id + ' AND location_name = "' + element.spl_location_name + '" AND status = "1" AND (parent_provider_location_id IS NULL OR billing_address_flag=1)), ' + addData.provider_id + ', "' + element.specialization + '", "' + current_date + '", "' + current_date + '", ' + current_user_id + ', ' + current_user_id + ')'
                                              , function (err, resultProviderLocationSpl) {
                                                if (err) {
                                                  con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong with doctors table please try again.";
                                                  });
                                                }

                                              });

                                          });

                                          //UPDATE DOCTOR END
                                        }
                                      });
                                    //DOCTOR STATUS UPDATE END

                                    //UPDATE DISCOUNT RATE
                                    con.query('UPDATE provider_discount_fee SET status=? WHERE provider_id=?',
                                      [0, addData.provider_id],
                                      function (err, updateDiscountRateStatus) {
                                        if (err) {
                                          con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong with user table please try again.";
                                          });
                                        } else {
                                          var arrD = addData.discount_rate;
                                          var arrF = addData.additional_fee;

                                          if (addData.additional_fee[0].additional_fee_type_id !== '') {
                                            var arrDF = arrD.concat(arrF);
                                          } else {
                                            var arrDF = arrD;
                                          }

                                          //date.format(now, 'YYYY-MM-DD')

                                          arrDF.forEach(function (element, idx) {
                                            var arrDiscountFee = [];
                                            if (element.discount_rate >= 0) {
                                              if (element.discount_from_date != '' && element.discount_from_date !== null) {
                                                var frA = element.discount_from_date.split('/');
                                                var fromD = frA[2] + '-' + frA[0] + '-' + frA[1];
                                              } else {
                                                var fromD = null;
                                              }
                                              if (element.discount_to_date != '' && element.discount_from_date !== null) {
                                                var toA = element.discount_to_date.split('/');
                                                var toD = toA[2] + '-' + toA[0] + '-' + toA[1];
                                              } else {
                                                var toD = null;
                                              }

                                              element.additional_conv_fee = null
                                              //if (element.discount_rate !== 0) {
                                              var discount_rate = element.discount_rate;
                                              //} else {
                                              //var discount_rate = null
                                              //}

                                              //if (element.redu_discount_rate !== 0) {
                                              var redu_discount_rate = element.redu_discount_rate
                                              //} else {
                                              //var redu_discount_rate = null
                                              //}
                                              arrDiscountFee.push([
                                                element.discount_type_id,
                                                discount_rate,
                                                redu_discount_rate,
                                                element.loan_term_month,
                                                element.additional_conv_fee,
                                                fromD,
                                                toD,


                                                current_date,
                                                current_date,
                                                current_user_id,
                                                current_user_id
                                              ]);
                                            } else {
                                              if (element.additional_fee_type_id !== '' && element.additional_fee_type_id !== undefined) {


                                                if (element.additional_from_date != '' && element.additional_from_date !== null) {
                                                  var FfrA = element.additional_from_date.split('/');
                                                  var FfromD = FfrA[2] + '-' + FfrA[0] + '-' + FfrA[1]
                                                } else {
                                                  var FfromD = null;
                                                }
                                                if (element.additional_to_date != '' && element.additional_to_date !== null) {
                                                  var FtoA = element.additional_to_date.split('/');
                                                  var FtoD = FtoA[2] + '-' + FtoA[0] + '-' + FtoA[1];
                                                } else {
                                                  var FtoD = null;
                                                }

                                                /*var FfrA = element.additional_from_date.split('/');
                                                var FtoA = element.additional_to_date.split('/');

                                                var FfromD = FfrA[2] + '-' + FfrA[0] + '-' + FfrA[1];
                                                var FtoD = FtoA[2] + '-' + FtoA[0] + '-' + FtoA[1];*/

                                                element.discount_rate = null
                                                element.redu_discount_rate = null
                                                element.loan_term_month = null
                                                if(element.additional_fee_type_id && element.additional_conv_fee) {
                                                  arrDiscountFee.push([
                                                    element.additional_fee_type_id,
                                                    element.discount_rate,
                                                    element.redu_discount_rate,
                                                    element.loan_term_month,
                                                    element.additional_conv_fee,
                                                    FfromD,
                                                    FtoD,
                                                    
                                                    current_date,
                                                    current_date,
                                                    current_user_id,
                                                    current_user_id
                                                  ]);
                                                }
                                              }
                                            }


                                            con.query('INSERT INTO discount_fee_ra(mdv_discount_rate_id, discount_rate, reduced_interest_pct, loan_term_month, fee_amount, start_date, end_date, date_created, date_modified, created_by, modified_by) VALUES ?',

                                              [arrDiscountFee],

                                              function (err, resultDiscountFeeRate) {

                                                if (err) {
                                                  con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong with provider discount table please try again.";
                                                  });
                                                } else {
                                                  //UPDATE DISCOUNT FEE
                                                  con.query('INSERT INTO provider_discount_fee(df_ra_id, provider_id, status, date_created, date_modified, created_by, modified_by) VALUES (?,?,?,?,?,?,?)',

                                                    [resultDiscountFeeRate.insertId, addData.provider_id, addData.provider_status, current_date, current_date, current_user_id, current_user_id],

                                                    function (err, resultProviderDiscountFee) {
                                                      if (err) {
                                                        con.rollback(function () {
                                                          obj.status = 0;
                                                          obj.message = "Something wrong with provider location table please try again.";
                                                        });
                                                      } else {
                                                        obj.status = 1;
                                                        obj.message = "Data added in provider discount fee";
                                                      }
                                                    });
                                                  //UPDATE DISCOUNT FEE END
                                                }

                                              });

                                          });
                                        }
                                      });
                                    //UPDATE DISCOUNT RATE END

                                    //UPDATE DOC REPO
                                    if (addData.documents[0].document_name !== '') {
                                      con.query('UPDATE doc_repo SET status=? WHERE provider_id=?',
                                        [0, addData.provider_id],
                                        function (err, updateDiscountRateStatus) {
                                          if (err) {
                                            con.rollback(function () {
                                              obj.status = 0;
                                              obj.message = "Something wrong with doc_repo table please try again.";
                                            });
                                          } else {
                                            var ct = 0;

                                            var oneDrive = require('./microsoft/oneDrive.js');
                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                              if (loginOne.status == 1) {
                                                ///////////////////////////////////
                                                addData.documents.forEach(function (element, idx) {
                                                  if (typeof element.document_upload === 'string') {

                                                    var sql = 'INSERT INTO doc_repo(provider_id, name, file_type, item_id, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)';
                                                    var sqlD = [addData.provider_id, element.document_name, element.document_type, element.item_id, addData.provider_status, current_date, current_date, current_user_id, current_user_id];
                                                    con.query(sql, sqlD
                                                      , function (err, resultProviderLocationSpl) {
                                                        if (err) {
                                                          con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Something wrong with provider location table please try again.";
                                                          });
                                                        }
                                                      });
                                                  } else {
                                                    var file_path = ''

                                                    if (req.files.length !== 0) {
                                                      file_path = req.files[ct].path;
                                                      var filename = req.files[ct].filename;
                                                      const path = require('path');
                                                      var curentfile = {
                                                        filename: filename,
                                                        path: path.join(__dirname, '..', file_path),
                                                        provider_id: addData.provider_id,
                                                      }
                                                      oneDrive.getOneDriveProviderUpload(loginOne.token, curentfile, function (one) {
                                                        if ((one.status == 1)) {
                                                          var item_id = one.item_id;
                                                          const fs = require('fs');
                                                          fs.unlinkSync(curentfile.path)
                                                        } else {
                                                          var item_id = '';
                                                        }
                                                        con.query('INSERT INTO doc_repo(provider_id, name, file_type, file_path, item_id, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?,?)',
                                                          [addData.provider_id, element.document_name, element.document_type, file_path, item_id, addData.provider_status, current_date, current_date, current_user_id, current_user_id]
                                                          , function (err, resultProviderLocationSpl) {
                                                            if (err) {
                                                              con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong with provider location table please try again.";
                                                              });
                                                            }
                                                          });
                                                      })
                                                    } else {
                                                      file_path = null;
                                                      con.query('INSERT INTO doc_repo(provider_id, name, file_type, file_path, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
                                                        [addData.provider_id, element.document_name, element.document_type, file_path, addData.provider_status, current_date, current_date, current_user_id, current_user_id]
                                                        , function (err, resultProviderLocationSpl) {
                                                          if (err) {
                                                            con.rollback(function () {
                                                              obj.status = 0;
                                                              obj.message = "Something wrong with provider location table please try again.";
                                                            });
                                                          }
                                                        });
                                                    }

                                                    ct++;

                                                  }


                                                });
                                                ////////////////////////////////////

                                                ////////////////////////////
                                              } else {
                                                addData.documents.forEach(function (element, idx) {
                                                  if (typeof element.document_upload === 'string') {

                                                    var sql = 'INSERT INTO doc_repo(provider_id, name, file_type, item_id, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)';
                                                    var sqlD = [addData.provider_id, element.document_name, element.document_type, element.item_id, addData.provider_status, current_date, current_date, current_user_id, current_user_id];

                                                  } else {
                                                    var file_path = ''

                                                    if (req.files.length !== 0) {
                                                      file_path = req.files[ct].path;
                                                    } else {
                                                      file_path = ''
                                                    }

                                                    var sql = 'INSERT INTO doc_repo(provider_id, name, file_type, file_path, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)';
                                                    var sqlD = [addData.provider_id, element.document_name, element.document_type, file_path, addData.provider_status, current_date, current_date, current_user_id, current_user_id];
                                                    ct++;

                                                  }
                                                  con.query(sql, sqlD
                                                    , function (err, resultProviderLocationSpl) {
                                                      if (err) {
                                                        con.rollback(function () {
                                                          obj.status = 0;
                                                          obj.message = "Something wrong with provider location table please try again.";
                                                        });
                                                      }
                                                    });

                                                });
                                              }
                                            })





                                          }
                                        });

                                    }
                                    //UPDATE DOC REPO

                                    //UPDATE PROVIDER PAYMENT TERM

                                    /*con.query('UPDATE provider_term SET payment_term_mdv_id = ? WHERE provider_id = ?' 
                                      ,[addData.payment_term, addData.provider_id]
                                      , function(err, resultProviderTerm) {

                                      });*/

                                    //UPDATE PROVIDER PAYMENT TERM


                                    con.commit(function (err) {
                                      if (err) {
                                        con.rollback(function () {
                                          obj.status = 0;
                                          obj.message = "Something wrong please try again.";
                                        });
                                      }
                                      obj.status = 1;
                                      obj.message = "Application submitted for approval";
                                      res.send(obj);
                                    });




                                  }

                                });
                              //LOCATION UPDATE END


                            }
                          });
                        //UPDATE LOCATION STATUS
                      }
                    })
                  //GET MAX LOC ID

                }

              });
            //USER UPDATE END

          }

        });
      //PROVIDER UPDATE END
    });
  })


  app.get('/api/provider-spl-procedure/', jwtMW, (req, res) => {
    con = require('../db');
    con.query('SELECT procedure_name, procedure_id FROM spec_procedure WHERE status=? AND deleted_flag=? AND spec_id=?',
      [
        1,
        0,
        req.query.id
      ]
      , function (error, rows, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1,
            "result": rows
          }
          res.send(obj);
        }
      })
  })

  //CITY SUGGESTIONS
  app.post('/api/provider-city-suggestion/', jwtMW, (req, res) => {
    con = require('../db');

    //return false;
    let sql = '';
    if (req.body.country_id !== 0 && req.body.state_id !== 0) {
      sql = 'SELECT region_id, name FROM regions WHERE status = ? AND country_id = ? AND state_id = ?';
      var edit = 0;
    } else {
      sql = 'SELECT region_id, name AS label FROM regions WHERE status = ?';
      var edit = 1;
    }
    con.query(sql,
      [1, req.body.country_id, req.body.state_id]
      , function (error, suggestions, fields) {
        if (error) {
          var obj = {
            "status": 0,
            "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "suggestions": suggestions
          }
          res.send(obj);

        }
      })
  })

}


