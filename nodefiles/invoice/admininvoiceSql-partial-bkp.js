var getOpenInvoiceDetails = function (con, callback) {
    con.query('SELECT provider_invoice.provider_invoice_id,provider_invoice.invoice_number,provider_invoice.provider_id,provider_invoice.total_amt, provider_invoice.total_hps_discount, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, provider_invoice.mdv_invoice_status_id, master_data_values.value as invoice_status, '
        + 'provider.provider_id,provider.name,provider.primary_phone, '
        + 'provider_location.address1, provider_location.address2, provider_location.city, provider_location.zip_code, states.name as state_name,countries.name as country_name, '
        + 'provider.name '
        + 'FROM provider_invoice '
        + 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id AND md_id=? '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
        + 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'INNER JOIN countries ON countries.id=states.country_id '
        + ' ORDER BY provider_invoice.provider_invoice_id DESC'
        ,
        [
            'Provider Invoice Status',
        ]
        , function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows
                }
                return callback(obj);
            }
        })
}
var getOpenSingleInvoiceDetails = function (con, invoice_id, callback) {

    con.query('SELECT provider_invoice.provider_invoice_id,provider_invoice.provider_id,provider_invoice.total_amt, provider_invoice.total_hps_discount, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, provider_invoice.mdv_invoice_status_id, master_data_values.value as invoice_status, '
        + 'provider.name '
        + 'FROM provider_invoice '
        + 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id=provider_invoice.mdv_invoice_status_id AND md_id=? '
        + 'WHERE provider_invoice.provider_invoice_id=? AND provider_invoice.mdv_invoice_status_id=? ORDER BY provider_invoice.provider_invoice_id DESC'
        ,
        [
            'Provider Invoice Status',
            invoice_id,
            1
        ]
        , function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": (Object.keys(rows).length > 0) ? rows[0] : '',
                }
                return callback(obj);
            }
        })
}
var getAdminInvoiceDetails = function (con, id, callback) {
    var obj = {};
    con.query('SELECT provider_invoice_detial.provider_id,provider_invoice_detial.provider_invoice_id,patient_procedure.application_id,patient_procedure.procedure_amt,patient_procedure.procedure_status, credit_applications.application_no, credit_applications.amount as loan_amount, '
        + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,regions.name,states.name '
        + 'FROM patient_procedure '
        + 'INNER JOIN credit_applications ON credit_applications.application_id=patient_procedure.application_id '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        + 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
        + 'INNER JOIN states ON states.state_id=regions.state_id '
        + 'INNER JOIN provider_invoice_detial ON credit_applications.application_id=provider_invoice_detial.application_id '
        + 'WHERE provider_invoice_detial.provider_invoice_id=?'
        ,
        [
            id,
        ]
        , function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows
                }
                return callback(obj);
            }
        })

}
var deleteAdminInvoiceapplication = function (con, data, callback) {
    
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    getOpenSingleInvoiceDetails(con, data.invoice_id, function (invoiceData) {
        if (invoiceData.status == 1) {
            getSelectedApp(con, data.provider_id, data.pp_id, function (appArray) {
                
                var amount = appArray.result.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['total_amount'] = currentValue.loan_amount;
                        accumulator['discount_amount'] = currentValue.hps_discount_amt;
                    } else {
                        accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                        accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
                    }
                    return accumulator
                }, []);
                
                con.beginTransaction(function (err) {
                    if (err) {
                        obj.status = 0;
                        obj.message = "Sorry we are not able to process your request please try again later.";
                        return callback(obj);
                    }
                    con.query('UPDATE provider_invoice SET '
                        + 'total_amt=?, '
                        + 'total_hps_discount=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE provider_invoice_id=? ',
                        [
                            (invoiceData.result.total_amt - amount.total_amount),
                            (invoiceData.result.total_hps_discount - amount.discount_amount),
                            current_date,
                            data.current_user_id,
                            data.invoice_id,
                        ]
                        , function (err, invoice) {
                            
                            if (err) {
                                obj.status = 0;
                                obj.message = "Sorry we are not able to process your request please try again later.";
                                return callback(obj);
                            } else {

                                con.query('UPDATE provider_invoice_detial SET '
                                    + 'note=?, '
                                    + 'status=?, '
                                    + 'date_modified=?, '
                                    + 'modified_by=? '
                                    + 'WHERE provider_invoice_id=? AND pp_id=?'
                                    ,
                                    [
                                        data.comment,
                                        1,
                                        current_date,
                                        data.current_user_id,
                                        data.invoice_id,
                                        data.pp_id,
                                    ]
                                    , function (error, rows, fields) {
                                        
                                        if (error) {
                                            obj.status = 0;
                                            obj.message = "Sorry we are not able to process your request please try again later.";
                                            return callback(obj);
                                        } else {
                                            con.commit(function (errCommit) {
                                                if (errCommit) {
                                                    obj.status = 0;
                                                    obj.message = "Sorry we are not able to process your request please try again later.";
                                                    return callback(obj);
                                                } else {
                                                    obj.status = 1;
                                                    obj.result = rows;
                                                    return callback(obj);
                                                }
                                            })
                                        }

                                    })
                            }
                        })
                });
            })
        } else {
            obj.status = 0;
            obj.message = "Sorry we are not able to process your request please try again later.";
            return callback(obj);
        }

    })

}
var listInvoiceApplication = function (con, data, callback) {
    con.query('SELECT patient_procedure.application_id,patient_procedure.procedure_amt,patient_procedure.procedure_status, credit_applications.application_no, credit_applications.amount as loan_amount, '
        + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,regions.name,states.name '
        + 'FROM patient_procedure '
        + 'INNER JOIN credit_applications ON credit_applications.application_id=patient_procedure.application_id AND credit_applications.status=1 AND credit_applications.plan_flag=1 '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 '
        + 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
        + 'INNER JOIN states ON states.state_id=regions.state_id '
        + 'WHERE patient.provider_id=? AND credit_applications.application_id NOT IN (SELECT provider_invoice_detial.application_id FROM provider_invoice_detial WHERE provider_id=?)'
        ,
        [
            data.provider_id,
            data.provider_id
        ]
        , function (error, rows, fields) {
            
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows
                }
                return callback(obj);
            }
        })
}

var getSelectedApp = function (con, provider_id, pp_id, callback) {

    var obj = {};
    con.query('SELECT provider.provider_id,provider.name,provider.primary_phone, '
        + 'provider_location.address1, provider_location.address2, provider_location.city, provider_location.county, regions.name as regions_name,states.name as state_name,countries.name as country_name '
        + 'FROM provider '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.primary_address_flag=1 '
        + 'INNER JOIN regions ON provider_location.region=regions.region_id '
        + 'INNER JOIN states ON states.state_id=regions.state_id '
        + 'INNER JOIN countries ON countries.id=states.country_id '
        + 'WHERE provider.provider_id=?'
        ,
        [
            provider_id
        ]
        , function (error, provider, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                con.query('SELECT patient_procedure.application_id,patient_procedure.provider_location_id,patient_procedure.provider_id,patient_procedure.doctor_id,patient_procedure.application_id,patient_procedure.procedure_amt,patient_procedure.hps_discount_amt,patient_procedure.procedure_status, credit_applications.application_no, payment_plan.pp_id, payment_plan.loan_amount as loan_amount, '
                    + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
                    + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name '
                    + 'FROM patient_procedure '
                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_procedure.pp_id '
                    + 'INNER JOIN credit_applications ON credit_applications.application_id=patient_procedure.application_id '
                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                    + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                    /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                    + 'WHERE patient_procedure.provider_id=? AND patient_procedure.pp_id IN(?)'
                    ,
                    [
                        provider_id, pp_id,
                    ]
                    , function (error, rows, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.result = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            obj.result = rows;
                            obj.provider = (provider.length > 0) ? provider[0] : '';
                            return callback(obj);
                        }
                    })

            }
        })

}

var addInvoiceApplication = function (con, data, invoiceData, callback) {

    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    getSelectedApp(con, invoiceData.provider_id, data.application_id, function (appArray) {
        
        var amount = appArray.result.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex == 0) {
                accumulator['total_amount'] = currentValue.loan_amount;
                accumulator['discount_amount'] = currentValue.hps_discount_amt;
            } else {
                accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
            }
            return accumulator
        }, []);
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Sorry we are not able to process your request please try again later.";
                return callback(obj);
            }
            con.query('UPDATE provider_invoice SET '
                + 'total_amt=?, '
                + 'total_hps_discount=?, '
                + 'date_modified=?, '
                + 'modified_by=? '
                + 'WHERE provider_invoice_id=? ',
                [
                    (amount.total_amount + invoiceData.total_amt),
                    (amount.discount_amount + invoiceData.total_hps_discount),
                    current_date,
                    current_user_id,
                    invoiceData.provider_invoice_id,
                ]
                , function (err, invoice) {
                    if (err) {
                        obj.status = 0;
                        obj.message = "Sorry we are not able to process your request please try again later.";
                        return callback(obj);
                    } else {
                        var values = [];
                        appArray.result.forEach(function (element, idx) {
                            values.push([invoiceData.provider_invoice_id, element.provider_location_id, element.provider_id, element.procedure_id, element.application_id, current_date, current_date, current_user_id, current_user_id]);
                        });
                        con.query('INSERT INTO `provider_invoice_detial`(`provider_invoice_id`, `provider_location_id`, `provider_id`, `procedure_id`, `application_id`, `date_created`, `date_modified`, `created_by`, `modified_by`) VALUES ?'
                            , [
                                values
                            ]
                            , function (error, invoiceDetails) {
                                if (error) {
                                    obj.status = 0;
                                    obj.message = "Sorry we are not able to process your request please try again later.";
                                    return callback(obj);
                                } else {
                                    con.commit(function (errCommit) {
                                        if (errCommit) {
                                            obj.status = 0;
                                            obj.message = "Sorry we are not able to process your request please try again later.";
                                            return callback(obj);
                                        } else {
                                            obj.status = 1;
                                            obj.message = "Invoice updated successfully.";
                                            return callback(obj);
                                        }
                                    })
                                }

                            })
                    }
                })
        });
    });
}

var cancelInvoice = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Sorry we are not able to process your request please try again later.";
            return callback(obj);
        }
        con.query('UPDATE provider_invoice SET '
            + 'total_amt=?, '
            + 'total_hps_discount=?, '
            + 'mdv_invoice_status_id=?, '
            + 'comment=?, '
            + 'date_modified=?, '
            + 'modified_by=? '
            + 'WHERE provider_invoice_id=?'
            ,
            [
                0,
                0,
                2,
                data.commentNote,
                current_date,
                current_user_id,
                data.invoice_id,
            ]
            , function (error, provider, fields) {
                if (error) {
                    obj.status = 0;
                    obj.result = "Sorry we are not able to process your request please try again later.";
                    return callback(obj);
                } else {
                    con.query('DELETE FROM provider_invoice_detial '
                        + 'WHERE provider_invoice_id=?'
                        ,
                        [
                            data.invoice_id,
                        ]
                        , function (error, rows, fields) {
                            if (error) {
                                obj.status = 0;
                                obj.message = "Sorry we are not able to process your request please try again later.";
                                return callback(obj);
                            } else {
                                con.commit(function (errCommit) {
                                    if (errCommit) {
                                        obj.status = 0;
                                        obj.message = "Sorry we are not able to process your request please try again later.";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.message = "Invoice cancellation process completed";
                                        return callback(obj);
                                    }
                                })
                            }

                        })
                }
            })
    });

}

var confirmInvoice = function (con, data, callback) {
    console.log(data)
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    var obj = {};

    con.query('INSERT INTO provider_invoice_partial (`provider_invoice_id`, `check_amount`, `bank_name`, `txn_no`, `date_paid`, `comment`, `mdv_invoice_status_id`, `date_created`, `date_modified`, `created_by`, `modified_by`) VALUES ?'
        ,
        [
            data.invoice_id,
            data.amount,
            data.bank_name,
            data.check_number,
            data.paid_date,
            data.comment,
            5,
            current_date,
            current_date,
            current_user_id,
            current_user_id,
        ]
        , function (err, partial_invoice) {
            if (err) {
                obj.status = 0;
                obj.message = "Partial Payment not inserted.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Partial Payment confirmed.";
                return callback(obj);
            }
        })

    /*con.query('UPDATE provider_invoice SET '
        + 'mdv_invoice_status_id=?, '
        + 'date_modified=?, '
        + 'modified_by=? '
        + 'WHERE provider_invoice_id=?'
        ,
        [
            data.amount,
            data.bank_name,
            data.check_number,
            data.paid_date,
            5,
            data.comment,
            current_date,
            current_user_id,
            data.invoice_id,
        ]
        , function (error, provider, fields) {
            if (error) {
                obj.status = 0;
                obj.message = "Payment not confirmed. Please try again later.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Payment confirmed.";
                return callback(obj);
            }
        })*/
}

var getCloseInvoiceDetails = function (con, callback) {
    con.query('SELECT provider_invoice.provider_invoice_id,provider_invoice.provider_id,provider_invoice.total_amt, provider_invoice.total_hps_discount, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, provider_invoice.mdv_invoice_status_id, master_data_values.value as invoice_status, '
        + 'provider.name '
        + 'FROM provider_invoice '
        + 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
        + 'INNER JOIN master_data_values ON master_data_values.invoice_status_id=provider_invoice.mdv_invoice_status_id AND md_id=? '
        + 'WHERE provider_invoice.mdv_invoice_status_id=? ORDER BY provider_invoice.provider_invoice_id DESC'
        ,
        [
            'Provider Invoice Status',
            3
        ]
        , function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows
                }
                return callback(obj);
            }
        })
}

var cancelConfirmations = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    var obj = {};

    con.query('UPDATE provider_invoice SET '
        + 'check_amount=?, '
        + 'bank_name=?, '
        + 'txn_no=?, '
        + 'date_paid=?, '
        + 'mdv_invoice_status_id=?, '
        + 'comment=?, '
        + 'date_modified=?, '
        + 'modified_by=? '
        + 'WHERE provider_invoice_id=?'
        ,
        [
            null,
            null,
            null,
            null,
            1,
            null,
            current_date,
            current_user_id,
            data.invoice_id,
        ]
        , function (error, provider, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Sorry we are not able to process your request please try again later.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Invoice cancellation process completed";
                return callback(obj);
            }
        })

}

var getAdminInvoiceNotes = function (con, id, callback) {
    var obj = {};
    con.query('SELECT check_amount,bank_name,txn_no,comment, DATE_FORMAT(date_paid,"%m/%d/%Y") as date_paid  '
        + 'FROM provider_invoice '
        + 'WHERE provider_invoice_id=?'
        ,
        [
            id,
        ]
        , function (error, rows, fields) {
            
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": rows
                }
                return callback(obj);
            }
        })

}

var providerInvoiceView = function (con, id, callback) {
    con.query('SELECT provider_invoice.invoice_number,DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created,provider.provider_id,provider.provider_ac,provider.name,provider.primary_phone, '
        + 'provider_location.address1, provider_location.address2, provider_location.city,provider_location.zip_code, states.name as state_name,countries.name as country_name '
        + 'FROM provider_invoice '
        + 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.primary_address_flag=1 '
        /*+ 'INNER JOIN regions ON provider_location.region=regions.region_id '*/
        + 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'INNER JOIN countries ON countries.id=states.country_id '
        + 'WHERE provider_invoice.provider_invoice_id=?'
        ,
        [id]
        , function (error, provider, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                return callback(obj);
            } else {
                con.query('SELECT provider_invoice_detial.provider_id,provider_invoice_detial.provider_invoice_id,patient_procedure.application_id,patient_procedure.procedure_amt,patient_procedure.hps_discount_amt,patient_procedure.procedure_status, credit_applications.application_no, payment_plan.pp_id,payment_plan.plan_number, payment_plan.loan_amount as loan_amount, '
                    + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
                    + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name '
                    + 'FROM provider_invoice_detial '
                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=provider_invoice_detial.pp_id '
                    + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                    + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status '
                    /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                    + 'INNER JOIN patient_procedure ON patient_procedure.pp_id=provider_invoice_detial.pp_id '
                    + 'WHERE provider_invoice_detial.provider_invoice_id=? AND provider_invoice_detial.status=?'
                    ,
                    [
                        id,
                        0
                    ]
                    , function (error, rows, fields) {
                        if (error) {
                            var obj = {
                                "status": 0,
                                "message": "Something wrong please try again."
                            }
                            return callback(obj);
                        } else {
                            var obj = {
                                "status": 1,
                                "result": rows,
                                "provider": (provider.length > 0) ? provider[0] : '',
                            }
                            return callback(obj);
                        }
                    })
            }
        });
}

var approveInvoice = function (con, data, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    var obj = {};

    con.query('UPDATE provider_invoice SET '
        + 'mdv_invoice_status_id=?, '
        + 'date_modified=?, '
        + 'modified_by=? '
        + 'WHERE provider_invoice_id=?'
        ,
        [
            3,
            current_date,
            current_user_id,
            data.invoice_id,
        ]
        , function (error, provider, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Sorry we are not able to process your request please try again later.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Invoice approved successfully.";
                return callback(obj);
            }
        })
}

var providerDetails = function (con, data, callback) {
    var obj = {};
    con.query('SELECT provider_invoice.invoice_number,provider.name,provider.email, '
        + 'provider_location.address1, provider_location.address2, provider_location.city, provider_location.zip_code, states.name as state_name,provider_location.primary_phone '
        + 'FROM provider_invoice '
        + 'INNER JOIN provider ON provider.provider_id=provider_invoice.provider_id '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
        + 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'WHERE provider_invoice.provider_invoice_id=?'
        ,
        [
            data.invoice_id
        ]
        , function (error, provider, fields) {
            if (error) {
                obj.status = 0;
                return callback(obj);
            } else {
                obj.status = 1;
                obj.provider = (provider.length>0)?provider[0]:'';
                return callback(obj);
            }
        })
}

exports.getOpenInvoiceDetails = getOpenInvoiceDetails;
exports.getOpenSingleInvoiceDetails = getOpenSingleInvoiceDetails;
exports.getAdminInvoiceDetails = getAdminInvoiceDetails;
exports.deleteAdminInvoiceapplication = deleteAdminInvoiceapplication;
exports.listInvoiceApplication = listInvoiceApplication;
exports.addInvoiceApplication = addInvoiceApplication;
exports.cancelInvoice = cancelInvoice;
exports.confirmInvoice = confirmInvoice;
exports.getCloseInvoiceDetails = getCloseInvoiceDetails;
exports.cancelConfirmations = cancelConfirmations;
exports.getAdminInvoiceNotes = getAdminInvoiceNotes;
exports.providerInvoiceView = providerInvoiceView;
exports.approveInvoice = approveInvoice;
exports.providerDetails = providerDetails;