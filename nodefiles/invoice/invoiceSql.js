var getSelectedApplication = function (con, data, callback) {
    console.log(data)
    console.log(data.provider_id)
    var obj = {};
    con.query('SELECT provider.provider_id,provider.provider_ac,provider.name,provider.primary_phone, '
        + 'provider_location.address1, provider_location.address2, provider_location.city,provider_location.zip_code, provider_location.county, states.name as state_name,countries.name as country_name, '
        //+ '((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM provider_invoice as p)) as invoice_number,SUM(provider_refund.refund_due) as refund_due,SUM(provider_refund_details.paid_amount) as iou_paid_amount '
        + '((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM provider_invoice as p)) as invoice_number '
        + 'FROM provider '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '
        /*+ 'INNER JOIN regions ON provider_location.region=regions.region_id '*/
        + 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'INNER JOIN countries ON countries.id=states.country_id '
        //+ 'LEFT JOIN provider_refund ON provider_refund.provider_id=provider.provider_id AND provider_refund.iou_flag=? '
        //+ 'LEFT JOIN provider_refund_details ON provider_refund.refund_id=provider_refund_details.refund_id '
        + 'WHERE provider.provider_id='+data.provider_id
        ,
        [
            1
        ]
        , function (error, provider, fields) {
            console.log(this.sql)
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                con.query('SELECT patient_procedure.application_id,patient_procedure.provider_location_id,patient_procedure.provider_id,patient_procedure.doctor_id,patient_procedure.application_id,patient_procedure.procedure_amt,patient_procedure.hps_discount_amt,patient_procedure.procedure_status, credit_applications.application_no, payment_plan.pp_id, payment_plan.plan_number, payment_plan.loan_amount as loan_amount, '
                    + 'patient.patient_ac,patient.f_name,patient.m_name,patient.l_name, DATE_FORMAT(CAST(AES_DECRYPT(patient.dob, "ramesh_cogniz") as CHAR),"%m/%d/%Y") as dob, patient.peimary_phone,  '
                    + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name, DATE_FORMAT(patient_procedure.procedure_date,"%m/%d/%Y") as procedure_date '
                    + 'FROM patient_procedure '
                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=patient_procedure.pp_id '
                    + 'INNER JOIN credit_applications ON credit_applications.application_id=patient_procedure.application_id '
                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                    + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
                    /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                    + 'WHERE patient_procedure.provider_id=? AND patient_procedure.pp_id IN(?)'
                    ,
                    [
                        data.provider_id, data.pp_id,
                    ]
                    , function (error, rows, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.result = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            con.query('SELECT '
                                //+ 'SUM(provider_refund.refund_due) as refund_due, '
                                + 'provider_refund.refund_due, '
                                //+ '(SELECT SUM(paid_amount) FROM provider_refund_details WHERE provider_refund.refund_id=provider_refund_details.refund_id) as iou_paid_amount '
                                + '(SELECT SUM(paid_amount) FROM provider_refund_details WHERE provider_refund.refund_id=provider_refund_details.refund_id) as iou_paid_amount '
                                + 'FROM provider_refund '
                                + 'WHERE provider_refund.iou_flag=? AND provider_refund.provider_id=?'
                                ,
                                [
                                    1, data.provider_id
                                ]
                                , function (error, IOUamt, fields) {
                                    if (error) {
                                        obj.status = 0;
                                        obj.result = "Something wrong please try again.";
                                        return callback(obj);
                                    } else {
                                        if (IOUamt.length > 0) {
                                            let refund_due = 0;
                                            let iou_paid_amount = 0;
                                            IOUamt = IOUamt.reduce(function (accumulator, currentValue, currentindex) {
                                                refund_due += currentValue.refund_due;
                                                iou_paid_amount += currentValue.iou_paid_amount;
                                                return accumulator;
                                              }, []);
                                            provider[0].refund_due = refund_due;//IOUamt[0].refund_due;
                                            provider[0].iou_paid_amount = iou_paid_amount;//IOUamt[0].iou_paid_amount;
                                            obj.status = 1;
                                            obj.result = rows;
                                            obj.invoice_number = data.invoice_number;
                                            obj.invoice_status = data.invoice_status;
                                            obj.invoiceStatus = data.invoiceStatus;
                                            obj.check_amount = data.check_amount;
                                            obj.bank_name = data.bank_name;
                                            obj.txn_no = data.txn_no;
                                            obj.date_paid = data.date_paid;
                                            obj.comment = data.comment;
                                            obj.date_created = data.date_created;
                                            obj.provider = (provider.length > 0) ? provider[0] : '';
                                            return callback(obj);
                                        } else {
                                            provider[0].refund_due = null;
                                            provider[0].iou_paid_amount = null;
                                            obj.status = 1;
                                            obj.result = rows;
                                            obj.invoice_number = data.invoice_number;
                                            obj.invoice_status = data.invoice_status;
                                            obj.invoiceStatus = data.invoiceStatus;
                                            obj.check_amount = data.check_amount;
                                            obj.bank_name = data.bank_name;
                                            obj.txn_no = data.txn_no;
                                            obj.date_paid = data.date_paid;
                                            obj.comment = data.comment;
                                            obj.date_created = data.date_created;
                                            obj.provider = (provider.length > 0) ? provider[0] : '';
                                            return callback(obj);
                                        }
                                    }
                                })
                        }
                    })

            }
        })

}
var setInvoice = function (con, data, current_user_id, provider_id, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    var amount = data.result.reduce(function (accumulator, currentValue, currentindex) {
        if (currentindex == 0) {
            accumulator['total_amount'] = currentValue.loan_amount;
            accumulator['discount_amount'] = currentValue.hps_discount_amt;
        } else {
            accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
            accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
        }
        return accumulator
    }, []);

    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Sorry we are not able to process your request please try again later.";
            return callback(obj);
        }
        con.query('INSERT INTO provider_invoice SET '
            + 'provider_id=?, '
            + 'invoice_number=((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM provider_invoice as p)), '
            + 'total_amt=?, '
            + 'total_hps_discount=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=? ',
            [
                provider_id,
                amount.total_amount,
                amount.discount_amount,
                current_date,
                current_date,
                current_user_id,
                current_user_id,
            ]
            , function (err, invoice) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Sorry we are not able to process your request please try again later.";
                    return callback(obj);
                } else {
                    var values = [];
                    data.result.forEach(function (element, idx) {
                        values.push([invoice.insertId, element.provider_location_id, element.provider_id, element.doctor_id, element.application_id, element.pp_id, current_date, current_date, current_user_id, current_user_id]);
                    });
                    con.query('INSERT INTO `provider_invoice_detial`(`provider_invoice_id`, `provider_location_id`, `provider_id`, `doctor_id`, `application_id`, `pp_id`, `date_created`, `date_modified`, `created_by`, `modified_by`) VALUES ?'
                        , [
                            values
                        ]
                        , function (error, invoiceDetails) {
                            if (error) {
                                obj.status = 0;
                                obj.message = "Sorry we are not able to process your request please try again later.";
                                return callback(obj);
                            } else {
                                con.commit(function (errCommit) {
                                    if (errCommit) {
                                        obj.status = 0;
                                        obj.message = "Sorry we are not able to process your request please try again later.";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.message = "Invoice created successfully.";
                                        return callback(obj);
                                    }
                                })
                            }

                        })
                }
            });

    })
    return false;
};
var getInvoiceApplication = function (con, data, callback) {

    var obj = {};
    con.query('SELECT provider_invoice_detial.provider_id,provider_invoice_detial.pp_id,provider_invoice.invoice_number, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, master_data_values.value, '
        //+ 'provider_invoice.mdv_invoice_status_id, provider_invoice.check_amount, provider_invoice.bank_name, provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y")date_paid, provider_invoice.comment,SUM(provider_refund.refund_due) as refund_due,SUM(paid_amount) as iou_paid_amount '
        + 'provider_invoice.mdv_invoice_status_id, provider_invoice.check_amount, provider_invoice.bank_name, provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y")date_paid, provider_invoice.comment '
        + 'FROM provider_invoice_detial '
        + 'INNER JOIN provider_invoice ON provider_invoice.provider_invoice_id=provider_invoice_detial.provider_invoice_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id = provider_invoice.mdv_invoice_status_id '
        + 'AND md_id = ? '
        //+ 'LEFT JOIN provider_refund_details ON provider_invoice.provider_invoice_id = provider_refund_details.provider_invoice_id '
        //+ 'LEFT JOIN provider_refund ON provider_refund.refund_id=provider_refund_details.refund_id AND provider_refund.iou_flag=? '
        + 'WHERE provider_invoice_detial.provider_invoice_id=? AND provider_invoice_detial.status=?'
        ,
        [
            'Provider Invoice Status', data.invoice_id, 0
        ]
        , function (error, application, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                var ppId = application.reduce(function (accumulator, currentValue) {
                    accumulator.push(currentValue.pp_id);
                    return accumulator
                }, []);
                con.query('SELECT provider_refund.refund_id,provider_refund.provider_id,SUM(provider_refund.refund_due) as refund_due, SUM(paid_amount) as iou_paid_amount '
                    //+ '(SELECT SUM(paid_amount) FROM provider_refund_details WHERE provider_refund.refund_id=provider_refund_details.refund_id) as iou_paid_amount '
                    + 'FROM provider_refund '
                    + 'INNER JOIN provider_refund_details ON provider_refund.refund_id=provider_refund_details.refund_id '
                    + 'WHERE provider_refund_details.provider_invoice_id=? AND provider_refund.iou_flag=?'
                    ,
                    [
                        data.invoice_id, 1
                    ]
                    , function (iouError, iouAmt, fields) {
                        if (iouError || iouAmt.length == 0) {
                            obj.status = 1;
                            obj.pp_id = ppId;
                            obj.invoice_number = (application.length > 0) ? application[0].invoice_number : '';
                            obj.date_created = (application.length > 0) ? application[0].date_created : '';
                            obj.invoiceStatus = (application.length > 0) ? application[0].mdv_invoice_status_id : '';
                            obj.invoice_status = (application.length > 0) ? application[0].value : '';
                            obj.check_amount = (application.length > 0) ? application[0].check_amount : '';
                            obj.bank_name = (application.length > 0) ? application[0].bank_name : '';
                            obj.txn_no = (application.length > 0) ? application[0].txn_no : '';
                            obj.date_paid = (application.length > 0) ? application[0].date_paid : '';
                            obj.comment = (application.length > 0) ? application[0].comment : '';
                            obj.refund_due = '';
                            obj.iou_paid_amount = '';

                            obj.provider_id = data.provider_id
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            obj.pp_id = ppId;
                            obj.invoice_number = (application.length > 0) ? application[0].invoice_number : '';
                            obj.date_created = (application.length > 0) ? application[0].date_created : '';
                            obj.invoiceStatus = (application.length > 0) ? application[0].mdv_invoice_status_id : '';
                            obj.invoice_status = (application.length > 0) ? application[0].value : '';
                            obj.check_amount = (application.length > 0) ? application[0].check_amount : '';
                            obj.bank_name = (application.length > 0) ? application[0].bank_name : '';
                            obj.txn_no = (application.length > 0) ? application[0].txn_no : '';
                            obj.date_paid = (application.length > 0) ? application[0].date_paid : '';
                            obj.comment = (application.length > 0) ? application[0].comment : '';
                            obj.refund_due = (iouAmt.length > 0) ? iouAmt[0].refund_due : '';
                            obj.iou_paid_amount = (iouAmt.length > 0) ? iouAmt[0].iou_paid_amount : '';

                            obj.provider_id = data.provider_id
                            return callback(obj);
                        }
                    })
            }
        })

}
var getInvoiceApplicationView = function (con, data, callback) {

    var obj = {};
    con.query('SELECT provider_invoice_detial.pp_id,provider_invoice.invoice_number, DATE_FORMAT(provider_invoice.date_created, "%m/%d/%Y") as date_created, master_data_values.value, '
        + 'provider_invoice.mdv_invoice_status_id, provider_invoice.check_amount, provider_invoice.bank_name, provider_invoice.txn_no, DATE_FORMAT(provider_invoice.date_paid, "%m/%d/%Y")date_paid, provider_invoice.comment '
        + 'FROM provider_invoice_detial '
        + 'INNER JOIN provider_invoice ON provider_invoice.provider_invoice_id=provider_invoice_detial.provider_invoice_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id = provider_invoice.mdv_invoice_status_id '
        + 'AND md_id = ? '
        + 'WHERE provider_invoice_detial.provider_invoice_id=? AND provider_invoice_detial.status=?'
        ,
        [
            'Provider Invoice Status', data.invoice_id, 0
        ]
        , function (error, application, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                var ppId = application.reduce(function (accumulator, currentValue) {
                    accumulator.push(currentValue.pp_id);
                    return accumulator
                }, []);
                obj.status = 1;
                obj.pp_id = ppId;
                obj.invoice_number = (application.length > 0) ? application[0].invoice_number : '';
                obj.date_created = (application.length > 0) ? application[0].date_created : '';
                obj.invoiceStatus = (application.length > 0) ? application[0].mdv_invoice_status_id : '';
                obj.invoice_status = (application.length > 0) ? application[0].value : '';
                obj.check_amount = (application.length > 0) ? application[0].check_amount : '';
                obj.bank_name = (application.length > 0) ? application[0].bank_name : '';
                obj.txn_no = (application.length > 0) ? application[0].txn_no : '';
                obj.date_paid = (application.length > 0) ? application[0].date_paid : '';
                obj.comment = (application.length > 0) ? application[0].comment : '';
                obj.refund_due = (application.length > 0) ? application[0].refund_due : '';
                obj.iou_paid_amount = (application.length > 0) ? application[0].iou_paid_amount : '';

                obj.provider_id = data.provider_id
                return callback(obj);
            }
        })

}
var getProviderDetails = function (con, id, callback) {
    var obj = {};
    con.query('SELECT provider.provider_id,provider.provider_ac,provider.name,provider.primary_phone, '
        + 'provider_location.address1, provider_location.address2, provider_location.city,provider_location.zip_code, provider_location.county, states.name as state_name,countries.name as country_name '
        + 'FROM provider '
        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.primary_address_flag=1 '
        /*+ 'INNER JOIN regions ON provider_location.region=regions.region_id '*/
        + 'INNER JOIN states ON states.state_id=provider_location.state '
        + 'INNER JOIN countries ON countries.id=states.country_id '
        + 'WHERE provider.provider_id=?'
        ,
        [
            id
        ]
        , function (error, provider, fields) {

            if (error) {
                obj.status = 0;
                obj.message = "Provider Not Found.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.providerDetails = (provider.length > 0) ? provider[0] : '';
                return callback(obj);
            }
        })
}


exports.getSelectedApplication = getSelectedApplication;
exports.setInvoice = setInvoice;
exports.getInvoiceApplication = getInvoiceApplication;
exports.getInvoiceApplicationView = getInvoiceApplicationView;
exports.getProviderDetails = getProviderDetails;