module.exports = (data, logoimg) => {
   console.log(data)
   let date = require('date-and-time');
   let now = new Date();
   let current_date = date.format(now, 'MM/DD/YYYY');
   var amount = data.result.reduce(function (accumulator, currentValue, currentindex) {
      if (currentindex == 0) {
         accumulator['total_amount'] = currentValue.loan_amount;
         accumulator['discount_amount'] = currentValue.hps_discount_amt;
      } else {
         accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
         accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
      }
      return accumulator
   }, []);
   var html = '';
   data.result.forEach(function (element, idx) {
      html = html + '<tr>'
         + '<td>' + element.plan_number + '</td>'
         + '<td>' + element.patient_ac + '</td>'
         + '<td>' + element.application_no + '</td>'
         + '<td>' + element.f_name + '</td>'
         + '<td>' + element.m_name + '</td>'
         + '<td>' + element.l_name + '</td>'
         + '<td>' + element.dob + '</td>'
         + '<td>' + element.peimary_phone + '</td>'
         + '<td>' + element.City + '</td>'
         + '<td>' + element.name + '</td>'
         + '<td>$' + parseFloat(element.loan_amount).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>PDF Result Template</title>
      <style>
         .invoice-box {
         max-width: 1200px;
         margin: auto;
         padding: 0px;
         font-size: 8px;
         line-height: 15px;
         font-family: 'Arial', sans-serif;
         color: #303030;
         }
         .margin-top {
         margin-top: 50px;
         }
         .justify-center {
         text-align: center;
         }
         .invoice-box table {
         width: 100%;
         line-height: inherit;
         text-align: left;
      border:solid 1px #ddf1ff; border-collapse:collapse;}
      .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
      .provider-info{width:38%; float:left}
      .invoice-info{width:30%; float:left;}
      .mid-info {width:28%; float:left; margin-right: 2%; margin-left:2%}
      .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:8px;}
      .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:8px;}
      .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
      .info-bx:after{margin-bottom:20px;}
      .detail-table th{background:#0e5d97; color:#fff;padding:2px 2px; font-size:8px; line-height:10px;}
      .detail-table td{padding:2px 2px; font-size:8px;}
         
         
      </style>
   </head>
   <body>
      <div class="invoice-box">
      <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
     <h1 class="justify-center">Provider Invoice</h1>
     <div class="info-bx">
        <div class="provider-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">
              <tr>
                 <th>Provider Information</th>
              </tr>
              <tr>
                 <td><strong>Account No: </strong> ${data.provider.provider_ac}</td>
              </tr>
              <tr>
                 <td><strong>Name: </strong> ${data.provider.name}</td>
              </tr>
              <tr>
                 <td><strong>Address: </strong> ${data.provider.address1 + ' ' + data.provider.address2 + ' ' + data.provider.city + ', ' + data.provider.state_name + ' - ' + data.provider.zip_code}</td>
              </tr>
              <tr>
                 <td><strong>Phone: </strong> ${data.provider.primary_phone}</td>
              </tr>
              <tr>
                 <td>&nbsp;</td>
              </tr>
           </table>
        </div>
     
        <div class="mid-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">
              <tr>
                 <th>Invoice Information</th>
              </tr>
              <tr>
                 <td><strong>Invoice Number: </strong> ${(data.invoice_number !== undefined) ? data.invoice_number : data.provider.invoice_number}</td>
              </tr>
              <tr>
                 <td><strong>Invoice Date: </strong> ${(data.date_created !== undefined) ? data.date_created : current_date}</td>
              </tr>
              <tr>
                 <td><strong>No of accounts included: </strong> ${data.result.length}</td>
              </tr>
              <tr>
                 <td><strong>Invoice Amount: </strong> $${parseFloat(amount.total_amount).toFixed(2)}</td>
              </tr>
              <tr>
                 <td>&nbsp;</td>
              </tr>
           </table>
        </div>
        <div class="invoice-info">
           <table cellpadding="0" cellspacing="0" class="blue-tble">
              <tr>
                 <th>&nbsp;</th>
              </tr>
              </tr>
                 <td><strong>Discount Amount: </strong> $${parseFloat(amount.discount_amount).toFixed(2)}</td>
              </tr>
              </tr>
                 <td><strong>Amount Due: </strong> $${(amount.total_amount)?parseFloat(amount.total_amount.toFixed(2) - amount.discount_amount.toFixed(2)).toFixed(2):'0.00'}</td>
              </tr>
               <tr>
                  <td><strong>IOU Adjustment :</strong> $${
                     (data.provider.iou_paid_amount != null) ?
                        (parseFloat(data.provider.refund_due - data.provider.iou_paid_amount) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                           parseFloat(amount.total_amount - amount.discount_amount).toFixed(2)
                           :
                           (parseFloat(data.provider.refund_due - data.provider.iou_paid_amount)).toFixed(2)
                        : (data.provider.refund_due != null) ?
                           (parseFloat(data.provider.refund_due) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                              parseFloat(amount.total_amount - amount.discount_amount).toFixed(2)
                              :
                              (parseFloat(data.provider.refund_due)).toFixed(2)

                           :
                           '0.00'}</td>
               </tr>
               <tr>
                  <td><strong>Total amount due after Adjustment :</strong> $${
                     (data.provider.iou_paid_amount != null) ?
                        (parseFloat(data.provider.refund_due - data.provider.iou_paid_amount) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                           '0.00'
                           :
                           (parseFloat(amount.total_amount - amount.discount_amount) - parseFloat(data.provider.refund_due - data.provider.iou_paid_amount)).toFixed(2)
                        : (data.provider.refund_due != null) ?
                           (parseFloat(data.provider.refund_due) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                              '0.00'
                              :
                              (amount.total_amount)?
                              (parseFloat(amount.total_amount - amount.discount_amount) - parseFloat(data.provider.refund_due)).toFixed(2)
                              :
                              '0.00'

                           :
                           parseFloat(amount.total_amount - amount.discount_amount).toFixed(2)}</td>
               </tr>
               <tr>
                  <td><strong>Remaining IOU Balance Amount:</strong> $${
                     (data.provider.iou_paid_amount != null) ?
                        (parseFloat(data.provider.refund_due - data.provider.iou_paid_amount) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                        (parseFloat(data.provider.refund_due - data.provider.iou_paid_amount) - parseFloat(amount.total_amount - amount.discount_amount)).toFixed(2)
                           :
                           '0.00'
                        : (data.provider.refund_due != null) ?
                           (parseFloat(data.provider.refund_due) > parseFloat(amount.total_amount - amount.discount_amount)) ?
                              (parseFloat(data.provider.refund_due) - parseFloat(amount.total_amount - amount.discount_amount)).toFixed(2)
                              :
                              '0.00'

                           :
                           '0.00'}</td>
               </tr>
           </table>
        </div>
     </div>
     
     <div class="detail-bx">
        <h2 class="justify-center">Invoice Detail</h2>
        <div class="detail-table">
           <table cellpadding="0" cellspacing="0">
              <tr>
                 <th>Plan ID</th>
                 <th>A/C Number</th>
                 <th>Application No</th>
                 <th>First Name</th>
                 <th>Middle Name</th>
                 <th>Last Name</th>
                 <th>DOB</th>
                 <th>Phone</th>
                 <th>City</th>
                 <th>State</th>
                 <th>Loan Amount</th>
              </tr>
              ${html}
           </table>
        </div>
     </div>
     
        
      </div>
   </body>
</html>
    `;
};