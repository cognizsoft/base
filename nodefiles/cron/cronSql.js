var getInvoiceDetails = function (con, callback) {
    var obj = {};

    con.query('SELECT payment_plan.application_id, DATE_FORMAT(pp_installments.due_date, "%m") as months, DATE_FORMAT(pp_installments.due_date, "%Y") as years '
        + 'FROM pp_installments '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'WHERE pp_installments.due_date <= DATE_FORMAT(DATE_ADD(now(), INTERVAL 5 DAY), "%Y-%m-%d") AND pp_installments.due_date >= DATE_FORMAT(now(), "%Y-%m-%d") '
        , function (error, details) {
            if (error) {
                obj.status = 0;
                return callback(obj);
            } else {
                obj.status = 1;
                obj.details = details;
                return callback(obj);
            }
        })
}

exports.getInvoiceDetails = getInvoiceDetails;