/*
* Title: email sent
* Descrpation :- This module belong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
var emailInvoice = function (con) {
    let date = require('date-and-time');
    let now = new Date();
    var cron = require('./cron/cronSql.js');
    cron.getInvoiceDetails(con, function (result) {
        if (result.status == 1) {
            result.details.forEach(element => {
                var dataSql = require('./payment/payment.sql.js');
                dataSql.getSingleInstallment(con, element.application_id, element.months, element.years, function (result1) {
                    if (result1.status == 1) {
                    } else {

                        dataSql.getInvoicePlans(con, element.application_id, function (result2) {
                            if (result2.status == 1) {

                                var chk_late_fee_arr = result1.payment_billing_late_fee;

                                var invoice_date = date.format(now, 'MM/DD/YYYY');
                                var invoice_number = result1.single_installment_payment[0].invoice_number;

                                var full_name = result2.plan[0].f_name + ' ' + result2.plan[0].m_name + ' ' + result2.plan[0].l_name;
                                var address = result2.plan[0].address1 + ' ' + result2.plan[0].address2 + ' ' + result2.plan[0].city;
                                var phone = result2.plan[0].peimary_phone;

                                var ac_no = result2.plan[0].application_no;

                                
                                var pymnt_due_date = result1.single_installment_payment[0].due_date;

                                var amount_due = 0;
                                //var amount_due_length = result1.single_installment_payment.length
                                if (result1.single_installment_payment) {

                                    result1.single_installment_payment.forEach(function (data) {
                                        if (data.installment_amt) {
                                            amount_due += data.installment_amt;
                                        }
                                    })

                                }


                                var late_fee = result1.payment_billing_late_fee[0].late_fee;

                                if (result2.plan) {
                                    var loan_plan_info = result2.plan.reduce(function (r, a) {
                                        r[a.pp_id] = r[a.pp_id] || [];

                                        r[a.pp_id].push(a);
                                        return r;

                                    }, []);
                                }

                                var loan_plan_info_arr = [];
                                if (loan_plan_info) {
                                    loan_plan_info.forEach(function (data) {
                                        if (data) {
                                            loan_plan_info_arr.push(data)
                                        }
                                    })
                                }


                                var loan_amount = result2.plan[0].credit_amount;

                                var payment_term = result2.plan[0].payment_term;

                                var interest_rate = result2.plan[0].interest_rate;

                                var fincharge = 0;
                                var totalEnclosed_amt = 0;
                                if (result1.single_installment_payment) {

                                    result1.single_installment_payment.forEach(function (data) {

                                        fincharge += ((data.installment_amt / data.payment_term) * data.interest_rate) / 100;

                                        totalEnclosed_amt += parseFloat(data.installment_amt)

                                    })

                                }

                                var total_amt_due = parseFloat(amount_due) + parseFloat(fincharge) + parseFloat(late_fee);

                                var last_payment_date = result2.last_payment_info[0].payment_date;

                                var last_mnthly_pymnt = result2.last_payment_info[0].payment_amount;

                                var last_addi_amount = result2.last_payment_info[0].additional_amount;

                                var last_late_fee = result2.last_payment_info[0].late_fee_received;

                                var last_fin_ch = result2.last_payment_info[0].fin_charge_amt;

                                var last_total_amount = last_mnthly_pymnt + last_addi_amount + last_late_fee + last_fin_ch;

                                

                                var emialTemp = require('./email.js');
                                var locals = {
                                    invoice_date: invoice_date,
                                    invoice_number: invoice_number,
                                    ac_no: ac_no,
                                    pymnt_due_date: pymnt_due_date,
                                    amount_due: parseFloat(amount_due).toFixed(2),
                                    late_fee_after: parseFloat(late_fee).toFixed(2),
                                    full_name: full_name,
                                    address: address,
                                    phone: phone,

                                    regu_mnthly_pymnt: parseFloat(amount_due).toFixed(2),
                                    ex_late_fee: parseFloat(late_fee).toFixed(2),
                                    ex_fin_charge: parseFloat(fincharge).toFixed(2),
                                    total_amt_due: parseFloat(total_amt_due).toFixed(2),

                                    last_reg_mnthly_pymnt: parseFloat(last_mnthly_pymnt).toFixed(2),
                                    last_payment_date: last_payment_date,
                                    last_addi_amount: parseFloat(last_addi_amount).toFixed(2),
                                    last_late_fee: parseFloat(last_late_fee).toFixed(2),
                                    last_fin_ch: parseFloat(last_fin_ch).toFixed(2),
                                    last_total_amount: parseFloat(last_total_amount).toFixed(2),

                                    loan_plan_info: loan_plan_info_arr
                                };

                                var toEmailAddress = result2.plan[0].email;
                                var template = 'customer-invoice';
                                emialTemp.emailTemplate(toEmailAddress, locals, template);
                            } 

                        });
                    }
                });
            });
        }

    });
};

/**
 * Miss payment
 */
var missInvoice = function (con) {
    con = require('../db');
    var emialTemp = require('./emailplan.js');
    var template = 'Payment missed';
    emialTemp.customEmailTemplateDetailsCron(con, template, function (tmpResult) {
        if (tmpResult.status == 1) {
            con.query('SELECT due_date, MONTHNAME(due_date) as month_name, patient.f_name,patient.m_name,patient.l_name,patient.email FROM patient_invoice '
                + 'INNER JOIN credit_applications ON credit_applications.application_id = patient_invoice.application_id '
                + 'INNER JOIN patient ON patient.patient_id = credit_applications.patient_id '
                + 'WHERE invoice_status = 3 AND MONTH(patient_invoice.due_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) AND YEAR(patient_invoice.due_date) = YEAR(CURDATE())'
                //+ 'WHERE due_date<=NOW() ORDER BY pp_id'
                , function (missErr, result, fields) {
                    if (!missErr && Object.keys(result).length > 0) {
                        for (const customerDetails of result) {
                            var emailOpt = {
                                toEmail: customerDetails.email,
                                subject: tmpResult.result[0].template_subject,
                            }
                            var context = {
                                customer_name: customerDetails.f_name + ' ' + customerDetails.m_name + ' ' + customerDetails.l_name,
                                invoice_month : customerDetails.month_name,                                
                            }

                            var html = tmpResult.result[0].template_content;
                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                            })
                        }
                    }
                })
        }
    })

}

exports.emailInvoice = emailInvoice;
exports.missInvoice = missInvoice;
