var timeScheduler = async function (cron) {
    con = require('../db');
    con.query('SELECT value FROM master_data_values WHERE status = ? AND md_id = ?'
        , [1, 'Invoice cron grace period']
        , function (errCron, cronTimer, fields) {

            if (errCron || Object.keys(cronTimer).length == 0) {
                cron.schedule('* * 5 * *', () => {
                    var cronJobDuePayment = require('./duePayemnt.js');
                    //cronJobDuePayment.deuPayment();
                });
            } else {
                console.log(cronTimer[0].value)
                cron.schedule('*/'+cronTimer[0].value+' * * * *', () => {
                    //console.log('call miss cron')
                  var cronJobMissPayment = require('./missPayemnt.js');
                  //cronJobMissPayment.missPayment();
                });
                cron.schedule('0 49 3 '+cronTimer[0].value+' * *', async () => {
                    var cronJobDuePayment = require('./duePayemnt.js');
                    cronJobDuePayment.deuPayment();
                });
                
                
            }
        })
}

exports.timeScheduler = timeScheduler;