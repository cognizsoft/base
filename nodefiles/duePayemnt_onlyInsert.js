/*
* Title: due payment
* Descrpation :- This module belong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
var deuPayment = function () {
    con = require('../db');
    var obj = {};
    con.query('SELECT credit_applications.patient_id,payment_plan.provider_id,pp_installments.pp_id,pp_installments.pp_installment_id,pp_installments.installment_amt,DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
        + 'master_data_values.value AS payment_term_month,payment_plan.discounted_interest_rate,'
        + '(SELECT sum(invoice_installments.amount_rcvd) FROM invoice_installments WHERE invoice_installments.pp_installment_id=pp_installments.pp_installment_id) as amount_rcvd, '
        //+ '(SELECT IF(paid_flag=1, 0, (previous_blc+late_fee_received+fin_charge_amt)) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldBlc '
        + '(SELECT IF(paid_flag=1, 0, (previous_late_fee+late_fee_received)) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldLateFee, '
        + '(SELECT IF(paid_flag=1, 0, (previous_fin_charge+fin_charge_amt)) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldFinCharge '
        + 'FROM pp_installments '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        + 'INNER JOIN interest_rate ON payment_plan.interest_rate_id = interest_rate.id '
        + 'INNER JOIN master_data_values ON master_data_values.mdv_id = interest_rate.mdv_payment_term_month '
        + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
        //+ 'WHERE MONTH(pp_installments.due_date) <= MONTH(CURRENT_DATE()) AND YEAR(pp_installments.due_date) <= YEAR(CURRENT_DATE()) '
        + 'WHERE pp_installments.due_date <= LAST_DAY(CURRENT_DATE()) '
        + 'AND pp_installments.pp_installment_id NOT IN (SELECT invoice_installments.pp_installment_id FROM invoice_installments INNER JOIN pp_installments ON pp_installments.pp_installment_id=invoice_installments.pp_installment_id WHERE MONTH(invoice_installments.date_created)=MONTH(CURRENT_DATE()) AND pp_installments.paid_flag=0) '
        + 'AND (pp_installments.paid_flag =0 OR pp_installments.partial_paid=1) ORDER by payment_plan.provider_id'
        , function (error, result, fields) {
            //console.log(this.sql)
            if (error || Object.keys(result).length == 0) {
            } else {
                con.query('SELECT '
                    + 'value,md_id '
                    + 'FROM master_data_values '
                    + 'WHERE status = ? AND (md_id = ? OR md_id = ?)'
                    , [1, 'Late Fee', 'Financial Charges']
                    , function (errLate, late_fee_rows, fields) {
                        //console.log(late_fee_rows)
                        if (errLate) { } else {
                            var lateFee = 0;
                            var finCharge = 0;
                            if (late_fee_rows.length > 0) {
                                var late = late_fee_rows.filter(function (item) {
                                    return item.md_id == 'Late Fee';
                                });
                                lateFee = (late.length > 0) ? late[0].value : 0;
                                var financial = late_fee_rows.filter(function (item) {
                                    return item.md_id == 'Financial Charges';
                                });
                                finCharge = (financial.length > 0) ? financial[0].value : 0;

                            }
                            //var lateFee = (late_fee_rows.length>0)?late_fee_rows[0].late_fee:0;
                            var checkLate = 0;
                            var allDetails = result && result.reduce(function (accumulator, currentValue, currentindex) {
                                //console.log(currentValue.due_date)
                                //console.log('--------')
                                if (!accumulator[currentValue.patient_id]) {
                                    if (new Date() > new Date(currentValue.due_date)) {
                                        //var finCharge = ((currentValue.installment_amt / currentValue.payment_term_month) * currentValue.discounted_interest_rate) / 100;
                                        checkLate=1;
                                        var prevBlc = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    } else {
                                        lateFee = (checkLate == 1) ? lateFee : 0;
                                        finCharge = (checkLate == 1) ? finCharge : 0;
                                        var prevBlc = 0;
                                    }
                                    if (new Date() < new Date(currentValue.due_date)) {
                                        if (new Date() > new Date(currentValue.due_date)) {
                                            finCharge = 0;
                                            lateFee = 0;
                                        }
                                    }
                                    var newAmt = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    accumulator[currentValue.patient_id] = {
                                        patient_id: currentValue.patient_id,
                                        totalAmt: newAmt,
                                        finCharge: finCharge,
                                        lateFee: lateFee,
                                        //oldBlc: currentValue.oldBlc,
                                        oldLateFee: currentValue.oldLateFee, 
                                        oldFinCharge: currentValue.oldFinCharge,
                                        prevBlc:prevBlc,
                                    };
                                } else {
                                    if (new Date() > new Date(currentValue.due_date)) {
                                        checkLate = 1;
                                        var prevBlc = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    } else {
                                        lateFee = (checkLate == 1) ? lateFee : 0;
                                        finCharge = (checkLate == 1) ? finCharge : 0;
                                        var prevBlc = 0;
                                    }
                                    
                                    var newAmt = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    accumulator[currentValue.patient_id].totalAmt += newAmt;
                                    accumulator[currentValue.patient_id].finCharge = finCharge;
                                    accumulator[currentValue.patient_id].lateFee = lateFee;
                                    //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
                                    accumulator[currentValue.patient_id].oldLateFee = currentValue.oldLateFee;
                                    accumulator[currentValue.patient_id].oldFinCharge = currentValue.oldFinCharge;
                                    accumulator[currentValue.patient_id].prevBlc += prevBlc;

                                }
                                return accumulator;
                            }, []);
                            var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
                                return allDetails[patient_id];
                            })
                            
                            con.beginTransaction(function (err) {
                                if (err) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    return callback(obj);
                                }


                                patientGroup && patientGroup.map((data, idx) => {
                                    mainTableDate(con, data.patient_id, data.totalAmt, data.prevBlc, data.finCharge, data.lateFee, data.oldLateFee, data.oldFinCharge, result, function (mainResponce) {
                                        //console.log(mainResponce);
                                    });
                                })

                                con.commit(function (err) {
                                    if (err) {
                                        //console.log('error')
                                    }
                                    //console.log('ok')
                                });
                            })
                        }
                    })
            }



        });
};


var mainTableDate = function (con, key, value, prevBlc, finCharge, lateFee, oldLateFee, oldFinCharge, result, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('INSERT INTO patient_invoice (invoice_number, patient_id, payment_amount, previous_blc, previous_late_fee, previous_fin_charge, late_fee_received, fin_charge_amt, due_date, paid_flag, created_by, modified_by, date_created, date_modified) '
        + 'VALUES(((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice as m)),?,?,?,?,?,?,?,LAST_DAY(now()),?,?,?,?,?)',
        [key, value, prevBlc, oldLateFee, oldFinCharge, lateFee, finCharge, 3, 1, 1, current_date, current_date]
        , function (error, resultMain, fields) {
            //console.log(error)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                var resultFilter = result.filter((r) => r.patient_id == key);

                resultFilter.forEach(function (element, idx) {

                    childTableDate(con, resultMain.insertId, element, function (childResponce) {
                        //console.log(childResponce);
                    });
                })
                obj.status = 1;
                return callback(obj);
            }
        });

}

var childTableDate = function (con, insertId, element, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('INSERT INTO invoice_installments (invoice_id, pp_id, pp_installment_id, addi_amt_invoice_flag, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?)',
        [insertId, element.pp_id, element.pp_installment_id, 1, current_date, current_date, 1, 1]
        , function (err, resultPPIns) {
            if (err) {
                con.rollback(function () {
                    obj.status = 3;
                    return callback(obj);
                });
            } else {
                obj.status = 4;
                return callback(obj);
            }
        })

}


exports.deuPayment = deuPayment;


