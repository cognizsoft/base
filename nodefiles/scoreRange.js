/*
* Title: masterValue
* Descrpation :- This module blong to user type all application
* Date :-  10 Feb 2019
* Author :- Aman
*/
module.exports = function(app,jwtMW){
  let date = require('date-and-time');
  

  /////////////////////////////
  /////GET MASTER VALUE/////
  ////////////////////////////

  app.get('/api/score-range/', jwtMW,(req,res) => {
    con = require('../db');
    con.query('SELECT '
    	+'id, '
    	+'amount, '
    	+'min_score, '
    	+'max_score, '
    	+'status '
    	+'FROM score_range WHERE deleted_flag = 0 ORDER BY id DESC', function (error,rows, fields){
      if(error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  /////////////////////////////
  /////UPDATE PAYBACK/////
  ////////////////////////////

  app.post('/api/score-range-update/', jwtMW,(req,res) => {
    con = require('../db');  
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let id = req.body.id;
    let amount = req.body.amount;
    let min_score = req.body.min_score;
    let max_score = req.body.max_score;
    let status = req.body.status;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE score_range SET amount=?, min_score=?, max_score=?, status=?, modified_at=?, modified_by=? WHERE id=?',

         [
            
            req.body.amount, req.body.min_score, req.body.max_score, req.body.status, current_date, current_user_id, req.body.id
            
          ],

         function (err,result){
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })


  /////////////////////////////
  /////INSERT PAYBACK/////
  ////////////////////////////

  app.post('/api/score-range-insert/', jwtMW,(req,res) => {
    con = require('../db');
    let current_user_id = req.body.current_user_id;
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
     con.query('INSERT INTO score_range (amount, min_score, max_score, status, created_by, modified_by, created_at, modified_at) VALUES(?,?,?,?,?,?,?,?)',
          [
            
            req.body.amount, req.body.min_score, req.body.max_score, req.body.status, current_user_id, current_user_id, current_date, current_date
            
          ], function (err,result){
          
          if(err) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
           "status": 1,
           "last_value_id": result.insertId,
           "amount": req.body.amount,
           "min_score": req.body.min_score,
           "max_score": req.body.max_score,
           "score_status": req.body.status
          }
          res.send(obj);
           
          }
     })
  })

  
  

    //other routes..
}
