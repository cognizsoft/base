module.exports = (locals, logoimg) => {
   let moment = require('moment');
   //console.log('loan_plan_info_arr')
   //console.log(locals.plan.monthlyPlan)
   //return false;

   var html = '';
   locals.plan.monthlyPlan.forEach(function (val, idx) {
      html = html + '<tr>'
         + '<td>' + (parseInt(idx)+1) + '</td>'
         + '<td>' + val.next_month + '</td>'
         + '<td>' + '$'+parseFloat(val.perMonth).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          	body{font-size: 14px; line-height: 20px; font-family: 'Arial', sans-serif; color: #303030;}
			.invoice-box {
			  max-width: 1200px;
			  margin: auto;
			  padding: 0px;
			  font-size: 15px;
			  line-height: 20px;
			  font-family: 'Arial', sans-serif;
			  color: #303030;
			  }
			  .margin-top {
			  margin-top: 50px;
			  }
			  .justify-center {
			  text-align: center;
			  }
			  .invoice-box table {
			  width: 100%;
			  line-height: inherit;
			  text-align: left;
			border:solid 1px #ddf1ff; border-collapse:collapse;}
			.invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
			.customer-info{width:100%;}
			.customer-info tr th,.customer-info tr td{width:33.33333333333333%;}
			.info-bx{justify-content:space-between !important}
			.blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:12px;}
			.blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:12px;}
			.invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
			.info-bx:after{margin-bottom:20px;}
			.detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:12px; line-height:22px;}
			.detail-table td{padding:5px 2px; font-size:12px;}
        </style>
     </head>
    
    <body>

	<div class="invoice-box">
		<div class="header-row">
	   		<h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
		<div>
		<span>Hey <strong>${locals.name},</strong><br><br> 
	</div>

	<div class="info-bx">
		<div class="customer-info">
			<table cellpadding="0" cellspacing="0" class="blue-tble">
			<tr>
			<th colspan="3">Basic Information</th>
			</tr>
			<tr>
			<td><strong>Credit Score: </strong> ${locals.bodydata.score}</td>
			<td><strong>Loan Amount: </strong> $${parseFloat(locals.bodydata.amount).toFixed(2)}</td>
			<td><strong>Provider Name: </strong> ${locals.providerDetails.name}</td>
			</tr>
			</table>
		</div>

	<div class="clearfix">
		<div class="loan-info invoice-bx">
		<h2 class="justify-center">Plan Details</h2>
			<table class="detail-table">
				<tr>
				<th>SN#</th>
				<th>Date</th>
				<th>Amount</th>
				</tr>
				${html}
			</table>
		</div>
	</div>

	<div>
		<span><br/>If you have any questions, you can just hit reply to contact our Health Partner support team. </span><br/><br/><br/>
	</div>

	<span>Best</span><br/>
	<span>Support team</span>

</body>

  </html>
      `;
};