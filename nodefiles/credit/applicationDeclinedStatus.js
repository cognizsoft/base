module.exports = (data, logoimg) => {
   
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body {
            font-size: 16px;
          }
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0px;
           font-size: 8px;
           line-height: 18px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           .justify-center {
           text-align: center !important;
           width: 100% !important;
           }
           
           
           
        </style>
     </head>
     <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>

       <h1 class="justify-center mb-20">LINE OF CREDIT DECLINED</h1>
         <div className="terms-condition-agreement">
           <div><strong>Name:</strong>${data.application.f_name + ' ' + data.application.m_name + ' ' + data.application.l_name}</div>
           <div>
           <strong>Address:</strong>${data.application.address1 + ' ' + data.application.address2}
           <br>
           ${data.application.City + ', ' + data.application.name + ' - ' + data.application.zip_code}
           </div>
           <div><strong>Phone:</strong>${data.application.phone_no}</div>
           <hr>
           <div>We want to thank you applying for line of created. Based on the information provided, your application was evaluated and reviewed based on many criteria including your credit history.</div>
           <div>We regret to inform you that we are unable to extend line of created at this time. We look forward to serve you in the future.</div>
           <div>If you have any additional question, our friendly customer service associate are eager to help.</div>

           <br>
           <div>Thanks, you very much for considering Health partner for your medical needs.</div>
           <br>
           <br>
           <br>
           <div>Sincerely</div>
           <div>Health Partner Inc.</div>       

        </div>
     </body>
  </html>
      `;
};