module.exports = (data, logoimg) => {
   let moment = require('moment');
   //console.log(data)
   //return false;
   let settlement_html = '';

   var principal_amt = (data.sttlement_detail[0].loan_amount > 0) ? '<tr><td className=""><strong>Principal Amount :</strong></td><td>$' + data.sttlement_detail[0].loan_amount.toFixed(2) +  '</td></tr>' : '';
   var adjusted_amount = (data.sttlement_detail[0].adjusted_amount) ? '<tr><td className=""><strong>Adjusted Amount :</strong></td><td>$' + data.sttlement_detail[0].adjusted_amount.toFixed(2) + '</td></tr>' : '';
   var interest_rate = (data.sttlement_detail[0].discounted_interest_rate) ? '<tr><td className=""><strong>Interest Rate (%) :</strong></td><td>' + data.sttlement_detail[0].discounted_interest_rate + '%</td></tr>' : '';
   var term = (data.sttlement_detail[0].term) ? '<tr><td className=""><strong>Term (month) :</strong></td><td>' + data.sttlement_detail[0].term + '</td></tr>' : '';
   var plan_number = (data.sttlement_detail[0].plan_number) ? '<tr><td className=""><strong>Current Plan :</strong></td><td>' + data.sttlement_detail[0].plan_number + '</td></tr>' : '';
   var plan_ref_no = (data.sttlement_detail[0].plan_ref_no) ? '<tr><td className=""><strong>Reference Plan :</strong> </td><td>' + data.sttlement_detail[0].plan_ref_no + '</td></tr>' : '';
   var note = (data.sttlement_detail[0].note) ? '<tr><td className=""><strong>Note :</strong></td><td>' + data.sttlement_detail[0].note + '</td></tr>' : '';
   settlement_html = settlement_html 
      + principal_amt
      + adjusted_amount
      + interest_rate
      + term
      + plan_number
      + plan_ref_no
      + note
      
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body {
            font-size: 16px;
          }
           
           .margin-top {
           margin-top: 50px;
           }
           .note {
            color: red;
           }
           .justify-center {
           text-align: center;
           width: 100%;
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .info{width: 48%; float: left; margin:0.5%}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .detail-table td{padding:8px 15px; font-size:10px;}
          span.agreement_date, .id_number, .id_exp_date {
              margin-left: 35px;
          }
         .info-bx{
            display: flex !important;
            width: 100%;
            justify-content: space-between;
            flex-wrap: wrap;
          }
          .info{background: #f1f9ff;}
          .invoice-box h4 {line-height: 5px; margin-bottom: 0;}
           .term-heading, .customer-heading {text-align: center; margin-top: 30px; font-size:20px}
          .hidden{visibility:hidden;}
          .customer-accnt .add-card{
            width: 32%;
            border: solid 1px #d7eeff !important;
            background: #f1f9ff;
            
          }
          .customer-accnt .witness-signature{
            float:right;
          }
          .customer-accnt .customer-signature{
            float:left;
          }
          .customer-accnt .image-area{
            margin-top: 10px;
          }
          .sing_box{
            padding: 10px 10px 10px 10px !important;         
          }
          .settlement-info {
            width:97%;
          }
          .settlement-info table {
            width:100%;
          }
        </style>
     </head>
     <body>
        <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
        <h1 class="justify-center">Settlement Plan Details</h1>
        <div class="info-bx invoice-box">
          
          <div class="info customer-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Customer Details</th>
                </tr>

                <tr>
                  <td className=""><strong>Account No :</strong></td>
                  <td>${data.customer_detail[0].patient_ac}</td>
                </tr>

                <tr>
                  <td className=""><strong>Name :</strong></td>
                  <td>${data.customer_detail[0].f_name +', '+ data.customer_detail[0].m_name +', '+ data.customer_detail[0].m_name}</td>
                </tr>

                <tr>
                  <td className=""><strong>Address :</strong></td>
                  <td>${data.customer_detail[0].address1 +', '+ data.customer_detail[0].City +', '+ data.customer_detail[0].state_name +', '+ data.customer_detail[0].zip_code}</td>
                </tr>

                <tr>
                  <td className=""><strong>Phone :</strong></td>
                  <td>${data.customer_detail[0].peimary_phone}</td>
                </tr>

             </table>
          </div>

          <div class="info provider-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Provider Details</th>
                </tr>

                <tr>
                  <td className=""><strong>Account No :</strong></td>
                  <td>${data.provider_detail[0].provider_ac}</td>
                </tr>

                <tr>
                  <td className=""><strong>Name :</strong></td>
                  <td>${data.provider_detail[0].name}</td>
                </tr>

                <tr>
                  <td className=""><strong>Address :</strong></td>
                  <td>${data.provider_detail[0].address1 +', '+ data.provider_detail[0].city +', '+ data.provider_detail[0].state_name +', '+ data.provider_detail[0].zip_code}</td>
                </tr>

                <tr>
                  <td className=""><strong>Phone :</strong></td>
                  <td>${data.provider_detail[0].primary_phone}</td>
                </tr>

             </table>
          </div>

          <div class="info settlement-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Settlement Details</th>
               </tr>

               ${settlement_html}

             </table>
          </div>

        </div>

     </body>
  </html>
      `;
};