var request = require('request');
/*const BASE_URL = 'https://sandbox-us-api.experian.com';
const CLIENT_ID = 'pfQCUobkuPMXFGnrXTkmmTs6E03WnIF1';
const CLIENT_SECRET = 'CzfSfcpAV8nOuh4L';*/
const BASE_URL = 'https://uat-us-api.experian.com';
//const BASE_URL = 'https://us-api.experian.com';
const CLIENT_ID = 'ZLagwXlNOYONFAHoxhoASqgjrxKVTuiW';
const CLIENT_SECRET = 'mMYbg1EKKZX8JmKr';

const USERNAME = 'ramesh@cognizsoft.com';
const PASSWORD = 'Cogniz@123';
const DEFAULT_TIMEOUT = require('http').createServer().timeout;
var experianLogin = async function (data, logindetails, callback) {

    var obj = {};
    return new Promise((resolve, reject) => {
        request({
            rejectUnauthorized: false,
            method: 'POST',
            baseUrl: BASE_URL,
            json: true,
            uri: '/oauth2/v1/token',
            headers: {
                'client_id': logindetails[0].api_key,
                'client_secret': logindetails[0].secret_key,
                'Content-Type': 'application/json',
                'grant_type': 'password'
            },
            body: {
                'username': logindetails[0].username,
                'password': logindetails[0].password
            }

        }, function (error, response, body) {

            if (error) {
                obj.status = 0;
                callback(obj);
            } else if (response.statusCode === 200) {
                var accessToken = body.access_token;
                var refreshToken = body.refresh_token;
                if (accessToken) {
                    obj.status = 1;
                    obj.accessToken = accessToken;
                    obj.refreshToken = refreshToken;
                    callback(obj);
                } else {
                    obj.status = 0;
                    obj.message = "Invaild details";
                    callback(obj);
                }
            } else {
                obj.status = 1;
                obj.message = "Invaild details";
                callback(obj);
            }
            resolve();
        });
    })
}
var experianApi = function (con, subscriberCode, token, data, coSigner, callback) {
    var obj = {};
    return new Promise((resolve, reject) => {
        
        let date = require('date-and-time');
        let now = (coSigner == 1) ? new Date(data.co_dob) :  new Date(data.dob);
        let dob_date = date.format(now, 'MMDDYYYY');
        if (coSigner == 1) {
            const filterType = data.co_location.filter(x => x.co_primary_address == 1);
            getStateAB(con, filterType[0].co_state, function (abbreviation) {

                var cityname = (filterType[0].co_City === undefined) ? filterType[0].co_city : filterType[0].co_City;

                if (abbreviation.status == 1) {

                    request({
                        rejectUnauthorized: false,
                        method: 'POST',
                        baseUrl: BASE_URL,
                        timeout: DEFAULT_TIMEOUT,
                        json: true,
                        uri: '/consumerservices/credit-profile/v2/credit-report',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + token.accessToken
                        },
                        body: {
                            "consumerPii": {
                                "primaryApplicant": {
                                    "name": {
                                        "lastName": data.co_last_name.trim(),
                                        "firstName": data.co_first_name.trim(),
                                    },
                                    "dob": {
                                        "dob": dob_date.trim()
                                    },
                                    "ssn": {
                                        "ssn": data.co_ssn.trim().replace(/-/gi, "")
                                    },
                                    "currentAddress": {
                                        "line1": filterType[0].co_address1.trim(),
                                        "line2": filterType[0].co_address2.trim(),
                                        "city": cityname.trim(),
                                        "state": abbreviation.abbr.trim(),
                                        "zipCode": filterType[0].co_zip_code.trim()
                                    },

                                },
                            },
                            "requestor": {
                                "subscriberCode": subscriberCode.trim()
                            },
                            "addOns": {
                                "riskModels": {
                                    "modelIndicator": [
                                        "V4"
                                    ],
                                    "scorePercentile": "Y"
                                },
                                "mla": "",
                                "ofacmsg": "Y",
                                "fraudShield": "N",
                                "paymentHistory84": "N"
                            },
                            "freezeOverride": {
                                "primaryApplFreezeOverrideCode": "",
                            },
                            /*"lastName": data.first_name,
                            "firstName": data.last_name,
                            "dob": data.dob,
                            "ssn": data.ssn.replace(/-/gi, ""),*/
                        }
                    }, function (error, response, body) {

                        if (error) {
                            obj.status = 0;
                            obj.message = "You are not access to customer score.";
                            callback(obj);
                        } else if (response.statusCode === 200) {

                            if (body.creditProfile[0].riskModel !== undefined) {
                                obj.status = 1;
                                obj.details = body;
                                callback(obj);
                            } else {
                                obj.status = 2;
                                obj.message = "Record not found on experian.";
                                callback(obj);
                            }

                        } else {
                            obj.status = 2;
                            var messagelist = '';
                            body.errors.forEach(function (element, idx) {

                                messagelist = messagelist + ' ' + element.message + ', ';
                            })

                            obj.message = messagelist; //"You are not access to customer score.";
                            callback(obj);
                        }
                        resolve()
                    });
                } else {
                    obj.status = 2;
                    obj.message = "State abbreviation not found. Please try again late.";
                    callback(obj);
                    resolve()
                }
            })
        } else {
            const filterType = data.location.filter(x => x.primary_address == 1);
            getStateAB(con, filterType[0].state, function (abbreviation) {

                var cityname = (filterType[0].City === undefined) ? filterType[0].city : filterType[0].City;

                if (abbreviation.status == 1) {

                    request({
                        rejectUnauthorized: false,
                        method: 'POST',
                        baseUrl: BASE_URL,
                        timeout: DEFAULT_TIMEOUT,
                        json: true,
                        uri: '/consumerservices/credit-profile/v2/credit-report',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + token.accessToken
                        },
                        body: {
                            "consumerPii": {
                                "primaryApplicant": {
                                    "name": {
                                        "lastName": data.last_name.trim(),
                                        "firstName": data.first_name.trim(),
                                    },
                                    "dob": {
                                        "dob": dob_date.trim()
                                    },
                                    "ssn": {
                                        "ssn": data.ssn.trim().replace(/-/gi, "")
                                    },
                                    "currentAddress": {
                                        "line1": filterType[0].address1.trim(),
                                        "line2": filterType[0].address2.trim(),
                                        "city": cityname.trim(),
                                        "state": abbreviation.abbr.trim(),
                                        "zipCode": filterType[0].zip_code.trim()
                                    },

                                },
                            },
                            "requestor": {
                                "subscriberCode": subscriberCode.trim()
                            },
                            "addOns": {
                                "riskModels": {
                                    "modelIndicator": [
                                        "V4"
                                    ],
                                    "scorePercentile": "Y"
                                },
                                "mla": "",
                                "ofacmsg": "Y",
                                "fraudShield": "N",
                                "paymentHistory84": "N"
                            },
                            "freezeOverride": {
                                "primaryApplFreezeOverrideCode": "",
                            },
                            /*"lastName": data.first_name,
                            "firstName": data.last_name,
                            "dob": data.dob,
                            "ssn": data.ssn.replace(/-/gi, ""),*/
                        }
                    }, function (error, response, body) {

                        if (error) {
                            obj.status = 0;
                            obj.message = "You are not access to customer score.";
                            callback(obj);
                        } else if (response.statusCode === 200) {

                            if (body.creditProfile[0].riskModel !== undefined) {
                                obj.status = 1;
                                obj.details = body;
                                callback(obj);
                            } else {
                                obj.status = 2;
                                obj.message = "Record not found on experian.";
                                callback(obj);
                            }

                        } else {
                            obj.status = 2;
                            var messagelist = '';
                            body.errors.forEach(function (element, idx) {

                                messagelist = messagelist + ' ' + element.message + ', ';
                            })

                            obj.message = messagelist; //"You are not access to customer score.";
                            callback(obj);
                        }
                        resolve()
                    });
                } else {
                    obj.status = 2;
                    obj.message = "State abbreviation not found. Please try again late.";
                    callback(obj);
                    resolve()
                }
            })
        }
    })

}

var getStateAB = function (con, id, callback) {
    con.query('SELECT abbreviation FROM states WHERE state_id=?',
        [
            id
        ]
        , function (error, result) {
            if (error) {
                var obj = {
                    "status": 0,
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "abbr": (result.length > 0) ? result[0].abbreviation : ''
                }
                return callback(obj);
            }
        })
}

exports.experianLogin = experianLogin;
exports.experianApi = experianApi;
