module.exports = (appDetails, planDetails, data) => {
   //console.log(appDetails);
   //console.log(planDetails[0].monthlyPlan);
   //console.log(data);

   //return false;
   var html = '';
   planDetails[0].monthlyPlan.forEach(function (element, idx) {
      html = html + '<tr>'
         + '<td>' + (idx + 1) + '</td>'
         + '<td>' + element.next_month + '</td>'
         + '<td>$' + parseFloat(element.perMonth).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
 <!doctype html>
 <html>
    <head>
       <meta charset="utf-8">
       <title>Customer Payment Plan</title>
       <style>
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 10px;
          line-height: 24px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:12px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:12px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:22px;}
       .detail-table td{padding:5px 2px; font-size:10px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
      <h1 class="justify-center">Customer Payment Plan</h1>
      <div class="info-bx">
         <div class="provider-info">
            <table cellpadding="0" cellspacing="0" class="blue-tble">
               <tr>
                  <th>Customer Information</th>
               </tr>
               <tr>
                  <td><strong>Account No: </strong> ${appDetails[0].patient_ac}</td>
               </tr>
               <tr>
                  <td><strong>Application No: </strong> ${appDetails[0].application_no}</td>
               </tr>
               <tr>
                  <td><strong>Name: </strong> ${appDetails[0].f_name+' '+appDetails[0].m_name+' '+appDetails[0].l_name}</td>
               </tr>
               <tr>
                  <td><strong>Address: </strong> ${appDetails[0].address1+' '+appDetails[0].address2+', '+appDetails[0].City+', '+appDetails[0].name+'-'+appDetails[0].zip_code}</td>
               </tr>
               <tr>
                  <td><strong>Phone: </strong> ${appDetails[0].phone_no}</td>
               </tr>
               <tr>
                  <td><strong>Credit Score: </strong> ${appDetails[0].score}</td>
               </tr>
                          
            </table>
         </div>
      
         <div class="invoice-info">
            <table cellpadding="0" cellspacing="0" class="blue-tble">
               <tr>
                  <th>Loan Information</th>
               </tr>
               <tr>
                  <td><strong>Approved Amount: </strong> $${parseFloat(appDetails[0].approve_amount).toFixed(2)}</td>
               </tr>
               <tr>
                  <td><strong>Principal Amount: </strong> $${parseFloat(appDetails[0].amount).toFixed(2)}</td>
               </tr>
               <tr>
                  <td><strong>Remaining Amount: </strong> $${parseFloat(appDetails[0].remaining_amount - appDetails[0].amount).toFixed(2)}</td>
               </tr>
               <tr>
                  <td><strong>Loan Amount: </strong> $${parseFloat(planDetails[0].totalAmount).toFixed(2)}</td>
               </tr>
               <tr>
                  <td><strong>Interst Rate: </strong> ${parseFloat(planDetails[0].interest_rate).toFixed(2)}%</td>
               </tr>
               <tr>
                  <td><strong>Term Month: </strong> ${planDetails[0].term_month}</td>
               </tr>
            </table>
         </div>
      </div>
      
      <div class="detail-bx">
         <h2 class="justify-center">Plan Details</h2>
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>SN#</th>
                  <th>Date</th>
                  <th>Amount</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>         
       </div>
    </body>
 </html>
     `;
};