module.exports = (planDetails, data, providerDetails, logoimg) => {
   console.log(data);
   //console.log(planDetails[0].monthlyPlan);
   //console.log(data);

   //return false;
   var html = '';
   planDetails[0].monthlyPlan.forEach(function (element, idx) {
      html = html + '<tr>'
         + '<td>' + (idx + 1) + '</td>'
         + '<td>' + element.next_month + '</td>'
         + '<td>$' + parseFloat(element.perMonth).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
            html{zoom: 0.7;}
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0px;
           font-size: 8px;
           line-height: 18px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           .margin-top {
           margin-top: 50px;
           }
           .justify-center {
           text-align: center;
           width: 100%;
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .provider-info{width:100%;}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:22px;}
        .detail-table td{padding:5px 2px; font-size:10px;}
           
           
        </style>
     </head>
     <body>
        <div class="invoice-box">
        <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
       <h1 class="justify-center">Customer Payment Plan</h1>
       <div class="info-bx">
          <div class="provider-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                   <th colspan="4">Basic Information</th>
                </tr>
                <tr>
                   <td><strong>Credit Score: </strong> ${data.score}</td>
                   <td><strong>Loan Amount: </strong> $${parseFloat(data.amount).toFixed(2)}</td>
                   <td><strong>Provider Name: </strong> ${providerDetails.name}</td>
                   <td><strong>Provider Discount: </strong> $${parseFloat(planDetails[0].discount_p).toFixed(2)}</td>
                </tr>
             </table>
          </div>
       
       </div>
       
       <div class="detail-bx">
          <h2 class="justify-center">Plan Details</h2>
          <div class="detail-table">
             <table cellpadding="0" cellspacing="0">
                <tr>
                   <th>SN#</th>
                   <th>Date</th>
                   <th>Amount</th>
                </tr>
                ${html}
             </table>
          </div>
       </div>         
        </div>
     </body>
  </html>
      `;
};