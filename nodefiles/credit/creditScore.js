var creditScore = function (con, data, applicationId, employed, callback) {
    var obj = {};
    return new Promise((resolve, reject) => {
        getRiskFactor(con, function (response) {
            if (response.status == 1) {
                var countScore = 0;
                response.result.forEach(function (element, idx) {
                    countScore = (element.weight_type == 1) ? parseFloat(countScore) + parseFloat(element.weight) : parseFloat(countScore) - parseFloat(element.weight);
                })
                var FFScore = parseFloat(data.details.creditProfile[0].riskModel[0].score);
                //FFScore = FFScore+countScore;
                getScoreThresholds(con, function (threes) {
                    if (threes.status == 1) {
                        if (FFScore >= threes.result[0].value) {
                            if (employed == 1) {
                                obj.status = 1;
                                obj.creditProfile = 1;
                                obj.creditScroe = FFScore;
                                obj.applicationId = applicationId;
                                obj.applicationStatus = 1;
                                obj.message = "Application approved successfully.";
                                callback(obj);
                                resolve();
                            } else {
                                obj.status = 1;
                                obj.creditProfile = 2;
                                obj.creditScroe = FFScore;
                                obj.applicationId = applicationId;
                                obj.applicationStatus = 2;
                                obj.message = "Application approved for manual process.";
                                callback(obj);
                                resolve();
                            }

                            // check for manual process
                            /*getManualScroe(con, function (manual) {
                                if (manual.status == 1) {
                                    //FFScore = FFScore+countScore;
                                    if (FFScore >= manual.result[0].value) {
                                        obj.status = 1;
                                        obj.creditProfile = 1;
                                        obj.creditScroe = FFScore;
                                        obj.applicationId = applicationId;
                                        obj.applicationStatus = 1;
                                        obj.message = "Application approved successfully.";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.creditProfile = 2;
                                        obj.creditScroe = FFScore;
                                        obj.applicationId = applicationId;
                                        obj.applicationStatus = 2;
                                        obj.message = "Application approved for manual process.";
                                        return callback(obj);
                                    }
                                } else {
                                    obj.status = 1;
                                    obj.creditProfile = 2;
                                    obj.creditScroe = FFScore;
                                    obj.applicationId = applicationId;
                                    obj.applicationStatus = 2;
                                    obj.message = "Application approved for manual process.";
                                    return callback(obj);
                                }
                            })*/
                        } else if (FFScore <= threes.result[0].value) {
                            /*obj.status = 1;
                            obj.creditProfile = 0;
                            obj.creditScroe = FFScore;
                            obj.applicationId = applicationId;
                            obj.applicationStatus = 0;
                            obj.message = "Application not approved.";
                            return callback(obj);*/
                            getManualScroe(con, function (manual) {
                                if (manual.status == 1) {
                                    //FFScore = FFScore+countScore;
                                    if (FFScore >= manual.result[0].value) {
                                        obj.status = 1;
                                        obj.creditProfile = 2;
                                        obj.creditScroe = FFScore;
                                        obj.applicationId = applicationId;
                                        obj.applicationStatus = 2;
                                        obj.message = "Application approved for manual process.";
                                        callback(obj);
                                        resolve();
                                    } else {
                                        obj.status = 1;
                                        obj.creditProfile = 0;
                                        obj.creditScroe = FFScore;
                                        obj.applicationId = applicationId;
                                        obj.applicationStatus = 0;
                                        obj.message = "Application not approved.";
                                        callback(obj);
                                        resolve();
                                    }
                                } else {
                                    obj.status = 1;
                                    obj.creditProfile = 0;
                                    obj.creditScroe = FFScore;
                                    obj.applicationId = applicationId;
                                    obj.applicationStatus = 0;
                                    obj.message = "Application not approved.";
                                    callback(obj);
                                    resolve();
                                }
                            })
                        } else {
                            obj.status = 1;
                            obj.creditProfile = 0;
                            obj.creditScroe = FFScore;
                            obj.applicationId = applicationId;
                            obj.applicationStatus = 0;
                            obj.message = "Application not approved.";
                            callback(obj);
                            resolve();
                        }
                    } else {
                        obj.status = 1;
                        obj.creditProfile = 0;
                        obj.creditScroe = FFScore;
                        obj.applicationId = applicationId;
                        obj.applicationStatus = 0;
                        obj.message = "Application not approved.";
                        callback(obj);
                        resolve();
                    }
                })
                //if()
                //return callback(response);
            } else {
                callback(response);
                resolve();
            }
        });
    })
}
var getRiskFactor = function (con, callback) {
    var obj = {};
    con.query('SELECT master_data_values.value,risk_factor_weight.weight,risk_factor_weight.weight_type FROM risk_factor_weight '
        + 'INNER JOIN master_data_values ON master_data_values.mdv_id=risk_factor_weight.mdv_factor_type_id '
        + 'WHERE risk_factor_weight.effective_date<=now() '
        + 'GROUP BY risk_factor_weight.mdv_factor_type_id DESC'
        , function (err, result) {
            if (err) {
                obj.status = 0;
                obj.message = "Risk Factor not found.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = result;
                return callback(obj);
            }

        });

}
var getScoreThresholds = function (con, callback) {
    var obj = {};
    con.query('SELECT value FROM score_thresholds WHERE '
        + 'effective_date<=now() '
        + 'AND status = 1 AND deleted_flag = 0 AND primary_flag = 1 ORDER BY id DESC limit 1'
        , function (err, result) {
            if (err) {
                obj.status = 0;
                obj.message = "Score thresholds not found.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = result;
                return callback(obj);
            }

        });

}
var getManualScroe = function (con, callback) {
    var obj = {};
    con.query('SELECT value FROM score_thresholds WHERE '
        + 'effective_date<=now() '
        + 'AND status = 1 AND deleted_flag = 0 AND primary_flag = 0 ORDER BY id DESC limit 1'
        , function (err, result) {
            if (err) {
                obj.status = 0;
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = result;
                return callback(obj);
            }

        });

}

var getMonthlyPlan = function (con, credit, procedure_date, package, callback) {
    if (credit[0].manual_term !== null && credit[0].manual_interest !== null) {
        
        let date = require('date-and-time');
        let moment = require('moment');

        let now = new Date(procedure_date);
        var planDetailsManual = [];

        package.forEach(function (element, idx) {
            var perMonth = 0;
            var lastMonth = 0;


            if (element.interest_rate > 0) {
                perMonthInt = (element.interest_rate / 12) / 100;

                perMonth = credit[0].amount * (perMonthInt * Math.pow((1 + perMonthInt), element.term_month)) / (Math.pow((1 + perMonthInt), element.term_month) - 1);
            } else {
                perMonth = credit[0].amount / element.term_month;
            }

            lastMonth = perMonth;
            var monthlyPlan = [];
            var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;

            for (var i = 1; i <= element.term_month; i++) {
                var month = {}
                if (date.format(now, 'DD') <= 15) {
                    //let next_month = date.addMonths(now, (i - 1));
                    let next_month = new Date(moment(now).add((i - 1), 'months'));
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = credit[0].manual_term;
                    month.next_month = next_month;
                    month.perMonth = parseFloat(perMonth).toFixed(2);
                    monthlyPlan.push(month)
                } else {
                    //let next_month = date.addMonths(now, (i));
                    let next_month = new Date(moment(now).add((i), 'months'));
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = credit[0].manual_term;
                    month.next_month = next_month;
                    month.perMonth = parseFloat(perMonth).toFixed(2);
                    monthlyPlan.push(month)
                }
            }

            var mytest = {}
            mytest.interest_rate = element.interest_rate;
            mytest.org_interest_rate = element.interest_rate;
            mytest.plan_id = element.id;
            mytest.term_month = element.term_month;
            mytest.firstMonth = parseFloat(perMonth).toFixed(2);
            mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
            mytest.monthlyPlan = monthlyPlan;
            mytest.totalAmount = totalAmount.toFixed(2);
            planDetailsManual.push(mytest)
        })
        var obj = {
            "status": 1,
            "planDetails": planDetailsManual,
        }
        return callback(planDetailsManual)
    } else {
        getDiscountDetails(con, credit[0].provider_id, function (reducedDiscount) {
            let date = require('date-and-time');
            let moment = require('moment');
            let applicationDate = credit[0].date_created.split('/');
            applicationDate = applicationDate[2] + '-' + applicationDate[1] + '-' + applicationDate[0];
            //let now = new Date(applicationDate);
            let now = new Date(procedure_date);
            var planDetails = [];
            if (reducedDiscount.status == 1) {
                var today = date.format(new Date(), 'YYYY/MM/DD');

                package.forEach(function (element, idx) {
                    var last_discount = 0;

                    var orgInterest = element.interest_rate;
                    var filterData = reducedDiscount.result.filter(x => x.term_month == element.term_month);
                    if (filterData.length > 0) {
                        var filterDataPromotional = filterData.filter(x => x.start_date <= today && x.end_date >= today && x.discount_type == 'Promotional');

                        if (filterDataPromotional.length > 0) {
                            last_discount = filterDataPromotional[0].reduced_interest_pct;
                            element.interest_rate = (element.interest_rate - ((filterDataPromotional[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                        } else {
                            const now = new Date(procedure_date);
                            let closest = Infinity;
                            filterData.forEach(function (d) {
                                const dateE = new Date(d.start_date);

                                if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                                    closest = d.start_date;

                                }


                            });

                            var filterDataStandered = filterData.filter(x => x.discount_type !== 'Promotional' && x.start_date == closest);

                            if (filterDataStandered.length > 0) {
                                last_discount = filterDataStandered[0].reduced_interest_pct;
                                element.interest_rate = (element.interest_rate - ((filterDataStandered[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                            } else if (last_discount !== 0) {
                                element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                            }
                        }

                    }

                    var perMonth = 0;
                    var lastMonth = 0;


                    if (element.interest_rate > 0) {
                        perMonthInt = (element.interest_rate / 12) / 100;

                        perMonth = credit[0].amount * (perMonthInt * Math.pow((1 + perMonthInt), element.term_month)) / (Math.pow((1 + perMonthInt), element.term_month) - 1);
                    } else {
                        perMonth = credit[0].amount / element.term_month;
                    }

                    lastMonth = perMonth;
                    var monthlyPlan = [];
                    var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;

                    for (var i = 1; i <= element.term_month; i++) {
                        var month = {}
                        if (date.format(now, 'DD') <= 15) {
                            let next_month = new Date(moment(now).add((i - 1), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(perMonth).toFixed(2);
                            monthlyPlan.push(month)
                        } else {
                            let next_month = new Date(moment(now).add((i), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(perMonth).toFixed(2);
                            monthlyPlan.push(month)
                        }
                    }

                    var mytest = {}
                    mytest.interest_rate = element.interest_rate;
                    mytest.org_interest_rate = orgInterest;
                    mytest.plan_id = element.id;
                    mytest.term_month = element.term_month;
                    mytest.firstMonth = parseFloat(perMonth).toFixed(2);
                    mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
                    mytest.monthlyPlan = monthlyPlan;
                    mytest.totalAmount = totalAmount.toFixed(2);
                    planDetails.push(mytest)
                })


                var obj = {
                    "status": 1,
                    "planDetails": planDetails,
                }
                return callback(planDetails)
            } else {

                package.forEach(function (element, idx) {

                    var orgInterest = element.interest_rate;

                    if (element.term_month > 12) {
                        if (element.interest_rate > 0) {
                            var perMonthInt = (element.interest_rate / 12) / 100;
                            //var perMonth = credit[0].amount * (perMonthInt / (1 - Math.pow(1 + perMonthInt, -(12))));

                            var perMonth = credit[0].amount * (perMonthInt * Math.pow((1 + perMonthInt), 12)) / (Math.pow((1 + perMonthInt), 12) - 1);
                        } else {
                            var perMonth = credit[0].amount / element.term_month;
                        }
                        var monthlyPlan = [];
                        var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;
                        for (var i = 1; i <= element.term_month; i++) {
                            var month = {}
                            if (date.format(now, 'DD') <= 15) {
                                let next_month = date.addMonths(now, (i - 1));

                                next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                                next_month = date.format(next_month, 'MM/DD/YYYY');
                                month.interest_id = element.id;
                                month.next_month = next_month;
                                month.perMonth = parseFloat(perMonth).toFixed(2);
                                monthlyPlan.push(month)
                            } else {
                                let next_month = date.addMonths(now, (i));
                                next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                                next_month = date.format(next_month, 'MM/DD/YYYY');
                                month.interest_id = element.id;
                                month.next_month = next_month;
                                month.perMonth = parseFloat(perMonth).toFixed(2);
                                monthlyPlan.push(month)
                            }

                        }
                    } else {
                        var perMonth = (parseFloat(credit[0].amount) +
                            parseFloat(credit[0].amount) *
                            (
                                (
                                    parseFloat(element.interest_rate) / 100
                                ) / parseFloat(element.term_month)
                            )
                            * parseFloat(element.term_month)
                        ) / parseFloat(element.term_month);
                        var monthlyPlan = [];
                        var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;
                        for (var i = 1; i <= element.term_month; i++) {
                            var month = {}
                            if (date.format(now, 'DD') <= 15) {
                                let next_month = date.addMonths(now, (i - 1));

                                next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                                next_month = date.format(next_month, 'MM/DD/YYYY');
                                month.interest_id = element.id;
                                month.next_month = next_month;
                                month.perMonth = parseFloat(perMonth).toFixed(2);
                                monthlyPlan.push(month)
                            } else {
                                let next_month = date.addMonths(now, (i));
                                next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                                next_month = date.format(next_month, 'MM/DD/YYYY');
                                month.interest_id = element.id;
                                month.next_month = next_month;
                                month.perMonth = parseFloat(perMonth).toFixed(2);
                                monthlyPlan.push(month)
                            }

                        }
                    }

                    //planDetails.interest_rate = element.interest_rate;
                    //planDetails.term_month = element.term_month; 
                    var mytest = {}
                    mytest.interest_rate = element.interest_rate;
                    mytest.org_interest_rate = orgInterest;
                    mytest.plan_id = element.id;
                    mytest.term_month = element.term_month;
                    mytest.monthlyPlan = monthlyPlan;
                    mytest.totalAmount = totalAmount.toFixed(2);
                    planDetails.push(mytest)
                })


                var obj = {
                    "status": 1,
                    "planDetails": planDetails,
                }
                return callback(planDetails)
            }
        })
    }


}

var getMonthlyPlanBudget = function (con, credit, procedure_date, package, monthly_amount, callback) {

    if (credit[0].manual_term !== null && credit[0].manual_interest !== null) {
        let date = require('date-and-time');
        let moment = require('moment');

        let now = new Date(procedure_date);
        var planDetailsManualBudget = [];

        package.forEach(function (element, idx) {
            var orgInterest = element.newAPR;
            var last_discount = 0;

            element.newAPR = element.newAPR / 100;
            var newTerm = (element.newAPR > 0) ? -Math.log(parseFloat(((parseFloat(-element.newAPR) * credit[0].amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(element.newAPR) / 12)) : (credit[0].amount / monthly_amount);
            element.term_month = Math.ceil(newTerm);

            var monthlyPlan = [];
            var totalAmount = 0;
            var perMonth = monthly_amount;
            var lastMonth = 0;
            for (var i = 1; i <= element.term_month; i++) {
                if (element.newAPR == 0) {
                    var redAmt = monthly_amount * i;
                    monthly_amount = (remainingAmt > monthly_amount) ? monthly_amount : remainingAmt;
                } else if (i < element.term_month) {
                    var redAmt = (((12 * monthly_amount) / element.newAPR) - credit[0].amount) * (Math.pow(1 + (element.newAPR / 12), i) - 1);
                } else {
                    var redAmt = 0;
                    monthly_amount = (remainingAmt * (1 + (element.newAPR / 12))).toFixed(2)
                }
                lastMonth = monthly_amount;
                redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
                remainingAmt = credit[0].amount - redAmt;
                totalAmount += parseFloat(monthly_amount);
                var month = {}
                if (date.format(now, 'DD') <= 15) {
                    let next_month = new Date(moment(now).add((i - 1), 'months'));
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = element.id;
                    month.next_month = next_month;
                    month.perMonth = parseFloat(monthly_amount).toFixed(2);
                    monthlyPlan.push(month)
                } else {
                    let next_month = new Date(moment(now).add((i), 'months'));
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = element.id;
                    month.next_month = next_month;
                    month.perMonth = parseFloat(monthly_amount).toFixed(2);
                    monthlyPlan.push(month)
                }
            }

            var mytest = {}
            mytest.interest_rate = element.interest_rate;
            mytest.org_interest_rate = orgInterest;
            mytest.plan_id = element.id;
            mytest.term_month = element.term_month;
            mytest.monthlyPlan = monthlyPlan;
            mytest.firstMonth = parseFloat(perMonth).toFixed(2);
            mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
            mytest.totalAmount = totalAmount.toFixed(2);
            planDetailsManualBudget.push(mytest)
        })

        var obj = {
            "status": 1,
            "planDetails": planDetailsManualBudget,
        }
        return callback(planDetailsManualBudget)

    } else {
        getDiscountDetails2(con, credit[0].provider_id, package[0].id, function (reducedDiscount) {

            let date = require('date-and-time');
            let moment = require('moment');
            let applicationDate = credit[0].date_created.split('/');
            applicationDate = applicationDate[2] + '-' + applicationDate[1] + '-' + applicationDate[0];
            //let now = new Date(applicationDate);
            let now = new Date(procedure_date);
            var planDetails = [];
            if (reducedDiscount.status == 1) {

                var today = date.format(new Date(), 'YYYY/MM/DD');

                var remainingAmt = parseFloat(credit[0].amount);
                package.forEach(function (element, idx) {
                    var last_discount = 0;
                    var orgInterest = element.newAPR;
                    var filterData = reducedDiscount.result;//.filter(x => x.term_month == element.term_month);
                    if (filterData.length > 0) {
                        var filterDataPromotional = filterData.filter(x => x.start_date <= today && x.end_date >= today && x.discount_type == 'Promotional');
                        if (filterDataPromotional.length > 0) {

                            last_discount = filterDataPromotional[0].reduced_interest_pct;
                            element.interest_rate = (element.interest_rate - ((filterDataPromotional[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                            element.newAPR = (element.newAPR - ((filterDataPromotional[0].reduced_interest_pct * element.newAPR) / 100)).toFixed(2);
                        } else {
                            const now = new Date(procedure_date);
                            let closest = Infinity;
                            filterData.forEach(function (d) {
                                const dateE = new Date(d.start_date);

                                if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                                    closest = d.start_date;

                                }


                            });
                            var filterDataStandered = filterData.filter(x => x.discount_type !== 'Promotional' && x.start_date == closest);

                            if (filterDataStandered.length > 0) {

                                last_discount = filterDataStandered[0].reduced_interest_pct;
                                element.interest_rate = (element.interest_rate - ((filterDataStandered[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                                element.newAPR = (element.newAPR - ((filterDataStandered[0].reduced_interest_pct * element.newAPR) / 100)).toFixed(2);
                            } else if (last_discount !== 0) {
                                element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                                element.newAPR = (element.newAPR - ((last_discount * element.newAPR) / 100)).toFixed(2);
                            }
                        }

                    } /*else if (last_discount !== 0) {
                        element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                        element.newAPR = (element.newAPR - ((last_discount * element.newAPR) / 100)).toFixed(2);
                    }*/


                    element.newAPR = element.newAPR / 100;
                    var newTerm = (element.newAPR > 0) ? -Math.log(parseFloat(((parseFloat(-element.newAPR) * credit[0].amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(element.newAPR) / 12)) : (credit[0].amount / monthly_amount);
                    element.term_month = Math.ceil(newTerm);

                    var monthlyPlan = [];
                    var totalAmount = 0;
                    var perMonth = monthly_amount;
                    var lastMonth = 0;
                    for (var i = 1; i <= element.term_month; i++) {
                        if (element.newAPR == 0) {
                            var redAmt = monthly_amount * i;
                            monthly_amount = (remainingAmt > monthly_amount) ? monthly_amount : remainingAmt;
                        } else if (i < element.term_month) {
                            var redAmt = (((12 * monthly_amount) / element.newAPR) - credit[0].amount) * (Math.pow(1 + (element.newAPR / 12), i) - 1);
                        } else {
                            var redAmt = 0;
                            monthly_amount = (remainingAmt * (1 + (element.newAPR / 12))).toFixed(2)
                        }
                        lastMonth = monthly_amount;
                        redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
                        remainingAmt = credit[0].amount - redAmt;
                        totalAmount += parseFloat(monthly_amount);
                        var month = {}
                        if (date.format(now, 'DD') <= 15) {
                            //let next_month = date.addMonths(now, (i - 1));
                            let next_month = new Date(moment(now).add((i - 1), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(monthly_amount).toFixed(2);
                            monthlyPlan.push(month)
                        } else {
                            //let next_month = date.addMonths(now, (i));
                            let next_month = new Date(moment(now).add((i), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(monthly_amount).toFixed(2);
                            monthlyPlan.push(month)
                        }
                    }


                    //planDetails.interest_rate = element.interest_rate;
                    //planDetails.term_month = element.term_month; 
                    var mytest = {}
                    mytest.interest_rate = element.interest_rate;
                    mytest.org_interest_rate = orgInterest;
                    mytest.plan_id = element.id;
                    mytest.term_month = element.term_month;
                    mytest.monthlyPlan = monthlyPlan;
                    mytest.firstMonth = parseFloat(perMonth).toFixed(2);
                    mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
                    mytest.totalAmount = totalAmount.toFixed(2);
                    planDetails.push(mytest)
                })


                var obj = {
                    "status": 1,
                    "planDetails": planDetails,
                }
                return callback(planDetails)
            } else {

                package.forEach(function (element, idx) {
                    var orgInterest = element.newAPR;


                    element.newAPR = element.newAPR / 100;
                    var newTerm = (element.newAPR > 0) ? -Math.log(parseFloat(((parseFloat(-element.newAPR) * credit[0].amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(element.newAPR) / 12)) : (credit[0].amount / monthly_amount);
                    element.term_month = Math.ceil(newTerm);

                    var monthlyPlan = [];
                    var totalAmount = 0;
                    for (var i = 1; i <= element.term_month; i++) {
                        if (element.newAPR == 0) {
                            var redAmt = monthly_amount;
                        } else if (i < element.term_month) {
                            var redAmt = (((12 * monthly_amount) / element.newAPR) - credit[0].amount) * (Math.pow(1 + (element.newAPR / 12), i) - 1);
                        } else {
                            var redAmt = 0;
                            monthly_amount = (remainingAmt * (1 + (element.newAPR / 12))).toFixed(2)
                        }

                        redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
                        remainingAmt = credit[0].amount - redAmt;
                        totalAmount += parseFloat(monthly_amount);
                        var month = {}
                        if (date.format(now, 'DD') <= 15) {
                            //let next_month = date.addMonths(now, (i - 1));
                            let next_month = new Date(moment().add((i - 1), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(monthly_amount).toFixed(2);
                            monthlyPlan.push(month)
                        } else {
                            //let next_month = date.addMonths(now, (i));
                            let next_month = new Date(moment().add((i), 'months'));
                            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                            next_month = date.format(next_month, 'MM/DD/YYYY');
                            month.interest_id = element.id;
                            month.next_month = next_month;
                            month.perMonth = parseFloat(monthly_amount).toFixed(2);
                            monthlyPlan.push(month)
                        }
                    }


                    //planDetails.interest_rate = element.interest_rate;
                    //planDetails.term_month = element.term_month; 
                    var mytest = {}
                    mytest.interest_rate = element.interest_rate;
                    mytest.org_interest_rate = orgInterest;
                    mytest.plan_id = element.id;
                    mytest.term_month = element.term_month;
                    mytest.monthlyPlan = monthlyPlan;
                    mytest.totalAmount = totalAmount.toFixed(2);
                    planDetails.push(mytest)
                })


                var obj = {
                    "status": 1,
                    "planDetails": planDetails,
                }
                return callback(planDetails)
            }
        })
    }

}


var getDiscountDetails = function (con, id, callback) {
    var obj = {};
    con.query('SELECT master_data_values.value AS term_month, master_data_values2.value AS discount_type, discount_fee_ra.reduced_interest_pct,DATE_FORMAT(discount_fee_ra.start_date, "%Y/%m/%d") as start_date,DATE_FORMAT(discount_fee_ra.end_date, "%Y/%m/%d") as end_date '
        + 'FROM provider_discount_fee '
        + 'INNER JOIN discount_fee_ra ON discount_fee_ra.df_ra_id=provider_discount_fee.df_ra_id '
        + 'INNER JOIN master_data_values ON master_data_values.mdv_id=discount_fee_ra.loan_term_month '
        + 'INNER JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=discount_fee_ra.mdv_discount_rate_id '
        + 'WHERE '
        + 'provider_discount_fee.provider_id=? AND provider_discount_fee.status=? AND discount_fee_ra.status=? ORDER BY discount_fee_ra.date_modified'
        ,
        [id, 1, 1]
        , function (err, result) {
            if (err) {
                obj.status = 0;
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = result;
                return callback(obj);
            }

        });
}

var getDiscountDetails2 = function (con, id, term_id, callback) {
    var obj = {};
    con.query('SELECT master_data_values.value AS term_month, master_data_values2.value AS discount_type, discount_fee_ra.reduced_interest_pct,DATE_FORMAT(discount_fee_ra.start_date, "%Y/%m/%d") as start_date,DATE_FORMAT(discount_fee_ra.end_date, "%Y/%m/%d") as end_date '
        + 'FROM provider_discount_fee '
        + 'INNER JOIN discount_fee_ra ON discount_fee_ra.df_ra_id=provider_discount_fee.df_ra_id '
        + 'INNER JOIN master_data_values ON master_data_values.mdv_id=discount_fee_ra.loan_term_month '
        + 'INNER JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=discount_fee_ra.mdv_discount_rate_id '
        + 'WHERE '
        + 'master_data_values.mdv_id=? AND provider_discount_fee.provider_id=? AND provider_discount_fee.status=? AND discount_fee_ra.status=? ORDER BY discount_fee_ra.date_modified'
        ,
        [term_id, id, 1, 1]
        , function (err, result) {
            if (err) {
                obj.status = 0;
                return callback(obj);
            } else {
                obj.status = 1;
                obj.result = result;
                return callback(obj);
            }

        });
}

var getEstMonthlyPlan = function (con, searchDetails, package, callback) {


    getDiscountDetails(con, searchDetails.provider_id, function (reducedDiscount) {

        let date = require('date-and-time');
        let moment = require('moment');
        //let now = new Date(applicationDate);
        let now = new Date();
        var planDetails = [];
        if (reducedDiscount.status == 1) {

            var today = date.format(new Date(), 'YYYY/MM/DD');

            package.forEach(function (element, idx) {
                var last_discount = 0;
                var filterData = reducedDiscount.result.filter(x => x.term_month == element.term_month);
                if (filterData.length > 0) {
                    var filterDataPromotional = filterData.filter(x => x.start_date <= today && x.end_date >= today);
                    if (filterDataPromotional.length > 0) {
                        last_discount = filterDataPromotional[0].reduced_interest_pct;
                        element.interest_rate = (element.interest_rate - ((filterDataPromotional[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                    } else {
                        const now = new Date();
                        let closest = Infinity;
                        filterData.forEach(function (d) {
                            const dateE = new Date(d.start_date);

                            if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                                closest = d.start_date;

                            }


                        });

                        var filterDataStandered = filterData.filter(x => x.discount_type !== 'Promotional' && x.start_date == closest);

                        if (filterDataStandered.length > 0) {
                            last_discount = filterDataStandered[0].reduced_interest_pct;
                            element.interest_rate = (element.interest_rate - ((filterDataStandered[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                        } else if (last_discount !== 0) {
                            element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                        }
                    }

                } /*else if (last_discount !== 0) {
                    element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                }*/
                /*
                * If term more then 12 month then use following per month interest rate.
                */

                if (element.interest_rate > 0) {
                    var perMonthInt = (element.interest_rate / 12) / 100;
                    //var perMonth = credit[0].amount * (perMonthInt / (1 - Math.pow(1 + perMonthInt, -(12))));

                    var perMonth = parseFloat(searchDetails.amount) * (perMonthInt * Math.pow((1 + perMonthInt), element.term_month)) / (Math.pow((1 + perMonthInt), element.term_month) - 1);
                } else {
                    var perMonth = parseFloat(searchDetails.amount) / element.term_month;
                }
                //var perMonthInt = (element.interest_rate / 12) / 100;

                //var perMonth = parseFloat(searchDetails.amount) * (perMonthInt * Math.pow((1 + perMonthInt), 12)) / (Math.pow((1 + perMonthInt), 12) - 1);


                var monthlyPlan = [];
                var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;

                for (var i = 1; i <= element.term_month; i++) {
                    var month = {}
                    if (date.format(now, 'DD') <= 15) {
                        //let next_month = date.addMonths(now, (i - 1));
                        let next_month = new Date(moment().add((i - 1), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(perMonth).toFixed(2);
                        monthlyPlan.push(month)
                    } else {
                        //let next_month = date.addMonths(now, (i));
                        let next_month = new Date(moment().add((i), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(perMonth).toFixed(2);
                        monthlyPlan.push(month)
                    }
                }

                //planDetails.interest_rate = element.interest_rate;
                //planDetails.term_month = element.term_month; 
                var mytest = {}
                mytest.interest_rate = element.interest_rate;
                mytest.plan_id = element.id;
                mytest.term_month = element.term_month;
                mytest.monthlyPlan = monthlyPlan;
                mytest.totalAmount = totalAmount.toFixed(2);
                planDetails.push(mytest)
            })


            var obj = {
                "status": 1,
                "planDetails": planDetails,
            }
            return callback(planDetails)
        } else {

            package.forEach(function (element, idx) {

                var perMonthInt = (element.interest_rate / 12) / 100;
                //var perMonth = credit[0].amount * (perMonthInt / (1 - Math.pow(1 + perMonthInt, -(12))));
                var perMonth = parseFloat(searchDetails.amount) * (perMonthInt * Math.pow((1 + perMonthInt), 12)) / (Math.pow((1 + perMonthInt), 12) - 1);

                var monthlyPlan = [];
                var totalAmount = parseFloat(perMonth).toFixed(2) * element.term_month;
                for (var i = 1; i <= element.term_month; i++) {
                    var month = {}
                    if (date.format(now, 'DD') <= 15) {
                        //let next_month = date.addMonths(now, (i - 1));
                        let next_month = new Date(moment().add((i - 1), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(perMonth).toFixed(2);
                        monthlyPlan.push(month)
                    } else {
                        //let next_month = date.addMonths(now, (i));
                        let next_month = new Date(moment().add((i), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(perMonth).toFixed(2);
                        monthlyPlan.push(month)
                    }

                }


                //planDetails.interest_rate = element.interest_rate;
                //planDetails.term_month = element.term_month; 
                var mytest = {}
                mytest.interest_rate = element.interest_rate;
                mytest.plan_id = element.id;
                mytest.term_month = element.term_month;
                mytest.monthlyPlan = monthlyPlan;
                mytest.totalAmount = totalAmount.toFixed(2);
                planDetails.push(mytest)
            })


            var obj = {
                "status": 1,
                "planDetails": planDetails,
            }
            return callback(planDetails)
        }
    })
    /*let date = require('date-and-time');
    let now = new Date();
    //let now = new Date();
    var planDetails = [];
    

        package.forEach(function (element, idx) {

            var perMonth = (parseFloat(searchDetails.amount) +
                parseFloat(searchDetails.amount) *
                (
                    (
                        parseFloat(element.interest_rate) / 100
                    ) / parseFloat(element.term_month)
                )
                * parseFloat(element.term_month)
            ) / parseFloat(element.term_month);


            var monthlyPlan = [];
            var totalAmount = perMonth.toFixed(2) * element.term_month;
            for (var i = 1; i <= element.term_month; i++) {
                var month = {}
                if (date.format(now, 'DD') <= 15) {
                    let next_month = date.addMonths(now, i);
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = element.id;
                    month.next_month = next_month;
                    month.perMonth = perMonth.toFixed(2);
                    monthlyPlan.push(month)
                } else {
                    let next_month = date.addMonths(now, (i + 1));
                    next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                    next_month = date.format(next_month, 'MM/DD/YYYY');
                    month.interest_id = element.id;
                    month.next_month = next_month;
                    month.perMonth = perMonth.toFixed(2);
                    monthlyPlan.push(month)
                }

            }
            //planDetails.interest_rate = element.interest_rate;
            //planDetails.term_month = element.term_month; 
            var mytest = {}
            mytest.interest_rate = element.interest_rate;
            mytest.plan_id = element.id;
            mytest.term_month = element.term_month;
            mytest.monthlyPlan = monthlyPlan;
            mytest.totalAmount = totalAmount.toFixed(2);
            planDetails.push(mytest)
        })
        

        var obj = {
            "status": 1,
            "planDetails": planDetails,
        }
        return callback(planDetails)*/


}
var getEstMonthlyPlanBudget = function (con, searchDetails, package, monthly_amount, callback) {




    getDiscountDetails2(con, searchDetails.provider_id, package[0].id, function (reducedDiscount) {

        let date = require('date-and-time');
        let moment = require('moment');
        let now = new Date();
        var planDetails = [];
        if (reducedDiscount.status == 1) {

            var today = date.format(new Date(), 'YYYY/MM/DD');

            var remainingAmt = parseFloat(searchDetails.amount);
            package.forEach(function (element, idx) {
                var last_discount = 0;
                var orgInterest = element.newAPR;
                var filterData = reducedDiscount.result;//.filter(x => x.term_month == element.term_month);
                if (filterData.length > 0) {
                    var filterDataPromotional = filterData.filter(x => x.start_date <= today && x.end_date >= today);
                    if (filterDataPromotional.length > 0) {

                        last_discount = filterDataPromotional[0].reduced_interest_pct;
                        element.interest_rate = (element.interest_rate - ((filterDataPromotional[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                        element.newAPR = (element.newAPR - ((filterDataPromotional[0].reduced_interest_pct * element.newAPR) / 100)).toFixed(2);
                    } else {
                        const now = new Date();
                        let closest = Infinity;
                        filterData.forEach(function (d) {
                            const dateE = new Date(d.start_date);

                            if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                                closest = d.start_date;

                            }


                        });

                        var filterDataStandered = filterData.filter(x => x.discount_type !== 'Promotional' && x.start_date == closest);

                        if (filterDataStandered.length > 0) {

                            last_discount = filterDataStandered[0].reduced_interest_pct;
                            element.interest_rate = (element.interest_rate - ((filterDataStandered[0].reduced_interest_pct * element.interest_rate) / 100)).toFixed(2);
                            element.newAPR = (element.newAPR - ((filterDataStandered[0].reduced_interest_pct * element.newAPR) / 100)).toFixed(2);
                        } else if (last_discount !== 0) {
                            element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                            element.newAPR = (element.newAPR - ((last_discount * element.newAPR) / 100)).toFixed(2);
                        }
                    }

                } /*else if (last_discount !== 0) {
                    element.interest_rate = (element.interest_rate - ((last_discount * element.interest_rate) / 100)).toFixed(2);
                    element.newAPR = (element.newAPR - ((last_discount * element.newAPR) / 100)).toFixed(2);
                }*/


                element.newAPR = element.newAPR / 100;
                var newTerm = (element.newAPR > 0) ? -Math.log(parseFloat(((parseFloat(-element.newAPR) * searchDetails.amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(element.newAPR) / 12)) : (searchDetails.amount / monthly_amount);
                element.term_month = Math.ceil(newTerm);

                var monthlyPlan = [];
                var totalAmount = 0;
                for (var i = 1; i <= element.term_month; i++) {
                    if (element.newAPR == 0) {
                        var redAmt = monthly_amount * i;
                        monthly_amount = (remainingAmt > monthly_amount) ? monthly_amount : remainingAmt;
                    } else if (i < element.term_month) {
                        var redAmt = (((12 * monthly_amount) / element.newAPR) - searchDetails.amount) * (Math.pow(1 + (element.newAPR / 12), i) - 1);
                    } else {
                        var redAmt = 0;
                        monthly_amount = (remainingAmt * (1 + (element.newAPR / 12))).toFixed(2)
                    }

                    redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
                    remainingAmt = searchDetails.amount - redAmt;
                    totalAmount += parseFloat(monthly_amount);
                    var month = {}
                    if (date.format(now, 'DD') <= 15) {
                        //let next_month = date.addMonths(now, (i - 1));
                        let next_month = new Date(moment().add((i - 1), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(monthly_amount).toFixed(2);
                        monthlyPlan.push(month)
                    } else {
                        //let next_month = date.addMonths(now, (i));
                        let next_month = new Date(moment().add((i), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(monthly_amount).toFixed(2);
                        monthlyPlan.push(month)
                    }
                }


                //planDetails.interest_rate = element.interest_rate;
                //planDetails.term_month = element.term_month; 
                var mytest = {}
                mytest.interest_rate = element.interest_rate;
                mytest.org_interest_rate = orgInterest;
                mytest.plan_id = element.id;
                mytest.term_month = element.term_month;
                mytest.monthlyPlan = monthlyPlan;
                mytest.totalAmount = totalAmount.toFixed(2);
                planDetails.push(mytest)
            })


            var obj = {
                "status": 1,
                "planDetails": planDetails,
            }
            return callback(planDetails)
        } else {

            package.forEach(function (element, idx) {
                var orgInterest = element.newAPR;
                element.newAPR = element.newAPR / 100;
                var newTerm = (element.newAPR > 0) ? -Math.log(parseFloat(((parseFloat(-element.newAPR) * searchDetails.amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(element.newAPR) / 12)) : (searchDetails.amount / monthly_amount);
                element.term_month = Math.ceil(newTerm);

                var monthlyPlan = [];
                var totalAmount = 0;
                for (var i = 1; i <= element.term_month; i++) {
                    if (element.newAPR == 0) {
                        var redAmt = monthly_amount;
                    } else if (i < element.term_month) {
                        var redAmt = (((12 * monthly_amount) / element.newAPR) - searchDetails.amount) * (Math.pow(1 + (element.newAPR / 12), i) - 1);
                    } else {
                        var redAmt = 0;
                        monthly_amount = (remainingAmt * (1 + (element.newAPR / 12))).toFixed(2)
                    }

                    redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
                    remainingAmt = searchDetails.amount - redAmt;
                    totalAmount += parseFloat(monthly_amount);
                    var month = {}
                    if (date.format(now, 'DD') <= 15) {
                        //let next_month = date.addMonths(now, (i - 1));
                        let next_month = new Date(moment().add((i - 1), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(monthly_amount).toFixed(2);
                        monthlyPlan.push(month)
                    } else {
                        //let next_month = date.addMonths(now, (i));
                        let next_month = new Date(moment().add((i), 'months'));
                        next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                        next_month = date.format(next_month, 'MM/DD/YYYY');
                        month.interest_id = element.id;
                        month.next_month = next_month;
                        month.perMonth = parseFloat(monthly_amount).toFixed(2);
                        monthlyPlan.push(month)
                    }
                }

                //planDetails.interest_rate = element.interest_rate;
                //planDetails.term_month = element.term_month; 
                var mytest = {}
                mytest.interest_rate = element.interest_rate;
                mytest.org_interest_rate = orgInterest;
                mytest.plan_id = element.id;
                mytest.term_month = element.term_month;
                mytest.monthlyPlan = monthlyPlan;
                mytest.totalAmount = totalAmount.toFixed(2);
                planDetails.push(mytest)
            })


            var obj = {
                "status": 1,
                "planDetails": planDetails,
            }
            return callback(planDetails)
        }
    })

}

var getMonthlyPlanRe = function (credit, pvrDetails, callback) {
    pvrDetails = pvrDetails[0];
    let date = require('date-and-time');
    let now = new Date(credit.procedure_date);
    //let now = new Date();
    var planDetails = [];
    var perMonth = 0;
    var lastMonth = 0;
    var orgInterest = pvrDetails.interest_rate;
    if (pvrDetails.discounted_interest_rate > 0) {
        var perMonthInt = (pvrDetails.discounted_interest_rate / 12) / 100;
        perMonth = credit.loan_amount * (perMonthInt * Math.pow((1 + perMonthInt), pvrDetails.payment_term_month)) / (Math.pow((1 + perMonthInt), pvrDetails.payment_term_month) - 1);
    } else {
        perMonth = credit.loan_amount / pvrDetails.payment_term_month;
    }

    lastMonth = perMonth;
    var monthlyPlan = [];
    var totalAmount = parseFloat(perMonth).toFixed(2) * pvrDetails.payment_term_month;

    for (var i = 1; i <= pvrDetails.payment_term_month; i++) {
        var month = {}
        if (date.format(now, 'DD') <= 15) {
            let next_month = date.addMonths(now, (i - 1));

            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
            next_month = date.format(next_month, 'MM/DD/YYYY');

            month.next_month = next_month;
            month.perMonth = parseFloat(perMonth).toFixed(2);
            monthlyPlan.push(month)
        } else {
            let next_month = date.addMonths(now, (i));
            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
            next_month = date.format(next_month, 'MM/DD/YYYY');

            month.next_month = next_month;
            month.perMonth = parseFloat(perMonth).toFixed(2);
            monthlyPlan.push(month)
        }

    }
    var mytest = {}
    mytest.interest_rate = pvrDetails.discounted_interest_rate;
    mytest.org_interest_rate = orgInterest;
    mytest.term_month = pvrDetails.payment_term_month;
    mytest.monthlyPlan = monthlyPlan;
    mytest.totalAmount = totalAmount.toFixed(2);
    mytest.firstMonth = parseFloat(perMonth).toFixed(2);
    mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
    planDetails.push(mytest)

    var obj = {
        "status": 1,
        "planDetails": planDetails,
    }
    return callback(planDetails)

}
var getMonthlyPlanBudgetRe = function (credit, pvrDetails, callback) {
    pvrDetails = pvrDetails[0];
    var monthly_amount = pvrDetails.monthly_amount;
    let date = require('date-and-time');
    let moment = require('moment');
    let now = new Date(credit.procedure_date);
    //let now = new Date();
    var planDetails = [];


    var orgInterest = pvrDetails.interest_rate;
    var remainingAmt = parseFloat(credit.loan_amount);

    pvrDetails.discounted_interest_rate = pvrDetails.discounted_interest_rate / 100;
    var newTerm = (pvrDetails.discounted_interest_rate > 0) ? -Math.log(parseFloat(((parseFloat(-pvrDetails.discounted_interest_rate) * credit.loan_amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(pvrDetails.discounted_interest_rate) / 12)) : (credit.loan_amount / monthly_amount);
    pvrDetails.payment_term_month = Math.ceil(newTerm);

    var monthlyPlan = [];
    var totalAmount = 0;
    var perMonth = monthly_amount;
    var lastMonth = 0;
    for (var i = 1; i <= pvrDetails.payment_term_month; i++) {
        if (pvrDetails.discounted_interest_rate == 0) {
            var redAmt = monthly_amount * i;
            monthly_amount = (remainingAmt > monthly_amount) ? monthly_amount : remainingAmt;
        } else if (i < pvrDetails.payment_term_month) {
            var redAmt = (((12 * monthly_amount) / pvrDetails.discounted_interest_rate) - credit.loan_amount) * (Math.pow(1 + (pvrDetails.discounted_interest_rate / 12), i) - 1);
        } else {
            var redAmt = 0;
            monthly_amount = (remainingAmt * (1 + (pvrDetails.discounted_interest_rate / 12))).toFixed(2)
        }
        lastMonth = monthly_amount;
        redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
        remainingAmt = credit.loan_amount - redAmt;
        totalAmount += parseFloat(monthly_amount);
        var month = {}
        if (date.format(now, 'DD') <= 15) {
            //let next_month = date.addMonths(now, (i - 1));
            let next_month = new Date(moment(credit.procedure_date).add((i - 1), 'months'));
            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
            next_month = date.format(next_month, 'MM/DD/YYYY');
            month.next_month = next_month;
            month.perMonth = parseFloat(monthly_amount).toFixed(2);
            monthlyPlan.push(month)
        } else {
            //let next_month = date.addMonths(now, (i));
            let next_month = new Date(moment(credit.procedure_date).add((i), 'months'));
            next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
            next_month = date.format(next_month, 'MM/DD/YYYY');
            month.next_month = next_month;
            month.perMonth = parseFloat(monthly_amount).toFixed(2);
            monthlyPlan.push(month)
        }
    }

    var mytest = {}
    mytest.interest_rate = pvrDetails.interest_rate;
    mytest.org_interest_rate = orgInterest;
    mytest.term_month = pvrDetails.payment_term_month;
    mytest.monthlyPlan = monthlyPlan;
    mytest.firstMonth = parseFloat(perMonth).toFixed(2);
    mytest.lastMonth = parseFloat(lastMonth).toFixed(2);
    mytest.totalAmount = totalAmount.toFixed(2);
    planDetails.push(mytest)

    return callback(planDetails)
}

exports.creditScore = creditScore;
exports.getMonthlyPlan = getMonthlyPlan;
exports.getRiskFactor = getRiskFactor;
exports.getEstMonthlyPlan = getEstMonthlyPlan;
exports.getMonthlyPlanBudget = getMonthlyPlanBudget;
exports.getEstMonthlyPlanBudget = getEstMonthlyPlanBudget;
exports.getMonthlyPlanRe = getMonthlyPlanRe;
exports.getMonthlyPlanBudgetRe = getMonthlyPlanBudgetRe;