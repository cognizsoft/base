module.exports = (data, logoimg) => {
   let moment = require('moment');
   //console.log(data)
   //return false;
   let customer_html = '';
   let provider_html = '';

   var refund_amt = (data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Payment Method :</strong></td><td>' + data.customer_refund[0].refundmethod +  '</td></tr>' : '';
   var check_date = (data.customer_refund[0].payment_method == 3 && data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Check Date :</strong></td><td>' + data.customer_refund[0].check_date + '</td></tr>' : '';
   var check_no = (data.customer_refund[0].payment_method == 3 && data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Check No :</strong></td><td>' + data.customer_refund[0].check_no + '</td></tr>' : '';
   var bank_name = (data.customer_refund[0].payment_method == 1 && data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Bank Name :</strong></td><td>' + data.customer_refund[0].ach_bank_name + '</td></tr>' : '';
   var account_no = (data.customer_refund[0].payment_method == 1 && data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Account No :</strong></td><td>' + data.customer_refund[0].ach_account_no + '</td></tr>' : '';
   var routing_no = (data.customer_refund[0].payment_method == 1 && data.customer_refund[0].refund_amt > 0) ? '<tr><td className=""><strong>Routing No :</strong> </td><td>' + data.customer_refund[0].ach_routing_no + '</td></tr>' : '';
   var note = (data.customer_refund[0].refund_comment) ? '<tr><td className=""><strong>Note :</strong></td><td>' + data.customer_refund[0].refund_comment + '</td></tr>' : '';
   var amount = (data.customer_refund[0].refund_amt) ? '<tr><td className=""><strong>Amount :</strong> </td><td>$' + data.customer_refund[0].refund_amt.toFixed(2) + '</td></tr>' : '';
   customer_html = customer_html 
      + refund_amt
      + check_date
      + check_no
      + bank_name
      + account_no
      + routing_no
      + note
      + amount

   var pro_refund_type = (data.provider_refund[0].iou_flag == 1) ? '<tr><td className=""><strong>Refund Type :</strong></td><td>IOU</td></tr>' : '<tr><td className=""><strong>Refund Type :</strong></td><td>Issue Refund</td></tr>';
   var pro_check_date = (data.provider_refund[0].payment_method == 3) ? '<tr><td className=""><strong>Check Date :</strong></td><td>' + data.provider_refund[0].check_date + '</td></tr>' : '';
   var pro_check_no = (data.provider_refund[0].payment_method == 3) ? '<tr><td className=""><strong>Check No :</strong></td><td>' + data.provider_refund[0].check_no + '</td></tr>' : '';
   var pro_bank_name = (data.provider_refund[0].payment_method == 1) ? '<tr><td className=""><strong>Bank Name :</strong></td><td>' + data.provider_refund[0].ach_bank_name + '</td></tr>' : '';
   var pro_account_no = (data.provider_refund[0].payment_method == 1) ? '<tr><td className=""><strong>Account No :</strong></td><td>' + data.provider_refund[0].ach_account_no + '</td></tr>' : '';
   var pro_routing_no = (data.provider_refund[0].payment_method == 1) ? '<tr><td className=""><strong>Routing No :</strong> </td><td>' + data.provider_refund[0].ach_routing_no + '</td></tr>' : '';
   var pro_note = (data.provider_refund[0].refund_comment) ? '<tr><td className=""><strong>Note :</strong></td><td>' + data.provider_refund[0].refund_comment + '</td></tr>' : '';
   var pro_refund_due = (data.provider_refund[0].refund_due) ? '<tr><td className=""><strong>Amount :</strong> </td><td>$' + data.provider_refund[0].refund_due.toFixed(2) + '</td></tr>' : '';
   provider_html = provider_html 
      + pro_refund_type
      + pro_check_date
      + pro_check_no
      + pro_bank_name
      + pro_account_no
      + pro_routing_no
      + pro_note
      + pro_refund_due
      
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body {
            font-size: 16px;
          }
           
           .margin-top {
           margin-top: 50px;
           }
           .note {
            color: red;
           }
           .justify-center {
           text-align: center;
           width: 100%;
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .info{width: 48%; float: left; margin:0.5%}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .detail-table td{padding:8px 15px; font-size:10px;}
          span.agreement_date, .id_number, .id_exp_date {
              margin-left: 35px;
          }
         .info-bx{
            display: flex !important;
            width: 100%;
            justify-content: space-between;
            flex-wrap: wrap;
          }
          .info{background: #f1f9ff;}
          .invoice-box h4 {line-height: 5px; margin-bottom: 0;}
           .term-heading, .customer-heading {text-align: center; margin-top: 30px; font-size:20px}
          .hidden{visibility:hidden;}
          .customer-accnt .add-card{
            width: 32%;
            border: solid 1px #d7eeff !important;
            background: #f1f9ff;
            
          }
          .customer-accnt .witness-signature{
            float:right;
          }
          .customer-accnt .customer-signature{
            float:left;
          }
          .customer-accnt .image-area{
            margin-top: 10px;
          }
          .sing_box{
            padding: 10px 10px 10px 10px !important;         
          }
        </style>
     </head>
     <body>
        <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
        <h1 class="justify-center">Cancel Refund Plan Details</h1>
        <div class="info-bx invoice-box">
          
          <div class="info customer-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Customer Details</th>
                </tr>

                <tr>
                  <td className=""><strong>Account No :</strong></td>
                  <td>${data.customer_detail[0].patient_ac}</td>
                </tr>

                <tr>
                  <td className=""><strong>Name :</strong></td>
                  <td>${data.customer_detail[0].f_name +', '+ data.customer_detail[0].m_name +', '+ data.customer_detail[0].m_name}</td>
                </tr>

                <tr>
                  <td className=""><strong>Address :</strong></td>
                  <td>${data.customer_detail[0].address1 +', '+ data.customer_detail[0].City +', '+ data.customer_detail[0].state_name +', '+ data.customer_detail[0].zip_code}</td>
                </tr>

                <tr>
                  <td className=""><strong>Phone :</strong></td>
                  <td>${data.customer_detail[0].peimary_phone}</td>
                </tr>

             </table>
          </div>

          <div class="info provider-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Provider Details</th>
                </tr>

                <tr>
                  <td className=""><strong>Account No :</strong></td>
                  <td>${data.provider_detail[0].provider_ac}</td>
                </tr>

                <tr>
                  <td className=""><strong>Name :</strong></td>
                  <td>${data.provider_detail[0].name}</td>
                </tr>

                <tr>
                  <td className=""><strong>Address :</strong></td>
                  <td>${data.provider_detail[0].address1 +', '+ data.provider_detail[0].city +', '+ data.provider_detail[0].state_name +', '+ data.provider_detail[0].zip_code}</td>
                </tr>

                <tr>
                  <td className=""><strong>Phone :</strong></td>
                  <td>${data.provider_detail[0].primary_phone}</td>
                </tr>

             </table>
          </div>

          <div class="info customer-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                  <th colSpan="2">Customer Refund Details</th>
               </tr>

               ${customer_html}

               
             </table>
          </div>

          <div class="info provider-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                   <th colSpan="2">Refund From Provider</th>
                </tr>

                ${provider_html}

             </table>
          </div>

        </div>

     </body>
  </html>
      `;
};