module.exports = (dataFilter, data, logoimg) => {
  //console.log(dataFilter)
    var html = '';
    let moment = require('moment');
    let fileName = '';
    if (dataFilter.filter_type == 1) {
        moment.updateLocale('en', {
           week: {
              dow: 1,
              doy: 1
           }
        });

        let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
        let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
        fileName = 'Weelky Application List (' + startOfWeek + ' - ' + endOfWeek + ')';
    } else if (dataFilter.filter_type == 2) {
        let monthName = moment().month((dataFilter.month - 1)).format("MMM");
        fileName = 'Monthly Application List (' + monthName + '-' + dataFilter.year + ')';
    } else if (dataFilter.filter_type == 3) {
        let queterName = ''
        if(dataFilter.quarter == 1){
           queterName = 'Q1';
        } else if(dataFilter.quarter == 2){
           queterName = 'Q2';
        } else if(dataFilter.quarter == 3){
           queterName = 'Q3';
        } else if(dataFilter.quarter == 4){
           queterName = 'Q4';
        }
        fileName = 'Quarterly Application List (' + queterName + '-' + dataFilter.year + ')';
    } else if (dataFilter.filter_type == 4) {
        fileName = 'Yearly Application List (' + dataFilter.year + ')';
    } else if (dataFilter.filter_type == 5) {
        let currentDate = moment().format('MM/DD/YYYY');
        fileName = 'Year to Date Application List (01/01/' + dataFilter.year + '-'+ currentDate +')';
    } else if (dataFilter.filter_type == 6) {
        let loanStart = moment(dataFilter.start_date).format('MM/DD/YYYY');
        let loanEnd = moment(dataFilter.end_date).format('MM/DD/YYYY');
        fileName = 'By Date Application List (' + loanStart + '-'+ loanEnd +')';
    } else {
        if(dataFilter.status_id != '' && dataFilter.status_id != 'all' && data.result.length>0) {
            fileName = 'Application List ('+data.result[0].status_name+')';
        } else {
            fileName = 'Application List';
        }
    }


    data.result.forEach(function (element, idx) {
        
        html = html + '<tr>'
            + '<td>' + element.application_id + '</td>'
            + '<td>' + element.patient_ac + '</td>'
            + '<td>' + element.application_no + '</td>'
            + '<td>' + element.f_name + ' ' + element.m_name + ' ' + element.l_name + '</td>'
            + '<td>' + element.address1 + '</td>'
            + '<td>' + element.City + '</td>'
            + '<td>' + element.state_name + '</td>'
            + '<td>' + element.peimary_phone + '</td>'
            + '<td>' + element.status_name + '</td>'
            + '<td>$' + parseFloat(element.approve_amount).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(element.override_amount).toFixed(2) + '</td>'
            + '<td>$' + parseFloat(element.remaining_amount).toFixed(2) + '</td>'
            + '<td>' + element.date_created + '</td>'
            + '<td>' + element.expiry_date + '</td>'
            + '</tr>';
    });
    
    return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>Application List</title>
       <style>
            html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 12px;
          line-height: 16px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
       .detail-table td{padding:5px 2px; font-size:10px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">${fileName}</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>ID</th>
                  <th>A/C No</th>
                  <th>App No</th>
                  <th>Full Name</th>
                  <th>Address</th>
                  <th>City</th>
                  <th>State</th>
                  <th>Primary Ph</th>
                  <th>Status</th>
                  <th>LOC</th>
                  <th>Over Amt</th>
                  <th>Avl Bal</th>
                  <th>Date Created</th>
                  <th>Expire Date</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};