module.exports = (planDetails, logoimg) => {
   let moment = require('moment');
   //console.log(planDetails)
   //return false;
   var html = '';
   planDetails.data.planEst.monthlyPlan.forEach(function (element, idx) {
      html = html + '<tr>'
         + '<td>' + (idx + 1) + '</td>'
         + '<td>' + element.next_month + '</td>'
         + '<td>$' + parseFloat(element.perMonth).toFixed(2) + '</td>'
         + '</tr>';
   })
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body {
            font-size: 16px;
          }
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0 30px;
           font-size: 12px;
           line-height: 14px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           .margin-top {
           margin-top: 50px;
           }
           .note {
            color: red;
           }
           .justify-center {
           text-align: center;
           width: 100%;
           }
           .invoice-box table {
           width: 100%;
           line-height: inherit;
           text-align: left;
        border:solid 1px #ddf1ff; border-collapse:collapse;}
        .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
        .info{width: 32%; float: left;}
        .info.provider-info{margin:0 2%;}
        .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
        .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
        .info-bx:after{margin-bottom:20px;}
        .detail-table th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
        .detail-table td{padding:8px 15px; font-size:10px;}
          span.agreement_date, .id_number, .id_exp_date {
              margin-left: 35px;
          }
         .info-bx{
            display: flex;
            width: 100%;
            justify-content: space-between;
            flex-wrap: wrap;
          }
          .info{background: #f1f9ff;}
          .invoice-box h4 {line-height: 5px; margin-bottom: 0;}
           .term-heading, .customer-heading {text-align: center; margin-top: 30px; font-size:20px}
          .hidden{visibility:hidden;}
          .customer-accnt .add-card{
            width: 32%;
            border: solid 1px #d7eeff !important;
            background: #f1f9ff;
            
          }
          .customer-accnt .witness-signature{
            float:right;
          }
          .customer-accnt .customer-signature{
            float:left;
          }
          .customer-accnt .image-area{
            margin-top: 10px;
          }
          .sing_box{
            padding: 10px 10px 10px 10px !important;         
          }
        </style>
     </head>
     <body>
       <div class="invoice-box">
         <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>

         <h1 class="term-heading">Terms of Agreement for Health Partner, Inc</h1>
         <div class="terms-condition-agreement">
           <div>Date <strong><u>${planDetails.current_date}</u></strong></div>
           <div>Location <u>${planDetails.data.providerDetails.address1 + ' ' + planDetails.data.providerDetails.address2 + ','}</u></div>
           <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>${planDetails.data.providerDetails.city + ', ' + planDetails.data.providerDetails.state_name + '-' + planDetails.data.providerDetails.zip_code}</u></div>
           <p>On or before Date, for value received, the undersigned <strong><u>${planDetails.data.customerDetails.first_name + ' ' + planDetails.data.customerDetails.middle_name + ' ' + planDetails.data.customerDetails.last_name}</u></strong> (the "Borrower") promises to pay to the order of HEALTH PARTNER INC. (the "Holder"), in the manner and at the place provided below, the principal sum of <strong><u>${'$' + parseFloat(planDetails.data.loan_amount).toFixed(2)}</u></strong>.</p>

           <h4><strong>1. PAYMENT :</strong></h4>
           <p>All payments of principal and interest under this note will be made in lawful money of the United States, without offset, deduction, or counterclaim, by wire transfer of immediately available funds to an account designated by the Holder in writing at least 7 days after the effective date of this note or, if this designation is not made, by check mailed to the Holder at 5720 Creedmoor Road, Suite 103, Raleigh, North Carolina, 27612, or at such other place as the Holder may designate in writing.</p>

           <h4><strong>2. INTEREST :</strong></h4>
           <p>Interest on the unpaid principal balance of this note is payable from the date of this note until this note is paid in full, at the rate of <strong><u>${planDetails.data.planEst.interest_rate + '%'}</u></strong> per year, or the maximum amount allowed by applicable law, whichever is less. Accrued interest will be computed on the basis of a 365-day or 366-day year, as the case may be, based on the actual number of days elapsed in the period in which it accrues.</p>

           <h4><strong>3. PREPAYMENT :</strong></h4>
           <p>The Borrower may prepay this note, in whole or in part, at any time before maturity without penalty or premium. Any partial prepayment will be credited first to accrued interest, then to principal. No prepayment extends or postpones the maturity date of this note.</p>

           <h4><strong>4. EVENTS OF DEFAULT :</strong></h4>
           <p>Each of the following constitutes an <strong>"Event of Default"</strong> under this note:</p>
           <ul>
              <li>The Borrower's failure to make any payment when due under the terms of this note, including the lump-sum payment due under this note at its maturity;</li>
              <li>The filing of any voluntary or involuntary petition in bankruptcy by or regarding the Borrower or the initiation of any proceeding under bankruptcy or insolvency laws against the Borrower;</li>
              <li>An assignment made by the Borrower for the benefit of creditors;</li>
              <li>The appointment of a receiver, custodian, trustee, or similar party to take possession of the Borrower's assets or property; or</li>
              <li>The death of the Borrower.</li>
           </ul>

           <h4><strong>5. ACCELERATION; REMEDIES ON DEFAULT :</strong></h4>
           <p>If any Event of Default occurs, all principal and other amounts owed under this note will become immediately due and payable without any action by the Holder, the Borrower, or any other person. The Holder, in addition to any rights and remedies available to the Holder under this note, may, in its sole discretion, pursue any legal or equitable remedies available to it under applicable law or in equity, including taking any of the following actions:</p>
           <ul>
              <li>Personally, or by agents or attorneys (in compliance with applicable law), take immediate possession of the collateral. To that end, the Holder may pursue the collateral where it may be found, and enter the Borrower's premises, with or without notice, demand, process of law, or legal procedure if this can be done without breach of the peace. If the premises on which any part of the collateral is located are not under the Borrower's direct control, the Borrower will exercise its best efforts to ensure that the Holder is promptly provided right of access to those premises. To the extent that the Borrower's consent would otherwise be required before a right of access could be granted, the Borrower hereby irrevocably grants that consent;</li>
              <li>Require the Borrower to assemble the collateral and make it available to the Holder at a place to be designated by the Holder that is reasonably convenient to both parties (it being acknowledged that the Borrower's premises are reasonably convenient to the Borrower);</li>
              <li>Sell, lease, or dispose of the collateral or any part of it in any manner permitted by applicable law or by contract; and</li>
              <li>Exercise all rights and remedies of a secured party under applicable law.</li>
           </ul>

           <h4><strong>6. WAIVER OF PRESENTMENT; DEMAND :</strong></h4>
           <p>The Borrower hereby waives presentment, demand, notice of dishonor, notice of default or delinquency, notice of protest and nonpayment, notice of costs, expenses or losses and interest on those, notice of interest on interest and late charges, and diligence in taking any action to collect any sums owing under this note, including (to the extent permitted by law) waiving the pleading of any statute of limitations as a defense to any demand against the undersigned. Acceptance by the Holder or any other holder of this note of any payment differing from the designated lump-sum payment listed above does not relieve the undersigned of the obligation to honor the requirements of this note.</p>

           <h4><strong>7. GOVERNING LAW :</strong></h4>
           <ul>
              <li><strong>Choice of La.</strong> The laws of the state of North Carolina govern this note (without giving effect to its conflicts of law principles).</li>
              <li><strong>Choice of Forum.</strong> Both parties consent to the personal jurisdiction of the state and federal courts in Wake, North Carolina.</li>
           </ul>

           <h4><strong>8. COLLECTION COSTS AND ATTORNEYS' FEES :</strong></h4>
           <p>The Borrower shall pay all expenses of the collection of indebtedness evidenced by this note, including reasonable attorneys' fees and court costs in addition to other amounts due.</p>

           <h4><strong>9. ASSIGNMENT AND DELEGATION :</strong></h4>
           <ul>
              <li><strong>No Assignment.</strong> The Borrower may not assign any of its rights under this note. All voluntary assignments of rights are limited by this subsection.</li>
              <li><strong>No Delegation.</strong> The Borrower may not delegate any performance under this note.</li>
              <li><strong>Enforceability of an Assignment or Delegation.</strong> If a purported assignment or purported delegation is made in violation of this section, it is void.</li>
           </ul>

           <h4><strong>10. SEVERABILITY :</strong></h4>
           <p>If any one or more of the provisions contained in this note is, for any reason, held to be invalid, illegal, or unenforceable in any respect, that invalidity, illegality, or unenforceability will not affect any other provisions of this note, but this note will be construed as if those invalid, illegal, or unenforceable provisions had never been contained in it, unless the deletion of those provisions would result in such a material change so as to cause completion of the transactions contemplated by this note to be unreasonable.</p>

           <h4><strong>11. NOTICES :</strong></h4>
           <ul>
              <li>Writing; Permitted Delivery Methods. Each party giving or making any notice, request, demand, or other communication required or permitted by this note shall give that notice in writing and use one of the following types of delivery, each of which is a writing for purposes of this note: personal delivery, mail (registered or certified mail, postage prepaid, return-receipt requested), nationally recognized overnight courier (fees prepaid), facsimile, or email.</li>
              <li>Addresses. A party shall address notices under this section to a party at the following addresses:
                 <p><strong>If to the Borrower :</strong></p>
                 <p><u>${planDetails.data.customerDetails.address1 + ' ' + planDetails.data.customerDetails.address2},</u></p>
                  <p><u>${planDetails.data.customerDetails.City + ', ' + planDetails.data.customerDetails.state_name + '-' + planDetails.data.customerDetails.zip_code}</u></p>
                  <p><u><strong>Phone:</strong> ${planDetails.data.customerDetails.peimary_phone}</u></p>

                 <strong>If to the Holder:</strong>
                 <p>Health Partner Inc.</p>
                 <p>5720 Creedmoor Road, Suite 103</p>
                 <p>Raleigh, North Carolina 27612</p>

              </li>
              <li><strong>Effectiveness.</strong> A notice is effective only if the party giving notice complies with subsections (a) and</li>
              <li>and if the recipient receives the notice</li>
           </ul>

           <h4><strong>12. WAIVER :</strong></h4>
           <p>No waiver of a breach, failure of any condition, or any right or remedy contained in or granted by the provisions of this note will be effective unless it is in writing and signed by the party waiving the breach, failure, right, or remedy. No waiver of any breach, failure, right, or remedy will be deemed a waiver of any other breach, failure, right, or remedy, whether or not similar, and no waiver will constitute a continuing waiver, unless the writing so specifies.</p>

           <h4><strong>13. HEADINGS :</strong></h4>
           <p>The descriptive headings of the sections and subsections of this note are for convenience only, and do not affect this note's construction or interpretation.</p>

           
        </div>
          <div style="page-break-before:always">&nbsp;</div>
         <h1 class="justify-center">Customer Payment Plan</h1>
       <div class="info-bx">
          
          <div class="info customer-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                   <th colSpan="2">Customer Information</th>
                </tr>
                <tr>
                   <td><strong>Account No:</strong> ${planDetails.data.customerDetails.patient_ac}</td>
                </tr>
                <tr>
                   <td><strong>Application No:</strong> ${planDetails.data.customerDetails.application_no}</td>
                </tr>
                <tr>
                   <td><strong>Name:</strong> ${planDetails.data.customerDetails.first_name + ' ' + planDetails.data.customerDetails.middle_name + ' ' + planDetails.data.customerDetails.last_name}</td>
                </tr>
                <tr>
                   <td><strong>Address:</strong> ${planDetails.data.customerDetails.address1 + ' ' + planDetails.data.customerDetails.address2 + ' ' + planDetails.data.customerDetails.City + ' ' + planDetails.data.customerDetails.state_name}</td>
                </tr>
                <tr>
                   <td><strong>Phone:</strong> ${planDetails.data.customerDetails.peimary_phone}</td>
                </tr>
                <tr>
                  <td><span class="hidden">####</span></td>
                </tr>
                <tr>
                  <td><span class="hidden">####</span></td>
                </tr>
             </table>
          </div>

          <div class="info provider-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                   <th colSpan="2">Provider Information</th>
                </tr>
                <tr>
                   <td><strong>Account No:</strong> ${planDetails.data.providerDetails.provider_ac}</td>
                </tr>
                <tr>
                   <td><strong>Name:</strong> ${planDetails.data.providerDetails.name}</td>
                </tr>
                <tr>
                   <td><strong>Address:</strong> ${planDetails.data.providerDetails.address1 + ' ' + planDetails.data.providerDetails.address2 + ' ' + planDetails.data.providerDetails.city + ' ' + planDetails.data.providerDetails.state_name}</td>
                </tr>
                <tr>
                   <td><strong>Phone:</strong> ${planDetails.data.providerDetails.primary_phone}</td>
                </tr>
                <tr>
                   <td><strong>Doctor:</strong> -</td>
                </tr>
                <tr>
                   <td><span class="hidden">####</span></td>
                </tr>
                <tr>
                   <td><span class="hidden">####</span></td>
                </tr>
             </table>
          </div>

          <div class="info loan-info">
             <table cellpadding="0" cellspacing="0" class="blue-tble">
                <tr>
                   <th colSpan="2">Loan Information</th>
                </tr>
                <tr>
                   <td><strong>Approved Amount:</strong> ${'$' + parseFloat(planDetails.data.customerDetails.approve_amount).toFixed(2)}</td>
                </tr>
                <tr>
                   <td><strong>Principal Amount:</strong> ${'$' + parseFloat(planDetails.data.loan_amount).toFixed(2)}</td>
                </tr>
                <tr>
                   <td><strong>Remaining Amount:</strong> ${'$' + parseFloat(planDetails.data.customerDetails.remaining_amount).toFixed(2)}</td>
                </tr>
                <tr>
                   <td><strong>Loan Amount:</strong> ${'$' + parseFloat(planDetails.data.planEst.totalAmount).toFixed(2)}</td>
                </tr>
                <tr>
                   <td><strong>APR:</strong> ${planDetails.data.planEst.interest_rate + '%'}</td>
                </tr>
                <tr>
                   <td><strong>Term Month:</strong> ${planDetails.data.planEst.term_month}</td>
                </tr>
                <tr>
                   <td><strong>Procedure Date:</strong> ${moment(planDetails.data.procedure_date).format('MM/DD/YYYY')}</td>
                </tr>
             </table>
          </div>
       
       </div>
       
       <div class="detail-bx">
          <h1 class="justify-center">Plan Details</h1>
          <div class="detail-table">
             <table cellpadding="0" cellspacing="0">
                <tr>
                   <th>SN#</th>
                   <th>Date</th>
                   <th>Amount</th>
                </tr>
                ${html}
             </table>
          </div>
       </div>
       <p class="note">Note:- This payment plan does not reflect possible charges occurring from late fees, transaction fees, or financial charges. Interest will be charged to your account at a variable APR of 26.99% from purchase date if the purchase amount is not paid in full within the loan term or if you make a late payment. To avoid late fees, you must make your Monthly Payments by the due date each month.</p>

       
       <h4><strong>BORROWER DETAILS :</strong></h4>
       <p><span class="borrower_name"><strong>Name of Borrower:</strong> ${(planDetails.data.name_of_borrower) ? planDetails.data.name_of_borrower : '_______________________________________________________________'}</span> <span class="agreement_date"><strong>Agreement Date:</strong> ${(planDetails.data.agreement_date) ? moment(planDetails.data.agreement_date).format('MM/DD/YYYY HH:mm:ss') : '_____________________'}</span></p>

       <h4><strong>ID PROOF DOCUMENT DETAILS :</strong></h4>
       <p><span class="id_name"><strong>ID Type:</strong> ${(planDetails.data.id_name) ? planDetails.data.id_name : '_____________________'}</span> <span class="id_number"><strong>ID Number:</strong> ${(planDetails.data.id_number) ? planDetails.data.id_number : '_____________________'}</span><span class="id_exp_date"><strong>ID Expire Date:</strong> ${(planDetails.data.expiry_date) ? moment(planDetails.data.expiry_date).format('MM/DD/YYYY') : '_____________________'}</span></p>
       
         <div class="customer-accnt">
            <div class="add-card sing_box customer-signature">
               <b>Customer Signature</b>
               <div class="image-area"><img src="${planDetails.data.customerSignature}"></div>
            </div>
            <div class="add-card sing_box witness-signature">
               <b>Witness Signature</b>
               <div class="image-area"><img src="${planDetails.data.witnessSignature}"></div>
            </div>
         </div>
        </div>
     </body>
  </html>
      `;
};