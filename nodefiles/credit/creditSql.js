const { resolve } = require('path');
const { promises } = require('fs');

const capitalize = (str) => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}

var DirectCreditSql = function (con, data, callback) {
    const md5 = require('md5');
    const aes256 = require('aes256');
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    let password = md5(data.password);
    var ase256key = 'ramesh';
    data.provider_id = (data.provider_id !== undefined) ? data.provider_id : null;

    //var encrypted = aes256.encrypt(ase256key, plaintext);
    //var decrypted = aes256.decrypt(key, encrypted);
    //return false;
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }


        existCustomer(con, data, function (result_id) {
            data.patient_id = result_id;

            if (result_id == '') {

                con.query('INSERT INTO patient SET '
                    + 'f_name=?, '
                    + 'patient_ac=((SELECT COALESCE(MAX(patient_ac),11111)+1 AS dt FROM patient as p)), '
                    + 'm_name=?, '
                    + 'l_name=?, '
                    + 'other_f_name=?, '
                    + 'other_m_name=?, '
                    + 'other_l_name=?, '
                    + 'gender=?, '
                    + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
                    + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
                    + 'employment_status=?, '
                    + 'employment_type=?, '
                    + 'annual_income=?, '
                    + 'employer_since=?, '
                    + 'employer_name=?, '
                    + 'employer_phone=?, '
                    + 'employer_email=?, '
                    + 'bank_name=?, '
                    + 'bank_address=?, '
                    + 'rounting_no=?, '
                    + 'account_number=?, '
                    + 'account_name=?, '
                    + 'account_type=?, '
                    + 'withdrawal_date=?, '
                    + 'peimary_phone=?, '
                    + 'alternative_phone=?, '
                    + 'email=?, '
                    + 'secondary_email=?, '
                    + 'status=?, '
                    + 'date_created=?, '
                    + 'date_modified=?, '
                    + 'created_by=?, '
                    + 'modified_by=?',
                    [
                        capitalize(data.first_name),
                        capitalize(data.middle_name),
                        capitalize(data.last_name),
                        capitalize(data.alias_first_name),
                        capitalize(data.alias_middle_name),
                        capitalize(data.alias_last_name),
                        (data.gender == 'M') ? 'M' : 'F',
                        data.dob,
                        data.ssn,
                        (data.employed == 1) ? 1 : 0,
                        data.employment_type,
                        data.annual_income,
                        data.employed_since,
                        capitalize(data.employer_name),
                        data.employer_phone,
                        data.employer_email.toLowerCase(),
                        capitalize(data.bank_name),
                        capitalize(data.bank_address),
                        data.rounting_no,
                        data.bank_ac,
                        capitalize(data.account_name),
                        data.account_type,
                        data.withdrawal_date,
                        data.phone_1,
                        data.phone_2,
                        data.email.toLowerCase(),
                        data.secondary_email.toLowerCase(),
                        data.status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                    ]
                    , function (err, resultmain) {
                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                                return callback(obj);
                            });
                        } else {
                            // insert user details
                            con.query('INSERT INTO user SET '
                                + 'user_type_role_id=?, '
                                + 'patient_id=?, '
                                + 'username=?, '
                                + 'password=?, '
                                + 'email_id=?, '
                                + 'f_name=?, '
                                + 'm_name=?, '
                                + 'l_name=?, '
                                + 'phone=?, '
                                + 'status=?, '
                                + 'date_created=?, '
                                + 'date_modified=?, '
                                + 'created_by=?, '
                                + 'modified_by=?',
                                [
                                    12,
                                    resultmain.insertId,
                                    data.username,
                                    password,
                                    data.email.toLowerCase(),
                                    capitalize(data.first_name),
                                    capitalize(data.middle_name),
                                    capitalize(data.last_name),
                                    data.phone_1,
                                    data.status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                ]
                                , function (err, resultuser) {

                                    var values = [];
                                    var billing = data.location.filter(x => x.billing_address == 1);
                                    if (billing.length < 1) {
                                        values.push([
                                            resultmain.insertId,
                                            data.billing_state,
                                            capitalize(data.billing_address1),
                                            capitalize(data.billing_address2),
                                            capitalize(data.billing_city),
                                            data.billing_zip_code,
                                            data.billing_how_long,
                                            0,
                                            1,
                                            0,
                                            data.billing_phone_no,
                                            data.status,
                                            current_date,
                                            current_date,
                                            current_user_id,
                                            current_user_id
                                        ]);
                                    }

                                    data.location.forEach(function (element, idx) {
                                        values.push([
                                            resultmain.insertId,
                                            element.state,
                                            capitalize(element.address1),
                                            capitalize(element.address2),
                                            capitalize(element.city),
                                            element.zip_code,
                                            element.how_long,
                                            element.primary_address,
                                            element.billing_address,
                                            (element.billing_address) ? 1 : 0,
                                            element.phone_no,
                                            data.status,
                                            current_date,
                                            current_date,
                                            current_user_id,
                                            current_user_id
                                        ]);
                                    });

                                    con.query('INSERT INTO patient_address ('
                                        + 'patient_id, '
                                        + 'state_id, '
                                        + 'address1, '
                                        + 'address2, '
                                        + 'City, '
                                        + 'zip_code, '
                                        + 'address_time_period, '
                                        + 'primary_address, '
                                        + 'billing_address, '
                                        + 'same_billing_flag, '
                                        + 'phone_no, '
                                        + 'status, '
                                        + 'date_created, '
                                        + 'date_modified, '
                                        + 'created_by, '
                                        + 'modified_by) VALUES ?',
                                        [
                                            values
                                        ]
                                        , function (err, resultuser) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    return callback(obj);
                                                });
                                            } else {
                                                con.query('INSERT INTO credit_applications SET '
                                                    + 'patient_id=?, '
                                                    + 'application_no=((SELECT COALESCE(MAX(application_no),11111)+1 AS dt FROM credit_applications as m)), '
                                                    + 'amount=?, '
                                                    + 'status=?, '
                                                    + 'date_created=?, '
                                                    + 'date_modified=?, '
                                                    + 'created_by=?, '
                                                    + 'modified_by=?',
                                                    [
                                                        resultmain.insertId,
                                                        data.amount,
                                                        0,
                                                        current_date,
                                                        current_date,
                                                        current_user_id,
                                                        current_user_id
                                                    ]
                                                    , function (err, application) {
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.";
                                                                return callback(obj);
                                                            });
                                                        } else {
                                                            con.commit(function (err) {
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Something wrong please try again.";
                                                                        return callback(obj);
                                                                    });
                                                                } else {
                                                                    var sql = "SELECT value as username, (SELECT value FROM `master_data_values` WHERE md_id=?) as password, (SELECT value FROM `master_data_values` WHERE md_id=?) as subscriberCode, (SELECT value FROM `master_data_values` WHERE md_id=?) as api_key, (SELECT value FROM `master_data_values` WHERE md_id=?) as secret_key  FROM `master_data_values` WHERE md_id=?";
                                                                    con.query(sql, ['Experian Password', 'Experian Subscriber Code', 'Experian API Key', 'Experian SECRET Key', 'Experian Username'], function (error, values) {
                                                                        if (error) {
                                                                            obj.status = 0;
                                                                            obj.applicationId = application.insertId;
                                                                            obj.message = "Application submitted for approval. But Experian details not found.";
                                                                            return callback(obj);
                                                                        } else {

                                                                            var sql = "UPDATE access_request SET profile_created = 1 WHERE id = " + data.access_request_id + " AND access_flag = 1";
                                                                            con.query(sql, function (error, updateVal) {
                                                                                if (error) {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Something wrong with access_request table";
                                                                                    return callback(obj);
                                                                                } else {
                                                                                    obj.status = 1;
                                                                                    obj.applicationId = application.insertId;
                                                                                    obj.experian = values;
                                                                                    obj.message = "Application submitted for approval";
                                                                                    return callback(obj);
                                                                                }
                                                                            });

                                                                            /*obj.status = 1;
                                                                            obj.applicationId = application.insertId;
                                                                            obj.experian = values;
                                                                            obj.message = "Application submitted for approval";
                                                                            return callback(obj);*/
                                                                        }
                                                                    });
                                                                }
                                                            })
                                                        }
                                                    });
                                            }

                                        });

                                });
                        }

                    });

            } else {
                obj.status = 0;
                obj.message = "Customer already exist";
                return callback(obj);
            }
        })

    });
}

//////////////
/////////////
//Experian login detail
var getExperianLoginDetail = async (con, callback) => {
    var obj = {};
    return new Promise((resolve, reject) => {
        var ExSql = "SELECT value as username, (SELECT value FROM `master_data_values` WHERE md_id=?) as password, (SELECT value FROM `master_data_values` WHERE md_id=?) as subscriberCode, (SELECT value FROM `master_data_values` WHERE md_id=?) as api_key, (SELECT value FROM `master_data_values` WHERE md_id=?) as secret_key  FROM `master_data_values` WHERE md_id=?";
        con.query(ExSql, ['Experian Password', 'Experian Subscriber Code', 'Experian API Key', 'Experian SECRET Key', 'Experian Username'], function (error, values) {
            if (error) {
                obj.status = 0;
                obj.message = "We are unable to process your request at this time. Please try again later.";
                callback(obj);
            } else {
                obj.status = 1;
                obj.experian = values;
                obj.message = "Application submitted for approval";
                callback(obj);
            }
            resolve();
        });
    })
}

exports.addpatientDetails = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    data.provider_id = (data.provider_id !== undefined) ? data.provider_id : null;
    return new Promise((resolve, reject) => {
        con.query('INSERT INTO patient SET '
            + 'f_name=?, '
            + 'patient_ac=((SELECT COALESCE(MAX(patient_ac),11111)+1 AS dt FROM patient as p)), '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'other_f_name=?, '
            + 'other_m_name=?, '
            + 'other_l_name=?, '
            + 'gender=?, '
            + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'employment_status=?, '
            + 'employment_type=?, '
            + 'annual_income=?, '
            + 'employer_since=?, '
            + 'employer_name=?, '
            + 'employer_phone=?, '
            + 'employer_email=?, '
            /*+ 'bank_name=?, '
            + 'bank_address=?, '
            + 'rounting_no=?, '
            + 'account_number=?, '
            + 'account_name=?, '
            + 'account_type=?, '*/
            + 'withdrawal_date=?, '
            + 'peimary_phone=?, '
            + 'alternative_phone=?, '
            + 'email=?, '
            + 'secondary_email=?, '
            //+ 'co_signer=?, '
            //+ 'relationship=?, '
            + 'status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                (coSigner == 1) ? capitalize(data.co_first_name) : capitalize(data.first_name),
                (coSigner == 1) ? capitalize(data.co_middle_name) : capitalize(data.middle_name),
                (coSigner == 1) ? capitalize(data.co_last_name) : capitalize(data.last_name),
                (coSigner == 1) ? '' : capitalize(data.alias_first_name),
                (coSigner == 1) ? '' : capitalize(data.alias_middle_name),
                (coSigner == 1) ? '' : capitalize(data.alias_last_name),
                (coSigner == 1) ? (data.co_gender == 'M') ? 'M' : 'F' : (data.gender == 'M') ? 'M' : 'F',
                (coSigner == 1) ? data.co_dob : data.dob,
                (coSigner == 1) ? data.co_ssn : data.ssn,
                (coSigner == 1) ? (data.co_employed == 1) ? 1 : 0 : (data.employed == 1) ? 1 : 0,
                (coSigner == 1) ? data.co_employment_type : data.employment_type,
                (coSigner == 1) ? data.co_annual_income : data.annual_income,
                (coSigner == 1) ? data.co_employed_since : data.employed_since,
                (coSigner == 1) ? capitalize(data.co_employer_name) : capitalize(data.employer_name),
                (coSigner == 1) ? data.co_employer_phone : data.employer_phone,
                (coSigner == 1) ? data.co_employer_email.toLowerCase() : data.employer_email.toLowerCase(),
                /*capitalize(data.bank_name),
                capitalize(data.bank_address),
                data.rounting_no,
                data.bank_ac,
                capitalize(data.account_name),
                data.account_type,*/
                (coSigner == 1) ? data.co_withdrawal_date : data.withdrawal_date,
                (coSigner == 1) ? data.co_phone_1 : data.phone_1,
                (coSigner == 1) ? data.co_phone_2 : data.phone_2,
                (coSigner == 1) ? data.co_email.toLowerCase() : data.email.toLowerCase(),
                (coSigner == 1) ? data.co_secondary_email.toLowerCase() : data.secondary_email.toLowerCase(),
                //data.co_signer,
                //(data.co_signer == 2) ? data.sd_relationship : null,
                data.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, resultmain) {

                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.state = 1;
                    obj.mainPatientId = resultmain.insertId;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addPatientUser = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    const md5 = require('md5');
    let password = (coSigner == 1) ? md5(data.co_password) : md5(data.password);
    return new Promise((resolve, reject) => {
        con.query('INSERT INTO user SET '
            + 'user_type_role_id=?, '
            + 'patient_id=?, '
            + 'username=?, '
            + 'password=?, '
            + 'email_id=?, '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'phone=?, '
            + 'status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                12,
                (coSigner == 1) ? data.co_patient_id : data.patient_id,
                (coSigner == 1) ? data.co_username : data.username,
                password,
                (coSigner == 1) ? data.co_email.toLowerCase() : data.email.toLowerCase(),
                (coSigner == 1) ? capitalize(data.co_first_name) : capitalize(data.first_name),
                (coSigner == 1) ? capitalize(data.co_middle_name) : capitalize(data.middle_name),
                (coSigner == 1) ? capitalize(data.co_last_name) : capitalize(data.last_name),
                (coSigner == 1) ? data.co_phone_1 : data.phone_1,
                data.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addPatientBank = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        let bankDetails = [];
        if (coSigner == 1) {
            data.co_bank.forEach(function (element, idx) {
                if (element.co_bank_name != '') {
                    bankDetails.push([
                        data.co_patient_id,
                        capitalize(element.co_bank_name),
                        element.co_bank_ac,
                        capitalize(element.co_bank_address),
                        element.co_rounting_no,
                        capitalize(element.co_account_name),
                        element.co_account_type,
                        element.co_primary_bank,
                        data.status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                    ]);
                }
            });
            con.query('INSERT INTO patient_bank_details ('
                + 'co_signer_id, '
                + 'bank_name, '
                + 'bank_ac, '
                + 'bank_address, '
                + 'rounting_no, '
                + 'account_name, '
                + 'account_type, '
                + 'primary_bank, '
                + 'status, '
                + 'date_created, '
                + 'date_modified, '
                + 'created_by, '
                + 'modified_by) VALUES ?',
                [
                    bankDetails
                ]
                , function (err, resultuser) {
                    if (err && bankDetails.length > 0) {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        callback(obj);
                    } else {
                        obj.status = 1;
                        callback(obj);
                    }
                    resolve();
                })
        } else {
            data.bank.forEach(function (element, idx) {
                if (element.bank_name != '') {
                    bankDetails.push([
                        data.patient_id,
                        capitalize(element.bank_name),
                        element.bank_ac,
                        capitalize(element.bank_address),
                        element.rounting_no,
                        capitalize(element.account_name),
                        element.account_type,
                        element.primary_bank,
                        data.status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                    ]);
                }
            });
            con.query('INSERT INTO patient_bank_details ('
                + 'patient_id, '
                + 'bank_name, '
                + 'bank_ac, '
                + 'bank_address, '
                + 'rounting_no, '
                + 'account_name, '
                + 'account_type, '
                + 'primary_bank, '
                + 'status, '
                + 'date_created, '
                + 'date_modified, '
                + 'created_by, '
                + 'modified_by) VALUES ?',
                [
                    bankDetails
                ]
                , function (err, resultuser) {
                    if (err && bankDetails.length > 0) {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        callback(obj);
                    } else {
                        obj.status = 1;
                        callback(obj);
                    }
                    resolve();
                })
        }

    })

}

exports.addPatientRelation = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        con.query('INSERT INTO patient_provider SET '
            + 'patient_id=?, '
            + 'provider_id=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                (coSigner == 1) ? data.co_patient_id : data.patient_id,
                data.provider_id,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, bridgeResult) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addpatientAddress = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        var values = [];
        if (coSigner == 1) {
            var billing = data.co_location.filter(x => x.co_billing_address == 1);
            if (billing.length < 1) {
                values.push([
                    data.co_patient_id,
                    data.co_billing_state,
                    capitalize(data.co_billing_address1),
                    capitalize(data.co_billing_address2),
                    capitalize(data.co_billing_city),
                    data.co_billing_zip_code,
                    data.co_billing_how_long,
                    0,
                    1,
                    0,
                    data.co_billing_phone_no,
                    data.status,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            }

            data.co_location.forEach(function (element, idx) {
                values.push([
                    data.co_patient_id,
                    element.co_state,
                    capitalize(element.co_address1),
                    capitalize(element.co_address2),
                    capitalize(element.co_city),
                    element.co_zip_code,
                    element.co_how_long,
                    element.co_primary_address,
                    element.co_billing_address,
                    (element.co_billing_address) ? 1 : 0,
                    element.co_phone_no,
                    data.status,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            });
            con.query('INSERT INTO patient_address ('
                + 'co_signer_id, '
                + 'state_id, '
                + 'address1, '
                + 'address2, '
                + 'City, '
                + 'zip_code, '
                + 'address_time_period, '
                + 'primary_address, '
                + 'billing_address, '
                + 'same_billing_flag, '
                + 'phone_no, '
                + 'status, '
                + 'date_created, '
                + 'date_modified, '
                + 'created_by, '
                + 'modified_by) VALUES ?',
                [
                    values
                ]
                , function (err, resultuser) {
                    if (err) {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        callback(obj);
                    } else {
                        obj.status = 1;
                        callback(obj);
                    }
                    resolve()
                })
        } else {
            var billing = data.location.filter(x => x.billing_address == 1);
            if (billing.length < 1) {
                values.push([
                    data.patient_id,
                    data.billing_state,
                    capitalize(data.billing_address1),
                    capitalize(data.billing_address2),
                    capitalize(data.billing_city),
                    data.billing_zip_code,
                    data.billing_how_long,
                    0,
                    1,
                    0,
                    data.billing_phone_no,
                    data.status,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            }

            data.location.forEach(function (element, idx) {
                values.push([
                    data.patient_id,
                    element.state,
                    capitalize(element.address1),
                    capitalize(element.address2),
                    capitalize(element.city),
                    element.zip_code,
                    element.how_long,
                    element.primary_address,
                    element.billing_address,
                    (element.billing_address) ? 1 : 0,
                    element.phone_no,
                    data.status,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            });
            con.query('INSERT INTO patient_address ('
                + 'patient_id, '
                + 'state_id, '
                + 'address1, '
                + 'address2, '
                + 'City, '
                + 'zip_code, '
                + 'address_time_period, '
                + 'primary_address, '
                + 'billing_address, '
                + 'same_billing_flag, '
                + 'phone_no, '
                + 'status, '
                + 'date_created, '
                + 'date_modified, '
                + 'created_by, '
                + 'modified_by) VALUES ?',
                [
                    values
                ]
                , function (err, resultuser) {
                    if (err) {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        callback(obj);
                    } else {
                        obj.status = 1;
                        callback(obj);
                    }
                    resolve()
                })
        }

    })
}

exports.addCreditApp = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        let sql = '';
        if (coSigner == 1) {
            sql = 'INSERT INTO credit_applications SET '
                + 'co_signer_id=?, '
                + 'application_no=((SELECT COALESCE(MAX(application_no),11111)+1 AS dt FROM credit_applications as m)), '
                + 'amount=?, '
                + 'status=?, '
                + 'co_signer=?,'
                //+ 'co_signer_is_primary=?,'
                + 'date_created=?, '
                + 'date_modified=?, '
                + 'created_by=?, '
                + 'modified_by=?';
        } else {
            sql = 'INSERT INTO credit_applications SET '
                + 'patient_id=?, '
                + 'application_no=((SELECT COALESCE(MAX(application_no),11111)+1 AS dt FROM credit_applications as m)), '
                + 'amount=?, '
                + 'status=?, '
                + 'co_signer=?,'
                //+ 'co_signer_is_primary=?,'
                + 'date_created=?, '
                + 'date_modified=?, '
                + 'created_by=?, '
                + 'modified_by=?';
        }
        con.query(sql,
            [
                (coSigner == 1) ? data.co_patient_id : data.patient_id,
                data.amount,
                0,
                (coSigner == 1) ? 1 : 0,
                //(coSigner == 1) ? data.is_co_primary : 0,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, application) {

                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    obj.applicationID = application.insertId;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addCreditRelation = async (con, data, appId, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        con.query('INSERT INTO credit_app_cosigner SET '
            + 'co_signer_id=?, '
            + 'application_id=?, '
            + 'primary_bill_pay_flag=?, '
            + 'relationship=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                data.co_signer_id,
                appId,
                (data.is_co_primary == 1) ? 1 : 0,
                data.co_relationship,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, application) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addSecondaryDetails = async (con, data, applicationId, patientId, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    return new Promise((resolve, reject) => {
        con.query('INSERT INTO credit_sec_details SET '
            + 'patient_id=?, '
            + 'applcation_id=?, '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'email=?, '
            + 'phone=?, '
            + 'relationship=?, '
            + 'address1=?, '
            + 'address2=?, '
            + 'city=?, '
            + 'state=?, '
            + 'country=?, '
            + 'zip_code=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                patientId,
                applicationId,
                capitalize(data.sd_first_name),
                (data.sd_middle_name != null && data.sd_middle_name != '') ? capitalize(data.sd_middle_name) : '',
                capitalize(data.sd_last_name),
                data.sd_email,
                data.sd_phone,
                data.sd_relationship,
                data.sd_address1,
                data.sd_address2,
                data.sd_city,
                data.sd_state,
                data.sd_country,
                data.sd_zip_code,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, resultmain) {

                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve()
            })
    })
}

exports.updatePatitentDetails = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    data.provider_id = (data.provider_id !== undefined) ? data.provider_id : null;
    return new Promise((resolve, reject) => {
        con.query('UPDATE patient SET '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'other_f_name=?, '
            + 'other_m_name=?, '
            + 'other_l_name=?, '
            + 'gender=?, '
            + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'employment_status=?, '
            + 'employment_type=?, '
            + 'annual_income=?, '
            + 'employer_since=?, '
            + 'employer_name=?, '
            + 'employer_phone=?, '
            + 'employer_email=?, '
            + 'withdrawal_date=?, '
            + 'peimary_phone=?, '
            + 'alternative_phone=?, '
            + 'email=?, '
            + 'secondary_email=?, '
            //+ 'co_signer=?, '
            //+ 'relationship=?, '
            + 'status=?, '
            + 'date_modified=?, '
            + 'modified_by=? WHERE patient_id=?',
            [
                (coSigner == 1) ? capitalize(data.co_first_name) : capitalize(data.first_name),
                (coSigner == 1) ? capitalize(data.co_middle_name) : capitalize(data.middle_name),
                (coSigner == 1) ? capitalize(data.co_last_name) : capitalize(data.last_name),
                (coSigner == 1) ? '' : capitalize(data.alias_first_name),
                (coSigner == 1) ? '' : capitalize(data.alias_middle_name),
                (coSigner == 1) ? '' : capitalize(data.alias_last_name),
                (coSigner == 1) ? (data.co_gender == 'M') ? 'M' : 'F' : (data.gender == 'M') ? 'M' : 'F',
                (coSigner == 1) ? data.co_dob : data.dob,
                (coSigner == 1) ? data.co_ssn : data.ssn,
                (coSigner == 1) ? (data.co_employed == 1) ? 1 : 0 : (data.employed == 1) ? 1 : 0,
                (coSigner == 1) ? data.co_employment_type : data.employment_type,
                (coSigner == 1) ? data.co_annual_income : data.annual_income,
                (coSigner == 1) ? data.co_employed_since : data.employed_since,
                (coSigner == 1) ? capitalize(data.co_employer_name) : capitalize(data.employer_name),
                (coSigner == 1) ? data.co_employer_phone : data.employer_phone,
                (coSigner == 1) ? data.co_employer_email.toLowerCase() : data.employer_email.toLowerCase(),
                (coSigner == 1) ? data.co_withdrawal_date : data.withdrawal_date,
                (coSigner == 1) ? data.co_phone_1 : data.phone_1,
                (coSigner == 1) ? data.co_phone_2 : data.phone_2,
                (coSigner == 1) ? data.co_email.toLowerCase() : data.email.toLowerCase(),
                (coSigner == 1) ? data.co_secondary_email.toLowerCase() : data.secondary_email.toLowerCase(),
                //data.co_signer,
                //(data.co_signer == 2) ? data.sd_relationship : null,
                data.status,
                current_date,
                current_user_id,
                (coSigner == 1) ? data.co_patient_id : data.patient_id
            ]
            , function (err, resultmain) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.state = 1;
                    obj.mainPatientId = (coSigner == 1) ? data.co_patient_id : data.patient_id;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.updatePatientUser = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    const md5 = require('md5');
    let password = (coSigner == 1) ? md5(data.co_password) : md5(data.password);
    return new Promise((resolve, reject) => {
        con.query('UPDATE user SET '
            + 'email_id=?, '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'phone=?, '
            + 'status=?, '
            + 'date_modified=?, '
            + 'modified_by=? WHERE patient_id=?',
            [
                (coSigner == 1) ? data.co_email.toLowerCase() : data.email.toLowerCase(),
                (coSigner == 1) ? capitalize(data.co_first_name) : capitalize(data.first_name),
                (coSigner == 1) ? capitalize(data.co_middle_name) : capitalize(data.middle_name),
                (coSigner == 1) ? capitalize(data.co_last_name) : capitalize(data.last_name),
                (coSigner == 1) ? data.co_phone_1 : data.phone_1,
                data.status,
                current_date,
                current_user_id,
                (coSigner == 1) ? data.co_patient_id : data.patient_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.updatePatientBank = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        let sql = '';
        if (coSigner == 1) {
            sql = 'UPDATE `patient_bank_details` SET status=? WHERE co_signer_id=?';
        } else {
            sql = 'UPDATE `patient_bank_details` SET status=? WHERE patient_id=?';
        }
        con.query(sql,
            [
                0,
                (coSigner) ? data.co_patient_id : data.patient_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                    resolve();
                } else {
                    let bankDetails = [];
                    if (coSigner == 1) {
                        data.co_bank.forEach(function (element, idx) {
                            if (element.co_bank_name != '') {
                                bankDetails.push([
                                    data.co_patient_id,
                                    capitalize(element.co_bank_name),
                                    element.co_bank_ac,
                                    capitalize(element.co_bank_address),
                                    element.co_rounting_no,
                                    capitalize(element.co_account_name),
                                    element.co_account_type,
                                    element.co_primary_bank,
                                    data.status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                ]);
                            }
                        });
                        con.query('INSERT INTO patient_bank_details ('
                            + 'co_signer_id, '
                            + 'bank_name, '
                            + 'bank_ac, '
                            + 'bank_address, '
                            + 'rounting_no, '
                            + 'account_name, '
                            + 'account_type, '
                            + 'primary_bank, '
                            + 'status, '
                            + 'date_created, '
                            + 'date_modified, '
                            + 'created_by, '
                            + 'modified_by) VALUES ?',
                            [
                                bankDetails
                            ]
                            , function (err, resultuser) {
                                if (err && bankDetails.length > 0) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    callback(obj);
                                } else {
                                    obj.status = 1;
                                    callback(obj);
                                }
                                resolve();
                            })
                    } else {
                        data.bank.forEach(function (element, idx) {
                            if (element.bank_name != '') {
                                bankDetails.push([
                                    data.patient_id,
                                    capitalize(element.bank_name),
                                    element.bank_ac,
                                    capitalize(element.bank_address),
                                    element.rounting_no,
                                    capitalize(element.account_name),
                                    element.account_type,
                                    element.primary_bank,
                                    data.status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                ]);
                            }
                        });
                        con.query('INSERT INTO patient_bank_details ('
                            + 'patient_id, '
                            + 'bank_name, '
                            + 'bank_ac, '
                            + 'bank_address, '
                            + 'rounting_no, '
                            + 'account_name, '
                            + 'account_type, '
                            + 'primary_bank, '
                            + 'status, '
                            + 'date_created, '
                            + 'date_modified, '
                            + 'created_by, '
                            + 'modified_by) VALUES ?',
                            [
                                bankDetails
                            ]
                            , function (err, resultuser) {
                                if (err && bankDetails.length > 0) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    callback(obj);
                                } else {
                                    obj.status = 1;
                                    callback(obj);
                                }
                                resolve();
                            })
                    }

                }
            })
    })

}

exports.updatepatientAddress = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        let sql = '';
        if (coSigner == 1) {
            sql = 'UPDATE `patient_address` SET status=? WHERE co_signer_id=?';
        } else {
            sql = 'UPDATE `patient_address` SET status=? WHERE patient_id=?';
        }
        con.query(sql,
            [
                0,
                (coSigner == 1) ? data.co_patient_id : data.patient_id
            ]
            , function (err, resultuser) {

                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                    resolve();
                } else {
                    var values = [];
                    if (coSigner == 1) {
                        var billing = data.co_location.filter(x => x.co_billing_address == 1);
                        if (billing.length < 1) {
                            values.push([
                                data.co_patient_id,
                                data.co_billing_state,
                                capitalize(data.co_billing_address1),
                                capitalize(data.co_billing_address2),
                                capitalize(data.co_billing_city),
                                data.co_billing_zip_code,
                                data.co_billing_how_long,
                                0,
                                1,
                                0,
                                data.co_billing_phone_no,
                                data.status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                            ]);
                        }

                        data.co_location.forEach(function (element, idx) {
                            values.push([
                                data.co_patient_id,
                                element.co_state,
                                capitalize(element.co_address1),
                                capitalize(element.co_address2),
                                capitalize(element.co_city),
                                element.co_zip_code,
                                element.co_how_long,
                                element.co_primary_address,
                                element.co_billing_address,
                                (element.co_billing_address) ? 1 : 0,
                                element.co_phone_no,
                                data.status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                            ]);
                        });
                        con.query('INSERT INTO patient_address ('
                            + 'co_signer_id, '
                            + 'state_id, '
                            + 'address1, '
                            + 'address2, '
                            + 'City, '
                            + 'zip_code, '
                            + 'address_time_period, '
                            + 'primary_address, '
                            + 'billing_address, '
                            + 'same_billing_flag, '
                            + 'phone_no, '
                            + 'status, '
                            + 'date_created, '
                            + 'date_modified, '
                            + 'created_by, '
                            + 'modified_by) VALUES ?',
                            [
                                values
                            ]
                            , function (err, resultuser) {
                                if (err) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    callback(obj);
                                } else {
                                    obj.status = 1;
                                    callback(obj);
                                }
                                resolve()
                            })
                    } else {
                        var billing = data.location.filter(x => x.billing_address == 1);
                        if (billing.length < 1) {
                            values.push([
                                data.patient_id,
                                data.billing_state,
                                capitalize(data.billing_address1),
                                capitalize(data.billing_address2),
                                capitalize(data.billing_city),
                                data.billing_zip_code,
                                data.billing_how_long,
                                0,
                                1,
                                0,
                                data.billing_phone_no,
                                data.status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                            ]);
                        }

                        data.location.forEach(function (element, idx) {
                            values.push([
                                data.patient_id,
                                element.state,
                                capitalize(element.address1),
                                capitalize(element.address2),
                                capitalize(element.city),
                                element.zip_code,
                                element.how_long,
                                element.primary_address,
                                element.billing_address,
                                (element.billing_address) ? 1 : 0,
                                element.phone_no,
                                data.status,
                                current_date,
                                current_date,
                                current_user_id,
                                current_user_id
                            ]);
                        });
                        con.query('INSERT INTO patient_address ('
                            + 'patient_id, '
                            + 'state_id, '
                            + 'address1, '
                            + 'address2, '
                            + 'City, '
                            + 'zip_code, '
                            + 'address_time_period, '
                            + 'primary_address, '
                            + 'billing_address, '
                            + 'same_billing_flag, '
                            + 'phone_no, '
                            + 'status, '
                            + 'date_created, '
                            + 'date_modified, '
                            + 'created_by, '
                            + 'modified_by) VALUES ?',
                            [
                                values
                            ]
                            , function (err, resultuser) {
                                if (err) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    callback(obj);
                                } else {
                                    obj.status = 1;
                                    callback(obj);
                                }
                                resolve()
                            })
                    }


                }
            })

    })
}

exports.checkAppRuning = async (con, data, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT credit_applications.application_id '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'LEFT JOIN payment_plan on payment_plan.application_id=credit_applications.application_id '
            + 'WHERE IF(payment_plan.plan_status IS NOT NULL, (patient.patient_id=? AND credit_applications.status != ? AND credit_applications.remaining_amount>? AND DATE_FORMAT(credit_applications.expiry_date, "%Y-%m-%d")>=DATE_FORMAT(now(), "%Y-%m-%d") AND payment_plan.plan_status NOT IN (0,7,8,11)), (patient.patient_id=? AND credit_applications.status != ? AND credit_applications.remaining_amount>? AND DATE_FORMAT(credit_applications.expiry_date, "%Y-%m-%d")>=DATE_FORMAT(now(), "%Y-%m-%d")))',
            [
                data.patient_id,
                0,
                0,
                data.patient_id,
                0,
                0,
            ]
            , function (err, planActive) {
                console.log(this.sql)
                console.log(planActive)
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = (planActive.length == 0) ? 1 : 0;
                    obj.message = (planActive.length == 0) ? '' : 'Another credit application under process.';
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.getAppDetails = async (con, appId, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,'
            + 'credit_applications.amount,credit_applications.override_amount,credit_applications.override_amount_cmt,credit_applications.score,approve_amount,remaining_amount,DATE_FORMAT(credit_applications.date_modified, "%m/%d/%Y") as authorization_date, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date,credit_applications.expiry_date_cmt,credit_applications.status as application_status,'
            + 'credit_applications.plan_flag,patient.alias_name,patient.patient_id,'
            + 'patient.profile_flag,patient.patient_ac,patient.f_name,patient.m_name,patient.l_name,patient.other_f_name,patient.other_m_name,patient.other_l_name,patient.status,'
            + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,patient.gender,patient.malling_address,patient.peimary_phone,patient.alternative_phone,patient.email,patient.secondary_email,patient.employment_status,patient.annual_income,patient.employer_name,DATE_FORMAT(patient.employer_since, "%m/%d/%Y") as employer_since,patient.employer_phone,patient.employer_email,patient.bank_name,patient.account_number,patient.bank_address,patient.rounting_no,patient.account_name,master_data_values.value as account_type, empType.value as emp_type_name, patient.employment_type,patient.withdrawal_date, '
            + 'user.username as username,user.email_id as user_eamil,user.phone as user_phone1,patient.alternative_phone as user_phone2, '
            + 'credit_applications_factor.score AS experian_status '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'LEFT JOIN credit_applications_factor ON credit_applications_factor.applcation_id=credit_applications.application_id '
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=patient.account_type '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=patient.employment_type '
            + 'WHERE '
            + 'application_id=?',
            [
                appId
            ]
            , function (err, planDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.planDetails = (planDetails) ? planDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getSecDetails = async (con, appId, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'credit_sec_details.f_name, credit_sec_details.m_name, credit_sec_details.l_name, credit_sec_details.email, credit_sec_details.phone, credit_sec_details.relationship, credit_sec_details.address1, credit_sec_details.address2, credit_sec_details.city, credit_sec_details.state, credit_sec_details.country, credit_sec_details.zip_code,states.name as state_name,countries.name as country_name, master_data_values.value as relationship_name '
            + 'FROM credit_sec_details '
            + 'INNER JOIN states ON states.state_id=credit_sec_details.state '
            + 'INNER JOIN countries ON countries.id=states.country_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_sec_details.relationship AND master_data_values.md_id=? '
            + 'WHERE credit_sec_details.applcation_id = ? ORDER BY credit_sec_details.id DESC LIMIT 1',
            [
                'Relationship', appId
            ]
            , function (err, secDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.secDetails = (secDetails.length > 0) ? secDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.updateSecondaryDetails = async (con, data, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    return new Promise((resolve, reject) => {
        con.query('SELECT id FROM credit_sec_details '
            + 'WHERE applcation_id=?',
            [
                data.app_id
            ]
            , function (err, resultcheck) {
                if (resultcheck.length == 0 && data.sd_first_name != '') {
                    con.query('INSERT INTO credit_sec_details SET '
                        + 'patient_id=?, '
                        + 'applcation_id=?, '
                        + 'f_name=?, '
                        + 'm_name=?, '
                        + 'l_name=?, '
                        + 'email=?, '
                        + 'phone=?, '
                        + 'relationship=?, '
                        + 'address1=?, '
                        + 'address2=?, '
                        + 'city=?, '
                        + 'state=?, '
                        + 'country=?, '
                        + 'zip_code=?, '
                        + 'date_created=?, '
                        + 'date_modified=?, '
                        + 'created_by=?, '
                        + 'modified_by=?',
                        [
                            data.patient_id,
                            data.app_id,
                            capitalize(data.sd_first_name),
                            (data.sd_middle_name != null && data.sd_middle_name != '') ? capitalize(data.sd_middle_name) : '',
                            capitalize(data.sd_last_name),
                            data.sd_email,
                            data.sd_phone,
                            data.sd_relationship,
                            data.sd_address1,
                            data.sd_address2,
                            data.sd_city,
                            data.sd_state,
                            data.sd_country,
                            data.sd_zip_code,
                            current_date,
                            current_date,
                            current_user_id,
                            current_user_id
                        ]
                        , function (err, resultmain) {

                            if (err) {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                                callback(obj);
                            } else {
                                obj.status = 1;
                                callback(obj);
                            }
                            resolve()
                        })
                } else {
                    con.query('UPDATE credit_sec_details SET '
                        + 'f_name=?, '
                        + 'm_name=?, '
                        + 'l_name=?, '
                        + 'email=?, '
                        + 'phone=?, '
                        + 'relationship=?, '
                        + 'address1=?, '
                        + 'address2=?, '
                        + 'city=?, '
                        + 'state=?, '
                        + 'country=?, '
                        + 'zip_code=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE applcation_id=?',
                        [
                            capitalize(data.sd_first_name),
                            capitalize(data.sd_middle_name),
                            capitalize(data.sd_last_name),
                            data.sd_email,
                            data.sd_phone,
                            data.sd_relationship,
                            data.sd_address1,
                            data.sd_address2,
                            data.sd_city,
                            data.sd_state,
                            data.sd_country,
                            data.sd_zip_code,
                            current_date,
                            current_user_id,
                            data.app_id
                        ]
                        , function (err, resultmain) {
                            if (err) {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                                callback(obj);
                            } else {
                                obj.status = 1;
                                callback(obj);
                            }
                            resolve()
                        })
                }
            })
    })
}

exports.getAddDetails = async (con, Id, coSigner, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        let cond = '';
        if (coSigner == 1) {
            cond = 'co_signer_id=? AND patient_address.status=?';
        } else {
            cond = 'patient_id=? AND patient_address.status=?';
        }
        con.query('SELECT '
            + 'patient_address.address1,patient_address.address2,patient_address.City as city,patient_address.county,patient_address.region_id,patient_address.address_time_period,patient_address.phone_no,patient_address.zip_code,states.name as state_name,countries.name as country_name,patient_address.state_id as state, '
            + 'patient_address.primary_address,patient_address.billing_address,patient_address.same_billing_flag, countries.id as country '
            + 'FROM patient_address '
            /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
            + 'INNER JOIN states ON states.state_id=regions.state_id '*/
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'INNER JOIN countries ON countries.id=states.country_id '
            + 'WHERE ' + cond,
            [
                Id,
                1
            ]
            , function (err, planaddressDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.appAddress = planaddressDetails;
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getExpDetails = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + '* '
            + 'FROM credit_applications_addressinformation '
            + 'WHERE '
            + 'applcation_id=?',
            [
                Id,
            ]
            , function (err, experianAddress) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.experianAddress = experianAddress;
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getExpEmpDetails = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + '* '
            + 'FROM credit_applications_employmentinformation '
            + 'WHERE '
            + 'applcation_id=?',
            [
                Id,
            ]
            , function (err, experianEmployment) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.experianEmployment = experianEmployment;
                    callback(obj)
                }
                resolve()
            })
    })
}
exports.getCoDetails = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'credit_app_cosigner.id,credit_app_cosigner.co_signer_id as patient_id,credit_app_cosigner.application_id,credit_app_cosigner.primary_bill_pay_flag,credit_app_cosigner.bill_pay_pct,credit_app_cosigner.relationship, master_data_values.value as relationship_name '
            + 'FROM credit_app_cosigner '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_app_cosigner.relationship AND master_data_values.md_id=? '
            + 'WHERE '
            + 'credit_app_cosigner.application_id=?',
            [
                'Relationship', Id,
            ]
            , function (err, coDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.coDetails = (coDetails.length > 0) ? coDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getCoApp = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'application_id '
            + 'FROM credit_applications '
            + 'WHERE '
            + 'co_signer_id=? ORDER BY application_id DESC LIMIT 1',
            [
                Id,
            ]
            , function (err, coApp) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.coApp = (coApp.length > 0) ? coApp[0].application_id : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getBankDetails = async (con, Id, coSigner, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        let cond = '';
        if (coSigner == 1) {
            cond = 'patient_bank_details.status = 1 AND patient_bank_details.co_signer_id=?';
        } else {
            cond = 'patient_bank_details.status = 1 AND patient_bank_details.patient_id=?';
        }
        con.query('SELECT '
            + 'patient_bank_details.bank_name,patient_bank_details.bank_ac,patient_bank_details.bank_address,patient_bank_details.rounting_no,patient_bank_details.account_name,patient_bank_details.account_type,patient_bank_details.primary_bank,'
            + 'master_data_values.value '
            + 'FROM patient_bank_details '
            + 'INNER JOIN master_data_values ON master_data_values.mdv_id = patient_bank_details.account_type '
            + 'WHERE ' + cond,
            [
                Id
            ]
            , function (err, bankDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.bankDetails = bankDetails;
                    callback(obj);
                }
                resolve()
            })
    })
}

exports.getCustomerDetails = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'patient.status,patient.bank_verify_amt,patient.bank_verify_amt_duration,patient.bank_verify_flag,patient.profile_flag,patient.patient_ac,patient.f_name,patient.m_name,patient.l_name,patient.other_f_name,patient.other_m_name,patient.other_l_name,AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,patient.gender,patient.malling_address,patient.peimary_phone,patient.alternative_phone,patient.email,patient.secondary_email,patient.employment_status,patient.annual_income,patient.employer_name,DATE_FORMAT(patient.employer_since, "%m/%d/%Y") as employer_since,patient.employer_phone,patient.employer_email,patient.bank_name,patient.account_number,patient.bank_address,patient.rounting_no,patient.account_name,patient.withdrawal_date,master_data_values.value as account_type, empType.value as emp_type_name,user.username as username,user.email_id as user_eamil,user.phone as user_phone1,patient.alternative_phone as user_phone2 '

            + 'FROM patient '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=patient.account_type '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=patient.employment_type '
            + 'WHERE '
            + 'patient.patient_id=?',
            [
                Id
            ]
            , function (err, planDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.customerDetails = (planDetails.length > 0) ? planDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getAllSecDetails = async (con, Id, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'credit_sec_details.f_name, credit_sec_details.m_name, credit_sec_details.l_name, credit_sec_details.email, credit_sec_details.phone, credit_sec_details.relationship, credit_sec_details.address1, credit_sec_details.address2, credit_sec_details.city, credit_sec_details.state, credit_sec_details.country, credit_sec_details.zip_code,states.name as state_name,countries.name as country_name, master_data_values.value as relationship_name '
            + 'FROM credit_sec_details '
            + 'INNER JOIN states ON states.state_id=credit_sec_details.state '
            + 'INNER JOIN countries ON countries.id=states.country_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_sec_details.relationship AND master_data_values.md_id=? '
            + 'WHERE credit_sec_details.patient_id = ? ORDER BY credit_sec_details.id DESC',
            [
                'Relationship', Id
            ]
            , function (err, secDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    obj.secDetails = (secDetails.length > 0) ? secDetails : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getCusAppDetails = async (con, Id, callback) => {
    return new Promise((resolve, reject) => {
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,credit_applications.document_flag,credit_applications.plan_flag,credit_applications.score,credit_applications.approve_amount,DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") AS expiry_date,credit_applications.remaining_amount,credit_applications.override_amount,credit_applications.status,DATE_FORMAT(credit_applications.date_created, "%m/%d/%Y") AS date_created,patient.f_name,patient.m_name,patient.l_name,patient.gender,patient.email,patient.peimary_phone,patient.patient_ac, '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, credit_applications.application_id, master_data_values.value as status_name, '
            + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_applications.status AND master_data_values.md_id=? '
            + 'WHERE credit_applications.co_signer=? AND credit_applications.delete_flag=? AND patient.patient_id=? ORDER BY credit_applications.application_id DESC',
            /*+ 'WHERE credit_applications.delete_flag=? AND '+cond+' ORDER BY application_id DESC',*/
            [
                'Application Status',
                0,
                0,
                Id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0
                    }
                    callback(obj)
                } else {
                    var obj = {
                        "status": 1,
                        "applicationDetails": rows
                    }
                    callback(obj)
                }
                resolve();
            })
    })
}

/******* new changes *********/
exports.deleteBank = async (con, id, callback) => {
    return new Promise((resolve, reject) => {
        con.query('DELETE FROM patient_bank_details '
            + 'WHERE `patient_id` IN (SELECT patient_id FROM credit_applications WHERE application_id=?) OR co_signer_id IN (SELECT co_signer_id FROM credit_app_cosigner WHERE application_id=?) ',
            [
                id,
                id,
            ]
            , function (err, delBank) {
                resolve();
            });
    })
}
exports.existCoCustomer = async (con, data, coSigner, callback) => {
    return new Promise((resolve, reject) => {
        if (data.co_patient_id === undefined) {
            con.query('SELECT co_signer_id,dob,ssn FROM co_signer WHERE f_name=? AND l_name=? AND AES_DECRYPT(dob, "ramesh_cogniz")=? AND AES_DECRYPT(ssn, "ramesh_cogniz")=?',
                [
                    data.co_first_name,
                    data.co_last_name,
                    data.co_dob,
                    data.co_ssn,
                ]
                , function (err, existCustoemr) {
                    var patient_id = (existCustoemr.length > 0) ? existCustoemr[0].co_signer_id : '';
                    callback(patient_id);
                    resolve();
                });
        } else {
            callback(data.co_patient_id);
            resolve();
        }
    })
}

exports.addpatientCoDetails = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        con.query('INSERT INTO co_signer SET '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'gender=?, '
            + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'employment_status=?, '
            + 'employment_type=?, '
            + 'annual_income=?, '
            + 'employer_since=?, '
            + 'employer_name=?, '
            + 'employer_phone=?, '
            + 'employer_email=?, '
            + 'withdrawal_date=?, '
            + 'peimary_phone=?, '
            + 'alternative_phone=?, '
            + 'email=?, '
            + 'secondary_email=?, '
            + 'status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                capitalize(data.co_first_name),
                capitalize(data.co_middle_name),
                capitalize(data.co_last_name),
                (data.co_gender == 'M') ? 'M' : 'F',
                data.co_dob,
                data.co_ssn,
                (data.co_employed == 1) ? 1 : 0,
                data.co_employment_type,
                data.co_annual_income,
                data.co_employed_since,
                capitalize(data.co_employer_name),
                data.co_employer_phone,
                data.co_employer_email.toLowerCase(),
                data.co_withdrawal_date,
                data.co_phone_1,
                data.co_phone_2,
                data.co_email.toLowerCase(),
                data.co_secondary_email.toLowerCase(),
                data.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, resultmain) {

                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.state = 1;
                    obj.mainPatientId = resultmain.insertId;
                    callback(obj);
                }
                resolve();
            })
    })
}


var updatePatientSingleBank = function (con, customerData, updatepatitentID, current_id, callback) {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    if (customerData.details.bank_id != '') {
        con.query('UPDATE `patient_bank_details` SET status=? WHERE bank_id=?',
            [
                0,
                customerData.details.bank_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    con.query('INSERT INTO patient_bank_details ('
                        + 'patient_id, '
                        + 'bank_name, '
                        + 'bank_ac, '
                        + 'bank_address, '
                        + 'rounting_no, '
                        + 'account_name, '
                        + 'account_type, '
                        + 'primary_bank, '
                        + 'status, '
                        + 'date_created, '
                        + 'date_modified, '
                        + 'created_by, '
                        + 'modified_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',
                        [
                            updatepatitentID,
                            (customerData.details.bank_name != null) ? capitalize(customerData.details.bank_name) : '',
                            (customerData.details.bank_ac != null) ? customerData.details.bank_ac : '',
                            (customerData.details.bank_address != null) ? capitalize(customerData.details.bank_address) : '',
                            (customerData.details.rounting_no != null) ? customerData.details.rounting_no : '',
                            (customerData.details.account_name != null) ? capitalize(customerData.details.account_name) : '',
                            (customerData.details.account_type != null) ? customerData.details.account_type : '',
                            1,
                            1,
                            current_date,
                            current_date,
                            current_id,
                            current_id
                        ]
                        , function (err, resultuser) {
                            if (err) {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                                callback(obj);
                            } else {
                                obj.status = 1;
                                callback(obj);
                            }
                        })
                }
            })
    } else {
        con.query('INSERT INTO patient_bank_details ('
            + 'patient_id, '
            + 'bank_name, '
            + 'bank_ac, '
            + 'bank_address, '
            + 'rounting_no, '
            + 'account_name, '
            + 'account_type, '
            + 'primary_bank, '
            + 'status, '
            + 'date_created, '
            + 'date_modified, '
            + 'created_by, '
            + 'modified_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [
                updatepatitentID,
                customerData.bank_name,
                customerData.bank_ac,
                customerData.bank_address,
                customerData.rounting_no,
                customerData.account_name,
                customerData.account_type,
                1,
                1,
                current_date,
                current_date,
                current_id,
                current_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
            })
    }


}

exports.updatePatitentCoDetails = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    return new Promise((resolve, reject) => {
        con.query('UPDATE co_signer SET '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'gender=?, '
            + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
            + 'employment_status=?, '
            + 'employment_type=?, '
            + 'annual_income=?, '
            + 'employer_since=?, '
            + 'employer_name=?, '
            + 'employer_phone=?, '
            + 'employer_email=?, '
            + 'withdrawal_date=?, '
            + 'peimary_phone=?, '
            + 'alternative_phone=?, '
            + 'email=?, '
            + 'secondary_email=?, '
            + 'status=?, '
            + 'date_modified=?, '
            + 'modified_by=? WHERE co_signer_id=?',
            [
                capitalize(data.co_first_name),
                capitalize(data.co_middle_name),
                capitalize(data.co_last_name),
                (data.co_gender == 'M') ? 'M' : 'F',
                data.co_dob,
                data.co_ssn,
                (data.co_employed == 1) ? 1 : 0,
                data.co_employment_type,
                data.co_annual_income,
                data.co_employed_since,
                capitalize(data.co_employer_name),
                data.co_employer_phone,
                data.co_employer_email.toLowerCase(),
                data.co_withdrawal_date,
                data.co_phone_1,
                data.co_phone_2,
                data.co_email.toLowerCase(),
                data.co_secondary_email.toLowerCase(),
                data.status,
                current_date,
                current_user_id,
                data.co_patient_id
            ]
            , function (err, resultmain) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.state = 1;
                    obj.mainPatientId = data.co_patient_id;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.addPatientCoUser = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    const md5 = require('md5');
    let password = (coSigner == 1) ? md5(data.co_password) : md5(data.password);
    return new Promise((resolve, reject) => {
        con.query('INSERT INTO user SET '
            + 'user_type_role_id=?, '
            + 'co_signer_id=?, '
            + 'username=?, '
            + 'password=?, '
            + 'email_id=?, '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'phone=?, '
            + 'status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                12,
                data.co_patient_id,
                data.co_username,
                password,
                data.co_email.toLowerCase(),
                capitalize(data.co_first_name),
                capitalize(data.co_middle_name),
                capitalize(data.co_last_name),
                data.co_phone_1,
                data.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.updatePatientCoUser = async (con, data, coSigner, callback) => {
    let obj = {}
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    const md5 = require('md5');
    let password = (coSigner == 1) ? md5(data.co_password) : md5(data.password);
    return new Promise((resolve, reject) => {
        con.query('UPDATE user SET '
            + 'email_id=?, '
            + 'f_name=?, '
            + 'm_name=?, '
            + 'l_name=?, '
            + 'phone=?, '
            + 'status=?, '
            + 'date_modified=?, '
            + 'modified_by=? WHERE co_signer_id=?',
            [
                data.co_email.toLowerCase(),
                capitalize(data.co_first_name),
                capitalize(data.co_middle_name),
                capitalize(data.co_last_name),
                data.co_phone_1,
                data.status,
                current_date,
                current_user_id,
                data.co_patient_id
            ]
            , function (err, resultuser) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    callback(obj);
                } else {
                    obj.status = 1;
                    callback(obj);
                }
                resolve();
            })
    })
}

exports.getAppCoDetails = async (con, appId, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,'
            + 'credit_applications.amount,credit_applications.override_amount,credit_applications.override_amount_cmt,credit_applications.score,approve_amount,remaining_amount,DATE_FORMAT(credit_applications.date_modified, "%m/%d/%Y") as authorization_date, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date,credit_applications.expiry_date_cmt,credit_applications.status as application_status,'
            + 'credit_applications.plan_flag,co_signer.co_signer_id as patient_id,'
            + 'co_signer.f_name,co_signer.m_name,co_signer.l_name,co_signer.status,'
            + 'AES_DECRYPT(co_signer.dob, "ramesh_cogniz") as dob,AES_DECRYPT(co_signer.ssn, "ramesh_cogniz") as ssn,co_signer.gender,co_signer.malling_address,co_signer.peimary_phone,co_signer.alternative_phone,co_signer.email,co_signer.secondary_email,co_signer.employment_status,co_signer.annual_income,co_signer.employer_name,DATE_FORMAT(co_signer.employer_since, "%m/%d/%Y") as employer_since,co_signer.employer_phone,co_signer.employer_email,empType.value as emp_type_name, co_signer.employment_type,co_signer.withdrawal_date, '
            + 'user.username as username,user.email_id as user_eamil,user.phone as user_phone1,co_signer.alternative_phone as user_phone2, '
            + 'credit_applications_factor.score AS experian_status '
            + 'FROM credit_applications '
            + 'INNER JOIN co_signer ON co_signer.co_signer_id=credit_applications.co_signer_id '
            + 'INNER JOIN user ON user.co_signer_id=co_signer.co_signer_id '
            + 'LEFT JOIN credit_applications_factor ON credit_applications_factor.applcation_id=credit_applications.application_id '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=co_signer.employment_type '
            + 'WHERE '
            + 'application_id=?',
            [
                appId
            ]
            , function (err, planDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.planDetails = (planDetails) ? planDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

exports.getAppCosignerDetails = async (con, appId, callback) => {
    let obj = {}
    return new Promise((resolve, reject) => {
        con.query('SELECT co_signer.f_name,co_signer.m_name,co_signer.l_name,co_signer.status,'
            + 'AES_DECRYPT(co_signer.dob, "ramesh_cogniz") as dob,AES_DECRYPT(co_signer.ssn, "ramesh_cogniz") as ssn,co_signer.gender,co_signer.malling_address,co_signer.peimary_phone,co_signer.alternative_phone,co_signer.email,co_signer.secondary_email,co_signer.employment_status,co_signer.annual_income,co_signer.employer_name,DATE_FORMAT(co_signer.employer_since, "%m/%d/%Y") as employer_since,co_signer.employer_phone,co_signer.employer_email,empType.value as emp_type_name, co_signer.employment_type,co_signer.withdrawal_date, '
            + 'user.username as username,user.email_id as user_eamil,user.phone as user_phone1,co_signer.alternative_phone as user_phone2 '

            + 'FROM co_signer '
            + 'INNER JOIN user ON user.co_signer_id=co_signer.co_signer_id '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=co_signer.employment_type '
            + 'WHERE '
            + 'co_signer.co_signer_id=?',
            [
                appId
            ]
            , function (err, planDetails) {
                if (err) {
                    obj.status = 0;
                    callback(obj);
                } else {
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.planDetails = (planDetails) ? planDetails[0] : '';
                    callback(obj)
                }
                resolve()
            })
    })
}

// this function use for save all data in table.
var creditSql = function (con, data, callback) {
    const md5 = require('md5');
    const aes256 = require('aes256');
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;
    let password = md5(data.password);
    var ase256key = 'ramesh';
    data.provider_id = (data.provider_id !== undefined) ? data.provider_id : null;

    //var encrypted = aes256.encrypt(ase256key, plaintext);
    //var decrypted = aes256.decrypt(key, encrypted);
    //return false;
    var obj = {};
    con.beginTransaction(function (err) {
        /*if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }*/

        existCustomer(con, data, function (result_id) {
            data.patient_id = result_id;

            // return false;
            if (data.patient_id !== undefined && data.patient_id != '') {

                con.query('SELECT credit_applications.application_id '
                    + 'FROM credit_applications '
                    + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
                    + 'LEFT JOIN payment_plan on payment_plan.application_id=credit_applications.application_id '
                    + 'WHERE (payment_plan.paid_flag=? OR expiry_date>=now()) AND (credit_applications.remaining_amount>? OR payment_plan.paid_flag=?) AND patient.patient_id=?  AND credit_applications.status NOT IN(0,3)',
                    [
                        0,
                        0,
                        0,
                        data.patient_id
                    ]
                    , function (err, planActive) {
                        if (err) {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            // check plan active or not
                            if (planActive.length == 0) {
                                con.query('UPDATE patient SET '
                                    + 'provider_id=?, '
                                    + 'f_name=?, '
                                    + 'm_name=?, '
                                    + 'l_name=?, '
                                    + 'other_f_name=?, '
                                    + 'other_m_name=?, '
                                    + 'other_l_name=?, '
                                    + 'gender=?, '
                                    + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
                                    + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
                                    + 'employment_status=?, '
                                    + 'employment_type=?, '
                                    + 'annual_income=?, '
                                    + 'employer_since=?, '
                                    + 'employer_name=?, '
                                    + 'employer_phone=?, '
                                    + 'employer_email=?, '
                                    /*+ 'bank_name=?, '
                                    + 'bank_address=?, '
                                    + 'rounting_no=?, '
                                    + 'account_number=?, '
                                    + 'account_name=?, '
                                    + 'account_type=?, '*/
                                    + 'withdrawal_date=?, '
                                    + 'peimary_phone=?, '
                                    + 'alternative_phone=?, '
                                    + 'email=?, '
                                    + 'secondary_email=?, '
                                    + 'status=?, '
                                    + 'date_modified=?, '
                                    + 'modified_by=? WHERE patient_id=?',
                                    [
                                        data.provider_id,
                                        capitalize(data.first_name),
                                        capitalize(data.middle_name),
                                        capitalize(data.last_name),
                                        capitalize(data.alias_first_name),
                                        capitalize(data.alias_middle_name),
                                        capitalize(data.alias_last_name),
                                        (data.gender == 'M') ? 'M' : 'F',
                                        data.dob,
                                        data.ssn,
                                        (data.employed == 1) ? 1 : 0,
                                        data.employment_type,
                                        data.annual_income,
                                        data.employed_since,
                                        capitalize(data.employer_name),
                                        data.employer_phone,
                                        data.employer_email.toLowerCase(),
                                        /*capitalize(data.bank_name),
                                        capitalize(data.bank_address),
                                        data.rounting_no,
                                        data.bank_ac,
                                        capitalize(data.account_name),
                                        data.account_type,*/
                                        data.withdrawal_date,
                                        data.phone_1,
                                        data.phone_2,
                                        data.email.toLowerCase(),
                                        data.secondary_email.toLowerCase(),
                                        data.status,
                                        current_date,
                                        current_user_id,
                                        data.patient_id
                                    ]
                                    , function (err, resultmain) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            con.query('UPDATE user SET '
                                                + 'email_id=?, '
                                                + 'f_name=?, '
                                                + 'm_name=?, '
                                                + 'l_name=?, '
                                                + 'phone=?, '
                                                + 'status=?, '
                                                + 'date_modified=?, '
                                                + 'modified_by=? WHERE patient_id=?',
                                                [
                                                    data.email.toLowerCase(),
                                                    capitalize(data.first_name),
                                                    capitalize(data.middle_name),
                                                    capitalize(data.last_name),
                                                    data.phone_1,
                                                    data.status,
                                                    current_date,
                                                    current_user_id,
                                                    data.patient_id
                                                ]
                                                , function (err, resultuser) {
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Something wrong please try again.";
                                                            return callback(obj);
                                                        });
                                                    } else {
                                                        con.query('UPDATE `credit_applications_bank_details` SET status=? WHERE patient_id=?',
                                                            [
                                                                0,
                                                                data.patient_id
                                                            ]
                                                            , function (err, resultuser) {
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Something wrong please try again.";
                                                                        return callback(obj);
                                                                    });
                                                                } else {
                                                                    let bankDetails = [];
                                                                    data.bank.forEach(function (element, idx) {
                                                                        if (element.bank_name != '') {
                                                                            bankDetails.push([
                                                                                data.patient_id,
                                                                                capitalize(element.bank_name),
                                                                                element.bank_ac,
                                                                                capitalize(element.bank_address),
                                                                                element.rounting_no,
                                                                                capitalize(element.account_name),
                                                                                element.account_type,
                                                                                element.primary_bank,
                                                                                data.status,
                                                                                current_date,
                                                                                current_date,
                                                                                current_user_id,
                                                                                current_user_id
                                                                            ]);
                                                                        }
                                                                    });
                                                                    con.query('INSERT INTO credit_applications_bank_details ('
                                                                        + 'patient_id, '
                                                                        + 'bank_name, '
                                                                        + 'bank_ac, '
                                                                        + 'bank_address, '
                                                                        + 'rounting_no, '
                                                                        + 'account_name, '
                                                                        + 'account_type, '
                                                                        + 'primary_bank, '
                                                                        + 'status, '
                                                                        + 'date_created, '
                                                                        + 'date_modified, '
                                                                        + 'created_by, '
                                                                        + 'modified_by) VALUES ?',
                                                                        [
                                                                            bankDetails
                                                                        ]
                                                                        , function (err, resultuser) {
                                                                            if (err && bankDetails.length > 0) {
                                                                                con.rollback(function () {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Something wrong please try again.";
                                                                                    return callback(obj);
                                                                                });
                                                                            } else {
                                                                                con.query('UPDATE `patient_address` SET status=? WHERE patient_id=?',
                                                                                    [
                                                                                        0,
                                                                                        data.patient_id
                                                                                    ]
                                                                                    , function (err, resultuser) {
                                                                                        if (err) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Something wrong please try again.";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            var values = [];
                                                                                            var billing = data.location.filter(x => x.billing_address == 1);
                                                                                            if (billing.length < 1) {
                                                                                                values.push([
                                                                                                    data.patient_id,
                                                                                                    data.billing_state,
                                                                                                    capitalize(data.billing_address1),
                                                                                                    capitalize(data.billing_address2),
                                                                                                    capitalize(data.billing_city),
                                                                                                    data.billing_zip_code,
                                                                                                    data.billing_how_long,
                                                                                                    0,
                                                                                                    1,
                                                                                                    0,
                                                                                                    data.billing_phone_no,
                                                                                                    data.status,
                                                                                                    current_date,
                                                                                                    current_date,
                                                                                                    current_user_id,
                                                                                                    current_user_id
                                                                                                ]);
                                                                                            }

                                                                                            data.location.forEach(function (element, idx) {
                                                                                                values.push([
                                                                                                    data.patient_id,
                                                                                                    element.state,
                                                                                                    capitalize(element.address1),
                                                                                                    capitalize(element.address2),
                                                                                                    capitalize(element.city),
                                                                                                    element.zip_code,
                                                                                                    element.how_long,
                                                                                                    element.primary_address,
                                                                                                    element.billing_address,
                                                                                                    (element.billing_address) ? 1 : 0,
                                                                                                    element.phone_no,
                                                                                                    data.status,
                                                                                                    current_date,
                                                                                                    current_date,
                                                                                                    current_user_id,
                                                                                                    current_user_id
                                                                                                ]);
                                                                                            });
                                                                                            con.query('INSERT INTO patient_address ('
                                                                                                + 'patient_id, '
                                                                                                + 'state_id, '
                                                                                                + 'address1, '
                                                                                                + 'address2, '
                                                                                                + 'City, '
                                                                                                + 'zip_code, '
                                                                                                + 'address_time_period, '
                                                                                                + 'primary_address, '
                                                                                                + 'billing_address, '
                                                                                                + 'same_billing_flag, '
                                                                                                + 'phone_no, '
                                                                                                + 'status, '
                                                                                                + 'date_created, '
                                                                                                + 'date_modified, '
                                                                                                + 'created_by, '
                                                                                                + 'modified_by) VALUES ?',
                                                                                                [
                                                                                                    values
                                                                                                ]
                                                                                                , function (err, resultuser) {

                                                                                                    if (err) {
                                                                                                        con.rollback(function () {
                                                                                                            obj.status = 0;
                                                                                                            obj.message = "Something wrong please try again.";
                                                                                                            return callback(obj);
                                                                                                        });
                                                                                                    } else {
                                                                                                        con.query('INSERT INTO credit_applications SET '
                                                                                                            + 'patient_id=?, '
                                                                                                            + 'application_no=((SELECT COALESCE(MAX(application_no),11111)+1 AS dt FROM credit_applications as m)), '
                                                                                                            + 'amount=?, '
                                                                                                            + 'status=?, '
                                                                                                            + 'date_created=?, '
                                                                                                            + 'date_modified=?, '
                                                                                                            + 'created_by=?, '
                                                                                                            + 'modified_by=?',
                                                                                                            [
                                                                                                                data.patient_id,
                                                                                                                data.amount,
                                                                                                                0,
                                                                                                                current_date,
                                                                                                                current_date,
                                                                                                                current_user_id,
                                                                                                                current_user_id
                                                                                                            ]
                                                                                                            , function (err, application, aa) {
                                                                                                                if (err) {
                                                                                                                    con.rollback(function () {
                                                                                                                        obj.status = 0;
                                                                                                                        obj.message = "Something wrong please try again.";
                                                                                                                        return callback(obj);
                                                                                                                    });
                                                                                                                } else {
                                                                                                                    existProviderPatient(con, data.provider_id, data.patient_id, function (exitsPatientProvider) {
                                                                                                                        if (exitsPatientProvider.exists) {
                                                                                                                            con.commit(function (err) {
                                                                                                                                if (err) {
                                                                                                                                    con.rollback(function () {
                                                                                                                                        obj.status = 0;
                                                                                                                                        obj.message = "Something wrong please try again.";
                                                                                                                                        return callback(obj);
                                                                                                                                    });
                                                                                                                                } else {
                                                                                                                                    var sql = "SELECT value as username, (SELECT value FROM `master_data_values` WHERE md_id=?) as password, (SELECT value FROM `master_data_values` WHERE md_id=?) as subscriberCode, (SELECT value FROM `master_data_values` WHERE md_id=?) as api_key, (SELECT value FROM `master_data_values` WHERE md_id=?) as secret_key  FROM `master_data_values` WHERE md_id=?";
                                                                                                                                    con.query(sql, ['Experian Password', 'Experian Subscriber Code', 'Experian API Key', 'Experian SECRET Key', 'Experian Username'], function (error, values) {
                                                                                                                                        if (error) {
                                                                                                                                            obj.status = 1;
                                                                                                                                            obj.applicationId = application.insertId;
                                                                                                                                            obj.message = "Application submitted for approval. But Experian details not found.";
                                                                                                                                            return callback(obj);
                                                                                                                                        } else {
                                                                                                                                            obj.status = 1;
                                                                                                                                            obj.applicationId = application.insertId;
                                                                                                                                            obj.experian = values;
                                                                                                                                            obj.message = "Application submitted for approval";
                                                                                                                                            return callback(obj);
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                }
                                                                                                                                /*obj.status = 1;
                                                                                                                                obj.applicationId = application.insertId;
                                                                                                                                obj.message = "Application submitted for approval";
                                                                                                                                return callback(obj);*/
                                                                                                                            });
                                                                                                                        } else {
                                                                                                                            con.query('INSERT INTO patient_provider SET '
                                                                                                                                + 'patient_id=?, '
                                                                                                                                + 'provider_id=?, '
                                                                                                                                + 'date_created=?, '
                                                                                                                                + 'date_modified=?, '
                                                                                                                                + 'created_by=?, '
                                                                                                                                + 'modified_by=?',
                                                                                                                                [
                                                                                                                                    data.patient_id,
                                                                                                                                    data.provider_id,
                                                                                                                                    current_date,
                                                                                                                                    current_date,
                                                                                                                                    current_user_id,
                                                                                                                                    current_user_id
                                                                                                                                ]
                                                                                                                                , function (err, bridgeResult) {
                                                                                                                                    if (err) {
                                                                                                                                        con.rollback(function () {
                                                                                                                                            obj.status = 0;
                                                                                                                                            obj.message = "Something wrong please try again.";
                                                                                                                                            return callback(obj);
                                                                                                                                        });
                                                                                                                                    } else {
                                                                                                                                        con.commit(function (err) {
                                                                                                                                            if (err) {
                                                                                                                                                con.rollback(function () {
                                                                                                                                                    obj.status = 0;
                                                                                                                                                    obj.message = "Something wrong please try again.";
                                                                                                                                                    return callback(obj);
                                                                                                                                                });
                                                                                                                                            } else {
                                                                                                                                                var sql = "SELECT value as username, (SELECT value FROM `master_data_values` WHERE md_id=?) as password, (SELECT value FROM `master_data_values` WHERE md_id=?) as subscriberCode, (SELECT value FROM `master_data_values` WHERE md_id=?) as api_key, (SELECT value FROM `master_data_values` WHERE md_id=?) as secret_key  FROM `master_data_values` WHERE md_id=?";
                                                                                                                                                con.query(sql, ['Experian Password', 'Experian Subscriber Code', 'Experian API Key', 'Experian SECRET Key', 'Experian Username'], function (error, values) {
                                                                                                                                                    if (error) {
                                                                                                                                                        obj.status = 1;
                                                                                                                                                        obj.applicationId = application.insertId;
                                                                                                                                                        obj.message = "Application submitted for approval. But Experian details not found.";
                                                                                                                                                        return callback(obj);
                                                                                                                                                    } else {
                                                                                                                                                        obj.status = 1;
                                                                                                                                                        obj.applicationId = application.insertId;
                                                                                                                                                        obj.experian = values;
                                                                                                                                                        obj.message = "Application submitted for approval";
                                                                                                                                                        return callback(obj);
                                                                                                                                                    }
                                                                                                                                                });
                                                                                                                                            }
                                                                                                                                            /*obj.status = 1;
                                                                                                                                            obj.applicationId = application.insertId;
                                                                                                                                            obj.message = "Application submitted for approval";
                                                                                                                                            return callback(obj);*/
                                                                                                                                        });
                                                                                                                                    }
                                                                                                                                });
                                                                                                                        }
                                                                                                                    })

                                                                                                                }
                                                                                                            });
                                                                                                    }
                                                                                                });
                                                                                        }

                                                                                    });
                                                                            }
                                                                        })
                                                                }
                                                            })
                                                    }
                                                });
                                        }
                                    })

                            } else {
                                obj.status = 0;
                                obj.message = "Another credit application under process.";
                                return callback(obj);
                            }
                        }

                    });

            } else {
                con.query('INSERT INTO patient SET '
                    + 'f_name=?, '
                    + 'patient_ac=((SELECT COALESCE(MAX(patient_ac),11111)+1 AS dt FROM patient as p)), '
                    + 'm_name=?, '
                    + 'l_name=?, '
                    + 'other_f_name=?, '
                    + 'other_m_name=?, '
                    + 'other_l_name=?, '
                    + 'gender=?, '
                    + 'dob=AES_ENCRYPT(?, "ramesh_cogniz"), '
                    + 'ssn=AES_ENCRYPT(?, "ramesh_cogniz"), '
                    + 'employment_status=?, '
                    + 'employment_type=?, '
                    + 'annual_income=?, '
                    + 'employer_since=?, '
                    + 'employer_name=?, '
                    + 'employer_phone=?, '
                    + 'employer_email=?, '
                    /*+ 'bank_name=?, '
                    + 'bank_address=?, '
                    + 'rounting_no=?, '
                    + 'account_number=?, '
                    + 'account_name=?, '
                    + 'account_type=?, '*/
                    + 'withdrawal_date=?, '
                    + 'peimary_phone=?, '
                    + 'alternative_phone=?, '
                    + 'email=?, '
                    + 'secondary_email=?, '
                    + 'status=?, '
                    + 'date_created=?, '
                    + 'date_modified=?, '
                    + 'created_by=?, '
                    + 'modified_by=?',
                    [
                        capitalize(data.first_name),
                        capitalize(data.middle_name),
                        capitalize(data.last_name),
                        capitalize(data.alias_first_name),
                        capitalize(data.alias_middle_name),
                        capitalize(data.alias_last_name),
                        (data.gender == 'M') ? 'M' : 'F',
                        data.dob,
                        data.ssn,
                        (data.employed == 1) ? 1 : 0,
                        data.employment_type,
                        data.annual_income,
                        data.employed_since,
                        capitalize(data.employer_name),
                        data.employer_phone,
                        data.employer_email.toLowerCase(),
                        /*capitalize(data.bank_name),
                        capitalize(data.bank_address),
                        data.rounting_no,
                        data.bank_ac,
                        capitalize(data.account_name),
                        data.account_type,*/
                        data.withdrawal_date,
                        data.phone_1,
                        data.phone_2,
                        data.email.toLowerCase(),
                        data.secondary_email.toLowerCase(),
                        data.status,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                    ]
                    , function (err, resultmain) {
                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                                return callback(obj);
                            });
                        } else {
                            // insert user details
                            con.query('INSERT INTO user SET '
                                + 'user_type_role_id=?, '
                                + 'patient_id=?, '
                                + 'username=?, '
                                + 'password=?, '
                                + 'email_id=?, '
                                + 'f_name=?, '
                                + 'm_name=?, '
                                + 'l_name=?, '
                                + 'phone=?, '
                                + 'status=?, '
                                + 'date_created=?, '
                                + 'date_modified=?, '
                                + 'created_by=?, '
                                + 'modified_by=?',
                                [
                                    12,
                                    resultmain.insertId,
                                    data.username,
                                    password,
                                    data.email.toLowerCase(),
                                    capitalize(data.first_name),
                                    capitalize(data.middle_name),
                                    capitalize(data.last_name),
                                    data.phone_1,
                                    data.status,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                ]
                                , function (err, resultuser) {
                                    let bankDetails = [];
                                    data.bank.forEach(function (element, idx) {
                                        if (element.bank_name != '') {
                                            bankDetails.push([
                                                resultmain.insertId,
                                                capitalize(element.bank_name),
                                                element.bank_ac,
                                                capitalize(element.bank_address),
                                                element.rounting_no,
                                                capitalize(element.account_name),
                                                element.account_type,
                                                element.primary_bank,
                                                data.status,
                                                current_date,
                                                current_date,
                                                current_user_id,
                                                current_user_id
                                            ]);
                                        }
                                    });
                                    con.query('INSERT INTO credit_applications_bank_details ('
                                        + 'patient_id, '
                                        + 'bank_name, '
                                        + 'bank_ac, '
                                        + 'bank_address, '
                                        + 'rounting_no, '
                                        + 'account_name, '
                                        + 'account_type, '
                                        + 'primary_bank, '
                                        + 'status, '
                                        + 'date_created, '
                                        + 'date_modified, '
                                        + 'created_by, '
                                        + 'modified_by) VALUES ?',
                                        [
                                            bankDetails
                                        ]
                                        , function (err, resultuser) {
                                            if (err && bankDetails.length > 0) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    return callback(obj);
                                                });
                                            } else {
                                                con.query('INSERT INTO patient_provider SET '
                                                    + 'patient_id=?, '
                                                    + 'provider_id=?, '
                                                    + 'date_created=?, '
                                                    + 'date_modified=?, '
                                                    + 'created_by=?, '
                                                    + 'modified_by=?',
                                                    [
                                                        resultmain.insertId,
                                                        data.provider_id,
                                                        current_date,
                                                        current_date,
                                                        current_user_id,
                                                        current_user_id
                                                    ]
                                                    , function (err, bridgeResult) {

                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.";
                                                                return callback(obj);
                                                            });
                                                        } else {
                                                            var values = [];
                                                            var billing = data.location.filter(x => x.billing_address == 1);
                                                            if (billing.length < 1) {
                                                                values.push([
                                                                    resultmain.insertId,
                                                                    data.billing_state,
                                                                    capitalize(data.billing_address1),
                                                                    capitalize(data.billing_address2),
                                                                    capitalize(data.billing_city),
                                                                    data.billing_zip_code,
                                                                    data.billing_how_long,
                                                                    0,
                                                                    1,
                                                                    0,
                                                                    data.billing_phone_no,
                                                                    data.status,
                                                                    current_date,
                                                                    current_date,
                                                                    current_user_id,
                                                                    current_user_id
                                                                ]);
                                                            }

                                                            data.location.forEach(function (element, idx) {
                                                                values.push([
                                                                    resultmain.insertId,
                                                                    element.state,
                                                                    capitalize(element.address1),
                                                                    capitalize(element.address2),
                                                                    capitalize(element.city),
                                                                    element.zip_code,
                                                                    element.how_long,
                                                                    element.primary_address,
                                                                    element.billing_address,
                                                                    (element.billing_address) ? 1 : 0,
                                                                    element.phone_no,
                                                                    data.status,
                                                                    current_date,
                                                                    current_date,
                                                                    current_user_id,
                                                                    current_user_id
                                                                ]);
                                                            });

                                                            con.query('INSERT INTO patient_address ('
                                                                + 'patient_id, '
                                                                + 'state_id, '
                                                                + 'address1, '
                                                                + 'address2, '
                                                                + 'City, '
                                                                + 'zip_code, '
                                                                + 'address_time_period, '
                                                                + 'primary_address, '
                                                                + 'billing_address, '
                                                                + 'same_billing_flag, '
                                                                + 'phone_no, '
                                                                + 'status, '
                                                                + 'date_created, '
                                                                + 'date_modified, '
                                                                + 'created_by, '
                                                                + 'modified_by) VALUES ?',
                                                                [
                                                                    values
                                                                ]
                                                                , function (err, resultuser) {
                                                                    if (err) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Something wrong please try again.";
                                                                            return callback(obj);
                                                                        });
                                                                    } else {
                                                                        con.query('INSERT INTO credit_applications SET '
                                                                            + 'patient_id=?, '
                                                                            + 'application_no=((SELECT COALESCE(MAX(application_no),11111)+1 AS dt FROM credit_applications as m)), '
                                                                            + 'amount=?, '
                                                                            + 'status=?, '
                                                                            + 'date_created=?, '
                                                                            + 'date_modified=?, '
                                                                            + 'created_by=?, '
                                                                            + 'modified_by=?',
                                                                            [
                                                                                resultmain.insertId,
                                                                                data.amount,
                                                                                0,
                                                                                current_date,
                                                                                current_date,
                                                                                current_user_id,
                                                                                current_user_id
                                                                            ]
                                                                            , function (err, application) {
                                                                                if (err) {
                                                                                    con.rollback(function () {
                                                                                        obj.status = 0;
                                                                                        obj.message = "Something wrong please try again.";
                                                                                        return callback(obj);
                                                                                    });
                                                                                } else {
                                                                                    con.commit(function (err) {
                                                                                        if (err) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Something wrong please try again.";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            var sql = "SELECT value as username, (SELECT value FROM `master_data_values` WHERE md_id=?) as password, (SELECT value FROM `master_data_values` WHERE md_id=?) as subscriberCode, (SELECT value FROM `master_data_values` WHERE md_id=?) as api_key, (SELECT value FROM `master_data_values` WHERE md_id=?) as secret_key  FROM `master_data_values` WHERE md_id=?";
                                                                                            con.query(sql, ['Experian Password', 'Experian Subscriber Code', 'Experian API Key', 'Experian SECRET Key', 'Experian Username'], function (error, values) {
                                                                                                if (error) {
                                                                                                    obj.status = 1;
                                                                                                    obj.applicationId = application.insertId;
                                                                                                    obj.message = "Application submitted for approval. But Experian details not found.";
                                                                                                    return callback(obj);
                                                                                                } else {
                                                                                                    obj.status = 1;
                                                                                                    obj.applicationId = application.insertId;
                                                                                                    obj.experian = values;
                                                                                                    obj.message = "Application submitted for approval";
                                                                                                    return callback(obj);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    })
                                                                                }
                                                                            });
                                                                    }

                                                                });
                                                        }
                                                    })
                                            }
                                        })

                                });
                        }

                    });
            }
        })



    });
}

var providerDiscountAll = function (con, id, amount, callback) {
    con.query('SELECT master_data_values2.value AS discount_type, discount_fee_ra.discount_rate, discount_fee_ra.fee_amount, provider.member_flag, DATE_FORMAT(discount_fee_ra.start_date, "%Y/%m/%d") as start_date, DATE_FORMAT(discount_fee_ra.end_date, "%Y/%m/%d") as end_date, master_data_values.value AS term_month '
        + 'FROM discount_fee_ra '
        + 'inner join provider_discount_fee on provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
        + 'INNER JOIN provider ON provider_discount_fee.provider_id=provider.provider_id '
        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.mdv_id = discount_fee_ra.loan_term_month '
        + 'INNER JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=discount_fee_ra.mdv_discount_rate_id '
        + 'WHERE provider.provider_id=? AND discount_fee_ra.start_date<=DATE_FORMAT(now(), "%Y-%m-%d") AND provider.member_flag=? AND provider_discount_fee.status=?',
        [
            id, 1, 1
        ], function (error, result) {

            if (error) {
                var obj = {
                    "status": 0,
                }
                return callback(obj);
            } else {
                var newAmount = 0;
                let date = require('date-and-time');
                var today = date.format(new Date(), 'YYYY/MM/DD');
                var filterDataPromotional = result.filter(x => x.start_date <= today && x.end_date >= today && x.discount_type == 'Promotional');
                if (filterDataPromotional.length > 0) {
                    filterDataPromotional.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                } else {
                    const now = new Date();
                    let closest = Infinity;
                    result.forEach(function (d) {
                        const dateE = new Date(d.start_date);

                        if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                            closest = d.start_date;

                        }


                    });

                    var filterDataStandered = result.filter(x => x.discount_type != 'Promotional' && x.start_date == closest);
                    filterDataStandered.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                }
                /*var filterDataPromotional = result.filter(x => x.start_date <= today && x.end_date >= today);
                if (filterDataPromotional.length > 0) {
                    filterDataPromotional.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                } else {
                    var filterDataStandered = result.filter(x => x.start_date == null && x.end_date == null);
                    filterDataStandered.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                }*/

                var obj = {
                    "status": 1,
                    "amount": newAmount,
                    "pro_discount": result,
                }
                return callback(obj);
            }

        })
}

var providerDiscount = function (con, id, amount, term_id, callback) {
    con.query('SELECT master_data_values2.value AS discount_type, discount_fee_ra.discount_rate, discount_fee_ra.fee_amount, provider.member_flag, DATE_FORMAT(discount_fee_ra.start_date, "%Y/%m/%d") as start_date, DATE_FORMAT(discount_fee_ra.end_date, "%Y/%m/%d") as end_date '
        + 'FROM discount_fee_ra '
        + 'inner join provider_discount_fee on provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
        + 'INNER JOIN provider ON provider_discount_fee.provider_id=provider.provider_id '
        //+ 'INNER JOIN interest_rate ON interest_rate.mdv_payment_term_month=discount_fee_ra.loan_term_month '
        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.mdv_id = discount_fee_ra.loan_term_month '
        + 'INNER JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=discount_fee_ra.mdv_discount_rate_id '
        + 'WHERE provider.provider_id=? AND provider.member_flag=? AND provider_discount_fee.status=? AND master_data_values.value =(SELECT value FROM master_data_values WHERE value>=? AND md_id="Payment Term Month" LIMIT 1)',
        [
            id, 1, 1, parseInt(term_id)
        ], function (error, result) {
            //AND provider_discount_fee.status=1 AND discount_fee_ra.start_date<=now() AND discount_fee_ra.end_date<=now()
            if (error) {
                var obj = {
                    "status": 0,
                }
                return callback(obj);
            } else {
                var newAmount = 0;
                let date = require('date-and-time');
                var today = date.format(new Date(), 'YYYY/MM/DD');
                var filterDataPromotional = result.filter(x => x.start_date <= today && x.end_date >= today && x.discount_type == 'Promotional');
                if (filterDataPromotional.length > 0) {
                    filterDataPromotional.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                } else {


                    const now = new Date();
                    let closest = Infinity;
                    result.forEach(function (d) {
                        const dateE = new Date(d.start_date);

                        if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional') {

                            closest = d.start_date;

                        }


                    });

                    result.forEach(function (element, idx) {

                        if (element.end_date == null && element.discount_type != 'Promotional' && element.start_date <= closest) {
                            newAmount = (amount * element.discount_rate) / 100;
                        } else if (element.discount_type != 'Promotional' && element.start_date <= closest && element.end_date >= today) {
                            newAmount = (amount * element.discount_rate) / 100;
                        }

                    })

                }
                /*var filterDataPromotional = result.filter(x => x.start_date <= today && x.end_date >= today);
                if (filterDataPromotional.length > 0) {
                    filterDataPromotional.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                } else {
                    var filterDataStandered = result.filter(x => x.start_date == null && x.end_date == null);
                    filterDataStandered.forEach(function (element, idx) {
                        newAmount = newAmount + (amount * element.discount_rate) / 100;
                    })
                }*/

                var obj = {
                    "status": 1,
                    "amount": newAmount
                }
                return callback(obj);
            }

        })
}
var existProviderPatient = function (con, provider_id, patient_id, callback) {
    return new Promise((resolve, reject) => {
        con.query('SELECT patient_id '
            + 'FROM patient_provider '
            + 'WHERE patient_id=? AND provider_id=? AND status=?',
            [
                patient_id, provider_id, 1
            ], function (error, result) {

                if (error) {
                    var obj = {
                        "status": 0,
                    }
                    callback(obj);
                    resolve();
                } else {
                    var obj = {
                        "status": 1,
                        "exists": (result.length > 0) ? 1 : 0
                    }
                    callback(obj);
                    resolve();
                }

            })
    })
}
// this function use for save credit application factor in table for corss check if any issue create
var applicationFactor = function (con, id, data, current_user_id) {

    var values = [];
    var addressvalues = [];
    var employevalues = [];
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    if (data.details !== undefined) {
        if (data.details.creditProfile[0].addressInformation !== undefined) {
            data.details.creditProfile[0].addressInformation.forEach(function (element, idx) {
                let censusGeoCode = (element.censusGeoCode !== undefined) ? element.censusGeoCode : null;
                let city = (element.city !== undefined) ? element.city : null;
                let countyCode = (element.countyCode !== undefined) ? element.countyCode : null;
                let dwellingType = (element.dwellingType !== undefined) ? element.dwellingType : null;
                let firstReportedDate = (element.firstReportedDate !== undefined) ? element.firstReportedDate : null;
                let lastReportingSubscriberCode = (element.lastReportingSubscriberCode !== undefined) ? element.lastReportingSubscriberCode : null;
                let lastUpdatedDate = (element.lastUpdatedDate !== undefined) ? element.lastUpdatedDate : null;
                let msaCode = (element.msaCode !== undefined) ? element.msaCode : null;
                let source = (element.source !== undefined) ? element.source : null;
                let state = (element.state !== undefined) ? element.state : null;
                let stateCode = (element.stateCode !== undefined) ? element.stateCode : null;
                let streetName = (element.streetName !== undefined) ? element.streetName : null;
                let streetPrefix = (element.streetPrefix !== undefined) ? element.streetPrefix : null;
                let streetSuffix = (element.streetSuffix !== undefined) ? element.streetSuffix : null;
                let timesReported = (element.timesReported !== undefined) ? element.timesReported : null;
                let unitId = (element.unitId !== undefined) ? element.unitId : null;
                let unitType = (element.unitType !== undefined) ? element.unitType : null;
                let zipCode = (element.zipCode !== undefined) ? element.zipCode : null;
                let date_created = (element.date_created !== undefined) ? element.date_created : null;
                let date_modified = (element.date_modified !== undefined) ? element.date_modified : null;
                let created_by = (element.created_by !== undefined) ? element.created_by : null;
                let modified_by = (element.modified_by !== undefined) ? element.modified_by : null;
                addressvalues.push([
                    id,
                    censusGeoCode,
                    city,
                    countyCode,
                    dwellingType,
                    firstReportedDate,
                    lastReportingSubscriberCode,
                    lastUpdatedDate,
                    msaCode,
                    source,
                    state,
                    stateCode,
                    streetName,
                    streetPrefix,
                    streetSuffix,
                    timesReported,
                    unitId,
                    unitType,
                    zipCode,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            })
            var sql = "INSERT INTO credit_applications_addressinformation (applcation_id, censusGeoCode, city, countyCode, dwellingType, firstReportedDate, lastReportingSubscriberCode, lastUpdatedDate, msaCode, source, state, stateCode, streetName, streetPrefix, streetSuffix, timesReported, unitId, unitType, zipCode, date_created, date_modified, created_by, modified_by) VALUES ?";
            con.query(sql, [addressvalues], function (error) {
                if (error) {
                } else {
                }
            });
        }
        if (data.details.creditProfile[0].employmentInformation !== undefined) {
            data.details.creditProfile[0].employmentInformation.forEach(function (element, idx) {

                let addressExtraLine = (element.addressExtraLine !== undefined) ? element.addressExtraLine : null;
                let addressFirstLine = (element.addressFirstLine !== undefined) ? element.addressFirstLine : null;
                let addressSecondLine = (element.addressSecondLine !== undefined) ? element.addressSecondLine : null;
                let firstReportedDate = (element.firstReportedDate !== undefined) ? element.firstReportedDate : null;
                let lastUpdatedDate = (element.lastUpdatedDate !== undefined) ? element.lastUpdatedDate : null;
                let name = (element.name !== undefined) ? element.name : null;
                let source = (element.source !== undefined) ? element.source : null;
                let zipCode = (element.zipCode !== undefined) ? element.zipCode : null;
                employevalues.push([
                    id,
                    addressExtraLine,
                    addressFirstLine,
                    addressSecondLine,
                    firstReportedDate,
                    lastUpdatedDate,
                    name,
                    source,
                    zipCode,
                    current_date,
                    current_date,
                    current_user_id,
                    current_user_id
                ]);
            })
            var sql = "INSERT INTO credit_applications_employmentinformation (applcation_id, addressExtraLine, addressFirstLine, addressSecondLine, firstReportedDate, lastUpdatedDate, name, source, zipCode, date_created, date_modified, created_by, modified_by) VALUES ?";
            con.query(sql, [employevalues], function (error) {
                if (error) {
                } else {
                }
            });
        }
        data.details.creditProfile[0].riskModel.forEach(function (element, idx) {
            element.scoreFactors.forEach(function (factor, idx) {
                values.push([id, element.modelIndicator, element.score, element.evaluation, factor.importance, factor.code, current_date, current_date, current_user_id, current_user_id]);
            })
        })
        var sql = "INSERT INTO credit_applications_factor (applcation_id, modelIndicator, score, evaluation, importance, code, date_created, date_modified, created_by, modified_by) VALUES ?";
        con.query(sql, [values], function (error) {
            if (error) {
            } else {
            }
        });
    }
}
var applicationScore = async (con, id, applicationStatus, creditScroe, current_user_id, creditScroeCo, co_signer, callback) => {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    return new Promise((resolve, reject) => {
        let applyCredit = (co_signer == 1) ? creditScroeCo.creditScroe : creditScroe;
        approveScore(con, applyCredit, function (amount) {
            if (amount.status == 1) {
                let expiry_date = date.addDays(now, parseInt(amount.expiryDay));
                expiry_date = date.format(expiry_date, 'YYYY-MM-DD');
                var sql = "UPDATE `credit_applications` SET approve_amount=?, remaining_amount=?, status=?, score = ? , expiry_date=?, date_modified = ? , modified_by= ? WHERE application_id = ?";
                if (applicationStatus == 0) {
                    con.query(sql, [0, 0, applicationStatus, creditScroe, expiry_date, current_date, current_user_id, id], function (error) {
                        obj.status = 1;
                        callback(obj);
                        resolve();
                    })
                } else {
                    con.query(sql, [amount.result, amount.result, applicationStatus, creditScroe, expiry_date, current_date, current_user_id, id], function (error) {
                        obj.status = 1;
                        callback(obj);
                        resolve();
                    })
                }

            } else {
                var sql = "UPDATE `credit_applications` SET approve_amount=?, remaining_amount=?, status=?, score = ? , expiry_date=?, date_modified = ? , modified_by= ? WHERE application_id = ?";

                con.query(sql, [0, 0, applicationStatus, creditScroe, expiry_date, current_date, current_user_id, id], function (error) {
                    obj.status = 1;
                    callback(obj);
                    resolve();
                })
            }
        });
    })
}

var approveScore = function (con, creditScroe, callback) {
    var obj = {};
    var sql = "SELECT amount FROM `score_range` WHERE min_score <= ? AND max_score >= ?";
    con.query(sql, [creditScroe, creditScroe], function (error, scoreAmount) {
        if (error) {
            obj.status = 0;
            return callback(obj);
        } else {
            var sql = "SELECT master_data_values.value FROM `master_data_values` WHERE master_data_values.md_id=? AND master_data_values.status=? AND master_data_values.deleted_flag=?";
            con.query(sql, ['LineOfCredit Expiration', 1, 0], function (error, expiryDay) {
                if (error) {
                    obj.status = 0;
                    return callback(obj);
                } else {
                    obj.status = 1;
                    obj.result = (scoreAmount.length > 0) ? scoreAmount[0].amount : '';
                    obj.expiryDay = (expiryDay.length > 0) ? expiryDay[0].value : '';
                    return callback(obj);
                }
            })
        }
    });
}

var getPlans = function (con, id, amount, provider_id, callback) {

    con.query('SELECT credit_applications.application_id, credit_applications.patient_id, credit_applications.remaining_amount, credit_applications.override_amount, credit_applications.remaining_amount as amount, credit_applications.score, credit_applications.manual_term, credit_applications.manual_interest, '
        + 'DATE_FORMAT(credit_applications.date_created, "%d/%m/%Y") as date_created, provider.member_flag,provider.provider_id, patient_provider.provider_id as exist_provider, '
        + 'credit_applications.application_no,credit_applications.approve_amount,patient.patient_ac,patient.email,patient.f_name,patient.m_name,patient.l_name,patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,patient_address.phone_no,states.name, states.name AS state_name, '
        + 'co_signer.co_signer_id as co_patient_id,co_signer.f_name as co_first_name, co_signer.m_name as co_middle_name,co_signer.l_name as co_last_name, '
        + 'co_patient_address.address1 as co_address1,co_patient_address.address2 as co_address2,co_patient_address.City as co_City,co_patient_address.zip_code as co_zip_code,co_patient_address.phone_no as co_phone_no, co_states.name as co_state_name '

        + 'FROM credit_applications '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id AND patient_address.status=? AND patient_address.primary_address=? '
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'LEFT JOIN patient_provider ON patient_provider.patient_id=patient.patient_id '
        + 'LEFT JOIN provider ON provider.provider_id=patient_provider.provider_id '
        /***Co-signer join if avaible */
        + 'LEFT JOIN credit_app_cosigner ON credit_app_cosigner.application_id = credit_applications.application_id '
        + 'LEFT JOIN co_signer ON credit_app_cosigner.co_signer_id = co_signer.co_signer_id '
        + 'LEFT JOIN patient_address as co_patient_address ON co_patient_address.co_signer_id=co_signer.co_signer_id AND co_patient_address.primary_address=1 AND co_patient_address.status=1 '
        + 'LEFT JOIN states as co_states ON co_states.state_id=co_patient_address.state_id '

        + 'WHERE credit_applications.status=? AND credit_applications.delete_flag=? AND credit_applications.application_id=?',
        [
            1,
            1,
            1,
            0,
            id,
        ]
        , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Application details not found."
                }
                return callback(obj);
            } else {
                if (rows.length !== 0) {
                    rows[0].amount = amount;
                    // get APR according to credit score
                    con.query('SELECT interest_rate, risk_factor FROM mfs_apr '
                        + 'WHERE status = ? AND deleted_flag=? '
                        + 'AND FLOOR(SUBSTRING_INDEX(score, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(score, "-", -1)) >= ?',
                        [
                            1,
                            0,
                            rows[0].score,
                            rows[0].score
                        ]
                        , function (aprErr, aprData, fields) {
                            if (aprErr || aprData.length == 0) {
                                var obj = {
                                    "status": 0,
                                    "message": "Payment plan not found1"
                                }
                                return callback(obj);
                            } else {
                                con.query('SELECT risk_factor FROM master_data_values '
                                    + 'WHERE status = ? AND deleted_flag=? '
                                    + 'AND FLOOR(SUBSTRING_INDEX(value, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(value, "-", -1)) >= ?',
                                    [
                                        1,
                                        0,
                                        rows[0].amount,
                                        rows[0].amount
                                    ]
                                    , function (amtErr, amtData, fields) {
                                        if (amtErr || amtData.length == 0) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Payment plan not found2"
                                            }
                                            return callback(obj);
                                        } else {

                                            /*if( rows[0].manual_term !== null && newTerm <= rows[0].manual_term ) {
                                                var termArray = {
                                                    id: rows[0].manual_term,
                                                    interest_rate: rows[0].manual_interest,
                                                    term_month: rows[0].manual_term,
                                                }
                                                
                                                var termData = [];
                                                termData.push(termArray);
                                                var obj = {
                                                    "status": 1,
                                                    "package": termData,
                                                    "rows": rows,
                                                }
                                                return callback(obj);
                                            } else if(rows[0].manual_term !== null && newTerm > rows[0].manual_term) {
                                                var obj = {
                                                    "status": 0,
                                                    "message": "Payment plan not found"
                                                }
                                                return callback(obj);
                                            } else {*/

                                            con.query('SELECT master_data_values.mdv_id as id,master_data_values.value as term_month,master_data_values.risk_factor,DATE_FORMAT( discount_fee_ra.end_date, "%Y/%m/%d") as end_date FROM master_data_values '
                                                + 'INNER JOIN discount_fee_ra ON discount_fee_ra.loan_term_month=master_data_values.mdv_id '
                                                + 'INNER JOIN provider_discount_fee ON provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
                                                + 'WHERE master_data_values.status = ? AND master_data_values.deleted_flag=? '
                                                + 'AND provider_discount_fee.status = ? AND discount_fee_ra.start_date<=DATE_FORMAT(now(), "%Y-%m-%d") AND provider_discount_fee.provider_id=?',
                                                [
                                                    1,
                                                    0,
                                                    1,
                                                    provider_id,
                                                ]
                                                , function (termErr, termData, fields) {

                                                    let date = require('date-and-time');
                                                    var today = date.format(new Date(), 'YYYY/MM/DD');
                                                    termData = termData.filter(x => {
                                                        if (x.end_date == null || x.end_date >= today) {
                                                            return x
                                                        } else if (x.end_date >= today) {
                                                            return x
                                                        }

                                                    });

                                                    if (termErr || termData.length == 0) {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Payment plan not found3"
                                                        }
                                                        return callback(obj);
                                                    } else {
                                                        termData = termData.sort((a, b) => parseFloat(b.term_month) - parseFloat(a.term_month));
                                                        termData = termData && termData.map((data, idx) => {
                                                            if (data.risk_factor > 0) {
                                                                data.interest_rate = parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) * data.risk_factor + amtData[0].risk_factor).toFixed(2);
                                                            } else {
                                                                data.interest_rate = parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) + amtData[0].risk_factor).toFixed(2);
                                                            }

                                                            delete data.risk_factor;
                                                            return data;
                                                        });

                                                        termData = termData.filter((thing, index, self) =>
                                                            index === self.findIndex((t) => (
                                                                t.term_month === thing.term_month
                                                            ))
                                                        )

                                                        var obj = {
                                                            "status": 1,
                                                            "package": termData,
                                                            "rows": rows,
                                                        }
                                                        return callback(obj);
                                                    }
                                                })
                                            //}
                                        }
                                    })
                            }
                        })

                } else {
                    var obj = {
                        "status": 0,
                        "message": "Application not found or plan already created."
                    }
                    return callback(obj);
                }

            }
        })
}

var budgetPlan = function (con, id, amount, provider_id, monthly_amount, callback) {
    con.query('SELECT credit_applications.application_id, credit_applications.patient_id, credit_applications.remaining_amount, credit_applications.override_amount, credit_applications.remaining_amount as amount, credit_applications.score, credit_applications.manual_term, credit_applications.manual_interest, '
        + 'DATE_FORMAT(credit_applications.date_created, "%d/%m/%Y") as date_created, provider.member_flag,provider.provider_id, patient_provider.provider_id as exist_provider, '
        + 'credit_applications.application_no,credit_applications.approve_amount,patient.patient_ac,patient.email,patient.f_name,patient.m_name,patient.l_name,patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,patient_address.phone_no,states.name, states.name AS state_name, '
        + 'co_signer.co_signer_id as co_patient_id,co_signer.f_name as co_first_name, co_signer.m_name as co_middle_name,co_signer.l_name as co_last_name, '
        + 'co_patient_address.address1 as co_address1,co_patient_address.address2 as co_address2,co_patient_address.City as co_City,co_patient_address.zip_code as co_zip_code,co_patient_address.phone_no as co_phone_no, co_states.name as co_state_name '

        + 'FROM credit_applications '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id AND patient_address.status=? AND patient_address.primary_address=? '
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'LEFT JOIN patient_provider ON patient_provider.patient_id=patient.patient_id '
        + 'LEFT JOIN provider ON provider.provider_id=patient_provider.provider_id '
        /***Co-signer join if avaible */
        + 'LEFT JOIN credit_app_cosigner ON credit_app_cosigner.application_id = credit_applications.application_id '
        + 'LEFT JOIN co_signer ON credit_app_cosigner.co_signer_id = co_signer.co_signer_id '
        + 'LEFT JOIN patient_address as co_patient_address ON co_patient_address.co_signer_id=co_signer.co_signer_id AND co_patient_address.primary_address=1 AND co_patient_address.status=1 '
        + 'LEFT JOIN states as co_states ON co_states.state_id=co_patient_address.state_id '

        + 'WHERE credit_applications.status=? AND credit_applications.delete_flag=? AND credit_applications.application_id=?',
        [
            1,
            1,
            1,
            0,
            id,
        ]
        , function (error, rows, fields) {

            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Application details not found."
                }
                return callback(obj);
            } else {
                if (rows.length !== 0) {

                    rows[0].amount = amount;
                    // get APR according to credit score
                    con.query('SELECT interest_rate, risk_factor FROM mfs_apr '
                        + 'WHERE status = ? AND deleted_flag=? '
                        + 'AND FLOOR(SUBSTRING_INDEX(score, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(score, "-", -1)) >= ?',
                        [
                            1,
                            0,
                            rows[0].score,
                            rows[0].score
                        ]
                        , function (aprErr, aprData, fields) {

                            if (aprErr || aprData.length == 0) {
                                var obj = {
                                    "status": 0,
                                    "message": "Payment plan not found."
                                }
                                return callback(obj);
                            } else {

                                con.query('SELECT risk_factor FROM master_data_values '
                                    + 'WHERE status = ? AND deleted_flag=? '
                                    + 'AND FLOOR(SUBSTRING_INDEX(value, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(value, "-", -1)) >= ?',
                                    [
                                        1,
                                        0,
                                        rows[0].amount,
                                        rows[0].amount
                                    ]
                                    , function (amtErr, amtData, fields) {
                                        if (amtErr || amtData.length == 0) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Payment plan not found"
                                            }
                                            return callback(obj);
                                        } else {
                                            var newAPR = (aprData[0].interest_rate + aprData[0].risk_factor + amtData[0].risk_factor);
                                            var newAPRP = (aprData[0].interest_rate + aprData[0].risk_factor + amtData[0].risk_factor) / 100;
                                            var newTerm = -Math.log(parseFloat(((parseFloat(-newAPRP) * rows[0].amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(newAPRP) / 12));
                                            newTerm = Math.ceil(newTerm);


                                            if (rows[0].manual_term !== null && newTerm <= rows[0].manual_term) {
                                                var termArray = {
                                                    id: rows[0].manual_term,
                                                    interest_rate: rows[0].manual_interest,
                                                    newAPR: rows[0].manual_interest,
                                                    term_month: rows[0].manual_term,
                                                }

                                                var budgetTerm = [];
                                                budgetTerm.push(termArray);

                                                var obj = {
                                                    "status": 1,
                                                    "package": budgetTerm,
                                                    "rows": rows,
                                                }
                                                return callback(obj);
                                            } else if (rows[0].manual_term !== null && newTerm > rows[0].manual_term) {
                                                var obj = {
                                                    "status": 0,
                                                    "message": "Payment plan not found"
                                                }
                                                return callback(obj);
                                            } else {

                                                con.query('SELECT mdv_id FROM master_data_values '
                                                    + 'WHERE  master_data_values.status = ? AND master_data_values.deleted_flag=? AND md_id = ? AND value>=? LIMIT 1 ',
                                                    [
                                                        1,
                                                        0,
                                                        'Payment Term Month',
                                                        newTerm
                                                    ]
                                                    , function (termMErr, termMData, fields) {
                                                        if (termMErr || termMData.length == 0) {
                                                            var obj = {
                                                                "status": 0,
                                                                "message": "Payment plan not found"
                                                            }
                                                            return callback(obj);
                                                        } else {
                                                            con.query('SELECT master_data_values.mdv_id as id,master_data_values.value as term_month,DATE_FORMAT( discount_fee_ra.end_date, "%Y/%m/%d") as end_date,master_data_values.risk_factor FROM master_data_values '
                                                                + 'INNER JOIN discount_fee_ra ON discount_fee_ra.loan_term_month=master_data_values.mdv_id '
                                                                + 'INNER JOIN provider_discount_fee ON provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
                                                                + 'WHERE master_data_values.status = ? AND master_data_values.deleted_flag=? '
                                                                + 'AND provider_discount_fee.status = ? AND provider_discount_fee.provider_id=? AND master_data_values.mdv_id = ?',
                                                                [
                                                                    1,
                                                                    0,
                                                                    1,
                                                                    provider_id,
                                                                    termMData[0].mdv_id
                                                                ]
                                                                , function (termErr, termData, fields) {
                                                                    let date = require('date-and-time');
                                                                    var today = date.format(new Date(), 'YYYY/MM/DD');
                                                                    termData = termData.filter(x => {
                                                                        if (x.end_date == null || x.end_date >= today) {
                                                                            return x
                                                                        } else if (x.end_date >= today) {
                                                                            return x
                                                                        }

                                                                    });

                                                                    if (termErr || termData.length == 0) {
                                                                        var obj = {
                                                                            "status": 0,
                                                                            "message": "Payment plan not found"
                                                                        }
                                                                        return callback(obj);
                                                                    } else {
                                                                        if (termData[0].risk_factor > 0) {
                                                                            var termArray = {
                                                                                id: termData[0].id,
                                                                                interest_rate: parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) * termData[0].risk_factor + amtData[0].risk_factor).toFixed(2),
                                                                                newAPR: newAPR,
                                                                                term_month: newTerm,
                                                                            }
                                                                        } else {
                                                                            var termArray = {
                                                                                id: termData[0].id,
                                                                                interest_rate: parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) + amtData[0].risk_factor).toFixed(2),
                                                                                newAPR: newAPR,
                                                                                term_month: newTerm,
                                                                            }
                                                                        }

                                                                        var budgetTerm = [];
                                                                        budgetTerm.push(termArray);

                                                                        var obj = {
                                                                            "status": 1,
                                                                            "package": budgetTerm,
                                                                            "rows": rows,
                                                                        }
                                                                        return callback(obj);
                                                                    }
                                                                })
                                                        }

                                                    })
                                            }
                                        }
                                    })
                            }
                        })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Application not found or plan already created."
                    }
                    return callback(obj);
                }

            }
        })
}

var fetchDoctorName = function (con, doctor_id, callback) {
    var obj = {};
    con.query('SELECT f_name, l_name FROM doctors WHERE id = ?'
        , [doctor_id]
        , function (err, doctor_result) {
            if (err) {
                obj.status = 0;
                obj.message = "Doctor not found";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.doctor_name = doctor_result[0];
                return callback(obj);
            }
        })
}


var createPlan = function (con, creditDetails, planDetails, current_id, customerData, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    //var insert_rate_id = (creditDetails[0].member_flag == 1) ? null : planDetails[0].plan_id;
    var term_id = planDetails[0].term_month;
    var termMonth = planDetails[0].term_month;
    var orgInterest = planDetails[0].org_interest_rate;
    var firstMonth = planDetails[0].firstMonth;
    var lastMonth = planDetails[0].lastMonth;

    //var discounted_interest_rate = planDetails[0].interest_rate;
    var discounted_interest_rate = planDetails[0].interest_rate;//(parseFloat(planDetails[0].interest_rate) / parseFloat(planDetails[0].term_month) * 12).toFixed(2);
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO payment_plan SET '
            + 'term=?, '
            + 'loan_type=?, '
            + 'interest_rate=?, '
            + 'discounted_interest_rate=?, '
            + 'monthly_amount=?, '
            + 'last_month_amount=?, '
            + 'plan_number=((SELECT COALESCE(MAX(plan_number),11111)+1 AS dt FROM payment_plan as p)), '
            + 'application_id=?, '
            + 'doctor_id=?, '
            + 'provider_id=?, '
            + 'amount=?, '
            + 'remaining_amount=?, '
            + 'loan_amount=?, '
            + 'id_type=?, '
            + 'id_number=?, '
            + 'id_expiry_date=?, '
            + 'co_id_type=?, '
            + 'co_id_number=?, '
            + 'co_id_expiry_date=?, '
            + 'co_name_of_borrower=?, '
            + 'co_agreement_date=?, '
            + 'agreement_flag=?, '
            + 'name_of_borrower=?, '
            + 'agreement_date=?, '
            + 'plan_status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                termMonth,
                customerData.details.loan_type,
                orgInterest,
                discounted_interest_rate,
                firstMonth,
                lastMonth,
                creditDetails[0].application_id,
                customerData.details.doctor,
                customerData.provider_id,
                planDetails[0].totalAmount,
                customerData.details.loan_amount,
                customerData.details.loan_amount,
                capitalize(customerData.details.id_name),
                customerData.details.id_number,
                customerData.details.expiry_date,
                capitalize(customerData.details.co_id_name),
                customerData.details.co_id_number,
                (customerData.details.co_expiry_date != null) ? customerData.details.co_expiry_date : '',
                capitalize(customerData.details.co_name_of_borrower),
                customerData.details.co_agreement_date,
                customerData.details.agree_with,
                capitalize(customerData.details.name_of_borrower),
                customerData.details.agreement_date,
                4,
                current_date,
                current_date,
                current_id,
                current_id
            ]
            , function (err, resultplan) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Payment plan cant created please contact with administration.";
                        return callback(obj);
                    });
                } else {
                    var values = [];
                    planDetails[0].monthlyPlan.forEach(function (element, idx) {
                        //let nextDate = new Date(element.next_month);
                        var nextDate = element.next_month.split('/');
                        var nextDate = nextDate[2] + '-' + nextDate[0] + '-' + nextDate[1];
                        values.push([resultplan.insertId, element.perMonth, nextDate, current_date, current_date, current_id, current_id])
                    });
                    con.query('INSERT INTO pp_installments (pp_id,installment_amt,due_date,date_created,date_modified,created_by,modified_by) VALUES ?',
                        [
                            values
                        ]
                        , function (error, resultuser) {
                            if (error) {

                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Payment plan cant created please contact with administration.2";
                                    return callback(obj);
                                });
                            } else {
                                con.query("UPDATE `credit_applications` SET remaining_amount=?, date_modified = ? , modified_by= ? WHERE application_id = ?",
                                    [(creditDetails[0].remaining_amount - customerData.details.loan_amount), current_date, current_id, customerData.application_id],
                                    function (err) {
                                        if (err) {

                                            con.rollback(function () {

                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            providerDiscount(con, customerData.provider_id, customerData.details.loan_amount, term_id, function (discount) {

                                                if (discount.status == 1) {
                                                    con.query('INSERT INTO patient_procedure SET '
                                                        + 'provider_location_id=?, '
                                                        + 'provider_id=?, '
                                                        + 'doctor_id=?, '
                                                        + 'application_id=?, '
                                                        + 'pp_id=?, '
                                                        + 'procedure_amt=?, '
                                                        + 'procedure_date=?, '
                                                        + 'procedure_status=?, '
                                                        + 'hps_discount_amt=?, '
                                                        + 'date_created=?, '
                                                        + 'date_modified=?, '
                                                        + 'created_by=?, '
                                                        + 'modified_by=?',
                                                        [
                                                            customerData.details.provider_location,
                                                            customerData.provider_id,
                                                            customerData.details.doctor,
                                                            customerData.application_id,
                                                            resultplan.insertId,
                                                            customerData.details.procedure_amount,
                                                            customerData.details.procedure_date,
                                                            1,
                                                            discount.amount,
                                                            current_date,
                                                            current_date,
                                                            current_id,
                                                            current_id
                                                        ]
                                                        , function (err, resultpro) {

                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong please try again.";
                                                                    return callback(obj);
                                                                });
                                                            } else {
                                                                var updatepatitentID = creditDetails[0].patient_id;
                                                                existProviderPatient(con, customerData.provider_id, creditDetails[0].patient_id, function (exitsPatientProvider) {
                                                                    if (exitsPatientProvider.exists) {
                                                                        updatePatientSingleBank(con, customerData, updatepatitentID, current_id, function (bankDetails) {

                                                                            if (bankDetails.status == 0) {
                                                                                con.rollback(function () {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Payment plan created but bank details not updated yet..";
                                                                                    return callback(obj);
                                                                                });
                                                                            } else {
                                                                                con.commit(function (err) {
                                                                                    if (err) {
                                                                                        con.rollback(function () {
                                                                                            obj.status = 0;
                                                                                            obj.message = "Payment plan created but bank details not updated yet..";
                                                                                            return callback(obj);
                                                                                        });
                                                                                    } else {
                                                                                        obj.status = 1;
                                                                                        obj.message = "Payment plan created successfully.";
                                                                                        obj.paymentPlan = resultplan.insertId;
                                                                                        return callback(obj);
                                                                                    }
                                                                                });
                                                                            }
                                                                        })
                                                                        /*var sql = 'UPDATE patient SET '
                                                                            + 'bank_name = ? , '
                                                                            + 'bank_address = ? , '
                                                                            + 'rounting_no = ? , '
                                                                            + 'account_number = ? , '
                                                                            + 'account_name = ? , '
                                                                            + 'account_type = ? , '
                                                                            + 'date_modified = ? , modified_by= ? WHERE patient_id = ?';
                                                                        con.query(sql, [
                                                                            (customerData.details.bank_name != null) ? capitalize(customerData.details.bank_name) : '',
                                                                            (customerData.details.bank_address != null) ? capitalize(customerData.details.bank_address) : '',
                                                                            (customerData.details.rounting_no != null) ? customerData.details.rounting_no : '',
                                                                            (customerData.details.bank_ac != null) ? customerData.details.bank_ac : '',
                                                                            (customerData.details.account_name != null) ? capitalize(customerData.details.account_name) : '',
                                                                            (customerData.details.account_type != null) ? customerData.details.account_type : '',
                                                                            current_date,
                                                                            current_id,
                                                                            updatepatitentID], function (errorupdate) {
                                                                                if (errorupdate) {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Payment plan created but bank details not updated yet..";
                                                                                    return callback(obj);
                                                                                } else {
                                                                                    con.commit(function (err) {
                                                                                        if (err) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            obj.status = 1;
                                                                                            obj.message = "Payment plan created successfully.";
                                                                                            obj.paymentPlan = resultplan.insertId;
                                                                                            return callback(obj);
                                                                                        }
                                                                                    });

                                                                                }
                                                                            });*/

                                                                    } else {
                                                                        con.query('INSERT INTO patient_provider SET '
                                                                            + 'patient_id=?, '
                                                                            + 'provider_id=?, '
                                                                            + 'date_created=?, '
                                                                            + 'date_modified=?, '
                                                                            + 'created_by=?, '
                                                                            + 'modified_by=?',
                                                                            [
                                                                                updatepatitentID,
                                                                                customerData.provider_id,
                                                                                current_date,
                                                                                current_date,
                                                                                current_id,
                                                                                current_id
                                                                            ]
                                                                            , function (err, patientProvider) {
                                                                                if (err) {
                                                                                    con.rollback(function () {
                                                                                        obj.status = 0;
                                                                                        obj.message = "Something wrong please try again.";
                                                                                        return callback(obj);
                                                                                    });
                                                                                } else {
                                                                                    updatePatientSingleBank(con, customerData, updatepatitentID, current_id, function (bankDetails) {
                                                                                        if (bankDetails.status == 0) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            con.commit(function (err) {
                                                                                                if (err) {
                                                                                                    con.rollback(function () {
                                                                                                        obj.status = 0;
                                                                                                        obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                        return callback(obj);
                                                                                                    });
                                                                                                } else {
                                                                                                    obj.status = 1;
                                                                                                    obj.message = "Payment plan created successfully.";
                                                                                                    obj.paymentPlan = resultplan.insertId;
                                                                                                    return callback(obj);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    })
                                                                                    /*var sql = 'UPDATE patient SET '
                                                                                        + 'bank_name = ? , '
                                                                                        + 'bank_address = ? , '
                                                                                        + 'rounting_no = ? , '
                                                                                        + 'account_number = ? , '
                                                                                        + 'account_name = ? , '
                                                                                        + 'account_type = ? , '
                                                                                        + 'date_modified = ? , modified_by= ? WHERE patient_id = ?';
                                                                                    con.query(sql, [
                                                                                        capitalize(customerData.details.bank_name),
                                                                                        capitalize(customerData.details.bank_address),
                                                                                        customerData.details.rounting_no,
                                                                                        customerData.details.bank_ac,
                                                                                        capitalize(customerData.details.account_name),
                                                                                        customerData.details.account_type,
                                                                                        current_date,
                                                                                        current_id,
                                                                                        updatepatitentID], function (errorupdate) {
                                                                                            if (errorupdate) {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            } else {


                                                                                            }
                                                                                        });*/
                                                                                }
                                                                            })
                                                                    }
                                                                })

                                                            }
                                                        })
                                                }
                                            })
                                        }
                                    })


                            }
                        });
                }
            });
    });
}

var getSinglePlans = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT payment_plan.pp_id,payment_plan.provider_id,payment_plan.interest_rate_id,'
            + 'payment_plan.application_id,payment_plan.amount,payment_plan.plan_number,payment_plan.discounted_interest_rate,CONCAT(doctors.f_name," ",doctors.l_name) as doctors_name,'
            + 'interest_rate.mdv_interest_rate,interest_rate.mdv_payment_term_month,'
            + 'credit_applications.patient_id,credit_applications.application_no,credit_applications.score,payment_plan.amount AS credit_amount,payment_plan.loan_amount AS loan_amount,'
            + 'patient.f_name,patient.m_name,patient.l_name,provider.name,patient.peimary_phone,patient.patient_ac,'
            + 'patient_address.address1, patient_address.address2, patient_address.City,patient_address.zip_code, patient_address.county, patient_address.region_id,'
            + 'states.name AS region_name,'
            + 'pp_installments.installment_amt AS monthly_payment,'
            + 'DATE_FORMAT(payment_plan.date_created, "%d/%m/%Y") AS date_created,master_data_values.value AS interest_rate,master_data_values2.value AS payment_term, provider.member_flag '
            + 'FROM payment_plan '
            + 'INNER JOIN doctors ON doctors.id=payment_plan.doctor_id '
            + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id '
            /*+ 'INNER JOIN regions ON patient_address.region_id=regions.region_id '*/
            + 'INNER JOIN states ON patient_address.state_id=states.state_id '
            + 'INNER JOIN pp_installments ON payment_plan.pp_id=pp_installments.pp_id '
            + 'LEFT JOIN interest_rate ON interest_rate.id=payment_plan.interest_rate_id '
            + 'LEFT JOIN master_data_values AS master_data_values2 ON master_data_values2.mdv_id=interest_rate.mdv_payment_term_month '
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=interest_rate.mdv_interest_rate '
            + 'LEFT JOIN provider ON provider.provider_id=patient.provider_id '
            + 'WHERE '
            + 'payment_plan.pp_id=? LIMIT 1',
            [
                id
            ]
            , function (err, plan) {

                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Payment plan not found.";
                        return callback(obj);
                    });
                } else {

                    con.query('SELECT '
                        + 'pp_installments.pp_id, '
                        + 'pp_installments.pp_installment_id, '
                        + 'pp_installments.installment_amt, '
                        + 'DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
                        + 'pp_installments.due_date AS comparison_date, '
                        + 'pp_installments.paid_flag, '
                        + 'pp_installments.missed_flag, '
                        + 'pp_installments.late_flag '

                        + 'FROM pp_installments '
                        + 'WHERE pp_installments.pp_id = ?'
                        ,
                        [
                            plan[0].pp_id
                        ]
                        , function (err, pymntPlanDetails) {

                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Payment Details not foundddd.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.plan = plan;
                                    //obj.plan_details = planDetails;
                                    obj.payment_plan_details = pymntPlanDetails;
                                    return callback(obj);
                                });
                            }
                        });

                }
            });
    });
    var obj = {};
}

var viewApplcation = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,'
            + 'credit_applications.amount,credit_applications.override_amount,credit_applications.override_amount_cmt,credit_applications.score,approve_amount,remaining_amount,DATE_FORMAT(credit_applications.date_modified, "%m/%d/%Y") as authorization_date, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date,credit_applications.expiry_date_cmt,credit_applications.status as application_status,'
            + 'credit_applications.plan_flag,'
            + 'patient.profile_flag,patient.patient_ac,patient.f_name,patient.m_name,patient.l_name,patient.other_f_name,patient.other_m_name,patient.other_l_name,patient.status,'
            + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,patient.gender,patient.malling_address,patient.peimary_phone,patient.alternative_phone,patient.email,patient.secondary_email,patient.employment_status,patient.annual_income,patient.employer_name,DATE_FORMAT(patient.employer_since, "%m/%d/%Y") as employer_since,patient.employer_phone,patient.employer_email,patient.bank_name,patient.account_number,patient.bank_address,patient.rounting_no,patient.account_name,master_data_values.value as account_type, empType.value as emp_type_name, '
            + 'user.username as username,user.email_id as user_eamil,user.phone as user_phone1,patient.alternative_phone as user_phone2, '
            + 'credit_applications_factor.score AS experian_status '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'LEFT JOIN credit_applications_factor ON credit_applications_factor.applcation_id=credit_applications.application_id '
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=patient.account_type '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=patient.employment_type '
            + 'WHERE '
            + 'application_id=?',
            [
                id
            ]
            , function (err, planDetails) {

                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application not found.";
                        return callback(obj);
                    });
                } else {
                    //const aes256 = require('aes256');
                    //var ase256key = 'ramesh';
                    //var encrypted = aes256.encrypt(ase256key, plaintext);
                    //planDetails[0].ssn = aes256.decrypt(ase256key, planDetails[0].ssn);
                    //planDetails[0].dob = aes256.decrypt(ase256key, planDetails[0].dob);
                    //planDetails[0].ssn = planDetails[0].ssn.toString();
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    //let date = require('date-and-time');
                    //let now = new Date(planDetails[0].dob);
                    //planDetails[0].dob = date.format(now, 'DD/MM/YYYY');
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.appDetails = (planDetails) ? planDetails[0] : '';
                    con.query('SELECT '
                        + 'credit_sec_details.f_name, credit_sec_details.m_name, credit_sec_details.l_name, credit_sec_details.email, credit_sec_details.phone, credit_sec_details.relationship, credit_sec_details.address1, credit_sec_details.address2, credit_sec_details.city, credit_sec_details.state, credit_sec_details.country, credit_sec_details.zip_code,states.name as state_name,countries.name as country_name, master_data_values.value as relationship_name '
                        + 'FROM credit_sec_details '
                        + 'INNER JOIN states ON states.state_id=credit_sec_details.state '
                        + 'INNER JOIN countries ON countries.id=states.country_id '
                        + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_sec_details.relationship AND master_data_values.md_id=? '
                        + 'WHERE credit_sec_details.applcation_id = ? ORDER BY credit_sec_details.id DESC LIMIT 1',
                        [
                            'Relationship', id
                        ]
                        , function (err, secDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Application not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.query('SELECT '
                                    + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.county,patient_address.region_id,patient_address.address_time_period,patient_address.primary_address,patient_address.billing_address,patient_address.phone_no,patient_address.zip_code,states.name as state_name,countries.name as country_name,patient_address.state_id as state '
                                    + 'FROM patient_address '
                                    /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '
                                    + 'INNER JOIN states ON states.state_id=regions.state_id '*/
                                    + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                                    + 'INNER JOIN countries ON countries.id=states.country_id '
                                    + 'WHERE '
                                    + 'patient_id=? AND patient_address.status=?',
                                    [
                                        obj.appDetails.patient_id,
                                        1
                                    ]
                                    , function (err, planaddressDetails) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Application not found.";
                                                return callback(obj);
                                            });
                                        } else {
                                            con.query('SELECT '
                                                + '* '
                                                + 'FROM credit_applications_addressinformation '
                                                + 'WHERE '
                                                + 'applcation_id=?',
                                                [
                                                    id,
                                                ]
                                                , function (err, experianAddress) {
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Application not found.";
                                                            return callback(obj);
                                                        });
                                                    } else {
                                                        con.query('SELECT '
                                                            + '* '
                                                            + 'FROM credit_applications_employmentinformation '
                                                            + 'WHERE '
                                                            + 'applcation_id=?',
                                                            [
                                                                id,
                                                            ]
                                                            , function (err, experianEmployment) {
                                                                if (err) {
                                                                    con.rollback(function () {
                                                                        obj.status = 0;
                                                                        obj.message = "Application not found.";
                                                                        return callback(obj);
                                                                    });
                                                                } else {
                                                                    con.commit(function (err) {
                                                                        if (err) {
                                                                            con.rollback(function () {
                                                                                obj.status = 0;
                                                                                obj.message = "Something wrong please try again.";
                                                                            });
                                                                            return callback(obj);
                                                                        }
                                                                        obj.status = 1;
                                                                        obj.appAddress = planaddressDetails;
                                                                        obj.experianAddress = experianAddress;
                                                                        obj.experianEmployment = experianEmployment;
                                                                        obj.secDetails = (secDetails.length > 0) ? secDetails[0] : '';
                                                                        return callback(obj);
                                                                    });
                                                                }
                                                            })
                                                    }
                                                })

                                        }
                                    });
                            }
                        })

                }
            });



    });
}

var searchCustomer = function (con, data, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'patient.patient_id,patient.profile_flag,patient.patient_ac,patient.f_name as first_name,patient.secondary_email,'
            + 'patient.m_name as middle_name,patient.l_name as last_name,patient.alias_name,'
            + 'patient.other_f_name as alias_first_name,patient.other_m_name as alias_middle_name,'
            + 'patient.other_l_name as alias_last_name,'
            + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,'
            + 'patient.gender,patient.status,patient.malling_address,patient.peimary_phone,patient.alternative_phone,'
            + 'patient.employment_status as employed,patient.annual_income as annual_income,'
            + 'patient.employer_name as employer_name,patient.employer_since as employed_since,'
            + 'patient.employer_phone as employer_phone,patient.employer_email as employer_email,'
            + 'patient.employment_type as employment_type,patient.bank_name as bank_name,patient.account_number as bank_ac,'
            + 'patient.bank_address as bank_address,patient.rounting_no as rounting_no,patient.account_name as account_name,'
            + 'patient.account_type as account_type,patient.alternative_phone as phone_2, user.username, user.email_id as email, user.phone as phone_1 '
            + 'FROM patient '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'WHERE '
            + 'patient.patient_ac=? AND patient.f_name=? AND patient.l_name=? AND AES_DECRYPT(patient.dob, "ramesh_cogniz")=? AND AES_DECRYPT(patient.ssn, "ramesh_cogniz")=?',
            [
                data.account_no,
                data.exist_first_name,
                data.exist_last_name,
                data.exist_dob,
                data.exist_ssn,
            ]
            , function (err, planDetails) {

                if (err || planDetails.length == 0) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Customer not found.";
                        return callback(obj);
                    });
                } else {
                    //var encrypted = aes256.encrypt(ase256key, plaintext);
                    //planDetails[0].ssn = aes256.decrypt(ase256key, planDetails[0].ssn);
                    //planDetails[0].dob = aes256.decrypt(ase256key, planDetails[0].dob);
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    //let date = require('date-and-time');
                    //let now = new Date(planDetails[0].dob);
                    //planDetails[0].dob = date.format(now, 'DD/MM/YYYY');
                    planDetails[0].dob = planDetails[0].dob.toString();
                    planDetails[0].freeze_override = 0;
                    planDetails[0].freeze_code = '';
                    planDetails[0].amount = '';
                    planDetails[0].procedure_status = 1;
                    con.query('SELECT '
                        + 'f_name, m_name, l_name, email, phone, relationship, address1, address2, city, state, country, zip_code '
                        + 'FROM credit_sec_details '
                        + 'WHERE credit_sec_details.patient_id = ? ORDER BY id DESC LIMIT 1',
                        [
                            planDetails[0].patient_id
                        ]
                        , function (err, secDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Application not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.query('SELECT '
                                    + 'bank_name,bank_ac,bank_address,rounting_no,account_name,account_type,primary_bank '
                                    + 'FROM patient_bank_details '
                                    + 'WHERE '
                                    + 'status = 1 AND patient_id=?',
                                    [
                                        planDetails[0].patient_id
                                    ]
                                    , function (err, bankDetails) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Application not found.";
                                                return callback(obj);
                                            });
                                        } else {
                                            con.query('SELECT '
                                                + 'patient_address.address1 as address1,patient_address.address2 as address2,'
                                                + 'patient_address.City as city,patient_address.county as county,'
                                                /*+ 'patient_address.region_id as region,'*/
                                                + 'patient_address.address_time_period as how_long,'
                                                + 'patient_address.primary_address as primary_address,'
                                                + 'patient_address.billing_address as billing_address,'
                                                + 'patient_address.same_billing_flag as same_billing_flag,'
                                                + 'patient_address.phone_no as phone_no,'
                                                + 'patient_address.zip_code as zip_code,'
                                                + 'states.state_id as state,'
                                                + 'countries.id as country '
                                                + 'FROM patient_address '
                                                /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                                                + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                                                + 'INNER JOIN countries ON countries.id=states.country_id '
                                                + 'WHERE '
                                                + 'patient_address.status = 1 AND patient_id=?',
                                                [
                                                    planDetails[0].patient_id
                                                ]
                                                , function (err, planaddressDetails) {
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Application not found.";
                                                            return callback(obj);
                                                        });
                                                    } else {
                                                        con.commit(function (err) {
                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong please try again.";
                                                                });
                                                                return callback(obj);
                                                            }
                                                            planDetails = (planDetails) ? planDetails[0] : '';
                                                            planDetails.password = '*****';
                                                            planDetails.confirm_password = '*****';
                                                            var filterData = planaddressDetails.filter(x => x.same_billing_flag == 0 && x.billing_address == 1);

                                                            if (filterData.length > 0) {
                                                                planDetails.billing_address1 = filterData[0].address1;
                                                                planDetails.billing_address2 = filterData[0].address2;
                                                                planDetails.billing_country = filterData[0].country;
                                                                planDetails.billing_state = filterData[0].state;
                                                                planDetails.billing_region = filterData[0].region;
                                                                planDetails.billing_county = filterData[0].county;
                                                                planDetails.billing_city = filterData[0].city;
                                                                planDetails.billing_zip_code = filterData[0].zip_code;
                                                                planDetails.billing_how_long = filterData[0].how_long;
                                                                planDetails.billing_phone_no = filterData[0].phone_no;
                                                                planaddressDetails.splice(planaddressDetails.findIndex(e => e.same_billing_flag == 0 && e.billing_address == 1), 1);
                                                            } else {
                                                                planDetails.billing_address1 = "";
                                                                planDetails.billing_address2 = "";
                                                                planDetails.billing_country = "";
                                                                planDetails.billing_state = "";
                                                                planDetails.billing_region = "";
                                                                planDetails.billing_county = "";
                                                                planDetails.billing_city = "";
                                                                planDetails.billing_zip_code = "";
                                                                planDetails.billing_how_long = "";
                                                                planDetails.billing_phone_no = "";
                                                                planDetails.sameBilling = false;
                                                            }
                                                            obj.status = 1;
                                                            obj.customerAddress = planaddressDetails;
                                                            obj.customerDetails = planDetails;
                                                            obj.bankDetails = bankDetails;
                                                            obj.secDetails = (secDetails.length > 0) ? secDetails[0] : '';
                                                            return callback(obj);
                                                        });
                                                    }
                                                });
                                        }
                                    })
                            }
                        })




                }
            });



    });
}

var existCustomer = async (con, data, coSigner, callback) => {
    return new Promise((resolve, reject) => {
        if (data.patient_id === undefined || data.co_patient_id === undefined) {
            con.query('SELECT patient_id,dob,ssn FROM patient WHERE f_name=? AND l_name=? AND AES_DECRYPT(patient.dob, "ramesh_cogniz")=? AND AES_DECRYPT(patient.ssn, "ramesh_cogniz")=?',
                [
                    (coSigner == 1) ? data.co_first_name : data.first_name,
                    (coSigner == 1) ? data.co_last_name : data.last_name,
                    (coSigner == 1) ? data.co_dob : data.dob,
                    (coSigner == 1) ? data.co_ssn : data.ssn,
                ]
                , function (err, existCustoemr) {
                    var patient_id = (existCustoemr.length > 0) ? existCustoemr[0].patient_id : '';
                    callback(patient_id);
                    resolve();
                });
        } else {
            callback((coSigner == 1) ? data.co_patient_id : data.patient_id);
            resolve();
        }
    })
}
var reviewApplcation = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT credit_applications.application_no,credit_applications.approve_amount,credit_applications.score,credit_applications.patient_id,patient.provider_id FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'WHERE '
            + 'credit_applications.application_id=?',
            [
                id
            ]
            , function (err, appDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application not found.";
                        return callback(obj);
                    });
                } else {
                    con.query('SELECT modelIndicator,score,evaluation,importance,code FROM credit_applications_factor '
                        + 'WHERE '
                        + 'applcation_id=?',
                        [
                            id,
                        ]
                        , function (err, apiDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Experian Risk Model not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.query('select mdv_id, value AS term_month, description AS term_month_desc, risk_factor, status FROM master_data_values WHERE deleted_flag = 0 AND md_id = "Payment Term Month" AND value <= 12 ORDER BY mdv_id ASC', function (error, terms, fields) {
                                    if (error) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Term not found.";
                                            return callback(obj);
                                        });
                                    } else {

                                        var dataScore = require('./creditScore.js');
                                        dataScore.getRiskFactor(con, function (response) {
                                            if (response.status == 1) {
                                                obj.status = 1;
                                                obj.appDetails = appDetails;
                                                obj.apiRiskFactor = apiDetails;
                                                obj.hpsRiskFactor = response.result;
                                                obj.term_month = terms;
                                                return callback(obj);
                                            } else {
                                                obj.status = 1;
                                                obj.appDetails = appDetails;
                                                obj.apiRiskFactor = apiDetails;
                                                obj.hpsRiskFactor = '';
                                                obj.term_month = terms;
                                                return callback(obj);
                                            }
                                        });

                                    }

                                })


                            }

                        });

                }

            });
    });
}
var manualAction = function (con, data, callback) {

    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let status = 0;
    let message = '';
    if (data.type == 1) {
        status = 1;
        message = 'Application approved successfully.';
    } else if (data.type == 2) {
        status = 0;
        message = 'Application rejected.';
    }
    else if (data.type == 3) {
        status = 4;
        message = 'Application under process for verification.';
    }
    if (status == 0) {
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            con.query('UPDATE credit_applications SET '
                + 'approve_amount=?, '
                + 'remaining_amount=?, '
                + 'note=?, '
                + 'status=?, '
                + 'date_modified=?, '
                + 'modified_by=? '
                + 'WHERE '
                + 'application_id=?',
                [
                    0,
                    0,
                    capitalize(data.comment),
                    status,
                    current_date,
                    data.current_user_id,
                    data.id,
                ]
                , function (err, appDetails) {

                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            return callback(obj);
                        });
                    } else {
                        if (data.type) {
                            con.query('DELETE FROM patient_bank_details '
                                + 'WHERE `patient_id` IN (SELECT patient_id FROM credit_applications WHERE application_id=?) OR co_signer_id IN (SELECT co_signer_id FROM credit_app_cosigner WHERE application_id=?) ',
                                [
                                    data.id,
                                    data.id,
                                ]
                                , function (err, emailDetails) {

                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                            return callback(obj);
                                        });
                                    } else {
                                        con.query('SELECT patient.email,patient.f_name,patient.m_name,patient.l_name FROM '
                                            + 'patient '
                                            + 'INNER JOIN credit_applications ON patient.patient_id=credit_applications.patient_id '
                                            + 'WHERE '
                                            + 'credit_applications.application_id=?',
                                            [
                                                data.id,
                                            ]
                                            , function (err, emailDetails) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong please try again.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    con.commit(function (err) {
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong please try again.";
                                                                return callback(obj);
                                                            });
                                                        }
                                                        var patientEmail = (Object.keys(emailDetails).length) ? emailDetails[0] : '';
                                                        obj.status = 1;
                                                        obj.message = message;
                                                        obj.type = data.type;
                                                        obj.email = patientEmail;
                                                        return callback(obj);
                                                    });
                                                }
                                            })
                                    }
                                })
                        } else {
                            con.query('SELECT patient.email,patient.f_name,patient.m_name,patient.l_name FROM '
                                + 'patient '
                                + 'INNER JOIN credit_applications ON patient.patient_id=credit_applications.patient_id '
                                + 'WHERE '
                                + 'credit_applications.application_id=?',
                                [
                                    data.id,
                                ]
                                , function (err, emailDetails) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                            return callback(obj);
                                        });
                                    } else {
                                        con.commit(function (err) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    return callback(obj);
                                                });
                                            }
                                            var patientEmail = (Object.keys(emailDetails).length) ? emailDetails[0] : '';
                                            obj.status = 1;
                                            obj.message = message;
                                            obj.type = data.type;
                                            obj.email = patientEmail;
                                            return callback(obj);
                                        });
                                    }
                                })
                        }
                    }
                });
        });
    } else {
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            if (data.approved_amt === undefined && data.credit_score === undefined) {
                con.query('UPDATE credit_applications SET '
                    + 'note=?, '
                    + 'status=?, '
                    + 'date_modified=?, '
                    + 'modified_by=? '
                    + 'WHERE '
                    + 'application_id=?',
                    [
                        capitalize(data.comment),
                        status,
                        current_date,
                        data.current_user_id,
                        data.id,
                    ]
                    , function (err, appDetails) {
                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.1";
                                return callback(obj);
                            });
                        } else {
                            con.query('SELECT patient.email,patient.f_name,patient.m_name,patient.l_name FROM '
                                + 'patient '
                                + 'INNER JOIN credit_applications ON patient.patient_id=credit_applications.patient_id '
                                + 'WHERE '
                                + 'credit_applications.application_id=?',
                                [
                                    data.id,
                                ]
                                , function (err, emailDetails) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.2";
                                            return callback(obj);
                                        });
                                    } else {
                                        con.commit(function (err) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.3";
                                                    return callback(obj);
                                                });
                                            }
                                            var patientEmail = (Object.keys(emailDetails).length) ? emailDetails[0] : '';
                                            obj.status = 1;
                                            obj.message = message;
                                            obj.type = data.type;
                                            obj.email = patientEmail;
                                            return callback(obj);
                                        });
                                    }
                                })

                        }
                    });
            } else {
                con.query('UPDATE credit_applications SET '
                    + 'approve_amount=?, '
                    + 'remaining_amount=?, '
                    + 'score=?, '
                    + 'manual_term=?, '
                    + 'manual_interest=?, '
                    + 'note=?, '
                    + 'status=?, '
                    + 'date_modified=?, '
                    + 'modified_by=? '
                    + 'WHERE '
                    + 'application_id=?',
                    [
                        data.approved_amt,
                        data.approved_amt,
                        data.credit_score,
                        data.manual_term,
                        data.manual_interest,
                        capitalize(data.comment),
                        status,
                        current_date,
                        data.current_user_id,
                        data.id,
                    ]
                    , function (err, appDetails) {

                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.4";
                                return callback(obj);
                            });
                        } else {
                            con.query('SELECT patient.email,patient.f_name,patient.m_name,patient.l_name FROM '
                                + 'patient '
                                + 'INNER JOIN credit_applications ON patient.patient_id=credit_applications.patient_id '
                                + 'WHERE '
                                + 'credit_applications.application_id=?',
                                [
                                    data.id,
                                ]
                                , function (err, emailDetails) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.5";
                                            return callback(obj);
                                        });
                                    } else {
                                        con.commit(function (err) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.6";
                                                    return callback(obj);
                                                });
                                            }
                                            var patientEmail = (Object.keys(emailDetails).length) ? emailDetails[0] : '';
                                            obj.status = 1;
                                            obj.message = message;
                                            obj.type = data.type;
                                            obj.email = patientEmail;
                                            return callback(obj);
                                        });
                                    }
                                })

                        }
                    });
            }
        });
    }


}
var rejectApplcation = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,'
            + 'credit_applications.approve_amount,credit_applications.score,credit_applications.note, '
            + 'credit_applications.plan_flag,'
            + 'patient.profile_flag,patient.patient_ac,patient.f_name,patient.m_name,patient.l_name,patient.other_f_name,patient.other_m_name,patient.other_l_name,patient.status, '
            + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,patient.gender,patient.malling_address,patient.peimary_phone,patient.alternative_phone,patient.email,patient.employment_status,patient.annual_income,patient.employer_name,DATE_FORMAT(patient.employer_since, "%m/%d/%Y") as employer_since,patient.employer_phone,patient.employer_email,patient.bank_name,patient.account_number,patient.bank_address,patient.rounting_no,patient.account_name,master_data_values.value as account_type, empType.value as emp_type_name, '
            + 'user.username as username,user.email_id as user_eamil,user.phone as user_phone1,patient.alternative_phone as user_phone2 '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=patient.account_type '
            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=patient.employment_type '
            + 'WHERE '
            + 'application_id=?',
            [
                id
            ]
            , function (err, planDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application not found.";
                        return callback(obj);
                    });
                } else {
                    //const aes256 = require('aes256');
                    //var ase256key = 'ramesh';
                    //var encrypted = aes256.encrypt(ase256key, plaintext);
                    //planDetails[0].ssn = aes256.decrypt(ase256key, planDetails[0].ssn);
                    //planDetails[0].dob = aes256.decrypt(ase256key, planDetails[0].dob);
                    //planDetails[0].ssn = planDetails[0].ssn.toString();
                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                    //let date = require('date-and-time');
                    //let now = new Date(planDetails[0].dob);
                    //planDetails[0].dob = date.format(now, 'DD/MM/YYYY');
                    let moment = require('moment');
                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                    obj.appDetails = (planDetails) ? planDetails[0] : '';
                    con.query('SELECT '
                        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.county,patient_address.region_id,patient_address.address_time_period,patient_address.primary_address,patient_address.billing_address,patient_address.phone_no,patient_address.zip_code,states.name as state_name,countries.name as country_name '
                        + 'FROM patient_address '
                        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
                        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
                        /*+ 'INNER JOIN states ON states.state_id=regions.state_id '*/
                        + 'INNER JOIN countries ON countries.id=states.country_id '
                        + 'WHERE '
                        + 'patient_id=? AND patient_address.status=? AND patient_address.primary_address=?',
                        [
                            obj.appDetails.patient_id,
                            1,
                            1
                        ]
                        , function (err, planaddressDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Application not found.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.appAddress = planaddressDetails;
                                    return callback(obj);
                                });
                            }
                        });
                }
            });



    });
}
var uploadDocumentApplcation = function (con, data, file, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO `doc_repo` SET '
            + 'application_id=?, '
            + 'pp_id=?, '
            + 'name=?, '
            + 'file_path=?, '
            + 'item_id=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                data.application_id,
                (data.pp_id) ? data.pp_id : null,
                file.filename,
                file.path,
                file.item_id,
                current_date,
                current_date,
                data.current_user_id,
                data.current_user_id
            ]
            , function (err, planDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Sorry, we are not able to upload documents right now.";
                        return callback(obj);
                    });
                } else {
                    con.query('UPDATE credit_applications SET '
                        + 'status=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE '
                        + 'application_id=?',
                        [
                            5,
                            current_date,
                            data.current_user_id,
                            data.application_id,
                        ]
                        , function (err, appDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Document uploaded but application not updated.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.message = "Document has been uploaded successfully";
                                    return callback(obj);
                                });
                            }
                        })
                }
            });
    });
}
var getDocumentApplcation = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT application_no,approve_amount,score FROM credit_applications WHERE '
            + 'application_id=?',
            [
                id
            ]
            , function (err, appDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Application not found.";
                        return callback(obj);
                    });
                } else {
                    con.query('SELECT name,file_path,item_id FROM doc_repo WHERE '
                        + 'application_id=? AND status=?',
                        [
                            id,
                            1
                        ]
                        , function (err, docDetails) {
                            con.commit(function (err) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Sorry, we are not able to get documents right now.";
                                    });
                                    return callback(obj);
                                }
                                obj.status = 1;
                                obj.appDocDetails = (appDetails.length > 0) ? appDetails[0] : '';
                                obj.docDetails = docDetails;
                                return callback(obj);
                            });
                        });
                }
            });
    });
}
var uploadAgrementApplcation = function (con, data, file, callback) {

    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO `doc_repo` SET '
            + 'pp_id=?, '
            + 'name=?, '
            + 'file_path=?, '
            + 'item_id=?, '
            + 'agrement_flag=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                data.pp_id,
                file.filename,
                file.path,
                file.item_id,
                1,
                current_date,
                current_date,
                data.current_user_id,
                data.current_user_id
            ]
            , function (err, planDetails) {

                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Sorry, we are not able to upload documents right now.";
                        return callback(obj);
                    });
                } else {
                    con.query('UPDATE credit_applications SET '
                        + 'document_flag=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE '
                        + 'application_id=?',
                        [
                            1,
                            current_date,
                            data.current_user_id,
                            data.application_id,
                        ]
                        , function (err, appDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Document uploaded but application not updated.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.message = "Document has been uploaded successfully";
                                    return callback(obj);
                                });
                            }
                        })
                }
            });
    });
}

var customerUpload = function (con, data, file, callback) {

    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.query('INSERT INTO `doc_repo` SET '
        + 'pp_id=?, '
        + 'name=?, '
        + 'file_path=?, '
        + 'item_id=?, '
        + 'agrement_flag=?, '
        + 'date_created=?, '
        + 'date_modified=?, '
        + 'created_by=?, '
        + 'modified_by=?',
        [
            data.pp_id,
            file.filename,
            file.path,
            file.item_id,
            1,
            current_date,
            current_date,
            data.current_user_id,
            data.current_user_id
        ]
        , function (err, planDetails) {
            console.log(err)
            if (err) {
                obj.status = 0;
                obj.message = "Sorry, we are not able to upload documents right now.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.message = "Document has been uploaded successfully";
                return callback(obj);
            }
        });
}

var uploadRejectionAgrementApplcation = function (con, data, file, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

    //return false;
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO `doc_repo` SET '
            + 'application_id=?, '
            + 'name=?, '
            + 'file_path=?, '
            + 'item_id=?, '
            + 'agrement_flag=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                data.application_id,
                file.filename,
                file.path,
                file.item_id,
                1,
                current_date,
                current_date,
                data.current_user_id,
                data.current_user_id
            ]
            , function (err, planDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Sorry, we are not able to upload documents right now.";
                        return callback(obj);
                    });
                } else {
                    con.query('UPDATE credit_applications SET '
                        + 'document_flag=?, '
                        + 'date_modified=?, '
                        + 'modified_by=? '
                        + 'WHERE '
                        + 'application_id=?',
                        [
                            1,
                            current_date,
                            data.current_user_id,
                            data.application_id,
                        ]
                        , function (err, appDetails) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Document uploaded but application not updated.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.message = "Document has been uploaded successfully";
                                    return callback(obj);
                                });
                            }
                        })
                }
            });
    });
}

var downloadAgrementApplcation = function (con, data, callback) {
    var obj = {};
    con.query('SELECT file_path FROM doc_repo '
        + 'WHERE '
        + 'application_id=? AND agrement_flag=?',
        [
            data.application_id,
            1
        ]
        , function (err, doc) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                    return callback(obj);
                });
            } else {
                doc = (doc.length > 0) ? doc[0] : ''
                obj.status = 1;
                obj.result = doc;
                return callback(obj);
            }
        })
}
var searchApplication = function (con, data, callback) {
    var obj = {};
    var cond = 'credit_applications.status=1';
    if (data.application_no != '') {
        cond = cond + ' AND credit_applications.application_no="' + data.application_no + '"';
    }
    if (data.exist_first_name != '') {
        cond = cond + ' AND patient.f_name="' + data.exist_first_name + '"';
    }
    if (data.exist_last_name != '') {
        cond = cond + ' AND patient.l_name="' + data.exist_last_name + '"';
    }
    if (data.exist_dob != '') {
        cond = cond + ' AND AES_DECRYPT(patient.dob, "ramesh_cogniz")="' + data.exist_dob + '"';
    }
    if (data.exist_ssn != '') {
        cond = cond + ' AND AES_DECRYPT(patient.ssn, "ramesh_cogniz")="' + data.exist_ssn + '"';
    }
    if (data.account_no != '') {
        cond = cond + ' AND patient.patient_ac ="' + data.account_no + '"';
    }
    con.query('SELECT '
        + 'patient.patient_id,patient.patient_ac,patient.f_name as first_name,'
        + 'patient.m_name as middle_name,patient.l_name as last_name,patient.alias_name,'
        //+ 'patient.bank_name,patient.bank_address,patient.rounting_no,patient.account_number,patient.account_name,patient.account_type,'
        + 'patient.malling_address,patient.peimary_phone, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name,'
        + 'credit_applications.application_id, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date, credit_applications.application_no, credit_applications.approve_amount, (credit_applications.remaining_amount + credit_applications.override_amount) AS remaining_amount, '
        + 'patient_bank_details.bank_id,patient_bank_details.bank_name,patient_bank_details.bank_ac,patient_bank_details.bank_address,patient_bank_details.rounting_no,patient_bank_details.account_name,patient_bank_details.account_type,patient_bank_details.primary_bank,'
        + 'master_data_values.value '
        + 'FROM patient '
        + 'INNER JOIN credit_applications ON credit_applications.patient_id=patient.patient_id '
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'LEFT JOIN patient_bank_details ON patient_bank_details.patient_id = patient.patient_id AND patient_bank_details.status = 1 AND patient_bank_details.primary_bank=1 '
        + 'LEFT JOIN master_data_values ON master_data_values.mdv_id = patient_bank_details.account_type '
        + 'WHERE ' + cond + 'ORDER BY credit_applications.application_id DESC'
        , function (err, applicationDetails) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Application not found.";
                    return callback(obj);
                });
            } else {
                obj.status = 1;
                obj.customerDetails = (applicationDetails.length > 0) ? applicationDetails : '';
                return callback(obj);
            }
        });
}
var applicationDetails = function (con, id, callback) {
    con.query('SELECT credit_applications.application_id, credit_applications.remaining_amount, credit_applications.approve_amount, credit_applications.score, '
        + 'DATE_FORMAT(credit_applications.date_created, "%d/%m/%Y") as date_created, provider.member_flag '
        + 'FROM credit_applications '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        + 'LEFT JOIN provider ON provider.provider_id=patient.provider_id '
        + 'WHERE credit_applications.status=? AND credit_applications.delete_flag=? AND credit_applications.application_id=? AND plan_flag=?',
        [
            1,
            0,
            id,
            0
        ]
        , function (error, rows, fields) {
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Application details not found."
                }
                return callback(obj);
            } else {
                var obj = {
                    "status": 1,
                    "rows": rows,
                }
                return callback(obj);
            }
        })
}

var updateRemaining = function (con, application_id, amount, current_user_id, provider_id, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var sql = "UPDATE `credit_applications` SET remaining_amount=?, date_modified = ? , modified_by= ? WHERE application_id = ?";
    con.query(sql, [amount, current_date, current_user_id, application_id], function (error) {
        if (error) {
            var obj = {
                "status": 0,
            }
            return callback(obj);
        } else {
            var obj = {
                "status": 0,
            }
            return callback(obj);
        }
    })

}
var getEstPlans = function (con, data, provider_id, callback) {
    con.query('SELECT interest_rate AS apr, risk_factor FROM mfs_apr '
        + 'WHERE status = ? AND deleted_flag=? '
        + 'AND FLOOR(SUBSTRING_INDEX(score, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(score, "-", -1)) >= ?',
        [
            1,
            0,
            data.score,
            data.score
        ]
        , function (aprErr, aprData, fields) {
            if (aprErr || aprData.length == 0) {
                var obj = {
                    "status": 0,
                    "message": "Payment plan not found"
                }
                return callback(obj);
            } else {
                con.query('SELECT risk_factor FROM master_data_values '
                    + 'WHERE status = ? AND deleted_flag=? '
                    + 'AND FLOOR(SUBSTRING_INDEX(value, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(value, "-", -1)) >= ?',
                    [
                        1,
                        0,
                        data.amount,
                        data.amount
                    ]
                    , function (amtErr, amtData, fields) {
                        if (amtErr || amtData.length == 0) {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found"
                            }
                            return callback(obj);
                        } else {

                            con.query('SELECT master_data_values.mdv_id as id,master_data_values.value as term_month,DATE_FORMAT( discount_fee_ra.end_date, "%Y/%m/%d") as end_date,master_data_values.risk_factor FROM master_data_values '
                                + 'INNER JOIN discount_fee_ra ON discount_fee_ra.loan_term_month=master_data_values.mdv_id '
                                + 'INNER JOIN provider_discount_fee ON provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
                                + 'WHERE master_data_values.status = ? AND master_data_values.deleted_flag=? '
                                + 'AND provider_discount_fee.status = ? AND discount_fee_ra.start_date<=DATE_FORMAT(now(), "%Y-%m-%d") AND provider_discount_fee.provider_id=?',
                                [
                                    1,
                                    0,
                                    1,
                                    provider_id,
                                ]
                                , function (termErr, termData, fields) {
                                    let date = require('date-and-time');
                                    var today = date.format(new Date(), 'YYYY/MM/DD');

                                    termData = termData.filter(x => {
                                        if (x.end_date == null || x.end_date >= today) {
                                            return x
                                        } else if (x.end_date >= today) {
                                            return x
                                        }

                                    });

                                    if (termErr || termData.length == 0) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Payment plan not found"
                                        }
                                        return callback(obj);
                                    } else {
                                        termData = termData.sort((a, b) => parseFloat(b.term_month) - parseFloat(a.term_month));
                                        termData = termData && termData.map((data, idx) => {
                                            if (data.risk_factor > 0) {
                                                data.interest_rate = parseFloat((aprData[0].apr + aprData[0].risk_factor) * data.risk_factor + amtData[0].risk_factor).toFixed(2);
                                            } else {
                                                data.interest_rate = parseFloat((aprData[0].apr + aprData[0].risk_factor) + amtData[0].risk_factor).toFixed(2);
                                            }
                                            delete data.risk_factor;
                                            return data;
                                        });

                                        termData = termData.filter((thing, index, self) =>
                                            index === self.findIndex((t) => (
                                                t.term_month === thing.term_month
                                            ))
                                        )

                                        var obj = {
                                            "status": 1,
                                            "package": termData,
                                        }
                                        return callback(obj);
                                    }
                                })
                        }
                    })
            }
        })

}
var budgetEstPlans = function (con, data, provider_id, monthly_amount, callback) {

    con.query('SELECT interest_rate, risk_factor FROM mfs_apr '
        + 'WHERE status = ? AND deleted_flag=? '
        + 'AND FLOOR(SUBSTRING_INDEX(score, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(score, "-", -1)) >= ?',
        [
            1,
            0,
            data.score,
            data.score
        ]
        , function (aprErr, aprData, fields) {

            if (aprErr || aprData.length == 0) {
                var obj = {
                    "status": 0,
                    "message": "Payment plan not found"
                }
                return callback(obj);
            } else {
                con.query('SELECT risk_factor FROM master_data_values '
                    + 'WHERE status = ? AND deleted_flag=? '
                    + 'AND FLOOR(SUBSTRING_INDEX(value, "-", 1)) <= ? AND FLOOR(SUBSTRING_INDEX(value, "-", -1)) >= ?',
                    [
                        1,
                        0,
                        data.amount,
                        data.amount
                    ]
                    , function (amtErr, amtData, fields) {

                        if (amtErr || amtData.length == 0) {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found"
                            }
                            return callback(obj);
                        } else {

                            var newAPR = (aprData[0].interest_rate + aprData[0].risk_factor + amtData[0].risk_factor);
                            var newAPRP = (aprData[0].interest_rate + aprData[0].risk_factor + amtData[0].risk_factor) / 100;

                            var newTerm = -Math.log(parseFloat(((parseFloat(-newAPRP) * data.amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(newAPRP) / 12));
                            newTerm = Math.ceil(newTerm);

                            con.query('SELECT mdv_id FROM master_data_values '
                                + 'WHERE  master_data_values.status = ? AND master_data_values.deleted_flag=? AND md_id = ? AND value>=? LIMIT 1 ',
                                [
                                    1,
                                    0,
                                    'Payment Term Month',
                                    newTerm
                                ]
                                , function (termMErr, termMData, fields) {
                                    if (termMErr || termMData.length == 0) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Payment plan not found"
                                        }
                                        return callback(obj);
                                    } else {
                                        con.query('SELECT master_data_values.mdv_id as id,master_data_values.value as term_month,DATE_FORMAT( discount_fee_ra.end_date, "%Y/%m/%d") as end_date,master_data_values.risk_factor FROM master_data_values '
                                            + 'INNER JOIN discount_fee_ra ON discount_fee_ra.loan_term_month=master_data_values.mdv_id '
                                            + 'INNER JOIN provider_discount_fee ON provider_discount_fee.df_ra_id=discount_fee_ra.df_ra_id '
                                            + 'WHERE master_data_values.status = ? AND master_data_values.deleted_flag=? '
                                            + 'AND provider_discount_fee.status = ? AND provider_discount_fee.provider_id=? AND master_data_values.mdv_id = ?',
                                            [
                                                1,
                                                0,
                                                1,
                                                provider_id,
                                                termMData[0].mdv_id
                                            ]
                                            , function (termErr, termData, fields) {
                                                let date = require('date-and-time');
                                                var today = date.format(new Date(), 'YYYY/MM/DD');
                                                termData = termData.filter(x => {
                                                    if (x.end_date == null || x.end_date >= today) {
                                                        return x
                                                    } else if (x.end_date >= today) {
                                                        return x
                                                    }

                                                });
                                                if (termErr || termData.length == 0) {
                                                    var obj = {
                                                        "status": 0,
                                                        "message": "Payment plan not found"
                                                    }
                                                    return callback(obj);
                                                } else {
                                                    if (termData[0].risk_factor > 0) {
                                                        var termArray = {
                                                            id: termData[0].id,
                                                            interest_rate: parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) * termData[0].risk_factor + amtData[0].risk_factor).toFixed(2),
                                                            newAPR: newAPR,
                                                            term_month: newTerm,
                                                        }
                                                    } else {
                                                        var termArray = {
                                                            id: termData[0].id,
                                                            interest_rate: parseFloat((aprData[0].interest_rate + aprData[0].risk_factor) + amtData[0].risk_factor).toFixed(2),
                                                            newAPR: newAPR,
                                                            term_month: newTerm,
                                                        }
                                                    }

                                                    var budgetTerm = [];
                                                    budgetTerm.push(termArray);
                                                    var obj = {
                                                        "status": 1,
                                                        "package": budgetTerm,
                                                    }
                                                    return callback(obj);
                                                }
                                            })
                                    }

                                })
                        }
                    })
            }
        })








}

var getApplicationDetails = function (con, id, callback) {
    return new Promise((resolve, reject) => {
        con.query('SELECT credit_applications.patient_id,credit_applications.application_id,credit_applications.application_no,DATE_FORMAT(credit_applications.date_modified, "%m/%d/%Y") as authorization_date, DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date, credit_applications.approve_amount, '
            + 'patient.patient_ac, patient.f_name, patient.m_name, patient.l_name, patient.email, '
            + 'patient_address.address1, patient_address.address2, patient_address.City, patient_address.zip_code,patient_address.phone_no, '
            + 'states.name '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_address ON patient.patient_id=patient_address.patient_id AND patient_address.status=? AND patient_address.primary_address=? '
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'WHERE credit_applications.application_id= ? ',
            [
                1,
                1,
                id
            ]
            , function (err, row, fields) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found"
                    }
                    callback(obj);
                    resolve();
                } else {
                    var obj = {
                        "status": 1,
                        "application": (row.length > 0) ? row[0] : '',
                    }
                    callback(obj);
                    resolve();
                }
            });
    })
}

var getAllDocumentApplcation = function (con, id, callback) {

    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'payment_plan.pp_id, '
            + 'payment_plan.plan_number, '
            + 'doc_repo.file_path, '
            + 'doc_repo.name, '
            + 'doc_repo.item_id '
            + 'FROM doc_repo '
            + 'LEFT JOIN payment_plan ON payment_plan.pp_id=doc_repo.pp_id '
            + 'LEFT JOIN credit_applications ON doc_repo.application_id=credit_applications.application_id '

            + 'WHERE (doc_repo.application_id = ? OR doc_repo.pp_id IN(SELECT pp_id FROM payment_plan WHERE application_id=?)) '
            + 'AND doc_repo.status = ?',
            [
                id, id, 1
            ]
            , function (err, allDocuments) {
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong while getting all documents.";
                        });
                        return callback(obj);
                    }
                    obj.status = 1;

                    obj.allDocuments = allDocuments;
                    return callback(obj);
                });
            });
    });
}
var getAppDocumentPlan = function (con, id, callback) {

    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'pp_id, '
            + 'plan_number '
            + 'FROM payment_plan '

            + 'WHERE application_id = ? ',
            [
                id
            ]
            , function (err, planDocuments) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong while getting all documents.";
                    });
                    return callback(obj);
                } else {

                    con.query('SELECT '
                        + 'payment_plan.pp_id, '
                        + 'payment_plan.plan_number, '
                        + 'doc_repo.file_path, '
                        + 'doc_repo.name, '
                        + 'doc_repo.item_id '
                        + 'FROM doc_repo '
                        + 'LEFT JOIN payment_plan ON payment_plan.pp_id=doc_repo.pp_id '
                        + 'LEFT JOIN credit_applications ON doc_repo.application_id=credit_applications.application_id '

                        + 'WHERE doc_repo.application_id = ? AND doc_repo.status = ?',
                        [
                            id, 1
                        ]
                        , function (err, allDocuments) {
                            con.commit(function (err) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong while getting all documents.";
                                    });
                                    return callback(obj);
                                }
                                obj.status = 1;
                                obj.planDocuments = planDocuments;
                                obj.allDocuments = allDocuments;
                                return callback(obj);
                            });
                        });
                }
            })

    });
}
var getplanDocumentPlan = function (con, id, callback) {

    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT '
            + 'payment_plan.pp_id, '
            + 'payment_plan.verify_doc, '
            + 'credit_applications.application_id, '
            + 'payment_plan.plan_number, '
            + 'doc_repo.file_path, '
            + 'doc_repo.name, '
            + 'doc_repo.item_id '
            + 'FROM doc_repo '
            + 'LEFT JOIN payment_plan ON payment_plan.pp_id=doc_repo.pp_id '
            + 'LEFT JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '

            + 'WHERE doc_repo.pp_id = ? AND doc_repo.status = ?',
            [
                id, 1
            ]
            , function (err, allDocuments) {
                con.commit(function (err) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong while getting all documents.";
                        });
                        return callback(obj);
                    }
                    obj.status = 1;
                    obj.planDocuments = '';
                    obj.allDocuments = allDocuments;
                    return callback(obj);
                });
            });


    });
}

var providerCareCloud = function (con, id, callback) {
    con.query('SELECT AES_DECRYPT(ap_key, "ramesh_cogniz") as ap_key,AES_DECRYPT(secret_key, "ramesh_cogniz") as secret_key '
        + 'FROM mfs_details '
        + 'WHERE provider_id = ? ',
        [
            id
        ]
        , function (error, result) {
            if (error) {
                var obj = {
                    "status": 0,
                }
                return callback(obj);
            } else {
                if (result.length > 0) {
                    result[0].ap_key = (result.length > 0) ? result[0].ap_key.toString() : '';
                    result[0].secret_key = (result.length > 0) ? result[0].secret_key.toString() : '';
                    result = result[0];
                } else {
                    result = ''
                }
                var obj = {
                    "status": 1,
                    "careCould": result
                }
                return callback(obj);
            }
        })
}
var documentApplcation = function (con, data, file, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    return new Promise((resolve, reject) => {
        con.query('INSERT INTO `doc_repo` SET '
            + 'application_id=?, '
            + 'name=?, '
            + 'file_path=?, '
            + 'item_id=?, '
            + 'agrement_flag=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                data.application_id,
                file.filename,
                file.path,
                file.item_id,
                1,
                current_date,
                current_date,
                data.current_user_id,
                data.current_user_id
            ]
            , function (err, planDetails) {
                if (err) {
                    obj.status = 0;
                    obj.message = "Sorry, we are not able to upload documents right now.";
                    callback(obj);
                    resolve();
                } else {
                    obj.status = 1;
                    obj.message = "Document has been uploaded successfully";
                    callback(obj);
                    resolve();
                }
            });
    })
}

var createPrintPlan = function (con, creditDetails, planDetails, current_id, customerData, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    //var insert_rate_id = (creditDetails[0].member_flag == 1) ? null : planDetails[0].plan_id;
    var term_id = planDetails[0].term_month;
    var termMonth = planDetails[0].term_month;
    var orgInterest = planDetails[0].org_interest_rate;
    var firstMonth = planDetails[0].firstMonth;
    var lastMonth = planDetails[0].lastMonth;

    var plan_status = 2;
    //var discounted_interest_rate = planDetails[0].interest_rate;
    var discounted_interest_rate = planDetails[0].interest_rate;//(parseFloat(planDetails[0].interest_rate) / parseFloat(planDetails[0].term_month) * 12).toFixed(2);
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO payment_plan SET '
            + 'term=?, '
            + 'loan_type=?, '
            + 'interest_rate=?, '
            + 'discounted_interest_rate=?, '
            + 'monthly_amount=?, '
            + 'last_month_amount=?, '
            + 'plan_number=((SELECT COALESCE(MAX(plan_number),11111)+1 AS dt FROM payment_plan as p)), '
            + 'application_id=?, '
            + 'doctor_id=?, '
            + 'provider_id=?, '
            + 'amount=?, '
            + 'remaining_amount=?, '
            + 'loan_amount=?, '
            + 'id_type=?, '
            + 'id_number=?, '
            + 'id_expiry_date=?, '
            + 'co_id_type=?, '
            + 'co_id_number=?, '
            + 'co_id_expiry_date=?, '
            + 'co_name_of_borrower=?, '
            + 'co_agreement_date=?, '
            + 'agreement_flag=?, '
            + 'name_of_borrower=?, '
            + 'agreement_date=?, '
            + 'plan_status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                termMonth,
                customerData.details.loan_type,
                orgInterest,
                discounted_interest_rate,
                firstMonth,
                lastMonth,
                creditDetails[0].application_id,
                customerData.details.doctor,
                customerData.provider_id,
                planDetails[0].totalAmount,
                customerData.details.loan_amount,
                customerData.details.loan_amount,
                capitalize(customerData.details.id_name),
                customerData.details.id_number,
                customerData.details.expiry_date,
                capitalize(customerData.details.co_id_name),
                customerData.details.co_id_number,
                (customerData.details.co_expiry_date != null) ? customerData.details.co_expiry_date : '',
                capitalize(customerData.details.co_name_of_borrower),
                customerData.details.co_agreement_date,
                customerData.details.agree_with,
                capitalize(customerData.details.name_of_borrower),
                customerData.details.agreement_date,
                plan_status,
                current_date,
                current_date,
                current_id,
                current_id
            ]
            , function (err, resultplan) {  
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "1 Payment plan cant created please contact with administration.";
                        return callback(obj);
                    });
                } else {
                    var values = [];
                    planDetails[0].monthlyPlan.forEach(function (element, idx) {
                        //let nextDate = new Date(element.next_month);
                        var nextDate = element.next_month.split('/');
                        var nextDate = nextDate[2] + '-' + nextDate[0] + '-' + nextDate[1];
                        values.push([resultplan.insertId, element.perMonth, nextDate, current_date, current_date, current_id, current_id])
                    });
                    con.query('INSERT INTO pp_installments (pp_id,installment_amt,due_date,date_created,date_modified,created_by,modified_by) VALUES ?',
                        [
                            values
                        ]
                        , function (error, resultuser) {
                            if (error) {

                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "2 Payment plan cant created please contact with administration.2";
                                    return callback(obj);
                                });
                            } else {
                                con.query("UPDATE `credit_applications` SET remaining_amount=?, date_modified = ? , modified_by= ? WHERE application_id = ?",
                                    [(creditDetails[0].remaining_amount - customerData.details.loan_amount), current_date, current_id, customerData.application_id],
                                    function (err) {
                                        if (err) {

                                            con.rollback(function () {

                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        } else {
                                            providerDiscount(con, customerData.provider_id, customerData.details.loan_amount, term_id, function (discount) {

                                                if (discount.status == 1) {
                                                    con.query('INSERT INTO patient_procedure SET '
                                                        + 'provider_location_id=?, '
                                                        + 'provider_id=?, '
                                                        + 'doctor_id=?, '
                                                        + 'application_id=?, '
                                                        + 'pp_id=?, '
                                                        + 'procedure_amt=?, '
                                                        + 'procedure_date=?, '
                                                        + 'procedure_status=?, '
                                                        + 'hps_discount_amt=?, '
                                                        + 'date_created=?, '
                                                        + 'date_modified=?, '
                                                        + 'created_by=?, '
                                                        + 'modified_by=?',
                                                        [
                                                            customerData.details.provider_location,
                                                            customerData.provider_id,
                                                            customerData.details.doctor,
                                                            customerData.application_id,
                                                            resultplan.insertId,
                                                            customerData.details.procedure_amount,
                                                            customerData.details.procedure_date,
                                                            1,
                                                            discount.amount,
                                                            current_date,
                                                            current_date,
                                                            current_id,
                                                            current_id
                                                        ]
                                                        , function (err, resultpro) {

                                                            if (err) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Something wrong please try again.";
                                                                    return callback(obj);
                                                                });
                                                            } else {
                                                                var updatepatitentID = creditDetails[0].patient_id;
                                                                existProviderPatient(con, customerData.provider_id, creditDetails[0].patient_id, function (exitsPatientProvider) {
                                                                    if (exitsPatientProvider.exists) {
                                                                        updatePatientSingleBank(con, customerData, updatepatitentID, current_id, function (bankDetails) {

                                                                            if (bankDetails.status == 0) {
                                                                                con.rollback(function () {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Payment plan created but bank details not updated yet..";
                                                                                    return callback(obj);
                                                                                });
                                                                            } else {
                                                                                con.commit(function (err) {
                                                                                    if (err) {
                                                                                        con.rollback(function () {
                                                                                            obj.status = 0;
                                                                                            obj.message = "Payment plan created but bank details not updated yet..";
                                                                                            return callback(obj);
                                                                                        });
                                                                                    } else {
                                                                                        obj.status = 1;
                                                                                        obj.message = "Payment plan created successfully.";
                                                                                        obj.paymentPlan = resultplan.insertId;
                                                                                        return callback(obj);
                                                                                    }
                                                                                });
                                                                            }
                                                                        })
                                                                        /*var sql = 'UPDATE patient SET '
                                                                            + 'bank_name = ? , '
                                                                            + 'bank_address = ? , '
                                                                            + 'rounting_no = ? , '
                                                                            + 'account_number = ? , '
                                                                            + 'account_name = ? , '
                                                                            + 'account_type = ? , '
                                                                            + 'date_modified = ? , modified_by= ? WHERE patient_id = ?';
                                                                        con.query(sql, [
                                                                            capitalize(customerData.details.bank_name),
                                                                            capitalize(customerData.details.bank_address),
                                                                            customerData.details.rounting_no,
                                                                            customerData.details.bank_ac,
                                                                            capitalize(customerData.details.account_name),
                                                                            customerData.details.account_type,
                                                                            current_date,
                                                                            current_id,
                                                                            updatepatitentID], function (errorupdate) {
                                                                                if (errorupdate) {
                                                                                    obj.status = 0;
                                                                                    obj.message = "Payment plan created but bank details not updated yet..";
                                                                                    return callback(obj);
                                                                                } else {
                                                                                    con.commit(function (err) {
                                                                                        if (err) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            obj.status = 1;
                                                                                            obj.message = "Payment plan created successfully.";
                                                                                            obj.paymentPlan = resultplan.insertId;
                                                                                            return callback(obj);
                                                                                        }
                                                                                    });

                                                                                }
                                                                            });*/

                                                                    } else {
                                                                        con.query('INSERT INTO patient_provider SET '
                                                                            + 'patient_id=?, '
                                                                            + 'provider_id=?, '
                                                                            + 'date_created=?, '
                                                                            + 'date_modified=?, '
                                                                            + 'created_by=?, '
                                                                            + 'modified_by=?',
                                                                            [
                                                                                updatepatitentID,
                                                                                customerData.provider_id,
                                                                                current_date,
                                                                                current_date,
                                                                                current_id,
                                                                                current_id
                                                                            ]
                                                                            , function (err, patientProvider) {
                                                                                if (err) {
                                                                                    con.rollback(function () {
                                                                                        obj.status = 0;
                                                                                        obj.message = "Something wrong please try again.";
                                                                                        return callback(obj);
                                                                                    });
                                                                                } else {
                                                                                    updatePatientSingleBank(con, customerData, updatepatitentID, current_id, function (bankDetails) {

                                                                                        if (bankDetails.status == 0) {
                                                                                            con.rollback(function () {
                                                                                                obj.status = 0;
                                                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            });
                                                                                        } else {
                                                                                            con.commit(function (err) {
                                                                                                if (err) {
                                                                                                    con.rollback(function () {
                                                                                                        obj.status = 0;
                                                                                                        obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                        return callback(obj);
                                                                                                    });
                                                                                                } else {
                                                                                                    obj.status = 1;
                                                                                                    obj.message = "Payment plan created successfully.";
                                                                                                    obj.paymentPlan = resultplan.insertId;
                                                                                                    return callback(obj);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    })
                                                                                    /*var sql = 'UPDATE patient SET '
                                                                                        + 'bank_name = ? , '
                                                                                        + 'bank_address = ? , '
                                                                                        + 'rounting_no = ? , '
                                                                                        + 'account_number = ? , '
                                                                                        + 'account_name = ? , '
                                                                                        + 'account_type = ? , '
                                                                                        + 'date_modified = ? , modified_by= ? WHERE patient_id = ?';
                                                                                    con.query(sql, [
                                                                                        capitalize(customerData.details.bank_name),
                                                                                        capitalize(customerData.details.bank_address),
                                                                                        customerData.details.rounting_no,
                                                                                        customerData.details.bank_ac,
                                                                                        capitalize(customerData.details.account_name),
                                                                                        customerData.details.account_type,
                                                                                        current_date,
                                                                                        current_id,
                                                                                        updatepatitentID], function (errorupdate) {
                                                                                            if (errorupdate) {
                                                                                                obj.status = 0;
                                                                                                obj.message = "3 Payment plan created but bank details not updated yet..";
                                                                                                return callback(obj);
                                                                                            } else {
                                                                                                con.commit(function (err) {
                                                                                                    if (err) {
                                                                                                        con.rollback(function () {
                                                                                                            obj.status = 0;
                                                                                                            obj.message = "Payment plan created but bank details not updated yet..";
                                                                                                            return callback(obj);
                                                                                                        });
                                                                                                    } else {
                                                                                                        obj.status = 1;
                                                                                                        obj.message = "Payment plan created successfully.";
                                                                                                        obj.paymentPlan = resultplan.insertId;
                                                                                                        return callback(obj);
                                                                                                    }
                                                                                                });

                                                                                            }
                                                                                        });*/
                                                                                }
                                                                            })
                                                                    }
                                                                })

                                                            }
                                                        })
                                                }
                                            })
                                        }
                                    })


                            }
                        });
                }
            });
    });
}

var uploadPlanAgreementDocument = function (con, data, file, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('UPDATE `doc_repo` SET '
            + 'status=?, '
            + 'delete_flag=?, '
            + 'date_modified=?, '
            + 'modified_by=? '

            + 'WHERE pp_id = ? AND agrement_flag = ?',
            [
                0,
                1,
                current_date,
                data.current_user_id,
                data.pp_id,
                1
            ]
            , function (err, docStatus) {

                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "1Sorry, we are not able to upload documents right now.";
                        return callback(obj);
                    });
                } else {
                    con.query('INSERT INTO `doc_repo` SET '
                        + 'application_id=?, '
                        + 'pp_id=?, '
                        + 'name=?, '
                        + 'file_path=?, '
                        + 'item_id=?, '
                        + 'agrement_flag=?, '
                        + 'date_created=?, '
                        + 'date_modified=?, '
                        + 'created_by=?, '
                        + 'modified_by=?',
                        [
                            data.application_id,
                            (data.pp_id) ? data.pp_id : null,
                            file.filename,
                            file.path,
                            file.item_id,
                            1,
                            current_date,
                            current_date,
                            data.current_user_id,
                            data.current_user_id
                        ]
                        , function (err, planDetails) {

                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Sorry, we are not able to upload documents right now.";
                                    return callback(obj);
                                });
                            } else {
                                con.commit(function (err) {
                                    if (err) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Something wrong please try again.";
                                        });
                                        return callback(obj);
                                    }
                                    obj.status = 1;
                                    obj.message = "Agreement has been uploaded successfully";
                                    return callback(obj);
                                });
                            }
                        });
                }
            });
    });
}

var getPlanAgreement = function (con, id, callback) {
    var obj = {};
    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('SELECT payment_plan.plan_number, '
            + 'payment_plan.term, '
            + 'payment_plan.discounted_interest_rate, '
            + 'payment_plan.application_id, '
            + 'payment_plan.loan_amount, '
            + 'payment_plan.pp_id, '
            + 'payment_plan.plan_status, '

            + 'credit_applications.application_no, '
            + 'credit_applications.approve_amount, '
            + 'credit_applications.score, '

            + 'patient.patient_ac '

            + 'FROM payment_plan '

            + 'INNER JOIN credit_applications '
            + 'ON payment_plan.application_id = credit_applications.application_id '

            + 'INNER JOIN patient '
            + 'ON credit_applications.patient_id = patient.patient_id '

            + 'WHERE payment_plan.pp_id = ?'
            , [id]
            , function (err, appDetails) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Plan not found.";
                        return callback(obj);
                    });
                } else {
                    con.query('SELECT name,file_path,item_id FROM doc_repo WHERE '
                        + 'pp_id=? AND status=?',
                        [
                            id,
                            1
                        ]
                        , function (err, docDetails) {
                            con.commit(function (err) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Sorry, we are not able to get agreement right now.";
                                    });
                                    return callback(obj);
                                }
                                obj.status = 1;
                                obj.appDocDetails = (appDetails.length > 0) ? appDetails[0] : '';
                                obj.docDetails = docDetails;
                                return callback(obj);
                            });
                        });
                }
            });
    });
}

var planManualAction = function (con, data, callback) {

    //return false;
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let message = '';

    if (data.type == 1) {
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            con.query('UPDATE payment_plan SET '
                + 'note=?, '
                + 'plan_status=?, '
                + 'date_modified=?, '
                + 'modified_by=? '
                + 'WHERE '
                + 'pp_id=?',
                [
                    capitalize(data.comment),
                    4,
                    current_date,
                    data.current_user_id,
                    data.id,
                ]
                , function (err, planStatus) {

                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            return callback(obj);
                        });
                    } else {
                        con.commit(function (err) {
                            if (err) {
                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    return callback(obj);
                                });
                            }
                            obj.status = 1;
                            obj.message = "Plan Approved.";
                            obj.type = data.type;
                            return callback(obj);
                        });

                    }
                });
        });
    } else if (data.type == 2) {
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            con.query('UPDATE payment_plan SET '
                + 'note=?, '
                + 'plan_status=?, '
                + 'date_modified=?, '
                + 'modified_by=? '
                + 'WHERE '
                + 'pp_id=?',
                [
                    capitalize(data.comment),
                    2,
                    current_date,
                    data.current_user_id,
                    data.id,
                ]
                , function (err, appDetails) {

                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            return callback(obj);
                        });
                    } else {
                        con.query('UPDATE doc_repo SET '
                            + 'delete_flag=?, '
                            + 'status=?, '
                            + 'date_modified=?, '
                            + 'modified_by=? '
                            + 'WHERE '
                            + 'pp_id=?',
                            [
                                1,
                                0,
                                current_date,
                                data.current_user_id,
                                data.id
                            ]
                            , function (err, docStatus) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        return callback(obj);
                                    });
                                } else {
                                    con.commit(function (err) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.status = 0;
                                                obj.message = "Something wrong please try again.";
                                                return callback(obj);
                                            });
                                        }
                                        obj.status = 1;
                                        obj.message = "Need more documents.";
                                        obj.type = data.type;
                                        return callback(obj);
                                    });
                                }
                            })

                    }
                });
        });
    } else {
        var dataSql = require('../payment/payment.sql.js');
        data.pp_id = data.id

        var data2 = {
            pp_id: data.id,
            note: data.comment
        }
        var data3 = {
            data: data2,
            current_user_id: data.current_user_id
        }

        dataSql.closePlan(con, data3, function (result) {
            if (result.status == 1) {
                result.type = data.type;
                return callback(result);
            } else {
                return callback(result);
            }
        });
    }

}

var createPlanWithSettement = function (con, datacPlan, callback) {

    var obj = {};
    //obj.status=0;
    //callback(obj)
    //return false;
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    //var insert_rate_id = (creditDetails[0].member_flag == 1) ? null : planDetails[0].plan_id;
    var term_id = datacPlan.data.planEst.term_month;
    var termMonth = datacPlan.data.planEst.term_month;
    var orgInterest = datacPlan.data.planEst.org_interest_rate;
    var firstMonth = datacPlan.data.planEst.firstMonth;
    var lastMonth = datacPlan.data.planEst.lastMonth;

    //var discounted_interest_rate = planDetails[0].interest_rate;
    var discounted_interest_rate = datacPlan.data.planEst.interest_rate;//(parseFloat(planDetails[0].interest_rate) / parseFloat(planDetails[0].term_month) * 12).toFixed(2);

    con.beginTransaction(function (err) {
        if (err) {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
            return callback(obj);
        }
        con.query('INSERT INTO payment_plan SET '
            + 'plan_ref_id=?,'
            + 'term=?, '
            + 'loan_type=?, '
            + 'interest_rate=?, '
            + 'discounted_interest_rate=?, '
            + 'monthly_amount=?, '
            + 'last_month_amount=?, '
            + 'plan_number=((SELECT COALESCE(MAX(plan_number),11111)+1 AS dt FROM payment_plan as p)), '
            + 'application_id=?, '
            + 'provider_id=?, '
            + 'amount=?, '
            + 'remaining_amount=?, '
            + 'loan_amount=?, '
            + 'id_type=?, '
            + 'id_number=?, '
            + 'id_expiry_date=?, '
            + 'agreement_flag=?, '
            + 'name_of_borrower=?, '
            + 'agreement_date=?, '
            + 'plan_type=?, '
            + 'plan_status=?, '
            + 'date_created=?, '
            + 'date_modified=?, '
            + 'created_by=?, '
            + 'modified_by=?',
            [
                datacPlan.data.pp_id,
                termMonth,
                datacPlan.data.loan_type,
                orgInterest,
                discounted_interest_rate,
                firstMonth,
                lastMonth,
                datacPlan.data.customerDetails.application_id,
                datacPlan.data.providerDetails.provider_id,
                datacPlan.data.planEst.totalAmount,
                datacPlan.data.loan_amount,
                datacPlan.data.loan_amount,
                capitalize(datacPlan.data.id_name),
                datacPlan.data.id_number,
                datacPlan.data.expiry_date,
                datacPlan.data.agree_with,
                capitalize(datacPlan.data.name_of_borrower),
                datacPlan.data.agreement_date,
                2,
                1,
                current_date,
                current_date,
                datacPlan.current_user_id,
                datacPlan.current_user_id
            ]
            , function (err, resultplan) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Payment plan cant created please contact with administration.";
                        return callback(obj);
                    });
                } else {
                    var values = [];
                    datacPlan.data.planEst.monthlyPlan.forEach(function (element, idx) {
                        //let nextDate = new Date(element.next_month);
                        var nextDate = element.next_month.split('/');
                        var nextDate = nextDate[2] + '-' + nextDate[0] + '-' + nextDate[1];
                        values.push([resultplan.insertId, element.perMonth, nextDate, current_date, current_date, datacPlan.current_user_id, datacPlan.current_user_id])
                    });
                    con.query('INSERT INTO pp_installments (pp_id,installment_amt,due_date,date_created,date_modified,created_by,modified_by) VALUES ?',
                        [
                            values
                        ]
                        , function (error, resultuser) {
                            if (error) {

                                con.rollback(function () {
                                    obj.status = 0;
                                    obj.message = "Payment plan cant created please contact with administration.2";
                                    return callback(obj);
                                });
                            } else {
                                existProviderPatient(con, datacPlan.data.providerDetails.provider_id, datacPlan.data.customerDetails.patient_id, function (exitsPatientProvider) {
                                    if (!exitsPatientProvider.exists) {
                                        con.query('INSERT INTO patient_provider SET '
                                            + 'patient_id=?, '
                                            + 'provider_id=?, '
                                            + 'date_created=?, '
                                            + 'date_modified=?, '
                                            + 'created_by=?, '
                                            + 'modified_by=?',
                                            [
                                                datacPlan.data.customerDetails.patient_id,
                                                datacPlan.data.providerDetails.provider_id,
                                                current_date,
                                                current_date,
                                                datacPlan.current_user_id,
                                                datacPlan.current_user_id
                                            ]
                                            , function (err, patientProvider) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong please try again.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    con.commit(function (err) {
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Payment plan created but bank details not updated yet..";
                                                                return callback(obj);
                                                            });
                                                        } else {
                                                            obj.status = 1;
                                                            obj.message = "Payment plan created successfully.";
                                                            obj.paymentPlan = resultplan.insertId;
                                                            return callback(obj);
                                                        }
                                                    });
                                                }
                                            })
                                    } else {
                                        con.commit(function (err) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Payment plan created but bank details not updated yet..";
                                                    return callback(obj);
                                                });
                                            } else {
                                                obj.status = 1;
                                                obj.message = "Payment plan created successfully.";
                                                obj.paymentPlan = resultplan.insertId;
                                                return callback(obj);
                                            }
                                        });
                                    }
                                })
                            }
                        });
                }
            });
    });
}

var creditapplicationList = function (con, data, callback) {
    var obj = {};
    let cond = 1;

    if (data.filter_type == 1) {

        cond = cond + ' AND WEEKOFYEAR(credit_applications.date_created)=WEEKOFYEAR(NOW())'
        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else if (data.filter_type == 2) {

        cond = cond + ' AND YEAR(credit_applications.date_created) = ' + data.year + ' AND MONTH(credit_applications.date_created)=' + data.month;
        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else if (data.filter_type == 3) {

        cond = cond + ' AND YEAR(credit_applications.date_created) = ' + data.year + ' AND QUARTER(credit_applications.date_created)=' + data.quarter;
        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else if (data.filter_type == 4) {

        cond = cond + ' AND YEAR(credit_applications.date_created) = ' + data.year;
        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else if (data.filter_type == 5) {

        cond = cond + ' AND YEAR(credit_applications.date_created) = YEAR(CURDATE()) AND DATE_FORMAT(credit_applications.date_created, "%Y-%m-%d")<=DATE_FORMAT(NOW(), "%Y-%m-%d")';
        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else if (data.filter_type == 6) {

        if (data.start_date != '') {
            cond = cond + ' AND DATE_FORMAT(credit_applications.date_created, "%Y-%m-%d") >="' + data.start_date + '"';
        }

        if (data.end_date != '') {
            cond = cond + ' AND DATE_FORMAT(credit_applications.date_created, "%Y-%m-%d") <="' + data.end_date + '"';
        }

        if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    } else {

        /*if (data.status_id == 3) {
            cond = cond + ' AND credit_applications.status IN(4,5)';
        } else if (data.status_id != 0 && data.status_id != 1 && data.status_id != 2) {
            cond = cond + '';
        } else {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }*/
        if (data.status_id == 3) {
            cond = cond + ' AND credit_applications.status IN(4,5)';
        } else if (data.status_id != '' && data.status_id !== 'all') {
            cond = cond + ' AND credit_applications.status ="' + data.status_id + '" ';
        }

    }




    con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,credit_applications.document_flag,credit_applications.plan_flag,credit_applications.score,credit_applications.approve_amount,DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") AS expiry_date,credit_applications.remaining_amount,credit_applications.override_amount,credit_applications.status,DATE_FORMAT(credit_applications.date_created, "%m/%d/%Y") AS date_created,patient.f_name,patient.m_name,patient.l_name,patient.gender,patient.email,patient.peimary_phone,patient.patient_ac, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, credit_applications.application_id, master_data_values.value as status_name, '
        + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists '
        
        + 'FROM credit_applications '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        /* 'INNER JOIN patient_provider ON patient.patient_id=patient_provider.patient_id '*/
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_applications.status AND master_data_values.md_id=? '
        /*+ 'WHERE credit_applications.delete_flag=? ORDER BY application_id DESC',*/
        + 'WHERE credit_applications.co_signer=? AND credit_applications.delete_flag=? AND ' + cond + ' ORDER BY application_id DESC',
        [
            'Application Status',
            0,
            0,
        ]
        , function (error, rows, fields) {

            if (error) {
                obj.status = 0;
                obj.message = "Something wrong. Please try again";
                return callback(obj);
            } else {
                con.query('SELECT value FROM master_data_values WHERE md_id=?'
                    ,
                    ['Year Start']
                    , function (error, yearStart, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.message = "Something wrong. Please try again";
                            return callback(obj);
                        } else {
                            con.query('SELECT '
                                + 'value, status_id '

                                + 'FROM master_data_values '
                                + 'WHERE md_id = "Application Status" AND status = 1'
                                , function (errA, app_status) {
                                    if (errA) {
                                        obj.status = 0;
                                        obj.message = "Something wrong. Please try again";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.result = rows;
                                        obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : '';
                                        obj.app_status = app_status;
                                        return callback(obj);
                                    }
                                })

                        }
                    })

            }
        })
}

var cancelRefundDetails = function (con, data, callback) {
    var obj = {};

    con.query('SELECT '
        + 'customer_refund.refund_id, '
        + 'customer_refund.refund_amt, '
        + 'customer_refund.payment_method, '
        + 'customer_refund.check_no, '
        + 'DATE_FORMAT(customer_refund.check_date, "%Y/%m/%d") AS check_date, '
        + 'customer_refund.ach_bank_name, '
        + 'customer_refund.ach_routing_no, '
        + 'customer_refund.ach_account_no, '
        + 'customer_refund.comments AS refund_comment, '
        + 'customer_refund.status, '

        + 'master_data_values.value AS refundmethod '

        + 'FROM customer_refund '

        + 'INNER JOIN master_data_values '
        + 'ON master_data_values.status_id = customer_refund.payment_method AND master_data_values.md_id = ? '

        + 'WHERE customer_refund.pp_id = ?',
        [
            'Payment Method',
            data.ppid
        ]
        , function (error, customer_refund, fields) {

            if (error) {
                obj.status = 0;
                obj.message = "Something wrong. Please try again";
                return callback(obj);
            } else {
                con.query('SELECT '
                    + 'refund_id, '
                    + 'refund_due, '
                    + 'payment_method, '
                    + 'check_no, '
                    + 'DATE_FORMAT(check_date, "%Y/%m/%d") AS check_date, '
                    + 'ach_bank_name, '
                    + 'ach_routing_no, '
                    + 'ach_account_no, '
                    + 'comments AS refund_comment, '
                    + 'iou_flag, '
                    + 'status '

                    + 'FROM provider_refund '

                    + 'WHERE pp_id = ?',
                    [
                        data.ppid
                    ]
                    , function (error, provider_refund, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.message = "Something wrong. Please try again";
                            return callback(obj);
                        } else {
                            con.query('SELECT '
                                + 'payment_plan.plan_number, '
                                + 'payment_plan.loan_amount, '
                                + 'payment_plan.discounted_interest_rate, '
                                + 'payment_plan.term, '
                                + 'payment_plan.adjusted_amount, '
                                + 'payment_plan.note, '
                                + 'payment_plan.plan_ref_id, '
                                + 'payment_plan.plan_status, '

                                + 'payment_plan2.plan_number AS plan_ref_no '

                                + 'FROM payment_plan '

                                + 'INNER JOIN payment_plan AS payment_plan2 '
                                + 'ON payment_plan2.pp_id = payment_plan.plan_ref_id '

                                + 'WHERE payment_plan.pp_id = ?',
                                [
                                    data.ppid
                                ]
                                , function (error, sttlement_detail, fields) {
                                    if (error) {
                                        obj.status = 0;
                                        obj.message = "Something wrong. Please try again";
                                        return callback(obj);
                                    } else {
                                        con.query('SELECT '
                                            + 'payment_plan.plan_number, '
                                            + 'payment_plan.provider_id, '

                                            + 'credit_applications.application_id, '

                                            + 'patient.f_name, '
                                            + 'patient.m_name, '
                                            + 'patient.l_name, '
                                            + 'patient.patient_ac, '
                                            + 'patient.peimary_phone, '

                                            + 'patient_address.address1, '
                                            + 'patient_address.City, '
                                            + 'patient_address.zip_code, '

                                            + 'states.name AS state_name, '

                                            + 'countries.name AS country_name '

                                            + 'FROM payment_plan '

                                            + 'INNER JOIN credit_applications '
                                            + 'ON credit_applications.application_id = payment_plan.application_id '

                                            + 'INNER JOIN patient '
                                            + 'ON patient.patient_id = credit_applications.patient_id '

                                            + 'INNER JOIN patient_address '
                                            + 'ON patient_address.patient_id = patient.patient_id AND patient_address.status = 1 '

                                            + 'INNER JOIN states '
                                            + 'ON states.state_id = patient_address.state_id '

                                            + 'INNER JOIN countries '
                                            + 'ON countries.id = states.country_id '

                                            + 'WHERE payment_plan.pp_id = ?',
                                            [
                                                data.ppid
                                            ]
                                            , function (error, customer_detail, fields) {
                                                if (error) {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong. Please try again";
                                                    return callback(obj);
                                                } else {
                                                    con.query('SELECT '
                                                        + 'payment_plan.plan_number, '
                                                        + 'payment_plan.provider_id, '

                                                        + 'provider.name, '
                                                        + 'provider.provider_ac, '
                                                        + 'provider.primary_phone, '

                                                        + 'provider_location.address1, '
                                                        + 'provider_location.state, '
                                                        + 'provider_location.city, '
                                                        + 'provider_location.zip_code, '

                                                        + 'states.name AS state_name, '

                                                        + 'countries.name AS country_name '

                                                        + 'FROM payment_plan '

                                                        + 'INNER JOIN provider '
                                                        + 'ON provider.provider_id = payment_plan.provider_id '

                                                        + 'INNER JOIN provider_location '
                                                        + 'ON provider_location.provider_id = provider.provider_id '

                                                        + 'INNER JOIN states '
                                                        + 'ON states.state_id = provider_location.state '

                                                        + 'INNER JOIN countries '
                                                        + 'ON countries.id = states.country_id '

                                                        + 'WHERE payment_plan.pp_id = ?',
                                                        [
                                                            data.ppid
                                                        ]
                                                        , function (error, provider_detail, fields) {
                                                            if (error) {
                                                                obj.status = 0;
                                                                obj.message = "Something wrong. Please try again";
                                                                return callback(obj);
                                                            } else {
                                                                obj.status = 1;
                                                                obj.customer_refund = customer_refund;
                                                                obj.provider_refund = provider_refund;
                                                                obj.sttlement_detail = sttlement_detail;
                                                                obj.customer_detail = customer_detail;
                                                                obj.provider_detail = provider_detail;
                                                                return callback(obj);
                                                            }

                                                        })

                                                }

                                            })


                                    }
                                })
                        }
                    })

            }
        })
}

var creditapplicationPlanReview = function (con, data, callback) {
    var obj = {};
    

    
    con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,credit_applications.document_flag,credit_applications.plan_flag,credit_applications.score,credit_applications.approve_amount,DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") AS expiry_date,credit_applications.remaining_amount,credit_applications.override_amount,credit_applications.status,DATE_FORMAT(credit_applications.date_created, "%m/%d/%Y") AS date_created,patient.f_name,patient.m_name,patient.l_name,patient.gender,patient.email,patient.peimary_phone,patient.patient_ac, '
        + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, credit_applications.application_id, master_data_values.value as status_name, '
        + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists, '
        + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id AND verify_doc = 0 AND plan_status IN(1,4)) as verify_doc '
        + 'FROM credit_applications '
        + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
        /* 'INNER JOIN patient_provider ON patient.patient_id=patient_provider.patient_id '*/
        + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
        /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
        + 'INNER JOIN states ON states.state_id=patient_address.state_id '
        + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_applications.status AND master_data_values.md_id=? '
        /*+ 'WHERE credit_applications.delete_flag=? ORDER BY application_id DESC',*/
        + 'WHERE credit_applications.co_signer=? AND credit_applications.delete_flag=? AND credit_applications.application_id IN(SELECT application_id FROM payment_plan WHERE verify_doc = 0 AND plan_status IN(1,4)) ORDER BY application_id DESC',
        [
            'Application Status',
            0,
            0,
        ]
        , function (error, rows, fields) {
            
            if (error) {
                obj.status = 0;
                obj.message = "Something wrong. Please try again";
                return callback(obj);
            } else {
                con.query('SELECT value FROM master_data_values WHERE md_id=?'
                    ,
                    ['Year Start']
                    , function (error, yearStart, fields) {
                        if (error) {
                            obj.status = 0;
                            obj.message = "Something wrong. Please try again";
                            return callback(obj);
                        } else {
                            con.query('SELECT '
                                + 'value, status_id '

                                + 'FROM master_data_values '
                                + 'WHERE md_id = "Application Status" AND status = 1'
                                , function (errA, app_status) {
                                    if (errA) {
                                        obj.status = 0;
                                        obj.message = "Something wrong. Please try again";
                                        return callback(obj);
                                    } else {
                                        obj.status = 1;
                                        obj.result = rows;
                                        obj.yearStart = (yearStart.length > 0) ? yearStart[0].value : '';
                                        obj.app_status = app_status;
                                        return callback(obj);
                                    }
                                })

                        }
                    })

            }
        })
}

exports.creditSql = creditSql;
exports.applicationFactor = applicationFactor;
exports.applicationScore = applicationScore;
exports.getPlans = getPlans;
exports.createPlan = createPlan;
exports.getSinglePlans = getSinglePlans;
exports.viewApplcation = viewApplcation;
exports.searchCustomer = searchCustomer;
exports.existCustomer = existCustomer;
exports.reviewApplcation = reviewApplcation;
exports.manualAction = manualAction;
exports.rejectApplcation = rejectApplcation;
exports.uploadDocumentApplcation = uploadDocumentApplcation;
exports.getDocumentApplcation = getDocumentApplcation;
exports.uploadAgrementApplcation = uploadAgrementApplcation;
exports.downloadAgrementApplcation = downloadAgrementApplcation;
exports.searchApplication = searchApplication;
exports.applicationDetails = applicationDetails;
exports.updateRemaining = updateRemaining;
exports.getEstPlans = getEstPlans;
exports.getApplicationDetails = getApplicationDetails;
exports.getAllDocumentApplcation = getAllDocumentApplcation;
exports.uploadRejectionAgrementApplcation = uploadRejectionAgrementApplcation;
exports.providerCareCloud = providerCareCloud;
exports.DirectCreditSql = DirectCreditSql;
exports.getExperianLoginDetail = getExperianLoginDetail;
exports.documentApplcation = documentApplcation;
exports.fetchDoctorName = fetchDoctorName;
exports.providerDiscountAll = providerDiscountAll;
exports.providerDiscount = providerDiscount;
exports.budgetPlan = budgetPlan;
exports.budgetEstPlans = budgetEstPlans;
exports.createPrintPlan = createPrintPlan;
exports.uploadPlanAgreementDocument = uploadPlanAgreementDocument;
exports.getPlanAgreement = getPlanAgreement;
exports.planManualAction = planManualAction;
exports.createPlanWithSettement = createPlanWithSettement;
exports.creditapplicationList = creditapplicationList;
exports.cancelRefundDetails = cancelRefundDetails;
exports.existProviderPatient = existProviderPatient;
exports.getAppDocumentPlan = getAppDocumentPlan;
exports.getplanDocumentPlan = getplanDocumentPlan;
exports.customerUpload = customerUpload;
exports.creditapplicationPlanReview = creditapplicationPlanReview;