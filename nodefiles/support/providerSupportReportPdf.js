module.exports = (data,logoimg) => {
    var html = '';
    let total_opened = 0;
    let total_closed = 0;
    let total_hold = 0;
    let total_pending = 0;

    var tickArr = data.result.reduce((acc, obj) => {
      const key1 = obj['date_created'];
      const key2 = obj['provider_id'];

      const key = key1+'-'+key2

      var opn = 0
      var clo = 0
      var pen = 0
      var hol = 0
      if(obj['status'] == 0) {
         ++clo
      }
      if(obj['status'] == 1 || obj['status'] == 2) {
         ++pen
      } 
      if(obj['status'] == 2) {
         ++hol
      }

      if (!acc[key]) {
         acc[key] = {
         ticket_opened:clo+pen,
         ticket_closed:clo,
         ticket_pending:pen,
         ticket_hold:hol,
         provider_id:obj['provider_id'],
         provider_name:obj['name'],
         date_created:obj['date_created'],
      };
      } else {
      acc[key].ticket_opened +=clo+pen;
      acc[key].ticket_closed +=clo;
      acc[key].ticket_pending +=pen;
      acc[key].ticket_hold +=hol;
      provider_id:obj['provider_id'];
      provider_name:obj['name'];
      date_created:obj['date_created'];
    }

        return acc;
    }, []);

    var ticket_array = new Array();
    for (var items in tickArr){
      ticket_array.push( tickArr[items] );
    }

    ticket_array.forEach(function (element, idx) {
        
        total_opened = total_opened + element.ticket_opened;
        total_closed = total_closed + element.ticket_closed;
        total_hold = total_hold + element.ticket_hold;
        total_pending = total_pending + element.ticket_pending;        

        html = html + '<tr>'
            + '<td>' + element.date_created + '</td>'
            + '<td>' + element.provider_name + '</td>'
            + '<td>' + element.ticket_opened + '</td>'
            + '<td>' + element.ticket_closed + '</td>'
            + '<td>' + element.ticket_hold + '</td>'
            + '<td>' + element.ticket_pending + '</td>'
            + '</tr>';
    });
    html = html + '<tr>'
        + '<td>Total</td>'
        + '<td></td>'
        + '<td>' + total_opened + '</td>'
        + '<td>' + total_closed + '</td>'
        + '<td>' + total_hold + '</td>'
        + '<td>' + total_pending + '</td>'
        + '</tr>';
    return `
 <!doctype html>
 <html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       
       <title>Provider Support Reports</title>
       <style>
            html{zoom: 0.7;}
          .invoice-box {
          max-width: 1200px;
          margin: auto;
          padding: 0px;
          font-size: 12px;
          line-height: 16px;
          font-family: 'Arial', sans-serif;
          color: #303030;
          }
          .margin-top {
          margin-top: 50px;
          }
          .justify-center {
          text-align: center;
          }
          .invoice-box table {
          width: 100%;
          line-height: inherit;
          text-align: left;
       border:solid 1px #ddf1ff; border-collapse:collapse;}
       .invoice-box table td{border:solid 1px #ddf1ff; border-collapse:collapse;}
       .provider-info{width:48%; float:left}
       .invoice-info{width:48%; float:right;}
       .blue-tble td{background:#f1f9ff; padding:8px 15px; font-size:10px;}
       .blue-tble th{background:#0e5d97; color:#fff;padding:8px 15px; font-size:10px;}
       .invoice-box:after, .info-bx:after{content:""; clear:both; display:block; width:100%;}
       .info-bx:after{margin-bottom:20px;}
       .detail-table th{background:#0e5d97; color:#fff;padding:5px 2px; font-size:10px; line-height:16px;}
       .detail-table td{padding:5px 2px; font-size:10px;}
          
          
       </style>
    </head>
    <body>
       <div class="invoice-box">
       <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
      <h2 class="justify-center">Provider Support Reports</h2>
      <div class="detail-bx">
         <div class="detail-table">
            <table cellpadding="0" cellspacing="0">
               <tr>
                  <th>Month</th>
                  <th>Provider</th>
                  <th>Opened</th>
                  <th>Closed</th>
                  <th>Hold</th>
                  <th>Pending</th>
               </tr>
               ${html}
            </table>
         </div>
      </div>
      
         
       </div>
    </body>
 </html>
     `;
};