

var customerAccountFilter = function (con, data, callback) {
    let cond = 'patient.status = 1';
    if (data.first_name != undefined && data.first_name != '') {
        cond = cond + ' AND patient.f_name ="' + data.first_name + '"';
    }
    if (data.last_name != undefined && data.last_name != '') {
        cond = cond + ' AND patient.l_name ="' + data.last_name + '"';
    }

    if (data.phone != undefined && data.phone != '') {
        cond = cond + ' AND patient.peimary_phone ="' + data.phone + '"';
    }
    if (data.ssn != undefined && data.ssn != '') {
        cond = cond + ' AND AES_DECRYPT(patient.ssn, "ramesh_cogniz") ="' + data.ssn + '"';
    }

    if (data.dob != undefined && data.dob != '') {
        cond = cond + ' AND AES_DECRYPT(patient.dob, "ramesh_cogniz") ="' + data.dob + '"';
    }

    var obj = {}
    con.query('SELECT '
        + 'patient.patient_ac, '
        + 'patient.f_name, '
        + 'patient.l_name, '
        + 'AES_DECRYPT(patient.dob, "ramesh_cogniz") AS dob, '
        + 'AES_DECRYPT(patient.ssn, "ramesh_cogniz") AS ssn, '
        + 'patient.peimary_phone, '

        + 'patient_address.address1, '
        + 'patient_address.address2, '
        + 'patient_address.City, '
        + 'patient_address.zip_code, '
        + 'patient_address.state_id, '

        + 'states.name AS state,'
        + 'states.country_id,'

        + 'countries.name AS country, '

        + 'credit_applications.application_id, '

        + 'user.user_id, '
        + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists '

        + 'FROM patient '

        + 'INNER JOIN patient_address '
        + 'ON patient.patient_id = patient_address.patient_id '

        + 'INNER JOIN states '
        + 'ON patient_address.state_id = states.state_id '

        + 'INNER JOIN countries '
        + 'ON states.country_id = countries.id '

        + 'INNER JOIN credit_applications '
        + 'ON credit_applications.patient_id = patient.patient_id '

        + 'INNER JOIN user '
        + 'ON patient.patient_id = user.patient_id '

        + 'WHERE ' + cond + ' GROUP BY patient.patient_id'
        , function (error, rows, fields) {
            console.log(this.sql)
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {

                let moment = require('moment');

                var userID = [];
                rows.forEach(function (element, idx) {
                    rows[idx].dob = moment(rows[idx].dob.toString()).format('MM/DD/YYYY');
                    rows[idx].ssn = rows[idx].ssn.toString();
                    userID.push(element.user_id)
                })

                obj.status = 1;
                obj.filter_by = data.sort_by
                obj.result = rows;
                return callback(obj);

            }
        })

}

var providerAccountFilter = function (con, data, callback) {
    let cond = 'provider.status = 1';
    if (data.provider_name !== '' && data.provider_name != undefined) {
        cond = cond + ' AND provider.name ="' + data.provider_name + '"';
    }
    if (data.phone != undefined && data.phone != '') {
        cond = cond + ' AND provider.primary_phone ="' + data.phone + '"';
    }
    if (data.ssn != undefined && data.ssn != '') {
        cond = cond + ' AND provider.tax_ssn_id ="' + data.ssn + '"';
    }

    var obj = {}
    con.query('SELECT '
        + 'provider.provider_id, '
        + 'provider.provider_ac, '
        + 'provider.name, '
        + 'provider.tax_ssn_id, '
        + 'provider.primary_phone, '

        + 'provider_location.address1, '
        + 'provider_location.address2, '
        + 'provider_location.city, '
        + 'provider_location.zip_code, '

        + 'states.name AS state,'
        + 'states.country_id,'

        + 'countries.name AS country, '

        + 'user.user_id '

        + 'FROM provider '

        + 'INNER JOIN provider_location ON provider_location.provider_id=provider.provider_id AND provider_location.status =1 AND provider_location.primary_address_flag= 1 AND (provider_location.parent_provider_location_id IS NULL OR (provider_location.parent_provider_location_id IS NOT NULL AND provider_location.billing_address_flag = 1)) '

        + 'INNER JOIN states '
        + 'ON provider_location.state = states.state_id '

        + 'INNER JOIN countries '
        + 'ON states.country_id = countries.id '

        + 'INNER JOIN user '
        + 'ON provider.provider_id = user.client_id '

        + 'WHERE ' + cond + ' GROUP BY provider.provider_id'
        , function (error, rows, fields) {
            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                obj.status = 1;
                obj.filter_by = data.sort_by
                obj.result = rows;
                return callback(obj);
            }
        })

}

var createTicket = function (con, data, callback) {
    var obj = {};

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    con.query('INSERT INTO cs_ticket SET '
        + 'ticket_source=?, '
        //+ 'incoming_call_flag=?, '
        + 'ac_ticket_flag=?, '
        + 'plan_ticket_flag=?, '
        + 'app_ticket_flag=?, '
        + 'invoice_ticket_flag=?, '
        //+ 'follow_up=?, '
        //+ 'follow_up_date_time=?, '
        + 'subject=?, '
        + 'provider_id=?, '
        + 'customer_id=?, '
        + 'app_id=?, '
        + 'pp_id=?, '
        + 'cust_inv_id=?, '
        + 'provider_inv_id=?, '
        + 'status=?, '
        + 'date_created=?, '
        + 'date_modified=?, '
        + 'created_by=?, '
        + 'modified_by=? ',
        [
            data.ticket_source,
            // data.call_type,
            (data.ticket_related_ac) ? data.ticket_related_ac : 0,
            (data.ticket_related_plan) ? data.ticket_related_plan : 0,
            (data.ticket_related_app) ? data.ticket_related_app : 0,
            (data.ticket_related_invoice) ? data.ticket_related_invoice : 0,
            //data.follow_up,
            //data.follow_up_date_time,
            data.subject,
            data.provider_id,
            data.customer_id,
            (data.application_id) ? data.application_id : data.application_no,
            (data.plan_no) ? data.plan_no : null,
            (!data.provider_id) ? (data.invoice_no) ? data.invoice_no : null : null,
            (data.provider_id) ? data.invoice_no : null,
            (data.note) ? 0 : 1,
            current_date,
            current_date,
            current_user_id,
            current_user_id
        ]
        , function (err, ticket) {

            if (err) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                data.application_id = (data.application_no) ? data.application_no : data.application_id
                con.query('INSERT INTO cs_ticket_details SET '
                    + 'ticket_id=?, '
                    //+ 'provider_id=?, '
                    //+ 'provider_invoice_id=?, '
                    //+ 'application_id=?, '
                    //+ 'pp_id=?, '
                    //+ 'customer_invoice_id=?, '
                    + 'caller_name=?, '
                    + 'hps_agent_name=?, '
                    + 'incoming_call_flag=?, '
                    + 'call_date_time=?, '
                    //+ 'reason=?, '
                    + 'description=?, '
                    + 'follow_up_flag=?, '
                    + 'follow_up_sch=?, '
                    + 'commented_by=?, '
                    + 'status=?, '
                    + 'date_created=?, '
                    + 'date_modified=?, '
                    + 'created_by=?, '
                    + 'modified_by=? ',
                    [
                        ticket.insertId,
                        //data.provider_id,
                        //(data.provider_id) ? data.invoice_no : null,
                        //data.application_id,
                        //(data.plan_no) ? data.plan_no : null,
                        //(!data.provider_id) ? data.invoice_no : null,
                        data.caller_name,
                        data.hps_agent_name,
                        data.call_type,
                        (data.commented_by == 2) ? current_date : data.call_date_time,
                        //data.subject,
                        data.description,
                        data.follow_up,
                        data.follow_up_date_time,
                        data.commented_by,
                        1,
                        current_date,
                        current_date,
                        current_user_id,
                        current_user_id
                    ]
                    , function (error, ticketDetails) {

                        if (error) {
                            obj.status = 0;
                            obj.result = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            obj.ticket_insert = {
                                "status": (data.note) ? "Close" : "Open",
                                "ticket_id": ticket.insertId,
                                "ticket_number": data.ticket_number,
                                "call_date_time": data.call_date_time,
                                "ticket_source": data.ticket_source,
                                "application_no": data.application_id,
                                "plan_no": data.plan_no,
                                "invoice_no": data.invoice_no,
                                "caller_name": data.caller_name,
                                "subject": data.subject,
                                "description": data.description,
                                "provider_id": data.provider_id,
                                "customer_id": data.customer_id,
                                "commented_by": data.commented_by,
                                "hps_agent_name": data.hps_agent_name,
                            }
                            obj.status = 1;
                            //obj.result = rows;
                            return callback(obj);
                        }
                    })

            }
        })
}

var createTicketDetail = function (con, data, callback) {
    var obj = {};

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = data.current_user_id;

    con.query('UPDATE cs_ticket SET '
        + 'status=?, '

        + 'date_modified=?, '
        + 'modified_by=? '

        + 'WHERE ticket_id=? '
        , [data.ticket_status, current_date, current_user_id, data.ticket_id]
        , function (error, ticketStatus) {

            if (error) {
                obj.status = 0;
                obj.result = "Something wrong please try again.";
                return callback(obj);
            } else {
                con.query('UPDATE cs_ticket_details SET '
                    + 'follow_up_flag=?, '
                    + 'date_modified=?, '
                    + 'modified_by=? '
                    + 'WHERE ticket_id=? '
                    , [0, current_date, current_user_id, data.ticket_id]
                    , function (error, ticketStatus) {
                        if (error) {
                            obj.status = 0;
                            obj.result = "Something wrong please try again.";
                            return callback(obj);
                        } else {
                            con.query('INSERT INTO cs_ticket_details SET '
                                + 'ticket_id=?, '
                                + 'caller_name=?, '
                                + 'hps_agent_name=?, '
                                + 'incoming_call_flag=?, '
                                + 'call_date_time=?, '
                                //+ 'reason=?, '
                                + 'description=?, '
                                + 'follow_up_flag=?, '
                                + 'follow_up_sch=?, '
                                + 'commented_by=?, '
                                + 'status=?, '
                                + 'date_created=?, '
                                + 'date_modified=?, '
                                + 'created_by=?, '
                                + 'modified_by=? ',
                                [
                                    data.ticket_id,
                                    data.caller_name,
                                    data.hps_agent_name,
                                    data.call_type,
                                    current_date,
                                    //data.subject,
                                    data.description,
                                    data.follow_up,
                                    data.follow_up_date_time,
                                    data.commented_by,
                                    1,
                                    current_date,
                                    current_date,
                                    current_user_id,
                                    current_user_id
                                ]
                                , function (error, ticketDetails) {

                                    if (error) {
                                        obj.status = 0;
                                        obj.result = "Something wrong please try again.";
                                        return callback(obj);
                                    } else {
                                        obj.ticket_insert = {
                                            "status": data.ticket_status,
                                            "ticket_submit": data.submit,
                                            "ticket_id": data.ticket_id,
                                            "cs_tkt_dtl_id": ticketDetails.insertId,
                                            "call_date_time": current_date,
                                            "call_type": data.call_type,
                                            "caller_name": data.caller_name,

                                            "description": data.description,

                                            "follow_up": data.follow_up,
                                            "follow_up_sch": data.follow_up_date_time,
                                            "commented_by": data.commented_by,
                                            "hps_agent_name": data.hps_agent_name,
                                            "ticket_status": (data.ticket_status == 0) ? 'Close' : (data.ticket_status == 1) ? 'Open' : 'Hold',
                                        }
                                        obj.status = 1;
                                        //obj.result = rows;
                                        return callback(obj);
                                    }
                                })
                        }
                    })
            }
        })
}

var getAllCustomerTickets = function (con, data, callback) {
    var obj = {};
    con.query('SELECT '
        + 'cs_ticket.ticket_id, '
        //+ 'cs_ticket.ticket_number, '
        + 'cs_ticket.ticket_source AS ticket_source_id, '
        + 'cs_ticket.ac_ticket_flag, '
        + 'cs_ticket.app_ticket_flag, '
        + 'cs_ticket.plan_ticket_flag, '
        + 'cs_ticket.invoice_ticket_flag, '
        + 'cs_ticket.subject, '
        + 'cs_ticket.provider_id, '
        + 'cs_ticket.customer_id, '
        + 'cs_ticket.app_id, '
        + 'cs_ticket.pp_id, '
        + 'cs_ticket.cust_inv_id, '
        + 'cs_ticket.provider_inv_id, '
        + 'cs_ticket.status AS status_id, '

        + 'cs_ticket_details.caller_name, '
        + 'cs_ticket_details.incoming_call_flag, '
        //+ 'cs_ticket_details.phone_no, '
        + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
        + 'cs_ticket_details.description, '
        + 'cs_ticket_details.follow_up_flag, '
        + 'cs_ticket_details.follow_up_sch, '

        + 'credit_applications.application_no, '
        + 'payment_plan.pp_id AS plan_number, '
        + 'patient_invoice.invoice_id AS invoice_number, '

        + 'master_data_values.value AS ticket_source, '
        + 'master_data_values2.value AS status '

        + 'FROM cs_ticket '

        + 'INNER JOIN cs_ticket_details '
        + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

        + 'LEFT JOIN credit_applications '
        + 'ON credit_applications.application_id = cs_ticket.app_id '

        + 'LEFT JOIN payment_plan '
        + 'ON payment_plan.pp_id = cs_ticket.pp_id '

        + 'LEFT JOIN patient_invoice '
        + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

        + 'LEFT JOIN master_data_values '
        + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

        + 'LEFT JOIN master_data_values AS master_data_values2 '
        + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

        + 'WHERE cs_ticket.app_id = ? AND cs_ticket.provider_id IS NULL '
        + 'GROUP BY cs_ticket.ticket_id '
        + 'ORDER BY cs_ticket.ticket_id DESC'
        ,
        [
            data.appid
        ]
        , function (error, tickets) {
            //console.log(error)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
                obj.tickets = tickets;
                con.query('SELECT '
                    + 'value, '
                    + 'status_id '

                    + 'FROM master_data_values '

                    + 'WHERE md_id = ?'

                    , ['CS Ticket Source']
                    , function (error, masterTicketSource) {
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Master Ticket Source not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.masterTicketSource = masterTicketSource
                            con.query('SELECT '
                                + 'pp_id, '
                                + 'plan_number '

                                + 'FROM payment_plan '

                                + 'WHERE application_id = ?'
                                , [data.appid]
                                , function (error, customerPlan) {
                                    if (error) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Customer Plan not found.";
                                            return callback(obj);
                                        });
                                    } else {
                                        obj.customerPlan = customerPlan
                                        con.query('SELECT '
                                            + 'patient_invoice.invoice_id, '
                                            + 'patient_invoice.invoice_number, '

                                            + 'patient_invoice_details.invoice_id, '
                                            + 'patient_invoice_details.pp_id '

                                            + 'FROM patient_invoice '

                                            + 'INNER JOIN patient_invoice_details '
                                            + 'ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '

                                            + 'WHERE patient_invoice.application_id = ? GROUP BY patient_invoice.invoice_id'
                                            , [data.appid]
                                            , function (error, customerInvoice) {
                                                if (error) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Customer Invoice not found.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.customerInvoice = customerInvoice
                                                    con.commit(function (err) {
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Plan not found.";
                                                                return callback(obj);
                                                            });
                                                        }
                                                        obj.status = 1;

                                                        return callback(obj);
                                                    });
                                                }
                                            })
                                    }
                                })
                        }
                    })


            }
        })

}

var getCustomerTickets = function (con, data, callback) {
    //console.log(data)
    //return false
    var obj = {};
    con.query('SELECT '
        + 'user.patient_id, '

        + 'credit_applications.application_id '

        + 'FROM user '
        + 'INNER JOIN credit_applications '
        + 'ON user.patient_id = credit_applications.patient_id '

        + 'WHERE user.user_id = ?'
        , [data.appid]
        , function (err, app_id) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Application ID not found";
                    return callback(obj);
                });
            } else {
                con.query('SELECT '
                    + 'cs_ticket.ticket_id, '
                    //+ 'cs_ticket.ticket_number, '
                    + 'cs_ticket.ticket_source AS ticket_source_id, '
                    + 'cs_ticket.ac_ticket_flag, '
                    + 'cs_ticket.app_ticket_flag, '
                    + 'cs_ticket.plan_ticket_flag, '
                    + 'cs_ticket.invoice_ticket_flag, '
                    + 'cs_ticket.subject, '
                    + 'cs_ticket.provider_id, '
                    + 'cs_ticket.customer_id, '
                    + 'cs_ticket.app_id, '
                    + 'cs_ticket.pp_id, '
                    + 'cs_ticket.cust_inv_id, '
                    + 'cs_ticket.provider_inv_id, '
                    + 'cs_ticket.status AS status_id, '

                    + 'cs_ticket_details.caller_name, '
                    + 'cs_ticket_details.incoming_call_flag, '
                    //+ 'cs_ticket_details.phone_no, '
                    + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
                    + 'cs_ticket_details.description, '
                    + 'cs_ticket_details.follow_up_flag, '
                    + 'cs_ticket_details.follow_up_sch, '

                    + 'credit_applications.application_no, '
                    + 'payment_plan.pp_id AS plan_number, '
                    + 'patient_invoice.invoice_id AS invoice_number, '

                    + 'master_data_values.value AS ticket_source, '
                    + 'master_data_values2.value AS status '

                    + 'FROM cs_ticket '

                    + 'INNER JOIN cs_ticket_details '
                    + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

                    + 'LEFT JOIN credit_applications '
                    + 'ON credit_applications.application_id = cs_ticket.app_id '

                    + 'LEFT JOIN payment_plan '
                    + 'ON payment_plan.pp_id = cs_ticket.pp_id '

                    + 'LEFT JOIN patient_invoice '
                    + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

                    + 'LEFT JOIN master_data_values '
                    + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

                    + 'LEFT JOIN master_data_values AS master_data_values2 '
                    + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

                    + 'WHERE cs_ticket.app_id = ? AND cs_ticket.provider_id IS NULL '
                    + 'GROUP BY cs_ticket.ticket_id '
                    + 'ORDER BY cs_ticket.ticket_id DESC'
                    ,
                    [
                        app_id[0].application_id
                    ]
                    , function (error, tickets) {
                        //console.log(error)
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Ticket not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.tickets = tickets;
                            con.query('SELECT '
                                + 'value, '
                                + 'status_id '

                                + 'FROM master_data_values '

                                + 'WHERE md_id = ?'

                                , ['CS Ticket Source']
                                , function (error, masterTicketSource) {
                                    if (error) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Master Ticket Source not found.";
                                            return callback(obj);
                                        });
                                    } else {
                                        obj.masterTicketSource = masterTicketSource
                                        con.query('SELECT '
                                            + 'pp_id, '
                                            + 'plan_number '

                                            + 'FROM payment_plan '

                                            + 'WHERE application_id = ?'
                                            , [app_id[0].application_id]
                                            , function (error, customerPlan) {
                                                console.log(this.sql)
                                                if (error) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Customer Plan not found.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.customerPlan = customerPlan
                                                    con.query('SELECT '
                                                        + 'invoice_id, '
                                                        + 'invoice_number '

                                                        + 'FROM patient_invoice '

                                                        + 'WHERE application_id = ?'
                                                        , [app_id[0].application_id]
                                                        , function (error, customerInvoice) {
                                                            if (error) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Customer Invoice not found.";
                                                                    return callback(obj);
                                                                });
                                                            } else {
                                                                obj.customerInvoice = customerInvoice
                                                                con.commit(function (err) {
                                                                    if (err) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Plan not found.";
                                                                            return callback(obj);
                                                                        });
                                                                    }
                                                                    obj.status = 1;

                                                                    return callback(obj);
                                                                });
                                                            }
                                                        })
                                                }
                                            })
                                    }
                                })


                        }
                    })
            }
        })



}
var getCosignerTickets = function (con, data, callback) {
    //console.log(data)
    //return false
    var obj = {};
    con.query('SELECT '
        + 'credit_applications.patient_id, '

        + 'credit_applications.application_id '

        + 'FROM user '
        + 'INNER JOIN credit_app_cosigner ON credit_app_cosigner.co_signer_id=user.co_signer_id '
        + 'INNER JOIN credit_applications ON credit_app_cosigner.application_id = credit_applications.application_id '
        + 'WHERE user.user_id = ?'
        , [data.appid]
        , function (err, app_id) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Application ID not found";
                    return callback(obj);
                });
            } else {
                const uniqueID = [...new Set(app_id.map(item => item.application_id))];
                //console.log(uniqueID)
                con.query('SELECT '
                    + 'cs_ticket.ticket_id, '
                    //+ 'cs_ticket.ticket_number, '
                    + 'cs_ticket.ticket_source AS ticket_source_id, '
                    + 'cs_ticket.ac_ticket_flag, '
                    + 'cs_ticket.app_ticket_flag, '
                    + 'cs_ticket.plan_ticket_flag, '
                    + 'cs_ticket.invoice_ticket_flag, '
                    + 'cs_ticket.subject, '
                    + 'cs_ticket.provider_id, '
                    + 'cs_ticket.customer_id, '
                    + 'cs_ticket.app_id, '
                    + 'cs_ticket.pp_id, '
                    + 'cs_ticket.cust_inv_id, '
                    + 'cs_ticket.provider_inv_id, '
                    + 'cs_ticket.status AS status_id, '

                    + 'cs_ticket_details.caller_name, '
                    + 'cs_ticket_details.incoming_call_flag, '
                    //+ 'cs_ticket_details.phone_no, '
                    + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
                    + 'cs_ticket_details.description, '
                    + 'cs_ticket_details.follow_up_flag, '
                    + 'cs_ticket_details.follow_up_sch, '

                    + 'credit_applications.application_no, '
                    + 'payment_plan.pp_id AS plan_number, '
                    + 'patient_invoice.invoice_id AS invoice_number, '

                    + 'master_data_values.value AS ticket_source, '
                    + 'master_data_values2.value AS status '

                    + 'FROM cs_ticket '

                    + 'LEFT JOIN cs_ticket_details '
                    + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

                    + 'LEFT JOIN credit_applications '
                    + 'ON credit_applications.application_id = cs_ticket.app_id '

                    + 'LEFT JOIN payment_plan '
                    + 'ON payment_plan.pp_id = cs_ticket.pp_id '

                    + 'LEFT JOIN patient_invoice '
                    + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

                    + 'LEFT JOIN master_data_values '
                    + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

                    + 'LEFT JOIN master_data_values AS master_data_values2 '
                    + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

                    + 'WHERE cs_ticket.app_id IN (?) AND cs_ticket.provider_id IS NULL '
                    + 'GROUP BY cs_ticket.ticket_id '
                    + 'ORDER BY cs_ticket.ticket_id DESC'
                    ,
                    [
                        uniqueID
                    ]
                    , function (error, tickets) {
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Ticket not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.tickets = tickets;
                            con.query('SELECT '
                                + 'value, '
                                + 'status_id '

                                + 'FROM master_data_values '

                                + 'WHERE md_id = ?'

                                , ['CS Ticket Source']
                                , function (error, masterTicketSource) {
                                    if (error) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Master Ticket Source not found.";
                                            return callback(obj);
                                        });
                                    } else {
                                        obj.masterTicketSource = masterTicketSource
                                        con.query('SELECT '
                                            + 'pp_id, '
                                            + 'plan_number '

                                            + 'FROM payment_plan '

                                            + 'WHERE application_id IN(?)'
                                            , [uniqueID]
                                            , function (error, customerPlan) {
                                                
                                                if (error) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Customer Plan not found.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.customerPlan = customerPlan
                                                    con.query('SELECT '
                                                        + 'invoice_id, '
                                                        + 'invoice_number '

                                                        + 'FROM patient_invoice '

                                                        + 'WHERE application_id IN(?)'
                                                        , [uniqueID]
                                                        , function (error, customerInvoice) {
                                                            if (error) {
                                                                con.rollback(function () {
                                                                    obj.status = 0;
                                                                    obj.message = "Customer Invoice not found.";
                                                                    return callback(obj);
                                                                });
                                                            } else {
                                                                obj.customerInvoice = customerInvoice
                                                                con.commit(function (err) {
                                                                    if (err) {
                                                                        con.rollback(function () {
                                                                            obj.status = 0;
                                                                            obj.message = "Plan not found.";
                                                                            return callback(obj);
                                                                        });
                                                                    }
                                                                    obj.status = 1;

                                                                    return callback(obj);
                                                                });
                                                            }
                                                        })
                                                }
                                            })
                                    }
                                })


                        }
                    })
            }
        })



}


var getAllProviderTickets = function (con, data, callback) {
    var obj = {};
    con.query('SELECT '
        + 'cs_ticket.ticket_id, '
        //+ 'cs_ticket.ticket_number, '
        + 'cs_ticket.ticket_source AS ticket_source_id, '
        + 'cs_ticket.ac_ticket_flag, '
        + 'cs_ticket.app_ticket_flag, '
        + 'cs_ticket.plan_ticket_flag, '
        + 'cs_ticket.invoice_ticket_flag, '
        + 'cs_ticket.subject, '
        + 'cs_ticket.provider_id, '
        + 'cs_ticket.customer_id, '
        + 'cs_ticket.app_id, '
        + 'cs_ticket.pp_id, '
        + 'cs_ticket.cust_inv_id, '
        + 'cs_ticket.provider_inv_id, '
        + 'cs_ticket.status AS status_id, '

        + 'cs_ticket_details.caller_name, '
        + 'cs_ticket_details.incoming_call_flag, '
        //+ 'cs_ticket_details.phone_no, '
        + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
        + 'cs_ticket_details.description, '
        + 'cs_ticket_details.follow_up_flag, '
        + 'cs_ticket_details.follow_up_sch, '

        + 'credit_applications.application_no, '

        + 'provider_invoice.invoice_number, '

        + 'master_data_values.value AS ticket_source, '
        + 'master_data_values2.value AS status '

        + 'FROM cs_ticket '

        + 'INNER JOIN cs_ticket_details '
        + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

        + 'LEFT JOIN credit_applications '
        + 'ON credit_applications.application_id = cs_ticket.app_id '

        + 'LEFT JOIN provider_invoice '
        + 'ON provider_invoice.provider_invoice_id = cs_ticket.provider_inv_id '

        + 'LEFT JOIN master_data_values '
        + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

        + 'LEFT JOIN master_data_values AS master_data_values2 '
        + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

        + 'WHERE cs_ticket.provider_id = ? '
        + 'GROUP BY cs_ticket.ticket_id '
        + 'ORDER BY cs_ticket.ticket_id DESC'
        ,
        [
            data.provider_id, 1
        ]
        , function (error, tickets) {
            //console.log(this.sql)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
                obj.tickets = tickets;
                con.query('SELECT '
                    + 'value, '
                    + 'status_id '

                    + 'FROM master_data_values '

                    + 'WHERE md_id = ?'

                    , ['CS Ticket Source']
                    , function (error, masterTicketSource) {
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Master Ticket Source not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.masterTicketSource = masterTicketSource
                            con.query('SELECT '
                                + 'patient_provider.patient_id, '
                                + 'patient_provider.provider_id, '

                                + 'credit_applications.application_no, '
                                + 'credit_applications.application_id '

                                + 'FROM patient_provider '

                                + 'INNER JOIN credit_applications '
                                + 'ON credit_applications.patient_id = patient_provider.patient_id '

                                + 'WHERE patient_provider.provider_id = ?'
                                , [data.provider_id]
                                , function (error, providerApplication) {
                                    if (error) {
                                        con.rollback(function () {
                                            obj.status = 0;
                                            obj.message = "Provider Applications not found.";
                                            return callback(obj);
                                        });
                                    } else {
                                        obj.providerApplication = providerApplication
                                        con.query('SELECT '
                                            + 'provider_invoice.provider_invoice_id, '
                                            + 'provider_invoice.invoice_number, '
                                            + 'provider_invoice.provider_id '

                                            + 'FROM provider_invoice '

                                            + 'WHERE provider_invoice.provider_id = ?'
                                            , [data.provider_id]
                                            , function (error, providerInvoice) {
                                                if (error) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Provider Invoice not found.";
                                                        return callback(obj);
                                                    });
                                                } else {
                                                    obj.providerInvoice = providerInvoice
                                                    con.commit(function (err) {
                                                        if (err) {
                                                            con.rollback(function () {
                                                                obj.status = 0;
                                                                obj.message = "Plan not found.";
                                                                return callback(obj);
                                                            });
                                                        }
                                                        obj.status = 1;

                                                        return callback(obj);
                                                    });
                                                }
                                            })
                                    }
                                })
                        }
                    })


            }
        })

}

var getAllTickets = function (con, data, callback) {
    let cond = ''
    if(data.status == 0){
        cond += ' AND cs_ticket.status=0 '
    } else if(data.status == 1){
        cond += ' AND cs_ticket.status=1 '
    } else if(data.status == 2){
        cond += ' AND cs_ticket.status=2 '
    } else if(data.status == 3){
        cond += ' AND cs_ticket_details.follow_up_flag=1 '
    }
    if(data.ticketType == 0){
        cond += ' AND cs_ticket.customer_id IS NOT NULL '
    } else {
        cond += ' AND cs_ticket.provider_id IS NOT NULL '
    }
    var obj = {};
    con.query('SELECT '
        + 'cs_ticket.ticket_id, '
        //+ 'cs_ticket.ticket_number, '
        + 'cs_ticket.ticket_source AS ticket_source_id, '
        + 'cs_ticket.ac_ticket_flag, '
        + 'cs_ticket.app_ticket_flag, '
        + 'cs_ticket.plan_ticket_flag, '
        + 'cs_ticket.invoice_ticket_flag, '
        + 'cs_ticket.subject, '
        + 'cs_ticket.provider_id, '
        + 'cs_ticket.customer_id, '
        + 'cs_ticket.app_id, '
        + 'cs_ticket.pp_id, '
        + 'cs_ticket.cust_inv_id, '
        + 'cs_ticket.provider_inv_id, '
        + 'cs_ticket.status AS status_id, '

        + 'cs_ticket_details.caller_name, '
        + 'cs_ticket_details.incoming_call_flag, '
        //+ 'cs_ticket_details.phone_no, '
        + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
        + 'cs_ticket_details.description, '
        + 'cs_ticket_details.follow_up_flag, '
        + 'cs_ticket_details.follow_up_sch, '

        + 'credit_applications.application_no, '
        + 'payment_plan.plan_number, '
        + 'patient_invoice.invoice_number, '
        + 'provider_invoice.invoice_number AS provider_invoice_number, '
        + 'patient.f_name, patient.m_name, patient.l_name, '
        + 'provider.name as provider_name, '
        + 'master_data_values.value AS ticket_source, '
        + 'master_data_values2.value AS status, '
        + '(SELECT follow_up_flag FROM cs_ticket_details WHERE ticket_id = cs_ticket.ticket_id ORDER BY cs_tkt_dtl_id DESC LIMIT 1) as follow_up_admin '
        + 'FROM cs_ticket '

        + 'LEFT JOIN cs_ticket_details '
        + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

        + 'LEFT JOIN credit_applications '
        + 'ON credit_applications.application_id = cs_ticket.app_id '

        + 'LEFT JOIN patient '
        + 'ON cs_ticket.customer_id = patient.patient_id '

        + 'LEFT JOIN payment_plan '
        + 'ON payment_plan.pp_id = cs_ticket.pp_Id '

        + 'LEFT JOIN patient_invoice '
        + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

        + 'LEFT JOIN provider_invoice '
        + 'ON provider_invoice.provider_invoice_id = cs_ticket.provider_inv_id '

        + 'LEFT JOIN provider '
        + 'ON cs_ticket.provider_id = provider.provider_id '

        + 'LEFT JOIN master_data_values '
        + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

        + 'LEFT JOIN master_data_values AS master_data_values2 '
        + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

        + 'WHERE cs_ticket.status IN (?) ' + cond
        + 'GROUP BY cs_ticket.ticket_id '
        + 'ORDER BY cs_ticket.ticket_id DESC'
        ,
        [
            [0, 1, 2]
        ]
        , function (error, tickets) {
            //console.log(this.sql)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
                obj.tickets = tickets;
                con.query('SELECT '
                    + 'value, '
                    + 'status_id '

                    + 'FROM master_data_values '

                    + 'WHERE md_id = ?'

                    , ['CS Ticket Source']
                    , function (error, masterTicketSource) {
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Master Ticket Source not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.masterTicketSource = masterTicketSource
                            con.commit(function (err) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Plan not found.";
                                        return callback(obj);
                                    });
                                }
                                obj.status = 1;

                                return callback(obj);
                            });
                        }
                    })


            }
        })

}

var getSingleTicket = function (con, data, callback) {
    var obj = {};
    con.query('SELECT '
        + 'cs_ticket.ticket_id, '
        //+ 'cs_ticket.ticket_number, '
        + 'cs_ticket.ticket_source AS ticket_source_id, '
        + 'cs_ticket.ac_ticket_flag, '
        + 'cs_ticket.app_ticket_flag, '
        + 'cs_ticket.plan_ticket_flag, '
        + 'cs_ticket.invoice_ticket_flag, '
        + 'cs_ticket.subject, '
        + 'cs_ticket.provider_id, '
        + 'cs_ticket.customer_id, '
        + 'cs_ticket.app_id, '
        + 'cs_ticket.pp_id, '
        + 'cs_ticket.cust_inv_id, '
        + 'cs_ticket.provider_inv_id, '
        + 'cs_ticket.status AS status_id, '
        + 'DATE_FORMAT(cs_ticket.date_created, "%m/%d/%Y %H:%i:%s") AS date_created, '

        + 'cs_ticket_details.caller_name, '
        + 'cs_ticket_details.hps_agent_name, '
        + 'cs_ticket_details.incoming_call_flag, '
        + 'cs_ticket_details.cs_tkt_dtl_id, '
        + 'cs_ticket_details.commented_by, '
        //+ 'cs_ticket_details.phone_no, '
        + 'DATE_FORMAT(cs_ticket_details.call_date_time, "%m/%d/%Y %H:%i:%s") AS call_date_time, '
        + 'cs_ticket_details.description, '
        + 'cs_ticket_details.follow_up_flag, '
        + 'DATE_FORMAT(cs_ticket_details.follow_up_sch, "%m/%d/%Y %H:%i:%s") AS follow_up_sch, '

        + 'credit_applications.application_no, '
        + 'payment_plan.plan_number, '
        + 'patient_invoice.invoice_number, '
        + 'provider_invoice.invoice_number AS provider_invoice_number, '

        + 'master_data_values.value AS ticket_source, '
        + 'patient.f_name,patient.m_name,patient.l_name,patient.patient_ac,provider.provider_ac,provider.name,'
        + 'master_data_values2.value AS status '

        + 'FROM cs_ticket '

        + 'INNER JOIN cs_ticket_details '
        + 'ON cs_ticket.ticket_id = cs_ticket_details.ticket_id '

        + 'LEFT JOIN credit_applications '
        + 'ON credit_applications.application_id = cs_ticket.app_id '

        + 'LEFT JOIN payment_plan '
        + 'ON payment_plan.pp_id = cs_ticket.pp_Id '

        + 'LEFT JOIN patient_invoice '
        + 'ON patient_invoice.invoice_id = cs_ticket.cust_inv_id '

        + 'LEFT JOIN provider_invoice '
        + 'ON provider_invoice.provider_invoice_id = cs_ticket.provider_inv_id '

        + 'LEFT JOIN master_data_values '
        + 'ON master_data_values.status_id = cs_ticket.ticket_source AND master_data_values.md_id="CS Ticket Source" '

        + 'LEFT JOIN master_data_values AS master_data_values2 '
        + 'ON master_data_values2.status_id = cs_ticket.status AND master_data_values2.md_id="CS Ticket Status" '

        + 'LEFT JOIN patient '
        + 'ON patient.patient_id = cs_ticket.customer_id '

        + 'LEFT JOIN provider '
        + 'ON provider.provider_id = cs_ticket.provider_id '

        + 'WHERE cs_ticket.ticket_id = ? '
        + 'ORDER BY cs_ticket.ticket_id DESC'
        ,
        [
            data.ticket_id
        ]
        , function (error, tickets) {
            //console.log(error)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Ticket not found.";
                    return callback(obj);
                });
            } else {
                obj.tickets = tickets;
                con.query('SELECT '
                    + 'value, '
                    + 'status_id '

                    + 'FROM master_data_values '

                    + 'WHERE md_id = ?'

                    , ['CS Ticket Source']
                    , function (error, masterTicketSource) {
                        if (error) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Master Ticket Source not found.";
                                return callback(obj);
                            });
                        } else {
                            obj.masterTicketSource = masterTicketSource
                            con.commit(function (err) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Plan not found.";
                                        return callback(obj);
                                    });
                                }
                                obj.status = 1;

                                return callback(obj);
                            });
                        }
                    })


            }
        })

}

exports.customerAccountFilter = customerAccountFilter;
exports.providerAccountFilter = providerAccountFilter;
exports.createTicket = createTicket;
exports.createTicketDetail = createTicketDetail;
exports.getAllCustomerTickets = getAllCustomerTickets;
exports.getAllProviderTickets = getAllProviderTickets;
exports.getAllTickets = getAllTickets;
exports.getSingleTicket = getSingleTicket;
exports.getCustomerTickets = getCustomerTickets;
exports.getCosignerTickets = getCosignerTickets;