/*
* Title: Admin Invoice
* Descrpation :- This module realted to adin invoice
* Date :- July 03, 2019
* Author :- Ramesh Kumar & Cogniz Software Solution
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/admin-open-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.getOpenInvoiceDetails(con, function (result) {
            res.send(result);
        });

    });

    app.get('/api/admin-view-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.getAdminInvoiceDetails(con, req.query.invoice_id, function (result) {
            res.send(result);
        });

    });
    app.post('/api/admin-delete-application/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        var dataSql = require('./invoice/admininvoiceSql.js');
        
        dataSql.deleteAdminInvoiceapplication(con, req.body, function (delresult) {
            if (delresult.status == 1) {
                dataSql.providerDetails(con, req.body, function (details) {
                    if (details.status == 1) {
                        var emialTemp = require('./emailplan.js');
                        /*var locals = {
                            name: details.provider.name,
                            address: details.provider.address1 + ' ' + details.provider.address2,
                            cityState: details.provider.city + ', ' + details.provider.state_name + ' - ' + details.provider.zip_code,
                            phone: details.provider.primary_phone,
                            comment: req.body.comment,
                            invoicenumber : details.provider.invoice_number
                        };*/
                        //var toEmailAddress = details.provider.email;
                        //var template = 'providerInvoicePlanReject';
                        //emialTemp.emailTemplate(toEmailAddress, locals, template);

                        var context = {
                            provider_name: details.provider.name,
                            provider_address: details.provider.address1 + ' ' + details.provider.address2 + '<br/> ' + details.provider.city + ', ' + details.provider.state_name + ' - ' + details.provider.zip_code,
                            provider_phone: details.provider.primary_phone,
                            hps_comment: req.body.comment,
                            invoice_number : details.provider.invoice_number
                        };
                        var template = 'Provider Invoice Plan Reject';
                        emialTemp.customEmailTemplateDetails(template, function (Result) {
                                                                                
                            if(Result.status == 1) {
                                
                                var emailOpt = {
                                    toEmail: details.provider.email,
                                    subject: Result.result[0].template_subject
                                }
                               
                                var html = Result.result[0].template_content;
                                
                                emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                    if(emailResult.status == 1) {
                                        obj.template = "Email sent successfully!"
                                    } else {
                                        obj.template = "Email not sent successfully!"
                                    }
                                });
                            } else {
                                obj.template = "Template details not found."
                            }

                        });

                    }
                })
                dataSql.providerInvoiceView(con, req.body.invoice_id, function (result) {
                    result.message = "Item removed from invoice successfully";
                    res.send(result);
                });
            } else {
                res.send(delresult);
            }

        });

    });
    app.get('/api/admin-list-application/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.listInvoiceApplication(con, req.query, function (delresult) {
            res.send(delresult);
        });

    });

    app.post('/api/admin-add-application/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.getOpenSingleInvoiceDetails(con, req.body.invoice_id, function (result) {
            if (result.status == 1) {
                dataSql.addInvoiceApplication(con, req.body, result.result, function (result) {
                    dataSql.getOpenInvoiceDetails(con, function (resultOpen) {
                        resultOpen.message = result.message;
                        res.send(resultOpen);
                    })
                });
            } else {
                res.send(result);
            }
        });


    });
    app.post('/api/admin-cancel-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.cancelInvoice(con, req.body, function (result) {
            if(result.status==1){
                
                dataSql.providerDetails(con, req.body, function (details) {
                    if (details.status == 1) {
                        var emialTemp = require('./emailplan.js');
                        /*var locals = {
                            name: details.provider.name,
                            address: details.provider.address1 + ' ' + details.provider.address2,
                            cityState: details.provider.city + ', ' + details.provider.state_name + ' - ' + details.provider.zip_code,
                            phone: details.provider.primary_phone,
                            comment: req.body.commentNote,
                            invoicenumber : details.provider.invoice_number
                        };
                        var toEmailAddress = details.provider.email;
                        var template = 'providerInvoiceReject';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                        var context = {
                            provider_name: details.provider.name,
                            provider_address: details.provider.address1 + ' ' + details.provider.address2 + '<br/> ' + details.provider.city + ', ' + details.provider.state_name + ' - ' + details.provider.zip_code,
                            provider_phone: details.provider.primary_phone,
                            hps_comment: req.body.commentNote,
                            invoice_number : details.provider.invoice_number
                        };
                        var template = 'Provider Invoice Reject';
                        emialTemp.customEmailTemplateDetails(template, function (Result) {
                                                                                
                            if(Result.status == 1) {
                                
                                var emailOpt = {
                                    toEmail: details.provider.email,
                                    subject: Result.result[0].template_subject
                                }
                               
                                var html = Result.result[0].template_content;
                                
                                emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                    if(emailResult.status == 1) {
                                        obj.template = "Template email sent successfully!"
                                    } else {
                                        obj.template = "Template email not sent successfully!"
                                    }
                                });
                            } else {
                                obj.template = "Template details not found."
                            }

                        });

                    }
                })
                
                dataSql.getOpenInvoiceDetails(con, function (resultOpen) {
                    resultOpen.message = result.message;
                    res.send(resultOpen);
                })
            }else{
                res.send(result);
            }
            
        });
    });
    app.post('/api/admin-confirm-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.confirmInvoice(con, req.body, function (result) {
            if(result.status==1){
                dataSql.getOpenInvoiceDetails(con, function (resultOpen) {
                    resultOpen.message = result.message;
                    res.send(resultOpen);
                })
            }else{
                res.send(result);
            }
            
        });
    });
    app.get('/api/admin-close-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.getCloseInvoiceDetails(con, function (result) {
            res.send(result);
        });

    });

    app.get('/api/admin-cancel-confirmations/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.cancelConfirmations(con, req.query, function (result) {
            if (result.status == 1) {
                dataSql.getCloseInvoiceDetails(con, function (resultOpen) {
                    resultOpen.message = result.message;
                    res.send(resultOpen);
                })
            } else {
                res.send(result);
            }

        });
    });
    app.get('/api/admin-invoice-notes/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.getAdminInvoiceNotes(con, req.query.invoice_id, function (result) {
            res.send(result);
        });

    });

    // view single invoice
    app.get('/api/provider-view-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.providerInvoiceView(con, req.query.id, function (result) {
            res.send(result);
        });
    });

    // approve invoice
    app.get('/api/admin-approve-invoice/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        dataSql.approveInvoice(con, req.query, function (result) {
            res.send(result);
        });
    });
}