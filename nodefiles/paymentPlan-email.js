module.exports = function (app, con, jwtMW) {
    let date = require('date-and-time');
    let now = new Date();


    /*
    * get all plan details
    */
    app.get('/api/creditapplication-all-plan-details', jwtMW, (req, res) => {
        // this function use for get credit application details

        var dataSql = require('./payment/payment.sql.js');
        dataSql.getAllPlansDetails(con, req.query.appid, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    /*
    * get data and send email report
    */
    app.get('/api/send-customer-invoice-report', jwtMW, (req, res) => {
        //console.log(req.query.app_id)
        //console.log(req.query.plan_id)
        //console.log(req.query.instlmnt_id)
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var dataSql = require('./payment/payment.sql.js');
        var dataSqlC = require('./credit/creditSql.js');
        var objE = {};
        dataSql.getSingleInstallment(con, req.query.app_id, req.query.plan_id, req.query.instlmnt_id, function (result1) {
            if(result1.status == 1) {
                res.send("something wrong")
            } else {

                dataSql.getInvoicePlans(con, req.query.app_id, function (result2) {
                    if (result2.status == 1) {
                        objE.result1 = result1;
                        objE.result2 = result2;
                        
                        console.log('--------result1------')
                        console.log(result1)
                        console.log('------result2------')
                        console.log(result2)
                        
                        //return false;
                        
                        var invoice_date = date.format(now, 'MM/DD/YYYY');
                        var invoice_number = result1.single_installment_payment[0].invoice_number;    
                        
                        var full_name = result2.plan[0].f_name+' '+result2.plan[0].m_name+' '+result2.plan[0].l_name;
                        var address = result2.plan[0].address1+' '+result2.plan[0].address2+' '+result2.plan[0].city;
                        var phone = result2.plan[0].peimary_phone;
                        
                        var ac_no = result2.plan[0].application_no;
                       
                        //var pddArr = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 0 ) : '';
                        var pymnt_due_date = result1.single_installment_payment[0].due_date;
                        
                        var amount_due = 0;
                        //var amount_due_length = result1.single_installment_payment.length
                        if(result1.single_installment_payment) {
                            
                            result1.single_installment_payment.forEach(function(data) {
                                if(data.installment_amt) {
                                    amount_due += data.installment_amt;
                                }
                            })
                            
                        }
                        /*for(var i=0;i<amount_due_length;i++) {
                            if(result1.single_installment_payment[i].installment_amt) {
                                amount_due += result1.single_installment_payment[i].installment_amt;
                            }
                        }
                        console.log(amount_due)*/
                        
                        //var amount_due = result2.plan[0].monthly_payment;

                        var late_fee = result1.payment_billing_late_fee[0].late_fee;
                        
                        if(result2.plan) {
                            var loan_plan_info = result2.plan.reduce(function (r, a) {
    
                                r[a.pp_id] = r[a.pp_id] || [];
                                
                                r[a.pp_id].push(a);
                                return r;
                                
                            }, []);
                        }
                        console.log('------loan_plan_info------')
                        console.log(loan_plan_info)
                        
                        return false;
                        
                        var loan_amount = result2.plan[0].credit_amount;

                        var payment_term = result2.plan[0].payment_term;

                        var interest_rate = result2.plan[0].interest_rate;

                        //====//
                        /*var getPaidBalnc = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 1 ) : '';
                        if(getPaidBalnc) {
                            var paidInsTotal = 0;
                            for (var i = 0; i<getPaidBalnc.length; i++) {
                                paidInsTotal += parseFloat(getPaidBalnc[i].payment_amount);
                            }
                        }
                        var current_balance = result2.plan[0].credit_amount-paidInsTotal;*/ 
                        //====//

                        //var fincharge = ((result1.single_installment_payment[0].installment_amt/payment_term)*interest_rate)/100;
                        var fincharge = 0;
                        var totalEnclosed_amt = 0;
                        if(result1.single_installment_payment) {

                            result1.single_installment_payment.forEach(function(data) {
                    
                                fincharge += ((data.installment_amt/data.payment_term)*data.interest_rate)/100;
                        
                                totalEnclosed_amt += parseFloat(data.installment_amt)
                    
                            })
                    
                        }
                        
                        var total_amt_due = parseFloat(amount_due)+parseFloat(fincharge)+parseFloat(late_fee);

                        //====Last Payment Info//
                        //var lastPymntInfo = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 1 ).pop() : '';
                        
                        var last_payment_date = result2.last_payment_info[0].payment_date;

                        var last_mnthly_pymnt = result2.last_payment_info[0].payment_amount;

                        var last_addi_amount = result2.last_payment_info[0].additional_amount;

                        var last_late_fee = result2.last_payment_info[0].late_fee_received;

                        var last_fin_ch = result2.last_payment_info[0].fin_charge_amt;

                        var last_total_amount = last_mnthly_pymnt+last_addi_amount+last_late_fee+last_fin_ch;

                        //return false;

                        var emialTemp = require('./email.js');
                        var locals = {
                            invoice_date: invoice_date,
                            invoice_number: invoice_number,
                            ac_no: ac_no,
                            pymnt_due_date: pymnt_due_date,
                            amount_due: parseFloat(amount_due).toFixed(2),
                            late_fee_after: parseFloat(late_fee).toFixed(2),
                            full_name: full_name,
                            address: address,
                            phone: phone,

                            loan_amount: '3002',
                            payment_term: '12',
                            interest_rate: '9',
                            monthly_payment: '245',
                            current_balance: '2567',

                            regu_mnthly_pymnt: parseFloat(amount_due).toFixed(2),
                            ex_late_fee: parseFloat(late_fee).toFixed(2),
                            ex_fin_charge: parseFloat(fincharge).toFixed(2),
                            total_amt_due: parseFloat(total_amt_due).toFixed(2),

                            last_reg_mnthly_pymnt: parseFloat(last_mnthly_pymnt).toFixed(2),
                            last_payment_date: last_payment_date,
                            last_addi_amount: parseFloat(last_addi_amount).toFixed(2),
                            last_late_fee: parseFloat(last_late_fee).toFixed(2),
                            last_fin_ch: parseFloat(last_fin_ch).toFixed(2),
                            last_total_amount: parseFloat(last_total_amount).toFixed(2)
                        };

                        //console.log(locals);

                        //return false;
                        var toEmailAddress = 'amandeep@euclidesolutions.co.in';
                        var template = 'customer-invoice';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);

                        var obj = {
                            "status": 1,
                            "message": "Email sent successfully"
                        }
                        obj.locals = locals
                        res.send(obj)
                    } else {
                        res.send("something wrong")
                    }

                });
                //res.send(objE);
            }
        });
        /*dataSqlC.getSinglePlans(con, req.query.app_id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });*/
    });
    /*
    * pay installment insert
    */
    app.post('/api/pay-installment-insert', jwtMW, (req, res) => {
        //console.log('req.query.planId')
        var dataSql = require('./payment/payment.sql.js');
        dataSql.payInstallmentInsert(con, req.body, function (result) {
            if(result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });

    /*
    * get single plan details on invoice admin
    */
    app.get('/api/application-plan-singleInvoice-payment-details', jwtMW, (req, res) => {
        // this function use for get credit application details
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getInvoicePlans(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

	/*
    * get single plan details in row
    */
    app.get('/api/application-plan-singleInstallment-payment-details', jwtMW, (req, res) => {
        console.log(req.query.planId)
        console.log(req.query.installmentId)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getSingleInstallment(con, req.query.appId, req.query.planId, req.query.installmentId, function (result) {
            if(result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });
    /*
    * get master fee option
    */
    app.get('/api/payment-master-fee-option', jwtMW, (req, res) => {
        // this function use for get credit application details
        //console.log("tested");
        //return false;
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getPaymentMasterFeeOption(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    /*
    * get waiver type option
    */
    app.get('/api/payment-late-fee-waiver-type-option', jwtMW, (req, res) => {
        console.log(req.query.id)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getLateFeeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    app.get('/api/payment-fin-charge-waiver-type-option', jwtMW, (req, res) => {
        console.log(req.query.id)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getFinChargeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });
}