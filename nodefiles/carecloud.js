module.exports = function (app, jwtMW) {
    var request = require('request');
    const BASE_URL = 'https://api.carecloud.com';
    const KEY_ID = 'dkJ3azhaV3l5VG9BX2NqS3QxSmpxTm1OZUZBUWpyREk6SkFnMGFkSm1aMmhRQVdoRg==';
    const KEY_SECRET = 'JAg0adJmZ2hQAWhF';

    const KEY_ID2 = 'dkJ3azhaV3l5VG9BX2NqS3QxSmpxTm1OZUZBUWpyREk6SkFnMGFkSm1aMmhRQVdoRg==';
    const KEY_SECRET2 = 'Zh1FTGpfFDczMlkx';
    const USERNAME = 'ramesh@cognizsoft.com';
    const PASSWORD = 'Cogniz@1234';
    app.post('/api/request-token/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.providerCareCloud(con, req.body.provider_id, function (result) {
            
            if (result.status == 1) {

                if (req.body.refreshToken == '') {
                    var KEY_IDs = Buffer.from(result.careCould.ap_key + ':' + result.careCould.secret_key).toString('base64');
                    var requestData = {
                        'grant_type': 'authorization_code',
                        'code': req.body.code,
                        'redirect_uri': req.body.url
                    }
                    request({
                        rejectUnauthorized: false,
                        method: 'POST',
                        url: 'https://api.carecloud.com/oauth2/access_token',
                        json: requestData,
                        headers: {
                            'Authorization': KEY_IDs,
                        },


                    }, function (error, response, body) {
                        
                        if (error) {
                            var obj = {
                                "status": 0,
                                "message": "Something wrong please try again."
                            }
                            res.send(obj);
                        } else if (response.statusCode === 200) {
                            body.expire_at = Math.floor(Date.now() / 1000) + body.expires_in;
                            var obj = {
                                "status": 1,
                                body: body
                            }
                            res.send(obj);
                        } else {
                            var obj = {
                                "status": 0,
                                "message": (body.error == 'invalid_grant') ? body.error_description : body.error
                            }
                            res.send(obj);
                        }
                    });
                } else {
                    var KEY_IDs = Buffer.from(result.careCould.ap_key + ':' + result.careCould.secret_key).toString('base64');


                    var requestData = {
                        'grant_type': 'refresh_token',
                        'refresh_token': 'YKsfOrE0-9XEINXrTL28AYnUyvye8l44'
                        //'code': req.body.code,
                        //'redirect_uri': req.body.url
                    }
                    request({
                        rejectUnauthorized: false,
                        method: 'POST',
                        url: 'https://api.carecloud.com/oauth2/access_token',
                        json: requestData,
                        headers: {
                            'Authorization': KEY_IDs,
                        },


                    }, function (error, response, body) {
                        
                        if (error) {
                            var obj = {
                                "status": 0,
                                "message": "Something wrong please try again."
                            }
                            res.send(obj);
                        } else if (response.statusCode === 200) {

                            body.expire_at = Math.floor(Date.now() / 1000) + body.expires_in;
                            var obj = {
                                "status": 1,
                                body: body
                            }
                            res.send(obj);
                        } else {
                            var obj = {
                                "status": 0,
                                "message": (body.error == 'invalid_grant') ? body.error_description : body.error
                            }
                            res.send(obj);
                        }
                    });
                }


            } else {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please check out care cloud setup."
                }
                res.send(obj);
            }
        })
        


    });

    app.post('/api/search-application/', jwtMW, (req, res) => {
        con = require('../db');

        con.query('SELECT value '
            + 'FROM master_data_values '
            + 'WHERE md_id = ? AND status = ? AND deleted_flag = ?',
            [
                'CareCould API Key',
                1,
                0
            ]
            , function (error, careCould) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT value '
                        + 'FROM master_data_values '
                        + 'WHERE md_id = ? AND status = ? AND deleted_flag = ?',
                        [
                            'CareCould SECRET Key',
                            1,
                            0
                        ]
                        , function (error, careCouldSECRET) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                                res.send(obj);
                            } else {
                                var KEY_IDs = Buffer.from(careCould[0].value + ':' + careCouldSECRET[0].value).toString('base64');
                                var requestData = {
                                    "patient": {
                                        "first_name": "jon",
                                        "last_name": "ick",
                                        "email": "jon@gamil.com",
                                        "prefix": "jonP",
                                        "suffix": "jonS",
                                        "ssn": "525847856",
                                        "gender_code": "M",
                                        "date_of_birth": "03/21/1999",
                                        "race_code": "1004-1",
                                        "marital_status_code": "D",
                                        "language_code": "eng",
                                        "chart_number": "656565656",
                                        "drivers_license_number": "56556566556",
                                        "drivers_license_state_code": "AK",
                                        "employment_status_code": "E",
                                        "school_name": "DAV",
                                        "employer_name": "CSS",
                                        "account_number": "56566566556565",
                                        "legacy_patient_id": "545454545",
                                        "employer_phone_number": "5685748568",
                                        "ethnicity_code": "H",
                                        "student_status_code": "F",
                                        "primary_care_physician_npi": 0,
                                        "referring_physician_npi": 0,
                                        "mother_maiden_name": "sdfs"
                                    },
                                    "addresses": [
                                        {
                                            "line1": "8864 Bowman",
                                            "line2": "Ave",
                                            "city": "ALASKA",
                                            "state": "ALASKA",
                                            "zip_code": "8568545",
                                            "country_name": "USA",
                                            "is_primary": true
                                        }
                                    ],
                                    "phones": [
                                        {
                                            "phone_number": "856958745",
                                            "phone_type_code": "B",
                                            "extension": "+1",
                                            "is_primary": true
                                        }
                                    ]
                                }
                                request({
                                    rejectUnauthorized: false,
                                    method: 'POST',
                                    url: 'https://api.carecloud.com/v2/patients',
                                    json: requestData,
                                    headers: {
                                        'Authorization': req.body.token_details.access_token,
                                    },


                                }, function (error, response, body) {
                                    
                                    if (error) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                    } else if (response.statusCode === 200) {
                                        var obj = {
                                            "status": 1,
                                            body: body
                                        }
                                        res.send(obj);
                                    } else {
                                        var obj = {
                                            "status": 0,
                                            "message": (body.error == 'invalid_grant') ? body.error_description : body.error
                                        }
                                        res.send(obj);
                                    }
                                });
                            }
                        })
                }
            })



    });

    let date = require('date-and-time');
    app.post('/api/provider-caredetails/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        if (req.body.id != '' && req.body.id !== undefined) {
            con.query('UPDATE mfs_details SET '
                + 'ap_key=AES_ENCRYPT(?, "ramesh_cogniz"), '
                + 'secret_key=AES_ENCRYPT(?, "ramesh_cogniz"), '
                + 'date_modified=?, '
                + 'modified_by=? '
                + 'WHERE id=?',
                [
                    req.body.api_key,
                    req.body.secret_key,
                    current_date,
                    current_user_id,
                    req.body.id,
                ]
                , function (err, result) {
                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Something wrong please try again."
                        }
                        res.send(obj);
                    } else {
                        con.query('SELECT id,AES_DECRYPT(ap_key, "ramesh_cogniz") as ap_key,AES_DECRYPT(secret_key, "ramesh_cogniz") as secret_key FROM mfs_details WHERE provider_id=?',
                            [
                                req.body.provider_id,
                            ]
                            , function (err, result) {
                                if (err) {
                                    var obj = {
                                        "status": 0,
                                        "message": "Something wrong please try again."
                                    }
                                    res.send(obj);
                                } else {
                                    if (result.length > 0) {
                                        result[0].ap_key = (result.length > 0) ? result[0].ap_key.toString() : '';
                                        result[0].secret_key = (result.length > 0) ? result[0].secret_key.toString() : '';
                                        result = result[0];
                                    } else {
                                        result = ''
                                    }
                                    var obj = {
                                        "status": 1,
                                        "message": "Your information has been submitted successfully.",
                                        "result": result
                                    }
                                    res.send(obj);
                                }
                            })

                    }
                })
        } else {
            con.query('INSERT INTO mfs_details (provider_id, md_id, ap_key, secret_key, date_created, created_by, modified_by) VALUES(?,?,AES_ENCRYPT(?, "ramesh_cogniz"),AES_ENCRYPT(?, "ramesh_cogniz"),?,?,?)',
                [
                    req.body.provider_id,
                    'CareCloud',
                    req.body.api_key,
                    req.body.secret_key,
                    current_date,
                    current_user_id,
                    current_user_id
                ]
                , function (err, result) {

                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Something wrong please try again."
                        }
                        res.send(obj);
                    } else {
                        con.query('SELECT id,AES_DECRYPT(ap_key, "ramesh_cogniz") as ap_key,AES_DECRYPT(secret_key, "ramesh_cogniz") as secret_key FROM mfs_details WHERE provider_id=?',
                            [
                                req.body.provider_id,
                            ]
                            , function (err, result) {

                                if (err) {
                                    var obj = {
                                        "status": 0,
                                        "message": "Something wrong please try again."
                                    }
                                    res.send(obj);
                                } else {
                                    if (result.length > 0) {
                                        result[0].ap_key = (result.length > 0) ? result[0].ap_key.toString() : '';
                                        result[0].secret_key = (result.length > 0) ? result[0].secret_key.toString() : '';
                                        result = result[0];
                                    } else {
                                        result = ''
                                    }
                                    var obj = {
                                        "status": 1,
                                        "message": "Your information has been submitted successfully.",
                                        "result": result
                                    }
                                    res.send(obj);
                                }
                            })

                    }
                })
        }
    })

    app.get('/api/provider-caredetails-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT id,AES_DECRYPT(ap_key, "ramesh_cogniz") as ap_key,AES_DECRYPT(secret_key, "ramesh_cogniz") as secret_key FROM mfs_details WHERE provider_id=?',
            [
                req.query.id,
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    if (result.length > 0) {
                        result[0].ap_key = (result.length > 0) ? result[0].ap_key.toString() : '';
                        result[0].secret_key = (result.length > 0) ? result[0].secret_key.toString() : '';
                        result = result[0];
                    } else {
                        result = ''
                    }

                    var obj = {
                        "status": 1,
                        "result": result
                    }
                    res.send(obj);

                }
            })
    })

    app.get('/api/mfs-cdetails/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.providerCareCloud(con, req.query.id, function (result) {
            if (result.status == 1) {
                var obj = {
                    "status": 1,
                    "careCould": result.careCould.ap_key
                }
                res.send(obj);
            } else {
                var obj = {
                    "status": 1,
                    "result": "CareCloud not setup yet."
                }
                res.send(obj);
            }
        });
    })
}