/*
* Title: Custoemr Dashboard Active Plan
* Descrpation :- This module blong to customer dashboard active plan 
* Date :- 24 May 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    var multer = require('multer');
    /*const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            const dir = './uploads/customers/' + req.body.customer_id + '/' + req.body.application_id
            var mkdirp = require('mkdirp');
            mkdirp(dir, function (err) {
                if (err) console.error(err)
                else cb(null, dir);
            });

        },
        filename: function (req, file, cb) {
            cb(null, Date.now() + file.originalname);
        }
    });

    const fileFilter = (req, file, cb) => {
        //if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
        //} else {
        // rejects storing a file
        //cb(null, false);
        //}
    }

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * 5
        },
        fileFilter: fileFilter
    });
    //var upload = multer({ dest: 'uploads/' })
    var dataupload = upload.single('imgedata');
*/




    app.post('/api/customer-patient-verify-amount', jwtMW, (req, res) => {
        con = require('../db');
        //console.log(req.body)
        //console.log(Date.now())
        //return false;

        var obj = {};
        con.beginTransaction(function (err) {
            if (err) {
                obj.verify_status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            con.query('UPDATE patient SET '
                + 'bank_verify_flag = ? '

                + 'WHERE '
                + 'patient_id=?',
                [
                    1,
                    req.body.patient_id
                ]
                , function (err, verifyAmt) {
                    //console.log(this.sql)
                    console.log(err)
                    if (err) {
                        con.rollback(function () {
                            obj.verify_status = 0;
                            obj.message = "Something wrong please try again.";
                            res.send(obj);
                        });
                    } else {
                        con.query('SELECT '
                            + 'patient.status,patient.bank_verify_amt,patient.bank_verify_amt_duration,patient.bank_verify_flag,patient.profile_flag,patient.patient_ac,patient.f_name,patient.m_name,patient.l_name,patient.other_f_name,patient.other_m_name,patient.other_l_name,AES_DECRYPT(patient.dob, "ramesh_cogniz") as dob,AES_DECRYPT(patient.ssn, "ramesh_cogniz") as ssn,patient.gender,patient.malling_address,patient.peimary_phone,patient.alternative_phone,patient.email,patient.employment_status,patient.annual_income,patient.employer_name,DATE_FORMAT(patient.employer_since, "%m/%d/%Y") as employer_since,patient.employer_phone,patient.employer_email,patient.bank_name,patient.account_number,patient.bank_address,patient.rounting_no,patient.account_name,patient.withdrawal_date,master_data_values.value as account_type, empType.value as emp_type_name,user.username as username,user.email_id as user_eamil,user.phone as user_phone1,patient.alternative_phone as user_phone2 '

                            + 'FROM patient '
                            + 'INNER JOIN user ON user.patient_id=patient.patient_id '
                            + 'LEFT JOIN master_data_values ON master_data_values.mdv_id=patient.account_type '
                            + 'LEFT JOIN master_data_values AS empType ON empType.mdv_id=patient.employment_type '
                            + 'WHERE '
                            + 'patient.patient_id=?',
                            [
                                req.body.patient_id
                            ]
                            , function (err, planDetails) {
                                console.log(planDetails)
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Customer not found.";
                                        res.send(obj);
                                    });
                                } else {
                                    const aes256 = require('aes256');
                                    var ase256key = 'ramesh';
                                    //var encrypted = aes256.encrypt(ase256key, plaintext);
                                    //planDetails[0].ssn = aes256.decrypt(ase256key, planDetails[0].ssn);
                                    //planDetails[0].dob = aes256.decrypt(ase256key, planDetails[0].dob);
                                    planDetails[0].ssn = (planDetails.length > 0) ? planDetails[0].ssn.toString() : '';
                                    //let date = require('date-and-time');
                                    //let now = new Date(planDetails[0].dob);
                                    //planDetails[0].dob = date.format(now, 'DD/MM/YYYY');
                                    let moment = require('moment');
                                    planDetails[0].dob = moment(planDetails[0].dob.toString()).format('MM/DD/YYYY');
                                    obj.customerDetails = (planDetails) ? planDetails[0] : '';

                                    con.commit(function (err) {
                                        if (err) {
                                            con.rollback(function () {
                                                obj.verify_status = 0;
                                                obj.message = "Something wrong please try again.";
                                            });
                                            res.send(obj);
                                        }
                                        obj.verify_status = 1;
                                        obj.patient_id = req.body.patient_id;
                                        obj.message = "Bank Account Verified Successfully!";
                                        obj.verifyAmt = verifyAmt;
                                        res.send(obj);
                                    });
                                }
                            })
                    }
                });



        });

    });


    // customer week month invoice report
    app.get('/api/customer-dashboard-weekmonth-list/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.customerInvoiceReports(con, function (result) {
            res.send(result);
        });
    });

    // customr weekly monthly filter option
    app.post('/api/customer-dashboard-weekmonth-filter/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
            res.send(result);
        });
    });

    // download pdf reports for custoemr weekly & monthly invoice
    app.post('/api/customer-dashboard-weeklt-monthly-invoice-pdf/', jwtMW, (req, res) => {
        con = require('../db');
        const pdf = require('html-pdf');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
            if (result.status == 1) {
                const pdfTemplate = require('./reports/customerDashboardInvoicePrintDocuments');

                const dir = './uploads/report/customer/'
                //var fullUrl = req.protocol + '://' + req.get('host');
                //var logoimg = fullUrl + '/uploads/logo.png';
                var fullUrl = req.get('origin');
                var logoimg = fullUrl + '/logo.png';
                var filename = dir + 'customer-invoice.pdf';
                pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }
                })
            }
        });
    });

    app.post('/api/customer-dashboard-weeklt-monthly-invoice-xls/', jwtMW, (req, res) => {
        con = require('../db');
        const excel = require('exceljs');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.customerInvoiceReportsFilter(con, req.body, function (result) {
            if (result.status == 1) {
                const jsonData = result.result;

                let workbook = new excel.Workbook(); //creating workbook
                let worksheet = workbook.addWorksheet('Customer Invoices'); //creating worksheet
                //  WorkSheet Header
                worksheet.columns = [
                    { header: 'Invoice ID', key: 'Invoice ID', width: 10 },
                    { header: 'Customer Name', key: 'Customer Name', width: 30 },
                    { header: 'Address', key: 'Address', width: 30 },
                    { header: 'City', key: 'City', width: 30 },
                    { header: 'State', key: 'State', width: 20, numFmt: '#.##' },
                    { header: 'Zip', key: 'Zip', width: 20, },
                    { header: 'Phone', key: 'Phone', width: 20 },
                    { header: 'Invoice date', key: 'Invoice date', width: 15 },
                    { header: 'Invoice due date', key: 'Invoice due date', width: 15 },
                    { header: 'Invoice Amt', key: 'Invoice amt', width: 15 },
                    { header: 'Addtl Amt', key: 'Invoice amt', width: 15 },
                    { header: 'Total Paid', key: 'Total Paid', width: 15 },
                    { header: 'Invoice status', key: 'Invoice status', width: 20, },
                ];

                // Add Array Rows
                //worksheet.addRows(jsonData);
                var rowValues = [];
                var count = 2;
                jsonData.forEach(function (element, idx) {
                    element.payment_date = (element.payment_date) ? element.payment_date : '-';



                    element.payment_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.payment_amount)).toFixed(2)
                    element.payment_amount = parseFloat(element.payment_amount);

                    element.paid_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.paid_amount)).toFixed(2)
                    element.paid_amount = parseFloat(element.paid_amount);


                    worksheet.getCell('A' + count).value = element.invoice_number;
                    worksheet.getCell('B' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
                    worksheet.getCell('C' + count).value = element.address1;
                    worksheet.getCell('D' + count).value = element.City;
                    worksheet.getCell('E' + count).value = element.state_name;
                    worksheet.getCell('F' + count).value = element.zip_code;
                    worksheet.getCell('G' + count).value = element.peimary_phone;
                    worksheet.getCell('H' + count).value = element.payment_date;
                    worksheet.getCell('I' + count).value = element.due_date;
                    worksheet.getCell('J' + count).value = element.payment_amount;
                    worksheet.getCell('K' + count).value = element.additional_amount;
                    worksheet.getCell('L' + count).value = element.additional_amount + element.paid_amount;
                    worksheet.getCell('M' + count).value = element.invoice_status_name;

                    worksheet.getCell('J' + count).numFmt = '0.00';
                    worksheet.getCell('K' + count).numFmt = '0.00';
                    worksheet.getCell('L' + count).numFmt = '0.00';

                    count++;
                });
                // set header details
                ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1'].map(key => {
                    worksheet.getCell(key).fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'aba2a2' },
                        bgColor: { argb: 'aba2a2' }
                    };
                });
                // merge A and B column
                worksheet.mergeCells('A' + count + ':D' + count);
                worksheet.getCell('A' + count).value = 'Total';
                // total for actual amount
                const sumRange = `${'J2'}:${'J' + (count - 1)}`;
                worksheet.getCell('J' + count).numFmt = '0.00';
                worksheet.getCell('J' + count).value = { formula: `SUM(${sumRange})` };
                // total for additional amount
                const sumRangeAdd = `${'K2'}:${'K' + (count - 1)}`;
                worksheet.getCell('K' + count).numFmt = '0.00';
                worksheet.getCell('K' + count).value = { formula: `SUM(${sumRangeAdd})` };
                // total for total paid amount
                const sumRangeAddTot = `${'L2'}:${'L' + (count - 1)}`;
                worksheet.getCell('L' + count).numFmt = '0.00';
                worksheet.getCell('L' + count).value = { formula: `SUM(${sumRangeAddTot})` };


                // Write to File
                const dir = './uploads/report/'

                var filename = dir + 'customer-invoice.xlsx';
                workbook.xlsx.writeFile(filename)
                    .then(function () {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }).catch(function () {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    });
            }
        });
    });

    /*
    /*update customer profile
    */
    app.post('/api/customer-profile-update/', jwtMW, (req, res) => {
        con = require('../db');
        // call sql function and callback for next process.
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.updateCustomerProfile(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });

    });

    /*
    * customer edit profile edit
    */
    app.get('/api/edit-customer-profile-details/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.editCustomerProfileDetails(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);

            }
        });
    });

	/*
	* get single plan details
	*/
    app.get('/api/customer-active-plan-details', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.getCustomerActivePlansDetails(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    const pdf = require('html-pdf');
    app.post('/api/customer-creditapplication-all-plan-details-pdf', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.getCustomerActivePlansDetails(con, req.body.current_user_id, function (result) {
            if (result.status == 1) {

                var pdfTemplate;
                var dir;
                var fullUrl;
                var logoimg;
                var filename;

                if (req.body.current_user_id && req.body.planid) {
                    pdfTemplate = require('./single-active-app-plan-details-pdf');

                    dir = './uploads/customer/' + req.body.current_user_id + '/' + req.body.planid + '/' + Math.random()
                    fullUrl = req.get('origin');
                    logoimg = fullUrl + '/logo.png';
                    filename = dir + 'single-app-plan-details.pdf';
                } else {
                    pdfTemplate = require('./active-app-plan-details-pdf');

                    dir = './uploads/customer/' + req.body.current_user_id + '/' + Math.random()
                    fullUrl = req.get('origin');
                    logoimg = fullUrl + '/logo.png';
                    filename = dir + 'app-plan-details.pdf';
                }

                /*console.log('result>?>?>')
                console.log(req.body)
                return false;*/
                pdf.create(pdfTemplate(result, req.body.planid, logoimg), {}).toFile(filename, (err) => {

                    if (err) {

                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "type": (req.body.current_user_id && req.body.planid) ? 1 : 2,
                            "result": filename
                        }
                        res.send(obj);

                    }
                })
            }
        });
    })

    //////////////////

    app.post('/api/pay-customer-installment-insert', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.payCustomerInstallmentInsert(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });

}