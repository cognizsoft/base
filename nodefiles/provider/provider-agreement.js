module.exports = (data,logoimg) => {
   let moment = require('moment');
   
   return `
  <!doctype html>
  <html>
     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <title>Customer Payment Plan</title>
        <style>
          html{zoom: 0.7;}
          body {
            font-size: 16px;
          }
           .invoice-box {
           max-width: 1200px;
           margin: auto;
           padding: 0 30px;
           font-size: 12px;
           line-height: 14px;
           font-family: 'Arial', sans-serif;
           color: #303030;
           }
           
           .justify-center {
           text-align: center;
           width: 100%;
           }
           .term-heading, .customer-heading {text-align: center; margin-top: 30px; font-size:15px}
          .hidden{visibility:hidden;}
          .customer-accnt .add-card{
            width: 32%;
            border: solid 1px #d7eeff !important;
            background: #f1f9ff;
            
          }
          .customer-accnt .witness-signature{
            float:right;
          }
          .customer-accnt .customer-signature{
            float:left;
          }
          .customer-accnt .image-area{
            margin-top: 10px;
          }
          .sing_box{
            padding: 10px 10px 10px 10px !important;         
          }
        </style>
     </head>
     <body>
       <div class="invoice-box">
         <h1 class="justify-center"><img src="${logoimg}" alt="${logoimg}" height="50"/></h1>
         <h1 class="term-heading">HEALTH PARTNER ACCEPTANCE AGREEMENT FOR PARTICIPATING PROFESSIONALS</h1>
         <div class="terms-condition-agreement">
            <p>Health Partner Inc (HPS) located at 5720 Creedmoor Rd, Suite 103, Raleigh, NC 27612 has established a private credit program for Customers/patients of health care professionals (the “Program”). Under the Program, Customers/patients may finance the purchase of goods and services provided by health care professionals who have applied to HPS and been approved for participation in the Program. For purposes of this Agreement, each such approved health care professional and any professional services corporation or other entity that submitted an application and was approved by HPS to participate in the Program, are collectively referred to herein as “Professional”. Under the Program, Professional will process credit applications by which Customers/patients apply toHPS to establish private label credit accounts (“Accounts”) and will accept private label credit issued under the Program (“Program Credit”), all in accordance with the terms set forth below, including mandatory arbitration of disputes between us, instead of class actions or jury trials.</p>
            <p>This Agreement is effective upon HPS’s approval of Professional’s application to participate in the Program.</p>
            <ol>
               <li><strong>HPS’s Obligations. HPS’s obligations include the following:</strong>

                  <ul>
                     <li>Establish and administer the Program in accordance with all applicable laws and the terms and conditions of this Agreement;</li>
                     <li>Provide a point-of-sale process for Professional to use to enter client/patient applications and Program Credit transactions for authorization and processing;</li>
                     <li>Provide to Professional an <u>“Operating Guide”</u> which shall set forth instructions on how to submit and process transactions for Program Credit and any separate instructions applicable solely to Private Label Credit, as well as other relevant Program information;</li>
                     <li>Provide to Professional the approved forms of Account credit disclosures (credit applications, terms, privacy policies) and updates as they are published; and</li>
                     <li>Contact Professional in the event of any dispute requiring support from Professional to resolve, which is made by an individual who has presented a Program Credit for the payment of goods or services.</li>
                  </ul>

               </li>

               <li><strong>Professional’s Obligations. Professional’s obligations include the following:</strong>

                  <ul>
                     <li>Display point-of-sale signage relating to the Private Label Credit issued under the Program which is distributed or approved by HPS;</li>
                     <li>Accept and process credit applications for Program Credit and credit transactions from customer/patients only for personal, family or household purposes and in accordance with this Agreement,theOperating Guideandwritteninstructions from HPS (e.g., ensure that requested fields are completely filled out, verify identification, provide required terms and disclosures etc.), without discrimination of any kind and do not process credit applications for Program Credit from Customers/patients who are under duress (Customers/patients who are under general sedation, in extreme discomfort or heavily medicated are deemed to be under duress);</li>
                     <li>Honor without discrimination valid Program Credit as a method of payment for purchases and process transactions in accordance with the terms of this Agreement, the Operating Guide, and such other procedures as HPS may from time to time prescribe for the authorization, processing, and settling of Program Credit transactions; Professional acknowledges that it received a copy of the Operating Guide;</li>
                     <li>Process only bona fide charges and credits for the sale of goods or customer/patient-directed care (which does not include non-vet emergency room treatment or chronic care) provided by Professional in the ordinary course (and do not process cash advances); ensure that each sale involving a Program Credit is evidenced by a single complete record with the sale date and the sale amount, and other information as required by HPS; transmit such transactions to HPS in the required format, as set forth in the Operating Guide; and ensure that the corresponding information about charges and credits to Accounts (collectively, <u>“Charge Transaction Data”</u>) is not submitted on behalf of a third party and has not been altered in any manner not authorized by the Credit account holder;</li>
                     <li>Ensure that all information abouttheProgram (otherthanHPS’sprintedterms),andallProgram advertisingconductedbyProfessional, provided or directed to prospective applicants, customer/patients and Credit account holders is complete, accurate and legally compliant, and refer prospective applicants and customer/patients to the printed Program terms for detailed information;</li>
                     <li>Deliver all goods and/or services covered by any charge processed under Section 3 hereof prior to the time the charge is processed; (g) Respond within ten (10) days, or such shorter time as required by this Agreement, to any inquiry from HPS, and fully cooperate with,<br />HPS in the resolution of disputes concerning sales charged to a Program Credit;</li>
                     <li>Obtain an authorization code from HPS on all transactions prior to submission, and call HPS’s voice authorization facility prior to completion of a transaction in any case involving suspicious or unusual circumstances, including those in which the signature on the sales slip does not match the signature on the driver’s license or any other form of identification.</li>
                     <li>Pay all applicable fees (collectively <u>“Professional Fees”</u>) set forth on HPS’s pricing schedule governing the Private Label Credit and issued under the Program;</li>
                     <li>Comply in all respects at all times with applicable laws, the terms of this Agreement, the Operating Guide (as such Operating Guide may be modified or updated from time to time by HPS) and other bulletins provided to Professional from time to time; and</li>
                     <li>With respect to documents and forms provided to, or to be executed by, a Credit holder or an applicant for a Program Credit or which constitute a disclosure required by HPS and/or applicable law in connection with the Program, only use such documents and forms that were provided to Professional, or approved in writing by HPS (and only use the latest version of such documents and forms), and do not modify any such approved documents or forms without HPS’s prior written consent.</li>
                     <li>Professional shall not process a charge for services not yet rendered, or goods not yet delivered, unless Professional provides reasonable disclosure to Credit holder, and either (i) the charges are for orthodontic services or custom products ordered by a Credit holder or (ii) the services are (1) intended to be and are completed, or (2) for out-of-pocket costs incurred by the Professional in anticipation of providing services (such as costs for labs, sedation, equipment, operating room/surgical center booking, anesthesia, etc.), within thirty (30) days of the applicable charge. HPS shall have the right to chargeback such advance charges if (a) reasonable disclosure is not given to Credit holder and the Credit holder elects not to complete the service, or (b) the anticipated services are not actually provided or the costs are not actually incurred within 30 days of the processing of such charges.</li>
                     <li>Train its employees, and ensure that any Service Provider or Third Party Vendor trains its employees, to operate the Program in accordance with applicable law, the Operating Guide, this Agreement, and any materials provided by HPS, as required by HPS, and not to create or use any alternate or supplemental training materials for the Program without the consent of HPS.</li>
                     <li>Comply with the “Transparency Principles” set forth on Exhibit A hereto.</li>
                     <li>Ensure that its employees do not submit unauthorized or fictitious applications and to notify HPS of, and cooperate with HPS in investigating, any suspected fraudulent conduct by an employee with respect to the Program.</li>
                  </ul>

               </li>

               <li><strong>Settlement Process/Payment for Charges.</strong>

                  <ul>
                     <li>Professional agrees to transmit to HPS, generally each day, but in no event later than two business days after the transaction date, complete and accurate Charge Transaction Data occurring since the immediately previous transmission, as provided in the Operating Guide. Upon receipt of the Charge Transaction Data and any required documents, and provided Professional is not in default under this Agreement, HPS will deposit to a HPS account designated by Professional the total amount of all charges reflected in such Charge Transaction Data, less the total of (i) any credits reflected in such Charge Transaction Data, (ii) any amounts being charged back to Professional, (iii) any Professional Fees, which HPS may choose to deduct on a daily or monthly basis, in its discretion (and/or corrections to any such fees based on erroneous information submitted by Professional), unless HPS elects to separately bill Professional for such fees, and (iv) at HPS’s option, any other amounts (including any fees) which may be owed to HPS or to any of HPS’s affiliates, by Professional, or by any of Professional’s affiliates, or under any other agreement or merchant number between HPS and Professional. If at any time, the amount HPS owes Professional is less than the amount Professional owes HPS, (without regard to any Reserve Account established pursuant to Section 3(d) hereof), Professional agrees to pay HPS the net difference.</li>
                     <li>Pricing relating to Professional Fees (as well as any participation percentages available to Professionals) under this Agreement will be provided by HPS on pricing schedules, which HPS may distribute from time to time. If, within the timeframe set forth within the Operating Guide or any pricing schedule, any Account to which a participation percentage was paid to Professional is: (i) paid in full, regardless of the funding source used to pay the Account in full, or (ii) in default (as evidenced by a copy of HPS’s “Notice of Default”), or (iii) the Account is charged back to Professional, then any and all of such participation funds paid by HPS shall be returned promptly to HPS following HPS’s request. HPS may modify the pricing schedule applicable to Private Label Account and/or General Purpose Account credit promotions under the Program in its discretion by notifying Professional (either through the online system used by Professional for the Program or otherwise in writing, which may be electronic). Such new prices will be applicable to Professional as of the effective date established by HPS.</li>
                     <li>Professional hereby authorizes HPS to initiate ACH credits and debits to Professional’s designated HPS account for purposes of settling transactions here under and making necessary adjustments and initiating payments due to HPS from Professional hereunder. Professional is solely liable for all fees and costs associated with such ACH Account and for all overdrafts. HPS will not be liable for any delays in receipt of funds or errors in ACH Account entries caused by third parties, including Professional’s HPS.</li>
                     <li>If HPS determines in its sole discretionthatProfessional’s financialconditionhasdeteriorated, if Professional breaches this Agreement, or if HPS experiences an unusual volume or nature of disputes and/or chargebacks, returns or credits relating to charges submitted by Professional (based on HPS’s experience with Professional and/or other professionals or providers in the same or similar practice areas), then HPS may withhold from the settlement payments otherwise due Professional an amount HPS deems necessary to fund a non-interest bearing reserve account (the <u>“Reserve Account”</u>). HPS shall be the sole ownerof the Reserve Account (if any), and may(but need not)debit the Reserve Account from time to time to satisfy any amounts owed by Professional to HPS. HPS will return to Professional any amounts remaining in the Reserve Account no later than one year after termination of Professional’s participation in the Program (the <u>“Final Liquidation Date”</u>).</li>
                     <li>HPS reserves the right to refuse to process any Charge Transaction Data presented by Professional (i) unless a proper authorization/approval code is recorded, (ii) if HPS determines that the Charge Transaction Data is or will become uncollectible from the Credit holder to which the transaction would otherwise be charged, or (iii) if HPS determines that the Charge Transaction Data was prepared in violation of any provision of this Agreement or the Operating Guide.</li>
                     <li>Professional will not (i) process any charge for more than the sale price of the goods or services (provided that Professional shall include in the transaction amount any taxes imposed by law (such taxes shall not be separately collected)), (ii) impose any surcharge on transactions made using a Program Credit, (iii) require the Credit holder to pay any part of any charge assessed by HPS to Professional, whether through any increase in price or otherwise, or to pay any contemporaneous finance charge in connection with the transaction charged to a Program Credit, or (iv) set a dollar amount above or below which Professional refuses to honor otherwise valid Program Credit, (v) accept payments for charges made previously at Professional and settled, or (vi) process any transaction that represents collection of a dishonored check.</li>
                     <li>Professional will not accept any payments from a Credit holder for charges billed on an Account and will instead refer the Credit holder to HPS’s payment address. If for any reason, Professional inadvertently accepts a Credit holder payment, Professional will hold such payment in trust for HPS and will immediately forward such payment to HPS for processing. Additionally, Professional hereby grants HPS a limited power of attorney to cash and retain for its own account any Credit holder payments on Accounts which are erroneously made out to Professional.</li>
                     <li>Professional acknowledges and agrees that all purchases made from Professional by Credit holders using their accounts are to be submitted to HPS for settlement. If, however, any such purchase is submitted to Professional’s HPS credit processor, HPS shall have the right to apply the Professional Fee applicable to a particular credit promotion then available under the Program. In such case, HPS shall also provide the Credit holder with the applicable credit promotion.</li>
                  </ul>

               </li>

               <li><strong>Credit Applications.</strong> Professional will follow all procedures provided to it by HPS in taking and submitting to HPS credit applications for Program Credit, will ensure that all credit applications are presented in a form approved by HPS and signed in person by the applicant, and will provide to each applicant at the time the credit application is submitted a complete and current copy of the applicable terms and conditions and privacy policy that applies to the Account. Provider will submit all completed credit applications toHPS for processing. Professional is prohibited from making any procedural orsystem edits that would prevent acredit application from reachingHPS. HPS may, in its sole discretion, approve or decline any application submitted. HPS may also decline to pay or credit settlement proceeds to Professional as would otherwise be required under Section 3 above if HPS determines that (i) Professional has falsified the application in any respect; (ii) Professional knows or reasonably should have known that the application contains false information; (iii) any information on the physical application does not match the information transmitted to HPS; (iv) the identification or verification requirements have not been satisfied; or (v) any other required procedures have not been met (an application meeting the description set forth in any of (i) through (v) above or that otherwise does not meet all of the requirements of this Section or the Operating Guide will be considered a “Defective Application”). If proceeds of any transactions have been credited to Professional’saccountpriortoHPS’s discovery of anyofthedefectssetforthabove,HPS maychargebacktheamountofanyorall transactions charged on the Account.
                  </li>

               <li><strong>Professional Applications and Contemporaneous Transactions for Dental and Audiology Services</strong>

                  <ul>
                     <li>Except as provided in Section 5(b), a Professional submitting Charge Transaction Data related to Dental or Audiology services (<u>“Dental or Audiology Professional”</u>) is prohibited from submitting Charge Transaction Data on a Credit holder’s Account within the three (3) days after such Dental or Audiology Professional submits a credit application to HPS on behalf of such Credit holder.</li>
                     <li>Exceptions. Section 5(a) is not applicable to any of the following transactions:

                           <ul>
                           <li>Professional submits Charge Transaction Data for transactions other than for Dental or Audiology services;</li>
                           <li>Professional submits Charge Transaction Data for aggregate transactions totaling less than One Thousand ($1,000.00) dollars made within three (3) days of such Credit holder’s application date;</li>
                           <li>Professional submits Charge Transaction Data for applications made directly by a Credit holder to HPS;</li>
                           <li>Professional submits Charge Transaction Data for applications submitted by a non-Dental or Audiology Professional. (c) HPS may charge back the amount of any or all transactions that fail to comply with this Section 5.</li>
                        </ul>

                     </li>
                  </ul>

               </li>

               <li><strong>Chargeback Rights</strong>

                  <ul>
                     <li><strong>HPS’s Right to Chargeback.</strong> HPS will bear all credit losses associated with purchases financed on Accounts. However, HPS may charge back to Professional any transaction on a Program Credit when one or more of the following occurs:

                           <ul>
                           <li>The Credit holder disputes the charge, if HPS has given Professional an opportunity to respond and HPS determines that the Credit holder’s dispute is valid.</li>
                           <li>The Credit holder refuses to pay, based on an assertion of a dispute about the quality of the merchandise or services purchased from, or any act or omission by Professional, including any alleged breach of warranty provided by or through Professional.</li>
                           <li>The charge(s) are incurred on an Account opened upon submission of a Defective Application.</li>
                           <li>The charge does not fully comply with any of (x) this Agreement (or any representations, warranties and covenants set forth herein), (y) the Operating Guide, or (z) applicable law.</li>
                           <li>The charge is disputed, and Professional cannot supply a copy of the underlying signed sales receipt, the signed Program Credit application, or other documents required in accordance with this Agreement, the Operating Guide or HPS within seven (7) days of HPS’s request.</li>
                           <li>HPS determines that (x) any charge does not represent a bona fide sale (including without limitation fraud arising from fraudulent activities of Professional’s employees) by Professional, or involved acts of fraud by any party, or (y) Professional did not obtain an authorization/approval code as provided for in Section 2(h).</li>
                           <li>The goods or services purchased have not been delivered, provided or shipped.</li>
                           <li>The Credit holder alleges that the Professional provided false or misleading information (e.g., incorrect information about credit promotions).</li>
                           <li>Any credit is submitted where there is no corresponding charge transaction.</li>
                           <li>Any disputed or fraudulent charge or credit relates to a transaction where the Credit holder was not physically present at Professional’s location (e.g., by telephone or via Internet).</li>
                           <li>Any disputed or fraudulent charge or credit relates to a transaction where the Credit holder did not physically present the credit (or other approved manifestation of an Account provided by HPS) or Professional failed to obtain a physical imprint or electronic record of the Program Credit (Professional acknowledges that, under these circumstances, the fact that an authorization/ approval code was obtained does not mean that a particular Program Credit transaction is in fact a valid or undisputed transaction entered into by the actual Credit holder or an authorized user of the Program Credit).</li>
                           <li>The transaction was submitted to HPS more than thirty (30) days after it occurred or after Professional is no longer an authorized participant in the Health Partner provider network.</li>
                           <li>The transaction (x) relates to a credit application that was processed while the Credit holder was under duress, or (y) was for treatment/procedures involving non-vet emergency room or chronic care.</li>
                        </ul>

                     </li>
                     <li>
                        <strong>Excessive Chargebacks.</strong> If HPS determines, in its sole discretion, that Professional is receiving an excessive amount of chargebacks or attempted chargebacks, in addition to HPS’s other remedies under this Agreement , HPS may take either or both of the following actions: (1) review Professional’s internal procedures relating to acceptance of Program Credit and notify Professional of new procedures Professional should adopt in order to avoid future chargebacks; and/or (2) notify Professional of the imposition of a charge (or a new rate with respect to such a charge for processing chargebacks).
                        </li>
                     <li>
                        <strong>Claims of Credit holders.</strong> Professional has full liability for the amount of any Charge Transaction Data for which Professional’s ACH Accounthasbeenprovisionallycreditedandwhichisthereafterthesubjectofachargeback.Professionalmaynot resubmit transactions financed on Private Label Accounts under any circumstances.
                        </li>
                  </ul>

               </li>

               <li><strong>INSTALLMENT LOANS.</strong> HPS and Professional agree to offer installment loans as a financing product under the Program (the <strong>“Installment Product”</strong>). The provisions of the Agreement will apply to the Installment Product to the greatest extent possible.

                     <ul>
                     <li>General References, Rights and Responsibilities
                           <ul>
                           <li>References to “Credit holders” in the Agreement will be deemed to include individuals who have applied for and been approved for an Installment Account.</li>
                           <li>References to “Accounts” in the Agreement will be deemed to include Installment Accounts. <strong>“Installment Account”</strong> means the legal relationship established by and between an Accountholder and HPS pursuant to a Credit Agreement, together with all Indebtedness owing thereunder from time to time and any current or future guaranties, security or other credit support therefor. <strong>“Credit Agreement”</strong> means the closed end credit agreement between HPS and each Accountholder pursuant to which such Accountholder may make a purchase financed on installment credit provided by HPS together with any modifications or amendments which may be made to such agreement, as well as any related notes.</li>
                           <li>References to “Charge Transaction Data” will include net loan amounts (e.g., less any consumer-paid HPS fees) under Credit Agreements.</li>
                           <li>The rights and responsibilities of HPS and Professional under the Program will apply to installment loans, except in any circumstance in which a provision purports to deal solely with issues relating to revolving credit and, in such case, if there is an analogous or comparable right or responsibility with respect to installment credit, then such provision will be deemed revised to the extent necessary to apply to such corresponding right or responsibility (e.g., HPS’s right to establish credit lines and authorize purchases under the Program will be deemed revised so as to refer to HPS’s right to assign loan amounts and establish credit terms under the Installment Product). If there is no such analogous or comparable right or responsibility, then such provision will apply solely to revolving credit extended under the Program.</li>
                           <li>Professional and HPS will work together in good faith to integrate the offering of the Installment Product with Professional’s sales processes, including allowing applications for the Installment Product to be processed through Professional’s website and use of the Installment Product for online purchases. “Internet Applications” will include applications for an Installment Account.</li>
                        </ul>
                     </li>
                     <li>The provisions of the Agreement notwithstanding, the pricing and other terms and conditions applicable to credit-based promotions on Program Credit will not apply to Installment Accounts. The pricing applicable to the Installment Product will be set forth on HPS’s pricing scheduling governing the Installment Product, which HPS may modify in its discretion with notice to Professional.</li>
                     <li>HPS will establish the standard Account terms and credit criteria to be applied with respect to Installment Accounts and may modify such terms and criteria from time to time, including (i) the interest rates/APR applicable to Installment Accounts, as well as all other terms upon which credit will be extended to Accountholders thereunder, and (ii) the credit criteria applicable to the establishment of Installment Accounts and the assignment of loan amounts.</li>
                     <li>HPS will develop and provide to Professional separate operating procedures for the Installment Product governing, among other things, the flow of Installment Account application information, Credit Agreement information, document retention and the logistics and specific procedures involved in the establishment and maintenance of Installment Accounts. All Credit Agreements will be processed and submitted to HPS in accordance with the Installment Product operating procedures.</li>
                     <li>The chargeback provisions of the Agreement will apply to purchases made on Installment Accounts, except for Section 6(a)(x) or (xi) with respect to credit-not-present purchase transactions.</li>
                  </ul>

               </li>

               <li><strong>Provision of Processing Terminals/ Credit Processing.</strong> HPS will provide a point-of-sale process, which may includeprocessingterminals orothermeans (each, a “Terminal”), to be used for the electronic authorizationandmonetarysettlement of Program Credit applications and Program Credit transactions (and which shall not be used to process other credit transactions without HPS’s consent). HPS may also supply Professional with a manual credit imprinter, for use in the event the Terminal malfunctions. Any Terminal or imprinter provided to Professional will remain HPS’s property for which HPS may charge a usage fee. Professional will return any terminals in its possession to HPS’s request. However, duringthetime Professionalhas possession of the Terminal, Professional will bear any personal property, use or excise taxes assessed on the Terminal. If Professional fails to timely return any Terminal upon HPS’s request following the termination of this Agreement, Professional shall pay to HPS an additional fee, as determined by HPS from time to time. Professional will be responsiblefor any damage or repair to a Terminal or imprinter provided to it byHPS, and Professional will safeguard the Terminal and imprinter and use them only in accordance with applicable instructions and specifications. HPS specifically does not grant to Professional any intellectual property rights associated with the Terminal or other point-of-sale equipment, software or peripherals.
                  </li>

               <li><strong>Ownership of Accounts and Information.</strong> Professional acknowledges that HPS owns all Accounts and Program Credit, and all information concerning Credit holders, applicants and Accounts obtained in connection with the Program (collectively, <u>“Credit holder Information”</u>), and that Professional has no ownership rights therein. The parties acknowledge that Credit holder Information, which documents the relationship between individual Customers/patients and HPS, is not considered <u>“Protected Health Information”</u>, as that term is defined in federal health care privacy regulations. Accordingly, Professional will not represent itself as the owner of, or the creditor on, any Account or Credit holder Information. As a precaution, to confirm HPS’s ownership of Accounts and related documentation, Professional hereby grants to HPS a first priority continuing security interest in any right, title or interest that Professional may now have or may hereafter be deemed to have in the Accounts and related documentation, and in the Reserve Account. Professional authorizes HPS to prepare and file any documentation required to evidence and enforce this security interest, including UCC financing statements, and will sign any related documentation requested by HPS, including without limitation, any intercreditor agreements necessary to ensure that none of Professional’s other creditors asserts any claim on the Accounts, the Reserve Account or any related documentation.

                  </li>

               <li><strong>Professional’s Representations, Warranties and Covenants.</strong> Professional represents, warrants and covenants as follows:

                     <ul>
                     <li>Professional will forward to HPS promptly after receipt, at any time during or following Professional’s participation in the Program, a copy of any legal proceeding, or a communication relating to an Account, a Program Credit transaction received from a Credit holder or from a governmental or regulatory authority.</li>
                     <li>Without HPS’s consent, Professional will not permit the sale of extended warranties, service contracts, gift certificates, stored value Credit (or reloads), or any other future service or delivery obligation (including any pre-paid membership (periodic or lifetime) or similar product), to be charged to Accounts, and will not add a transaction fee to any charge or transaction.</li>
                     <li>Professional will issue a credit to an Account (and not give any Credit holder cash) in connection with any return or exchange of merchandise or services originally charged to the corresponding Program Credit; such credit shall be included in the next business day’s transmission of Charge Transaction Data; the amount of such credit cannot exceed the amount shown as the total on the original charge slip, except by the exact amount required to reimburse the Credit holder for postage that the Credit holder paid to return merchandise; and, Professional shall not issue a credit to any Credit holder unless Professional shall have previously completed a retail charge transaction with such Credit holder on the corresponding Program Credit.</li>
                     <li>On behalf of HPS, Professional shall (i) store (in accordance with the security requirements set forth below) original documentation of each Program Credit transaction for at least one year from the date of the respective transaction; (ii) retain copies of all charge and credit slips, original completed and signed Program Credit applications, and copies of all Charge Transaction Data submitted to HPS, for at least seventy-two (72) months and thereafter continuously unless after retaining such documents for the seventy-two (72) month period Professional offers to ship such documents to HPS and HPS authorizes Professional to securely destroy them instead; and (iii) provide any or all of these records to HPS promptly, but no later than seven (7) business days following HPS’s request.</li>
                     <li>Professional is in compliance with, and will continue to comply to the reasonable satisfaction of HPS with, all applicable laws, rules and regulations, including but not limited to: laws relating to (i) its sales of merchandise and services; (ii) the advertising or sale of products and services on credit; (iii) point-of-sale practices and representations made by Professional’s employees and representatives; and (iv) laws relating to privacy and data security, including without limitation the requirement to have and maintain a written data security policy.</li>
                     <li>Professional will provide only truthful and complete information to Credit holders regarding Accounts and will take no action to prevent any amounts charged to any Program Credit from being valid and enforceable against the applicable Credit holder.</li>
                     <li>Professional will properly code all promotional charges and will make any corrections necessary in the event of mistakes and disputes regarding promotions.</li>
                     <li>Professional is and will at all times remain solvent, duly organized, validly existing and in good standing under the laws of its state of formation, will not violate its organizational documents or materially violate any agreementsithas withthirdparties,and will adviseHPS promptly of any condition or default under any agreement Professional has with any third party that may materially affect Professional’s prospects, continued operations, or property.</li>
                     <li>Any and all information previously furnished by Professional to HPS, or any information subsequently furnished by Professional, including information provided in Professional’s credit application or registration for participation in the Program, is or shall be true and correct in all material respects when furnished.</li>
                     <li>With respect to any transaction for which a Program Credit is not physically presented, such as in any on-line, mail, telephone or pre-authorized transaction, Professional must (i) have notified HPS in writing of Professional’s intention to conduct such transactions, and HPS must have agreed to accept them, and (ii) have reasonable procedures in place to ensure that each Program Credit sale is made to a purchaser who actually is the Credit holder or the authorized user of the Program Credit.</li>
                     <li>For a sale/service charged to an account where the Credit holder pays in installments or on a deferred payment plan, a separate sales authorization has been obtained and a separate sale record has been prepared for each installment transaction or deferred payment on the date(s) the Credit holder agreed to be charged. All installments and deferred payments, whether or not they have been submitted to HPS for processing, shall be deemed to be a part of the original sale.</li>
                     <li>Professional will not submit a Program Credit transaction for the purpose of (i) disbursing cash (or scrip) to the Credit holder, or (ii) refinancing an existing debt.</li>
                     <li>Professional will not require (i) a Credit holder to complete any post credit or similar device that includes the Credit holder’s name, account number, Program Credit expiration date, signature, or any other related account data when any such information would be in plain view when mailed, or (ii) request an account number from a Credit holder for any purpose other than as payment for the sale of Professional’s goods and/or services.</li>
                     <li>Subject to Section 2(m), Professional shall not submit Charge Transaction Data for services not yet rendered, unless those services are (i) intended to be and are completed, or (ii) for out-of-pocket costs incurred, within thirty (30) days of the applicable transaction date.</li>
                     <li>Professional will review on a regular basis and will ensure that (x) each health care professional participating as a “Professional” in the Program hereunder has all required medical or professional license(s), (y) such license(s) are current, and (z) all requirements (including as set forth in HPS’s enrollment materials for Professionals) in connection therewith have been and are continuing to be satisfied by each such health care professional.</li>
                     <li>If Professional accepts credit applications in both English and Spanish, Professional will ensure that its systems provide the ability to send a language identifier to HPS and will provide application disclosures, whether in written or electronic form, in the same language as the related application.</li>
                     <li>Professional will not offer any incentives to its employees or conduct employee contests in connection with its operation of the Program.</li>
                  </ul>

               </li>

            </ol>
           
        </div>
        <div class="customer-accnt">
            <p><span class="agreement_date"><strong>Agreement Date:</strong> ${moment().format('MM/DD/YYYY HH:mm:ss')}</span></p><br/>
            <div class="add-card sing_box customer-signature">
               <b>Signature</b>
               <div class="image-area"><img src="${data.signature}"></div>
            </div>
         </div>
         
        </div>
     </body>
  </html>
      `;
};