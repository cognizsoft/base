/*
* Title: States
* Descrpation :- This module blong to states realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/state-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT states.state_id, states.name, states.abbreviation, states.serving, states.status, states.country_id, countries.name as country FROM states INNER JOIN countries ON states.country_id = countries.id WHERE delete_flag = ? ORDER BY states.state_id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })


    app.get('/api/state-country-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT id, name FROM countries WHERE deleted_flag = ?',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })


    app.post('/api/state-insert/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO states (name, abbreviation, serving, status, country_id, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
            [
                req.body.name,
                req.body.abbreviation,
                req.body.serving,
                req.body.status,
                req.body.country,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_state_id": result.insertId,
                        "name": req.body.name,
                        "abbreviation": req.body.abbreviation,
                        "serving": req.body.serving,
                        "state_status": req.body.status,
                        "country": req.body.country,
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/state-name-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.state_id === undefined) {
          sql = 'SELECT name FROM states WHERE delete_flag=? AND name=?';
          var edit = 0;
        } else {
          sql = 'SELECT name FROM states WHERE delete_flag=? AND name=? AND state_id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            0,
            req.body.name,
            req.body.state_id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0,
                "edit": edit
              }
              res.send(obj);
    
            }
          })
      })
    
      app.post('/api/state-abbreviation-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.state_id === undefined) {
          sql = 'SELECT abbreviation FROM states WHERE delete_flag=? AND abbreviation=?';
          var edit = 0;
        } else {
          sql = 'SELECT abbreviation FROM states WHERE delete_flag=? AND abbreviation=? AND state_id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            0,
            req.body.abbreviation,
            req.body.state_id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                  "status": 0,
                  "message": "Something wrong please try again."
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0,
                "edit": edit
              }
              res.send(obj);
    
            }
          })
      })

      app.post('/api/state-update/', jwtMW,(req,res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
         con.query('UPDATE states SET name=?, abbreviation=?, serving=?, status=?, date_modified=?, modified_by=? WHERE state_id=?',
         [
          req.body.name,
          req.body.abbreviation,
          req.body.serving,
          req.body.status,
          current_date,
          current_user_id,
          req.body.state_id
         ]
         , function (err,result){
              if(err) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
              } else {
                var obj = {
                    "status": 1
                }
                res.send(obj);
               
              }
         })
      })
}
