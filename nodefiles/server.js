/*
* Title: Server
* Descrpation :- This module blong to manage all request according to react call
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const exjwt = require('express-jwt');

const port = 5000;

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
var con = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  database:'health',

});
app.use(cors());
con.connect(function(error){
  if(error) console.log('mysql not found')
  else console.log('Connected')
});
// check user token
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
  next();
});
/*========= Here we will set up an express jsonwebtoken middleware(simply required for express to properly utilize the token for requests) You MUST instantiate this with the same secret that will be sent to the client ============*/
const jwtMW = exjwt({
  secret: 'ramesh'
});

require('./users.js')(app,con,jwtMW);
require('./userTypes.js')(app,con,jwtMW);
require('./userRoles.js')(app,con,jwtMW);
require('./permission-filter.js')(app,con,jwtMW);
require('./master.js')(app,con,jwtMW);
require('./masterValue.js')(app,con,jwtMW);
require('./states.js')(app,con,jwtMW);
require('./regions.js')(app,con,jwtMW);
require('./questions.js')(app,con,jwtMW);
require('./surchargeType.js')(app,con,jwtMW);
require('./riskFactor.js')(app,con,jwtMW);
require('./lateFeeWaiver.js')(app,con,jwtMW);
require('./scoreThreshold.js')(app,con,jwtMW);
require('./systemModule.js')(app,con,jwtMW);
require('./payback.js')(app,con,jwtMW);
require('./interestRate.js')(app,con,jwtMW);
require('./financialCharges.js')(app,con,jwtMW);
require('./country.js')(app,con,jwtMW);
require('./creditapplication.js')(app,con,jwtMW);
require('./experian.js')(app,con);

app.listen(port,() => console.log(`Server started on port ${port}`));
