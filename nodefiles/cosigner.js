module.exports = function (app, jwtMW) {
    app.get('/api/creditapplication-list-co-signer/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,credit_applications.document_flag,credit_applications.plan_flag,credit_applications.score,credit_applications.approve_amount,DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date,credit_applications.remaining_amount,credit_applications.override_amount,credit_applications.status,patient.f_name,patient.m_name,patient.l_name,patient.gender,patient.email,patient.peimary_phone,patient.patient_ac, '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, credit_applications.application_id, master_data_values.value as status_name, '
            + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_applications.status AND master_data_values.md_id=? '
            + 'INNER JOIN credit_app_cosigner ON credit_app_cosigner.application_id=credit_applications.application_id '
            + 'WHERE credit_applications.co_signer=? AND credit_applications.delete_flag=? AND credit_app_cosigner.co_signer_id=? ORDER BY application_id DESC',
            /*+ 'WHERE credit_applications.delete_flag=? AND '+cond+' ORDER BY application_id DESC',*/
            [
                'Application Status',
                0,
                0,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });
    // cosigner weekly monthly filter option
    app.post('/api/cosigner-dashboard-weekmonth-filter/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.cosignerInvoiceReportsFilter(con, req.body, function (result) {
            res.send(result);
        });
    });
    // download pdf reports for custoemr weekly & monthly invoice
    app.post('/api/cosigner-dashboard-weeklt-monthly-invoice-pdf/', jwtMW, (req, res) => {
        con = require('../db');
        const pdf = require('html-pdf');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.cosignerInvoiceReportsFilter(con, req.body, function (result) {
            if (result.status == 1) {
                const pdfTemplate = require('./reports/customerDashboardInvoicePrintDocuments');

                const dir = './uploads/report/customer/'
                //var fullUrl = req.protocol + '://' + req.get('host');
                //var logoimg = fullUrl + '/uploads/logo.png';
                var fullUrl = req.get('origin');
                var logoimg = fullUrl + '/logo.png';
                var filename = dir + 'customer-invoice.pdf';
                pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }
                })
            }
        });
    });

    app.post('/api/cosigner-dashboard-weeklt-monthly-invoice-xls/', jwtMW, (req, res) => {
        con = require('../db');
        const excel = require('exceljs');
        var dataSql = require('./reports/customerDashboardInvoiceReports.js');
        dataSql.cosignerInvoiceReportsFilter(con, req.body, function (result) {
            if (result.status == 1) {
                const jsonData = result.result;

                let workbook = new excel.Workbook(); //creating workbook
                let worksheet = workbook.addWorksheet('Customer Invoices'); //creating worksheet
                //  WorkSheet Header
                worksheet.columns = [
                    { header: 'Invoice ID', key: 'Invoice ID', width: 10 },
                    { header: 'Customer Name', key: 'Customer Name', width: 30 },
                    { header: 'Address', key: 'Address', width: 30 },
                    { header: 'City', key: 'City', width: 30 },
                    { header: 'State', key: 'State', width: 20, numFmt: '#.##' },
                    { header: 'Zip', key: 'Zip', width: 20, },
                    { header: 'Phone', key: 'Phone', width: 20 },
                    { header: 'Invoice date', key: 'Invoice date', width: 15 },
                    { header: 'Invoice due date', key: 'Invoice due date', width: 15 },
                    { header: 'Invoice Amt', key: 'Invoice amt', width: 15 },
                    { header: 'Addtl Amt', key: 'Invoice amt', width: 15 },
                    { header: 'Total Paid', key: 'Total Paid', width: 15 },
                    { header: 'Invoice status', key: 'Invoice status', width: 20, },
                ];

                // Add Array Rows
                //worksheet.addRows(jsonData);
                var rowValues = [];
                var count = 2;
                jsonData.forEach(function (element, idx) {
                    element.payment_date = (element.payment_date) ? element.payment_date : '-';



                    element.payment_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.payment_amount)).toFixed(2)
                    element.payment_amount = parseFloat(element.payment_amount);

                    element.paid_amount = (parseFloat(element.late_fee_received) + parseFloat(element.fin_charge_received) + parseFloat(element.paid_amount)).toFixed(2)
                    element.paid_amount = parseFloat(element.paid_amount);


                    worksheet.getCell('A' + count).value = element.invoice_number;
                    worksheet.getCell('B' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
                    worksheet.getCell('C' + count).value = element.address1;
                    worksheet.getCell('D' + count).value = element.City;
                    worksheet.getCell('E' + count).value = element.state_name;
                    worksheet.getCell('F' + count).value = element.zip_code;
                    worksheet.getCell('G' + count).value = element.peimary_phone;
                    worksheet.getCell('H' + count).value = element.payment_date;
                    worksheet.getCell('I' + count).value = element.due_date;
                    worksheet.getCell('J' + count).value = element.payment_amount;
                    worksheet.getCell('K' + count).value = element.additional_amount;
                    worksheet.getCell('L' + count).value = element.additional_amount + element.paid_amount;
                    worksheet.getCell('M' + count).value = element.invoice_status_name;

                    worksheet.getCell('J' + count).numFmt = '0.00';
                    worksheet.getCell('K' + count).numFmt = '0.00';
                    worksheet.getCell('L' + count).numFmt = '0.00';

                    count++;
                });
                // set header details
                ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1'].map(key => {
                    worksheet.getCell(key).fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'aba2a2' },
                        bgColor: { argb: 'aba2a2' }
                    };
                });
                // merge A and B column
                worksheet.mergeCells('A' + count + ':D' + count);
                worksheet.getCell('A' + count).value = 'Total';
                // total for actual amount
                const sumRange = `${'J2'}:${'J' + (count - 1)}`;
                worksheet.getCell('J' + count).numFmt = '0.00';
                worksheet.getCell('J' + count).value = { formula: `SUM(${sumRange})` };
                // total for additional amount
                const sumRangeAdd = `${'K2'}:${'K' + (count - 1)}`;
                worksheet.getCell('K' + count).numFmt = '0.00';
                worksheet.getCell('K' + count).value = { formula: `SUM(${sumRangeAdd})` };
                // total for total paid amount
                const sumRangeAddTot = `${'L2'}:${'L' + (count - 1)}`;
                worksheet.getCell('L' + count).numFmt = '0.00';
                worksheet.getCell('L' + count).value = { formula: `SUM(${sumRangeAddTot})` };


                // Write to File
                const dir = './uploads/report/'

                var filename = dir + 'customer-invoice.xlsx';
                workbook.xlsx.writeFile(filename)
                    .then(function () {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }).catch(function () {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    });
            }
        });
    });

    /*
    * View customer
    */
    getCosignerDetails = async (req, res, next) => {
        con = require('../db');
        let dataSql = require('./credit/creditSql.js');
        let obj = {}
        obj.status = 0;
        let appDetails;


        await dataSql.getAppCosignerDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.CoappDetails = sResult.planDetails;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getAddDetails(con, req.query.id, 1, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.CoappAddress = sResult.appAddress;
            }
        })
        if (appDetails == 0) { return false }



        await dataSql.getBankDetails(con, req.query.id, 1, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.CobankDetails = sResult.bankDetails;
            }
        })
        if (appDetails == 0) { return false }

        obj.status = 1;
        res.send(obj)
    }
    app.get('/api/cosigner-view', jwtMW, getCosignerDetails)
    /*
    * customer edit profile edit
    */
    app.get('/api/edit-cosigner-profile-details/', jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.editCosignerProfileDetails(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);

            }
        });
    });

    /*
    /*update customer profile
    */
    app.post('/api/cosigner-profile-update/', jwtMW, (req, res) => {
        
        con = require('../db');
        // call sql function and callback for next process.
        var dataSql = require('./customer-dashboard/plan.js');
        dataSql.updateCosignerProfile(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });

    });
}