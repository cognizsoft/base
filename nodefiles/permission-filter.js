/*
* Title: permission filter
* Descrpation :- This module blong to user type all application
* Date :- 11 March 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
  let date = require('date-and-time');
  app.get('/api/permision-filter', jwtMW, (req, res) => {
    con = require('../db');
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('select mdv_id,value,status from master_data_values where md_id=? AND deleted_flag = ?',
        [
          'User Type',
          0
        ]
        , function (error, rows) {
          if (error) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.UserType = rows;
          }
        });
      con.query('SELECT user_type_role.user_type_role_id AS mdv_id, '
        +'master_data_values.value, '
        +'master_data_values.status '
        
        +'FROM master_data_values '

        +'INNER JOIN user_type_role ON user_type_role.mdv_role_id=master_data_values.mdv_id AND user_type_role.mdv_type_id=? '

        +'WHERE master_data_values.md_id=? AND master_data_values.deleted_flag = ? '

        +'ORDER BY user_type_role.user_type_role_id ASC',
        [
          1,
          'User Role',
          0
        ]
        , function (err, result) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          } else {
            obj.userRole = result;
          }

        });

      
      con.query('SELECT system_module.sys_mod_id,system_module.description, '
              
        +'(SELECT permission.create_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS create_flag, '

        +'(SELECT permission.edit_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS edit_flag,'

        +'(SELECT permission.view_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS view_flag,'

        +'(SELECT permission.delete_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS delete_flag '

        +'FROM system_module',
      [
        1,
        1,
        1,
        1
      ]
      , function (error, rowsper, fields) {
        if (!error){
            obj.perresult = rowsper;
        }
      })
      con.commit(function (err) {
        if (err) {
          con.rollback(function () {
            obj.status = 0;
            obj.message = "Something wrong please try again.";
          });
        } else {
          obj.status = 1;
        }
        res.send(obj);
      });
    });
    //var result;
    // get user type details
    /*con.query('select mdv_id,value,status from master_data_values where md_id="User Role" AND deleted_flag = 0', function (error, rows, fields) {
      if (error) console.log(error)
      else {
        result = {
          "status": 1,
          "result": rows,
        }
        res.send(result);
      }
    })*/

  })

  app.get('/api/permision-module', jwtMW, (req, res) => {
    con = require('../db');
    var result;
    // get user type details
    //console.log(req.query.user_role)
    
    con.query('SELECT system_module.sys_mod_id,system_module.description, '
              
      +'(SELECT permission.create_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS create_flag, '

      +'(SELECT permission.edit_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS edit_flag,'

      +'(SELECT permission.view_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS view_flag,'

      +'(SELECT permission.delete_flag FROM permission WHERE permission.sys_mod_id =  system_module.sys_mod_id AND permission.user_type_role_id = ?) AS delete_flag '

      +'FROM system_module',
      [
        req.query.user_role,
        req.query.user_role,
        req.query.user_role,
        req.query.user_role
      ], 
      function (error, rows, fields) {
      if (error) {
        var obj = {
            "status": 0,
            "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        result = {
          "status": 1,
          "result": rows,
        }
        res.send(result);
      }
    })

  })
  app.post('/api/permision-submit', jwtMW, (req, res) => {
    con = require('../db');
    let permission = req.body.permission;
    var values = [];
    var del_values = [];
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    permission.forEach(function (element) {
      del_values.push([element.sys_mod_id, req.body.role_id]);
      values.push([element.sys_mod_id, req.body.role_id, element.edit_flag, element.view_flag, element.delete_flag, element.create_flag, current_date, current_date, req.body.current_user_id, req.body.current_user_id]);
    });
    var sql_del = "DELETE FROM permission WHERE (sys_mod_id, user_type_role_id) IN (?)";
    con.query(sql_del, [del_values], function (error) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        var sql = "INSERT INTO permission (sys_mod_id, user_type_role_id, edit_flag, view_flag, delete_flag, create_flag, date_created, date_modified, created_by, modified_by) VALUES ?";
        con.query(sql, [values], function (error) {
          if (error) {
            var obj = {
              "status": 0,
              "message": "Something wrong please try again."
            }
            res.send(obj);
          } else {
            var obj = {
              "status": 1,
              "message": "Record updated successfully."
            }
            res.send(obj);
          }
        });
      }
    });


  })




  //other routes..
}
