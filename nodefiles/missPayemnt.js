/*
* Title: miss payment
* Descrpation :- This module belong to get and update miss payment accrording to payment plan
* Date :- Jan 09 2019
* Author :- Ramesh Kumar
*/
var missPayment = function () {
    con = require('../db');
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    // get missed installment
    con.query('SELECT pp_installment_id, pp_id FROM pp_installments '
        + 'WHERE missed_flag = 0 AND paid_flag = 0 AND due_date<=NOW() ORDER BY pp_id'
        //+ 'WHERE due_date<=NOW() ORDER BY pp_id'
        , function (missErr, result, fields) {
            if (!missErr && Object.keys(result).length > 0) {
                // get unique plan
                let allPlan = result && [...new Set(result.map(item => item.pp_id))];
                let totalPlan = allPlan.length;
                allPlan.map((current_pp_id, idx) => {
                    //filter installment details according to plan
                    var planInstallment = result.filter(function (e) {
                        return (e.pp_id == current_pp_id);
                    });
                    // update installment details
                    installmentTable(con, planInstallment, function (Responce) {

                        if (Responce.status == 1) {
                            // update missed counter in plan
                            con.query('UPDATE payment_plan SET '
                                + 'late_count=(late_count+' + Responce.count + '), '
                                + 'missed_count=(missed_count+' + Responce.count + '), '
                                + 'date_modified=?, '
                                + 'modified_by=? '
                                + 'WHERE pp_id=?'
                                , [current_date, 1, current_pp_id],
                                function (updaetErr, result) {
                                    if (!updaetErr) {
                                        // call update plan function
                                        planUpdate(con, current_pp_id, function (responceplan) {
                                            if (responceplan.status == 1) {
                                                if (0 === --totalPlan) {
                                                    lockApplication()
                                                    console.log('finaly done')
                                                }
                                            } else {
                                                if (0 === --totalPlan) {
                                                    lockApplication()
                                                    console.log('done')
                                                }
                                            }

                                        })
                                    } else {
                                        if (0 === --totalPlan) {
                                            lockApplication()
                                            console.log('done')
                                        }
                                    }
                                })
                        } else {
                            if (0 === --totalPlan) {
                                lockApplication()
                                console.log('done')
                            }
                        }
                    });

                })
            } else {
                lockApplication()
            }
        });
};
/*
* Title :- planUpdate
* description :- this function use for update due installment according to full intrest rate
* Date :- Jan 17, 2020
*/
var planUpdate = function (con, planID, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let obj = {};
    // get missed payment from master define by admin
    con.query('SELECT value '
        + 'FROM master_data_values '
        + 'WHERE md_id=? ORDER BY mdv_id LIMIT 1'
        , ['Missed Payment Interest Rate Reset'], function (masterErr, masterResult) {
            if (!masterErr) {
                // get plan details
                con.query('SELECT DATE_FORMAT(pp_installments.due_date, "%Y-%m-%d") as due_date,pp_installments.pp_installment_id, pp_installments.installment_amt, payment_plan.interest_rate, '
                    + 'payment_plan.loan_amount,payment_plan.loan_type,payment_plan.term,pp_installments.paid_flag,payment_plan.monthly_amount,payment_plan.discounted_interest_rate '
                    + 'FROM pp_installments '
                    + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
                    //+ 'WHERE late_flag = 0 AND due_date<=NOW() ORDER BY pp_id'
                    + 'WHERE missed_count>=? AND payment_plan.pp_id=? AND payment_plan.interest_rate!=payment_plan.discounted_interest_rate ORDER BY pp_installment_id'
                    , [masterResult[0].value, planID], function (getErr, result, fields) {
                        // check plan avaiable or not
                        if (!getErr && Object.keys(result).length > 0) {
                            // check plabn type
                            if (result[0].loan_type == 1) {
                                // check plan grate then 12 month
                                if (result[0].term > 12) {
                                    // get new per month installment
                                    var perMonthInt = (parseFloat(result[0].interest_rate) / 12) / 100;
                                    var perMonth = parseFloat(result[0].loan_amount) * (perMonthInt * Math.pow((1 + perMonthInt), 12)) / (Math.pow((1 + perMonthInt), 12) - 1);

                                    var values = [];
                                    var totalAmount = 0;//parseFloat(perMonth).toFixed(2) * result[0].term;
                                    for (var i = 0; i < result[0].term; i++) {
                                        if (result[i].paid_flag == 1) {
                                            totalAmount += result[i].installment_amt;
                                        } else {
                                            totalAmount += perMonth;
                                            values.push(result[i].pp_installment_id)
                                        }
                                    }
                                    // update installment
                                    con.query('UPDATE pp_installments SET '
                                        + 'installment_amt=?, '
                                        + 'date_modified=?, '
                                        + 'modified_by=? '
                                        + 'WHERE pp_installment_id IN(?)'
                                        , [perMonth, current_date, 1, values],
                                        function (updaetErr, updated) {
                                            if (!updaetErr) {
                                                // update plan
                                                con.query('UPDATE payment_plan SET '
                                                    + 'discounted_interest_rate=?, '
                                                    + 'amount=?, '
                                                    + 'plan_updated=?, '
                                                    + 'date_modified=?, '
                                                    + 'modified_by=? '
                                                    + 'WHERE pp_id=?'
                                                    , [result[0].interest_rate, totalAmount.toFixed(2), 1, current_date, 1, planID],
                                                    function (mainErr, result) {
                                                        if (!mainErr) {
                                                            obj.status = 1;
                                                            return callback(obj);
                                                        } else {
                                                            obj.status = 0;
                                                            return callback(obj);
                                                        }
                                                    })
                                            } else {
                                                obj.status = 0;
                                                return callback(obj);
                                            }
                                        })

                                } else {
                                    var perMonth = (parseFloat(result[0].loan_amount) +
                                        parseFloat(result[0].loan_amount) *
                                        (
                                            (
                                                parseFloat(result[0].interest_rate) / 100
                                            ) / parseFloat(result[0].term)
                                        )
                                        * parseFloat(result[0].term)
                                    ) / parseFloat(result[0].term);
                                    var values = [];
                                    var totalAmount = 0;
                                    for (var i = 0; i < result[0].term; i++) {
                                        if (result[i].paid_flag == 1) {
                                            totalAmount += result[i].installment_amt;
                                        } else {
                                            totalAmount += perMonth;
                                            values.push(result[i].pp_installment_id)
                                        }
                                    }

                                    con.query('UPDATE pp_installments SET '
                                        + 'installment_amt=?, '
                                        + 'date_modified=?, '
                                        + 'modified_by=? '
                                        + 'WHERE pp_installment_id IN(?)'
                                        , [perMonth, current_date, 1, values],
                                        function (updaetErr, updated) {
                                            if (!updaetErr) {
                                                con.query('UPDATE payment_plan SET '
                                                    + 'discounted_interest_rate=?, '
                                                    + 'amount=?, '
                                                    + 'plan_updated=?, '
                                                    + 'date_modified=?, '
                                                    + 'modified_by=? '
                                                    + 'WHERE pp_id=?'
                                                    , [result[0].interest_rate, totalAmount.toFixed(2), 1, current_date, 1, planID],
                                                    function (mainErr, result) {
                                                        if (!mainErr) {
                                                            obj.status = 1;
                                                            return callback(obj);
                                                        } else {
                                                            obj.status = 0;
                                                            return callback(obj);
                                                        }
                                                    })
                                            } else {
                                                obj.status = 0;
                                                return callback(obj);
                                            }
                                        })
                                }
                            } else if (result[0].loan_type == 0) {
                                // check paid amount
                                var paidRecd = result.filter(function (e) {
                                    return (e.paid_flag == 1);
                                });
                                var term_month = 0;
                                // get discount rate and monthly payment
                                if (paidRecd.length > 0) {
                                    var interest_rate_dis = result[0].discounted_interest_rate / 100;
                                    var monthly_amount_dis = result[0].monthly_amount;
                                }

                                // get interst rate
                                var interest_rate = result[0].interest_rate / 100;
                                var amount = result[0].loan_amount;
                                var monthly_amount = result[0].monthly_amount;
                                // get new term
                                var newTerm = -Math.log(parseFloat(((parseFloat(-interest_rate) * amount) / (12 * monthly_amount) + 1))) / Math.log((1 + parseFloat(interest_rate) / 12));
                                term_month += Math.ceil(newTerm);
                                var totalAmount = 0;
                                var remainingAmt = parseFloat(result[0].loan_amount);
                                var dataCount = result.length;
                                var z = 0;

                                // get new monthly installment
                                var w = 1;
                                for (var i = 1; i <= term_month; i++) {
                                    // check new term term with old term
                                    if (z == (term_month - 1)) {
                                        //check base loan amount
                                        if (w == 1) {
                                            result[0].loan_amount = remainingAmt;
                                        }
                                        // get base remaining amount
                                        if (i < term_month) {
                                            var redAmt = (((12 * monthly_amount) / interest_rate) - result[0].loan_amount) * (Math.pow(1 + (interest_rate / 12), w) - 1);
                                        } else {
                                            var redAmt = 0;
                                            monthly_amount = (remainingAmt * (1 + (interest_rate / 12))).toFixed(2);
                                        }
                                        remainingAmt = result[0].loan_amount - redAmt;
                                        totalAmount += parseFloat(monthly_amount);
                                        w++;
                                        let newDate = new Date(result[0].due_date);
                                        newDate = date.addMonths(newDate, (i - 1));
                                        newDate = new Date(date.format(newDate, 'YYYY'), date.format(newDate, 'MM'), 0);
                                        newDate = date.format(newDate, 'YYYY-MM-DD');
                                        // insert new installment
                                        con.query('INSERT INTO pp_installments '
                                            + '(`pp_id`, `installment_amt`, `due_date`, date_created, date_modified, created_by, modified_by) '
                                            + 'VALUES (?,?,?,?,?,?,?) '
                                            , [planID, monthly_amount, newDate, current_date, current_date, 1, 1],
                                            function (updaetErr, updated) {
                                                if (!updaetErr) {
                                                    if (0 === --dataCount) {
                                                        // update plan
                                                        updaetPlan(con, result[0].interest_rate, totalAmount, current_date, planID, function (Responce) {
                                                            if (Responce.status == 1) {
                                                                obj.status = 1;
                                                                return callback(obj);
                                                            } else {
                                                                obj.status = 0;
                                                                return callback(obj);
                                                            }
                                                        })
                                                    }
                                                }
                                            })
                                    } else {
                                        // check paid installment
                                        if (result[z].paid_flag == 1) {
                                            totalAmount += result[z].installment_amt;
                                            var amountPiad = (((12 * monthly_amount_dis) / interest_rate_dis) - result[0].loan_amount) * (Math.pow(1 + (interest_rate_dis / 12), i) - 1);
                                            remainingAmt = result[0].loan_amount - amountPiad;
                                            --dataCount;
                                        } else {
                                            // check new base amount
                                            if (w == 1) {
                                                result[0].loan_amount = remainingAmt;
                                            }
                                            // get base remaining amount
                                            if (i < term_month) {
                                                var redAmt = (((12 * monthly_amount) / interest_rate) - result[0].loan_amount) * (Math.pow(1 + (interest_rate / 12), w) - 1);
                                            } else {
                                                var redAmt = 0;

                                                monthly_amount = (remainingAmt * (1 + (interest_rate / 12))).toFixed(2);
                                            }
                                            remainingAmt = result[0].loan_amount - redAmt;
                                            totalAmount += parseFloat(monthly_amount);
                                            w++;
                                            //update installment
                                            con.query('UPDATE pp_installments SET '
                                                + 'installment_amt=?, '
                                                + 'date_modified=?, '
                                                + 'modified_by=? '
                                                + 'WHERE pp_installment_id IN(?)'
                                                , [monthly_amount, current_date, 1, result[z].pp_installment_id],
                                                function (updaetErr, updated) {
                                                    if (!updaetErr) {
                                                        if (0 === --dataCount) {
                                                            // update plan
                                                            updaetPlan(con, result[0].interest_rate, totalAmount, current_date, planID, function (Responce) {
                                                                if (Responce.status == 1) {
                                                                    obj.status = 1;
                                                                    return callback(obj);
                                                                } else {
                                                                    obj.status = 0;
                                                                    return callback(obj);
                                                                }
                                                            })
                                                        }
                                                    }
                                                })
                                        }
                                    }

                                    z++;

                                }
                            }
                        } else {
                            obj.status = 0;
                            return callback(obj);
                        }
                    })
            }
        })
}
/*
* Title :- updaetPlan
* description :- this function use for update payment plan
* Date :- Jan 17, 2020
*/
var updaetPlan = function (con, interest_rate, totalAmount, current_date, planID, callback) {
    var obj = {};
    con.query('UPDATE payment_plan SET '
        + 'discounted_interest_rate=?, '
        + 'amount=?, '
        + 'plan_updated=?, '
        + 'date_modified=?, '
        + 'modified_by=? '
        + 'WHERE pp_id=?'
        , [interest_rate, totalAmount.toFixed(2), 1, current_date, 1, planID],
        function (mainErr, result) {
            if (!mainErr) {
                obj.status = 1;
                return callback(obj);
            } else {
                obj.status = 0;
                return callback(obj);
            }
        })

}
/*
* Title :- installmentTable
* description :- this function use for update installment
* Date :- Jan 17, 2020
*/
var installmentTable = function (con, planInstallment, callback) {
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let totalInst = planInstallment.length;
    let count = 0;

    planInstallment.map((data, idx) => {
        con.query('UPDATE pp_installments SET '
            + 'missed_flag=?, '
            + 'late_flag=?, '
            + 'date_modified=?, '
            + 'modified_by=? '
            + 'WHERE pp_installment_id=?'
            , [1, 1, current_date, 1, data.pp_installment_id],
            function (updaetErr, result) {
                if (!updaetErr) {
                    count++;
                    if (0 === --totalInst) {
                        obj.status = 1;
                        obj.count = count;
                        return callback(obj);
                    }
                }
            })
    })


}
/*
* Title :- lockApplication
* description :- this function use for lock application
* Date :- Jan 17, 2020
*/
var lockApplication = function () {
    con = require('../db');
    var obj = {};
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    con.query('UPDATE credit_applications SET status=6 '
        + 'WHERE application_id IN(SELECT application_id FROM payment_plan WHERE missed_count >= 2 GROUP BY application_id)'
        , function (missErr, result, fields) {
            if (!missErr) {
                console.log('Done')
            } else {
                console.log('error found')
            }
        })
}
exports.missPayment = missPayment;


