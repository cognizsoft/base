/*
* Title :- oneDrive
* Descrpation :- This file use for get onedrive information and use upload files.
* Date :- Jan 20, 2020
*/
module.exports = function (app) {
    var loginOneDrive = function (con, callback) {

        con.query('SELECT value, md_id '
            + 'FROM `master_data_values` WHERE md_id IN("OneDrive Username", "OneDrive Password", "OneDrive Client Secret", "OneDrive Client Id", "OneDrive Tenant ID")',
            function (error, result) {
                if (error) {
                    var obj = {
                        status: 0,
                    }
                    return callback(obj);
                } else {
                    var obj = {
                        status: 1,
                        result: result
                    }
                    return callback(obj);
                }
            })
    }
    var request = require('request');
    app.get('/api/onedrive/', (req, res) => {
        con = require('../../db');
        loginOneDrive(con, function (loginDetails) {
            if (loginDetails.status == 1 && loginDetails.result.length == 5) {
                // get username
                var username = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Username';
                });
                username = (username.length > 0) ? username[0].value : '';
                // get password
                var password = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Password';
                });
                password = (password.length > 0) ? password[0].value : '';
                // get client id
                var clientSecret = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client Secret';
                });
                clientSecret = (clientSecret.length > 0) ? clientSecret[0].value : '';
                // get secrate 
                var clientId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client ID';
                });
                clientId = (clientId.length > 0) ? clientId[0].value : '';
                // get Telent ID
                var telentId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Tenant ID';
                });
                telentId = (telentId.length > 0) ? telentId[0].value : '';
                // call one drive
                var requestParams = {
                    grant_type: 'password',
                    client_id: clientId,
                    client_secret: clientSecret,
                    scope: 'user.read openid profile offline_access',
                    username: username,
                    password: password,
                };
                request.post({ url: 'https://login.microsoftonline.com/' + telentId + '/oauth2/v2.0/token', form: requestParams }, function (err, response, body) {
                    var parsedBody = JSON.parse(body);
                    if (err) {
                        console.log(err);
                    } else if (parsedBody.error) {
                        console.log(parsedBody.error_description);
                    } else {
                        // If successful, return the access token.
                        var fs = require('fs');
                        var mime = require('mime-types');
                        var file = '../node3/uploads/datafile/Customer/4/plan_generate_agreement.pdf'; // Filename you want to upload on your local PC
                        var onedrive_folder = 'Production/Customer/4'; // Folder name on OneDrive
                        var onedrive_filename = 'plan_generate_agreement.pdf'; // Filename on OneDrive
                        fs.readFile(file, function read(e, f) {
                            request.put({
                                url: 'https://graph.microsoft.com/v1.0/me/drive/root:/' + onedrive_folder + '/' + onedrive_filename + ':/content',
                                headers: {
                                    'Authorization': "Bearer " + parsedBody.access_token,
                                    'Content-Type': mime.lookup(file),
                                },
                                body: f,
                            }, function (er, re, bo) {
                                var obj = {};
                                obj.status = 1;
                                obj.message = bo;
                                res.send(obj);
                            });
                        });

                    }
                });
            } else {
                var obj = {
                    status: 0,
                    message: 'OneDrive Details not found.'
                }
            }
        })
        return false;

    });

    app.get('/api/onedrive2/', (req, res) => {
        con = require('../../db');
        loginOneDrive(con, function (loginDetails) {
            if (loginDetails.status == 1 && loginDetails.result.length == 5) {
                var username = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Username';
                });
                username = (username.length > 0) ? username[0].value : '';
                // get password
                var password = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Password';
                });
                password = (password.length > 0) ? password[0].value : '';
                // get client id
                var clientSecret = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client Secret';
                });
                clientSecret = (clientSecret.length > 0) ? clientSecret[0].value : '';
                // get secrate 
                var clientId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client ID';
                });
                clientId = (clientId.length > 0) ? clientId[0].value : '';
                // get Telent ID
                var telentId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Tenant ID';
                });
                telentId = (telentId.length > 0) ? telentId[0].value : '';

                var requestParams = {
                    grant_type: 'password',
                    client_id: clientId,
                    client_secret: clientSecret,
                    scope: 'user.read openid profile offline_access',
                    username: username,
                    password: password,
                };
                request.post({ url: 'https://login.microsoftonline.com/' + telentId + '/oauth2/v2.0/token', form: requestParams }, function (err, response, body) {
                    var parsedBody = JSON.parse(body);
                    if (err) {
                        console.log(err);
                    } else if (parsedBody.error) {
                        console.log(parsedBody.error_description);
                    } else {
                        var fs = require('fs');
                        var mime = require('mime-types');
                        var file = '../node3/uploads/dadaJi.pdf'; // Filename you want to upload on your local PC
                        var onedrive_folder = 'HPS'; // Folder name on OneDrive
                        var onedrive_filename = 'dadaJi.pdf'; // Filename on OneDrive
                        request.get({
                            //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/content',
                            //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/preview',
                            url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/thumbnails',
                            headers: {
                                'Authorization': "Bearer " + parsedBody.access_token,

                            },
                        }, function (er, re, bo) {
                            console.log(JSON.parse(bo).value[0].large.url)
                            var obj = {};
                            obj.status = 1;
                            obj.file = JSON.parse(bo).getUrl;
                            res.send(JSON.parse(bo).value[0].large.url);
                        });
                    }
                })
            }
        })
    })
}

var getOneDriveFiles = function (callback) {
    loginOneDrive(con, function (loginDetails) {
        if (loginDetails.status == 1 && loginDetails.result.length == 5) {
            var username = loginDetails.result.filter(function (item) {
                return item.md_id == 'OneDrive Username';
            });
            username = (username.length > 0) ? username[0].value : '';
            // get password
            var password = loginDetails.result.filter(function (item) {
                return item.md_id == 'OneDrive Password';
            });
            password = (password.length > 0) ? password[0].value : '';
            // get client id
            var clientSecret = loginDetails.result.filter(function (item) {
                return item.md_id == 'OneDrive Client Secret';
            });
            clientSecret = (clientSecret.length > 0) ? clientSecret[0].value : '';
            // get secrate 
            var clientId = loginDetails.result.filter(function (item) {
                return item.md_id == 'OneDrive Client ID';
            });
            clientId = (clientId.length > 0) ? clientId[0].value : '';
            // get Telent ID
            var telentId = loginDetails.result.filter(function (item) {
                return item.md_id == 'OneDrive Tenant ID';
            });
            telentId = (telentId.length > 0) ? telentId[0].value : '';

            var requestParams = {
                grant_type: 'password',
                client_id: clientId,
                client_secret: clientSecret,
                scope: 'user.read openid profile offline_access',
                username: username,
                password: password,
            };
            request.post({ url: 'https://login.microsoftonline.com/' + telentId + '/oauth2/v2.0/token', form: requestParams }, function (err, response, body) {
                var parsedBody = JSON.parse(body);
                if (err) {
                    console.log(err);
                } else if (parsedBody.error) {
                    console.log(parsedBody.error_description);
                } else {
                    var fs = require('fs');
                    var mime = require('mime-types');
                    var file = '../node3/uploads/dadaJi.pdf'; // Filename you want to upload on your local PC
                    var onedrive_folder = 'HPS'; // Folder name on OneDrive
                    var onedrive_filename = 'dadaJi.pdf'; // Filename on OneDrive
                    request.post({
                        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/content',
                        url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/preview',
                        headers: {
                            'Authorization': "Bearer " + parsedBody.access_token,

                        },
                    }, function (er, re, bo) {
                        console.log(bo)
                        var obj = {};
                        obj.status = 1;
                        obj.file = JSON.parse(bo).getUrl;
                        res.send(obj);
                    });
                }
            })
        }
    })
}

exports.getOneDriveFiles = getOneDriveFiles;


