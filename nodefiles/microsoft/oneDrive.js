var loginOneDrive = function (con, callback) {

    con.query('SELECT value, md_id '
        + 'FROM `master_data_values` WHERE md_id IN("OneDrive Username", "OneDrive Password", "OneDrive Client Secret", "OneDrive Client Id", "OneDrive Tenant ID")',
        function (error, result) {
            if (error) {
                var obj = {
                    status: 0,
                }
                return callback(obj);
            } else {
                var obj = {
                    status: 1,
                    result: result
                }
                return callback(obj);
            }
        })
}

var getOneDriveLogin = function (callback) {
    var request = require('request');
    con = require('../../db');
    return new Promise((resolve, reject) => {
        loginOneDrive(con, function (loginDetails) {
            if (loginDetails.status == 1 && loginDetails.result.length == 5) {
                var username = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Username';
                });
                username = (username.length > 0) ? username[0].value : '';
                // get password
                var password = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Password';
                });
                password = (password.length > 0) ? password[0].value : '';
                // get client id
                var clientSecret = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client Secret';
                });
                clientSecret = (clientSecret.length > 0) ? clientSecret[0].value : '';
                // get secrate 
                var clientId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Client ID';
                });
                clientId = (clientId.length > 0) ? clientId[0].value : '';
                // get Telent ID
                var telentId = loginDetails.result.filter(function (item) {
                    return item.md_id == 'OneDrive Tenant ID';
                });
                telentId = (telentId.length > 0) ? telentId[0].value : '';

                var requestParams = {
                    grant_type: 'password',
                    client_id: clientId,
                    client_secret: clientSecret,
                    scope: 'user.read openid profile offline_access',
                    username: username,
                    password: password,
                };
                request.post({ url: 'https://login.microsoftonline.com/' + telentId + '/oauth2/v2.0/token', form: requestParams }, function (err, response, body) {
                    var parsedBody = JSON.parse(body);
                    if (err) {
                        var obj = {};
                        obj.status = 0;
                        //obj.token = parsedBody.access_token;
                        callback(obj);
                        resolve();
                    } else if (parsedBody.error) {
                        var obj = {};
                        obj.status = 0;
                        //obj.token = parsedBody.access_token;
                        callback(obj);
                        resolve();
                    } else {
                        var obj = {};
                        obj.status = 1;
                        obj.token = parsedBody.access_token;
                        callback(obj);
                        resolve();
                    }
                })
            }
        })
    })
}

var getOneDriveFilesShareLink = function (token, item_id, callback) {
    var request = require('request');
    var obj = {};
    request.get({
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/content',
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/'+item_id+'/preview',
        url: 'https://graph.microsoft.com/v1.0/me/drive/items/' + item_id + '/content',
        headers: {
            'Authorization': "Bearer " + token,
        },
        encoding: null,
    }, function (er, re, bo) {
        //if()
        //var jsd = bo.toString()
        var fn = re.headers['content-disposition']
        var fnSp = fn.split(';')
        var fnSl = fnSp[2].slice(10)
        var fnSlLast = fnSl.slice(0, -1)
        var filename = fnSlLast.substr(0, fnSlLast.lastIndexOf('.'))
        var ext = fnSlLast.substring(fnSlLast.lastIndexOf(".") + 1)
        //console.log(filename+'--'+ext)
        obj.bo = bo;
        obj.filename = filename;
        obj.ext = ext;
        obj.status = 1;
        //obj.found = (JSON.parse(jsd).error) ? false : true

        return callback(obj);
    });

}

var getOneDriveFiles = function (token, item_id, callback) {
    var request = require('request');
    request.get({
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/content',
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/'+item_id+'/preview',
        url: 'https://graph.microsoft.com/v1.0/me/drive/items/' + item_id + '/thumbnails',
        headers: {
            'Authorization': "Bearer " + token,
        },
    }, function (er, re, bo) {
        var obj = {};
        obj.status = 1;
        obj.file = JSON.parse(bo).value[0].large.url;
        return callback(obj);
    });

}

var getOneDriveDownload = function (token, item_id, callback) {
    var request = require('request');
    request.post({
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/012KHYTUUGE4RRRPIH7JCLUWN3LCGFRZIK/content',
        url: 'https://graph.microsoft.com/v1.0/me/drive/items/' + item_id + '/preview',
        //url: 'https://graph.microsoft.com/v1.0/me/drive/items/'+item_id+'/thumbnails',
        headers: {
            'Authorization': "Bearer " + token,
        },
    }, function (er, re, bo) {
        //console.log(JSON.parse(bo).getUrl)
        var obj = {};
        obj.status = 1;
        obj.file = JSON.parse(bo).getUrl;
        return callback(obj);
    });

}

var getOneDriveUpload = function (token, file, callback) {
    var onedrive_filename = file.filename; // Filename on OneDrive
    var provider_id = file.provider_id;
    var customer_id = file.customer_id;
    var file = file.path; // Filename you want to upload on your local PC
    return new Promise((resolve, reject) => {
        var request = require('request');
        var fs = require('fs');
        var mime = require('mime-types');

        if (provider_id) {
            var onedrive_folder = 'HPS/Provider/' + provider_id; // Folder name on OneDrive
        } else {
            var onedrive_folder = 'HPS/Customer/' + customer_id; // Folder name on OneDrive
        }
        
        fs.readFile(file, function read(e, f) {
            request.put({
                url: 'https://graph.microsoft.com/v1.0/me/drive/root:/' + onedrive_folder + '/' + onedrive_filename + ':/content',
                headers: {
                    'Authorization': "Bearer " + token,
                    'Content-Type': mime.lookup(file),
                },
                body: f,
            }, function (er, re, bo) {
                if (er) {
                    var obj = {};
                    obj.status = 1;
                    callback(obj);
                    resolve()
                } else {
                    var obj = {};
                    obj.status = 1;
                    obj.item_id = JSON.parse(bo).id;
                    callback(obj);
                    resolve()
                }
            });
        });
    })

}

var getOneDriveProviderUpload = function (token, file, callback) {

    var request = require('request');
    var fs = require('fs');
    var mime = require('mime-types');
    var onedrive_filename = file.filename; // Filename on OneDrive
    var onedrive_folder = 'HPS/Provider/' + file.provider_id; // Folder name on OneDrive
    var file = file.path; // Filename you want to upload on your local PC
    fs.readFile(file, function read(e, f) {
        request.put({
            url: 'https://graph.microsoft.com/v1.0/me/drive/root:/' + onedrive_folder + '/' + onedrive_filename + ':/content',
            headers: {
                'Authorization': "Bearer " + token,
                'Content-Type': mime.lookup(file),
            },
            body: f,
        }, function (er, re, bo) {
            if (er) {
                var obj = {};
                obj.status = 1;
                return callback(obj);
            } else {
                var obj = {};
                obj.status = 1;
                obj.item_id = JSON.parse(bo).id;
                return callback(obj);
            }
        });
    });

}

exports.getOneDriveFiles = getOneDriveFiles;
exports.getOneDriveLogin = getOneDriveLogin;
exports.getOneDriveDownload = getOneDriveDownload;
exports.getOneDriveUpload = getOneDriveUpload;
exports.getOneDriveProviderUpload = getOneDriveProviderUpload;
exports.getOneDriveFilesShareLink = getOneDriveFilesShareLink;


