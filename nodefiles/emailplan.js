/*
* Title: email sent
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
var emailTemplate = function (toEmailAddress,locals,template,callback) {
  var nodemailer = require('nodemailer');
  const Email = require('email-templates');
  const path = require('path');
  var transport = nodemailer.createTransport({
  /*host: 'ourhealthpartner.com',
    port: 587,
    secure: false,    //<<here
      auth: {
        user: "support@ourhealthpartner.com",
        pass: "Sp5cq12~"
      },*/
      tls: {
          rejectUnauthorized: false
      },
  });
  const email = new Email({
      message: {
          //from: 'info@ourhealthpartner.com'
          from: 'Health Partner Support <info@ourhealthpartner.com>',
          attachments: [
              {
                filename: 'agreement.pdf',
                path: locals.filepath // stream this file
              }
            ]
      },
      send: true,
      juice: true,
        juiceResources: {
          preserveImportant: true,
          webResources: {
            relativeTo: path.join(__dirname, '..', 'emails/'+template+'/assets'),
            images: true
          }
        },
      transport
  });
  //var fullUrl = req.protocol + '://' + req.get('host');
  email.send({
      template: template,//'usersregister',
      message: {
          to: toEmailAddress
      },
      locals: locals,
      
      /*{
          name: 'Elon',
          url : fullUrl
      },*/

  })
  .then((result) => {
      var obj = {
          status:1
      }
      return callback(obj);
  })
  .catch((result) => {
      var obj = {
          status:0
      }
      return callback(obj);
  });
};

var customEmailTemplate = function (emailOpt,context,html,callback) {

  var nodemailer = require('nodemailer');
  var handlebars = require('handlebars');
  const path = require('path');
  //const ABSPATH = path.dirname(process.mainModule.filename);

  var transport = nodemailer.createTransport({
      tls: {
          rejectUnauthorized: false
      },
  });
  
  if(emailOpt.attachment_name && emailOpt.attachment) {

      var attchmnt = [
        {
          filename: emailOpt.attachment_name,
          path: emailOpt.attachment
        }
      ]

  } else {
      var attchmnt = []
  }
  
  var templateEngine = handlebars.compile(html);
  var htmlToSend = templateEngine(context);
  var mailOptions = {
      from: 'Health Partner Support <info@ourhealthpartner.com>',
      to: emailOpt.toEmail,
      subject: emailOpt.subject,
      //text: 'You have a submission with the following details...',
      html: htmlToSend,
      attachments: attchmnt
  };
  transport.sendMail(mailOptions, function(error, info) {
      if (error) {
          var obj = {
            status:0
          }
          return callback(obj);
      } else {
          var obj = {
            status:1,
            message: "Reminder email sent"
          }
          return callback(obj);
      }
  })

};

var customEmailTemplateShare = function (emailOpt,context,html,callback) {
  var nodemailer = require('nodemailer');
  var handlebars = require('handlebars');
  const path = require('path');
  //const ABSPATH = path.dirname(process.mainModule.filename);

  var transport = nodemailer.createTransport({
      tls: {
          rejectUnauthorized: false
      },
  });
  
  
  var templateEngine = handlebars.compile(html);
  var htmlToSend = templateEngine(context);
  var mailOptions = {
      from: 'Health Partner Support <info@ourhealthpartner.com>',
      to: emailOpt.toEmail,
      subject: emailOpt.subject,
      html: htmlToSend,
      attachments: emailOpt.attachments
  };
  transport.sendMail(mailOptions, function(error, info) {
      if (error) {
          var obj = {
            status:0
          }
          return callback(obj);
      } else {
          var obj = {
            status:1,
            message: "Reminder email sent"
          }
          return callback(obj);
      }
  })

};

var customEmailTemplateDetails = function (template_name, callback) {
    return new Promise((resolve, reject) => {
        con.query('SELECT template_id AS id, template_name, template_subject, template_content, status FROM templates WHERE template_name = ?',
            [template_name]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    callback(obj);
                    resolve();
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    callback(obj);
                    resolve();
                }
            })
    })
};


var customEmailTemplateDetailsCron = function (con, template_name,callback) {

  con.query('SELECT template_id AS id, template_name, template_subject, template_content, status FROM templates WHERE template_name = ?',
    [template_name]
    , function (error, rows, fields) {
        if (error) {
            var obj = {
                "status": 0,
                "message": "Something wrong please try again."
            }
            return callback(obj);
        } else {
            var obj = {
                "status": 1,
                "result": rows,
            }
            return callback(obj);
        }
    })

};

exports.emailTemplate = emailTemplate;
exports.customEmailTemplate = customEmailTemplate;
exports.customEmailTemplateDetails = customEmailTemplateDetails;
exports.customEmailTemplateShare = customEmailTemplateShare
exports.customEmailTemplateDetailsCron = customEmailTemplateDetailsCron;
