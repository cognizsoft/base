/*
* Title: userTypes
* Descrpation :- This module blong to user Role all application
* Date :- 24 May 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    var multer = require('multer');
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {

            var customer_id;
            if (req.body.customer_id == undefined) {
                customer_id = 'reject-application';
            } else {
                customer_id = req.body.customer_id;
            }

            const dir = './uploads/customers/' + customer_id + '/' + req.body.application_id
            var mkdirp = require('mkdirp');
            mkdirp(dir, function (err) {
                if (err) console.error(err)
                else cb(null, dir);
            });

        },
        filename: function (req, file, cb) {
            cb(null, Date.now() + file.originalname);
        }
    });

    const fileFilter = (req, file, cb) => {
        //if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
        //} else {
        // rejects storing a file
        //cb(null, false);
        //}
    }

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * 10
        },
        fileFilter: fileFilter
    });
    //var upload = multer({ dest: 'uploads/' })
    var dataupload = upload.single('imgedata');
    app.get('/api/creditapplication-option/', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        var obj = {};
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
            }
            con.query('SELECT id,name FROM countries WHERE status=? AND deleted_flag=?',
                [
                    1,
                    0
                ]
                , function (error, countries) {
                    if (error) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.countries = countries;
                    }
                });
            con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Employment Type'
                ]
                , function (err, employment) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.employment = employment;
                    }

                });
            con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Bank Account Type'
                ]
                , function (err, bank_type) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.bank_type = bank_type;
                    }

                });

            // check provider id exists or not
            if (req.query.id !== undefined && req.query.id != '') {
                con.query('SELECT provider_id,provider_location_id,location_name FROM provider_location WHERE status=? AND provider_id=? AND (parent_provider_location_id IS NULL OR parent_provider_location_id=provider_location_id)',
                    [
                        1, req.query.id
                    ]
                    , function (err, location) {

                        if (err) {
                            con.rollback(function () {
                                obj.status = 0;
                                obj.message = "Something wrong please try again.";
                            });
                        } else {
                            obj.location = location;
                        }
                    });
            }

            con.query('SELECT status_id, value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                [
                    1, 0, 'Relationship'
                ]
                , function (err, relationship) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                        });
                    } else {
                        obj.relationship = relationship;
                    }

                });
            con.commit(function (err) {
                if (err) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                    });
                }
                obj.status = 1;
                res.send(obj);
            });
        })
    })

    app.get('/api/creditapplication-speciality/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        con.query('SELECT master_data_values.mdv_id,master_data_values.value,provider_loc_spec.procedure_id '
            + 'FROM provider_loc_spec '
            + 'INNER JOIN master_data_values ON master_data_values.mdv_id=provider_loc_spec.mdv_speciality_id '
            + 'WHERE provider_loc_spec.status=? AND provider_loc_spec.provider_location_provider_location_id=? AND provider_loc_spec.provider_location_provider_id=?',
            [
                1,
                req.query.id,
                req.query.provider_id
            ]
            , function (error, speciality) {
                if (error) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        res.send(obj);
                    });
                } else {
                    obj.speciality = speciality;
                    obj.status = 1;
                    res.send(obj);
                }
            });


    })

    app.get('/api/creditapplication-states/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT state_id,name FROM states WHERE status=? AND delete_flag=? AND serving=? AND country_id=?',
            [
                1,
                0,
                1,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })

    app.get('/api/creditapplication-region/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT region_id,name FROM regions WHERE status=? AND delete_flag=? AND serving=? AND state_id=?',
            [
                1,
                0,
                1,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    })

    app.get('/mytest/', (req, res) => {
        con = require('../db');
        var experian = require('./credit/experianApi.js');
        var body = {
            "name": "Experian",
            "street": "475 Anton Blvd",
            "city": "Costa Mesa",
            "state": "CA",
            "zip": "92626",
            "phone": "8772847942",
            "geo": true,
            "subcode": "0563736"
        }
        var experian2 = [
            {
                username: 'ramesh@cognizsoft.com',
                password: 'Cogniz@1234'
            }
        ]
        experian.experianLogin(body, experian2, function (login) {

            experian.experianApi(login, body, function (score) {
                res.send(score);
            })
        })
    })

    /*
    /*INSERT CREDIT APPLICATION DETAILS
    */
    app.post('/api/creditapplication-submit/', jwtMW, (req, res) => {
        con = require('../db');
        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        dataSql.creditSql(con, req.body, function (result) {
            if (result.status == 1) {
                var experian = require('./credit/experianApi.js');
                experian.experianLogin(req.body, result.experian, function (login) {
                    //check login success or not
                    if (login.status == 1) {
                        // get applcation score
                        experian.experianApi(login, req.body, function (score) {
                            if (score.status == 1) {
                                // check return scorss according to risk factor
                                var scroeCal = require('./credit/creditScore.js');
                                scroeCal.creditScore(con, score, result.applicationId, function (finalResult) {
                                    // save application factor
                                    dataSql.applicationScore(con, result.applicationId, finalResult.applicationStatus, finalResult.creditScroe, req.body.current_user_id)
                                    dataSql.applicationFactor(con, result.applicationId, score, req.body.current_user_id)
                                    res.send(finalResult);
                                })
                                //res.send(score);
                            } else {
                                res.send(score);
                            }
                        })

                    } else {
                        res.send(login);
                    }
                })
            } else {
                res.send(result);
            }
        });

        //var obj = {};
        //obj.status = 0;
        //obj.message = "Something wrong please try again.";
        //res.send(obj);
        //return false;
        /*const md5 = require('md5');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        let password = md5(req.body.password);
        var obj = {};*/



        //return false;
        // start Transaction



    });

    /*
    * Get all credit application
    */
    app.get('/api/creditapplication-list/', jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.creditapplicationList(con, req.query, function (result) {
            res.send(result);
        });

    });

    /*
    * Get payment plan
    */
    app.get('/api/creditapplication-plan', jwtMW, (req, res) => {
        con = require('../db');


        let date = require('date-and-time');
        var today = date.format(new Date(), 'YYYY/MM/DD');
        //return false;
        // this function use for get credit application details
        var dataSql = require('./credit/creditSql.js');
        if (req.query.loan_type == 0) {
            dataSql.budgetPlan(con, req.query.id, req.query.amount, req.query.provider_id, req.query.monthly_amount, function (result) {

                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest, newAPR: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.query.provider_id : result.rows[0].provider_id;
                    result.rows[0].provider_id = req.query.provider_id;
                    if (result.package.length !== 0) {
                        // this function use for get credit plan array
                        var scroeCal = require('./credit/creditScore.js');
                        scroeCal.getMonthlyPlanBudget(con, result.rows, req.query.procedure_date, package, req.query.monthly_amount, function (plan) {

                            if (plan.length !== 0) {


                                dataSql.providerDiscount(con, req.query.provider_id, req.query.amount, plan[0].term_month, function (discount) {

                                    var pro_discount = discount.pro_discount;

                                    let pr_dis = plan.map((e) => {
                                        /*var newAmount = 0;
                                        var filterDataPromotional = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month);
                                        if (filterDataPromotional.length > 0) {
                                            filterDataPromotional.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        } else {
                                            var filterDataStandered = pro_discount.filter(x => x.start_date == null && x.end_date == null && x.term_month == e.term_month);
                                            filterDataStandered.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        }*/
                                        e.discount_p = discount.amount
                                        return e;
                                    });

                                    plan = pr_dis

                                    var providerFile = require('./invoice/invoiceSql.js');
                                    providerFile.getProviderDetails(con, req.query.provider_id, function (providerDetails) {
                                        if (providerDetails.status == 1) {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": result.rows,
                                                "providerDetails": providerDetails.providerDetails,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": result.rows,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        }
                                    })

                                });
                            }
                        })
                    } else {
                        var obj = {
                            "status": 0,
                            "message": "Payment plan not found"
                        }
                        res.send(obj);
                    }
                } else {
                    res.send(result);
                }
            })
        } else {
            dataSql.getPlans(con, req.query.id, req.query.amount, req.query.provider_id, function (result) {


                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    result.rows[0].provider_id = req.query.provider_id;
                    if (result.package.length !== 0) {
                        // this function use for get credit plan array
                        var scroeCal = require('./credit/creditScore.js');
                        scroeCal.getMonthlyPlan(con, result.rows, req.query.procedure_date, package, function (plan) {
                            if (plan.length !== 0) {

                                dataSql.providerDiscountAll(con, req.query.provider_id, req.query.amount, function (discount) {

                                    var pro_discount = discount.pro_discount;

                                    let pr_dis = plan.map((e) => {
                                        var newAmount = 0;
                                        var filterDataPromotional = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month && x.discount_type == 'Promotional');
                                        if (filterDataPromotional.length > 0) {
                                            filterDataPromotional.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        } else {
                                            const now = new Date();
                                            let closest = Infinity;
                                            pro_discount.forEach(function (d) {
                                                const dateE = new Date(d.start_date);

                                                if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional' && e.term_month == d.term_month) {

                                                    closest = d.start_date;

                                                }


                                            });
                                            pro_discount.forEach(function (element, idx) {

                                                if (element.end_date == null && element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest) {
                                                    newAmount = (req.query.amount * element.discount_rate) / 100;
                                                } else if (element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest && element.end_date >= today) {
                                                    newAmount = (req.query.amount * element.discount_rate) / 100;
                                                }

                                            })
                                        }
                                        e.discount_p = newAmount
                                        return e;
                                    });

                                    plan = pr_dis

                                    var providerFile = require('./invoice/invoiceSql.js');
                                    providerFile.getProviderDetails(con, req.query.provider_id, function (providerDetails) {
                                        if (providerDetails.status == 1) {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": result.rows,
                                                "providerDetails": providerDetails.providerDetails,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": result.rows,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        }
                                    })

                                });
                            }
                        })
                    } else {
                        var obj = {
                            "status": 0,
                            "message": "Payment plan not found"
                        }
                        res.send(obj);
                    }
                } else {
                    res.send(result);
                }

            });
        }

        return false;


    });


    /*

    /*
    * Create plan for customer according to interast rate
    */

    app.post('/api/creditapplication-create-plan/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let now = new Date();
        req.body.current_date = date.format(now, 'MM/DD/YYYY');

        var dataSql = require('./credit/creditSql.js');
        if (req.body.details.loan_type == 0) {
            dataSql.budgetPlan(con, req.body.application_id, req.body.details.loan_amount, req.body.provider_id, req.body.details.monthly_amount, function (result) {
                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest, newAPR: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.body.provider_id : result.rows[0].provider_id;
                    result.rows[0].provider_id = req.body.provider_id;
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getMonthlyPlanBudget(con, result.rows, req.body.details.procedure_date, package, req.body.details.monthly_amount, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            // submit plan details
                            dataSql.createPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {
                                if (createStatus.status == 1) {


                                    dataSql.fetchDoctorName(con, req.body.details.doctor, function (Doctor) {
                                        if (Doctor.status == 1) {
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {

                                                    // submit plan details
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-generate-agreement');
                                                    const dir = './uploads/customer/' + result.rows[0].patient_ac;
                                                    var filename = dir + '/plan-agreement-' + result.rows[0].application_no + '-' + createStatus.paymentPlan + '.pdf';


                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    req.body.cus_info = {
                                                        account_no: result.rows[0].patient_ac,
                                                        application_no: result.rows[0].application_no,
                                                        name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name,
                                                        address: result.rows[0].address1 + ', ' + result.rows[0].City + ' ' + result.rows[0].state_name,
                                                        phone: result.rows[0].phone_no,
                                                        credit_score: result.rows[0].score,
                                                        co_patient_id: result.rows[0].co_patient_id,
                                                        co_name: result.rows[0].co_first_name + ' ' + result.rows[0].co_middle_name + ' ' + result.rows[0].co_last_name,
                                                        co_address: result.rows[0].co_address1 + ' ' + result.rows[0].co_address2 + ' ' + result.rows[0].co_City + ' ' + result.rows[0].co_state_name,
                                                        co_phone: result.rows[0].co_phone_no,
                                                    }

                                                    req.body.pro_info = {
                                                        account_no: providerDetails.providerDetails.provider_ac,
                                                        name: providerDetails.providerDetails.name,
                                                        address: providerDetails.providerDetails.address1 + ' ' + providerDetails.providerDetails.address2,
                                                        city: providerDetails.providerDetails.city,
                                                        state_name: providerDetails.providerDetails.state_name,
                                                        zip_code: providerDetails.providerDetails.zip_code,
                                                        phone: providerDetails.providerDetails.primary_phone,
                                                        doctor: req.body.details.doctor,
                                                        doctor_name: Doctor.doctor_name.f_name + ' ' + Doctor.doctor_name.l_name
                                                    }

                                                    req.body.laon_info = {
                                                        approve_amount: result.rows[0].approve_amount,
                                                        principal_amount: result.rows[0].amount,
                                                        remaining_amount: parseFloat(result.rows[0].remaining_amount) + parseFloat(result.rows[0].override_amount) - parseFloat(req.body.details.loan_amount),
                                                        loan_amount: filterType[0].totalAmount,
                                                        interest_rate: (parseFloat(filterType[0].interest_rate) / parseFloat(filterType[0].term_month) * 12).toFixed(2),
                                                        term_month: filterType[0].term_month
                                                    }

                                                    pdf.create(pdfTemplate(filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {

                                                            createStatus.obj = {
                                                                "status": 0,
                                                                "message": "Currently we are not able to print payment plan",
                                                            }
                                                            //res.send(obj);
                                                        } else {

                                                            var data = {
                                                                pp_id: createStatus.paymentPlan,
                                                                current_user_id: req.body.current_user_id,
                                                            }

                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-agreement-' + result.rows[0].application_no + '-' + data.pp_id + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.rows[0].patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //res.send(one);
                                                                        if ((one.status == 1)) {

                                                                            // sending mail tpo customer for plan agrement
                                                                            req.body.plan_details = filterType;


                                                                            //if (email_result.status == 1) {
                                                                            var emialTemp = require('./emailplan.js');

                                                                            emialTemp.customEmailTemplateDetails('Plan Agreement Generate', function (Result) {

                                                                                if (Result.status == 1) {
                                                                                    var context = {
                                                                                        customer_name: req.body.cus_info.name
                                                                                    };

                                                                                    var emailOpt = {
                                                                                        toEmail: result.rows[0].email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-generate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email sent successfully"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 1,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }

                                                                            });

                                                                            /*var locals = {
                                                                                email_data: req.body,
                                                                                filepath:path.join(__dirname, '..', filename),
                                                                            };
                                                                            var toEmailAddress = result.rows[0].email;
                        
                                                                            var template = 'plan-generate-agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/
                                                                            // end here

                                                                            curentfile.item_id = one.item_id;
                                                                            //const fs = require('fs');
                                                                            //fs.unlinkSync(curentfile.path)
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                    })
                                                                }
                                                            })
                                                            createStatus.obj = {
                                                                "status": 1,
                                                                "file_path": filename
                                                            }
                                                            //res.send(obj);
                                                        }
                                                    })





                                                    res.send(createStatus);

                                                }
                                            });
                                        } else {
                                            res.send(Doctor);
                                        }
                                    })

                                } else {
                                    res.send(createStatus);
                                }

                            });
                        } else {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found please contact with administration."
                            }
                            res.send(obj);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found please contact with administration."
                    }
                    res.send(obj);
                }
            });
        } else {
            dataSql.getPlans(con, req.body.application_id, req.body.details.loan_amount, req.body.provider_id, function (result) {
                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.body.provider_id : result.rows[0].provider_id;
                    result.rows[0].provider_id = req.body.provider_id;
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getMonthlyPlan(con, result.rows, req.body.details.procedure_date, package, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            // submit plan details
                            dataSql.createPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {

                                if (createStatus.status == 1) {


                                    dataSql.fetchDoctorName(con, req.body.details.doctor, function (Doctor) {
                                        if (Doctor.status == 1) {
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {

                                                    // submit plan details
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-generate-agreement');
                                                    const dir = './uploads/customer/' + result.rows[0].patient_ac;
                                                    var filename = dir + '/plan-agreement-' + result.rows[0].application_no + '-' + createStatus.paymentPlan + '.pdf';

                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    req.body.cus_info = {
                                                        account_no: result.rows[0].patient_ac,
                                                        application_no: result.rows[0].application_no,
                                                        name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name,
                                                        address: result.rows[0].address1 + ', ' + result.rows[0].City + ' ' + result.rows[0].state_name,
                                                        phone: result.rows[0].phone_no,
                                                        credit_score: result.rows[0].score,
                                                        co_patient_id: result.rows[0].co_patient_id,
                                                        co_name: result.rows[0].co_first_name + ' ' + result.rows[0].co_middle_name + ' ' + result.rows[0].co_last_name,
                                                        co_address: result.rows[0].co_address1 + ' ' + result.rows[0].co_address2 + ' ' + result.rows[0].co_City + ' ' + result.rows[0].co_state_name,
                                                        co_phone: result.rows[0].co_phone_no,
                                                    }

                                                    req.body.pro_info = {
                                                        account_no: providerDetails.providerDetails.provider_ac,
                                                        name: providerDetails.providerDetails.name,
                                                        address: providerDetails.providerDetails.address1 + ' ' + providerDetails.providerDetails.address2,
                                                        city: providerDetails.providerDetails.city,
                                                        state_name: providerDetails.providerDetails.state_name,
                                                        zip_code: providerDetails.providerDetails.zip_code,
                                                        phone: providerDetails.providerDetails.primary_phone,
                                                        doctor: req.body.details.doctor,
                                                        doctor_name: Doctor.doctor_name.f_name + ' ' + Doctor.doctor_name.l_name
                                                    }

                                                    req.body.laon_info = {
                                                        approve_amount: result.rows[0].approve_amount,
                                                        principal_amount: result.rows[0].amount,
                                                        remaining_amount: parseFloat(result.rows[0].remaining_amount) + parseFloat(result.rows[0].override_amount) - parseFloat(req.body.details.loan_amount),
                                                        loan_amount: filterType[0].totalAmount,
                                                        interest_rate: (filterType[0].interest_rate) ? parseFloat(filterType[0].interest_rate).toFixed(2) : '0.00',
                                                        term_month: filterType[0].term_month
                                                    }

                                                    pdf.create(pdfTemplate(filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {

                                                            createStatus.obj = {
                                                                "status": 0,
                                                                "message": "Currently we are not able to print payment plan",
                                                            }
                                                            //res.send(obj);
                                                        } else {

                                                            var data = {
                                                                pp_id: createStatus.paymentPlan,
                                                                current_user_id: req.body.current_user_id,
                                                            }

                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-agreement-' + result.rows[0].application_no + '-' + data.pp_id + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.rows[0].patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //res.send(one);
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            req.body.plan_details = filterType;


                                                                            //if (email_result.status == 1) {
                                                                            var emialTemp = require('./emailplan.js');

                                                                            emialTemp.customEmailTemplateDetails('Plan Agreement Generate', function (Result) {

                                                                                if (Result.status == 1) {
                                                                                    var context = {
                                                                                        customer_name: req.body.cus_info.name
                                                                                    };

                                                                                    var emailOpt = {
                                                                                        toEmail: result.rows[0].email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }
                                                                                    //var toEmailAddress = 'amandeep@euclidesolutions.co.in';

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {

                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 1,
                                                                                                "message": "Email sent successfully"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }
                                                                            });

                                                                            /*var locals = {
                                                                                email_data: req.body,
                                                                                filepath:path.join(__dirname, '..', filename),
                                                                            };
                                                                            var toEmailAddress = result.rows[0].email;
                        
                                                                            var template = 'plan-generate-agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/
                                                                            // end here
                                                                            curentfile.item_id = one.item_id;

                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                    })
                                                                }
                                                            })
                                                            createStatus.obj = {
                                                                "status": 1,
                                                                "file_path": filename
                                                            }
                                                            //res.send(obj);
                                                        }
                                                    })



                                                    res.send(createStatus);

                                                }
                                            });
                                        } else {
                                            res.send(Doctor);
                                        }
                                    })

                                } else {
                                    res.send(createStatus);
                                }

                            });
                        } else {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found please contact with administration."
                            }
                            res.send(obj);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found please contact with administration."
                    }
                    res.send(obj);
                }
            });
        }




    });
    /*
    * get single plan details
    */
    app.get('/api/creditapplication-singleplan-details', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        var dataSql = require('./credit/creditSql.js');
        dataSql.getSinglePlans(con, req.query.id, function (result) {
            if (result.status == 1) {
                var providerFile = require('./invoice/invoiceSql.js');
                providerFile.getProviderDetails(con, result.plan[0].provider_id, function (providerDetails) {
                    if (providerDetails.status == 1) {
                        result.providerDetails = providerDetails.providerDetails;
                        res.send(result);
                    } else {
                        res.send(result);
                    }
                })
            } else {
                res.send(result);
            }

        });

    });
    /*
    * view application
    */
    getAppliationDetails = async (req, res, next) => {
        con = require('../db');
        let dataSql = require('./credit/creditSql.js');
        let obj = {}
        obj.status = 0;

        let appDetails;
        await dataSql.getAppDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.appDetails = sResult.planDetails;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getSecDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.secDetails = sResult.secDetails;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getAddDetails(con, obj.appDetails.patient_id, 0, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.appAddress = sResult.appAddress;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getExpDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.experianAddress = sResult.experianAddress;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getExpEmpDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.experianEmployment = sResult.experianEmployment;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getBankDetails(con, obj.appDetails.patient_id, 0, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.bankDetails = sResult.bankDetails;
            }
        })
        if (appDetails == 0) { return false }



        await dataSql.getCoDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {

                obj.coDetails = sResult.coDetails;
            }
        })
        if (appDetails == 0) { return false }
        /*************Get co-signer details************/
        if (obj.coDetails != '') {

            await dataSql.getCoApp(con, obj.coDetails.patient_id, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    req.query.id = sResult.coApp;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getAppCoDetails(con, req.query.id, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CoappDetails = sResult.planDetails;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getSecDetails(con, req.query.id, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CosecDetails = sResult.secDetails;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getAddDetails(con, obj.coDetails.patient_id, 1, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CoappAddress = sResult.appAddress;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getExpDetails(con, req.query.id, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CoexperianAddress = sResult.experianAddress;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getExpEmpDetails(con, req.query.id, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CoexperianEmployment = sResult.experianEmployment;
                }
            })
            if (appDetails == 0) { return false }

            await dataSql.getBankDetails(con, obj.coDetails.patient_id, 1, function (sResult) {
                if (sResult.status == 0) {
                    appDetails = 0;
                    obj.message = "Application details not found."
                    res.send(obj);
                } else {
                    obj.CobankDetails = sResult.bankDetails;
                }
            })
            if (appDetails == 0) { return false }
        }
        /*************Get co-signer details************/
        obj.status = 1;

        res.send(obj)
    }
    app.get('/api/creditapplication-view-application', jwtMW, getAppliationDetails)



    /*
    /*INSERT CREDIT APPLICATION DETAILS
    */
    app.post('/api/creditapplication-search/', jwtMW, (req, res) => {
        con = require('../db');
        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        dataSql.searchCustomer(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
                /*var obj = {
                    "status": 0
                }
                res.send(obj);*/
            }
        });
    });


    /*
    * review application
    */
    app.get('/api/creditapplication-review-application', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        var dataSql = require('./credit/creditSql.js');
        dataSql.reviewApplcation(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });

    });

    /*
    * manual application
    */
    app.post('/api/creditapplication-manual-actions', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        // this function use for get credit application details
        var obj = {};
        var dataSql = require('./credit/creditSql.js');
        dataSql.manualAction(con, req.body, function (result) {
            if (result.status == 1) {
                dataSql.getApplicationDetails(con, req.body.id, function (result) {

                    if (result.application.email != '') {
                        var emialTemp = require('./emailplan.js');

                        var context = {
                            customer_name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                            customer_address: result.application.address1 + ' ' + result.application.address2,
                            customer_phone: result.application.phone_no,
                            account_number: result.application.patient_ac,
                            application_number: result.application.application_no,
                            approved_amount: result.application.approve_amount,
                            authorization_date: result.application.authorization_date,
                            authorization_expiration: result.application.expiry_date,
                            hps_comment: req.body.comment,
                        };

                        if (req.body.type == 1) {

                            //emialTemp.emailTemplate(toEmailAddress, locals, template);
                            //var template = 'applicationmanualapprove';
                            var template = 'Application Approval';

                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                if (Result.status == 1) {

                                    var emailOpt = {
                                        toEmail: result.application.email,
                                        subject: Result.result[0].template_subject,
                                    }

                                    var html = Result.result[0].template_content;

                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                        if (emailResult.status == 1) {
                                            var Eobj = {
                                                "status": 1,
                                                "message": "Email sent successfully"
                                            }
                                        } else {
                                            var Eobj = {
                                                "status": 0,
                                                "message": "Email not sent successfully!"
                                            }
                                        }
                                    });
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Template details not found."
                                    }
                                }
                            });

                            //NEW
                            // this code use for save document
                            const pdf = require('html-pdf');
                            const dir = './uploads/customer/' + result.application.patient_ac;
                            var filename = dir + '/application-approved-' + result.application.application_no + '.pdf';
                            //var fullUrl = req.protocol + '://' + req.get('host');
                            //var logoimg = fullUrl + '/uploads/logo.png';
                            var fullUrl = req.get('origin');
                            var logoimg = fullUrl + '/logo.png';

                            const pdfTemplate = require('./credit/applicationStatus');
                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                if (err) {
                                } else {
                                    var data = {
                                        application_id: result.application.application_id,
                                        current_user_id: req.body.current_user_id,
                                    }
                                    // onedrive
                                    const path = require('path');
                                    var curentfile = {
                                        filename: 'application-approval-' + result.application.application_no + '.pdf',
                                        path: path.join(__dirname, '..', filename),
                                        customer_id: result.application.patient_ac,
                                    }

                                    var oneDrive = require('./microsoft/oneDrive.js');
                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                        if (loginOne.status == 1) {
                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {

                                                if ((one.status == 1)) {
                                                    curentfile.item_id = one.item_id;
                                                    const fs = require('fs');
                                                    //fs.unlinkSync(curentfile.path)
                                                } else {
                                                    curentfile.item_id = '';
                                                }
                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                    //res.send(result);
                                                });
                                            })
                                        }
                                    })
                                }
                            })


                        } else if (req.body.type == 2) {
                            //var template = 'applicationreject';
                            //emialTemp.emailTemplate(toEmailAddress, locals, template);
                            var template = 'Application Reject';
                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                if (Result.status == 1) {

                                    var emailOpt = {
                                        toEmail: result.application.email,
                                        subject: Result.result[0].template_subject,
                                    }

                                    var html = Result.result[0].template_content;

                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                        if (emailResult.status == 1) {
                                            var Eobj = {
                                                "status": 1,
                                                "message": "Email sent successfully"
                                            }
                                        } else {
                                            var Eobj = {
                                                "status": 0,
                                                "message": "Email not sent successfully!"
                                            }
                                        }
                                    });
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Template details not found."
                                    }
                                }

                            });
                        } else if (req.body.type == 3) {
                            //var template = 'applicationmanualdocument';
                            //emialTemp.emailTemplate(toEmailAddress, locals, template);

                            var template = 'Application Manual Document';
                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                if (Result.status == 1) {

                                    var emailOpt = {
                                        toEmail: result.application.email,
                                        subject: Result.result[0].template_subject,
                                    }

                                    var html = Result.result[0].template_content;

                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                        if (emailResult.status == 1) {
                                            var Eobj = {
                                                "status": 1,
                                                "message": "Email sent successfully"
                                            }
                                        } else {
                                            var Eobj = {
                                                "status": 0,
                                                "message": "Email not sent successfully!"
                                            }
                                        }
                                    });
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Template details not found."
                                    }
                                }

                            });
                        }
                    }





                    //res.send(result);
                });
                /*if (result.email != '') {
                    var emialTemp = require('./email.js');
                    var locals = {
                        name: result.email.f_name + ' ' + result.email.m_name + ' ' + result.email.l_name,
                        hpscomment: req.body.comment,
                    };
                    var toEmailAddress = result.email.email;
                    if (req.body.type == 1) {
                        var template = 'applicationmanualapprove';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);
                    } else if (req.body.type == 2) {
                        var template = 'applicationreject';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);
                    } else if (req.body.type == 3) {
                        var template = 'applicationmanualdocument';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);
                    }
                }*/
                res.send(result);
            } else {
                res.send(result);
            }
        });

    });
    /*
    * view application
    */
    app.get('/api/creditapplication-reject', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        var dataSql = require('./credit/creditSql.js');
        dataSql.rejectApplcation(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                var obj = {
                    "status": 0
                }
                res.send(obj);
            }
            var obj = {
                "status": 0
            }
            //res.send(result);
        });

    });
    /*
    * upload document
    */
    app.post('/api/creditapplication-upload', dataupload, jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.getApplicationDetails(con, req.body.application_id, function (appDetails) {
            if (appDetails.status) {
                const path = require('path');
                var curentfile = {
                    filename: 'application-document-' + req.file.filename,
                    path: path.join(__dirname, '..', req.file.path),
                    customer_id: appDetails.application.patient_ac,
                }
                const dir = './uploads/customer/' + appDetails.application.patient_ac + '/';
                let distPath = dir + curentfile.filename;
                const fs = require('fs');
                fs.copyFile(curentfile.path, distPath, (err) => {
                    if (err) {
                        var obj = {
                            status: 0,
                            message: 'Something wrong please try again.'
                        }
                        res.send(obj);
                    } else {
                        var oneDrive = require('./microsoft/oneDrive.js');
                        oneDrive.getOneDriveLogin(function (loginOne) {
                            if (loginOne.status == 1) {
                                oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                    if ((one.status == 1)) {
                                        req.file.item_id = one.item_id;
                                        const fs = require('fs');
                                        fs.unlinkSync(curentfile.path)
                                    } else {
                                        req.file.item_id = '';
                                    }
                                    dataSql.uploadDocumentApplcation(con, req.body, req.file, function (result) {
                                        res.send(result);
                                    });
                                })
                            } else {
                                res.send(loginOne);
                            }
                        })
                    }
                })

            } else {
                var obj = {
                    status: 0,
                    message: 'Something wrong please try again.'
                }
                res.send(obj);
            }

        });
    });
    /*
    *
    */

    app.get('/api/creditapplication-getDocument/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        var oneDrive = require('./microsoft/oneDrive.js');
        dataSql.getDocumentApplcation(con, req.query.id, function (result) {

            if (result.status == 1) {
                let totalPlan = result.docDetails.length;
                result.docDetails.map((data, idx) => {
                    //getOneDriveFiles
                    oneDrive.getOneDriveLogin(function (loginOne) {
                        if (loginOne.status == 1) {
                            oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                if (one.status == 1) {
                                    data.file_path = one.file
                                }
                                if (0 === --totalPlan) {
                                    res.send(result);
                                }
                            })
                        }
                    })

                })

            } else {
                res.send(result);
            }
        });
    });
    /*
    * Provider area start here
    */
    /*
     * Get all credit application
     */
    app.get('/api/creditapplication-list-provider/', jwtMW, (req, res) => {
        con = require('../db');
        let cond = 1;

        if (req.query.status_id == 3) {
            cond = cond + ' AND credit_applications.status IN(4,5)';
        } else if (req.query.status_id != 0 && req.query.status_id != 1 && req.query.status_id != 2) {
            cond = cond + '';
        } else {
            cond = cond + ' AND credit_applications.status ="' + req.query.status_id + '" ';
        }


        con.query('SELECT credit_applications.application_id,credit_applications.application_no,credit_applications.patient_id,credit_applications.document_flag,credit_applications.plan_flag,credit_applications.score,credit_applications.approve_amount,DATE_FORMAT(credit_applications.expiry_date, "%m/%d/%Y") as expiry_date,credit_applications.remaining_amount,credit_applications.override_amount,credit_applications.status,patient.f_name,patient.m_name,patient.l_name,patient.gender,patient.email,patient.peimary_phone,patient.patient_ac, '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, credit_applications.application_id, master_data_values.value as status_name, '
            + '(SELECT count(pp_id) FROM payment_plan WHERE application_id=credit_applications.application_id) as plan_exists '
            + 'FROM credit_applications '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '
            + 'INNER JOIN patient_provider ON patient.patient_id=patient_provider.patient_id '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'INNER JOIN master_data_values ON master_data_values.status_id=credit_applications.status AND master_data_values.md_id=? '
            + 'WHERE credit_applications.co_signer=? AND credit_applications.delete_flag=? AND patient_provider.provider_id=? AND ' + cond + ' ORDER BY application_id DESC',
            /*+ 'WHERE credit_applications.delete_flag=? AND '+cond+' ORDER BY application_id DESC',*/
            [
                'Application Status',
                0,
                0,
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var dataSql = require('./credit/creditSql.js');
                    dataSql.providerCareCloud(con, req.query.id, function (result) {
                        if (result.status == 1) {
                            var obj = {
                                "status": 1,
                                "result": rows,
                                "careCould": result.careCould.ap_key
                            }
                            res.send(obj);
                        } else {
                            var obj = {
                                "status": 1,
                                "result": rows
                            }
                            res.send(obj);
                        }
                    });
                }
            })
    });
    /*
    /*INSERT CREDIT APPLICATION DETAILS
    */
    createAppliation = async (req, res, next) => {
        var obj = {};

        con = require('../db');
        // get experan login details
        var dataSql = require('./credit/creditSql.js');
        var experian = require('./credit/experianApi.js');
        var scroeCal = require('./credit/creditScore.js');
        let expLogin;
        await dataSql.getExperianLoginDetail(con, (exResult) => {
            if (exResult.status == 0) {
                expLogin = 0;
                res.send(exResult);
            } else {
                expLogin = exResult;
            }
        });
        // stop script
        if (expLogin == 0) { return false }

        // Check experion login details.
        let expToken;
        await experian.experianLogin(req.body, expLogin.experian, function (login) {
            if (login.status == 0) {
                expToken = 0;
                res.send(login);
            } else {
                expToken = login;
            }
        })
        // stop script
        if (expToken == 0) { return false }

        // check custoemr exits or not
        await dataSql.existCustomer(con, req.body, 0, (exResult) => {
            req.body.patient_id = exResult;
        });
        // co-signer insert
        if (req.body.co_signer == 1) {
            await dataSql.existCoCustomer(con, req.body, 1, (exResult) => {
                req.body.co_patient_id = exResult;
            });
        }

        // check customer exists or not
        let cusCreditApp;
        let cusCreditAppCo;
        if (req.body.patient_id !== undefined && req.body.patient_id != '') {

            /**************** Check profile already runing or not*****************/
            let checkExists = 1;
            await dataSql.checkAppRuning(con, req.body, (exResult) => {
                if (exResult.status == 0) {
                    checkExists = 0;
                    res.send(exResult);
                }
            });
            if (checkExists == 0) { return false }
            //planRuning
            //res.send({ status: 0 })
            //return false;
            /**************** Check profile already runing or not*****************/

            /******* co-signer code area *****************/
            let cusAddCo;
            let checkCOInsert = 0;
            if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                await dataSql.updatePatitentCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id == '') {
                checkCOInsert = 1;
                req.body.co_patient_id = null;
                await dataSql.addpatientCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
                req.body.co_patient_id = (req.body.co_signer == 1) ? cusAddCo.mainPatientId : null;
            }

            /******* co-signer code area *****************/

            let cusAdd;
            // add customer basic details
            await dataSql.updatePatitentDetails(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAdd = 0;
                    res.send(exResult);
                } else {
                    cusAdd = exResult;
                }
            });
            // stop script
            if (cusAdd == 0) { return false }



            let cusUser;
            // add customer user details
            await dataSql.updatePatientUser(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusUser = 0;
                    res.send(exResult);
                } else {
                    cusUser = exResult;
                }
            });
            // stop script
            if (cusUser == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {

                let cusUserCo;
                // add customer user details
                await dataSql.updatePatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusUserCo;
                // add customer user details
                await dataSql.addPatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            }

            /******* co-signer code area *****************/
            let cusBank;
            // add customer bank details
            await dataSql.updatePatientBank(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusBank = 0;
                    res.send(exResult);
                } else {
                    cusBank = exResult;
                }
            });
            // stop script
            if (cusBank == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {
                let cusBankCo;
                // add customer user details
                await dataSql.updatePatientBank(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusBankCo;
                // add customer user details
                await dataSql.addPatientBank(con, req.body, 1, (exResult) => {

                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            let cusAddr;
            await dataSql.updatepatientAddress(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAddr = 0;
                    res.send(exResult);
                } else {
                    cusAddr = exResult;
                }
            });

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && req.body.co_patient_id != '' && checkCOInsert == 0) {
                let cusAddr;
                // add customer user details
                await dataSql.updatepatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            } else if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusAddr;
                // add customer user details
                await dataSql.addpatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            }
            /******* co-signer code area *****************/

            // add credit application details
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            await dataSql.addCreditApp(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusCreditApp = 0;
                    res.send(exResult);
                } else {
                    cusCreditApp = exResult;
                }
            });
            // stop script
            if (cusCreditApp == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1) {
                req.body.co_signer_id = null;
                // add customer user details
                await dataSql.addCreditApp(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusCreditAppCo = 0;
                        res.send(exResult);
                    } else {
                        cusCreditAppCo = exResult;
                    }
                });
                // stop script
                if (cusCreditAppCo == 0) { return false }
            }
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            /******* co-signer code area *****************/

            let checkProviderEXITS;
            await dataSql.existProviderPatient(con, req.body.provider_id, req.body.patient_id, (exResult) => {
                if (exResult.status == 0) {
                    checkProviderEXITS = 0;
                    res.send(exResult);
                } else {
                    checkProviderEXITS = exResult;
                }
            })
            if (checkProviderEXITS == 0) { return false }
            // CHECK PROVIDER AND CUSTOMER RELATION EXISTS OR NOT
            if (checkProviderEXITS.exists == 0) {
                // add relationship
                let cusRel;
                await dataSql.addPatientRelation(con, req.body, 0, (exResult) => {
                    if (exResult.status == 0) {
                        cusRel = 0;
                        res.send(exResult);
                    } else {
                        cusRel = exResult;
                    }
                });
                // stop script
                if (cusRel == 0) { return false }
            }
        } else {

            /******* co-signer code area *****************/
            let cusAddCo;
            let checkCOInsert = 0;
            if (req.body.co_signer == 1 && req.body.co_patient_id == '') {
                checkCOInsert = 1;
                await dataSql.addpatientCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
                req.body.co_patient_id = (req.body.co_signer == 1) ? cusAddCo.mainPatientId : null;
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                await dataSql.updatePatitentCoDetails(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddCo = 0;
                        res.send(exResult);
                    } else {
                        cusAddCo = exResult;
                    }
                });
                // stop script
                if (cusAddCo == 0) { return false }
            }

            /******* co-signer code area *****************/
            let cusAdd;
            // add customer basic details
            await dataSql.addpatientDetails(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAdd = 0;
                    res.send(exResult);
                } else {
                    cusAdd = exResult;
                }
            });
            // stop script
            if (cusAdd == 0) { return false }
            //res.send({status:0})
            //return false;
            req.body.patient_id = cusAdd.mainPatientId;
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusUserCo;
                // add customer user details
                await dataSql.addPatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusUserCo;
                // add customer user details
                await dataSql.updatePatientCoUser(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusUserCo = 0;
                        res.send(exResult);
                    } else {
                        cusUserCo = exResult;
                    }
                });
                // stop script
                if (cusUserCo == 0) { return false }
            }

            /******* co-signer code area *****************/

            let cusUser;
            // add customer user details
            await dataSql.addPatientUser(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusUser = 0;
                    res.send(exResult);
                } else {
                    cusUser = exResult;
                }
            });
            // stop script
            if (cusUser == 0) { return false }

            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusBankCo;
                // add customer user details
                await dataSql.addPatientBank(con, req.body, 1, (exResult) => {

                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusBankCo;
                // add customer user details
                await dataSql.updatePatientBank(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusBankCo = 0;
                        res.send(exResult);
                    } else {
                        cusBankCo = exResult;
                    }
                });
                // stop script
                if (cusBankCo == 0) { return false }
            }

            /******* co-signer code area *****************/
            let cusBank;
            // add customer bank details
            await dataSql.addPatientBank(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusBank = 0;
                    res.send(exResult);
                } else {
                    cusBank = exResult;
                }
            });
            // stop script
            if (cusBank == 0) { return false }

            // add customer provider relationship
            let cusRel;
            await dataSql.addPatientRelation(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusRel = 0;
                    res.send(exResult);
                } else {
                    cusRel = exResult;
                }
            });
            // stop script
            if (cusRel == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1 && checkCOInsert == 1) {
                let cusAddr;
                // add customer user details
                await dataSql.addpatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            } else if (req.body.co_signer == 1 && req.body.co_patient_id != '') {
                let cusAddr;
                // add customer user details
                await dataSql.updatepatientAddress(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusAddr = 0;
                        res.send(exResult);
                    } else {
                        cusAddr = exResult;
                    }
                });
                // stop script
                if (cusAddr == 0) { return false }
            }


            /******* co-signer code area *****************/
            // add customer address details
            let cusAddr;
            await dataSql.addpatientAddress(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusAddr = 0;
                    res.send(exResult);
                } else {
                    cusAddr = exResult;
                }
            });
            // stop script
            if (cusAddr == 0) { return false }
            /******* co-signer code area *****************/
            if (req.body.co_signer == 1) {
                req.body.co_signer_id = null;
                // add customer user details
                await dataSql.addCreditApp(con, req.body, 1, (exResult) => {
                    if (exResult.status == 0) {
                        cusCreditAppCo = 0;
                        res.send(exResult);
                    } else {
                        cusCreditAppCo = exResult;
                    }
                });
                // stop script
                if (cusCreditAppCo == 0) { return false }
            }
            /******* co-signer code area *****************/

            // add credit application details
            //let cusCreditApp;
            req.body.co_signer_id = (req.body.co_signer == 1) ? req.body.co_patient_id : null;
            await dataSql.addCreditApp(con, req.body, 0, (exResult) => {
                if (exResult.status == 0) {
                    cusCreditApp = 0;
                    res.send(exResult);
                } else {
                    cusCreditApp = exResult;
                }
            });
            // stop script
            if (cusCreditApp == 0) { return false }
            /******************** */
        }

        /////////////////////////////////////
        ///////////////// REPATE CODE ///////
        /////////////////////////////////////
        /*********************
         * get and update co-signer details
         *********************/
        let expScoreDetailsCo = {};
        let expScoreCo;
        if (req.body.co_signer == 1) {

            await experian.experianApi(con, expLogin.experian[0].subscriberCode, expToken, req.body, 1, function (score) {
                if (score.status == 1) {
                    expScoreCo = score;
                } else if (score.status == 2) {
                    expScoreCo = score;
                } else {
                    expScoreCo = 0;
                    res.send(score);
                }
            })
            // stop script
            if (expScoreCo == 0) { return false }
            // get customer credit score according to experian data
            if (expScoreCo.status == 1) {
                await scroeCal.creditScore(con, expScoreCo, cusCreditApp.applicationID, req.body.employed, function (finalResult) {
                    if (finalResult.status == 0) {
                        expScoreDetailsCo = 0;
                        res.send(finalResult);
                    } else {
                        expScoreDetailsCo = finalResult;
                    }
                })
                // stop script
                if (expScoreDetailsCo == 0) { return false }
            } else {
                expScoreDetailsCo.applicationStatus = 2;
                expScoreDetailsCo.creditScroe = 0;
                expScoreDetailsCo.status = 1;
                expScoreDetailsCo.creditProfile = 2;
                expScoreDetailsCo.applicationId = cusCreditAppCo.applicationID;
                expScoreDetailsCo.message = 'Application approved for manual process.';
            }
            // add credit application details
            let cusCreditAppUpdateCo;
            await dataSql.applicationScore(con, cusCreditAppCo.applicationID, expScoreDetailsCo.applicationStatus, expScoreDetailsCo.creditScroe, req.body.current_user_id, expScoreDetailsCo, req.body.co_signer, function (done) {
                if (done.status == 0) {
                    cusCreditAppUpdateCo = 0;
                    res.send(done);
                } else {
                    cusCreditAppUpdateCo = done;
                }
            });
            // stop script
            if (cusCreditAppUpdateCo == 0) { return false }

            // add credit realtionship with co-signer
            let addCoRealtion;
            await dataSql.addCreditRelation(con, req.body, cusCreditApp.applicationID, function (done) {
                if (done.status == 0) {
                    addCoRealtion = 0;
                    res.send(done);
                } else {
                    addCoRealtion = done;
                }
            });
            // stop script
            if (addCoRealtion == 0) { return false }

        }
        /*********************
         * get and update co-signer details
         *********************/
        // get experion credit details
        let expScore;
        await experian.experianApi(con, expLogin.experian[0].subscriberCode, expToken, req.body, 0, function (score) {
            if (score.status == 1) {
                expScore = score;
            } else if (score.status == 2) {
                expScore = score;
            } else {
                expScore = 0;
                res.send(score);
            }
        })
        // stop script
        if (expScore == 0) { return false }
        let secDetails;

        await dataSql.addSecondaryDetails(con, req.body, cusCreditApp.applicationID, req.body.patient_id, function (sResult) {
            if (sResult.status == 0) {
                secDetails = 0;
                res.send(sResult);
            }
        })
        if (secDetails == 0) { return false }
        // get customer credit score according to experian data
        let expScoreDetails = {};
        if (expScore.status == 1) {
            await scroeCal.creditScore(con, expScore, cusCreditApp.applicationID, req.body.employed, function (finalResult) {
                if (finalResult.status == 0) {
                    expScoreDetails = 0;
                    res.send(finalResult);
                } else {
                    expScoreDetails = finalResult;
                }
            })
            // stop script
            if (expScoreDetails == 0) { return false }
        } else {
            expScoreDetails.applicationStatus = 2;
            expScoreDetails.creditScroe = 0;
            expScoreDetails.status = 1;
            expScoreDetails.creditProfile = 2;
            expScoreDetails.applicationId = cusCreditApp.applicationID;
            expScoreDetails.message = 'Application approved for manual process.';
        }


        if (req.body.co_signer == 1) {
            expScoreDetails.status = expScoreDetailsCo.status;
            expScoreDetails.message = expScoreDetailsCo.message;
            expScoreDetails.applicationStatus = expScoreDetailsCo.applicationStatus;
            //expScoreDetails.creditScroe = expScoreDetailsCo.creditScroe;
        }

        // add credit application details
        let cusCreditAppUpdate;
        await dataSql.applicationScore(con, cusCreditApp.applicationID, expScoreDetails.applicationStatus, expScoreDetails.creditScroe, req.body.current_user_id, expScoreDetailsCo, req.body.co_signer, function (done) {
            if (done.status == 0) {
                cusCreditAppUpdate = 0;
                res.send(done);
            } else {
                cusCreditAppUpdate = done;
            }
        });
        // stop script
        if (cusCreditAppUpdate == 0) { return false }
        // get application details
        //dataSql.getApplicationDetails(con, result.applicationId, function (result) {
        let cusAppDetails;
        await dataSql.getApplicationDetails(con, cusCreditApp.applicationID, function (getDetails) {
            if (getDetails.status == 0) {
                cusAppDetails = 0;
                res.send(getDetails);
            } else {
                cusAppDetails = getDetails;
            }
        });
        // stop script
        if (cusAppDetails == 0) { return false }
        /*
        * Create pdf document according to application status
        */
        const pdf = require('html-pdf');
        const dir = './uploads/customer/' + cusAppDetails.application.patient_ac;
        var filename = dir + '/application-' + cusAppDetails.application.application_no + '.pdf';
        var fullUrl = req.get('origin');
        var logoimg = fullUrl + '/logo.png';
        // set pdf according to application status
        const path = require('path');
        var curentfile = {
            path: path.join(__dirname, '..', filename),
            customer_id: cusAppDetails.application.patient_ac,
        }
        let pdfCreateStatus;
        let templateEmail;
        if (expScoreDetails.applicationStatus == 1) {
            templateEmail = 'Application Approval';
            curentfile.filename = 'application-approval-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);


        } else if (expScoreDetails.applicationStatus == 2) {
            templateEmail = 'Application Manual';
            curentfile.filename = 'application-manual-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationManualStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);


        } else if (expScoreDetails.applicationStatus == 0) {
            templateEmail = 'Application Decline';
            curentfile.filename = 'application-declined-' + cusAppDetails.application.application_no + '.pdf';
            const pdfTemplate = require('./credit/applicationDeclinedStatus');
            const createPDF = (cusAppDetails, logoimg, filename) => new Promise(((resolve, reject) => {
                pdf.create(pdfTemplate(cusAppDetails, logoimg), {}).toFile(filename, (err, buffer) => {
                    if (err !== null) { reject(0); }
                    else { resolve(1); }
                });
            }));

            pdfCreateStatus = await createPDF(cusAppDetails, logoimg, filename);
            await dataSql.deleteBank(con, cusCreditApp.applicationID, (exResult) => {

            });

        }
        // stop script
        if (pdfCreateStatus == 0) { return false }
        var repoData = {
            application_id: cusAppDetails.application.application_id,
            current_user_id: req.body.current_user_id,
        }

        var oneDrive = require('./microsoft/oneDrive.js');
        //oneDrive.getOneDriveLogin(function (loginOne) {
        let oneDriveLogin;
        await oneDrive.getOneDriveLogin(function (loginOne) {
            if (loginOne.status == 0) {
                oneDriveLogin = 0;
                res.send(getDetails);
            } else {
                oneDriveLogin = loginOne;
            }
        });
        // stop script
        if (oneDriveLogin == 0) { return false }
        let driveuploadStatus;
        await oneDrive.getOneDriveUpload(oneDriveLogin.token, curentfile, function (one) {
            //res.send(one);
            if ((one.status == 1)) {
                curentfile.item_id = one.item_id;
                const fs = require('fs');
                //fs.unlinkSync(curentfile.path)
            } else {
                driveuploadStatus = 0;
                curentfile.item_id = '';
                one.message = 'Currently can\'t upload on drive. Please try again'
                res.send(one);
            }
        })
        // stop script
        if (driveuploadStatus == 0) { return false }
        //dataSql.getApplicationDetails(con, result.applicationId, function (result) {
        let cusRepoDetails;
        await dataSql.documentApplcation(con, repoData, curentfile, function (result) {
            if (result.status == 0) {
                cusRepoDetails = 0;
                res.send(result);
            } else {
                cusRepoDetails = result;
            }
        });
        if (cusRepoDetails == 0) { return false }
        // Finaly get email templete for customer
        var emialTemp = require('./emailplan.js');
        let emailStatus;
        await emialTemp.customEmailTemplateDetails(templateEmail, function (emailResult) {
            if (emailResult.status == 0) {
                emailStatus = 0;
                res.send(emailResult);
            } else {
                emailStatus = emailResult;
            }
        })
        if (emailStatus == 0) { return false }

        var context = {
            customer_name: cusAppDetails.application.f_name + ' ' + cusAppDetails.application.m_name + ' ' + cusAppDetails.application.l_name,
            customer_address: cusAppDetails.application.address1 + ' ' + cusAppDetails.application.address2,
            customer_phone: cusAppDetails.application.phone_no,
            account_number: cusAppDetails.application.patient_ac,
            application_number: cusAppDetails.application.application_no,
            approved_amount: cusAppDetails.application.approve_amount,
            authorization_date: cusAppDetails.application.authorization_date,
            authorization_expiration: cusAppDetails.application.expiry_date
        };
        var emailOpt = {
            toEmail: cusAppDetails.application.email,
            subject: emailStatus.result[0].template_subject,
        }
        // save credit experion risk factor or customer details
        dataSql.applicationFactor(con, cusCreditApp.applicationID, expScore, req.body.current_user_id)
        if (req.body.co_signer == 1) {
            dataSql.applicationFactor(con, cusCreditAppCo.applicationID, expScoreCo, req.body.current_user_id)
        }
        var html = emailStatus.result[0].template_content;
        // finally send mail to customer
        emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {

            if (emailResult.status == 1) {
                var Eobj = {
                    "status": 1,
                    "message": "Email sent successfully"
                }
            } else {
                var Eobj = {
                    "status": 0,
                    "message": "Email not sent successfully!"
                }
            }
        });

        //console.log(cusCreditAppUpdate)
        res.send(expScoreDetails)
        //console.log(cusAppDetails)
        //res.send({ status: 0 })
        return false;
        //updatePatitentDetails

    }
    app.post('/api/creditapplication-submit-provider/', jwtMW, createAppliation);
    editAppliation = async (req, res, next) => {

        var obj = {};
        con = require('../db');
        // get experan login details
        var dataSql = require('./credit/creditSql.js');
        let cusAdd;
        // add customer basic details
        await dataSql.updatePatitentDetails(con, req.body, 0, (exResult) => {
            if (exResult.status == 0) {
                cusAdd = 0;
                res.send(exResult);
            } else {
                cusAdd = exResult;
            }
        });
        // stop script
        if (cusAdd == 0) { return false }

        let cusUser;
        // add customer user details
        await dataSql.updatePatientUser(con, req.body, 0, (exResult) => {
            if (exResult.status == 0) {
                cusUser = 0;
                res.send(exResult);
            } else {
                cusUser = exResult;
            }
        });
        // stop script
        if (cusUser == 0) { return false }

        let cusBank;
        // add customer bank details
        await dataSql.updatePatientBank(con, req.body, 0, (exResult) => {
            if (exResult.status == 0) {
                cusBank = 0;
                res.send(exResult);
            } else {
                cusBank = exResult;
            }
        });
        // stop script
        if (cusBank == 0) { return false }

        let cusAddr;
        await dataSql.updatepatientAddress(con, req.body, 0, (exResult) => {
            if (exResult.status == 0) {
                cusAddr = 0;
                res.send(exResult);
            } else {
                cusAddr = exResult;
            }
        });
        // stop script
        if (cusAddr == 0) { return false }

        let updateSec;
        await dataSql.updateSecondaryDetails(con, req.body, (exResult) => {
            if (exResult.status == 0) {
                updateSec = 0;
                res.send(exResult);
            } else {
                updateSec = exResult;
            }
        })
        // stop script
        if (updateSec == 0) { return false }

        /*****************************/
        if (req.body.co_patient_id != undefined || req.body.co_patient_id == '') {
            let cusAddCo;
            await dataSql.updatePatitentCoDetails(con, req.body, 1, (exResult) => {
                if (exResult.status == 0) {
                    cusAddCo = 0;
                    res.send(exResult);
                } else {
                    cusAddCo = exResult;
                }
            });
            // stop script
            if (cusAddCo == 0) { return false }



            let cusUserCo;
            // add customer user details
            await dataSql.updatePatientCoUser(con, req.body, 1, (exResult) => {
                if (exResult.status == 0) {
                    cusUserCo = 0;
                    res.send(exResult);
                } else {
                    cusUserCo = exResult;
                }
            });
            // stop script
            if (cusUserCo == 0) { return false }


            let cusBankCo;
            // add customer user details
            await dataSql.updatePatientBank(con, req.body, 1, (exResult) => {
                if (exResult.status == 0) {
                    cusBankCo = 0;
                    res.send(exResult);
                } else {
                    cusBankCo = exResult;
                }
            });
            // stop script
            if (cusBankCo == 0) { return false }


            let cusAddr;
            // add customer user details
            await dataSql.updatepatientAddress(con, req.body, 1, (exResult) => {
                if (exResult.status == 0) {
                    cusAddr = 0;
                    res.send(exResult);
                } else {
                    cusAddr = exResult;
                }
            });
            // stop script
            if (cusAddr == 0) { return false }
        }
        /*****************************/

        obj.status = 1;
        obj.message = 'Application details updated successfully.';
        obj.applicationId = req.body.app_id;
        res.send(obj)
    }
    app.post('/api/creditapplication-edit-appliaction/', jwtMW, editAppliation);

    app.post('/api/creditapplication-submit-provider2/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};

        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        //dataSql.creditSql(con, req.body, function (result) {
        dataSql.getExperianLoginDetail(con, function (exResult) {
            if (exResult.status == 1) {
                var experian = require('./credit/experianApi.js');
                experian.experianLogin(req.body, exResult.experian, function (login) {
                    //check login success or not
                    if (login.status == 1) {
                        // get applcation score
                        //experian.experianApi(con, exResult.experian[0].subscriberCode, login, req.body, function (score) {
                        //if (score.status == 1) {

                        //save application if all details are correct
                        dataSql.creditSql(con, req.body, function (result) {

                            if (result.status == 1) {

                                experian.experianApi(con, exResult.experian[0].subscriberCode, login, req.body, function (score) {

                                    if (score.status == 1) {

                                        // check return scorss according to risk factor
                                        var scroeCal = require('./credit/creditScore.js');
                                        scroeCal.creditScore(con, score, result.applicationId, req.body.employed, function (finalResult) {

                                            // save application factor
                                            dataSql.applicationScore(con, result.applicationId, finalResult.applicationStatus, finalResult.creditScroe, req.body.current_user_id, function (done) {
                                                dataSql.getApplicationDetails(con, result.applicationId, function (result) {
                                                    if (result.application.email != '') {
                                                        var emialTemp = require('./emailplan.js');

                                                        var context = {
                                                            customer_name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                                                            customer_address: result.application.address1 + ' ' + result.application.address2,
                                                            customer_phone: result.application.phone_no,
                                                            account_number: result.application.patient_ac,
                                                            application_number: result.application.application_no,
                                                            approved_amount: result.application.approve_amount,
                                                            authorization_date: result.application.authorization_date,
                                                            authorization_expiration: result.application.expiry_date
                                                        };
                                                        //var toEmailAddress = result.application.email;
                                                        if (finalResult.applicationStatus == 1) {
                                                            var template = 'Application Approval';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;

                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 1,
                                                                                "message": "Email sent successfully"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        } else if (finalResult.applicationStatus == 0) {
                                                            var template = 'Application Decline';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;
                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 1,
                                                                                "message": "Email sent successfully"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        } else if (finalResult.applicationStatus == 2) {
                                                            var template = 'Application Manual';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.application.email,
                                                                        subject: Result.result[0].template_subject,
                                                                    }

                                                                    var html = Result.result[0].template_content;
                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        if (emailResult.status == 1) {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email sent successfully!"
                                                                            }
                                                                        } else {
                                                                            var Eobj = {
                                                                                "status": 0,
                                                                                "message": "Email not sent successfully!"
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Template details not found."
                                                                    }
                                                                }

                                                            });
                                                        }

                                                        // this code use for save document
                                                        const pdf = require('html-pdf');
                                                        const dir = './uploads/provider/' + req.body.provider_id;
                                                        var filename = dir + '/application-' + result.application.application_no + '.pdf';
                                                        //var fullUrl = req.protocol + '://' + req.get('host');
                                                        //var logoimg = fullUrl + '/uploads/logo.png';
                                                        var fullUrl = req.get('origin');
                                                        var logoimg = fullUrl + '/logo.png';

                                                        if (finalResult.applicationStatus == 1) {
                                                            const pdfTemplate = require('./credit/applicationStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-approval-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })


                                                                }
                                                            })
                                                        } else if (finalResult.applicationStatus == 0) {

                                                            const pdfTemplate = require('./credit/applicationDeclinedStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-declined-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        } else if (finalResult.applicationStatus == 2) {

                                                            const pdfTemplate = require('./credit/applicationManualStatus');
                                                            pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                                if (err) {
                                                                } else {
                                                                    var data = {
                                                                        application_id: result.application.application_id,
                                                                        current_user_id: req.body.current_user_id,
                                                                    }
                                                                    const path = require('path');
                                                                    var curentfile = {
                                                                        filename: 'application-manual-' + result.application.application_no + '.pdf',
                                                                        path: path.join(__dirname, '..', filename),
                                                                        customer_id: result.application.patient_ac,
                                                                    }
                                                                    var oneDrive = require('./microsoft/oneDrive.js');
                                                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                                                        if (loginOne.status == 1) {
                                                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                                //res.send(one);
                                                                                if ((one.status == 1)) {
                                                                                    curentfile.item_id = one.item_id;
                                                                                    const fs = require('fs');
                                                                                    fs.unlinkSync(curentfile.path)
                                                                                } else {
                                                                                    curentfile.item_id = '';
                                                                                }
                                                                                dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                                    //res.send(result);
                                                                                });
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }

                                                    }
                                                    //res.send(result);
                                                });
                                            })
                                            dataSql.applicationFactor(con, result.applicationId, score, req.body.current_user_id)

                                            res.send(finalResult);
                                        })

                                    } else if (score.status == 2) {
                                        /////////////\\\\\\\\
                                        obj.status = 1;
                                        obj.creditProfile = 2;
                                        obj.creditScroe = 0;
                                        obj.applicationId = result.applicationId;
                                        obj.applicationStatus = 2;
                                        obj.message = "Application approved for manual process.";

                                        dataSql.applicationScore(con, result.applicationId, 2, 0, req.body.current_user_id, function (done) {
                                            dataSql.getApplicationDetails(con, result.applicationId, function (result) {
                                                if (result.application.email != '') {
                                                    //var emialTemp = require('./email.js');
                                                    /*var locals = {
                                                        name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                                                        address: result.application.address1 + ' ' + result.application.address2,
                                                        cityState: result.application.City + ', ' + result.application.name + ' - ' + result.application.zip_code,
                                                        phone: result.application.phone_no,
                                                        accountNumber: result.application.patient_ac,
                                                        applicationNumber: result.application.application_no,
                                                        amountApprove: result.application.approve_amount,
                                                        authDate: result.application.authorization_date,
                                                        expDate: result.application.expiry_date,
                                                    };
                                                    var toEmailAddress = result.application.email;
                                                 
                                                    var template = 'applicationmanualemail';
                                                    emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                                                    var context = {
                                                        customer_name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                                                        customer_address: result.application.address1 + ' ' + result.application.address2,
                                                        customer_phone: result.application.phone_no,
                                                        account_number: result.application.patient_ac,
                                                        application_number: result.application.application_no,
                                                        approved_amount: result.application.approve_amount,
                                                        authorization_date: result.application.authorization_date,
                                                        authorization_expiration: result.application.expiry_date
                                                    };

                                                    var emialTemp = require('./emailplan.js');
                                                    var template = 'Application Manual';

                                                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                        if (Result.status == 1) {

                                                            var emailOpt = {
                                                                toEmail: result.application.email,
                                                                subject: Result.result[0].template_subject,
                                                            }

                                                            var html = Result.result[0].template_content;
                                                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                if (emailResult.status == 1) {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Email sent successfully!"
                                                                    }
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Email not sent successfully!"
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            var Eobj = {
                                                                "status": 0,
                                                                "message": "Template details not found."
                                                            }
                                                        }

                                                    });


                                                    // this code use for save document
                                                    const pdf = require('html-pdf');
                                                    const dir = './uploads/provider/' + req.body.provider_id;
                                                    var filename = dir + '/application-' + result.application.application_no + '.pdf';
                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    //if (finalResult.applicationStatus == 2) {

                                                    const pdfTemplate = require('./credit/applicationManualStatus');
                                                    pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                        } else {
                                                            var data = {
                                                                application_id: result.application.application_id,
                                                                current_user_id: req.body.current_user_id,
                                                            }
                                                            // onedrive
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'application-manual-' + result.application.application_no + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.application.patient_ac,
                                                            }

                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {

                                                                        if ((one.status == 1)) {
                                                                            curentfile.item_id = one.item_id;
                                                                            const fs = require('fs');
                                                                            fs.unlinkSync(curentfile.path)
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        dataSql.documentApplcation(con, data, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                    //}

                                                }
                                                //res.send(result);
                                            });
                                        })
                                        //dataSql.applicationFactor(con, result.applicationId, score, req.body.current_user_id)

                                        res.send(obj);
                                        //return callback(obj);
                                        //////////\\\\\
                                    } else {
                                        res.send(score);
                                    }
                                })
                            } else {
                                res.send(result);
                            }
                        })

                        //} else {
                        //res.send(score);
                        //}
                        //})

                    } else {
                        res.send(login);
                    }
                })
            } else {
                res.send(exResult);
            }
        });
    });

    /*
    * UPload credit application agrement
    */
    /*
     * upload document
     */
    app.post('/api/creditapplication-agrement', dataupload, jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.uploadAgrementApplcation(con, req.body, req.file, function (result) {
            res.send(result);
        });
    });

    /*
    * UPload reject credit application agrement
    */

    app.post('/api/creditapplication-rejection-agrement', dataupload, jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        var obj = {};

        //return false

        con.query('SELECT application_no FROM credit_applications WHERE application_id = ?'
            , [req.body.application_id]
            , function (error, appId) {
                if (error) {
                    obj.status = 0;
                    obj.message = "Application not found";
                } else {

                    var data = {
                        current_user_id: req.body.current_user_id,
                        application_id: appId[0].application_no,
                    }

                    const path = require('path');
                    var curentfile = {
                        filename: 'application-declined-' + appId[0].application_no + '-' + req.file.filename,
                        path: path.join(__dirname, '..', req.file.path),
                        customer_id: appId[0].application_no,
                    }
                    var oneDrive = require('./microsoft/oneDrive.js');
                    oneDrive.getOneDriveLogin(function (loginOne) {
                        if (loginOne.status == 1) {
                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                if ((one.status == 1)) {
                                    req.file.item_id = one.item_id;
                                    const fs = require('fs');
                                    fs.unlinkSync(curentfile.path)
                                } else {
                                    req.file.item_id = '';
                                }
                                /*dataSql.uploadDocumentApplcation(con, req.body, req.file, function (result) {
                                    res.send(result);
                                });*/
                                dataSql.uploadRejectionAgrementApplcation(con, req.body, req.file, function (result) {
                                    res.send(result);
                                });
                            })
                        } else {
                            res.send(loginOne);
                        }
                    })

                }
            });

    });

    // download agrement

    app.post('/api/download-agreement', dataupload, jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.downloadAgrementApplcation(con, req.body, function (result) {

            res.send(result);
        });
    });

    /*
    /*INSERT CREDIT APPLICATION DETAILS
    */
    app.post('/api/customerapplication-search/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.searchApplication(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
                /*var obj = {
                    "status": 0
                }
                res.send(obj);*/
            }
        });
    });


    /*
    /*GET CREDIT APPLICATION DETAILS
    */
    app.get('/api/creditapplication-details/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.applicationDetails(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
                /*var obj = {
                    "status": 0
                }
                res.send(obj);*/
            }
        });
    });

    /*
    * Print Plan
    */
    app.post('/api/print-plan', dataupload, jwtMW, (req, res) => {
        con = require('../db');
        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        dataSql.getPlans(con, req.body.application_id, req.body.loan_amount, req.body.provider_id, function (result) {
            if (result.status == 1) {

                var package;
                if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                    var man = [];
                    man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest })
                    package = man;
                } else {
                    package = result.package
                }

                result.rows[0].provider_id = req.body.provider_id;
                var scroeCal = require('./credit/creditScore.js');
                scroeCal.getMonthlyPlan(con, result.rows, req.body.procedure_date, package, function (plan) {
                    // check paln avaibale or not
                    if (plan.length !== 0) {
                        const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                        var providerFile = require('./invoice/invoiceSql.js');
                        providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                            if (providerDetails.status == 1) {
                                // submit plan details
                                const pdf = require('html-pdf');


                                const pdfTemplate = require('./credit/printPlan');

                                const dir = './uploads/provider/' + req.body.provider_id + '/' + Math.random()
                                //var fullUrl = req.protocol + '://' + req.get('host');
                                var fullUrl = req.get('origin');
                                var logoimg = fullUrl + '/logo.png';


                                var filename = dir + 'print_plan.pdf';

                                pdf.create(pdfTemplate(result.rows, filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {

                                    if (err) {

                                        var obj = {
                                            "status": 0,
                                            "message": "Currently we are not able to print payment plan",
                                        }
                                        res.send(obj);
                                    } else {
                                        var obj = {
                                            "status": 1,
                                            "file_path": filename
                                        }
                                        res.send(obj);
                                    }
                                })
                            } else {
                                var obj = {
                                    "status": 0,
                                    "message": "Provider information not found."
                                }
                                res.send(result);
                            }
                        })

                        /*dataSql.createPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {
                            res.send(createStatus);
                            

                        });*/
                    } else {
                        var obj = {
                            "status": 0,
                            "message": "Payment plan not found please contact with administration."
                        }
                        res.send(result);
                    }
                })
            } else {
                var obj = {
                    "status": 0,
                    "message": "Payment plan not found please contact with administration."
                }
                res.send(result);
            }
        });
        /*var dataSql = require('./credit/creditSql.js');
        dataSql.downloadAgrementApplcation(con, req.body, function (result) {

            res.send(result);
        });*/
    });

    /*
    * Expiry date
    */
    app.post('/api/app-expire-date-update/', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let exp = new Date(req.body.expiry_date)
        let expiry_date = date.format(exp, 'YYYY-MM-DD');

        let current_user_id = req.body.current_user_id;

        con.query('UPDATE credit_applications SET expiry_date=?, expiry_date_cmt=?, date_modified=?, modified_by=? WHERE application_id=?',

            [

                req.body.expiry_date, req.body.expiry_date_cmt, current_date, current_user_id, req.body.application_id

            ],

            function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0
                    }
                    res.send(obj);
                } else {
                    /*var obj = {
                        "status": 1
                    }*/

                    var dataSql = require('./credit/creditSql.js');
                    dataSql.viewApplcation(con, req.body.application_id, function (result) {
                        if (result.status == 1) {
                            res.send(result);
                        } else {
                            var obj = {
                                "status": 0
                            }
                            res.send(obj);
                        }
                        var obj = {
                            "status": 0
                        }
                    });

                    //res.send(obj);

                }
            })
    })

    /*
    * Expiry date
    */
    app.post('/api/app-overide-amount/', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;

        con.query('UPDATE credit_applications SET override_amount=?, override_amount_cmt=?, date_modified=?, modified_by=? WHERE application_id=?',

            [

                req.body.overide_amount, req.body.overide_amount_cmt, current_date, current_user_id, req.body.application_id

            ],

            function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0
                    }
                    res.send(obj);
                } else {
                    var dataSql = require('./credit/creditSql.js');
                    dataSql.viewApplcation(con, req.body.application_id, function (result) {
                        if (result.status == 1) {
                            res.send(result);
                        } else {
                            var obj = {
                                "status": 0
                            }
                            res.send(obj);
                        }
                        var obj = {
                            "status": 0
                        }
                    });

                    //res.send(obj);

                }
            })
    })

    /*
    * Estimate Plan
    */
    app.get('/api/estimate-plan', jwtMW, (req, res) => {
        con = require('../db');
        let date = require('date-and-time');
        var today = date.format(new Date(), 'YYYY/MM/DD');

        var dataSql = require('./credit/creditSql.js');
        if (req.query.loan_type == 0) {
            dataSql.budgetEstPlans(con, req.query, req.query.provider_id, req.query.monthly_amount, function (result) {

                if (result.status == 1) {



                    if (result.package.length !== 0) {
                        var scroeCal = require('./credit/creditScore.js');
                        scroeCal.getEstMonthlyPlanBudget(con, req.query, result.package, req.query.monthly_amount, function (plan) {
                            if (plan.length !== 0) {

                                dataSql.providerDiscount(con, req.query.provider_id, req.query.amount, plan[0].term_month, function (discount) {

                                    var pro_discount = discount.pro_discount;

                                    let pr_dis = plan.map((e) => {
                                        /*var newAmount = 0;
                                        var filterDataPromotional = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month);
                                        if (filterDataPromotional.length > 0) {
                                            filterDataPromotional.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        } else {
                                            var filterDataStandered = pro_discount.filter(x => x.start_date == null && x.end_date == null && x.term_month == e.term_month);
                                            filterDataStandered.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        }*/
                                        e.discount_p = discount.amount
                                        return e;
                                    });

                                    plan = pr_dis

                                    var providerFile = require('./invoice/invoiceSql.js');
                                    providerFile.getProviderDetails(con, req.query.provider_id, function (providerDetails) {
                                        if (providerDetails.status == 1) {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": req.query,
                                                "providerDetails": providerDetails.providerDetails,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": req.query,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        }
                                    })
                                });

                            }
                        })
                    } else {
                        var obj = {
                            "status": 0,
                            "message": "Payment plan not found"
                        }
                        res.send(obj);
                    }
                } else {
                    res.send(result);
                }

            });
        } else {
            dataSql.getEstPlans(con, req.query, req.query.provider_id, function (result) {

                if (result.status == 1) {
                    if (result.package.length !== 0) {
                        var scroeCal = require('./credit/creditScore.js');
                        scroeCal.getEstMonthlyPlan(con, req.query, result.package, function (plan) {
                            if (plan.length !== 0) {

                                dataSql.providerDiscountAll(con, req.query.provider_id, req.query.amount, function (discount) {

                                    var pro_discount = discount.pro_discount;

                                    let pr_dis = plan.map((e) => {
                                        var newAmount = 0;
                                        var filterDataPromotional = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month && x.discount_type == 'Promotional');
                                        if (filterDataPromotional.length > 0) {
                                            filterDataPromotional.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })
                                        } else {
                                            const now = new Date();
                                            let closest = Infinity;
                                            pro_discount.forEach(function (d) {
                                                const dateE = new Date(d.start_date);

                                                if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional' && e.term_month == d.term_month) {

                                                    closest = d.start_date;

                                                }


                                            });
                                            /*var filterDataStandered = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month && x.discount_type != 'Promotional');
                                            filterDataStandered.forEach(function (element, idx) {
                                                newAmount = newAmount + (req.query.amount * element.discount_rate) / 100;
                                            })*/
                                            pro_discount.forEach(function (element, idx) {

                                                if (element.end_date == null && element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest) {
                                                    newAmount = (req.query.amount * element.discount_rate) / 100;
                                                } else if (element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest && element.end_date >= today) {
                                                    newAmount = (req.query.amount * element.discount_rate) / 100;
                                                }

                                            })
                                        }
                                        e.discount_p = newAmount
                                        return e;
                                    });

                                    plan = pr_dis

                                    var providerFile = require('./invoice/invoiceSql.js');
                                    providerFile.getProviderDetails(con, req.query.provider_id, function (providerDetails) {
                                        if (providerDetails.status == 1) {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": req.query,
                                                "providerDetails": providerDetails.providerDetails,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": plan,
                                                "applcation_details": req.query,
                                                "provider_discount": discount,
                                            }
                                            res.send(obj);
                                        }
                                    })
                                });

                            }
                        })
                    } else {
                        var obj = {
                            "status": 0,
                            "message": "Payment plan not found"
                        }
                        res.send(obj);
                    }
                } else {
                    res.send(result);
                }

            });
        }


    });

    /*
    * application details
    */
    app.get('/api/application-details', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.getApplicationDetails(con, req.query.id, function (result) {
            res.send(result);
        });

    });
    /*
    * application email
    */
    app.get('/api/application-email', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.getApplicationDetails(con, req.query.id, function (result) {

            if (result.application.email != '') {
                var emialTemp = require('./emailplan.js');
                var context = {
                    customer_name: result.application.f_name + ' ' + result.application.m_name + ' ' + result.application.l_name,
                    customer_address: result.application.address1 + ' ' + result.application.address2,
                    customer_phone: result.application.phone_no,
                    account_number: result.application.patient_ac,
                    application_number: result.application.application_no,
                    approved_amount: result.application.approve_amount,
                    authorization_date: result.application.authorization_date,
                    authorization_expiration: result.application.expiry_date,
                    hps_comment: req.body.comment,
                };
                var toEmailAddress = result.application.email;
                if (req.query.type == 1) {
                    //var template = 'applicationauthorizedemail';
                    //emialTemp.emailTemplate(toEmailAddress, locals, template);

                    var template = 'Application Approval';

                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                        if (Result.status == 1) {

                            var emailOpt = {
                                toEmail: result.application.email,
                                subject: Result.result[0].template_subject,
                            }

                            var html = Result.result[0].template_content;
                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                if (emailResult.status == 1) {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email sent successfully!"
                                    }
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email not sent successfully!"
                                    }
                                }
                            });
                        } else {
                            var Eobj = {
                                "status": 0,
                                "message": "Template details not found."
                            }
                        }
                    });

                } else if (req.query.type == 0) {
                    //var template = 'applicationdeclinedemail';
                    //emialTemp.emailTemplate(toEmailAddress, locals, template);
                    var template = 'Application Decline';
                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                        if (Result.status == 1) {

                            var emailOpt = {
                                toEmail: result.application.email,
                                subject: Result.result[0].template_subject,
                            }

                            var html = Result.result[0].template_content;

                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                if (emailResult.status == 1) {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email sent successfully!"
                                    }
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email not sent successfully!"
                                    }
                                }
                            });
                        } else {
                            var Eobj = {
                                "status": 0,
                                "message": "Template details not found."
                            }
                        }

                    });
                } else if (req.query.type == 2) {
                    //var template = 'applicationmanualemail';
                    //emialTemp.emailTemplate(toEmailAddress, locals, template);
                    var template = 'Application Manual';
                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                        if (Result.status == 1) {

                            var emailOpt = {
                                toEmail: result.application.email,
                                subject: Result.result[0].template_subject,
                            }

                            var html = Result.result[0].template_content;

                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                if (emailResult.status == 1) {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email sent successfully!"
                                    }
                                } else {
                                    var Eobj = {
                                        "status": 0,
                                        "message": "Email not sent successfully!"
                                    }
                                }
                            });
                        } else {
                            var Eobj = {
                                "status": 0,
                                "message": "Template details not found."
                            }
                        }

                    });
                }
                var obj = {
                    status: 1,
                    message: "Email Sent successfully"
                }
                res.send(obj);
            } else {
                var obj = {
                    status: 0,
                    message: "There was a problem sending the message. Please try again later"
                }
                res.send(obj);
            }
            //res.send(result);
        });

    });

    /*
        * Print Estimate Plan
        */
    app.post('/api/print-estimate-plan', dataupload, jwtMW, (req, res) => {
        con = require('../db');
        let date = require('date-and-time');
        var today = date.format(new Date(), 'YYYY/MM/DD');
        //return false;
        // call sql function and callback for next process.
        var dataSql = require('./credit/creditSql.js');
        dataSql.getEstPlans(con, req.body, req.body.provider_id, function (result) {
            if (result.status == 1) {
                if (result.package.length !== 0) {
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getEstMonthlyPlan(con, req.body, result.package, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {

                            dataSql.providerDiscountAll(con, req.body.provider_id, req.body.amount, function (discount) {

                                var pro_discount = discount.pro_discount;

                                let pr_dis = plan.map((e) => {
                                    var newAmount = 0;
                                    var filterDataPromotional = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month && x.discount_type == 'Promotional');
                                    if (filterDataPromotional.length > 0) {
                                        filterDataPromotional.forEach(function (element, idx) {
                                            newAmount = newAmount + (req.body.amount * element.discount_rate) / 100;
                                        })
                                    } else {
                                        const now = new Date();
                                        let closest = Infinity;
                                        pro_discount.forEach(function (d) {
                                            const dateE = new Date(d.start_date);

                                            if (dateE <= now && (dateE > new Date(closest) || dateE < closest) && d.discount_type !== 'Promotional' && e.term_month == d.term_month) {

                                                closest = d.start_date;

                                            }


                                        });

                                        pro_discount.forEach(function (element, idx) {

                                            if (element.end_date == null && element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest) {
                                                newAmount = (req.body.amount * element.discount_rate) / 100;
                                            } else if (element.term_month == e.term_month && element.discount_type != 'Promotional' && element.start_date <= closest && element.end_date >= today) {
                                                newAmount = (req.body.amount * element.discount_rate) / 100;
                                            }

                                        })
                                        /*var filterDataStandered = pro_discount.filter(x => x.start_date <= today && x.end_date >= today && x.term_month == e.term_month && x.discount_type != 'Promotional');
                                        filterDataStandered.forEach(function (element, idx) {
                                            newAmount = newAmount + (req.body.amount * element.discount_rate) / 100;
                                        })*/
                                    }
                                    e.discount_p = newAmount
                                    return e;
                                });

                                plan = pr_dis

                                const filterType = plan.filter(x => x.plan_id == req.body.plan_id);


                                var providerFile = require('./invoice/invoiceSql.js');
                                providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                    if (providerDetails.status == 1) {
                                        // submit plan details
                                        const pdf = require('html-pdf');
                                        const pdfTemplate = require('./credit/printEstimatePlan');
                                        const dir = './uploads/provider/' + req.body.provider_id;
                                        var filename = dir + 'print_esimate_plan.pdf';

                                        //var fullUrl = req.protocol + '://' + req.get('host');
                                        //var logoimg = fullUrl + '/uploads/logo.png';
                                        var fullUrl = req.get('origin');
                                        var logoimg = fullUrl + '/logo.png';


                                        pdf.create(pdfTemplate(filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                            if (err) {

                                                var obj = {
                                                    "status": 0,
                                                    "message": "Currently we are not able to print payment plan",
                                                }
                                                res.send(obj);
                                            } else {
                                                var obj = {
                                                    "status": 1,
                                                    "file_path": filename
                                                }
                                                res.send(obj);
                                            }
                                        })
                                    } else {
                                        var obj = {
                                            "status": 0,
                                            "message": "Currently we are not able to print payment plan",
                                        }
                                        res.send(obj);
                                    }
                                })
                            });


                        } else {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found please contact with administration."
                            }
                            res.send(result);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found"
                    }
                    res.send(obj);
                }
            } else {
                res.send(result);
            }

        });

    });
    /*
    * Application all documents
    */
    app.get('/api/creditapplication-all-documents', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        var oneDrive = require('./microsoft/oneDrive.js');
        dataSql.getAppDocumentPlan(con, req.query.id, function (result) {

            if (result.status == 1) {
                let totalPlan = result.allDocuments.length;
                result.allDocuments.map((data, idx) => {
                    //getOneDriveFiles
                    oneDrive.getOneDriveLogin(function (loginOne) {
                        if (loginOne.status == 1) {
                            oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                if (one.status == 1) {
                                    data.file_path = one.file
                                }
                                if (0 === --totalPlan) {
                                    res.send(result);
                                }
                            })
                        }
                    })

                })

            } else {
                res.send(result);
            }

        });

    });

    app.get('/api/creditapplication-plan-documents', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        var oneDrive = require('./microsoft/oneDrive.js');
        dataSql.getplanDocumentPlan(con, req.query.id, function (result) {

            if (result.status == 1) {
                let totalPlan = result.allDocuments.length;

                if (totalPlan > 0) {
                    result.allDocuments.map((data, idx) => {
                        //getOneDriveFiles
                        oneDrive.getOneDriveLogin(function (loginOne) {
                            if (loginOne.status == 1) {
                                oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                    if (one.status == 1) {
                                        data.file_path = one.file
                                    }
                                    if (0 === --totalPlan) {
                                        res.send(result);
                                    }
                                })
                            }
                        })

                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Document not found."
                    }
                    res.send(obj);
                }

            } else {
                res.send(result);
            }

        });

    });
    app.get('/api/creditapplication-disk-documents/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT patient.patient_ac '
            + 'FROM payment_plan '
            + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '

            + 'WHERE payment_plan.pp_id=?',

            [
                req.query.id
            ]
            , function (error, rows, fields) {
                if (error || rows.length == 0) {
                    var obj = {
                        "status": 0,
                        "message": "We are not able fetch document from local disk."
                    }
                    res.send(obj);
                } else {
                    console.log(rows[0].patient_ac)
                    const fs = require('fs');
                    const path = require('path');
                    //fs.unlinkSync(curentfile.path);
                    const directory = './uploads/customer/' + rows[0].patient_ac;
                    fs.readdir(directory, (err, files) => {
                        if (err) {
                            var obj = {
                                "status": 0,
                                "message": "We are not able fetch document from local disk."
                            }
                            res.send(obj);
                        } else {
                            let localDisk = [];
                            let totalFile = files.length;
                            for (const file of files) {
                                let innerDetails = {
                                    "filename": file,
                                    "file_path": path.join(directory, file)
                                }
                                localDisk.push(innerDetails)
                                //localDisk.filename = file;
                                //localDisk.file_path = path.join(directory, file);
                                if (0 === --totalFile) {
                                    let obj = {
                                        "status": 1,
                                        "planDocuments": '',
                                        "allDocuments": localDisk,
                                    }
                                    res.send(obj);
                                }
                            }
                            //console.log(localDisk)
                            //console.log(files.length)

                        };

                    });
                }
            })

    });

    app.post('/api/creditapplication-disk-cleanup/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT patient.patient_ac '
            + 'FROM payment_plan '
            + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
            + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '

            + 'WHERE payment_plan.pp_id=?',
            [
                req.body.id
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    const fs = require('fs');
                    const path = require('path');

                    let directory = './uploads/customer/' + rows[0].patient_ac;
                    let totalFile = req.body.docs.length;
                    for (const file of req.body.docs) {
                        //var newfilename = directory + '/' + file.filename;//path.join(directory, file.filename)
                        fs.unlink(path.join(directory, file.filename), err => {
                            //if (err) throw err;
                        });



                        if (0 === --totalFile) {

                            var obj = {
                                "status": 1,
                                "message": "Disk cleanup successfully completed."
                            }
                            res.send(obj);
                        }
                    }


                    //fs.unlinkSync(curentfile.path);
                    /*const directory = './uploads/customer/' + rows[0].patient_ac;
                    fs.readdir(directory, (err, files) => {
                        if (err) {
                            var obj = {
                                "status": 0,
                                "message": "We are not able to complete disk cleanup right now."
                            }
                            res.send(obj);
                        } else {
                            for (const file of files) {
                                fs.unlink(path.join(directory, file), err => {
                                    //if (err) throw err;
                                });
                            }
                            var obj = {
                                "status": 1,
                                "message": "Disk cleanup successfully completed."
                            }
                            res.send(obj);
                        };

                    });*/
                }
            })

    });

    app.get('/api/creditapplication-verify-plan/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('UPDATE payment_plan SET verify_doc=? WHERE pp_id=?',
            [
                1, req.query.id
            ]
            , function (error, rows, fields) {
                console.log(this.sql)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something went wrong. Please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "message": "Plan verification completed."
                    }
                    res.send(obj);
                }
            })

    });


    movefilesDrive = async (req, res, next) => {
        con = require('../db');
        //return false;
        var obj = {}
        const moveFile = (data) => new Promise(((resolve, reject) => {

            con.query('SELECT patient.patient_ac '
                + 'FROM payment_plan '
                + 'INNER JOIN credit_applications ON credit_applications.application_id = payment_plan.application_id '
                + 'INNER JOIN patient ON patient.patient_id=credit_applications.patient_id '

                + 'WHERE payment_plan.pp_id=?',

                [
                    data.id
                ]
                , function (error, rows, fields) {
                    if (error) {
                        reject(0);
                        /*var obj = {
                            "status": 0,
                            "message": "Something wrong please try again."
                        }
                        res.send(obj);*/
                    } else {
                        resolve(rows[0].patient_ac);
                    }
                })
        }));

        var newResult = await moveFile(req.body);
        if (newResult) {
            var oneDrive = require('./microsoft/oneDrive.js');
            //oneDrive.getOneDriveLogin(function (loginOne) {
            let oneDriveLogin;
            await oneDrive.getOneDriveLogin(function (loginOne) {
                if (loginOne.status == 0) {
                    oneDriveLogin = 0;
                    obj.status = 0;
                    obj.message = "Something wrong please try again."
                    res.send(obj);
                } else {
                    oneDriveLogin = loginOne;
                }
            });
            if (oneDriveLogin == 0) { return false }

            var planData = {
                "pp_id": req.body.id,
                "current_user_id": req.body.current_user_id,
            }
            var dataSql = require('./credit/creditSql.js');
            let directory = './uploads/customer/' + newResult;
            const path = require('path');
            let totalFile = req.body.docs.length;
            for (const file of req.body.docs) {
                var newfilename = directory + '/' + file.filename;//path.join(directory, file.filename)
                var curentfile = {
                    path: path.join(__dirname, '..', newfilename),
                    customer_id: newResult,
                    filename: file.filename,
                }

                let driveuploadStatus;
                await oneDrive.getOneDriveUpload(oneDriveLogin.token, curentfile, function (one) {
                    if ((one.status == 1)) {
                        curentfile.item_id = one.item_id;
                    } else {
                        driveuploadStatus = 0;
                        curentfile.item_id = '';
                    }
                })


                dataSql.customerUpload(con, planData, curentfile, function (result) {
                    //res.send(result);
                });

                if (0 === --totalFile) {
                    if (driveuploadStatus == 0) {
                        obj.status = 0;
                        obj.message = 'Currently can\'t upload on drive. Please try again'
                        res.send(obj);
                        return false
                    } else {
                        obj.status = 1;
                        res.send(obj);
                        return false
                    }
                }
            }

            //res.send({ status: 1 })
        } else {
            obj.status = 0;
            obj.message = "Something wrong please try again."
            res.send(obj);
        }

    }

    app.post('/api/creditapplication-move-documents', jwtMW, movefilesDrive);

    // get document

    app.get('/api/creditapplication-download-documents', jwtMW, (req, res) => {

        var oneDrive = require('./microsoft/oneDrive.js');
        oneDrive.getOneDriveLogin(function (loginOne) {
            if (loginOne.status == 1) {
                oneDrive.getOneDriveDownload(loginOne.token, req.query.id, function (one) {
                    res.send(one);
                })
            }
        })

    });

    /*
    * send plan mail
    */
    app.post('/api/plan-email', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        var obj = {};
        var dataSql = require('./credit/creditSql.js');
        dataSql.getPlans(con, req.body.score, req.body.amount, req.body.provider_id, function (result) {
            if (result.status == 1) {
                //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.body.provider_id : result.rows[0].provider_id;

                var package;
                if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                    var man = [];
                    man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest })
                    package = man;
                } else {
                    package = result.package
                }

                result.rows[0].provider_id = req.body.provider_id;
                var scroeCal = require('./credit/creditScore.js');
                scroeCal.getMonthlyPlan(con, result.rows, req.body.procedure_date,
                    package, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            var providerFile = require('./invoice/invoiceSql.js');
                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                if (providerDetails.status == 1) {
                                    //var emialTemp = require('./email.js');
                                    var locals = {
                                        name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name,
                                        providerDetails: providerDetails.providerDetails,
                                        customerDetails: result.rows[0],
                                        plan: filterType[0],
                                    };
                                    /*var toEmailAddress = result.rows[0].email;
                                    var template = 'customerplan';
                                    emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                                    const pdf = require('html-pdf');
                                    const pdfTemplate = require('./credit/customer-plan-pdf');
                                    const dir = './uploads/provider/' + req.body.provider_id;
                                    var filename = dir + '/customer-plan.pdf';

                                    var fullUrl = req.get('origin');
                                    var logoimg = fullUrl + '/logo.png';

                                    pdf.create(pdfTemplate(locals, logoimg), {}).toFile(filename, (err) => {
                                        if (err) {

                                            obj.plan = {
                                                "status": 0,
                                                "message": "Currently we are not able to print payment plan",
                                            }
                                            //res.send(obj);
                                        } else {

                                            const path = require('path');
                                            var curentfile = {
                                                filename: 'customer-plan.pdf',
                                                path: path.join(__dirname, '..', filename),
                                                customer_id: req.body.provider_id,
                                            }
                                            var oneDrive = require('./microsoft/oneDrive.js');
                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                if (loginOne.status == 1) {
                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                        //res.send(one);
                                                        if ((one.status == 1)) {

                                                            var context = {
                                                                customer_name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name
                                                            }

                                                            var emialTemp = require('./emailplan.js');
                                                            var template = 'Customer Plan';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: result.rows[0].email,
                                                                        subject: Result.result[0].template_subject,
                                                                        attachment_name: 'customer-plan.pdf',
                                                                        attachment: path.join(__dirname, '..', filename)
                                                                    }

                                                                    var html = Result.result[0].template_content;

                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        const fs = require('fs');
                                                                        fs.unlinkSync(curentfile.path);
                                                                        if (emailResult.status == 1) {
                                                                            var obj = {
                                                                                status: 1,
                                                                                message: "Email Sent successfully"
                                                                            }
                                                                            res.send(obj);
                                                                        } else {
                                                                            var obj = {
                                                                                status: 1,
                                                                                message: "Email not Sent successfully!"
                                                                            }
                                                                            res.send(obj);
                                                                        }
                                                                    });
                                                                } else {
                                                                    var obj = {
                                                                        status: 1,
                                                                        message: "Template details not found."
                                                                    }
                                                                    res.send(obj);
                                                                }

                                                            });

                                                            //curentfile.item_id = one.item_id;
                                                        } else {
                                                            curentfile.item_id = '';
                                                        }

                                                    })
                                                }
                                            })
                                            /*obj.file = {
                                                "status": 1,
                                                "file_path": filename
                                            }*/
                                            //res.send(obj);
                                        }
                                    })


                                } else {
                                    var obj = {
                                        "status": 0,
                                        "message": "There was a problem sending the message. Please try again later"
                                    }
                                    res.send(result);
                                }
                            })

                            /*dataSql.createPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {
                                res.send(createStatus);
                                
    
                            });*/
                        } else {
                            var obj = {
                                "status": 0,
                                "message": "There was a problem sending the message. Please try again later"
                            }
                            res.send(result);
                        }
                    })
            } else {
                var obj = {
                    "status": 0,
                    "message": "There was a problem sending the message. Please try again later"
                }
                res.send(result);
            }
        });
    });

    app.post('/api/plan-est-email', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        var dataSql = require('./credit/creditSql.js');
        req.body.amount = req.body.loan_amount;
        dataSql.getEstPlans(con, req.body, req.body.provider_id, function (result) {

            if (result.status == 1) {
                if (result.package.length !== 0) {
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getEstMonthlyPlan(con, req.body, result.package, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            var providerFile = require('./invoice/invoiceSql.js');
                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                if (providerDetails.status == 1) {

                                    //var emialTemp = require('./email.js');
                                    var locals = {
                                        name: req.body.email_address,
                                        providerDetails: providerDetails.providerDetails,
                                        bodydata: req.body,
                                        plan: filterType[0],
                                    };
                                    /*var toEmailAddress = req.body.email_address;
                                    var template = 'estimateplan';
                                    emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                                    const pdf = require('html-pdf');
                                    const pdfTemplate = require('./credit/customer-estimate-plan-pdf');
                                    const dir = './uploads/provider/' + req.body.provider_id;
                                    var filename = dir + '/customer-plan-estimate.pdf';

                                    var fullUrl = req.get('origin');
                                    var logoimg = fullUrl + '/logo.png';

                                    pdf.create(pdfTemplate(locals, logoimg), {}).toFile(filename, (err) => {
                                        if (err) {

                                            obj.plan = {
                                                "status": 0,
                                                "message": "Currently we are not able to print payment plan",
                                            }
                                            //res.send(obj);
                                        } else {

                                            const path = require('path');
                                            var curentfile = {
                                                filename: 'customer-plan-estimate.pdf',
                                                path: path.join(__dirname, '..', filename),
                                                customer_id: req.body.provider_id,
                                            }
                                            var oneDrive = require('./microsoft/oneDrive.js');
                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                if (loginOne.status == 1) {
                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                        //res.send(one);
                                                        if ((one.status == 1)) {

                                                            var context = {
                                                                customer_name: req.body.email_address
                                                            }

                                                            var emialTemp = require('./emailplan.js');
                                                            var template = 'Customer Plan Estimate';

                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                if (Result.status == 1) {

                                                                    var emailOpt = {
                                                                        toEmail: req.body.email_address,
                                                                        subject: Result.result[0].template_subject,
                                                                        attachment_name: 'customer-plan-estimate.pdf',
                                                                        attachment: path.join(__dirname, '..', filename)
                                                                    }

                                                                    var html = Result.result[0].template_content;

                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                        const fs = require('fs');
                                                                        fs.unlinkSync(curentfile.path);
                                                                        if (emailResult.status == 1) {
                                                                            var obj = {
                                                                                status: 1,
                                                                                message: "Email Sent successfully!"
                                                                            }
                                                                            res.send(obj);
                                                                        } else {
                                                                            var obj = {
                                                                                status: 0,
                                                                                message: "Email not sent successfully!"
                                                                            }
                                                                            res.send(obj);
                                                                        }
                                                                    });
                                                                } else {
                                                                    var obj = {
                                                                        status: 0,
                                                                        message: "Template details not found."
                                                                    }
                                                                    res.send(obj);
                                                                }

                                                            });

                                                            //curentfile.item_id = one.item_id;
                                                        } else {
                                                            curentfile.item_id = '';
                                                        }

                                                    })
                                                }
                                            })
                                        }
                                    })
                                } else {
                                    var obj = {
                                        "status": 0,
                                        "message": "There was a problem sending the message. Please try again later",
                                    }
                                    res.send(obj);
                                }
                            })

                        } else {
                            var obj = {
                                "status": 0,
                                "message": "There was a problem sending the message. Please try again later"
                            }
                            res.send(result);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "There was a problem sending the message. Please try again later"
                    }
                    res.send(obj);
                }
            } else {
                res.send(result);
            }

        });
    });
    // get doctoers according to location
    app.get('/api/creditapplication-doctors/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        con.query('SELECT id, CONCAT(f_name," ",l_name) as name '
            + 'FROM doctors '
            + 'WHERE location_id=? AND provider_id=? AND status=?',
            [
                req.query.id,
                req.query.provider_id,
                1,
            ]
            , function (error, doctors) {
                if (error) {
                    con.rollback(function () {
                        obj.status = 0;
                        obj.message = "Something wrong please try again.";
                        res.send(obj);
                    });
                } else {
                    obj.doctors = doctors;
                    obj.status = 1;
                    res.send(obj);
                }
            });


    })

    //unlock application
    app.post('/api/application-unlock/', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;

        con.query('UPDATE credit_applications SET status=?, date_modified=?, modified_by=? WHERE application_id=?',

            [

                1, current_date, current_user_id, req.body.app_id

            ],

            function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0
                    }
                    res.send(obj);
                } else {
                    var dataSql = require('./credit/creditSql.js');
                    dataSql.viewApplcation(con, req.body.app_id, function (result) {
                        if (result.status == 1) {
                            res.send(result);
                        } else {
                            var obj = {
                                "status": 0
                            }
                            res.send(obj);
                        }
                        var obj = {
                            "status": 1
                        }
                    });

                    //res.send(obj);

                }
            })
    })

    /**
     * this api use for refetch creditapplication details.
     */
    app.get('/api/creditapplication-cir', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        /**
         * Get experian details.
         * Take action according to status
         */
        dataSql.getExperianLoginDetail(con, function (exResult) {
            if (exResult.status == 1) {
                var experian = require('./credit/experianApi.js');
                /**
                 * Get experian login.
                 * Take action according to status
                 */
                experian.experianLogin(req.body, exResult.experian, function (login) {
                    if (login.status == 1) {
                        /**
                         * Get application details.
                         * Take action according to status.
                         * Set new object according to result.
                         */
                        dataSql.viewApplcation(con, req.query.id, function (result) {
                            if (result.status == 1) {
                                var data = {
                                    location: result.appAddress,
                                    dob: result.appDetails.dob,
                                    last_name: result.appDetails.l_name,
                                    first_name: result.appDetails.f_name,
                                    ssn: result.appDetails.ssn,
                                }

                                /**
                                 * Call experian CIR api for get customer details.
                                 * Take action according to status
                                 */
                                experian.experianApi(con, exResult.experian[0].subscriberCode, login, data, 0, function (score) {
                                    if (score.status == 1) {
                                        var scroeCal = require('./credit/creditScore.js');
                                        /**
                                         * Check experian score and descide action action
                                         * Take action according to status
                                         * Set application status manual beacuse this application already in manual.
                                         * Admin will action on application according to credit score.
                                         */
                                        scroeCal.creditScore(con, score, req.query.id, result.appDetails.employment_status, function (finalResult) {
                                            finalResult.applicationStatus = 2;
                                            /**
                                             * Update experian details in application.
                                             * Take action according to status
                                             */
                                            dataSql.applicationScore(con, req.query.id, finalResult.applicationStatus, finalResult.creditScroe, req.query.current_user_id, '', 0, function (done) {
                                                if (done.status == 1) {
                                                    dataSql.applicationFactor(con, req.query.id, score, req.query.current_user_id)
                                                    /**
                                                     * Get current application details and send back on font end.
                                                     * Take action according to status
                                                     * Set custom message according experian
                                                     */
                                                    dataSql.reviewApplcation(con, req.query.id, function (result) {
                                                        if (result.status == 1) {
                                                            result.message = 'Experian CIR completed and details updated automatically. Now you take any action according.'
                                                            res.send(result);
                                                        } else {
                                                            result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                                            res.send(result);
                                                        }
                                                    });
                                                } else {
                                                    /**
                                                     * Get current application details and send back on font end.
                                                     * Take action according to status
                                                     * Set custom message according experian
                                                     */
                                                    dataSql.reviewApplcation(con, req.query.id, function (result) {
                                                        if (result.status == 1) {
                                                            result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                                            res.send(result);
                                                        } else {
                                                            result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                                            res.send(result);
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    } else {
                                        /**
                                         * Get current application details and send back on font end.
                                         * Take action according to status
                                         * Set custom message according experian
                                         */
                                        dataSql.reviewApplcation(con, req.query.id, function (result) {
                                            if (result.status == 1) {
                                                result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                                res.send(result);
                                            } else {
                                                result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                                res.send(result);
                                            }
                                        });
                                    }
                                })
                            } else {
                                /**
                                 * Get current application details and send back on font end.
                                 * Take action according to status
                                 * Set custom message according experian
                                 */
                                dataSql.reviewApplcation(con, req.query.id, function (result) {
                                    if (result.status == 1) {
                                        result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                        res.send(result);
                                    } else {
                                        result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                        res.send(result);
                                    }
                                });
                            }
                        })
                    } else {
                        /**
                         * Get current application details and send back on font end.
                         * Take action according to status
                         * Set custom message according experian
                         */
                        dataSql.reviewApplcation(con, req.query.id, function (result) {
                            if (result.status == 1) {
                                result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                res.send(result);
                            } else {
                                result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                                res.send(result);
                            }
                        });
                    }
                });
            } else {
                /**
                 * Get current application details and send back on font end.
                 * Take action according to status
                 * Set custom message according experian
                 */
                dataSql.reviewApplcation(con, req.query.id, function (result) {
                    if (result.status == 1) {
                        result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                        res.send(result);
                    } else {
                        result.message = 'Experian CIR completed but we are not able to complete your request at this time. Please try again or now you take any action according.'
                        res.send(result);
                    }
                });
            }
        })
    });

    //resend application documents
    app.post('/api/resend-creditapplication-documents', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        var obj = {};
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;

        con.query('SELECT '
            + 'credit_applications.patient_id, '
            + 'patient.email, '
            + 'patient.f_name, '
            + 'patient.l_name '

            + 'FROM credit_applications '

            + 'INNER JOIN patient '
            + 'ON credit_applications.patient_id = patient.patient_id '

            + 'WHERE credit_applications.application_id = ? '
            + 'AND credit_applications.status = ?'
            ,
            [

                req.body.app_id, 1

            ],

            function (err, result) {
                if (err) {
                    obj.status = 0
                } else {
                    var oneDrive = require('./microsoft/oneDrive.js');
                    obj.app_data = result
                    obj.doc_data = req.body.docs
                    obj.status = 1

                    var file_path = [];
                    let totalPlan = req.body.docs.length;

                    var locals = [{}];
                    req.body.docs.map((data, idx) => {
                        oneDrive.getOneDriveLogin(function (loginOne) {
                            if (loginOne.status == 1) {

                                oneDrive.getOneDriveFilesShareLink(loginOne.token, data.item_id, function (one) {
                                    if (one.status == 1) {

                                        locals[idx] = {};
                                        locals[idx]['filename'] = data.name;
                                        locals[idx]['content'] = one.bo;

                                        file_path[idx] = one.file
                                        //obj.message = "Email sent successfully!"
                                    }
                                    if (0 === --totalPlan) {




                                        var emialTemp = require('./emailplan.js');
                                        var template = 'Resend Application Documents';

                                        var context = {
                                            customer_name: result[0].f_name + ' ' + result[0].l_name,
                                        };

                                        emialTemp.customEmailTemplateDetails(template, function (Result) {

                                            if (Result.status == 1) {

                                                var emailOpt = {
                                                    toEmail: result[0].email,
                                                    subject: Result.result[0].template_subject,
                                                    attachments: locals
                                                }

                                                var html = Result.result[0].template_content;

                                                emialTemp.customEmailTemplateShare(emailOpt, context, html, function (emailResult) {
                                                    if (emailResult.status == 1) {
                                                        var Eobj = {
                                                            "status": 0,
                                                            "message": "Email sent successfully!"
                                                        }
                                                    } else {
                                                        var Eobj = {
                                                            "status": 0,
                                                            "message": "Email not sent successfully!"
                                                        }
                                                    }
                                                });
                                            } else {
                                                var Eobj = {
                                                    "status": 0,
                                                    "message": "Template details not found."
                                                }
                                            }

                                        });

                                        res.send(obj);
                                    }
                                })
                            }
                        })

                    })


                    //res.send(obj);

                }
                //res.send(obj);
            })
    })

    /*
    * Create plan for customer according to interast rate
    */

    app.post('/api/creditapplication-create-print-plan/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let now = new Date();
        req.body.current_date = date.format(now, 'MM/DD/YYYY');

        var dataSql = require('./credit/creditSql.js');
        if (req.body.details.loan_type == 0) {
            dataSql.budgetPlan(con, req.body.application_id, req.body.details.loan_amount, req.body.provider_id, req.body.details.monthly_amount, function (result) {
                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest, newAPR: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.body.provider_id : result.rows[0].provider_id;
                    result.rows[0].provider_id = req.body.provider_id;
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getMonthlyPlanBudget(con, result.rows, req.body.details.procedure_date, package, req.body.details.monthly_amount, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            // submit plan details
                            dataSql.createPrintPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {
                                if (createStatus.status == 1) {


                                    dataSql.fetchDoctorName(con, req.body.details.doctor, function (Doctor) {
                                        if (Doctor.status == 1) {
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {

                                                    // submit plan details
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-generate-agreement');
                                                    const dir = './uploads/customer/' + result.rows[0].patient_ac;
                                                    var filename = dir + '/plan-agreement-' + result.rows[0].application_no + '-' + createStatus.paymentPlan + '.pdf';

                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    req.body.cus_info = {
                                                        account_no: result.rows[0].patient_ac,
                                                        application_no: result.rows[0].application_no,
                                                        name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name,
                                                        address: result.rows[0].address1 + ', ' + result.rows[0].City + ' ' + result.rows[0].state_name,
                                                        phone: result.rows[0].phone_no,
                                                        credit_score: result.rows[0].score,
                                                        co_patient_id: result.rows[0].co_patient_id,
                                                        co_name: result.rows[0].co_first_name + ' ' + result.rows[0].co_middle_name + ' ' + result.rows[0].co_last_name,
                                                        co_address: result.rows[0].co_address1 + ' ' + result.rows[0].co_address2 + ' ' + result.rows[0].co_City + ' ' + result.rows[0].co_state_name,
                                                        co_phone: result.rows[0].co_phone_no,
                                                    }

                                                    req.body.pro_info = {
                                                        account_no: providerDetails.providerDetails.provider_ac,
                                                        name: providerDetails.providerDetails.name,
                                                        address: providerDetails.providerDetails.address1 + ' ' + providerDetails.providerDetails.address2,
                                                        city: providerDetails.providerDetails.city,
                                                        state_name: providerDetails.providerDetails.state_name,
                                                        zip_code: providerDetails.providerDetails.zip_code,
                                                        phone: providerDetails.providerDetails.primary_phone,
                                                        doctor: req.body.details.doctor,
                                                        doctor_name: Doctor.doctor_name.f_name + ' ' + Doctor.doctor_name.l_name
                                                    }

                                                    req.body.laon_info = {
                                                        approve_amount: result.rows[0].approve_amount,
                                                        principal_amount: result.rows[0].amount,
                                                        remaining_amount: parseFloat(result.rows[0].remaining_amount) + parseFloat(result.rows[0].override_amount) - parseFloat(req.body.details.loan_amount),
                                                        loan_amount: filterType[0].totalAmount,
                                                        //interest_rate: (parseFloat(filterType[0].interest_rate) / parseFloat(filterType[0].term_month) * 12).toFixed(2),
                                                        interest_rate: (filterType[0].interest_rate) ? parseFloat(filterType[0].interest_rate).toFixed(2) : '0.00',
                                                        term_month: filterType[0].term_month
                                                    }

                                                    pdf.create(pdfTemplate(filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {

                                                            createStatus.obj = {
                                                                "status": 0,
                                                                "message": "Currently we are not able to print payment plan",
                                                            }
                                                            //res.send(obj);
                                                        } else {

                                                            var data = {
                                                                pp_id: createStatus.paymentPlan,
                                                                current_user_id: req.body.current_user_id,
                                                                application_id: result.rows[0].application_id,
                                                            }

                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-agreement-' + result.rows[0].application_no + '-' + data.pp_id + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.rows[0].patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //res.send(one);
                                                                        if ((one.status == 1)) {

                                                                            // sending mail tpo customer for plan agrement
                                                                            req.body.plan_details = filterType;


                                                                            //if (email_result.status == 1) {
                                                                            var emialTemp = require('./emailplan.js');

                                                                            emialTemp.customEmailTemplateDetails('Plan Agreement Generate', function (Result) {

                                                                                if (Result.status == 1) {
                                                                                    var context = {
                                                                                        customer_name: req.body.cus_info.name
                                                                                    };

                                                                                    var emailOpt = {
                                                                                        toEmail: result.rows[0].email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-generate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email sent successfully!"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }

                                                                            });

                                                                            /*var locals = {
                                                                                email_data: req.body,
                                                                                filepath:path.join(__dirname, '..', filename),
                                                                            };
                                                                            var toEmailAddress = result.rows[0].email;
                        
                                                                            var template = 'plan-generate-agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/
                                                                            // end here


                                                                            dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                                //res.send(result);
                                                                            });

                                                                            curentfile.item_id = one.item_id;

                                                                            createStatus.file = filename
                                                                            createStatus.file_status = 1
                                                                            res.send(createStatus);
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                            createStatus.obj = {
                                                                "status": 1,
                                                                "file_path": filename
                                                            }
                                                            //res.send(obj);
                                                        }
                                                    })


                                                }
                                            });
                                        } else {
                                            res.send(Doctor);
                                        }
                                    })

                                } else {
                                    res.send(createStatus);
                                }

                            });
                        } else {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found please contact with administration."
                            }
                            res.send(obj);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found please contact with administration."
                    }
                    res.send(obj);
                }
            });
        } else {
            dataSql.getPlans(con, req.body.application_id, req.body.details.loan_amount, req.body.provider_id, function (result) {
                if (result.status == 1) {
                    var package;
                    if (result.rows[0].manual_term != null && result.rows[0].manual_interest != null) {
                        var man = [];
                        man.push({ id: result.rows[0].manual_term, term_month: result.rows[0].manual_term, interest_rate: result.rows[0].manual_interest })
                        package = man;
                    } else {
                        package = result.package
                    }
                    //result.rows[0].provider_id = (result.rows[0].provider_id == null) ? req.body.provider_id : result.rows[0].provider_id;
                    result.rows[0].provider_id = req.body.provider_id;
                    var scroeCal = require('./credit/creditScore.js');
                    scroeCal.getMonthlyPlan(con, result.rows, req.body.details.procedure_date, package, function (plan) {
                        // check paln avaibale or not
                        if (plan.length !== 0) {
                            const filterType = plan.filter(x => x.plan_id == req.body.plan_id);
                            // submit plan details
                            dataSql.createPrintPlan(con, result.rows, filterType, req.body.current_user_id, req.body, function (createStatus) {
                                if (createStatus.status == 1) {


                                    dataSql.fetchDoctorName(con, req.body.details.doctor, function (Doctor) {
                                        if (Doctor.status == 1) {
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, req.body.provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {

                                                    // submit plan details
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-generate-agreement');
                                                    const dir = './uploads/customer/' + result.rows[0].patient_ac;
                                                    var filename = dir + '/plan-agreement-' + result.rows[0].application_no + '-' + createStatus.paymentPlan + '.pdf';


                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    req.body.cus_info = {
                                                        account_no: result.rows[0].patient_ac,
                                                        application_no: result.rows[0].application_no,
                                                        name: result.rows[0].f_name + ' ' + result.rows[0].m_name + ' ' + result.rows[0].l_name,
                                                        address: result.rows[0].address1 + ', ' + result.rows[0].City + ' ' + result.rows[0].state_name,
                                                        phone: result.rows[0].phone_no,
                                                        credit_score: result.rows[0].score,
                                                        co_patient_id: result.rows[0].co_patient_id,
                                                        co_name: result.rows[0].co_first_name + ' ' + result.rows[0].co_middle_name + ' ' + result.rows[0].co_last_name,
                                                        co_address: result.rows[0].co_address1 + ' ' + result.rows[0].co_address2 + ' ' + result.rows[0].co_City + ' ' + result.rows[0].co_state_name,
                                                        co_phone: result.rows[0].co_phone_no,
                                                    }

                                                    req.body.pro_info = {
                                                        account_no: providerDetails.providerDetails.provider_ac,
                                                        name: providerDetails.providerDetails.name,
                                                        address: providerDetails.providerDetails.address1 + ' ' + providerDetails.providerDetails.address2,
                                                        city: providerDetails.providerDetails.city,
                                                        state_name: providerDetails.providerDetails.state_name,
                                                        zip_code: providerDetails.providerDetails.zip_code,
                                                        phone: providerDetails.providerDetails.primary_phone,
                                                        doctor: req.body.details.doctor,
                                                        doctor_name: Doctor.doctor_name.f_name + ' ' + Doctor.doctor_name.l_name
                                                    }

                                                    req.body.laon_info = {
                                                        approve_amount: result.rows[0].approve_amount,
                                                        principal_amount: result.rows[0].amount,
                                                        remaining_amount: parseFloat(result.rows[0].remaining_amount) + parseFloat(result.rows[0].override_amount) - parseFloat(req.body.details.loan_amount),
                                                        loan_amount: filterType[0].totalAmount,
                                                        interest_rate: (filterType[0].interest_rate) ? parseFloat(filterType[0].interest_rate).toFixed(2) : '0.00',
                                                        term_month: filterType[0].term_month
                                                    }

                                                    pdf.create(pdfTemplate(filterType, req.body, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {

                                                            createStatus.obj = {
                                                                "status": 0,
                                                                "message": "Currently we are not able to print payment plan",
                                                            }
                                                            //res.send(obj);
                                                        } else {

                                                            var data = {
                                                                pp_id: createStatus.paymentPlan,
                                                                current_user_id: req.body.current_user_id,
                                                                application_id: result.rows[0].application_id,
                                                            }

                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-agreement-' + result.rows[0].application_no + '-' + data.pp_id + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: result.rows[0].patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //res.send(one);
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            req.body.plan_details = filterType;


                                                                            //if (email_result.status == 1) {
                                                                            var emialTemp = require('./emailplan.js');

                                                                            emialTemp.customEmailTemplateDetails('Plan Agreement Generate', function (Result) {

                                                                                if (Result.status == 1) {
                                                                                    var context = {
                                                                                        customer_name: req.body.cus_info.name
                                                                                    };

                                                                                    var emailOpt = {
                                                                                        toEmail: result.rows[0].email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }
                                                                                    //var toEmailAddress = 'amandeep@euclidesolutions.co.in';

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {

                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email sent successfully!"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }
                                                                            });

                                                                            /*var locals = {
                                                                                email_data: req.body,
                                                                                filepath:path.join(__dirname, '..', filename),
                                                                            };
                                                                            var toEmailAddress = result.rows[0].email;
                        
                                                                            var template = 'plan-generate-agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/
                                                                            // end here
                                                                            dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                                //res.send(result);
                                                                            });

                                                                            curentfile.item_id = one.item_id;

                                                                            createStatus.file = filename
                                                                            createStatus.file_status = 1
                                                                            res.send(createStatus);

                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                            createStatus.obj = {
                                                                "status": 1,
                                                                "file_path": filename
                                                            }

                                                        }
                                                    })




                                                }
                                            });
                                        } else {
                                            res.send(Doctor);
                                        }
                                    })

                                } else {
                                    res.send(createStatus);
                                }

                            });
                        } else {
                            var obj = {
                                "status": 0,
                                "message": "Payment plan not found please contact with administration."
                            }
                            res.send(obj);
                        }
                    })
                } else {
                    var obj = {
                        "status": 0,
                        "message": "Payment plan not found please contact with administration."
                    }
                    res.send(obj);
                }
            });
        }




    });
    /*
   * upload plan agreement view details
   */
    app.get('/api/creditapplication-plan-agreement-view-details', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details
        con.query('SELECT payment_plan.plan_number, '
            + 'payment_plan.term, '
            + 'payment_plan.discounted_interest_rate, '
            + 'payment_plan.application_id, '
            + 'payment_plan.loan_amount, '
            + 'payment_plan.pp_id, '
            + 'payment_plan.plan_status, '

            + 'credit_applications.application_no, '
            + 'credit_applications.approve_amount, '
            + 'credit_applications.score, '

            + 'patient.patient_ac '

            + 'FROM payment_plan '

            + 'INNER JOIN credit_applications '
            + 'ON payment_plan.application_id = credit_applications.application_id '

            + 'INNER JOIN patient '
            + 'ON credit_applications.patient_id = patient.patient_id '

            + 'WHERE payment_plan.pp_id = ?'
            , [req.query.planid]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0
                    }
                    res.send(obj);
                } else {
                    res.send(result)
                }

            });

    });

    /*
    * upload plan agreement document
    */
    app.post('/api/upload-plan-agreement', dataupload, jwtMW, (req, res) => {
        //return false 
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        const path = require('path');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var curentfile = {
            filename: 'plan-agreement-' + req.body.application_no + '-' + req.body.pp_id + '-' + req.file.filename,
            path: path.join(__dirname, '..', req.file.path),
            customer_id: req.body.customer_id,
        }
        var oneDrive = require('./microsoft/oneDrive.js');
        oneDrive.getOneDriveLogin(function (loginOne) {
            if (loginOne.status == 1) {
                oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                    if ((one.status == 1)) {
                        req.file.item_id = one.item_id;
                        const fs = require('fs');
                        fs.unlinkSync(curentfile.path)
                    } else {
                        req.file.item_id = '';
                    }
                    dataSql.uploadPlanAgreementDocument(con, req.body, req.file, function (result) {
                        if (result.status == 1) {
                            con.query('UPDATE payment_plan SET '
                                + 'plan_status = ?, '
                                + 'modified_by = ?, '
                                + 'date_created = ? '
                                + 'WHERE pp_id = ?'
                                ,
                                [
                                    3,
                                    req.body.current_user_id,
                                    current_date,
                                    req.body.pp_id
                                ]
                                , function (error, plan) {
                                    if (error) {
                                        var error = "Something wrong!"
                                        res.send(error);
                                    } else {
                                        result.plan = plan
                                        res.send(result);
                                    }
                                })
                        } else {
                            res.send(result);
                        }
                    });
                })
            } else {
                res.send(loginOne);
            }
        })

    });

    /*
    * review plan agreement document detail
    */
    app.get('/api/review-plan-agreement-detail', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        var oneDrive = require('./microsoft/oneDrive.js');
        dataSql.getPlanAgreement(con, req.query.id, function (result) {
            if (result.status == 1) {
                let totalPlan = result.docDetails.length;
                result.docDetails.map((data, idx) => {
                    //getOneDriveFiles
                    oneDrive.getOneDriveLogin(function (loginOne) {
                        if (loginOne.status == 1) {
                            oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                if (one.status == 1) {
                                    data.file_path = one.file
                                }
                                if (0 === --totalPlan) {
                                    res.send(result);
                                }
                            })
                        }
                    })

                })

            } else {
                res.send(result);
            }
        });
    });

    /*
    * manual application
    */
    app.post('/api/plan-reject-approve-actions', jwtMW, (req, res) => {
        con = require('../db');
        //return false;
        // this function use for get credit application details
        var obj = {};
        var dataSql = require('./credit/creditSql.js');
        dataSql.planManualAction(con, req.body, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });

    });

    /*
    * provider plan list
    */
    app.get('/api/provider-plan-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT payment_plan.pp_id, '
            + 'payment_plan.term, '
            + 'payment_plan.discounted_interest_rate, '
            + 'payment_plan.loan_amount, '
            + 'payment_plan.plan_number, '
            + 'payment_plan.application_id, '
            + 'payment_plan.plan_status, '

            + 'credit_applications.application_no, '

            + 'patient.f_name, '
            + 'patient.l_name, '
            + 'patient.m_name, '
            + 'patient.patient_ac, '

            + 'patient_address.address1, '
            + 'patient_address.City, '
            + 'patient_address.phone_no, '

            + 'master_data_values.value AS status_name, '

            + 'DATE_FORMAT(patient_procedure.procedure_date, "%m/%d/%Y") AS procedure_date, '
            + 'patient_procedure.procedure_amt '

            + 'FROM payment_plan '

            + 'INNER JOIN credit_applications '
            + 'ON credit_applications.application_id=payment_plan.application_id '

            + 'INNER JOIN patient '
            + 'ON patient.patient_id=credit_applications.patient_id '

            + 'INNER JOIN patient_address '
            + 'ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address = 1 '

            + 'INNER JOIN master_data_values '
            + 'ON master_data_values.status_id = payment_plan.plan_status '

            + 'INNER JOIN patient_procedure '
            + 'ON patient_procedure.pp_id = payment_plan.pp_id '

            + 'WHERE payment_plan.provider_id = ? '
            + 'AND payment_plan.plan_status = ? AND patient_address.status = ? AND master_data_values.md_id=?'
            ,
            [
                req.query.id,
                4,
                1,
                'Customer Plan Status'
            ]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });


    app.post('/api/provider-submit-plan', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./invoice/admininvoiceSql.js');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        con.query('UPDATE payment_plan SET '
            + 'plan_status = ?, '
            + 'modified_by = ?, '
            + 'date_created = ? '
            + 'WHERE pp_id IN (?) ',
            [
                1,
                req.body.current_user_id,
                current_date,
                req.body.pp_id,
            ],
            function (err, plan, fields) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    dataSql.invoiceApplication(con, req.body.pp_id, function (appInfo) {
                        let moment = require('moment');
                        if (appInfo.status == 1) {

                            let planDetails = appInfo.result;

                            var dataInvoice = require('./payment/payment.sql.js');
                            let planLenght = planDetails.length;
                            planDetails.map((nextInvoice, idx) => {
                                if (moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('DD') > 15) {
                                    var newDate = moment(nextInvoice.procedure_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                    newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
                                    nextDueDate = newDate;
                                } else {
                                    var newDate = new Date(moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('YYYY'), moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('MM'), 0);
                                    nextDueDate = newDate;
                                }
                                nextDueDate = moment(nextDueDate).format('YYYY-MM-DD');
                                var newData = {
                                    appid: nextInvoice.application_id,
                                    date: nextDueDate,
                                    current_user_id: req.body.current_user_id,
                                    pp_id: nextInvoice.pp_id,
                                }

                                dataInvoice.createInvoiceOnFlay(con, newData, function (invoice) {

                                })
                            })
                            var obj = {
                                "status": 1,
                                "result": plan
                            }
                            res.send(obj);
                        } else {
                            var obj = {
                                "status": 1,
                                "result": plan
                            }
                            res.send(obj);
                        }
                    });

                }
            })
    })

    //Download plan agreement
    app.post('/api/download-plan-agreement', jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        con.query('SELECT doc_repo.item_id, '
            + 'doc_repo.file_path, '
            + 'patient.patient_ac '

            + 'FROM doc_repo '

            + 'INNER JOIN payment_plan '
            + 'ON payment_plan.pp_id = doc_repo.pp_id '

            + 'INNER JOIN credit_applications '
            + 'ON credit_applications.application_id = payment_plan.application_id '

            + 'INNER JOIN patient '
            + 'ON patient.patient_id = credit_applications.patient_id '

            + 'WHERE doc_repo.pp_id=? AND doc_repo.agrement_flag=? AND doc_repo.status=? AND doc_repo.delete_flag=? ',
            [
                req.body.pp_id,
                1,
                1,
                0
            ], function (err, docDetail) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    if (docDetail.length > 0) {

                        if (docDetail[0].item_id) {
                            var oneDrive = require('./microsoft/oneDrive.js');
                            oneDrive.getOneDriveLogin(function (loginOne) {
                                if (loginOne.status == 1) {
                                    oneDrive.getOneDriveFilesShareLink(loginOne.token, docDetail[0].item_id, function (one) {
                                        const fs = require('fs');
                                        const path = require('path');
                                        var mkdirp = require('mkdirp');
                                        var getDirName = require('path').dirname;

                                        var dir = './uploads/customers/' + docDetail[0].patient_ac;
                                        var filename = dir + '/' + Math.random() + 'plan_generate_agreement.pdf';
                                        var file_path = path.join(__dirname, '..', filename)




                                        mkdirp(getDirName(file_path), function (err) {
                                            if (err) {
                                                var obj = {
                                                    "status": 0,
                                                    "message": "Something wrong please try again.2"
                                                }
                                                res.send(obj);
                                            } else {
                                                fs.writeFile(file_path, one.bo, 'binary', function (err) {

                                                    if (err) {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Something wrong please try again.3"
                                                        }
                                                        res.send(obj);
                                                    } else {

                                                        one.patient_ac = docDetail[0].patient_ac
                                                        one.pp_id = req.body.pp_id
                                                        one.file_path = filename
                                                        res.send(one);
                                                    }
                                                });
                                            }
                                        });



                                    })
                                }
                            })
                        } else {
                            con.query('SELECT DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS date_created, '

                                + 'payment_plan.loan_amount, '
                                + 'payment_plan.amount, '
                                + 'payment_plan.term, '
                                + 'payment_plan.discounted_interest_rate, '

                                + 'provider.provider_ac, '
                                + 'provider.name AS provider_name, '
                                + 'provider.primary_phone AS provider_phone, '

                                + 'provider_location.address1, '
                                + 'provider_location.city, '
                                + 'provider_location.zip_code, '
                                + 'provider_location.state, '

                                + 'states.name AS state, '

                                + 'patient_provider.patient_id, '

                                + 'patient.f_name AS customer_fname, '
                                + 'patient.l_name AS customer_lname, '
                                + 'patient.patient_ac AS account_no, '

                                + 'patient_address.address1 AS customer_address, '
                                + 'patient_address.City AS customer_city, '
                                + 'patient_address.phone_no AS customer_phone, '

                                + 'patient_procedure.procedure_date, '

                                + 'doctors.f_name AS doctor_fname, '
                                + 'doctors.l_name AS doctor_lname, '

                                + 'credit_applications.approve_amount, '
                                + 'credit_applications.remaining_amount, '
                                + 'credit_applications.application_no '

                                + 'FROM payment_plan '

                                + 'INNER JOIN provider_location '
                                + 'ON provider_location.provider_id = payment_plan.provider_id '

                                + 'INNER JOIN states '
                                + 'ON states.state_id = provider_location.state '

                                + 'INNER JOIN patient_provider '
                                + 'ON patient_provider.provider_id = provider_location.provider_id '

                                + 'INNER JOIN patient '
                                + 'ON patient.patient_id = patient_provider.patient_id '

                                + 'INNER JOIN patient_address '
                                + 'ON patient_address.patient_id = patient.patient_id '
                                + 'AND patient_address.status = 1 '

                                + 'INNER JOIN provider '
                                + 'ON provider.provider_id = patient_provider.provider_id '

                                + 'INNER JOIN patient_procedure '
                                + 'ON patient_procedure.pp_id = payment_plan.pp_id '
                                + 'AND patient_procedure.provider_id = provider.provider_id '

                                + 'INNER JOIN doctors '
                                + 'ON patient_procedure.doctor_id = doctors.id '

                                + 'INNER JOIN credit_applications '
                                + 'ON credit_applications.application_id = payment_plan.application_id '

                                + 'WHERE payment_plan.pp_id=?',

                                [
                                    req.body.pp_id
                                ]
                                , function (err, agreementDetail) {
                                    if (err) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                    } else {

                                        con.query('SELECT '
                                            + 'installment_amt AS perMonth, '
                                            + 'DATE_FORMAT(due_date, "%m/%d/%Y") AS next_month '

                                            + 'FROM pp_installments '

                                            + 'WHERE pp_id = ?',
                                            [
                                                req.body.pp_id
                                            ]
                                            , function (error, installments) {
                                                if (error) {
                                                    var obj = {
                                                        "status": 0,
                                                        "message": "Something wrong please try again."
                                                    }
                                                    res.send(obj);
                                                } else {

                                                    var data = {}
                                                    data.cus_info = {
                                                        account_no: agreementDetail[0].account_no,
                                                        application_no: agreementDetail[0].application_no,
                                                        name: agreementDetail[0].customer_fname + ' ' + agreementDetail[0].customer_lname,
                                                        address: agreementDetail[0].customer_address + ' ' + agreementDetail[0].customer_city,
                                                        phone: agreementDetail[0].customer_phone
                                                    }

                                                    data.pro_info = {
                                                        account_no: agreementDetail[0].provider_ac,
                                                        name: agreementDetail[0].provider_name,
                                                        address: agreementDetail[0].address1,
                                                        city: agreementDetail[0].city,
                                                        state_name: agreementDetail[0].state,
                                                        zip_code: agreementDetail[0].zip_code,
                                                        phone: agreementDetail[0].provider_phone,
                                                        doctor_name: agreementDetail[0].doctor_fname + ' ' + agreementDetail[0].doctor_lname
                                                    }

                                                    data.laon_info = {
                                                        approve_amount: agreementDetail[0].approve_amount,
                                                        principal_amount: agreementDetail[0].loan_amount,
                                                        remaining_amount: agreementDetail[0].remaining_amount,
                                                        loan_amount: agreementDetail[0].amount,
                                                        interest_rate: parseFloat(agreementDetail[0].discounted_interest_rate).toFixed(2),
                                                        term_month: agreementDetail[0].term
                                                    }
                                                    data.current_date = agreementDetail[0].date_created
                                                    data.details = agreementDetail[0].procedure_date

                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-generate-agreement');
                                                    const dir = './uploads/customers/' + agreementDetail[0].account_no + '/' + Math.random();
                                                    var filename = dir + 'plan_generate_agreement.pdf';

                                                    //var fullUrl = req.protocol + '://' + req.get('host');
                                                    //var logoimg = fullUrl + '/uploads/logo.png';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';

                                                    var mainmonthly = {
                                                        monthlyPlan: installments
                                                    }
                                                    var planDetails = []
                                                    planDetails.push(mainmonthly)

                                                    pdf.create(pdfTemplate(planDetails, data, planDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                            var obj = {
                                                                "status": 0,
                                                                "message": "Something wrong please try again."
                                                            }
                                                            res.send(obj);
                                                        } else {

                                                            var data = {
                                                                pp_id: req.body.pp_id,
                                                                current_user_id: req.body.current_user_id,
                                                            }

                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-agreement-' + agreementDetail[0].application_no + '-' + req.body.pp_id + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: agreementDetail[0].account_no,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        //res.send(one);
                                                                        if ((one.status == 1)) {

                                                                            dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                                //res.send(result);
                                                                            });

                                                                            curentfile.item_id = one.item_id;

                                                                            data.status = 1
                                                                            data.file_path = filename
                                                                            res.send(data)

                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                        }
                                                    })


                                                }
                                            })

                                    }
                                })
                        }

                    } else {
                        con.query('SELECT DATE_FORMAT(payment_plan.date_created, "%m/%d/%Y") AS date_created, '

                            + 'payment_plan.loan_amount, '
                            + 'payment_plan.amount, '
                            + 'payment_plan.term, '
                            + 'payment_plan.discounted_interest_rate, '

                            + 'provider.provider_ac, '
                            + 'provider.name AS provider_name, '
                            + 'provider.primary_phone AS provider_phone, '

                            + 'provider_location.address1, '
                            + 'provider_location.city, '
                            + 'provider_location.zip_code, '
                            + 'provider_location.state, '

                            + 'states.name AS state, '

                            + 'patient_provider.patient_id, '

                            + 'patient.f_name AS customer_fname, '
                            + 'patient.l_name AS customer_lname, '
                            + 'patient.patient_ac AS account_no, '

                            + 'patient_address.address1 AS customer_address, '
                            + 'patient_address.City AS customer_city, '
                            + 'patient_address.phone_no AS customer_phone, '

                            + 'patient_procedure.procedure_date, '

                            + 'doctors.f_name AS doctor_fname, '
                            + 'doctors.l_name AS doctor_lname, '

                            + 'credit_applications.approve_amount, '
                            + 'credit_applications.remaining_amount, '
                            + 'credit_applications.application_no '

                            + 'FROM payment_plan '

                            + 'INNER JOIN provider_location '
                            + 'ON provider_location.provider_id = payment_plan.provider_id '

                            + 'INNER JOIN states '
                            + 'ON states.state_id = provider_location.state '

                            + 'INNER JOIN patient_provider '
                            + 'ON patient_provider.provider_id = provider_location.provider_id '

                            + 'INNER JOIN patient '
                            + 'ON patient.patient_id = patient_provider.patient_id '

                            + 'INNER JOIN patient_address '
                            + 'ON patient_address.patient_id = patient.patient_id '
                            + 'AND patient_address.status = 1 '

                            + 'INNER JOIN provider '
                            + 'ON provider.provider_id = patient_provider.provider_id '

                            + 'INNER JOIN patient_procedure '
                            + 'ON patient_procedure.pp_id = payment_plan.pp_id '
                            + 'AND patient_procedure.provider_id = provider.provider_id '

                            + 'INNER JOIN doctors '
                            + 'ON patient_procedure.doctor_id = doctors.id '

                            + 'INNER JOIN credit_applications '
                            + 'ON credit_applications.application_id = payment_plan.application_id '

                            + 'WHERE payment_plan.pp_id=?',

                            [
                                req.body.pp_id
                            ]
                            , function (err, agreementDetail) {
                                if (err) {
                                    var obj = {
                                        "status": 0,
                                        "message": "Something wrong please try again."
                                    }
                                    res.send(obj);
                                } else {

                                    con.query('SELECT '
                                        + 'installment_amt AS perMonth, '
                                        + 'DATE_FORMAT(due_date, "%m/%d/%Y") AS next_month '

                                        + 'FROM pp_installments '

                                        + 'WHERE pp_id = ?',
                                        [
                                            req.body.pp_id
                                        ]
                                        , function (error, installments) {
                                            if (error) {
                                                var obj = {
                                                    "status": 0,
                                                    "message": "Something wrong please try again."
                                                }
                                                res.send(obj);
                                            } else {

                                                var data = {}
                                                data.cus_info = {
                                                    account_no: agreementDetail[0].account_no,
                                                    application_no: agreementDetail[0].application_no,
                                                    name: agreementDetail[0].customer_fname + ' ' + agreementDetail[0].customer_lname,
                                                    address: agreementDetail[0].customer_address + ' ' + agreementDetail[0].customer_city,
                                                    phone: agreementDetail[0].customer_phone
                                                }

                                                data.pro_info = {
                                                    account_no: agreementDetail[0].provider_ac,
                                                    name: agreementDetail[0].provider_name,
                                                    address: agreementDetail[0].address1,
                                                    city: agreementDetail[0].city,
                                                    state_name: agreementDetail[0].state,
                                                    zip_code: agreementDetail[0].zip_code,
                                                    phone: agreementDetail[0].provider_phone,
                                                    doctor_name: agreementDetail[0].doctor_fname + ' ' + agreementDetail[0].doctor_lname
                                                }

                                                data.laon_info = {
                                                    approve_amount: agreementDetail[0].approve_amount,
                                                    principal_amount: agreementDetail[0].loan_amount,
                                                    remaining_amount: agreementDetail[0].remaining_amount,
                                                    loan_amount: agreementDetail[0].amount,
                                                    interest_rate: parseFloat(agreementDetail[0].discounted_interest_rate).toFixed(2),
                                                    term_month: agreementDetail[0].term
                                                }
                                                data.current_date = agreementDetail[0].date_created
                                                data.details = agreementDetail[0].procedure_date

                                                const pdf = require('html-pdf');
                                                const pdfTemplate = require('./credit/plan-generate-agreement');
                                                const dir = './uploads/customers/' + agreementDetail[0].account_no + '/' + Math.random();
                                                var filename = dir + 'plan_generate_agreement.pdf';

                                                //var fullUrl = req.protocol + '://' + req.get('host');
                                                //var logoimg = fullUrl + '/uploads/logo.png';
                                                var fullUrl = req.get('origin');
                                                var logoimg = fullUrl + '/logo.png';

                                                var mainmonthly = {
                                                    monthlyPlan: installments
                                                }
                                                var planDetails = []
                                                planDetails.push(mainmonthly)

                                                pdf.create(pdfTemplate(planDetails, data, planDetails, logoimg), {}).toFile(filename, (err) => {
                                                    if (err) {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Something wrong please try again."
                                                        }
                                                        res.send(obj);
                                                    } else {

                                                        var data = {
                                                            pp_id: req.body.pp_id,
                                                            current_user_id: req.body.current_user_id,
                                                        }

                                                        const path = require('path');
                                                        var curentfile = {
                                                            filename: 'plan-agreement-' + agreementDetail[0].application_no + '-' + req.body.pp_id + '.pdf',
                                                            path: path.join(__dirname, '..', filename),
                                                            customer_id: agreementDetail[0].account_no,
                                                        }
                                                        var oneDrive = require('./microsoft/oneDrive.js');
                                                        oneDrive.getOneDriveLogin(function (loginOne) {
                                                            if (loginOne.status == 1) {
                                                                oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                    //res.send(one);
                                                                    if ((one.status == 1)) {

                                                                        dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });

                                                                        curentfile.item_id = one.item_id;

                                                                        data.status = 1
                                                                        data.file_path = filename
                                                                        res.send(data)

                                                                    } else {
                                                                        curentfile.item_id = '';
                                                                    }
                                                                })
                                                            }
                                                        })

                                                    }
                                                })


                                            }
                                        })

                                }
                            })
                    }
                    //res.send(obj); 012KHYTUSBNBO5UVM2CJD35LWRGCP3WYXW
                }
            })


    });

    //Download doc
    app.post('/api/download-doc', jwtMW, (req, res) => {

        con = require('../db');

        var oneDrive = require('./microsoft/oneDrive.js');
        oneDrive.getOneDriveLogin(function (loginOne) {
            if (loginOne.status == 1) {
                oneDrive.getOneDriveFilesShareLink(loginOne.token, req.body.item_id, function (one) {
                    const fs = require('fs');
                    const path = require('path');
                    var mkdirp = require('mkdirp');
                    var getDirName = require('path').dirname;

                    var dir = './uploads/customers/all-downloads';
                    var filename = dir + '/' + Math.random() + one.filename + '.' + one.ext;
                    var file_path = path.join(__dirname, '..', filename)

                    mkdirp(getDirName(file_path), function (err) {
                        if (err) {
                            var obj = {
                                "status": 0,
                                "message": "Something wrong please try again.2"
                            }
                            res.send(obj);
                        } else {
                            fs.writeFile(file_path, one.bo, 'binary', function (err) {

                                if (err) {
                                    var obj = {
                                        "status": 0,
                                        "message": "Something wrong please try again.3"
                                    }
                                    res.send(obj);
                                } else {

                                    one.file_path = filename
                                    one.file_name = one.filename
                                    res.send(one);
                                }
                            });
                        }
                    });



                })
            }
        })

    });

    //Download application pdf
    app.get('/api/creditapplication-list-pdf/', jwtMW, (req, res) => {
        const pdf = require('html-pdf');
        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.creditapplicationList(con, req.query, function (result) {
            if (result.status == 1) {
                const pdfTemplate = require('./credit/applicationListPdf');

                const dir = './uploads/report/'
                //var fullUrl = req.protocol + '://' + req.get('host');
                //var logoimg = fullUrl + '/uploads/logo.png';
                var fullUrl = req.get('origin');
                var logoimg = fullUrl + '/logo.png';
                var filename = dir + 'application-list.pdf';
                pdf.create(pdfTemplate(req.query, result, logoimg), {}).toFile(filename, (err) => {
                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }
                })
            }
        });

    });

    app.get('/api/creditapplication-list-xls/', jwtMW, (req, res) => {
        con = require('../db');
        let moment = require('moment');
        const excel = require('exceljs');
        var dataSql = require('./credit/creditSql.js');
        dataSql.creditapplicationList(con, req.query, function (result) {
            if (result.status == 1) {
                const jsonData = result.result;

                let workbook = new excel.Workbook(); //creating workbook
                let worksheet = workbook.addWorksheet('Application List'); //creating worksheet

                let dataFilter = req.query;
                let fileName = '';
                if (dataFilter.filter_type == 1) {
                    moment.updateLocale('en', {
                        week: {
                            dow: 1,
                            doy: 1
                        }
                    });

                    let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
                    let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
                    fileName = 'Weelky Application List (' + startOfWeek + ' - ' + endOfWeek + ')';
                } else if (dataFilter.filter_type == 2) {
                    let monthName = moment().month(dataFilter.month).format("MMM");
                    fileName = 'Monthly Application List (' + monthName + '-' + dataFilter.year + ')';
                } else if (dataFilter.filter_type == 3) {
                    let queterName = ''
                    if (dataFilter.quarter == 1) {
                        queterName = 'Q1';
                    } else if (dataFilter.quarter == 2) {
                        queterName = 'Q2';
                    } else if (dataFilter.quarter == 3) {
                        queterName = 'Q3';
                    } else if (dataFilter.quarter == 4) {
                        queterName = 'Q4';
                    }
                    fileName = 'Quarterly Application List (' + queterName + '-' + dataFilter.year + ')';
                } else if (dataFilter.filter_type == 4) {
                    fileName = 'Yearly Application List (' + dataFilter.year + ')';
                } else if (dataFilter.filter_type == 5) {
                    let currentDate = moment().format('MM/DD/YYYY');
                    fileName = 'Year to Date Application List (01/01/' + dataFilter.year + '-' + currentDate + ')';
                } else if (dataFilter.filter_type == 6) {
                    let loanStart = moment(dataFilter.start_date).format('MM/DD/YYYY');
                    let loanEnd = moment(dataFilter.end_date).format('MM/DD/YYYY');
                    fileName = 'By Date Application List (' + loanStart + '-' + loanEnd + ')';
                } else {
                    if (dataFilter.status_id != '' && dataFilter.status_id != 'all' && jsonData.length > 0) {

                        fileName = 'Application List (' + jsonData[0].status_name + ')';
                    } else {
                        fileName = 'Application List';
                    }
                }

                worksheet.mergeCells('A1:N3');

                worksheet.getCell('A1').value = fileName;
                //worksheet.getCell('A1').alignment = { horizontal: 'center' };

                worksheet.getCell('A1').alignment = {
                    vertical: 'middle', horizontal: 'center'
                };
                //worksheet.getRow('A1').font = { size: 22 };
                worksheet.getCell('A1').font = {
                    size: 16,
                    bold: true
                };
                ['A1'].map(key => {
                    worksheet.getCell(key).fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'ABA286' },
                        bgColor: { argb: 'ABA286' },
                    };


                });

                worksheet.getCell('A5').value = "ID";
                worksheet.getCell('B5').value = "A/C No";
                worksheet.getCell('C5').value = "App No";
                worksheet.getCell('D5').value = "Full Name";
                worksheet.getCell('E5').value = "Address";
                worksheet.getCell('F5').value = "City";
                worksheet.getCell('G5').value = "State";
                worksheet.getCell('H5').value = "Primary Ph";
                worksheet.getCell('I5').value = "Status";
                worksheet.getCell('J5').value = "LOC";
                worksheet.getCell('K5').value = "Over Amt";
                worksheet.getCell('L5').value = "Avl Bal";
                worksheet.getCell('M5').value = "Date Created";
                worksheet.getCell('N5').value = "Expire Date";

                worksheet.columns = [
                    { key: 'ID', width: 10 },
                    { key: 'A/C No', width: 15 },
                    { key: 'App No', width: 15 },
                    { key: 'Full Name', width: 15 },
                    { key: 'Address', width: 15 },
                    { key: 'City', width: 15 },
                    { key: 'State', width: 10, },
                    { key: 'Primary Ph', width: 10, },
                    { key: 'Status', width: 10, },
                    { key: 'LOC', width: 10 },
                    { key: 'Over Amt', width: 15 },
                    { key: 'Avl Bal', width: 15 },
                    { key: 'Date Created', width: 15 },
                    { key: 'Expire Date', width: 10 },];

                //  WorkSheet Header
                /*worksheet.columns = [
                    { header: 'ID', key: 'ID', width: 10 },
                    { header: 'A/C No', key: 'A/C No', width: 10 },
                    { header: 'App No', key: 'App No', width: 30 },
                    { header: 'Full Name', key: 'Full Name', width: 30 },
                    { header: 'Address', key: 'Address', width: 20 },
                    { header: 'City', key: 'City', width: 30 },
                    { header: 'State', key: 'State', width: 30 },
                    { header: 'Primary Ph', key: 'Primary Ph', width: 30 },
                    { header: 'Status', key: 'Status', width: 20, },
                    { header: 'LOC', key: 'LOC', width: 20 },
                    { header: 'Over Amt', key: 'Over Amt', width: 15 },
                    { header: 'Avl Bal', key: 'Avl Bal', width: 15 },
                    { header: 'Date Created', key: 'Date Created', width: 15 },
                    { header: 'Expire Date', key: 'Expire Date', width: 15 },
                ];*/

                // Add Array Rows
                //worksheet.addRows(jsonData);
                var rowValues = [];
                var count = 6;
                jsonData.forEach(function (element, idx) {
                    var appstatus = '';

                    worksheet.getCell('A' + count).value = element.application_id;
                    worksheet.getCell('B' + count).value = element.patient_ac;
                    worksheet.getCell('C' + count).value = element.application_no;
                    worksheet.getCell('D' + count).value = element.f_name + ' ' + element.m_name + ' ' + element.l_name;
                    worksheet.getCell('E' + count).value = element.address1;
                    worksheet.getCell('F' + count).value = element.City;
                    worksheet.getCell('G' + count).value = element.state_name;
                    worksheet.getCell('H' + count).value = element.peimary_phone;
                    worksheet.getCell('I' + count).value = element.status_name;
                    worksheet.getCell('J' + count).value = '$' + parseFloat(element.approve_amount).toFixed(2);
                    worksheet.getCell('K' + count).value = '$' + parseFloat(element.override_amount).toFixed(2);
                    worksheet.getCell('L' + count).value = '$' + parseFloat(element.remaining_amount).toFixed(2);
                    worksheet.getCell('M' + count).value = element.date_created;
                    worksheet.getCell('N' + count).value = element.expiry_date;

                    count++;
                });
                // set header details
                ['A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'I5', 'J5', 'K5', 'L5', 'M5', 'N5'].map(key => {
                    worksheet.getCell(key).fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'aba2a2' },
                        bgColor: { argb: 'aba2a2' }
                    };
                });

                // Write to File
                const dir = './uploads/report/'

                var filename = dir + 'application-list.xlsx';
                workbook.xlsx.writeFile(filename)
                    .then(function () {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }).catch(function () {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    });
            }
        });
    });

    app.get('/api/cancel-refund-plan-details', jwtMW, (req, res) => {
        con = require('../db');

        var dataSql = require('./credit/creditSql.js');

        dataSql.cancelRefundDetails(con, req.query, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    app.post('/api/cancel-refund-plan-details-pdf/', jwtMW, (req, res) => {
        //res.send(req.body)
        //return false
        con = require('../db');
        const pdf = require('html-pdf');
        var dataSql = require('./credit/creditSql.js');
        dataSql.cancelRefundDetails(con, req.body, function (result) {

            //return false;
            if (result.status == 1) {

                var pdfTemplate;

                if (req.body.type == 1) {
                    pdfTemplate = require('./credit/cancelRefundDetailsPdf');
                } else {
                    pdfTemplate = require('./credit/settlementDetailsPdf');
                }

                const dir = './uploads/report/'
                //var fullUrl = req.protocol + '://' + req.get('host');
                //var logoimg = fullUrl + '/uploads/logo.png';
                var fullUrl = req.get('origin');
                var logoimg = fullUrl + '/logo.png';
                var filename = dir + 'plan-details.pdf';
                pdf.create(pdfTemplate(result, logoimg), {}).toFile(filename, (err) => {
                    if (err) {
                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "result": filename
                        }
                        res.send(obj);
                    }
                })
            }
        });
    });

    /*
    * Get all credit application
    */
    app.get('/api/creditapplication-review', jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./credit/creditSql.js');
        dataSql.creditapplicationPlanReview(con, req.query, function (result) {
            res.send(result);
        });

    });
}
