/*
* Title: Region
* Descrpation :- This module blong to regions realted api in whole application
* Date :- 23 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');

    app.post('/api/city-country-states-list/', jwtMW, (req, res) => {
        con = require('../db');
        //let sql = '';
        console.log(req.body)
        //let sql = 'SELECT name FROM states WHERE country_id=? AND status=?';
        
        
        con.query('SELECT state_id, name FROM states WHERE country_id=? AND status=?'
          , [req.body.country_id,1]
          , function (error, rows, fields) {
            
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "country_states": rows
              }
              res.send(obj);
    
            }
          })
      })

    app.get('/api/region-list/', jwtMW, (req, res) => {
        con = require('../db');
        let result = {};
        
        con.query('SELECT regions.region_id AS city_id, '
            + 'regions.country_id, '
            + 'regions.state_id, '
            + 'regions.name, '
            + 'regions.serving, '
            + 'regions.status, '

            + 'states.name AS state_name, '
            
            + 'countries.name AS country_name '
            
            + 'FROM regions '

            + 'INNER JOIN states '
            + 'ON states.state_id=regions.state_id '

            + 'INNER JOIN countries '
            + 'ON countries.id=regions.country_id '

            + 'WHERE regions.delete_flag = ? ORDER BY regions.region_id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    con.query('SELECT state_id,name FROM states WHERE status=? AND delete_flag = ?',
                        [
                            1, 0
                        ]
                        , function (error, state, fields) {
                            if (error) {
                                var obj = {
                                    "status": 0,
                                    "message": "Something wrong please try again."
                                }
                            }
                            else {
                                con.query('SELECT id,name FROM countries WHERE status=? AND deleted_flag = ?',
                                [
                                    1, 0
                                ]
                                , function (error, countries, fields) {
                                    if (error) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                    }
                                    else {
                                        result.status = 1;
                                        result.result = rows;
                                        result.state = state;
                                        result.countries = countries;
                                        res.send(result);
                                    }
                                })
                            }
                        })
                }
            })
    })


    app.post('/api/region-name-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.state_id === undefined) {
          sql = 'SELECT name FROM regions WHERE delete_flag=? AND name=?';
          var edit = 0;
        } else {
          sql = 'SELECT name FROM regions WHERE delete_flag=? AND name=? AND region_id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            0,
            req.body.name,
            req.body.state_id
          ]
          , function (error, rows, fields) {
    
            if (error) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0,
                "edit": edit
              }
              res.send(obj);
    
            }
          })
      })

    app.post('/api/region-insert/', jwtMW, (req, res) => {
        con = require('../db');
        console.log(req.body)
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO regions (country_id, state_id, name, serving, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
            [
                req.body.country_id,
                req.body.state_id,
                req.body.name,
                req.body.serving,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {
                console.log(err)
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_city_id": result.insertId,
                        "country_id": req.body.country_id,
                        "state_id": req.body.state_id,
                        "name": req.body.name,
                        "serving": req.body.serving,
                        "city_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/region-update/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('UPDATE regions SET state_id=?, country_id=?, name=?, serving=?, status=?, date_modified=?, modified_by=? WHERE region_id=?',
            [
                req.body.state_id,
                req.body.country_id,
                req.body.name,
                req.body.serving,
                req.body.status,
                current_date,
                current_user_id,
                req.body.city_id,
            ]
            , function (err, result) {
                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1
                    }
                    res.send(obj);

                }
            })
    })

}
