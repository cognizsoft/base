/*
* Title: due payment
* Descrpation :- This module belong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/

exports.deuPayment = async () => {
    let dueData;
    let chargesResult;
    await getPlanDetailsCron((result) => {
        dueData = result;
    });
    if (dueData.status == 0) { return false }
    await getCharges((result) => {
        chargesResult = result;
    })
    let moment = require('moment');
    var invoiceDate = moment().endOf('month').format("YYYY-MM-DD");
    //console.log(invoiceDate)
    var CheckPatitent = '';
    var invoiceDetails = '';
    /////////////////////////
    var lateFee = chargesResult.lateFee;
    var finCharge = chargesResult.finCharge;
    var PartialPayPCT = chargesResult.PartialPayPCT;
    /********************************/
    var checkLate = 0;
    var lateFeePatient = 0;
    var finChargePatient = 0;
    let vi = 0;
    for (x of dueData.result) {
        if (CheckPatitent == '' || CheckPatitent != x.patient_id) {
            CheckPatitent = x.patient_id;
            invoiceDetails = '';
            await getPreinvoiceDetailsCron(x.application_id, invoiceDate, 0, function (iResult) {

                dueData.result[vi].preDetails = iResult;
                invoiceDetails = iResult;
            });
        } else {
            dueData.result[vi].preDetails = invoiceDetails;
        }
        vi++;
    }
    
    let allDetails = dueData.result.reduce((accumulator, currentValue, currentindex) => {
        var installmentFin = 0;
        /*
        * Get and merge last invoice details
        */
        var oldAmount = (currentValue.preDetails.child.length > 0) ? currentValue.preDetails.child.filter((s, idx) => s.pp_id == currentValue.pp_id) : [];
        // check previous record found or not
        if (oldAmount.length > 0) {
            //result[currentindex].principal_amount = (oldAmount[0].invoice_status != 5) ? oldAmount[0].principal_amount : 0;
            dueData.result[currentindex].principal_amount = oldAmount[0].principal_amount;
            dueData.result[currentindex].currentInterest = oldAmount[0].interest;
            dueData.result[currentindex].palnAmount = oldAmount[0].interest + oldAmount[0].principal_amount;
            //var previousPlanBlc = (oldAmount[0].invoice_status != 5) ? oldAmount[0].interest + oldAmount[0].principal_amount : oldAmount[0].interest;
            var previousPlanBlc = (oldAmount[0].invoice_status != 5) ? oldAmount[0].interest + oldAmount[0].principal_amount : oldAmount[0].interest;
            //var lastInvoiceStatus = oldAmount[0].invoice_status;
        } else {
            dueData.result[currentindex].principal_amount = 0;
            dueData.result[currentindex].currentInterest = 0;
            dueData.result[currentindex].palnAmount = 0;
            var previousPlanBlc = null;
            //var lastInvoiceStatus = oldAmount[0].invoice_status;
        }
        if (oldAmount.length > 0 && oldAmount[0].invoice_status == 5) {
            //invoiceDetails.prevBlc += oldAmount[0].interest;
        }
        dueData.result[currentindex].oldLateFee = currentValue.preDetails.oldLateFee;
        dueData.result[currentindex].oldFinCharge = currentValue.preDetails.oldFinCharge;
        dueData.result[currentindex].due_date = currentValue.preDetails.due_date;
        dueData.result[currentindex].prevBlc = currentValue.preDetails.prevBlc;

        dueData.result[currentindex].fin_charge = 0;
        if (!accumulator[currentValue.patient_id]) {
            lateFeePatient = 0;
            finChargePatient = 0;
            checkLate = 0;
            newAmt = 0;
            newAmount = 0;
            installmentFin = 0;
            // check monthly amount with previous balance amount

            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                currentValue.installments_count = currentValue.payment_term_month - 1;
                currentValue.monthly_amount = currentValue.last_month_amount;
            }
            var InterestRate = currentValue.discounted_interest_rate / 100;
            var newAmount = currentValue.remaining_amount * InterestRate / 12;
            if (oldAmount.length > 0 && oldAmount[0].invoice_status == 5) {
                var lastAmt = currentValue.monthly_amount - newAmount;
                lastAmt = parseFloat(lastAmt.toFixed(2))
                dueData.result[currentindex].principal_amount = dueData.result[currentindex].principal_amount - lastAmt;
                dueData.result[currentindex].currentInterest += newAmount;
                newAmount = dueData.result[currentindex].currentInterest;
            } else {
                dueData.result[currentindex].currentInterest += newAmount;
            }
            newAmount = currentValue.monthly_amount - newAmount;
            newAmount = parseFloat(newAmount.toFixed(2))
            if (currentValue.remaining_amount >= newAmount) {
                dueData.result[currentindex].principal_amount += newAmount;
                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                dueData.result[currentindex].palnAmount += currentValue.monthly_amount;
                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                    var variation = currentValue.remaining_amount - dueData.result[currentindex].principal_amount;
                    dueData.result[currentindex].principal_amount += variation;
                    newAmt += variation;
                }
            } else {
                dueData.result[currentindex].principal_amount += currentValue.remaining_amount;
                dueData.result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                newAmt = parseFloat(newAmt.toFixed(2))
            }
            /*
            * Check and apply late fee or fincial charge
            */
            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
            // check due date
            // check deferred invoice
            if (oldAmount.length > 0 && oldAmount[0].invoice_status == 5) {

                //check fincal chage apply or not
                if (oldAmount[0].fin_charge_waived == 0) {
                    //DeferredAmount = oldAmount[0].principal_amount;
                    installmentFin = parseFloat(((previousPlanBlc + oldAmount[0].principal_amount) * finCharge / 100).toFixed(2));
                    finChargePatient += installmentFin;
                    dueData.result[currentindex].fin_charge = installmentFin;
                }
                // check late fee apply or not
                if (oldAmount[0].late_fee_waived == 0) {
                    lateFeePatient = lateFee;
                    checkLate = 1;
                }
                //} else if ((new Date() > new Date(oldAmount[0].due_date) && oldAmount[0].due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
            } else if (oldAmount.length > 0 && oldAmount[0].invoice_status == 3) {

                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                checkLate = 1;
                lateFeePatient = lateFee;
                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                finChargePatient += installmentFin;
                dueData.result[currentindex].fin_charge = installmentFin;

            } else {
                if (checkLate == 1 || (currentValue.preDetails.lateApply == 1 && parseFloat(currentValue.preDetails.latePCT) <= PartialPayPCT)) {
                    lateFeePatient = lateFee;
                    checkLate = 1;
                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                    finChargePatient += installmentFin;
                    dueData.result[currentindex].fin_charge = installmentFin;
                } else {
                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                    if (currentValue.preDetails.lateApply == 1 && parseFloat(currentValue.preDetails.latePCT) < 100) {
                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                        finChargePatient += installmentFin;
                    } else {
                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                    }
                    dueData.result[currentindex].fin_charge = installmentFin;
                }
            }
            accumulator[currentValue.patient_id] = {
                patient_id: currentValue.patient_id,
                application_id: currentValue.application_id,
                totalAmt: newAmt,
                finCharge: finChargePatient,
                lateFee: lateFeePatient,
                //oldBlc: currentValue.oldBlc,
                oldLateFee: (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee,
                oldFinCharge: (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge,
                prevBlc: currentValue.prevBlc,
                invoiceStatus: currentValue.preDetails.invoiceStatus,
                invoiceFinStatus: currentValue.preDetails.fin_charge_waived,
                invoiceLateStatus: currentValue.preDetails.late_fee_waived,
            };
        } else {
            var newAmt = 0;
            // check monthly amount with previous balance amount
            if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                currentValue.installments_count = currentValue.payment_term_month - 1;
                currentValue.monthly_amount = currentValue.last_month_amount;
            }
            var InterestRate = currentValue.discounted_interest_rate / 100;
            var newAmount = currentValue.remaining_amount * InterestRate / 12;
            newAmount = parseFloat(newAmount.toFixed(2))
            //result[currentindex].currentInterest += newAmount;
            if (oldAmount.length > 0 && oldAmount[0].invoice_status == 5) {
                var lastAmt = currentValue.monthly_amount - newAmount;
                lastAmt = parseFloat(lastAmt.toFixed(2))
                dueData.result[currentindex].principal_amount = dueData.result[currentindex].principal_amount - lastAmt;
                dueData.result[currentindex].currentInterest += newAmount;
                newAmount = dueData.result[currentindex].currentInterest;
            } else {
                dueData.result[currentindex].currentInterest += newAmount;
            }
            newAmount = currentValue.monthly_amount - newAmount;
            if (currentValue.remaining_amount >= newAmount) {
                dueData.result[currentindex].principal_amount += newAmount;
                newAmt = currentValue.monthly_amount;// + currentValue.prevBlc;
                dueData.result[currentindex].palnAmount += currentValue.monthly_amount;
                if (currentValue.installments_count + 1 >= currentValue.payment_term_month) {
                    var variation = currentValue.remaining_amount - dueData.result[currentindex].principal_amount;
                    dueData.result[currentindex].principal_amount += variation;
                    newAmt += variation;
                }
            } else {
                dueData.result[currentindex].principal_amount += currentValue.remaining_amount;
                dueData.result[currentindex].palnAmount += currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                newAmt = currentValue.remaining_amount + currentValue.monthly_amount - newAmount;
                newAmt = parseFloat(newAmt.toFixed(2))
            }
            var getLastDate = moment(moment().add((-1), 'months'), "YYYY/MM/DD").format('MM/DD/YYYY');
            var getLastDate = new Date(moment(getLastDate, "MM/DD/YYYY").format('YYYY'), moment(getLastDate, "MM/DD/YYYY").format('MM'), -15);
            /*
            * Check and apply late fee or fincial charge
            */
            if (oldAmount.length > 0 && oldAmount[0].invoice_status == 5) {
                //check fincal chage apply or not
                if (oldAmount[0].fin_charge_waived == 0) {
                    //DeferredAmount = oldAmount[0].principal_amount;
                    installmentFin = parseFloat(((previousPlanBlc + oldAmount[0].principal_amount) * finCharge / 100).toFixed(2));
                    finChargePatient += installmentFin;
                    dueData.result[currentindex].fin_charge = installmentFin;
                }
                // check late fee apply or not
                if (oldAmount[0].late_fee_waived == 0) {
                    lateFeePatient = lateFee;
                    checkLate = 1;
                }
                //} else if ((new Date() > new Date(oldAmount[0].due_date) && oldAmount[0].due_date !== null) || (currentValue.due_date === null && new Date(currentValue.date_created) < new Date(getLastDate))) {
            } else if (oldAmount.length > 0 && oldAmount[0].invoice_status == 3) {
                previousPlanBlc = (previousPlanBlc !== null) ? previousPlanBlc : currentValue.monthly_amount;
                checkLate = 1;
                lateFeePatient = lateFee;
                installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                finChargePatient += installmentFin;
                dueData.result[currentindex].fin_charge = installmentFin;
            } else {
                if (checkLate == 1 || (currentValue.preDetails.lateApply == 1 && parseFloat(currentValue.preDetails.latePCT) <= PartialPayPCT)) {
                    lateFeePatient = lateFee;
                    checkLate = 1;
                    installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                    finChargePatient += installmentFin;
                    dueData.result[currentindex].fin_charge = installmentFin;
                } else {
                    lateFeePatient = (checkLate == 1) ? lateFee : 0;
                    if (currentValue.preDetails.lateApply == 1 && parseFloat(currentValue.preDetails.latePCT) < 100) {
                        installmentFin = parseFloat((previousPlanBlc * finCharge / 100).toFixed(2));
                        finChargePatient += installmentFin;
                    } else {
                        finChargePatient += (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                    }

                    dueData.result[currentindex].fin_charge = installmentFin;
                }


            }
            prevBlc = 0;
            accumulator[currentValue.patient_id].totalAmt += newAmt;
            accumulator[currentValue.patient_id].finCharge = finChargePatient;
            accumulator[currentValue.patient_id].lateFee = lateFeePatient;
            //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
            accumulator[currentValue.patient_id].oldLateFee = (currentValue.oldLateFee === null) ? 0 : currentValue.oldLateFee;
            accumulator[currentValue.patient_id].oldFinCharge = (currentValue.oldFinCharge === null) ? 0 : currentValue.oldFinCharge;
            accumulator[currentValue.patient_id].prevBlc = currentValue.prevBlc;
        }
        return accumulator;
    }, []);
    
    var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
        return allDetails[patient_id];
    })
    //console.log(patientGroup)
    //return false;
    con = require('../db');
    var minLenght = patientGroup.length;
    patientGroup && patientGroup.map((dataArr, idx) => {
        dataArr.totalAmt = dataArr.totalAmt + dataArr.prevBlc;
        mainTableDate(con, dataArr.patient_id, dataArr.totalAmt, invoiceDate, dataArr.application_id, dataArr.prevBlc, dataArr.finCharge, dataArr.lateFee, dataArr.oldLateFee, dataArr.oldFinCharge, 1, dueData.result, 0, dataArr.invoiceStatus, dataArr.invoiceFinStatus, dataArr.invoiceLateStatus, function (mainResponce) {

            if (0 === --minLenght) {
                console.log('done')
                //obj.status = 1;
                //obj.message = "Invoice created successfully.";
                //return callback(obj);
            }
        });
    })
}

var mainTableDate = function (con, key, value, due_date, appid, prevBlc, finCharge, lateFee, oldLateFee, oldFinCharge, current_user_id, result, on_fly, invoiceStatus, invoiceFinStatus, invoiceLateStatus, callback) {

    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('INSERT INTO patient_invoice (invoice_number, application_id, patient_id, payment_amount, previous_blc,previous_late_fee, previous_fin_charge, late_fee_due, fin_charge_due, due_date, invoice_status, on_fly, fin_charge_waived, late_fee_waived, created_by, modified_by, date_created, date_modified) '
        + 'VALUES(((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice as m)),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [appid, key, value, prevBlc, oldLateFee, oldFinCharge, lateFee, finCharge, due_date, invoiceStatus, on_fly, invoiceFinStatus, invoiceLateStatus, current_user_id, current_user_id, current_date, current_date]
        , function (error, resultMain, fields) {
            console.log(error)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                var resultFilter = result.filter((r) => r.patient_id == key);
                childLenght = resultFilter.length;
                resultFilter.forEach(function (element, idx) {

                    childTableDate(con, resultMain.insertId, element, current_user_id, invoiceStatus, function (childResponce) {
                        if (0 === --childLenght) {
                            obj.status = 1;
                            return callback(obj);
                        }
                    });
                })
            }
        });

}

var childTableDate = function (con, insertId, element, current_user_id, invoiceStatus, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    //element.monthly_amount = element.monthly_amount + element.currentInterest + element.principal_amount;
    con.query('INSERT INTO patient_invoice_details (invoice_id, application_id, pp_id, invoice_amount, installment_interest, installment_principal_amount, interest, principal_amount, fin_charge, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [insertId, element.application_id, element.pp_id, element.palnAmount, element.currentInterest, element.principal_amount, element.currentInterest, element.principal_amount, element.fin_charge, current_date, current_date, current_user_id, current_user_id]
        , function (err, resultPPIns) {
            if (err) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                var count = (invoiceStatus == 5) ? element.installments_count : element.installments_count + 1;
                con.query('UPDATE payment_plan SET installments_count=? WHERE pp_id=?',
                    [count, element.pp_id]
                    , function (errUpdate, resultPPIns) {
                        if (errUpdate) {
                            obj.status = 0;
                            return callback(obj);
                        } else {
                            obj.status = 1;
                            return callback(obj);
                        }
                    })
            }
        })

}

var getPreinvoiceDetailsCron = async (appid, newDate, onFlay, callback) => {
    con = require('../db');
    return new Promise((resolve, reject) => {
        con.query('SELECT '
            + 'patient_payment_pause.fin_charge_waived,patient_payment_pause.late_fee_waived,patient_payment_pause.interest_waived '
            + 'FROM patient_payment_pause '
            + 'INNER JOIN credit_applications on credit_applications.patient_id = patient_payment_pause.patient_id '
            + 'WHERE credit_applications.application_id= ? AND patient_payment_pause.status = ? AND patient_payment_pause.start_date <= ? AND patient_payment_pause.end_date >= ? ',
            [appid, 1, newDate, newDate]
            , function (pauseError, pauseResult, fields) {

                if (pauseResult.length > 0) {
                    if (onFlay == 1) {
                        var obj = {}
                        obj.status = 1;
                        obj.oldLateFee = 0;
                        obj.oldFinCharge = 0;
                        obj.due_date = null;
                        obj.prevBlc = 0;
                        obj.child = {}
                        obj.lateApply = 0;
                        obj.invoiceStatus = 5;
                        obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                        obj.late_fee_waived = pauseResult[0].late_fee_waived;
                        callback(obj);
                        resolve();
                    }
                    var moment = require('moment');
                    var lastDate = moment(newDate, "YYYY-MM-DD").subtract((1), 'months').format('YYYY-MM-DD');

                    con.query('SELECT '
                        + 'application_id,invoice_id,previous_late_fee,late_fee_received,late_fee_due,payment_amount,paid_amount,previous_blc, '
                        + 'previous_fin_charge,fin_charge_received,fin_charge_due, invoice_status, '
                        + 'DATE_FORMAT(due_date, "%m/%d/%Y") as due_date '
                        + 'FROM patient_invoice '
                        //+ 'WHERE application_id=? ORDER BY invoice_id DESC LIMIT 1',
                        + 'WHERE invoice_status!=? AND application_id=? AND YEAR(due_date) = YEAR(?) AND MONTH(due_date)=MONTH(?) ',
                        [0, appid, lastDate, lastDate]
                        , function (mainError, mainResult, fields) {
                            if (mainError || mainResult.length == 0) {
                                var obj = {}
                                obj.status = 1;
                                obj.oldLateFee = 0;
                                obj.oldFinCharge = 0;
                                obj.due_date = null;
                                obj.prevBlc = 0;
                                obj.child = {}
                                obj.lateApply = 0;
                                obj.invoiceStatus = 5;
                                obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                                obj.late_fee_waived = pauseResult[0].late_fee_waived;
                                callback(obj);
                                resolve();
                            } else {

                                var uInvoiceID = mainResult && [...new Set(mainResult.map(item => item.invoice_id))];

                                con.query('SELECT '
                                    + 'patient_invoice_details.pp_id,patient_invoice_details.interest,patient_invoice_details.principal_amount,patient_invoice.invoice_status,patient_invoice.fin_charge_waived,patient_invoice.late_fee_waived, patient_invoice.due_date '
                                    + 'FROM patient_invoice_details '
                                    + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                    + 'WHERE patient_invoice_details.invoice_id IN(?)',
                                    [uInvoiceID]
                                    , function (error, result, fields) {
                                        if (error || result.length == 0) {
                                            var obj = {}
                                            obj.status = 1;
                                            obj.oldLateFee = 0;
                                            obj.oldFinCharge = 0;
                                            obj.due_date = null;
                                            obj.prevBlc = 0;
                                            obj.lateApply = 0;
                                            obj.application_id = mainResult[0].application_id;
                                            obj.child = {}
                                            obj.invoiceStatus = 5;
                                            obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                                            obj.late_fee_waived = pauseResult[0].late_fee_waived;
                                            callback(obj);
                                            resolve();
                                        } else {
                                            var obj = {}
                                            obj.status = 1;
                                            obj.oldLateFee = 0;
                                            obj.oldFinCharge = 0;
                                            obj.due_date = null;
                                            obj.prevBlc = 0;
                                            obj.lateApply = 1;
                                            obj.child = {}
                                            obj.invoiceStatus = 5;
                                            obj.fin_charge_waived = pauseResult[0].fin_charge_waived;
                                            obj.late_fee_waived = pauseResult[0].late_fee_waived;
                                            var total_payment_amount = 0;
                                            var total_paid_amount = 0;

                                            mainResult.map((data, idx) => {
                                                if (data.invoice_status == 0) {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                } else if (data.invoice_status == 5) {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    obj.prevBlc += data.previous_blc;
                                                } else {
                                                    if (data.invoice_status == 1 || data.invoice_status == 4) {
                                                        obj.oldLateFee += 0;
                                                        obj.oldFinCharge += 0;
                                                        var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                        var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                        obj.prevBlc += payment_amount - paid_amount;
                                                        total_payment_amount += payment_amount;
                                                        total_paid_amount += paid_amount;
                                                    } else {
                                                        obj.oldLateFee += data.previous_late_fee;
                                                        obj.oldLateFee += data.late_fee_due;
                                                        obj.oldFinCharge += data.previous_fin_charge;
                                                        obj.oldFinCharge += data.fin_charge_due;
                                                        var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                        var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                        obj.prevBlc += payment_amount - paid_amount;
                                                        total_payment_amount += payment_amount;
                                                        total_paid_amount += paid_amount;
                                                    }

                                                }


                                            })
                                            var due_date = moment(mainResult[0].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                            due_date = new Date(moment(due_date, "MM/DD/YYYY").format('YYYY'), moment(due_date, "MM/DD/YYYY").format('MM'), 0);
                                            due_date = moment(due_date).format('MM/DD/YYYY');
                                            obj.due_date = due_date;

                                            obj.latePCT = (total_paid_amount == 0) ? 100 : total_paid_amount / total_payment_amount * 100;
                                            obj.application_id = mainResult[0].application_id;
                                            obj.child = result;

                                            callback(obj);
                                            resolve();
                                        }
                                    })
                            }
                        });
                } else {
                    if (onFlay == 1) {
                        var obj = {}
                        obj.status = 1;
                        obj.oldLateFee = 0;
                        obj.oldFinCharge = 0;
                        obj.due_date = null;
                        obj.prevBlc = 0;
                        obj.child = {}
                        obj.lateApply = 0;
                        obj.invoiceStatus = 3;
                        obj.fin_charge_waived = 0;
                        obj.late_fee_waived = 0;
                        callback(obj);
                        resolve();
                    }
                    var moment = require('moment');

                    var lastDate = moment(newDate, "YYYY-MM-DD").subtract((1), 'months').format('YYYY-MM-DD');

                    con.query('SELECT '
                        + 'application_id,invoice_id,previous_late_fee,late_fee_received,late_fee_due,payment_amount,paid_amount,previous_blc, '
                        + 'previous_fin_charge,fin_charge_received,fin_charge_due, invoice_status, '
                        + 'DATE_FORMAT(due_date, "%m/%d/%Y") as due_date '
                        + 'FROM patient_invoice '
                        //+ 'WHERE application_id=? ORDER BY invoice_id DESC LIMIT 1',
                        + 'WHERE invoice_status!=? AND application_id=? AND YEAR(due_date) = YEAR(?) AND MONTH(due_date)=MONTH(?) ',
                        [0, appid, lastDate, lastDate]
                        , function (mainError, mainResult, fields) {
                            if (mainError || mainResult.length == 0) {
                                var obj = {}
                                obj.status = 1;
                                obj.oldLateFee = 0;
                                obj.oldFinCharge = 0;
                                obj.due_date = null;
                                obj.prevBlc = 0;
                                obj.child = {}
                                obj.lateApply = 0;
                                obj.invoiceStatus = 3;
                                obj.fin_charge_waived = 0;
                                obj.late_fee_waived = 0;
                                callback(obj);
                                resolve();
                            } else {

                                var uInvoiceID = mainResult && [...new Set(mainResult.map(item => item.invoice_id))];

                                con.query('SELECT '
                                    + 'patient_invoice_details.pp_id,patient_invoice_details.interest,patient_invoice_details.principal_amount,patient_invoice.invoice_status,patient_invoice.fin_charge_waived,patient_invoice.late_fee_waived, patient_invoice.due_date '
                                    + 'FROM patient_invoice_details '
                                    + 'INNER JOIN patient_invoice ON patient_invoice.invoice_id=patient_invoice_details.invoice_id '
                                    + 'WHERE patient_invoice_details.invoice_id IN(?)',
                                    [uInvoiceID]
                                    , function (error, result, fields) {
                                        if (error || result.length == 0) {
                                            var obj = {}
                                            obj.status = 1;
                                            obj.oldLateFee = 0;
                                            obj.oldFinCharge = 0;
                                            obj.due_date = null;
                                            obj.prevBlc = 0;
                                            obj.lateApply = 0;
                                            obj.application_id = mainResult[0].application_id;
                                            obj.child = {}
                                            obj.invoiceStatus = 3;
                                            obj.fin_charge_waived = 0;
                                            obj.late_fee_waived = 0;
                                            callback(obj);
                                            resolve();
                                        } else {
                                            var obj = {}
                                            obj.status = 1;
                                            obj.oldLateFee = 0;
                                            obj.oldFinCharge = 0;
                                            obj.due_date = null;
                                            obj.prevBlc = 0;
                                            obj.lateApply = 1;
                                            obj.child = {}
                                            obj.invoiceStatus = 3;
                                            obj.fin_charge_waived = 0;
                                            obj.late_fee_waived = 0;
                                            var total_payment_amount = 0;
                                            var total_paid_amount = 0;

                                            mainResult.map((data, idx) => {
                                                if (data.invoice_status == 0) {
                                                    obj.oldLateFee += data.previous_late_fee;
                                                    obj.oldLateFee += data.late_fee_due;
                                                    obj.oldFinCharge += data.previous_fin_charge;
                                                    obj.oldFinCharge += data.fin_charge_due;
                                                    var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                    var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                    obj.prevBlc += payment_amount - paid_amount;
                                                    total_payment_amount += payment_amount;
                                                    total_paid_amount += paid_amount;
                                                } else {
                                                    if (data.invoice_status == 1 || data.invoice_status == 4) {
                                                        obj.oldLateFee += 0;
                                                        obj.oldFinCharge += 0;
                                                        var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                        var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                        obj.prevBlc += payment_amount - paid_amount;
                                                        total_payment_amount += payment_amount;
                                                        total_paid_amount += paid_amount;
                                                    } else if (data.invoice_status == 5) {
                                                        obj.oldLateFee += data.previous_late_fee;
                                                        obj.oldLateFee += data.late_fee_due;
                                                        obj.oldFinCharge += data.previous_fin_charge;
                                                        obj.oldFinCharge += data.fin_charge_due;
                                                        obj.prevBlc += data.previous_blc;
                                                    } else {
                                                        obj.oldLateFee += data.previous_late_fee;
                                                        obj.oldLateFee += data.late_fee_due;
                                                        obj.oldFinCharge += data.previous_fin_charge;
                                                        obj.oldFinCharge += data.fin_charge_due;
                                                        var payment_amount = (data.payment_amount !== null) ? data.payment_amount : 0;
                                                        var paid_amount = (data.paid_amount !== null) ? data.paid_amount : 0;
                                                        obj.prevBlc += payment_amount - paid_amount;
                                                        total_payment_amount += payment_amount;
                                                        total_paid_amount += paid_amount;
                                                    }

                                                }


                                            })
                                            var due_date = moment(mainResult[0].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                            due_date = new Date(moment(due_date, "MM/DD/YYYY").format('YYYY'), moment(due_date, "MM/DD/YYYY").format('MM'), 0);
                                            due_date = moment(due_date).format('MM/DD/YYYY');
                                            obj.due_date = due_date;

                                            obj.latePCT = (total_paid_amount == 0) ? 100 : total_paid_amount / total_payment_amount * 100;
                                            obj.application_id = mainResult[0].application_id;
                                            obj.child = result;

                                            callback(obj);
                                            resolve();
                                        }
                                    })
                            }
                        });
                }
            })
    })
}

var getPlanDetailsCron = async (callback) => {
    con = require('../db');
    var obj = { status: 0 };
    return new Promise((resolve, reject) => {
        con.query('SELECT payment_plan.installments_count,payment_plan.pp_id,DATE_FORMAT(payment_plan.date_created, "%Y-%m-%d") as date_created, '
            + 'credit_applications.application_id,credit_applications.patient_id,payment_plan.term AS payment_term_month,payment_plan.discounted_interest_rate,'
            + 'payment_plan.interest_rate, payment_plan.monthly_amount,payment_plan.last_month_amount,payment_plan.remaining_amount,'
            + 'payment_plan.installments_count,payment_plan.loan_type,payment_plan.loan_amount '
            + 'FROM payment_plan '
            + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
            + 'WHERE payment_plan.plan_status = 1  AND payment_plan.remaining_amount > 0 '
            + 'AND payment_plan.pp_id NOT IN (SELECT patient_invoice_details.pp_id FROM patient_invoice_details INNER JOIN patient_invoice ON patient_invoice.invoice_id = patient_invoice_details.invoice_id WHERE patient_invoice_details.pp_id=payment_plan.pp_id AND MONTH(patient_invoice.due_date) = MONTH(CURRENT_DATE()) AND YEAR(patient_invoice.due_date) = YEAR(CURRENT_DATE())) ORDER by credit_applications.patient_id'
            , function (error, result, fields) {
                if (!error && result.length > 0) {
                    try {
                        obj.status = 1;
                        obj.result = result;
                        callback(obj)
                        resolve()
                    } catch (e) {
                        callback(obj)
                        resolve()
                    }
                }
            })
    })
}

var getCharges = async (callback) => {
    con = require('../db');
    var obj = {};
    return new Promise((resolve, reject) => {
        obj.lateFee = 0;
        obj.finCharge = 0;
        obj.PartialPayPCT = 0;
        con.query('SELECT '
            + 'value,md_id '
            + 'FROM master_data_values '
            + 'WHERE status = ? AND (md_id = ? OR md_id = ? OR md_id = ?)'
            , [1, 'Late Fee', 'Financial Charges', 'Partial Pay PCT']
            , function (errLate, late_fee_rows, fields) {
                if (!errLate) {
                    if (late_fee_rows.length > 0) {
                        var late = late_fee_rows.filter(function (item) {
                            return item.md_id == 'Late Fee';
                        });
                        obj.lateFee = (late.length > 0) ? late[0].value : 0;
                        var financial = late_fee_rows.filter(function (item) {
                            return item.md_id == 'Financial Charges';
                        });
                        obj.finCharge = (financial.length > 0) ? financial[0].value : 0;
                        var PartialPay = late_fee_rows.filter(function (item) {
                            return item.md_id == 'Partial Pay PCT';
                        });
                        obj.PartialPayPCT = (PartialPay.length > 0) ? PartialPay[0].value : 0;
                    }
                    callback(obj)
                    resolve()
                } else {
                    callback(obj)
                    resolve()
                }
            })
    })
}

var deuPayment2 = function () {
    con = require('../db');
    var obj = {};

    con.query('SELECT credit_applications.patient_id,payment_plan.provider_id,pp_installments.pp_id,pp_installments.pp_installment_id,pp_installments.installment_amt,DATE_FORMAT(pp_installments.due_date, "%m/%d/%Y") AS due_date, '
        + 'payment_plan.term AS payment_term_month,payment_plan.discounted_interest_rate,payment_plan.interest_rate,'
        + '(SELECT sum(invoice_installments.amount_rcvd) FROM invoice_installments WHERE invoice_installments.pp_installment_id=pp_installments.pp_installment_id) as amount_rcvd, '
        //+ '(SELECT IF(paid_flag=1, 0, (previous_blc+late_fee_received+fin_charge_amt)) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldBlc '
        + '(SELECT IF(paid_flag=1, 0, (IF(ISNULL(previous_late_fee), 0, previous_late_fee)+IF(ISNULL(late_fee_received), 0, late_fee_received))) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldLateFee, '
        + '(SELECT IF(paid_flag=1, 0, (IF(ISNULL(previous_fin_charge), 0, previous_fin_charge)+IF(ISNULL(fin_charge_amt), 0, fin_charge_amt))) FROM patient_invoice WHERE patient_id=credit_applications.patient_id ORDER BY invoice_id DESC LIMIT 1 ) as oldFinCharge '
        + 'FROM pp_installments '
        + 'INNER JOIN payment_plan ON payment_plan.pp_id=pp_installments.pp_id '
        //+ 'INNER JOIN interest_rate ON payment_plan.interest_rate_id = interest_rate.id '
        //+ 'INNER JOIN master_data_values ON master_data_values.mdv_id = payment_plan.term_id '
        + 'INNER JOIN credit_applications ON payment_plan.application_id=credit_applications.application_id '
        //+ 'WHERE MONTH(pp_installments.due_date) <= MONTH(CURRENT_DATE()) AND YEAR(pp_installments.due_date) <= YEAR(CURRENT_DATE()) '
        + 'WHERE pp_installments.due_date <= LAST_DAY(CURRENT_DATE()) '
        + 'AND pp_installments.pp_installment_id NOT IN (SELECT invoice_installments.pp_installment_id FROM invoice_installments INNER JOIN pp_installments ON pp_installments.pp_installment_id=invoice_installments.pp_installment_id WHERE MONTH(invoice_installments.due_date)=MONTH(CURRENT_DATE()) AND pp_installments.paid_flag=0) '
        + 'AND (pp_installments.paid_flag =0 OR pp_installments.partial_paid=1) ORDER by credit_applications.patient_id,pp_installments.due_date'
        , function (error, result, fields) {
            //console.log(result)
            if (error || Object.keys(result).length == 0) {
            } else {
                con.query('SELECT '
                    + 'value,md_id '
                    + 'FROM master_data_values '
                    + 'WHERE status = ? AND (md_id = ? OR md_id = ?)'
                    , [1, 'Late Fee', 'Financial Charges']
                    , function (errLate, late_fee_rows, fields) {
                        if (errLate) { } else {
                            var lateFee = 0;
                            var finCharge = 0;
                            if (late_fee_rows.length > 0) {
                                var late = late_fee_rows.filter(function (item) {
                                    return item.md_id == 'Late Fee';
                                });
                                lateFee = (late.length > 0) ? late[0].value : 0;
                                var financial = late_fee_rows.filter(function (item) {
                                    return item.md_id == 'Financial Charges';
                                });
                                finCharge = (financial.length > 0) ? financial[0].value : 0;

                            }
                            var checkLate = 0;
                            var lateFeePatient = 0;
                            var finChargePatient = 0;
                            var allDetails = result && result.reduce(function (accumulator, currentValue, currentindex) {
                                if (!accumulator[currentValue.patient_id]) {
                                    lateFeePatient = 0;
                                    finChargePatient = 0;
                                    checkLate = 0;
                                    if (new Date() > new Date(currentValue.due_date)) {
                                        checkLate = 1;
                                        lateFeePatient = lateFee;
                                        //finChargePatient = finCharge;
                                        finChargePatient = ((currentValue.installment_amt / currentValue.payment_term_month) * currentValue.interest_rate) / 100;

                                        var prevBlc = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    } else {
                                        lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                        finChargePatient = (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                        var prevBlc = 0;
                                    }

                                    var newAmt = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    accumulator[currentValue.patient_id] = {
                                        patient_id: currentValue.patient_id,
                                        totalAmt: newAmt,
                                        finCharge: finChargePatient,
                                        lateFee: lateFeePatient,
                                        //oldBlc: currentValue.oldBlc,
                                        oldLateFee: currentValue.oldLateFee,
                                        oldFinCharge: currentValue.oldFinCharge,
                                        prevBlc: prevBlc,
                                    };
                                } else {
                                    if (new Date() > new Date(currentValue.due_date)) {
                                        checkLate = 1;
                                        lateFeePatient = lateFee;
                                        //finChargePatient = finCharge;
                                        finChargePatient += ((currentValue.installment_amt / currentValue.payment_term_month) * currentValue.interest_rate) / 100;

                                        var prevBlc = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    } else {
                                        lateFeePatient = (checkLate == 1) ? lateFee : 0;
                                        finChargePatient = (checkLate == 1) ? parseFloat(finChargePatient) : 0;
                                        var prevBlc = 0;
                                    }

                                    var newAmt = (currentValue.amount_rcvd != null) ? (currentValue.installment_amt - currentValue.amount_rcvd) : currentValue.installment_amt;
                                    accumulator[currentValue.patient_id].totalAmt += newAmt;
                                    accumulator[currentValue.patient_id].finCharge = finChargePatient;
                                    accumulator[currentValue.patient_id].lateFee = lateFeePatient;
                                    //accumulator[currentValue.patient_id].oldBlc = currentValue.oldBlc;
                                    accumulator[currentValue.patient_id].oldLateFee = currentValue.oldLateFee;
                                    accumulator[currentValue.patient_id].oldFinCharge = currentValue.oldFinCharge;
                                    accumulator[currentValue.patient_id].prevBlc += prevBlc;

                                }
                                return accumulator;
                            }, []);
                            var patientGroup = allDetails && Object.keys(allDetails).map((patient_id, idx) => {
                                return allDetails[patient_id];
                            })
                            //console.log(patientGroup)
                            //return false;
                            con.beginTransaction(function (err) {
                                if (err) {
                                    obj.status = 0;
                                    obj.message = "Something wrong please try again.";
                                    return callback(obj);
                                }


                                patientGroup && patientGroup.map((data, idx) => {

                                    con.query('SELECT payment_amount,invoice_id FROM patient_invoice WHERE patient_id=? AND due_date=LAST_DAY(now()) AND paid_flag != ?'
                                        , [data.patient_id, 1], function (checkErr, checkResult, fields) {
                                            if (checkErr || checkResult.length == 0) {
                                                mainTableDate(con, data.patient_id, data.totalAmt, data.prevBlc, data.finCharge, data.lateFee, data.oldLateFee, data.oldFinCharge, result, function (mainResponce) {
                                                    //console.log(mainResponce);
                                                });
                                            } else {
                                                var totalAmount = parseFloat(checkResult[0].payment_amount) + data.totalAmt;
                                                mainTableDateUpdate(con, data.patient_id, checkResult[0].invoice_id, totalAmount, result, function (mainResponce) {
                                                    //console.log(mainResponce);
                                                });
                                            }
                                        })
                                    /*mainTableDate(con, data.patient_id, data.totalAmt, data.prevBlc, data.finCharge, data.lateFee, data.oldLateFee, data.oldFinCharge, result, function (mainResponce) {
                                        //console.log(mainResponce);
                                    });*/
                                })

                                con.commit(function (err) {
                                    if (err) {
                                        //console.log('error')
                                    }
                                    //console.log('ok')
                                });
                            })
                        }
                    })
            }


        });
};

var mainTableDateUpdate = function (con, patient_id, invoice_id, value, result, callback) {
    const moment = require('moment');
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    var resultFilter = result.filter((r) => r.patient_id == patient_id);

    var dueDate = (resultFilter.length > 0) ? moment(resultFilter[0].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD') : 'LAST_DAY(now())';

    con.query('UPDATE patient_invoice SET payment_amount=?, modified_by=?, date_modified=? WHERE invoice_id=?',
        [value, 1, current_date, invoice_id]
        , function (error, resultMain, fields) {
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {
                //var resultFilter = result.filter((r) => r.patient_id == patient_id);

                resultFilter.forEach(function (element, idx) {

                    childTableDate(con, invoice_id, element, dueDate, function (childResponce) {
                        //console.log(childResponce);
                    });
                })
                obj.status = 1;
                return callback(obj);
            }
        });

}

var mainTableDateold = function (con, key, value, prevBlc, finCharge, lateFee, oldLateFee, oldFinCharge, result, callback) {
    const moment = require('moment');
    let date = require('date-and-time');
    let now = new Date();

    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    var resultFilter = result.filter((r) => r.patient_id == key);


    var maxRecord = resultFilter.reduce(function (a, b) {
        return new Date(a.due_date) > new Date(b.due_date) ? a : b;
    });
    var dueDate = (maxRecord.due_date !== undefined) ? moment(maxRecord.due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD') : 'LAST_DAY(now())';

    con.query('INSERT INTO patient_invoice (invoice_number, patient_id, payment_amount, previous_blc, previous_late_fee, previous_fin_charge, late_fee_received, fin_charge_amt, due_date, paid_flag, created_by, modified_by, date_created, date_modified) '
        + 'VALUES(((SELECT COALESCE(MAX(invoice_number),11111)+1 AS dt FROM patient_invoice as m)),?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [key, value, prevBlc, oldLateFee, oldFinCharge, lateFee, finCharge, dueDate, 3, 1, 1, current_date, current_date]
        , function (error, resultMain, fields) {
            console.log(error)
            if (error) {
                con.rollback(function () {
                    obj.status = 0;
                    return callback(obj);
                });
            } else {


                resultFilter.forEach(function (element, idx) {

                    childTableDate(con, resultMain.insertId, element, dueDate, function (childResponce) {
                        //console.log(childResponce);
                    });
                })
                obj.status = 1;
                return callback(obj);
            }
        });

}

var childTableDateold = function (con, insertId, element, dueDate, callback) {
    let date = require('date-and-time');
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    var obj = {};
    con.query('INSERT INTO invoice_installments (invoice_id, pp_id, pp_installment_id, addi_amt_invoice_flag, due_date, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?,?)',
        [insertId, element.pp_id, element.pp_installment_id, 1, dueDate, current_date, current_date, 1, 1]
        , function (err, resultPPIns) {

            if (err) {
                con.rollback(function () {
                    obj.status = 3;
                    return callback(obj);
                });
            } else {
                obj.status = 4;
                return callback(obj);
            }
        })

}


//exports.deuPayment = deuPayment;


