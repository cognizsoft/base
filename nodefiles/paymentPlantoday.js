module.exports = function (app, con, jwtMW) {
    let date = require('date-and-time');
    let now = new Date();
    /*
    * get data and send email report
    */
    app.get('/api/send-customer-invoice-report', jwtMW, (req, res) => {
        //console.log(req.query.app_id)
        //console.log(req.query.plan_id)
        //console.log(req.query.instlmnt_id)
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var dataSql = require('./payment/payment.sql.js');
        var dataSqlC = require('./credit/creditSql.js');
        var objE = {};
        dataSql.getSingleInstallment(con, req.query.plan_id, req.query.instlmnt_id, function (result1) {
            if(result1.status == 1) {
                res.send("something wrong")
            } else {

                dataSqlC.getSinglePlans(con, req.query.app_id, function (result2) {
                    if (result2.status == 1) {
                        objE.result1 = result1;
                        objE.result2 = result2;
                        //console.log(result1.single_installment_payment[0].installment_amt)
                        var invoice_date = current_date;
                        var ac_no = result2.plan[0].application_no;
                        //====//
                        var pddArr = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 0 ) : '';
                        var pymnt_due_date = pddArr.due_date;
                        //====//

                        //amount due--monthly payment
                        var amount_due = result2.plan[0].monthly_payment;

                        var late_fee = result1.payment_billing_late_fee[0].late_fee;

                        var full_name = result2.plan[0].f_name+' '+result2.plan[0].m_name+' '+result2.plan[0].l_name;

                        var address = result2.plan[0].address1+' '+result2.plan[0].address2+' '+result2.plan[0].city+' '+result2.plan[0].region_name;

                        var phone = result2.plan[0].peimary_phone;

                        var loan_amount = result2.plan[0].credit_amount;

                        var payment_term = result2.plan[0].payment_term;

                        var interest_rate = result2.plan[0].interest_rate;

                        //====//
                        var getPaidBalnc = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 1 ) : '';
                        if(getPaidBalnc) {
                            var paidInsTotal = 0;
                            for (var i = 0; i<getPaidBalnc.length; i++) {
                                paidInsTotal += parseFloat(getPaidBalnc[i].payment_amount);
                            }
                        }
                        var curBlncArr = result2.plan[0].credit_amount-paidInsTotal; 
                        //====//

                        var fincharge = ((result1.single_installment_payment[0].installment_amt/payment_term)*interest_rate)/100;

                        var total_amt_due = amount_due+fincharge;

                        //====//
                        var lastPymntInfo = (result2.plan) ? result2.plan.filter(x => x.paid_flag == 1 ).pop() : '';
                        //====//
                        var last_payment_date = lastPymntInfo.payemnt_date;

                        var last_mnthly_pymnt = lastPymntInfo.payment_amount;

                        var last_addi_amount = lastPymntInfo.additional_amount;

                        var last_late_fee = (lastPymntInfo.late_fee_received !== null) ? lastPymntInfo.late_fee_received : 0;

                        var last_fin_ch = lastPymntInfo.fin_charge_amt;

                        var last_total_amount = last_mnthly_pymnt+last_addi_amount+last_late_fee+last_fin_ch;

                        

                        var emialTemp = require('./email.js');
                        var locals = {
                            test: 'testing'
                        };
                        var toEmailAddress = 'ramesh.bhambhu@gmail.com';
                        var template = 'customer-invoice';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);

                        res.send(objE);
                    } else {
                        res.send("something wrong")
                    }

                });
                //res.send(objE);
            }
        });
        /*dataSqlC.getSinglePlans(con, req.query.app_id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });*/
    });
    /*
    * pay installment insert
    */
    app.post('/api/pay-installment-insert', jwtMW, (req, res) => {
        //console.log('req.query.planId')
        var dataSql = require('./payment/payment.sql.js');
        dataSql.payInstallmentInsert(con, req.body, function (result) {
            if(result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });

	/*
    * get single plan details in row
    */
    app.get('/api/application-plan-singleInstallment-payment-details', jwtMW, (req, res) => {
        console.log(req.query.planId)
        console.log(req.query.installmentId)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getSingleInstallment(con, req.query.planId, req.query.installmentId, function (result) {
            if(result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });
    /*
    * get master fee option
    */
    app.get('/api/payment-master-fee-option', jwtMW, (req, res) => {
        // this function use for get credit application details
        //console.log("tested");
        //return false;
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getPaymentMasterFeeOption(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    /*
    * get waiver type option
    */
    app.get('/api/payment-late-fee-waiver-type-option', jwtMW, (req, res) => {
        console.log(req.query.id)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getLateFeeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    app.get('/api/payment-fin-charge-waiver-type-option', jwtMW, (req, res) => {
        console.log(req.query.id)
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getFinChargeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });
}