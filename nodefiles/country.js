/*
* Title: States
* Descrpation :- This module blong to states realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/country-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT id, name, abbreviation, status FROM countries WHERE deleted_flag = ? ORDER BY id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })


    app.post('/api/country-insert/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO countries (name, abbreviation, status, date_created, date_modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?)',
            [
                req.body.name,
                req.body.abbreviation,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_country_id": result.insertId,
                        "name": req.body.name,
                        "abbreviation": req.body.abbreviation,
                        "country_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/country-name-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.id === undefined) {
          sql = 'SELECT name FROM countries WHERE deleted_flag=? AND name=?';
          var edit = 0;
        } else {
          sql = 'SELECT name FROM countries WHERE deleted_flag=? AND name=? AND id !=?';
          var edit = 1;
        }
        con.query(sql,
          [
            0,
            req.body.name,
            req.body.id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                "status": 0
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0,
                "edit": edit
              }
              res.send(obj);
    
            }
          })
      })
    
      app.post('/api/country-abbreviation-exist/', jwtMW, (req, res) => {
        con = require('../db');
        let sql = '';
        if (req.body.state_id === undefined) {
          sql = 'SELECT abbreviation FROM states WHERE delete_flag=? AND abbreviation=?';
        } else {
          sql = 'SELECT abbreviation FROM states WHERE delete_flag=? AND abbreviation=? AND state_id !=?';
        }
        con.query(sql,
          [
            0,
            req.body.abbreviation,
            req.body.state_id
          ]
          , function (error, rows, fields) {
    
            if (error) {
              var obj = {
                "status": 0
              }
              res.send(obj);
            } else {
    
              var obj = {
                "status": 1,
                "exist": (rows.length > 0) ? 1 : 0
              }
              res.send(obj);
    
            }
          })
      })

      app.post('/api/country-update/', jwtMW,(req,res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
         con.query('UPDATE countries SET name=?, abbreviation=?, status=?, date_modified=?, modified_by=? WHERE id=?',
         [
          req.body.name,
          req.body.abbreviation,
          req.body.status,
          current_date,
          current_user_id,
          req.body.id
         ]
         , function (err,result){
              if(err) {
                var obj = {
                  "status": 0
                }
                res.send(obj);
              } else {
                var obj = {
                 "status": 1
                }
                res.send(obj);
               
              }
         })
      })
}
