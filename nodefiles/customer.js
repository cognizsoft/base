module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    var multer = require('multer');
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {

            var customer_id;
            if (req.body.customer_id == undefined) {
                customer_id = 'all-documents';
            } else {
                customer_id = req.body.customer_id;
            }

            const dir = './uploads/customers/' + customer_id
            var mkdirp = require('mkdirp');
            mkdirp(dir, function (err) {
                if (err) console.error(err)
                else cb(null, dir);
            });

        },
        filename: function (req, file, cb) {
            cb(null, Date.now() + file.originalname);
        }
    });

    const fileFilter = (req, file, cb) => {
        cb(null, true);
    }

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * 10
        },
        fileFilter: fileFilter
    });
    var dataupload = upload.single('imgedata');

    /*
    * Get all customer
    */
    app.get('/api/customer-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT patient.patient_id, patient.patient_ac, patient.f_name, patient.m_name, patient.l_name, patient.gender, patient.email, patient.peimary_phone, patient.status, provider.name, '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, '
            + '(SELECT application_id FROM credit_applications WHERE patient_id=patient.patient_id AND (status=1 OR status=6) ORDER BY application_id DESC LIMIT 1) as application_id,'
            + '(SELECT approve_amount FROM credit_applications WHERE patient_id=patient.patient_id AND (status=1 OR status=6) ORDER BY application_id DESC LIMIT 1) as approve_amount,'
            + '(SELECT (remaining_amount+override_amount) as remaining_amount FROM credit_applications WHERE patient_id=patient.patient_id AND (status=1 OR status=6) ORDER BY application_id DESC LIMIT 1) as remaining_amount, '
            + '(SELECT count(pp_id) FROM payment_plan INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id WHERE credit_applications.patient_id=patient.patient_id) as plan_exists, '
            + '(SELECT MAX(plan_status) FROM payment_plan INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id WHERE credit_applications.patient_id=patient.patient_id and (payment_plan.plan_status=10 OR payment_plan.plan_status=9)) as plan_status '
            + 'FROM patient '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'LEFT JOIN provider on provider.provider_id=patient.provider_id '
            + 'WHERE patient.delete_flag=? ORDER BY patient.patient_id DESC',
            [
                0,
            ]
            , function (error, rows, fields) {

                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });
    /*
    * View customer
    */
    getCustomerDetails = async (req, res, next) => {
        con = require('../db');
        let dataSql = require('./credit/creditSql.js');
        let obj = {}
        obj.status = 0;
        let appDetails;
        await dataSql.getCustomerDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.customerDetails = sResult.customerDetails;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getAllSecDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.secDetails = sResult.secDetails;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getAddDetails(con, req.query.id, 0, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.appAddress = sResult.appAddress;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getExpDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.experianAddress = sResult.experianAddress;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getExpEmpDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.experianEmployment = sResult.experianEmployment;
            }
        })
        if (appDetails == 0) { return false }

        await dataSql.getBankDetails(con, req.query.id, 0, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.bankDetails = sResult.bankDetails;
            }
        })
        if (appDetails == 0) { return false }
        /****************Get application details******************/
        await dataSql.getCusAppDetails(con, req.query.id, function (sResult) {
            if (sResult.status == 0) {
                appDetails = 0;
                obj.message = "Application details not found."
                res.send(obj);
            } else {
                obj.applicationDetails = sResult.applicationDetails;
            }
        })
        if (appDetails == 0) { return false }

        obj.status = 1;
        res.send(obj)
    }
    app.get('/api/customer-view', jwtMW, getCustomerDetails)

    // provider
    /*
    * Get all provider customer
    */
    app.get('/api/customer-list-provider/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT patient.patient_id, patient.patient_ac, patient.f_name, patient.m_name, patient.l_name, patient.gender, patient.email, patient.peimary_phone, patient.status, provider.name, '
            //+ 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, payment_plan.application_id '
            + 'patient_address.address1,patient_address.address2,patient_address.City,patient_address.zip_code,states.name as state_name, '
            + '(SELECT application_id FROM credit_applications WHERE patient_id=patient.patient_id ORDER BY application_id DESC LIMIT 1) as application_id,'
            + '(SELECT approve_amount FROM credit_applications WHERE patient_id=patient.patient_id ORDER BY application_id DESC LIMIT 1) as approve_amount,'
            + '(SELECT (remaining_amount+override_amount) as remaining_amount FROM credit_applications WHERE patient_id=patient.patient_id ORDER BY application_id DESC LIMIT 1) as remaining_amount, '
            + '(SELECT count(pp_id) FROM payment_plan INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id WHERE credit_applications.patient_id=patient.patient_id) as plan_exists, '
            + '(SELECT MAX(plan_status) FROM payment_plan INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id WHERE credit_applications.patient_id=patient.patient_id and payment_plan.plan_status=9) as plan_status '
            + 'FROM patient '
            + 'INNER JOIN patient_address ON patient_address.patient_id=patient.patient_id AND patient_address.primary_address=1 AND patient_address.status=1 '
            /*+ 'INNER JOIN regions ON regions.region_id=patient_address.region_id '*/
            + 'INNER JOIN states ON states.state_id=patient_address.state_id '
            + 'INNER JOIN patient_provider ON patient_provider.patient_id=patient.patient_id '
            + 'INNER JOIN provider on provider.provider_id=patient_provider.provider_id '
            /*+ 'LEFT JOIN payment_plan ON payment_plan.provider_id=patient_provider.provider_id '*/

            + 'WHERE patient.delete_flag=? AND patient_provider.provider_id=? ORDER BY patient.patient_id DESC',
            [
                0,
                req.query.id
            ]

            , function (error, rows, fields) {
                console.log(error)
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows
                    }
                    res.send(obj);
                }
            })
    });

    app.post('/api/patient-verify-amount', jwtMW, (req, res) => {
        con = require('../db');

        var obj = {};
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                return callback(obj);
            }
            con.query('SELECT '
                + 'value '

                + 'FROM master_data_values '

                + 'WHERE '
                + 'md_id = ?',
                [
                    'Bank Verification Duration'
                ]
                , function (berr, bankDur) {
                    if (berr) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong please try again.";
                            res.send(obj);
                        });
                    } else {
                        con.query('UPDATE patient SET '
                            + 'bank_verify_amt = ?,'
                            + 'bank_verify_amt_duration = ? '

                            + 'WHERE '
                            + 'patient_id=?',
                            [
                                req.body.verify_amount,
                                (Date.now() / 1000) + (3600 * bankDur[0].value),
                                req.body.patient_id
                            ]
                            , function (err, verifyAmt) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong please try again.";
                                        res.send(obj);
                                    });
                                } else {
                                    con.query('SELECT '
                                        + 'f_name, '
                                        + 'l_name, '
                                        + 'email '

                                        + 'FROM patient '

                                        + 'WHERE '
                                        + 'patient_id=?',
                                        [
                                            req.body.patient_id
                                        ]
                                        , function (err, pateintDetail) {
                                            if (err) {
                                                con.rollback(function () {
                                                    obj.status = 0;
                                                    obj.message = "Something wrong please try again.";
                                                    res.send(obj);
                                                });
                                            } else {
                                                con.commit(function (err) {
                                                    if (err) {
                                                        con.rollback(function () {
                                                            obj.status = 0;
                                                            obj.message = "Something wrong please try again.";
                                                        });
                                                        res.send(obj);
                                                    }

                                                    var emialTemp = require('./emailplan.js');
                                                    var context = {
                                                        customer_name: pateintDetail[0].f_name + ' ' + pateintDetail[0].l_name
                                                    };

                                                    //var template = 'bank-account-verify';
                                                    //emialTemp.emailTemplate(toEmailAddress, locals, template);
                                                    var template = 'Bank Account Verification';

                                                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                        if (Result.status == 1) {

                                                            var emailOpt = {
                                                                toEmail: pateintDetail[0].email,
                                                                subject: Result.result[0].template_subject,
                                                            }

                                                            var html = Result.result[0].template_content;
                                                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                if (emailResult.status == 1) {
                                                                    obj.template = "Email sent successfully!"
                                                                } else {
                                                                    obj.template = "Email not sent successfully!"
                                                                }
                                                            });
                                                        } else {
                                                            obj.template = "Template details not found."
                                                        }

                                                    });


                                                    obj.status = 1;
                                                    obj.message = "Amount Submitted Successfully!";
                                                    obj.patient = pateintDetail;
                                                    obj.verifyAmt = verifyAmt;
                                                    res.send(obj);
                                                });
                                            }
                                        });
                                    //
                                }
                            });

                    }

                })

        });

    });

    /*
    * Pause custoemr invoice
    */
    app.post('/api/customer-invoice-pause/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let date = require('date-and-time');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');

        con.query('INSERT INTO patient_payment_pause (patient_id, start_date, end_date, fin_charge_waived, late_fee_waived,interest_waived, due_to, status, created_by, modified_by, date_created, date_modified) '
            + 'VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
            [req.body.patient_id, req.body.start_date, req.body.end_date, req.body.fin_charge_waived, req.body.late_fee_waived, req.body.interest_waived, req.body.due_to, req.body.status, req.body.current_user_id, req.body.current_user_id, current_date, current_date]
            , function (error, resultMain, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var dataSql = require('./payment/payment.sql.js');
                    dataSql.pauseTrigger(con, req.body, function (invoice) {
                        if (invoice.status == 1) {
                            con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
                                [req.body.patient_id]
                                , function (error, result, fields) {
                                    if (error) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                    } else {
                                        var obj = {
                                            "status": 1,
                                            "result": result,
                                            "message": "Record added successfully."
                                        }
                                        res.send(obj);
                                    }
                                })
                        } else {
                            con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
                                [req.body.patient_id]
                                , function (error, result, fields) {
                                    if (error) {
                                        var obj = {
                                            "status": 0,
                                            "message": "Something wrong please try again."
                                        }
                                        res.send(obj);
                                    } else {
                                        var obj = {
                                            "status": 1,
                                            "result": result,
                                            "message": "Record added successfully."
                                        }
                                        res.send(obj);
                                    }
                                })
                        }
                    })


                }
            })

    });

    /*
    * Pause custoemr invoice list
    */
    app.get('/api/customer-invoice-pause-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
            [req.query.id]
            , function (error, result, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": result,
                    }
                    res.send(obj);
                }
            })

    });

    /*
    * Pause custoemr invoice
    */
    app.post('/api/customer-invoice-pause-update/', jwtMW, (req, res) => {
        con = require('../db');
        var obj = {};
        let date = require('date-and-time');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var dataSql = require('./payment/payment.sql.js');
        dataSql.pauseTriggerUpdate(con, req.body, function (invoice) {
            if (invoice.status == 1) {
                con.query('UPDATE patient_payment_pause SET '
                    + 'start_date=?, '
                    + 'end_date=?, '
                    + 'fin_charge_waived=?, '
                    + 'late_fee_waived=?, '
                    + 'interest_waived=?, '
                    + 'due_to=?, '
                    + 'status=?, '
                    + 'date_modified=?, '
                    + 'modified_by=? '
                    + 'WHERE id=?',
                    [req.body.start_date, req.body.end_date, req.body.fin_charge_waived, req.body.late_fee_waived, req.body.interest_waived, req.body.due_to, req.body.status, req.body.current_user_id, current_date, req.body.id]
                    , function (error, resultMain, fields) {
                        if (error) {
                            var obj = {
                                "status": 0,
                                "message": "Something wrong please try again."
                            }
                            res.send(obj);
                        } else {
                            if (req.body.status == 1) {
                                dataSql.pauseTrigger(con, req.body, function (invoice) {
                                    if (invoice.status == 1) {
                                        con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
                                            [req.body.patient_id]
                                            , function (error, result, fields) {
                                                if (error) {
                                                    var obj = {
                                                        "status": 0,
                                                        "message": "Something wrong please try again."
                                                    }
                                                    res.send(obj);
                                                } else {
                                                    var obj = {
                                                        "status": 1,
                                                        "result": result,
                                                        "message": "Record added successfully."
                                                    }
                                                    res.send(obj);
                                                }
                                            })
                                    } else {
                                        con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
                                            [req.body.patient_id]
                                            , function (error, result, fields) {
                                                if (error) {
                                                    var obj = {
                                                        "status": 0,
                                                        "message": "Something wrong please try again."
                                                    }
                                                    res.send(obj);
                                                } else {
                                                    var obj = {
                                                        "status": 1,
                                                        "result": result,
                                                        "message": "Record added successfully."
                                                    }
                                                    res.send(obj);
                                                }
                                            })
                                    }
                                })
                            } else {
                                con.query('SELECT id, patient_id, DATE_FORMAT(start_date, "%m/%d/%Y") as start_date,DATE_FORMAT(end_date, "%m/%d/%Y") as end_date, fin_charge_waived, late_fee_waived, interest_waived, due_to, status  FROM patient_payment_pause WHERE patient_id = ? ',
                                    [req.body.patient_id]
                                    , function (error, result, fields) {
                                        if (error) {
                                            var obj = {
                                                "status": 0,
                                                "message": "Something wrong please try again."
                                            }
                                            res.send(obj);
                                        } else {
                                            var obj = {
                                                "status": 1,
                                                "result": result,
                                                "message": "Record added successfully."
                                            }
                                            res.send(obj);
                                        }
                                    })
                            }

                        }
                    })
            } else {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
            }

        })
        return false;


    });


    /*
    * for admin upload patient document
    */
    app.post('/api/admin-upload-patient-document', dataupload, jwtMW, (req, res) => {
        //console.log(req.file)
        //console.log(req.body)
        //return false 
        con = require('../db');
        const path = require('path');
        let now = new Date();
        var obj = {};
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var curentfile = {
            filename: req.file.filename,
            path: path.join(__dirname, '..', req.file.path),
            customer_id: req.body.customer_id,
        }
        var oneDrive = require('./microsoft/oneDrive.js');

        con.query('SELECT patient_ac FROM patient WHERE patient_id = ? ',
            [req.body.customer_id]
            , function (error, cusAc, fields) {
                if (error) {
                    var error = "Something wrong!"
                    res.send(error);
                } else {
                    const dir = './uploads/customer/' + cusAc[0].patient_ac + '/';
                    let distPath = dir + curentfile.filename;
                    const fs = require('fs');
                    fs.copyFile(curentfile.path, distPath, (err) => {
                        if (err) {
                            var error = "Something wrong!"
                            res.send(error);
                        } else {

                            oneDrive.getOneDriveLogin(function (loginOne) {
                                if (loginOne.status == 1) {
                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                        if ((one.status == 1)) {
                                            req.file.item_id = one.item_id;

                                            fs.unlinkSync(curentfile.path)
                                        } else {
                                            req.file.item_id = '';
                                        }
                                        con.query('INSERT INTO `doc_repo` SET '
                                            + 'name=?, '
                                            + 'file_path=?, '
                                            + 'item_id=?, '
                                            + 'patient_id=?, '
                                            + 'date_created=?, '
                                            + 'date_modified=?, '
                                            + 'created_by=?, '
                                            + 'modified_by=?',
                                            [
                                                req.file.filename,
                                                req.file.path,
                                                req.file.item_id,
                                                req.body.customer_id,
                                                current_date,
                                                current_date,
                                                req.body.current_user_id,
                                                req.body.current_user_id
                                            ]
                                            , function (error, docs) {
                                                if (error) {
                                                    var error = "Something wrong!"
                                                    res.send(error);
                                                } else {
                                                    obj.docs = docs
                                                    obj.customer_id = req.body.customer_id;
                                                    obj.message = 'Document uploaded successfully!';
                                                    res.send(obj);
                                                }
                                            })

                                    });

                                } else {
                                    res.send(loginOne);
                                }
                            })

                        }
                    });
                }
            })

    });

    /*
    * Get Customer all documents
    */
    app.get('/api/admin-get-patient-documents', jwtMW, (req, res) => {
        con = require('../db');
        var oneDrive = require('./microsoft/oneDrive.js');

        var obj = {};
        con.beginTransaction(function (err) {
            if (err) {
                obj.status = 0;
                obj.message = "Something wrong please try again.";
                res.send(obj);
            }
            con.query('SELECT '
                + 'payment_plan.pp_id, '
                + 'payment_plan.plan_number '
                + 'FROM payment_plan '
                + 'INNER JOIN credit_applications ON credit_applications.application_id=payment_plan.application_id '
                + 'WHERE credit_applications.patient_id = ? ',
                [
                    req.query.id
                ]
                , function (err, planDocuments) {
                    if (err) {
                        con.rollback(function () {
                            obj.status = 0;
                            obj.message = "Something wrong while getting all documents.";
                            res.send(obj);
                        });

                    } else {
                        con.query('SELECT '
                            + 'credit_applications.application_id, '
                            + 'payment_plan.pp_id '
                            + 'FROM credit_applications '
                            + 'LEFT JOIN payment_plan ON payment_plan.application_id = credit_applications.application_id '
                            + 'WHERE credit_applications.patient_id =? ',
                            [
                                req.query.id
                            ]
                            , function (err, customerDetails) {
                                if (err) {
                                    con.rollback(function () {
                                        obj.status = 0;
                                        obj.message = "Something wrong while getting all documents.";
                                    });
                                    res.send(obj);
                                } else {
                                    let appId = [...new Set(customerDetails.map(item => item.application_id))];
                                    let ppId = [...new Set(customerDetails.map(item => item.pp_id))];
                                    con.query('SELECT '
                                        + 'doc_repo.file_path, '
                                        + 'doc_repo.name, '
                                        + 'doc_repo.item_id '

                                        + 'FROM doc_repo '

                                        //+ 'WHERE (doc_repo.patient_id =? OR doc_repo.application_id IN(?) OR doc_repo.pp_id IN(?)) '
                                        + 'WHERE doc_repo.application_id IN(?) '
                                        + 'AND doc_repo.status = ?',
                                        [
                                            appId, 1
                                        ]
                                        , function (err, allDocuments) {
                                            con.commit(function (err) {
                                                if (err) {
                                                    con.rollback(function () {
                                                        obj.status = 0;
                                                        obj.message = "Something wrong while getting all documents.";
                                                    });
                                                    res.send(obj);
                                                }
                                                obj.status = 1;
                                                obj.allDocuments = allDocuments;
                                                obj.planDocuments = planDocuments;
                                                if (allDocuments.length > 0) {
                                                    let totalPlan = allDocuments.length;
                                                    allDocuments.map((data, idx) => {
                                                        oneDrive.getOneDriveLogin(function (loginOne) {
                                                            if (loginOne.status == 1) {
                                                                oneDrive.getOneDriveFiles(loginOne.token, data.item_id, function (one) {
                                                                    if (one.status == 1) {
                                                                        data.file_path = one.file
                                                                    }
                                                                    if (0 === --totalPlan) {
                                                                        res.send(obj);
                                                                    }
                                                                })
                                                            }
                                                        })

                                                    })
                                                } else {
                                                    res.send(obj);
                                                }
                                            });
                                        });
                                }
                            })
                    }
                })


        });

    });


    /*
* for provider upload patient document
*/
    app.post('/api/provider-upload-patient-document', dataupload, jwtMW, (req, res) => {

        con = require('../db');
        const path = require('path');
        let now = new Date();
        var obj = {};
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var curentfile = {
            filename: req.file.filename,
            path: path.join(__dirname, '..', req.file.path),
            customer_id: req.body.customer_id,
        }
        var oneDrive = require('./microsoft/oneDrive.js');

        con.query('SELECT patient.patient_ac FROM patient INNER JOIN credit_applications ON credit_applications.patient_id=patient.patient_id WHERE credit_applications.application_id = ? ',
            [req.body.app_id]
            , function (error, cusAc, fields) {
                if (error) {

                    obj.status = 0;
                    obj.message = "Something wrong!"

                    res.send(obj);
                } else {
                    const dir = './uploads/customer/' + cusAc[0].patient_ac + '/';
                    let distPath = dir + curentfile.filename;
                    const fs = require('fs');
                    fs.copyFile(curentfile.path, distPath, (err) => {
                        if (err) {
                            var error = "Something wrong!"
                            res.send(error);
                        } else {

                            oneDrive.getOneDriveLogin(function (loginOne) {
                                if (loginOne.status == 1) {
                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                        if ((one.status == 1)) {
                                            req.file.item_id = one.item_id;

                                            fs.unlinkSync(curentfile.path)
                                        } else {
                                            req.file.item_id = '';
                                        }
                                        con.query('INSERT INTO `doc_repo` SET '
                                            + 'name=?, '
                                            + 'file_path=?, '
                                            + 'item_id=?, '
                                            + 'application_id=?, '
                                            + 'date_created=?, '
                                            + 'date_modified=?, '
                                            + 'created_by=?, '
                                            + 'modified_by=?',
                                            [
                                                req.file.filename,
                                                req.file.path,
                                                req.file.item_id,
                                                req.body.app_id,
                                                current_date,
                                                current_date,
                                                req.body.current_user_id,
                                                req.body.current_user_id
                                            ]
                                            , function (error, docs) {
                                                if (error) {
                                                    var error = "Something wrong!"
                                                    res.send(error);
                                                } else {
                                                    obj.docs = docs
                                                    obj.customer_id = req.body.customer_id;
                                                    obj.message = 'Document uploaded successfully!';
                                                    res.send(obj);
                                                }
                                            })

                                    });

                                } else {
                                    res.send(loginOne);
                                }
                            })

                        }
                    });
                }
            })

    });

}