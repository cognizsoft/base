module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    let now = new Date();

    app.post('/api/create-invoice', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        // create invoice
        dataSql.createInvoice(con, req.body, function (invoice) {
            if (invoice.status == 1) {
                dataSql.getAllPlansDetails(con, req.body.appid, function (result) {
                    if (result.status == 1) {
                        result.message = invoice.message
                        res.send(result);
                    } else {
                        res.send(result);
                    }
                });
            } else {
                res.send(invoice);
            }
        })

        /*dataSql.getAllPlansDetails(con, req.query.appid, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });*/

    });

    /*
    * get all plan details
    */
    app.get('/api/creditapplication-all-plan-details', jwtMW, (req, res) => {
        con = require('../db');

        var dataSql = require('./payment/payment.sql.js');
        dataSql.getAllPlansDetails(con, req.query.appid, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    /*
    * get all plan details pdf download
    */

    const pdf = require('html-pdf');
    app.post('/api/creditapplication-all-plan-details-pdf', jwtMW, (req, res) => {
        con = require('../db');

        //return false;
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getAllPlansDetails(con, req.body.appid, function (result) {
            if (result.status == 1) {
                //console.log('result-----pdf')
                //console.log(result)
                //return false;
                var pdfTemplate;
                var dir;
                var fullUrl;
                var logoimg;
                var filename;

                if (req.body.appid && req.body.planid) {
                    console.log('single plan')
                    pdfTemplate = require('./single-plan-details-pdf');

                    dir = './uploads/' + req.body.appid + '/' + req.body.planid + '/' + Math.random()
                    fullUrl = req.get('origin');
                    logoimg = fullUrl + '/logo.png';
                    filename = dir + 'single-plan-details.pdf';
                } else {
                    console.log('full plan')
                    pdfTemplate = require('./app-plan-details-pdf');

                    dir = './uploads/' + req.body.appid + '/' + Math.random()
                    fullUrl = req.get('origin');
                    logoimg = fullUrl + '/logo.png';
                    filename = dir + 'app-plan-details.pdf';
                }

                //console.log('result--------====')
                //console.log(result)
                //return false;




                pdf.create(pdfTemplate(result, req.body.planid, logoimg), {}).toFile(filename, (err) => {

                    if (err) {

                        var obj = {
                            "status": 0,
                            "message": "Currently we are not able to generate pdf",
                        }
                        res.send(obj);
                    } else {
                        var obj = {
                            "status": 1,
                            "type": (req.body.appid && req.body.planid) ? 1 : 2,
                            "result": filename
                        }
                        res.send(obj);

                    }
                })
            }
        });



    })

    /*
    * get data and send email report
    */
    app.get('/api/send-customer-invoice-report', jwtMW, (req, res) => {
        con = require('../db');
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        var dataSql = require('./payment/payment.sql.js');
        var dataSqlC = require('./credit/creditSql.js');
        var objE = {};
        var obj = {};
        dataSql.getSingleInstallment(con, req.query.id, function (result1) {
            if (result1.status == 1) {
                res.send("something wrong")
            } else {

                dataSql.getInvoicePlans(con, req.query.id, function (result2) {
                    if (result2.status == 1) {
                        objE.result1 = result1;
                        objE.result2 = result2;

                        //console.log(result2)

                        var chk_late_fee_arr = result1.payment_billing_late_fee;

                        var invoice_date = date.format(now, 'MM/DD/YYYY');
                        var invoice_number = result2.invoice_dtl[0].invoice_number;

                        var full_name = result2.invoice_dtl[0].f_name + ' ' + result2.invoice_dtl[0].m_name + ' ' + result2.invoice_dtl[0].l_name;
                        var address = result2.invoice_dtl[0].address1;
                        var address_city = result2.invoice_dtl[0].City;
                        var address_zip = result2.invoice_dtl[0].zip_code;
                        var phone = result2.invoice_dtl[0].peimary_phone;

                        var ac_no = result2.invoice_dtl[0].patient_ac;

                        //var pddArr = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 0 ) : '';
                        var pymnt_due_date = result2.invoice_dtl[0].due_date;

                        var amount_due = result2.invoice_dtl[0].payment_amount;
                        //var amount_due_length = result1.single_installment_payment.length
                        /*if(result1.single_installment_payment) {
                            
                            result1.single_installment_payment.forEach(function(data) {
                                if(data.installment_amt) {
                                    amount_due += data.installment_amt;
                                }
                            })
                            
                        }*/


                        var master_late_fee = result2.late_fee;

                        //var late_fee_chk = (new Date(pymnt_due_date) < new Date()) ? late_fee : 0
                        //var late_fee_after_text = (new Date(pymnt_due_date) < new Date()) ? 'after('+pymnt_due_date+')' : ''

                        if (result2.plan) {
                            var loan_plan_info = result2.plan.reduce(function (r, a) {

                                r[a.pp_id] = r[a.pp_id] || [];

                                r[a.pp_id].push(a);
                                return r;

                            }, []);
                        }


                        var loan_plan_info_arr = [];
                        if (loan_plan_info) {
                            loan_plan_info.forEach(function (data) {
                                if (data) {
                                    loan_plan_info_arr.push(data)
                                }
                            })
                        }

                        /*var loan_plan_info_arr = result2.plan && result2.plan.reduce(function (accumulator, currentValue, currentindex) {
                            if (!accumulator[currentValue.pp_id]) {
                                // get recived amount
                                var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
                                   if (planindex == 0) {
                                      accumulator = 0;
                                   }
                                   accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
                                   return accumulator;
                                }, 0);
                                accumulator[currentValue.pp_id] = { paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.status, invoice_exist: currentValue.invoice_exist, note: currentValue.note };
                             } else {
                                //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
                             }
                    
                             return accumulator;
                        }, []);

                        console.log('loan_plan_info_arr')
                        console.log(loan_plan_info_arr)*/

                        var loan_amount = result2.invoice_dtl[0].amount;

                        var payment_term = result2.invoice_dtl[0].term_month;

                        var interest_rate = result2.invoice_dtl[0].discounted_interest_rate;

                        //====//
                        /*var getPaidBalnc = (result2.payment_plan_details) ? result2.payment_plan_details.filter(x => x.paid_flag == 1 ) : '';
                        if(getPaidBalnc) {
                            var paidInsTotal = 0;
                            for (var i = 0; i<getPaidBalnc.length; i++) {
                                paidInsTotal += parseFloat(getPaidBalnc[i].payment_amount);
                            }
                        }
                        var current_balance = result2.plan[0].credit_amount-paidInsTotal;*/
                        //====//

                        //var fincharge = ((result1.single_installment_payment[0].installment_amt/payment_term)*interest_rate)/100;
                        var late_fee = result2.invoice_dtl[0].late_fee_received;
                        var prev_late_fee = (result2.invoice_dtl[0].previous_late_fee) ? result2.invoice_dtl[0].previous_late_fee : 0;

                        var fincharge = result2.invoice_dtl[0].fin_charge_amt;
                        var prev_fincharge = (result2.invoice_dtl[0].previous_fin_charge) ? result2.invoice_dtl[0].previous_fin_charge : 0;
                        /*var totalEnclosed_amt = 0;
                        if(result1.single_installment_payment) {
    
                            result1.single_installment_payment.forEach(function(data) {
                    
                                fincharge += ((data.installment_amt/data.payment_term)*data.interest_rate)/100;
                        
                                totalEnclosed_amt += parseFloat(data.installment_amt)
                    
                            })
                    
                        }*/

                        if (result2.invoice_dtl) {

                            var pa = parseFloat(result2.invoice_dtl[0].payment_amount);
                            var lfr = parseFloat(result2.invoice_dtl[0].late_fee_received);
                            var lfp = (result2.invoice_dtl[0].previous_late_fee) ? parseFloat(result2.invoice_dtl[0].previous_late_fee) : 0;
                            var fca = parseFloat(result2.invoice_dtl[0].fin_charge_amt);
                            var fcp = (result2.invoice_dtl[0].previous_fin_charge) ? parseFloat(result2.invoice_dtl[0].previous_fin_charge) : 0;
                            var ttotal = pa + lfr + lfp + fca + fcp;
                            var three_per_charge = parseFloat(((ttotal * 3) / 100).toFixed(2))

                        }

                        if (result2.invoice_dtl) {

                            var pa = parseFloat(result2.invoice_dtl[0].payment_amount);
                            var lfr = parseFloat(result2.invoice_dtl[0].late_fee_received);
                            var lfp = (result2.invoice_dtl[0].previous_late_fee) ? parseFloat(result2.invoice_dtl[0].previous_late_fee) : 0;
                            var fca = parseFloat(result2.invoice_dtl[0].fin_charge_amt);
                            var fcp = (result2.invoice_dtl[0].previous_fin_charge) ? parseFloat(result2.invoice_dtl[0].previous_fin_charge) : 0;

                            var total_credit = pa + lfr + lfp + fca + fcp;
                            var percentage = parseFloat(((total_credit * 3) / 100)).toFixed(2)
                            var total_amt_due = (parseFloat(total_credit) + parseFloat(percentage)).toFixed(2)

                        }



                        var last_mnthly_pymnt = (result2.invoice_last_pymt.length !== 0) ? result2.invoice_last_pymt[0].payment_amount : 0;

                        var last_payment_date = (result2.invoice_last_pymt.length !== 0) ? result2.invoice_last_pymt[0].payment_date : '-';

                        var last_paid_amount = (result2.invoice_last_pymt.length !== 0) ? result2.invoice_last_pymt[0].paid_amount : 0;

                        if (result2.invoice_last_pymt !== undefined && result2.invoice_last_pymt.length > 0) {

                            var pv_lf = (result2.invoice_last_pymt[0].previous_late_fee) ? result2.invoice_last_pymt[0].previous_late_fee : 0;
                            var lf = (result2.invoice_last_pymt[0].late_fee_received) ? result2.invoice_last_pymt[0].late_fee_received : 0;

                            if (result2.invoice_last_pymt[0].mdv_late_percentage) {
                                var lateff = ((parseFloat(pv_lf) + parseFloat(lf)) - ((parseFloat(pv_lf) + parseFloat(lf)) * result2.invoice_last_pymt[0].mdv_late_percentage) / 100)

                                var last_late_fee = lateff.toFixed(2);
                            }

                        } else {
                            var last_late_fee = 0;
                        }

                        ////////

                        if (result2.invoice_last_pymt !== undefined && result2.invoice_last_pymt.length > 0) {

                            var pv_fc = (result2.invoice_last_pymt[0].previous_fin_charge) ? result2.invoice_last_pymt[0].previous_fin_charge : 0;
                            var fc = (result2.invoice_last_pymt[0].fin_charge_amt) ? result2.invoice_last_pymt[0].fin_charge_amt : 0;

                            if (result2.invoice_last_pymt[0].mdv_fin_percentage) {
                                var finCh = ((parseFloat(pv_fc) + parseFloat(fc)) - ((parseFloat(pv_fc) + parseFloat(fc)) * result2.invoice_last_pymt[0].mdv_fin_percentage) / 100)

                                var last_fin_ch = finCh.toFixed(2);
                            }

                        } else {
                            var last_fin_ch = 0;
                        }

                        ///

                        if (result2.invoice_last_pymt !== undefined && result2.invoice_last_pymt.length > 0) {

                            var pv_lf = (result2.invoice_last_pymt[0].previous_late_fee) ? result2.invoice_last_pymt[0].previous_late_fee : 0;
                            var lf = (result2.invoice_last_pymt[0].late_fee_received) ? result2.invoice_last_pymt[0].late_fee_received : 0;

                            var pv_fc = (result2.invoice_last_pymt[0].previous_fin_charge) ? result2.invoice_last_pymt[0].previous_fin_charge : 0;
                            var fc = (result2.invoice_last_pymt[0].fin_charge_amt) ? result2.invoice_last_pymt[0].fin_charge_amt : 0;

                            var lfee;
                            var fchh;

                            if (result2.invoice_last_pymt[0].mdv_fin_percentage) {
                                var finCh = ((parseFloat(pv_fc) + parseFloat(fc)) - ((parseFloat(pv_fc) + parseFloat(fc)) * result2.invoice_last_pymt[0].mdv_fin_percentage) / 100)

                                fchh = finCh.toFixed(2);
                            } else {
                                fchh = parseFloat(pv_fc) + parseFloat(fc)
                            }

                            if (result2.invoice_last_pymt[0].mdv_late_percentage) {
                                var lateff = ((parseFloat(pv_lf) + parseFloat(lf)) - ((parseFloat(pv_lf) + parseFloat(lf)) * result2.invoice_last_pymt[0].mdv_late_percentage) / 100)

                                lfee = lateff.toFixed(2);
                            } else {
                                lfee = parseFloat(pv_lf) + parseFloat(lf)
                            }

                            var totl = parseFloat(lfee) + parseFloat(fchh) + parseFloat(result2.invoice_last_pymt[0].payment_amount);

                            var last_total_amount = totl.toFixed(2);

                        } else {
                            var last_total_amount = 0;
                        }


                        //var emialTemp = require('./email.js');
                        var locals = {
                            invoice_msg: req.query.invoice_msg,
                            invoice_date: invoice_date,
                            invoice_number: invoice_number,
                            ac_no: ac_no,
                            pymnt_due_date: pymnt_due_date,
                            amount_due: parseFloat(amount_due).toFixed(2),
                            //late_fee_after: parseFloat(late_fee).toFixed(2),
                            //late_fee_after_text: late_fee_after_text,
                            master_late_fee: master_late_fee,
                            full_name: full_name,
                            address: address,
                            address_city: address_city,
                            address_zip: address_zip,
                            phone: phone,

                            three_per_charge: three_per_charge,

                            regu_mnthly_pymnt: parseFloat(amount_due).toFixed(2),
                            late_fee: parseFloat(late_fee).toFixed(2),
                            prev_late_fee: parseFloat(prev_late_fee).toFixed(2),
                            fin_charge: parseFloat(fincharge).toFixed(2),
                            prev_fin_charge: parseFloat(prev_fincharge).toFixed(2),
                            //late_fee: parseFloat(late_fee_chk).toFixed(2),
                            //fin_charge: parseFloat(fincharge).toFixed(2),
                            total_amt_due: parseFloat(total_amt_due).toFixed(2),

                            last_reg_mnthly_pymnt: parseFloat(last_mnthly_pymnt).toFixed(2),
                            last_payment_date: last_payment_date,
                            last_paid_amount: parseFloat(last_paid_amount).toFixed(2),
                            last_late_fee: parseFloat(last_late_fee).toFixed(2),
                            last_fin_ch: parseFloat(last_fin_ch).toFixed(2),
                            last_total_amount: parseFloat(last_total_amount).toFixed(2),

                            loan_plan_info: loan_plan_info_arr
                        };


                        /*var toEmailAddress = result2.invoice_dtl[0].email;
                        var template = 'customer-invoice';
                        emialTemp.emailTemplate(toEmailAddress, locals, template);*/

                        const pdf = require('html-pdf');
                        const pdfTemplate = require('./payment/customer-invoice-pdf');
                        const dir = './uploads/customer/' + ac_no;
                        var filename = dir + '/customer-invoice-' + invoice_number + '.pdf';

                        var fullUrl = req.get('origin');
                        var logoimg = fullUrl + '/logo.png';

                        pdf.create(pdfTemplate(locals, loan_plan_info_arr, logoimg), {}).toFile(filename, (err) => {
                            if (err) {

                                obj.plan = {
                                    "status": 0,
                                    "message": "Currently we are not able to print payment plan",
                                }
                                //res.send(obj);
                            } else {

                                const path = require('path');
                                var curentfile = {
                                    filename: 'invoice-number-' + invoice_number + '.pdf',
                                    path: path.join(__dirname, '..', filename),
                                    customer_id: ac_no,
                                }
                                //console.log(path.join(__dirname, '..', filename))
                                var oneDrive = require('./microsoft/oneDrive.js');
                                oneDrive.getOneDriveLogin(function (loginOne) {
                                    if (loginOne.status == 1) {
                                        oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                            //res.send(one);
                                            if ((one.status == 1)) {

                                                var context = {
                                                    customer_name: full_name
                                                }

                                                var emialTemp = require('./emailplan.js');
                                                var template = 'Customer Invoice';

                                                emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                    if (Result.status == 1) {

                                                        var emailOpt = {
                                                            toEmail: result2.invoice_dtl[0].email,
                                                            subject: Result.result[0].template_subject,
                                                            attachment_name: 'customer-invoice-' + invoice_number + '.pdf',
                                                            attachment: path.join(__dirname, '..', filename)
                                                        }

                                                        var html = Result.result[0].template_content;

                                                        emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                            const fs = require('fs');
                                                            fs.unlinkSync(curentfile.path);
                                                            console.log(emailResult)
                                                            if (emailResult.status == 1) {
                                                                var obj = {
                                                                    "status": 1,
                                                                    "message": "Email sent successfully"
                                                                }
                                                                obj.locals = locals
                                                                res.send(obj)
                                                            } else {
                                                                var obj = {
                                                                    "status": 0,
                                                                    "message": "Email not sent successfully"
                                                                }
                                                                obj.locals = locals
                                                                res.send(obj)
                                                            }
                                                        });
                                                    } else {
                                                        var obj = {
                                                            "status": 0,
                                                            "message": "Template detail not found"
                                                        }
                                                        obj.locals = locals
                                                        res.send(obj)
                                                    }

                                                });

                                                //curentfile.item_id = one.item_id;
                                            } else {
                                                curentfile.item_id = '';
                                            }

                                        })
                                    }
                                })
                                obj.file = {
                                    "status": 1,
                                    "file_path": filename
                                }
                                //res.send(obj);
                            }
                        })
                    } else {
                        res.send("something wrong")
                    }

                });
                //res.send(objE);
            }
        });
        /*dataSqlC.getSinglePlans(con, req.query.app_id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
    
        });*/
    });
    /*
    * pay installment insert
    */
    app.post('/api/pay-installment-insert', jwtMW, (req, res) => {
        con = require('../db');

        var dataSql = require('./payment/payment.sql.js');

        dataSql.payInstallmentInsert(con, req.body, function (result) {
            /*if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }*/
            dataSql.customerDetails(con, req.body, function (customerRes) {

                if (customerRes.status == 1) {
                    var emialTemp = require('./emailplan.js');
                    var template = 'Payment Received';
                    emialTemp.customEmailTemplateDetails(template, function (tmpResult) {
                        if (tmpResult.status == 1) {
                            var emailOpt = {
                                toEmail: customerRes.customer.email,
                                subject: tmpResult.result[0].template_subject,
                            }
                            var context = {
                                customer_name: customerRes.customer.f_name + ' ' + customerRes.customer.m_name + ' ' + customerRes.customer.l_name,
                                invoice_number: customerRes.invoiceNumber,
                                invoice_status: customerRes.invoiceStatus,
                                payment_method: customerRes.payMethod,
                                comment: req.body.comments,
                                invoice_amount: req.body.additional_amount,
                            }

                            var html = tmpResult.result[0].template_content;
                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                res.send(result);
                            })
                        } else {
                            res.send(result);
                        }
                    })
                } else {
                    res.send(result);
                }
            });
        });
    });

    /*
    * get single plan details on invoice admin
    */
    app.get('/api/application-plan-singleInvoice-payment-details', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getInvoicePlans(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

	/*
    * get single plan details in row
    */
    app.get('/api/application-plan-singleInstallment-payment-details', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getSingleInstallment(con, req.query.invoiceId, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }
        });
    });
    /*
    * get master fee option
    */
    app.get('/api/payment-master-fee-option', jwtMW, (req, res) => {
        con = require('../db');
        // this function use for get credit application details

        var dataSql = require('./payment/payment.sql.js');
        dataSql.getPaymentMasterFeeOption(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    /*
    * get waiver type option
    */
    app.get('/api/payment-late-fee-waiver-type-option', jwtMW, (req, res) => {
        con = require('../db');

        var dataSql = require('./payment/payment.sql.js');
        dataSql.getLateFeeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });

    app.get('/api/payment-fin-charge-waiver-type-option', jwtMW, (req, res) => {
        con = require('../db');

        var dataSql = require('./payment/payment.sql.js');
        dataSql.getFinChargeWaiverType(con, req.query.id, function (result) {
            if (result.status == 1) {
                res.send(result);
            } else {
                res.send(result);
            }

        });

    });
    /*
    * get data for view recipit
    */
    app.get('/api/view-recepit', jwtMW, (req, res) => {
        con = require('../db');


        var dataSql = require('./payment/payment.sql.js');
        var obj = {};
        dataSql.customerInvoice(con, req.query.app_id, function (result) {
            res.send(result);
        });

    });

    /**
     * regenerate-plan
     */
    app.post('/api/regenerate-plan/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        var obj = {};
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getCurrentPlanDetails(con, req.body.data.plan_id, function (result) {
            if (result.status == 1) {
                dataSql.cancelPlan(con, req.body, function (cancelResult) {
                    if (cancelResult.status == 1) {
                        var appDetails = [{ application_id: result.data[0].application_id, remaining_amount: result.data[0].remaining_amount + result.data[0].loan_amount, patient_id: result.data[0].patient_id }];

                        var customerData = {
                            details: result.data[0],
                            provider_id: result.data[0].provider_id,
                            application_id: result.data[0].application_id,
                        };

                        customerData.details.remaining_amount = parseFloat(customerData.details.remaining_amount) + parseFloat(result.data[0].loan_amount) - parseFloat(req.body.data.loan_amount);
                        //customerData.details = customerData.details.payment_term_month
                        customerData.details.doctor = customerData.details.doctor_id;
                        customerData.details.loan_amount = req.body.data.loan_amount;
                        customerData.details.procedure_date = req.body.data.procedure_date;
                        customerData.details.procedure_amount = req.body.data.procedure_amount;
                        customerData.details.customerSignature = req.body.data.customerSignature;
                        customerData.details.witnessSignature = req.body.data.witnessSignature;


                        customerData.details.id_name = req.body.data.id_name;
                        customerData.details.id_number = req.body.data.id_number;
                        customerData.details.expiry_date = req.body.data.expiry_date;
                        customerData.details.agree_with = req.body.data.agree_with;
                        customerData.details.name_of_borrower = req.body.data.name_of_borrower;
                        customerData.details.agreement_date = req.body.data.agreement_date;
                        customerData.details.current_date = date.format(now, 'MM/DD/YYYY');
                        customerData.details.co_id_name = req.body.data.co_id_name;
                        customerData.details.co_id_number = req.body.data.co_id_number;
                        customerData.details.co_expiry_date = req.body.data.co_expiry_date;
                        customerData.details.cosignerSignature = req.body.data.cosignerSignature;
                        customerData.details.co_name_of_borrower = req.body.data.co_name_of_borrower;
                        customerData.details.co_agreement_date = req.body.data.co_agreement_date;


                        var scroeCal = require('./credit/creditScore.js');
                        if (result.data[0].loan_type == 1) {
                            scroeCal.getMonthlyPlanRe(req.body.data, result.data, function (plan) {
                                if (plan.length !== 0) {
                                    var dataCreate = require('./credit/creditSql.js');
                                    dataCreate.createPlan(con, appDetails, plan, req.body.current_user_id, customerData, function (createStatus) {
                                        if (createStatus.status == 1) {
                                            //console.log(createStatus)
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {
                                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;

                                                    // generate plan pdf according to
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-regenerate-agreement');
                                                    const dir = './uploads/customer/' + customerData.details.patient_ac;
                                                    var filename = dir + '/plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';
                                                    pdf.create(pdfTemplate(plan, customerData, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                            var obj = {
                                                                status: 0,
                                                                message: 'Currently not able to generate plan.'
                                                            }
                                                            res.send(obj);
                                                        } else {
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: customerData.details.patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            //var emialTemp = require('./emailplan.js');
                                                                            var context = {
                                                                                customer_name: customerData.details.f_name + ' ' + customerData.details.l_name,
                                                                            };

                                                                            /*var template = 'plan_regenerate_agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/

                                                                            var emialTemp = require('./emailplan.js');
                                                                            var template = 'Plan Agreement Regenerated';

                                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                                if (Result.status == 1) {

                                                                                    var emailOpt = {
                                                                                        toEmail: customerData.details.email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-regenerate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            obj.template = "Template email sent successfully!"
                                                                                        } else {
                                                                                            obj.template = "Template email not sent successfully!"
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    obj.template = "Template details not found."
                                                                                }

                                                                            });


                                                                            // end here
                                                                            curentfile.item_id = one.item_id;
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        customerData.pp_id = createStatus.paymentPlan;
                                                                        customerData.current_user_id = req.body.current_user_id;
                                                                        dataCreate.uploadAgrementApplcation(con, customerData, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                        obj = {
                                                                            status: 1,
                                                                            message: 'Payment plan created successfully.'
                                                                        }
                                                                        res.send(obj)
                                                                    })
                                                                } else {
                                                                    obj = {
                                                                        status: 1,
                                                                        message: 'Payment plan created successfully.'
                                                                    }
                                                                    res.send(obj)
                                                                }
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    obj = {
                                                        status: 0,
                                                        message: 'Currently not able to generate plan.'
                                                    }
                                                    res.send(obj)
                                                }
                                            })


                                        } else {
                                            obj = {
                                                status: 0,
                                                message: 'Currently not able to generate plan.'
                                            }
                                            res.send(obj)
                                        }
                                    })
                                } else {
                                    obj = {
                                        status: 0,
                                        message: 'Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        } else {
                            scroeCal.getMonthlyPlanBudgetRe(req.body.data, result.data, function (plan) {
                                if (plan.length !== 0) {
                                    var dataCreate = require('./credit/creditSql.js');
                                    dataCreate.createPlan(con, appDetails, plan, req.body.current_user_id, customerData, function (createStatus) {
                                        if (createStatus.status == 1) {
                                            //console.log(createStatus)
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {
                                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;

                                                    // generate plan pdf according to
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-regenerate-agreement');
                                                    const dir = './uploads/customer/' + customerData.details.patient_ac;
                                                    var filename = dir + '/plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';
                                                    pdf.create(pdfTemplate(plan, customerData, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                            var obj = {
                                                                status: 0,
                                                                message: 'Currently not able to generate plan.'
                                                            }
                                                            res.send(obj);
                                                        } else {
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: customerData.details.patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            var emialTemp = require('./emailplan.js');
                                                                            var context = {
                                                                                customer_name: customerData.details.f_name + ' ' + customerData.details.l_name,
                                                                            };

                                                                            /*var template = 'plan_regenerate_agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/

                                                                            var emialTemp = require('./emailplan.js');
                                                                            var template = 'Plan Agreement Regenerated';

                                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                                if (Result.status == 1) {

                                                                                    var emailOpt = {
                                                                                        toEmail: customerData.details.email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-regenerate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            obj.template = "Template email sent successfully!"
                                                                                        } else {
                                                                                            obj.template = "Template email not sent successfully!"
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    obj.template = "Template details not found."
                                                                                }

                                                                            });

                                                                            // end here
                                                                            curentfile.item_id = one.item_id;
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }
                                                                        customerData.pp_id = createStatus.paymentPlan;
                                                                        customerData.current_user_id = req.body.current_user_id;
                                                                        dataCreate.uploadAgrementApplcation(con, customerData, curentfile, function (result) {
                                                                            //res.send(result);
                                                                        });
                                                                        obj = {
                                                                            status: 1,
                                                                            message: 'Payment plan created successfully.'
                                                                        }
                                                                        res.send(obj)
                                                                    })
                                                                } else {
                                                                    obj = {
                                                                        status: 1,
                                                                        message: 'Payment plan created successfully.'
                                                                    }
                                                                    res.send(obj)
                                                                }
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    obj = {
                                                        status: 0,
                                                        message: 'Currently not able to generate plan.'
                                                    }
                                                    res.send(obj)
                                                }
                                            })


                                        } else {
                                            obj = {
                                                status: 0,
                                                message: 'Currently not able to generate plan.'
                                            }
                                            res.send(obj)
                                        }
                                    })
                                } else {
                                    obj = {
                                        status: 0,
                                        message: 'Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        }
                    } else {
                        obj = {
                            status: 0,
                            message: 'Currently not able to generate plan.'
                        }
                        res.send(obj)
                    }
                })
            } else {
                obj = {
                    status: 0,
                    message: 'Currently not able to generate plan.'
                }
                res.send(obj)
            }
            //res.send(result);
        });

    })

    /**
     * /api/regenerate-plan-process
     */

    app.post('/api/regenerate-plan-process/', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getCurrentPlanDetails(con, req.body.data.plan_id, function (result) {
            
            //console.log(result.data[0])
            if (result.status == 1) {
                var scroeCal = require('./credit/creditScore.js');
                if (result.data[0].loan_type == 1) {
                    scroeCal.getMonthlyPlanRe(req.body.data, result.data, function (plan) {
                        if (plan.length !== 0) {
                            var providerFile = require('./invoice/invoiceSql.js');
                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                if (providerDetails.status == 1) {
                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;
                                    var obj = {
                                        "status": 1,
                                        "planDetails": plan[0],
                                        "providerDetails": providerDetails.providerDetails,
                                    }
                                    res.send(obj);
                                } else {
                                    var obj = {
                                        status: 0,
                                        message: 'Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        } else {
                            var obj = {
                                status: 0,
                                message: 'Currently not able to generate plan.'
                            }
                            res.send(obj)
                        }
                    })
                } else {
                    scroeCal.getMonthlyPlanBudgetRe(req.body.data, result.data, function (plan) {
                        if (plan.length !== 0) {
                            var providerFile = require('./invoice/invoiceSql.js');
                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                if (providerDetails.status == 1) {
                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;
                                    var obj = {
                                        "status": 1,
                                        "planDetails": plan[0],
                                        "providerDetails": providerDetails.providerDetails,
                                    }
                                    res.send(obj);
                                } else {
                                    var obj = {
                                        status: 0,
                                        message: 'Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        } else {
                            var obj = {
                                status: 0,
                                message: 'Currently not able to generate plan.'
                            }
                            res.send(obj)
                        }
                    })
                }

            } else {
                var obj = {
                    status: 0,
                    message: 'Currently not able to generate plan.'
                }
                res.send(obj)
            }
        });
    })
    /*
    * get data for get close plan option
    */
    app.get('/api/option-to-close-plan', jwtMW, (req, res) => {
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        var obj = {};
        dataSql.getOptionToClose(con, req.query.pp_id, function (result) {
            res.send(result);
        });

    });

    /*
    * Plan action
    */
    app.post('/api/plan-action/', jwtMW, (req, res) => {

        con = require('../db');
        var dataInvoice = require('./payment/payment.sql.js');
        var obj = {};

        if (req.body.data.action_type == 0) {
            if (req.body.data.confirmation_provider == 1) {
                dataInvoice.providerConfirmation(con, req.body, function (result) {
                    res.send(result);
                });
            } else {
                dataInvoice.closePlan(con, req.body, function (result) {
                    res.send(result);
                });
            }
        } else if (req.body.data.action_type == 1) {
            dataInvoice.payfullPlan(con, req.body, function (result) {
                res.send(result);
            });
        } else if (req.body.data.action_type == 2) {
            dataInvoice.newSettementPlan(con, req.body, function (result) {

                if (result.status == 1) {
                    var dataSql = require('./credit/creditSql.js');
                    dataSql.createPlanWithSettement(con, req.body, function (createStatus) {
                        if (createStatus.status == 1) {
                            const pdf = require('html-pdf');
                            const pdfTemplate = require('./credit/plan-settlement-agreement');
                            const dir = './uploads/provider/' + req.body.data.providerDetails.provider_id;
                            var filename = dir + '/plan-settlement-agreement.pdf';
                            var fullUrl = req.get('origin');
                            var logoimg = fullUrl + '/logo.png';
                            let date = require('date-and-time');
                            req.body.current_date = date.format(now, 'MM/DD/YYYY');
                            pdf.create(pdfTemplate(req.body, logoimg), {}).toFile(filename, (err) => {
                                if (err) {

                                    createStatus.obj = {
                                        "status": 0,
                                        "message": "Currently we are not able to print payment plan",
                                    }
                                    //res.send(obj);
                                } else {
                                    var data = {
                                        pp_id: createStatus.paymentPlan,
                                        current_user_id: req.body.current_user_id,
                                    }

                                    const path = require('path');
                                    var curentfile = {
                                        filename: 'plan-agreement-' + req.body.data.customerDetails.application_no + '-' + createStatus.paymentPlan + '.pdf',
                                        path: path.join(__dirname, '..', filename),
                                        customer_id: req.body.data.customerDetails.patient_ac,
                                    }
                                    var oneDrive = require('./microsoft/oneDrive.js');
                                    oneDrive.getOneDriveLogin(function (loginOne) {
                                        if (loginOne.status == 1) {
                                            oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                if ((one.status == 1)) {

                                                    var context = {
                                                        customer_name: req.body.data.customerDetails.f_name + ' ' + req.body.data.customerDetails.l_name,
                                                    };

                                                    var emialTemp = require('./emailplan.js');
                                                    var template = 'Plan Agreement Generate';

                                                    emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                        if (Result.status == 1) {

                                                            var emailOpt = {
                                                                toEmail: req.body.data.customerDetails.email,
                                                                subject: Result.result[0].template_subject,
                                                                attachment_name: 'plan-settlement-agreement.pdf',
                                                                attachment: path.join(__dirname, '..', filename)
                                                            }

                                                            var html = Result.result[0].template_content;

                                                            emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                const fs = require('fs');
                                                                fs.unlinkSync(curentfile.path);
                                                                if (emailResult.status == 1) {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Email sent successfully!"
                                                                    }
                                                                } else {
                                                                    var Eobj = {
                                                                        "status": 0,
                                                                        "message": "Email not sent successfully!"
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            var Eobj = {
                                                                "status": 0,
                                                                "message": "Template details not found."
                                                            }
                                                        }

                                                    });
                                                    curentfile.item_id = one.item_id;
                                                } else {
                                                    curentfile.item_id = '';
                                                }
                                                dataSql.uploadAgrementApplcation(con, data, curentfile, function (result) {
                                                    //res.send(result);
                                                });
                                                let moment = require('moment');
                                                req.body.data.procedure_date = moment().format('MM/DD/YYYY');

                                                if (moment(req.body.data.procedure_date, "MM/DD/YYYY").format('DD') > 15) {
                                                    var newDate = moment(req.body.data.procedure_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                                                    newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
                                                    nextDueDate = newDate;
                                                } else {
                                                    var newDate = new Date(moment(req.body.data.procedure_date, "MM/DD/YYYY").format('YYYY'), moment(req.body.data.procedure_date, "MM/DD/YYYY").format('MM'), 0);
                                                    nextDueDate = newDate;
                                                }
                                                nextDueDate = moment(nextDueDate).format('YYYY-MM-DD');

                                                var newData = {
                                                    appid: req.body.data.customerDetails.application_id,
                                                    date: nextDueDate,
                                                    current_user_id: req.body.current_user_id,
                                                    pp_id: createStatus.paymentPlan,
                                                }
                                                dataInvoice.createInvoiceOnFlayAdmin(con, newData, function (invoice) {

                                                })
                                                res.send(createStatus);
                                            })
                                        } else {
                                            res.send(createStatus);
                                        }
                                    })


                                }
                            })

                        } else {
                            res.send(createStatus);
                        }
                    })
                } else {
                    res.send(obj);
                }
            });
            //console.log(req.body)

        } else if (req.body.data.action_type == 3) {
            dataInvoice.refundCustomerInvoice(con, req.body, function (result) {
                //console.log(result)
                if (result.status == 1) {
                    dataInvoice.refundProviderInvoice(con, req.body, function (proResult) {
                        res.send(result)
                    })
                } else {
                    res.send(result)
                }
            });

        }

    })

    /**
     * regenerate-plan
     */
    app.post('/api/regenerate-print-plan/', jwtMW, (req, res) => {
        con = require('../db');
        let now = new Date();
        var obj = {};
        
        var dataSql = require('./payment/payment.sql.js');
        dataSql.getCurrentPlanDetails(con, req.body.data.plan_id, function (result) {
            if (result.status == 1) {
                dataSql.cancelPlan(con, req.body, function (cancelResult) {
                    if (cancelResult.status == 1) {
                        var appDetails = [{ application_id: result.data[0].application_id, remaining_amount: result.data[0].remaining_amount + result.data[0].loan_amount, patient_id: result.data[0].patient_id }];

                        var customerData = {
                            details: result.data[0],
                            provider_id: result.data[0].provider_id,
                            application_id: result.data[0].application_id,
                        };

                        customerData.details.remaining_amount = parseFloat(customerData.details.remaining_amount) + parseFloat(result.data[0].loan_amount) - parseFloat(req.body.data.loan_amount);
                        //customerData.details = customerData.details.payment_term_month
                        customerData.details.doctor = customerData.details.doctor_id;
                        customerData.details.loan_amount = req.body.data.loan_amount;
                        customerData.details.procedure_date = req.body.data.procedure_date;
                        customerData.details.procedure_amount = req.body.data.procedure_amount;
                        customerData.details.customerSignature = req.body.data.customerSignature;
                        customerData.details.witnessSignature = req.body.data.witnessSignature;


                        customerData.details.id_name = req.body.data.id_name;
                        customerData.details.id_number = req.body.data.id_number;
                        customerData.details.expiry_date = req.body.data.expiry_date;
                        customerData.details.agree_with = req.body.data.agree_with;
                        customerData.details.name_of_borrower = req.body.data.name_of_borrower;
                        customerData.details.agreement_date = req.body.data.agreement_date;
                        customerData.details.current_date = date.format(now, 'MM/DD/YYYY');
                        customerData.details.co_id_name = req.body.data.co_id_name;
                        customerData.details.co_id_number = req.body.data.co_id_number;
                        customerData.details.co_expiry_date = req.body.data.co_expiry_date;
                        customerData.details.cosignerSignature = req.body.data.cosignerSignature;
                        customerData.details.co_name_of_borrower = req.body.data.co_name_of_borrower;
                        customerData.details.co_agreement_date = req.body.data.co_agreement_date;


                        var scroeCal = require('./credit/creditScore.js');
                        if (result.data[0].loan_type == 1) {
                            scroeCal.getMonthlyPlanRe(req.body.data, result.data, function (plan) {
                                if (plan.length !== 0) {
                                    var dataCreate = require('./credit/creditSql.js');
                                    dataCreate.createPrintPlan(con, appDetails, plan, req.body.current_user_id, customerData, function (createStatus) {
                                        if (createStatus.status == 1) {
                                            //console.log(createStatus)
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {
                                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;

                                                    // generate plan pdf according to
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-regenerate-agreement');

                                                    const dir = './uploads/customer/' + customerData.details.patient_ac;
                                                    var filename = dir + '/plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf';

                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';
                                                    pdf.create(pdfTemplate(plan, customerData, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                            var obj = {
                                                                status: 0,
                                                                message: '1Currently not able to generate plan.'
                                                            }
                                                            res.send(obj);
                                                        } else {
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: customerData.details.patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            //var emialTemp = require('./emailplan.js');
                                                                            var context = {
                                                                                customer_name: customerData.details.f_name + ' ' + customerData.details.l_name,
                                                                            };

                                                                            /*var template = 'plan_regenerate_agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/

                                                                            var emialTemp = require('./emailplan.js');
                                                                            var template = 'Plan Agreement Regenerated';

                                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                                if (Result.status == 1) {

                                                                                    var emailOpt = {
                                                                                        toEmail: customerData.details.email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-regenerate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email sent successfully!"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }

                                                                            });

                                                                            dataCreate.uploadAgrementApplcation(con, customerData, curentfile, function (result) {
                                                                                //res.send(result);
                                                                            });

                                                                            curentfile.item_id = one.item_id;

                                                                            customerData.pp_id = createStatus.paymentPlan;
                                                                            customerData.current_user_id = req.body.current_user_id;
                                                                            obj = {
                                                                                status: 1,
                                                                                file_status: 1,
                                                                                message: 'Payment plan created successfully.',
                                                                                file: filename
                                                                            }
                                                                            res.send(obj)
                                                                            // end here
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }

                                                                    })
                                                                } else {
                                                                    obj = {
                                                                        status: 1,
                                                                        message: 'Payment plan created successfully.'
                                                                    }
                                                                    res.send(obj)
                                                                }
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    obj = {
                                                        status: 0,
                                                        message: '2Currently not able to generate plan.'
                                                    }
                                                    res.send(obj)
                                                }
                                            })


                                        } else {
                                            obj = {
                                                status: 0,
                                                message: '3Currently not able to generate plan.'
                                            }
                                            res.send(obj)
                                        }
                                    })
                                } else {
                                    obj = {
                                        status: 0,
                                        message: '4Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        } else {
                            scroeCal.getMonthlyPlanBudgetRe(req.body.data, result.data, function (plan) {
                                if (plan.length !== 0) {
                                    var dataCreate = require('./credit/creditSql.js');
                                    dataCreate.createPrintPlan(con, appDetails, plan, req.body.current_user_id, customerData, function (createStatus) {
                                        if (createStatus.status == 1) {
                                            //console.log(createStatus)
                                            var providerFile = require('./invoice/invoiceSql.js');
                                            providerFile.getProviderDetails(con, result.data[0].provider_id, function (providerDetails) {
                                                if (providerDetails.status == 1) {
                                                    providerDetails.providerDetails.docter_name = result.data[0].docter_fname + ' ' + result.data[0].docter_l_name;

                                                    // generate plan pdf according to
                                                    const pdf = require('html-pdf');
                                                    const pdfTemplate = require('./credit/plan-regenerate-agreement');

                                                    const dir = './uploads/customer/' + customerData.details.patient_ac;
                                                    var filename = dir + '/plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf';
                                                    var fullUrl = req.get('origin');
                                                    var logoimg = fullUrl + '/logo.png';
                                                    pdf.create(pdfTemplate(plan, customerData, providerDetails.providerDetails, logoimg), {}).toFile(filename, (err) => {
                                                        if (err) {
                                                            var obj = {
                                                                status: 0,
                                                                message: '5Currently not able to generate plan.'
                                                            }
                                                            res.send(obj);
                                                        } else {
                                                            const path = require('path');
                                                            var curentfile = {
                                                                filename: 'plan-regenerate-' + customerData.details.application_no + '-' + createStatus.paymentPlan + '.pdf',
                                                                path: path.join(__dirname, '..', filename),
                                                                customer_id: customerData.details.patient_ac,
                                                            }
                                                            var oneDrive = require('./microsoft/oneDrive.js');
                                                            oneDrive.getOneDriveLogin(function (loginOne) {
                                                                if (loginOne.status == 1) {
                                                                    oneDrive.getOneDriveUpload(loginOne.token, curentfile, function (one) {
                                                                        if ((one.status == 1)) {
                                                                            // sending mail tpo customer for plan agrement
                                                                            var emialTemp = require('./emailplan.js');
                                                                            var context = {
                                                                                customer_name: customerData.details.f_name + ' ' + customerData.details.l_name,
                                                                            };

                                                                            /*var template = 'plan_regenerate_agreement';
                                                                            emialTemp.emailTemplate(toEmailAddress, locals, template, function (emailResult) {
                                                                                const fs = require('fs');
                                                                                fs.unlinkSync(curentfile.path);
                                                                            });*/

                                                                            var emialTemp = require('./emailplan.js');
                                                                            var template = 'Plan Agreement Regenerated';

                                                                            emialTemp.customEmailTemplateDetails(template, function (Result) {

                                                                                if (Result.status == 1) {

                                                                                    var emailOpt = {
                                                                                        toEmail: customerData.details.email,
                                                                                        subject: Result.result[0].template_subject,
                                                                                        attachment_name: 'plan-regenerate-agreement.pdf',
                                                                                        attachment: path.join(__dirname, '..', filename)
                                                                                    }

                                                                                    var html = Result.result[0].template_content;

                                                                                    emialTemp.customEmailTemplate(emailOpt, context, html, function (emailResult) {
                                                                                        const fs = require('fs');
                                                                                        //fs.unlinkSync(curentfile.path);
                                                                                        if (emailResult.status == 1) {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email sent successfully!"
                                                                                            }
                                                                                        } else {
                                                                                            var Eobj = {
                                                                                                "status": 0,
                                                                                                "message": "Email not sent successfully!"
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    var Eobj = {
                                                                                        "status": 0,
                                                                                        "message": "Template details not found."
                                                                                    }
                                                                                }

                                                                            });

                                                                            // end here
                                                                            curentfile.item_id = one.item_id;
                                                                            customerData.pp_id = createStatus.paymentPlan;
                                                                            customerData.current_user_id = req.body.current_user_id;
                                                                            dataCreate.uploadAgrementApplcation(con, customerData, curentfile, function (result) {
                                                                                //res.send(result);
                                                                            });
                                                                            obj = {
                                                                                status: 1,
                                                                                file_status: 1,
                                                                                message: 'Payment plan created successfully.',
                                                                                file: filename
                                                                            }
                                                                            res.send(obj)
                                                                        } else {
                                                                            curentfile.item_id = '';
                                                                        }

                                                                    })
                                                                } else {
                                                                    obj = {
                                                                        status: 1,
                                                                        message: 'Payment plan created successfully.'
                                                                    }
                                                                    res.send(obj)
                                                                }
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    obj = {
                                                        status: 0,
                                                        message: '6Currently not able to generate plan.'
                                                    }
                                                    res.send(obj)
                                                }
                                            })


                                        } else {
                                            obj = {
                                                status: 0,
                                                message: '7Currently not able to generate plan.'
                                            }
                                            res.send(obj)
                                        }
                                    })
                                } else {
                                    obj = {
                                        status: 0,
                                        message: '8Currently not able to generate plan.'
                                    }
                                    res.send(obj)
                                }
                            })
                        }
                    } else {
                        obj = {
                            status: 0,
                            message: '9Currently not able to generate plan.'
                        }
                        res.send(obj)
                    }
                })
            } else {
                obj = {
                    status: 0,
                    message: '10Currently not able to generate plan.'
                }
                res.send(obj)
            }
            //res.send(result);
        });

    })
    /*
    * Get plan details
    */

    app.get('/api/close-plan-details/', jwtMW, (req, res) => {

        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        var obj = {};
        dataSql.getclosePlanDetails(con, req.query.pp_id, function (result) {
            res.send(result);
        });
    })
    /*
    * Confirmed cancelation plan by provider
    */
    app.get('/api/confirmed-cancellation/', jwtMW, (req, res) => {
        console.log(req.query.pp_id)
        con = require('../db');
        var dataSql = require('./payment/payment.sql.js');
        var obj = {};
        dataSql.confirmedCancellation(con, req.query, function (result) {
            res.send(result);
        });
    })

}