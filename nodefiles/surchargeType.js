/*
* Title: States
* Descrpation :- This module blong to states realted api in whole application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, jwtMW) {
    let date = require('date-and-time');
    app.get('/api/surcharge-type-list/', jwtMW, (req, res) => {
        con = require('../db');
        con.query('SELECT id, name, amount, DATE_FORMAT(effective_date,"%Y-%m-%d") AS effective_date, status FROM surcharge_type WHERE deleted_flag = ? ORDER BY id DESC',
            [0]
            , function (error, rows, fields) {
                if (error) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "result": rows,
                    }
                    res.send(obj);
                }
            })
    })


     ////////////////////
  ////CHECK EXIST/////
  ///////////////////

  app.post('/api/surcharge-type-exist/', jwtMW, (req, res) => {
    con = require('../db');
    let sql = '';
    //console.log(req.body.id)
    if (req.body.id
     === undefined) {
      sql = 'SELECT name FROM surcharge_type WHERE name=?';
      var edit = 0;
      //console.log('add condition')
    } else {
      sql = 'SELECT name FROM surcharge_type WHERE name=? AND id !=?';
      //console.log('update condition')
      var edit = 1;
    }
    con.query(sql,
      [
        req.body.name,
        req.body.id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
              "status": 0,
              "message": "Something wrong please try again."
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0,
            "edit": edit
          }
          res.send(obj);

        }
      })
  })


    app.post('/api/surcharge-type-insert/', jwtMW, (req, res) => {
      con = require('../db');
        console.log(req.body);
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
        con.query('INSERT INTO surcharge_type (name, amount, effective_date, status, created, modified, created_by, modified_by) VALUES(?,?,?,?,?,?,?,?)',
            [
                req.body.surcharge_type,
                req.body.surcharge_amount,
                req.body.date,
                req.body.status,
                current_date,
                current_date,
                current_user_id,
                current_user_id
            ]
            , function (err, result) {

                if (err) {
                    var obj = {
                        "status": 0,
                        "message": "Something wrong please try again."
                    }
                    res.send(obj);
                } else {
                    var obj = {
                        "status": 1,
                        "last_state_id": result.insertId,
                        "surcharge_type": req.body.surcharge_type,
                        "surcharge_amount": req.body.surcharge_amount,
                        "date": req.body.date,
                        "state_status": req.body.status
                    }
                    res.send(obj);

                }
            })
    })

    app.post('/api/surcharge-type-update/', jwtMW,(req,res) => {
        con = require('../db');
        let now = new Date();
        let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
        let current_user_id = req.body.current_user_id;
         con.query('UPDATE surcharge_type SET name=?, amount=?, effective_date=?, status=?, modified=?, modified_by=? WHERE id=?',
         [
          req.body.name,
          req.body.amount,
          req.body.effective_date,
          req.body.status,
          current_date,
          current_user_id,
          req.body.id
         ]
         , function (err,result){
              if(err) {
                var obj = {
                    "status": 0,
                    "message": "Something wrong please try again."
                }
                res.send(obj);
              } else {
                var obj = {
                    "status": 1
                }
                res.send(obj);
               
              }
         })
      })
}
