/*
* Title: Server
* Descrpation :- This module blong to manage all request according to react call
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const exjwt = require('express-jwt');
var cron = require('node-cron');
const port = 5000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/uploads', express.static(__dirname + '/uploads'));


/*var con = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  database:'health3',

});*/
app.use(cors());
/*con.connect(function(error){
  if(error) console.log('mysql not found')
  else console.log('Connected')
});*/
// check user token
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
  next();
});
const jwtMW = exjwt({
  secret: 'ramesh'
});

app.use(exjwt({ secret: jwtMW, credentialsRequired: false }), function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    var obj = {
      "status": 0,
      "message": "Token is expired. Please login again."
    }
    res.send(obj);
  } else {
    next();
  }
});
/*========= Here we will set up an express jsonwebtoken middleware(simply required for express to properly utilize the token for requests) You MUST instantiate this with the same secret that will be sent to the client ============*/

cron.schedule('0 0 1 * *', () => {
  var cronJob = require('./nodefiles/cronJob.js');
  cronJob.missInvoice();
})
/*cron.schedule('* 0 * * *', () => {
  //console.log('running a task every two minutes');
  var cronJob = require('./nodefiles/cronJob.js');
  cronJob.emailInvoice(con);
});*/
var cronTimer = require('./nodefiles/cronTimer.js');
//cronTimer.timeScheduler(cron)
cron.schedule('*/1 * * * *', () => {
  //console.log('running a task every two minutes');
  //var cronJob = require('./nodefiles/cronJob.js');
  //cronJob.emailInvoice(con);
  var cronJobDuePayment = require('./nodefiles/duePayemnt.js');
  //cronJobDuePayment.deuPayment();
});

cron.schedule('*/3 * * * *', () => {
  var cronJobMissPayment = require('./nodefiles/missPayemnt.js');
  //cronJobMissPayment.missPayment();
});


require('./nodefiles/users.js')(app, jwtMW);
require('./nodefiles/userTypes.js')(app, jwtMW);
require('./nodefiles/userRoles.js')(app, jwtMW);
require('./nodefiles/permission-filter.js')(app, jwtMW);
require('./nodefiles/master.js')(app, jwtMW);
require('./nodefiles/masterValue.js')(app, jwtMW);
require('./nodefiles/states.js')(app, jwtMW);
require('./nodefiles/regions.js')(app, jwtMW);
require('./nodefiles/questions.js')(app, jwtMW);
require('./nodefiles/surchargeType.js')(app, jwtMW);
require('./nodefiles/riskFactor.js')(app, jwtMW);
require('./nodefiles/lateFeeWaiver.js')(app, jwtMW);
require('./nodefiles/scoreThreshold.js')(app, jwtMW);
require('./nodefiles/systemModule.js')(app, jwtMW);
require('./nodefiles/payback.js')(app, jwtMW);
require('./nodefiles/interestRate.js')(app, jwtMW);
require('./nodefiles/financialCharges.js')(app, jwtMW);
require('./nodefiles/country.js')(app, jwtMW);
require('./nodefiles/creditapplication.js')(app, jwtMW);
require('./nodefiles/experian.js')(app);
require('./nodefiles/provider.js')(app, jwtMW);
require('./nodefiles/customer.js')(app, jwtMW);
require('./nodefiles/providerInvoice.js')(app, jwtMW);
require('./nodefiles/procedures.js')(app, jwtMW);
require('./nodefiles/paymentPlan.js')(app, jwtMW);
require('./nodefiles/adminInvoice.js')(app, jwtMW);
require('./nodefiles/adminReport.js')(app, jwtMW);
require('./nodefiles/customerDashboard.js')(app, jwtMW);
require('./nodefiles/scoreRange.js')(app, jwtMW);
require('./nodefiles/carecloud.js')(app, jwtMW);
require('./nodefiles/dashboard.js')(app, jwtMW);
require('./nodefiles/signup.js')(app, jwtMW);
require('./nodefiles/termMonth.js')(app, jwtMW);
require('./nodefiles/loanAmount.js')(app, jwtMW);
require('./nodefiles/microsoft/microsoftOneDrive.js')(app);
//require('./nodefiles/microsoft/hellosign.js')(app);
require('./nodefiles/answers.js')(app, jwtMW);
require('./nodefiles/emailTemplate.js')(app, jwtMW);
require('./nodefiles/customerSupport.js')(app, jwtMW);
require('./nodefiles/cosigner.js')(app, jwtMW);

// start node server
app.listen(port, () => console.log(`Server started on port ${port}`));

