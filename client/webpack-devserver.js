// node.js server used to serve assets bundled by Webpack
// use `npm start` command to launch the server.
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config.js');
console.log('Starting the dev web server...');
const port = 3000;
const path = require('path');

const options = {
  publicPath: config.output.publicPath,
  
  inline: true,
  progress:true,
  contentBase: './src/index.js',
  stats: { colors: true }
};
console.log(options)
const server = new WebpackDevServer(webpack(config), options);

server.listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
  }
  console.log('WebpackDevServer listening at localhost:', port);
});