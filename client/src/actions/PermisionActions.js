import { NotificationManager } from 'react-notifications';
import { permissionService,userService } from '../apifile';
import {
    PERMISSION_REQUEST,
    PERMISSION_SUCCESS,
    PERMISSION_FAILURE,
    PERMISSION_MODULE_REQUEST,
    PERMISSION_MODULE_SUCCESS,
    PERMISSION_MODULE_FAILURE,
    PERMISSION_MODULE_RESET,
    USER_ROLE,
    USER_ROLE_SUCCESS,
    USER_ROLE_FAILURE
} from 'Actions/types';



/*
* Title :- getUserRole
* Descrption :- this function use for get user role according to uesr type
* Author :- Cogniz software & ramesh Kumar 
* Date :- 8 May 2019
*/

export const getUserRoleP = (id) => (dispatch) => {
    dispatch({ type: USER_ROLE });
    userService.getUserRole(id)
        .then((role) => {
            dispatch({ type: USER_ROLE_SUCCESS, payload: role.result });
        })
        .catch((error) => {
            dispatch({ type: USER_ROLE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getPermisionFilter
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 11 March 2019
*/

export const getPermisionFilter = (history) => (dispatch) => {
    dispatch({ type: PERMISSION_REQUEST });
    permissionService.getPermisionFilter()
        .then((usertypes) => {
            dispatch({ type: PERMISSION_SUCCESS, payload: usertypes});
        })
        .catch((error) => {
            dispatch({ type: PERMISSION_FAILURE });
            NotificationManager.error(error.message);
        });    
}

/*
* Title :- getPermisionReset
* Descrption :- this function use for reset permission data
* Author :- Cogniz software & ramesh Kumar 
* Date :- 11 March 2019
*/
export const getPermisionReset = (history,user_type) => (dispatch) => {
    dispatch({ type: PERMISSION_MODULE_RESET, payload: user_type });
}


/*
* Title :- getPermisionFilter
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 26 March 2019
*/
export const getPermisionModule = (history,user_role) => (dispatch) => {
    dispatch({ type: PERMISSION_MODULE_REQUEST });
    permissionService.getPermisionModule(user_role)
        .then((module) => {
            dispatch({ type: PERMISSION_MODULE_SUCCESS, payload: module.result });
            
        })
        .catch((error) => {
            dispatch({ type: PERMISSION_MODULE_FAILURE });
            NotificationManager.error(error.message);
        });

    
}

/*
* Title :- getPermisionFilter
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 26 March 2019
*/
export const getPermisionSubmit = (history,formData) => (dispatch) => {
    dispatch({ type: PERMISSION_MODULE_REQUEST });
    permissionService.getPermisionSubmite(formData)
        .then((module) => {
            //dispatch({ type: PERMISSION_MODULE_SUCCESS });
            NotificationManager.success(module.message);
        })
        .catch((error) => {
            dispatch({ type: PERMISSION_MODULE_FAILURE });
            NotificationManager.error(error.message);
        });

    
}
