import { NotificationManager } from 'react-notifications';
import { ScoreRangeService } from '../apifile';
import {
    SCORE_RANGE_LIST,
    SCORE_RANGE_LIST_SUCCESS,
    SCORE_RANGE_LIST_FAILURE,

    SCORE_RANGE_UPDATE,
    SCORE_RANGE_UPDATE_SUCCESS,
    SCORE_RANGE_UPDATE_FAILURE,

    SCORE_RANGE_DELETE,
    SCORE_RANGE_DELETE_SUCCESS,
    SCORE_RANGE_DELETE_FAILURE,

    SCORE_RANGE_INSERT,
    SCORE_RANGE_INSERT_SUCCESS,
    SCORE_RANGE_INSERT_FAILURE,

} from 'Actions/types';



/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const ScoreRangeList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: SCORE_RANGE_LIST });
    ScoreRangeService.getAllScoreRange(formData)
        .then((ScoreRange) => {
        	dispatch({ type: SCORE_RANGE_LIST_SUCCESS, payload:ScoreRange.result});
        })
        .catch((error) => {
            dispatch({ type: SCORE_RANGE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updatePayback
* Descrption :- this function use for update insert Payback data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateScoreRange = (formData, history) => (dispatch) => {
    dispatch({type: SCORE_RANGE_UPDATE});
        ScoreRangeService.updateScoreRange(formData)
        .then((ScoreRangeUpdate) => {
            dispatch({ type: SCORE_RANGE_UPDATE_SUCCESS, payload:ScoreRangeUpdate.result});
        })
        .catch((error) => {
            dispatch({ type: SCORE_RANGE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}



/*
* Title :- insertPayback
* Descrption :- this function use for insert Payback data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertScoreRange = (formData, history) => (dispatch) => {
    dispatch({type: SCORE_RANGE_INSERT});
        ScoreRangeService.insertScoreRange(formData)
        .then((ScoreRangeInsert) => {
        	NotificationManager.success('Record added successfully!');
            dispatch({ type: SCORE_RANGE_INSERT_SUCCESS, payload:ScoreRangeInsert});
        })
        .catch((error) => {
            dispatch({ type: SCORE_RANGE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

