import { NotificationManager } from 'react-notifications';
import { paymentPlanService, userRolesService } from '../apifile';
import {
    
    PAYMENT_MASTER_FEE_OPTION,
    PAYMENT_MASTER_FEE_OPTION_SUCCESS,
    PAYMENT_MASTER_FEE_OPTION_FAILURE,
    
    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION,
    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_SUCCESS,
    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_FAILURE,

    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION,
    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_SUCCESS,
    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_FAILURE,
    
    PAYMENT_SINGLE_INSTALLMENT,
    PAYMENT_SINGLE_INSTALLMENT_SUCCESS,
    PAYMENT_SINGLE_INSTALLMENT_FAILURE,

    PAY_INSTALLMENT_INSERT,
    PAY_INSTALLMENT_INSERT_SUCCESS,
    PAY_INSTALLMENT_INSERT_FAILURE,

    CLEAR_REDIRECT_URL,
    CLEAR_REDIRECT_URL_SUCCESS,
    CLEAR_REDIRECT_URL_FAILURE,

    SEND_EMAIL_REPORT,
    SEND_EMAIL_REPORT_SUCCESS,
    SEND_EMAIL_REPORT_FAILURE,

    ALL_PLAN_DETAILS,
    ALL_PLAN_DETAILS_SUCCESS,
    ALL_PLAN_DETAILS_FAILURE,

    INVOICE_PLAN_REQUEST,
    INVOICE_PLAN_SUCCESS,
    INVOICE_PLAN_FAILURE,

    VIEW_RECIPIT_REQUEST,
    VIEW_RECIPIT_SUCCESS,
    VIEW_RECIPIT_FAILURE,

    CREATE_INVOICE_REQUEST,
    CREATE_INVOICE_SUCCESS,
    CREATE_INVOICE_FAILURE,

    APP_PLAN_DETAILS_PDF_DOWNLOAD_REQUEST,
    APP_PLAN_DETAILS_PDF_DOWNLOAD_SUCCESS,
    APP_PLAN_DETAILS_PDF_DOWNLOAD_FAILURE,

    REGENERATE_PROCESS_REQUEST,
    REGENERATE_PROCESS_SUCCESS,
    REGENERATE_PROCESS_FAILURE,
    REGENERATE_PLAN_PROCESS,

    OPTION_TO_CLOSE_REQUEST,
    OPTION_TO_CLOSE_SUCCESS,
    OPTION_TO_CLOSE_FAILURE,

    OPTION_TO_CLOSE_ACTION_REQUEST,
    OPTION_TO_CLOSE_ACTION_SUCCESS,
    OPTION_TO_CLOSE_ACTION_FAILURE,

    REGENERATE_PRINT_PROCESS_REQUEST,
    REGENERATE_PRINT_PROCESS_SUCCESS,
    REGENERATE_PRINT_PROCESS_FAILURE,
    REGENERATE_PRINT_PLAN_PROCESS,

    CURRENT_PLAN_DETAILS_REQUEST,
    CURRENT_PLAN_DETAILS_SUCCESS,
    CURRENT_PLAN_DETAILS_FAILURE,

    CONFIRMED_CANCELLATION_PLAN_REQUEST,
    CONFIRMED_CANCELLATION_PLAN_SUCCESS,
    CONFIRMED_CANCELLATION_PLAN_FAILURE,

} from 'Actions/types';

/*
* Title :- confirmedCancellation
* Descrption :- this function use to confirmed cancellation
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jun 6, 2020
*/
export const confirmedCancellation = (pp_id,app_id,provider_confirm,refund_type) => (dispatch) => {
    dispatch({ type: CONFIRMED_CANCELLATION_PLAN_REQUEST });
    paymentPlanService.confirmedCancellation(pp_id,provider_confirm,refund_type)
        .then((result) => {
            dispatch(allPlanDetails(app_id))
            dispatch({ type: CONFIRMED_CANCELLATION_PLAN_SUCCESS, payload:result});
            NotificationManager.success(result.message)
        })
        .catch((error) => {
            dispatch({ type: CONFIRMED_CANCELLATION_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- getClosePlanDetails
* Descrption :- this function use to get plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- May 5, 2020
*/
export const getClosePlanDetails = (pp_id) => (dispatch) => {
    dispatch({ type: CURRENT_PLAN_DETAILS_REQUEST });
    paymentPlanService.getClosePlanDetails(pp_id)
        .then((result) => {
            dispatch({ type: CURRENT_PLAN_DETAILS_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: CURRENT_PLAN_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- regeneratePrintPlan
* Descrption :- this function use for regeneratePrintPlan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/
export const regeneratePrintPlan = (data) => (dispatch) => {
    dispatch({ type: ALL_PLAN_DETAILS, });
    paymentPlanService.regeneratePrintPlan(data)
        .then((plan) => {
            if(plan.status == 1){
                dispatch(allPlanDetails(data.app_id))
            }
            dispatch({ type: REGENERATE_PRINT_PLAN_PROCESS, payload:plan});
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: REGENERATE_PRINT_PLAN_PROCESS });
            NotificationManager.error(error);
        });
}

/*
* Title :- closeCurrentPlan
* Descrption :- this function use for close plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 20 April 2020
*/
export const closeCurrentPlan = (data) => (dispatch) => {
    dispatch({ type: OPTION_TO_CLOSE_ACTION_REQUEST });
    paymentPlanService.closeCurrentPlan(data)
        .then((result) => {
            NotificationManager.success(result.message);
            dispatch({ type: OPTION_TO_CLOSE_ACTION_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: OPTION_TO_CLOSE_ACTION_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- viewOptionToClose
* Descrption :- this function use to get option for close plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 17, 2020
*/
export const viewOptionToClose = (pp_id) => (dispatch) => {
    dispatch({ type: OPTION_TO_CLOSE_REQUEST });
    paymentPlanService.viewOptionToClose(pp_id)
        .then((result) => {
            dispatch({ type: OPTION_TO_CLOSE_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: OPTION_TO_CLOSE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- regeneratePlanProcess
* Descrption :- this function use for regenerate plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/
export const regeneratePlanProcess = (data) => (dispatch) => {
    dispatch({ type: REGENERATE_PROCESS_REQUEST, });
    paymentPlanService.regeneratePlanProcess(data)
        .then((plan) => {
            dispatch({ type: REGENERATE_PROCESS_SUCCESS, payload:plan});
        })
        .catch((error) => {
            dispatch({ type: REGENERATE_PROCESS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- regeneratePlan
* Descrption :- this function use for regenerate plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/
export const regeneratePlan = (data) => (dispatch) => {
    dispatch({ type: ALL_PLAN_DETAILS, });
    paymentPlanService.regeneratePlan(data)
        .then((plan) => {
            if(plan.status == 1){
                dispatch(allPlanDetails(data.app_id))
            }
            dispatch({ type: REGENERATE_PLAN_PROCESS, payload:plan});
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: REGENERATE_PLAN_PROCESS });
            NotificationManager.error(error);
        });
}

export const adminPlanDetailsDownload = (appid, planid) => (dispatch) => {
    dispatch({ type: APP_PLAN_DETAILS_PDF_DOWNLOAD_REQUEST, });
    paymentPlanService.adminPlanDetailsDownload(appid, planid)
        .then((plan_details) => {
            //dispatch({ type: APP_PLAN_DETAILS__PDF_DOWNLOAD_SUCCESS, payload:plan_details});
        })
        .catch((error) => {
            dispatch({ type: APP_PLAN_DETAILS_PDF_DOWNLOAD_FAILURE });
            NotificationManager.error(error);
        });
}

export const createInvoice = (date,appid) => (dispatch) => {
    dispatch({ type: CREATE_INVOICE_REQUEST });
    paymentPlanService.createInvoice(date,appid)
        .then((result) => {
            dispatch(allPlanDetails(appid))
            dispatch({ type: CREATE_INVOICE_SUCCESS, payload:result});
            NotificationManager.success(result.message);
        })
        .catch((error) => {
            dispatch({ type: CREATE_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

export const viewReceipt = (app_id) => (dispatch) => {
    dispatch({ type: VIEW_RECIPIT_REQUEST });
    paymentPlanService.viewReceipt(app_id)
        .then((result) => {
            dispatch({ type: VIEW_RECIPIT_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: VIEW_RECIPIT_FAILURE });
            NotificationManager.error(error);
        });
}

export const invoiceDetails = (id) => (dispatch) => {
    dispatch({ type: INVOICE_PLAN_REQUEST, });
    paymentPlanService.invoiceDetails(id)
        .then((invoice) => {
            dispatch({ type: INVOICE_PLAN_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: INVOICE_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}

export const allPlanDetails = (appid) => (dispatch) => {
    dispatch({ type: ALL_PLAN_DETAILS, });
    paymentPlanService.allPlanDetails(appid)
        .then((plan) => {
            dispatch({ type: ALL_PLAN_DETAILS_SUCCESS, payload:plan});
        })
        .catch((error) => {
            dispatch({ type: ALL_PLAN_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

export const sendReport = (invoice_msg, id) => (dispatch) => {
    dispatch({ type: SEND_EMAIL_REPORT });
    paymentPlanService.sendReport(invoice_msg, id)
        .then((send_report) => {
            dispatch({ type: SEND_EMAIL_REPORT_SUCCESS, payload:send_report});
            NotificationManager.success(send_report.message);
        })
        .catch((error) => {
            dispatch({ type: SEND_EMAIL_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const clearRedirectURL = (formData, history) => (dispatch) => {
    dispatch({type: CLEAR_REDIRECT_URL});
    
}

export const insertPayPayment = (formData, history) => (dispatch) => {
    dispatch({type: PAY_INSTALLMENT_INSERT});
        paymentPlanService.insertPayPayment(formData)
        .then((PayInstallment) => {
            NotificationManager.success('Payment successfully!');
            dispatch({ type: PAY_INSTALLMENT_INSERT_SUCCESS, payload:PayInstallment});
        })
        .catch((error) => {
            dispatch({ type: PAY_INSTALLMENT_INSERT_FAILURE });
            NotificationManager.error(error);
        });
}

export const getSingleInstallment = (invoiceId) => (dispatch) => {
    dispatch({ type: PAYMENT_SINGLE_INSTALLMENT });
    paymentPlanService.getSingleInstallment(invoiceId)
        .then((option) => {
            //option.payment_invoice_detail && option.payment_invoice_detail.map(function(opt, key) {
                if(option.single_installment_payment_edit.length !== 0) {
                    dispatch(getFinChargeWaiverType(option.single_installment_payment_edit[0].mdv_fin_id));
                }
                if(option.single_installment_payment_edit.length !== 0) {
                    dispatch(getLateFeeWaiverType(option.single_installment_payment_edit[0].mdv_late_id));
                }
            //})
            dispatch({ type: PAYMENT_SINGLE_INSTALLMENT_SUCCESS, payload:option});
        })
        .catch((error) => {
            dispatch({ type: PAYMENT_SINGLE_INSTALLMENT_FAILURE });
            NotificationManager.error(error.message);
        });
}


export const getLateFeeWaiverType = (id) => (dispatch) => {
    dispatch({ type: PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION });
    paymentPlanService.getLateFeeWaiverType(id)
        .then((option) => {
            dispatch({ type: PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_SUCCESS, payload:option});
        })
        .catch((error) => {
            dispatch({ type: PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const getFinChargeWaiverType = (id) => (dispatch) => {
    dispatch({ type: PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION });
    paymentPlanService.getFinChargeWaiverType(id)
        .then((option) => {
            dispatch({ type: PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_SUCCESS, payload:option});
        })
        .catch((error) => {
            dispatch({ type: PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const getPaymentMasterFeeOption = (formData, history) => (dispatch) => {
    dispatch({ type: PAYMENT_MASTER_FEE_OPTION });
    paymentPlanService.getPaymentMasterFeeOption(formData)
        .then((option) => {
            dispatch({ type: PAYMENT_MASTER_FEE_OPTION_SUCCESS, payload:option});
        })
        .catch((error) => {
            dispatch({ type: PAYMENT_MASTER_FEE_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}





