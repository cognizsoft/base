import { NotificationManager } from 'react-notifications';
import { scoreThresholdService } from '../apifile';
import {
    SCORE_THRESHOLD_LIST,
    SCORE_THRESHOLD_LIST_SUCCESS,
    SCORE_THRESHOLD_LIST_FAILURE,

    SCORE_THRESHOLD_UPDATE,
    SCORE_THRESHOLD_UPDATE_SUCCESS,
    SCORE_THRESHOLD_UPDATE_FAILURE,

    SCORE_THRESHOLD_DELETE,
    SCORE_THRESHOLD_DELETE_SUCCESS,
    SCORE_THRESHOLD_DELETE_FAILURE,

    SCORE_THRESHOLD_INSERT,
    SCORE_THRESHOLD_INSERT_SUCCESS,
    SCORE_THRESHOLD_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE

} from 'Actions/types';


/*
* Title :- checkScorThresholdExist
* Descrption :- this function use for check System Module exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkScoreThresholdExist = (name, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    scoreThresholdService.checkScoreThresholdExist(name, id)
        .then((scoreThreshold) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: scoreThreshold});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const scoreThresholdMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    scoreThresholdService.getAllScoreThresholdMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const scoreThresholdList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: SCORE_THRESHOLD_LIST });
    scoreThresholdService.getAllScoreThreshold(formData)
        .then((scoreThreshold) => {
        	dispatch({ type: SCORE_THRESHOLD_LIST_SUCCESS, payload:scoreThreshold.result});
        })
        .catch((error) => {
            dispatch({ type: SCORE_THRESHOLD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateMasterValue
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateScoreThreshold = (formData, history) => (dispatch) => {
    dispatch({type: SCORE_THRESHOLD_UPDATE});
        scoreThresholdService.updateScoreThreshold(formData)
        .then((scoreThresholdUpdate) => {
            dispatch({ type: SCORE_THRESHOLD_UPDATE_SUCCESS, payload:scoreThresholdUpdate.result});
            NotificationManager.success('Record updated successfully!');
        })
        .catch((error) => {
            dispatch({ type: SCORE_THRESHOLD_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertScoreThreshold
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertScoreThreshold = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: SCORE_THRESHOLD_INSERT});
        scoreThresholdService.insertScoreThreshold(formData)
        .then((ScoreThresholdInsert) => {
            dispatch({ type: SCORE_THRESHOLD_INSERT_SUCCESS, payload:ScoreThresholdInsert});
            NotificationManager.success('Record added successfully!');
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: SCORE_THRESHOLD_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

