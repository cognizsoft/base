import { NotificationManager } from 'react-notifications';
import { emailTempService } from '../apifile';
import {
    EMAIL_TEMPLATE_LIST,
    EMAIL_TEMPLATE_LIST_SUCCESS,
    EMAIL_TEMPLATE_LIST_FAILURE,
    EMAIL_TEMPLATE_INSERT,
    EMAIL_TEMPLATE_INSERT_SUCCESS,
    EMAIL_TEMPLATE_INSERT_FAILURE,
    EMAIL_TEMPLATE_UPDATE,
    EMAIL_TEMPLATE_UPDATE_SUCCESS,
    EMAIL_TEMPLATE_UPDATE_FAILURE,
    EMAIL_TEMPLATE_DELETE,
    EMAIL_TEMPLATE_DELETE_SUCCESS,
    EMAIL_TEMPLATE_DELETE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,

    EMAIL_TEMPLATE_VARIABLE,
    EMAIL_TEMPLATE_VARIABLE_SUCCESS,
    EMAIL_TEMPLATE_VARIABLE_FAILURE,
} from 'Actions/types';

/*
* Title :- getEmailTempVariable
* Descrption :- this function use for get email template list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getEmailTempVariable = (temp_id) => (dispatch) => {
    dispatch({ type: EMAIL_TEMPLATE_VARIABLE });
    emailTempService.getEmailTempVariable(temp_id)
        .then((emailTempVar) => {
            dispatch({ type: EMAIL_TEMPLATE_VARIABLE_SUCCESS, payload: emailTempVar.result });
        })
        .catch((error) => {
            dispatch({ type: EMAIL_TEMPLATE_VARIABLE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkEmailTempExist
* Descrption :- this function use for check email template exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkEmailTempExist = (value, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    emailTempService.checkEmailTempExist(value, id)
        .then((EmailTemp) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: EmailTemp});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllEmailTemp
* Descrption :- this function use for get email template list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllEmailTemp = () => (dispatch) => {
    dispatch({ type: EMAIL_TEMPLATE_LIST });
    emailTempService.getAllEmailTemp()
        .then((emailTemp) => {
            dispatch({ type: EMAIL_TEMPLATE_LIST_SUCCESS, payload: emailTemp });
        })
        .catch((error) => {
            dispatch({ type: EMAIL_TEMPLATE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertEmailTemp
* Descrption :- this function use for insert email template information
* Author :- Hp & Aman 
* Date :- 24 April 2019
*/

export const insertEmailTemp = (formData) => (dispatch) => {
    dispatch({ type: EMAIL_TEMPLATE_INSERT });
    emailTempService.insertEmailTemp(formData)
        .then((emailTemp) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: EMAIL_TEMPLATE_INSERT_SUCCESS, payload: emailTemp });
        })
        .catch((error) => {
            dispatch({ type: EMAIL_TEMPLATE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateEmailTemp
* Descrption :- this function use for update email template record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateEmailTemp = (formData) => (dispatch) => {
    dispatch({ type: EMAIL_TEMPLATE_UPDATE });
    emailTempService.updateEmailTemp(formData)
        .then((data) => {
            dispatch({ type: EMAIL_TEMPLATE_UPDATE_SUCCESS, payload: data.result });
            NotificationManager.success('Email Template Updated!');
        })
        .catch((error) => {
            dispatch({ type: EMAIL_TEMPLATE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteEmailTemp
* Descrption :- this function use for update email template record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const deleteEmailTemp = (formData) => (dispatch) => {
    dispatch({ type: EMAIL_TEMPLATE_DELETE });
    emailTempService.deleteEmailTemp(formData)
        .then((emailTemp) => {
            NotificationManager.success('Template Deleted!');
            dispatch({ type: EMAIL_TEMPLATE_DELETE_SUCCESS, payload: emailTemp.id });
        })
        .catch((error) => {
            dispatch({ type: EMAIL_TEMPLATE_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}
