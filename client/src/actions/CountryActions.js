import { NotificationManager } from 'react-notifications';
import { countryService } from '../apifile';
import {
    COUNTRY_LIST,
    COUNTRY_LIST_SUCCESS,
    COUNTRY_LIST_FAILURE,
    COUNTRY_INSERT,
    COUNTRY_INSERT_SUCCESS,
    COUNTRY_INSERT_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,
    COUNTRY_UPDATE,
    COUNTRY_UPDATE_SUCCESS,
    COUNTRY_UPDATE_FAILURE,
} from 'Actions/types';



/*
* Title :- getAllUsers
* Descrption :- this function use for get state list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 22 March 2019
*/
export const getAllCountry = () => (dispatch) => {
    dispatch({ type: COUNTRY_LIST });
    countryService.getAllCountry()
        .then((country) => {
            dispatch({ type: COUNTRY_LIST_SUCCESS, payload: country.result });
        })
        .catch((error) => {
            dispatch({ type: COUNTRY_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- insertState
* Descrption :- this function use for insert state information
* Author :- Hp & Aman 
* Date :- 22 April 2019
*/

export const insertCountry = (formData) => (dispatch) => {
    dispatch({ type: COUNTRY_INSERT });
    countryService.insertCountry(formData)
        .then((country) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: COUNTRY_INSERT_SUCCESS, payload: country });
        })
        .catch((error) => {
            dispatch({ type: COUNTRY_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkNameExist
* Descrption :- this function use for check state name exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkSANameExist = (value, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    countryService.checkSNameExist(value, id)
        .then((country) => {
            //user.abbreviation=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: country});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkAbbreviationExist
* Descrption :- this function use for check state Abbreviation exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkCAbbreviationExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    countryService.checkAbbreviationExist(value, md_id)
        .then((country) => {
            user.abbreviation=1;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: country });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateState
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const updateCountry = (formData) => (dispatch) => {
    dispatch({ type: COUNTRY_UPDATE });
    countryService.updateCountry(formData)
        .then((country) => {
            dispatch({ type: COUNTRY_UPDATE_SUCCESS, payload: country.result });
            NotificationManager.success('Country Updated!');
        })
        .catch((error) => {
            dispatch({ type: COUNTRY_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}