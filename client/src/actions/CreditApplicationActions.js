import { NotificationManager } from 'react-notifications';
import { creditApplicationService, userRolesService } from '../apifile';
import {
    APPLICATION_OPTION_LIST,
    APPLICATION_OPTION_SUCCESS,
    APPLICATION_OPTION_FAILURE,
    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,
    ADD_MORE_LIST,
    REMOVE_ADD_MORE_LIST,
    REGION_LIST,
    REGION_LIST_SUCCESS,
    REGION_LIST_FAILURE,
    APPLICATION_SUBMIT,
    APPLICATION_SUBMIT_SUCCESS,
    APPLICATION_SUBMIT_FAILURE,
    CREDIT_APPLICATION_LIST,
    CREDIT_APPLICATION_LIST_SUCCESS,
    CREDIT_APPLICATION_LIST_FAILURE,
    PAYMENT_PLAN_REQUEST,
    PAYMENT_PLAN_SUCCESS,
    PAYMENT_PLAN_FAILURE,
    CREATE_PLAN_REQUEST,
    CREATE_PLAN_SUCCESS,
    CREATE_PLAN_FAILURE,
    SINGLE_PLAN_REQUEST,
    SINGLE_PLAN_SUCCESS,
    SINGLE_PLAN_FAILURE,
    VIEW_APPLICATION_REQUEST,
    VIEW_APPLICATION_SUCCESS,
    VIEW_APPLICATION_FAILURE,
    CUSTOMER_SEARCH_REQUEST,
    CUSTOMER_SEARCH_SUCCESS,
    CUSTOMER_SEARCH_FAILURE,
    REMOVE_CUSTOMER_SEARCH,
    REVIEW_APPLICATION_REQUEST,
    REVIEW_APPLICATION_SUCCESS,
    REVIEW_APPLICATION_FAILURE,
    MANUAL_ACTION_REQUEST,
    MANUAL_ACTION_SUCCESS,
    MANUAL_ACTION_FAILURE,
    REJECT_ACTION_REQUEST,
    REJECT_ACTION_SUCCESS,
    REJECT_ACTION_FAILURE,
    UPLOAD_DOCUMENT_REQUEST,
    UPLOAD_DOCUMENT_SUCCESS,
    UPLOAD_DOCUMENT_FAILURE,
    REVIEW_DOCUMENT_REQUEST,
    REVIEW_DOCUMENT_SUCCESS,
    REVIEW_DOCUMENT_FAILURE,
    REVIEW_MANUAL_ACTION,
    SPECIALITY_LIST,
    SPECIALITY_LIST_SUCCESS,
    SPECIALITY_LIST_FAILURE,
    REPORT_DOWNLOAD_LIST_REQUEST,
    REPORT_DOWNLOAD_LIST_SUCCESS,
    REPORT_DOWNLOAD_LIST_FAILURE,
    CUSTOMER_SEARCH_APPlICTION_REQUEST,
    CUSTOMER_SEARCH_APPlICTION_SUCCESS,
    CUSTOMER_SEARCH_APPlICTION_FAILURE,
    APPlICTION_DETAILS_REQUEST,
    APPlICTION_DETAILS_SUCCESS,
    APPlICTION_DETAILS_FAILURE,
    APPlICTION_PLAN_RESET,
    APP_EXPIRE_DATE_UPDATE,
    APP_EXPIRE_DATE_UPDATE_SUCCESS,
    APP_EXPIRE_DATE_UPDATE_FAILURE,
    ESTIMATE_DETAILS_REQUEST,
    ESTIMATE_DETAILS_SUCCESS,
    ESTIMATE_DETAILS_FAILURE,
    CUSTOMER_INFORMATION_REQUEST,
    CUSTOMER_INFORMATION_SUCCESS,
    CUSTOMER_INFORMATION_FAILURE,
    APPLICTION_EMAIL_REQUEST,
    APPLICTION_EMAIL_SUCCESS,
    APPLICTION_EMAIL_FAILURE,

    ALL_APPLICATION_DOC_DETAILS,
    ALL_APPLICATION_DOC_DETAILS_SUCCESS,
    ALL_APPLICATION_DOC_DETAILS_FAILURE,

    UPLOAD_REJECTION_DOCUMENT_REQUEST,
    UPLOAD_REJECTION_DOCUMENT_SUCCESS,
    UPLOAD_REJECTION_DOCUMENT_FAILURE,

    DOCTORS_LIST,
    DOCTORS_LIST_SUCCESS,
    DOCTORS_LIST_FAILURE,

    APP_OVERRIDE_AMOUNT_UPDATE,
    APP_OVERRIDE_AMOUNT_UPDATE_SUCCESS,
    APP_OVERRIDE_AMOUNT_UPDATE_FAILURE,

    UNLOCK_APPLICATION,
    UNLOCK_APPLICATION_SUCCESS,
    UNLOCK_APPLICATION_FAILURE,

    DOWNLOAD_FILE_REQUEST,
    DOWNLOAD_FILE_SUCCESS,
    DOWNLOAD_FILE_FAILURE,

    APP_DOC_RESEND_REQUEST,
    APP_DOC_RESEND_SUCCESS,
    APP_DOC_RESEND_FAILURE,

    CREATE_PRINT_PLAN_REQUEST,
    CREATE_PRINT_PLAN_SUCCESS,
    CREATE_PRINT_PLAN_FAILURE,

    UPLOAD_AGREEMENT_VIEW_DETAILS_REQUEST,
    UPLOAD_AGREEMENT_VIEW_DETAILS_SUCCESS,
    UPLOAD_AGREEMENT_VIEW_DETAILS_FAILURE,

    UPLOAD_PLAN_AGREEMENT_REQUEST,
    UPLOAD_PLAN_AGREEMENT_SUCCESS,
    UPLOAD_PLAN_AGREEMENT_FAILURE,

    PLAN_AGREEMENT_REVIEW_DETAILS_REQUEST,
    PLAN_AGREEMENT_REVIEW_DETAILS_SUCCESS,
    PLAN_AGREEMENT_REVIEW_DETAILS_FAILURE,

    PLAN_REJECT_APPROVE_ACTION_REQUEST,
    PLAN_REJECT_APPROVE_ACTION_SUCCESS,
    PLAN_REJECT_APPROVE_ACTION_FAILURE,

    PROVIDER_PLAN_LIST_REQUEST,
    PROVIDER_PLAN_LIST_SUCCESS,
    PROVIDER_PLAN_LIST_FAILURE,

    PROVIDER_PLAN_SUBMIT_REQUEST,
    PROVIDER_PLAN_SUBMIT_SUCCESS,
    PROVIDER_PLAN_SUBMIT_FAILURE,

    DOWNLOAD_PLAN_AGREEMENT_REQUEST,
    DOWNLOAD_PLAN_AGREEMENT_SUCCESS,
    DOWNLOAD_PLAN_AGREEMENT_FAILURE,

    DOWNLOAD_DOC_REQUEST,
    DOWNLOAD_DOC_SUCCESS,
    DOWNLOAD_DOC_FAILURE,

    CREDIT_APPLICATION_LIST_DOWN_REQUEST,
    CREDIT_APPLICATION_LIST_DOWN_SUCCESS,
    CREDIT_APPLICATION_LIST_DOWN_FAILURE,

    CANCEL_REFUND_PLAN_DETAILS_REQUEST,
    CANCEL_REFUND_PLAN_DETAILS_SUCCESS,
    CANCEL_REFUND_PLAN_DETAILS_FAILURE,

    CANCEL_REFUND_PLAN_DETAILS_DOWN_REQUEST,
    CANCEL_REFUND_PLAN_DETAILS_DOWN_SUCCESS,
    CANCEL_REFUND_PLAN_DETAILS_DOWN_FAILURE,

    CUSTOMER_PROFILE_UPDATE_REQUEST,
    CUSTOMER_PROFILE_UPDATE_SUCCESS,
    CUSTOMER_PROFILE_UPDATE_FAILURE,

    DISK_CLEANUP_REQUEST,
    DISK_CLEANUP_SUCCESS,
    DISK_CLEANUP_FAILURE,
    

} from 'Actions/types';

export const creditApplicationPlanReview = () => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST, });
    dispatch({type: CUSTOMER_PROFILE_UPDATE_REQUEST});
    creditApplicationService.creditApplicationPlanReview()
        .then((list) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_SUCCESS, payload: list });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const verifyPlanAction = (id) => (dispatch) => {
    dispatch({ type: APP_DOC_RESEND_REQUEST, });
    creditApplicationService.verifyPlanAction(id)
        .then((all_doc) => {
            dispatch(planDoc(id))
            NotificationManager.success(all_doc.message);
            dispatch({ type: APP_DOC_RESEND_SUCCESS, payload: all_doc });
        })
        .catch((error) => {
            dispatch({ type: APP_DOC_RESEND_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- Resend move doc on one drive
* Descrption :- this function use for resend application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- Desc 10, 2020
*/

export const resendMoveDocDrive = (id, docs) => (dispatch) => {
    dispatch({ type: APP_DOC_RESEND_REQUEST, });
    creditApplicationService.resendMoveDocDrive(id, docs)
        .then((all_doc) => {
            dispatch({ type: APP_DOC_RESEND_SUCCESS, payload: all_doc });
            NotificationManager.success("Document successfully moved.");
        })
        .catch((error) => {
            dispatch({ type: APP_DOC_RESEND_FAILURE });
            NotificationManager.error(error);
        });
}
export const viewDisk = (id) => (dispatch) => {
    dispatch({ type: ALL_APPLICATION_DOC_DETAILS, });
    creditApplicationService.viewDisk(id)
        .then((all_doc) => {
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_SUCCESS, payload: all_doc });
        })
        .catch((error) => {
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- PLan Documents
* Descrption :- this function use for get plan documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- Dec 09, 2019
*/

export const planDoc = (id) => (dispatch) => {
    dispatch({ type: ALL_APPLICATION_DOC_DETAILS, });
    creditApplicationService.planDoc(id)
        .then((all_doc) => {
            
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_SUCCESS, payload: all_doc });
        })
        .catch((error) => {
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

export const applicationDiskcleanup = (id, docs) => (dispatch) => {
    dispatch({ type: DISK_CLEANUP_REQUEST });
    creditApplicationService.applicationDiskcleanup(id, docs)
        .then((clean) => {
            dispatch(viewDisk(id));
            dispatch({ type: DISK_CLEANUP_SUCCESS });
            NotificationManager.success(clean.message);
        })
        .catch((error) => {
            dispatch({ type: DISK_CLEANUP_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- editApplicationProvider
* Descrption :- this function use for edit application data
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 07 2020
*/

export const editApplicationProvider = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_UPDATE_REQUEST });
    creditApplicationService.editApplicationProvider(data)
        .then((submit) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_SUCCESS, payload: submit });
            if (submit.applicationStatus == 0) {
                NotificationManager.error(submit.message);
            } else {
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_FAILURE });
            NotificationManager.error(error);
        });
}


export const cancelRefundDetailsDownload = (formData) => (dispatch) => {
    dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_DOWN_REQUEST });
    creditApplicationService.cancelRefundDetailsDownload(formData)
        .then((downloadList) => {
            dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_DOWN_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const cancelRefundDetails = (ppid) => (dispatch) => {
    dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_REQUEST, });
    creditApplicationService.cancelRefundDetails(ppid)
        .then((plan) => {
            dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: CANCEL_REFUND_PLAN_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

export const creditApplicationListDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_REQUEST });
    creditApplicationService.creditApplicationListDownloadXLS(formData)
        .then((downloadList) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const creditApplicationListDownload = (formData) => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_REQUEST });
    creditApplicationService.creditApplicationListDownload(formData)
        .then((downloadList) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const downloadDoc = (item_id) => (dispatch) => {
    dispatch({ type: DOWNLOAD_DOC_REQUEST, });
    creditApplicationService.downloadDoc(item_id)
        .then((doc) => {
            dispatch({ type: DOWNLOAD_DOC_SUCCESS, payload: doc });
            //NotificationManager.success("Email sent successfully!");
        })
        .catch((error) => {
            dispatch({ type: DOWNLOAD_DOC_FAILURE });
            NotificationManager.error(error);
        });
}

export const downloadPlanAgreement = (pp_id) => (dispatch) => {
    dispatch({ type: DOWNLOAD_PLAN_AGREEMENT_REQUEST, });
    creditApplicationService.downloadPlanAgreement(pp_id)
        .then((doc) => {
            dispatch({ type: DOWNLOAD_PLAN_AGREEMENT_SUCCESS, payload: doc });
            //NotificationManager.success("Email sent successfully!");
        })
        .catch((error) => {
            dispatch({ type: DOWNLOAD_PLAN_AGREEMENT_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerPlanSubmit
* Descrption :- this function use for submit invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 01, 2019
*/

export const providerPlanSubmit = (list) => (dispatch) => {
    dispatch({ type: PROVIDER_PLAN_SUBMIT_REQUEST, });
    creditApplicationService.providerPlanSubmit(list)
        .then((plan) => {
            dispatch(providerPlanList());
            dispatch({ type: PROVIDER_PLAN_SUBMIT_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_PLAN_SUBMIT_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- providerPlanList
* Descrption :- this function use for get credit application plans
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const providerPlanList = () => (dispatch) => {
    dispatch({ type: PROVIDER_PLAN_LIST_REQUEST, });
    creditApplicationService.providerPlanList()
        .then((planList) => {
            dispatch({ type: PROVIDER_PLAN_LIST_SUCCESS, payload: planList });
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_PLAN_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- planRejectOrApprove
* Descrption :- this function use for manual action
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/

export const planRejectOrApprove = (id, type, comment) => (dispatch) => {
    dispatch({ type: PLAN_REJECT_APPROVE_ACTION_REQUEST, });
    creditApplicationService.planRejectOrApprove(id, type, comment)
        .then((appStatus) => {
            dispatch({ type: PLAN_REJECT_APPROVE_ACTION_SUCCESS, payload: appStatus });
            NotificationManager.success(appStatus.message);
        })
        .catch((error) => {
            dispatch({ type: PLAN_REJECT_APPROVE_ACTION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- planReviewAgreementDetails
* Descrption :- this function use for get plan agreement details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

export const planReviewAgreementDetails = (id) => (dispatch) => {
    dispatch({ type: PLAN_AGREEMENT_REVIEW_DETAILS_REQUEST, });
    creditApplicationService.planReviewAgreementDetails(id)
        .then((doc) => {
            dispatch({ type: PLAN_AGREEMENT_REVIEW_DETAILS_SUCCESS, payload: doc });
        })
        .catch((error) => {
            dispatch({ type: PLAN_AGREEMENT_REVIEW_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- planUploadAgreement
* Descrption :- this function use for upload agreement document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

export const planUploadAgreement = (datafrom) => (dispatch) => {
    dispatch({ type: UPLOAD_PLAN_AGREEMENT_REQUEST, });
    creditApplicationService.planUploadAgreement(datafrom)
        .then((doc) => {
            dispatch({ type: UPLOAD_PLAN_AGREEMENT_SUCCESS, payload: doc });
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: UPLOAD_PLAN_AGREEMENT_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- uploadAgreementViewDetails
* Descrption :- this function use for get details for plan agreement
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 18, 2019
*/

export const uploadAgreementViewDetails = (planid) => (dispatch) => {
    dispatch({ type: UPLOAD_AGREEMENT_VIEW_DETAILS_REQUEST, });
    creditApplicationService.uploadAgreementViewDetails(planid)
        .then((plandetails) => {
            dispatch({ type: UPLOAD_AGREEMENT_VIEW_DETAILS_SUCCESS, payload: plandetails });
        })
        .catch((error) => {
            dispatch({ type: UPLOAD_AGREEMENT_VIEW_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- Resend Documents
* Descrption :- this function use for resend application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const resendApplicationDoc = (id, docs) => (dispatch) => {
    dispatch({ type: APP_DOC_RESEND_REQUEST, });
    creditApplicationService.resendApplicationDoc(id, docs)
        .then((all_doc) => {
            dispatch({ type: APP_DOC_RESEND_SUCCESS, payload: all_doc });
            NotificationManager.success("Email sent successfully!");
        })
        .catch((error) => {
            dispatch({ type: APP_DOC_RESEND_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- experianCIR
* Descrption :- this function use for experian CIR and manual data
* Author :- Cogniz software & ramesh Kumar 
* Date :- Feb 26, 2020
*/

export const experianCIR = (id) => (dispatch) => {
    dispatch({ type: REVIEW_APPLICATION_REQUEST, });
    creditApplicationService.experianCIR(id)
        .then((appdetails) => {
            NotificationManager.success(appdetails.message);
            dispatch({ type: REVIEW_APPLICATION_SUCCESS, payload: appdetails });
        })
        .catch((error) => {
            dispatch({ type: REVIEW_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- All Documents
* Descrption :- this function use for get application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const downloadOneDrive = (id) => (dispatch) => {
    dispatch({ type: DOWNLOAD_FILE_REQUEST, });
    creditApplicationService.downloadOneDrive(id)
        .then((result) => {
            dispatch({ type: DOWNLOAD_FILE_SUCCESS, payload: result });
        })
        .catch((error) => {
            dispatch({ type: DOWNLOAD_FILE_FAILURE });
            NotificationManager.error(error);
        });
}


export const unlockApplication = (app_id) => (dispatch) => {
    dispatch({ type: UNLOCK_APPLICATION });
    creditApplicationService.unlockApplication(app_id)
        .then((unlock_app) => {
            dispatch({ type: UNLOCK_APPLICATION_SUCCESS, payload: unlock_app });
            NotificationManager.success("Application unlock successfully!");
        })
        .catch((error) => {
            dispatch({ type: UNLOCK_APPLICATION_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- eamilPlan
* Descrption :- this function use for send plan mail
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const sendEstEmail = (data) => (dispatch) => {
    dispatch({ type: APPLICTION_EMAIL_REQUEST, });
    creditApplicationService.sendEstEmail(data)
        .then((plan) => {
            dispatch({ type: APPLICTION_EMAIL_SUCCESS });
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: APPLICTION_EMAIL_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- eamilPlan
* Descrption :- this function use for send plan mail
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const eamilPlan = (score, loan_amount, plan_id, procedure_date) => (dispatch) => {
    dispatch({ type: APPLICTION_EMAIL_REQUEST, });
    creditApplicationService.eamilPlan(score, loan_amount, plan_id, procedure_date)
        .then((plan) => {
            dispatch({ type: APPLICTION_EMAIL_SUCCESS });
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: APPLICTION_EMAIL_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- All Documents
* Descrption :- this function use for get application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const applicationDoc = (id) => (dispatch) => {
    dispatch({ type: ALL_APPLICATION_DOC_DETAILS, });
    creditApplicationService.applicationDoc(id)
        .then((all_doc) => {
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_SUCCESS, payload: all_doc });
        })
        .catch((error) => {
            dispatch({ type: ALL_APPLICATION_DOC_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- printEstimatePlan
* Descrption :- this function use for download plan estimate
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 21, 2019
*/

export const printEstimatePlan = (score, loan_amount, plan_id) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    creditApplicationService.printEstimatePlan(score, loan_amount, plan_id)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- estimatePlanClear
* Descrption :- this function use for clear exists details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/

export const estimatePlanClear = (score, amount) => (dispatch) => {
    dispatch({ type: ESTIMATE_DETAILS_FAILURE });
}

/*
* Title :- applicationEmail
* Descrption :- this function use for send email
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/
export const applicationEmail = (id, type) => (dispatch) => {
    dispatch({ type: APPLICTION_EMAIL_REQUEST, });
    creditApplicationService.applicationEmail(id, type)
        .then((plan) => {
            dispatch({ type: APPLICTION_EMAIL_SUCCESS });
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: APPLICTION_EMAIL_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- applicationFinalDetails
* Descrption :- this function use for get customer information according to application
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/
export const applicationFinalDetails = (id) => (dispatch) => {
    dispatch({ type: CUSTOMER_INFORMATION_REQUEST, });
    creditApplicationService.applicationFinalDetails(id)
        .then((plan) => {
            dispatch({ type: CUSTOMER_INFORMATION_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_INFORMATION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- estimatePlan
* Descrption :- this function use for get estimate plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/

export const estimatePlan = (score, amount, loan_type, monthly_amount) => (dispatch) => {
    dispatch({ type: ESTIMATE_DETAILS_REQUEST, });
    creditApplicationService.estimatePlan(score, amount, loan_type, monthly_amount)
        .then((plan) => {
            dispatch({ type: ESTIMATE_DETAILS_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: ESTIMATE_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- updateAppExpireDate
* Descrption :- this function use for update application expiry date
* Author :- Cogniz software so & Aman 
* Date :- Aug 23, 2019
*/

export const updateAppExpireDate = (formData, history) => (dispatch) => {
    dispatch({ type: APP_EXPIRE_DATE_UPDATE });
    creditApplicationService.updateAppExpireDate(formData)
        .then((appExpireDateUpdate) => {
            dispatch({ type: APP_EXPIRE_DATE_UPDATE_SUCCESS, payload: appExpireDateUpdate });
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: APP_EXPIRE_DATE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateOverrideAmount
* Descrption :- this function use for update application expiry date
* Author :- Cogniz software so & Aman 
* Date :- Aug 23, 2019
*/

export const updateOverrideAmount = (formData, history) => (dispatch) => {
    dispatch({ type: APP_OVERRIDE_AMOUNT_UPDATE });
    creditApplicationService.updateOverrideAmount(formData)
        .then((appOvrAmtUpdate) => {
            dispatch({ type: APP_OVERRIDE_AMOUNT_UPDATE_SUCCESS, payload: appOvrAmtUpdate });
        })
        .catch((error) => {
            dispatch({ type: APP_OVERRIDE_AMOUNT_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- printPlan
* Descrption :- this function use for download plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 21, 2019
*/

export const printPlan = (application_id, loan_amount, plan_id, procedure_date) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    creditApplicationService.printPlan(application_id, loan_amount, plan_id, procedure_date)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- planReset
* Descrption :- this function use for reset Plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 20, 2019
*/

export const planReset = (id) => (dispatch) => {
    dispatch({ type: APPlICTION_PLAN_RESET, payload: id });
}

/*
* Title :- applicationDetailsData
* Descrption :- this function use for get application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 13, 2019
*/

export const applicationDetailsData = (id) => (dispatch) => {
    dispatch({ type: APPlICTION_DETAILS_REQUEST });
    creditApplicationService.applicationDetailsData(id)
        .then((search) => {
            dispatch({ type: APPlICTION_DETAILS_SUCCESS, payload: search });
        })
        .catch((error) => {
            dispatch({ type: APPlICTION_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- searchCustomerApplication
* Descrption :- this function use for search customer application
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/

export const searchCustomerApplication = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_SEARCH_APPlICTION_REQUEST });
    creditApplicationService.searchCustomerApplication(data)
        .then((search) => {
            dispatch({ type: CUSTOMER_SEARCH_APPlICTION_SUCCESS, payload: search });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SEARCH_APPlICTION_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- downloadAgrement
* Descrption :- this function use for download application agrement
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const downloadAgrement = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    creditApplicationService.downloadAgrement(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- uploadAgreement
* Descrption :- this function use for upload application aggrement
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const uploadAgreement = (datafrom) => (dispatch) => {
    dispatch({ type: UPLOAD_DOCUMENT_REQUEST, });
    creditApplicationService.uploadAgreement(datafrom)
        .then((doc) => {
            dispatch({ type: UPLOAD_DOCUMENT_SUCCESS, payload: doc });
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: UPLOAD_DOCUMENT_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- uploadRejectionAgreement
* Descrption :- this function use for upload application aggrement
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const uploadRejectionAgreement = (datafrom) => (dispatch) => {
    dispatch({ type: UPLOAD_REJECTION_DOCUMENT_REQUEST, });
    creditApplicationService.uploadRejectionAgreement(datafrom)
        .then((reject_doc) => {
            dispatch({ type: UPLOAD_REJECTION_DOCUMENT_SUCCESS, payload: reject_doc });
            NotificationManager.success(reject_doc.message);
        })
        .catch((error) => {
            dispatch({ type: UPLOAD_REJECTION_DOCUMENT_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- getDoctors
* Descrption :- this function use for get doctors according to location
* Author :- Cogniz software & ramesh Kumar 
* Date :- 23 Oct 2019
*/

export const getDoctors = (id) => (dispatch) => {
    dispatch({ type: DOCTORS_LIST });
    creditApplicationService.getDoctors(id)
        .then((state) => {
            dispatch({ type: DOCTORS_LIST_SUCCESS, payload: state });
        })
        .catch((error) => {
            dispatch({ type: DOCTORS_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- getStates
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const getSpeciality = (id) => (dispatch) => {
    dispatch({ type: SPECIALITY_LIST });
    creditApplicationService.getSpeciality(id)
        .then((state) => {
            dispatch({ type: SPECIALITY_LIST_SUCCESS, payload: state });
        })
        .catch((error) => {
            dispatch({ type: SPECIALITY_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- creditApplicationRemoveManualActions
* Descrption :- this function use for search customer
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/
export const creditApplicationRemoveManualActions = (id) => (dispatch) => {
    dispatch({ type: REVIEW_MANUAL_ACTION, });
}

/*
* Title :- creditApplicationReviewDocument
* Descrption :- this function use for get application document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

export const creditApplicationReviewDocument = (id) => (dispatch) => {
    dispatch({ type: REVIEW_DOCUMENT_REQUEST, });
    creditApplicationService.creditApplicationReviewDocument(id)
        .then((doc) => {
            dispatch({ type: REVIEW_DOCUMENT_SUCCESS, payload: doc });
        })
        .catch((error) => {
            dispatch({ type: REVIEW_DOCUMENT_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- creditApplicationUploadDocument
* Descrption :- this function use for upload credit application document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

export const creditApplicationUploadDocument = (datafrom) => (dispatch) => {
    dispatch({ type: UPLOAD_DOCUMENT_REQUEST, });
    creditApplicationService.creditApplicationUploadDocument(datafrom)
        .then((doc) => {
            dispatch({ type: UPLOAD_DOCUMENT_SUCCESS, payload: doc });
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: UPLOAD_DOCUMENT_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- rejectApplication
* Descrption :- this function use for get reject application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/

export const rejectApplication = (id) => (dispatch) => {
    dispatch({ type: REJECT_ACTION_REQUEST, });
    creditApplicationService.rejectApplication(id)
        .then((appStatus) => {
            dispatch({ type: REJECT_ACTION_SUCCESS, payload: appStatus });
        })
        .catch((error) => {
            dispatch({ type: REJECT_ACTION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- creditApplicationManualActions
* Descrption :- this function use for manual action
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/

export const creditApplicationManualActions = (id, type, comment) => (dispatch) => {
    dispatch({ type: MANUAL_ACTION_REQUEST, });
    creditApplicationService.creditApplicationManualActions(id, type, comment)
        .then((appStatus) => {
            dispatch({ type: MANUAL_ACTION_SUCCESS, payload: appStatus });
            NotificationManager.success(appStatus.message);
        })
        .catch((error) => {
            dispatch({ type: MANUAL_ACTION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- creditApplicationReview
* Descrption :- this function use for get details for review application
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 18, 2019
*/

export const creditApplicationReview = (id) => (dispatch) => {
    dispatch({ type: REVIEW_APPLICATION_REQUEST, });
    creditApplicationService.creditApplicationReview(id)
        .then((appdetails) => {
            dispatch({ type: REVIEW_APPLICATION_SUCCESS, payload: appdetails });
        })
        .catch((error) => {
            dispatch({ type: REVIEW_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- submitSearchCustomer
* Descrption :- this function use for search customer
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/

export const removeSearchCustomer = () => (dispatch) => {
    dispatch({ type: REMOVE_CUSTOMER_SEARCH });
}


/*
* Title :- submitSearchCustomer
* Descrption :- this function use for search customer
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/

export const submitSearchCustomer = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_SEARCH_REQUEST });
    creditApplicationService.submitSearchCustomer(data)
        .then((search) => {
            let addError = [];
            let bankError = [];
            search.customerAddress && search.customerAddress.map(function (item, idx) {
                addError = addError.concat([{
                    address1: "",
                    country: "",
                    state: "",
                    region: "",
                    city: "",
                    zip_code: "",
                    how_long: "",
                    phone_no: "",
                }]);
                dispatch(addMore());
                dispatch(getStates(item.country, idx));
                //dispatch(getRegion(item.state, idx));

            });
            if (search.customerDetails.billing_country != '') {
                dispatch(getStates(search.customerDetails.billing_country));
            }
            search.bankDetails && search.bankDetails.map(function (item, idx) {
                bankError = bankError.concat([{
                    bank_name: "",
                    bank_address: "",
                    rounting_no: "",
                    bank_ac: "",
                    account_name: "",
                    account_type: "",
                }]);
                //dispatch(getRegion(item.state, idx));

            });
            /*if(search.customerDetails.billing_country != ''){
                dispatch(getRegion(search.customerDetails.billing_state));
            }*/

            search.addError = addError;
            search.bankError = bankError;
            dispatch({ type: CUSTOMER_SEARCH_SUCCESS, payload: search });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SEARCH_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- submitApplicationProvider
* Descrption :- this function use for submit application data
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jun 12 2019
*/

export const submitApplicationProvider = (data) => (dispatch) => {
    dispatch({ type: APPLICATION_SUBMIT });
    creditApplicationService.submitApplicationProvider(data)
        .then((submit) => {
            dispatch({ type: APPLICATION_SUBMIT_SUCCESS, payload: submit });
            if (submit.applicationStatus == 0) {
                NotificationManager.error(submit.message);
            } else {
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: APPLICATION_SUBMIT_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- creditApplicationListProvider
* Descrption :- this function use for get crdit application list
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 11, 2019
*/

export const creditApplicationListProvider = (status_id) => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST, });
    creditApplicationService.creditApplicationListProvider(status_id)
        .then((list) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_SUCCESS, payload: list });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- planDetails
* Descrption :- this function use for view application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const viewApplication = (id) => (dispatch) => {
    dispatch({ type: VIEW_APPLICATION_REQUEST, });
    creditApplicationService.viewApplication(id)
        .then((plan) => {
            dispatch({ type: VIEW_APPLICATION_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: VIEW_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- planDetails
* Descrption :- this function use for get single plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const planDetails = (id) => (dispatch) => {
    dispatch({ type: SINGLE_PLAN_REQUEST, });
    creditApplicationService.planDetails(id)
        .then((plan) => {
            dispatch({ type: SINGLE_PLAN_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: SINGLE_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- createPaymentPrintPlan
* Descrption :- this function use for create plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const createPaymentPrintPlan = (application_id, plan_id, details) => (dispatch) => {
    dispatch({ type: CREATE_PRINT_PLAN_REQUEST, });
    creditApplicationService.createPaymentPrintPlan(application_id, plan_id, details)
        .then((plan) => {
            dispatch({ type: CREATE_PRINT_PLAN_SUCCESS, payload: plan });
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: CREATE_PRINT_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- createPaymentPlan
* Descrption :- this function use for create plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const createPaymentPlan = (application_id, plan_id, details) => (dispatch) => {
    dispatch({ type: CREATE_PLAN_REQUEST, });
    creditApplicationService.createPaymentPlan(application_id, plan_id, details)
        .then((plan) => {
            dispatch({ type: CREATE_PLAN_SUCCESS, payload: plan });
            NotificationManager.success(plan.message);
        })
        .catch((error) => {
            dispatch({ type: CREATE_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- paymentPlan
* Descrption :- this function use for get playment plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const paymentPlan = (id, amount, loan_type, monthly_amount, procedure_date) => (dispatch) => {
    dispatch({ type: PAYMENT_PLAN_REQUEST, });
    creditApplicationService.paymentPlan(id, amount, loan_type, monthly_amount, procedure_date)
        .then((plan) => {
            dispatch({ type: PAYMENT_PLAN_SUCCESS, payload: plan });
        })
        .catch((error) => {
            dispatch({ type: PAYMENT_PLAN_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- creditApplicationList
* Descrption :- this function use for get crdit application list
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const creditApplicationList = (status_id) => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST, });
    dispatch({type: CUSTOMER_PROFILE_UPDATE_REQUEST});
    creditApplicationService.creditApplicationList(status_id)
        .then((list) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_SUCCESS, payload: list });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- applicationOption
* Descrption :- this function use for get credit application option data
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const applicationOption = () => (dispatch) => {
    dispatch({ type: APPLICATION_OPTION_LIST });
    creditApplicationService.applicationOption()
        .then((option) => {
            dispatch(getStates(option.countries[0].id, 0));
            dispatch(getStates(option.countries[0].id));
            dispatch(getStates(option.countries[0].id, 0, 1));
            dispatch({ type: APPLICATION_OPTION_SUCCESS, payload: option });
        })
        .catch((error) => {
            dispatch({ type: APPLICATION_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getStates
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const getStates = (id, idx, Cidx) => (dispatch) => {
    dispatch({ type: STATES_LIST });
    creditApplicationService.getStates(id)
        .then((state) => {
            state.idx = idx;
            state.Cidx = Cidx;
            dispatch({ type: STATES_LIST_SUCCESS, payload: state });
        })
        .catch((error) => {
            dispatch({ type: STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- addMore
* Descrption :- this function use get region according to state
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const getRegion = (id, idx) => (dispatch) => {
    dispatch({ type: REGION_LIST });
    creditApplicationService.getRegion(id)
        .then((region) => {
            region.idx = idx
            dispatch({ type: REGION_LIST_SUCCESS, payload: region });
        })
        .catch((error) => {
            dispatch({ type: REGION_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- submitApplication
* Descrption :- this function use for submit application data
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const submitApplication = (data) => (dispatch) => {
    dispatch({ type: APPLICATION_SUBMIT });
    creditApplicationService.submitApplication(data)
        .then((submit) => {
            dispatch({ type: APPLICATION_SUBMIT_SUCCESS, payload: submit });
            if (submit.applicationStatus == 0) {
                NotificationManager.error(submit.message);
            } else {
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: APPLICATION_SUBMIT_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- addMore
* Descrption :- this function use add new state value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const addMore = () => (dispatch) => {
    dispatch({ type: ADD_MORE_LIST });
}
/*
* Title :- removeAddMore
* Descrption :- this function use for remove old value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const removeAddMore = (idx, Cidx) => (dispatch) => {
    var data = {
        idx: idx,
        Cidx: Cidx,
    }
    dispatch({ type: REMOVE_ADD_MORE_LIST, payload: data });
}



