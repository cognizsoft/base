import { NotificationManager } from 'react-notifications';
import { statesService } from '../apifile';
import {
    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,
    STATES_INSERT,
    STATES_INSERT_SUCCESS,
    STATES_INSERT_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,
    STATES_UPDATE,
    STATES_UPDATE_SUCCESS,
    STATES_UPDATE_FAILURE,

    COUNTRY_LIST,
    COUNTRY_LIST_SUCCESS,
    COUNTRY_LIST_FAILURE,
} from 'Actions/types';


/*
* Title :- getAllCountries
* Descrption :- this function use for get state list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 22 March 2019
*/
export const getAllCountries = () => (dispatch) => {
    dispatch({ type: COUNTRY_LIST });
    statesService.getAllCountry()
        .then((country) => {
            dispatch({ type: COUNTRY_LIST_SUCCESS, payload: country.result });
        })
        .catch((error) => {
            dispatch({ type: COUNTRY_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- getAllUsers
* Descrption :- this function use for get state list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 22 March 2019
*/
export const getAllstates = () => (dispatch) => {
    dispatch({ type: STATES_LIST });
    statesService.getAllstates()
        .then((user) => {
            dispatch({ type: STATES_LIST_SUCCESS, payload: user.result });
        })
        .catch((error) => {
            dispatch({ type: STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- insertState
* Descrption :- this function use for insert state information
* Author :- Hp & Aman 
* Date :- 22 April 2019
*/

export const insertState = (formData) => (dispatch) => {
    dispatch({ type: STATES_INSERT });
    statesService.insertState(formData)
        .then((user) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: STATES_INSERT_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: STATES_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkNameExist
* Descrption :- this function use for check state name exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkSNameExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    statesService.checkSNameExist(value, md_id)
        .then((user) => {
            user.abbreviation=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: user});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkAbbreviationExist
* Descrption :- this function use for check state Abbreviation exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkAbbreviationExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    statesService.checkAbbreviationExist(value, md_id)
        .then((user) => {
            user.abbreviation=1;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateState
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const updateState = (formData) => (dispatch) => {
    dispatch({ type: STATES_UPDATE });
    statesService.updateState(formData)
        .then((state) => {
            dispatch({ type: STATES_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('State Updated!');
        })
        .catch((error) => {
            dispatch({ type: STATES_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}