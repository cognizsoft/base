import { NotificationManager } from 'react-notifications';
import { AdminReportService } from '../apifile';
import {
    ADMIN_REPORT_LIST,
    ADMIN_REPORT_LIST_SUCCESS,
    ADMIN_REPORT_LIST_FAILURE,
    REPORT_DOWNLOAD_LIST_REQUEST,
    REPORT_DOWNLOAD_LIST_SUCCESS,
    REPORT_DOWNLOAD_LIST_FAILURE,
    PROVIDER_INVOICE_REPORT_LIST,
    PROVIDER_INVOICE_REPORT_LIST_SUCCESS,
    PROVIDER_INVOICE_REPORT_LIST_FAILURE,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,
    PRINT_INVOICE_REQUEST,
    PRINT_INVOICE_SUCCESS,
    PRINT_INVOICE_FAILURE,

    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_REQUEST,
    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_SUCCESS,
    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_FAILURE,
    
} from 'Actions/types';

/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report XLS
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/

export const customerWithdrawalReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerWithdrawalReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report PDF
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/

export const customerWithdrawalReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerWithdrawalReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerWithdrawalReportFilter
* Descrption :- this function use for get customer withdrawal report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2020
*/

export const customerWithdrawalReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerWithdrawalReportFilter(data)
        .then((invoice) => {
        	dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}


export const customerfullInvoiceDownload = (data) => (dispatch) => {
    dispatch({ type: PRINT_INVOICE_REQUEST });
    AdminReportService.customerfullInvoiceDownload(data)
        .then((invoice) => {
        	dispatch({ type: PRINT_INVOICE_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PRINT_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerInvoiceReportDownloadXLS
* Descrption :- this function use for download customr weekly & monthly invoice xlsx reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerInvoiceReportDownload
* Descrption :- this function use for download customer weekly & monthly invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerInvoiceReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- customerInvoiceReportFilter
* Descrption :- this function use for get customer invoice filter details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerInvoiceReportFilter(data)
        .then((invoice) => {
        	dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerWeekMonthInvoiceReport
* Descrption :- this function use for get customer week month reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerWeekMonthInvoiceReport = () => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerWeekMonthInvoiceReport()
        .then((invoice) => {
        	dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- providerInvoiceReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerInvoiceReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerSingleInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerSingleInvoiceReportDownload = (invoice_id, provider_id) => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_REQUEST });
    AdminReportService.providerSingleInvoiceReportDownload(invoice_id, provider_id)
        .then((fullReportList) => {
            dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerInvoiceReportFilter
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/

export const providerInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_REPORT_LIST });
    AdminReportService.providerInvoiceReportFilter(data)
        .then((invoice) => {
        	dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerInvoiceReport
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/

export const providerInvoiceReport = () => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_REPORT_LIST });
    AdminReportService.providerInvoiceReport()
        .then((invoice) => {
        	dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- loanReportDownloadXLS
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.loanReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- loanReportDownload
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.loanReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- loanReportFilter
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportFilter = (formData) => (dispatch) => {
    dispatch({ type: ADMIN_REPORT_LIST });
    AdminReportService.loanReportFilter(formData)
        .then((fullReportList) => {
        	dispatch({ type: ADMIN_REPORT_LIST_SUCCESS, payload:fullReportList.result});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_REPORT_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- FinancialChargesList
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const fullReportList = (formData, history) => (dispatch) => {
    dispatch({ type: ADMIN_REPORT_LIST });
    AdminReportService.fullReportList(formData)
        .then((fullReportList) => {
        	dispatch({ type: ADMIN_REPORT_LIST_SUCCESS, payload:fullReportList.result});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_REPORT_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


