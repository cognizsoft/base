import { NotificationManager } from 'react-notifications';
import { loanAmountService } from '../apifile';
import {

    LOAN_AMOUNT_LIST,
    LOAN_AMOUNT_LIST_SUCCESS,
    LOAN_AMOUNT_LIST_FAILURE,

    LOAN_AMOUNT_UPDATE,
    LOAN_AMOUNT_UPDATE_SUCCESS,
    LOAN_AMOUNT_UPDATE_FAILURE,

    LOAN_AMOUNT_DELETE,
    LOAN_AMOUNT_DELETE_SUCCESS,
    LOAN_AMOUNT_DELETE_FAILURE,

    LOAN_AMOUNT_INSERT,
    LOAN_AMOUNT_INSERT_SUCCESS,
    LOAN_AMOUNT_INSERT_FAILURE,

    LOAN_AMOUNT_EXIST,
    LOAN_AMOUNT_EXIST_SUCCESS,
    LOAN_AMOUNT_EXIST_FAILURE
} from 'Actions/types';


/*
* Title :- userTypesService
* Descrption :- this function use for name exist or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- May, 05 2019
*/
export const checkLoanAmountExist = (value,md_id) => (dispatch) => {
    dispatch({type: LOAN_AMOUNT_EXIST});
    loanAmountService.checkLoanAmountExist(value,md_id)
        .then((master) => {
            dispatch({ type: LOAN_AMOUNT_EXIST_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: LOAN_AMOUNT_EXIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const listLoanAmount = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: LOAN_AMOUNT_LIST });
    loanAmountService.listLoanAmount(formData)
        .then((userType) => {
        	dispatch({ type: LOAN_AMOUNT_LIST_SUCCESS, payload:userType.result});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: LOAN_AMOUNT_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateLoanAmount = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: LOAN_AMOUNT_UPDATE});
        loanAmountService.updateLoanAmount(formData)
        .then((userTypeUpdate) => {
            dispatch({ type: LOAN_AMOUNT_UPDATE_SUCCESS, payload:userTypeUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: LOAN_AMOUNT_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteLoanAmount = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: LOAN_AMOUNT_DELETE});
    loanAmountService.deleteLoanAmount(formData)
    .then((userTypeDelete) => {
        dispatch({ type: LOAN_AMOUNT_DELETE_SUCCESS, payload:userTypeDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: LOAN_AMOUNT_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertLoanAmount = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: LOAN_AMOUNT_INSERT});
        loanAmountService.insertLoanAmount(formData)
        .then((loanAmountInsert) => {
            dispatch({ type: LOAN_AMOUNT_INSERT_SUCCESS, payload:loanAmountInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: LOAN_AMOUNT_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

