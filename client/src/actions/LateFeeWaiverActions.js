import { NotificationManager } from 'react-notifications';
import { lateFeeWaiverService } from '../apifile';
import {
    LATE_FEE_WAIVER_LIST,
    LATE_FEE_WAIVER_LIST_SUCCESS,
    LATE_FEE_WAIVER_LIST_FAILURE,

    LATE_FEE_WAIVER_UPDATE,
    LATE_FEE_WAIVER_UPDATE_SUCCESS,
    LATE_FEE_WAIVER_UPDATE_FAILURE,

    LATE_FEE_WAIVER_DELETE,
    LATE_FEE_WAIVER_DELETE_SUCCESS,
    LATE_FEE_WAIVER_DELETE_FAILURE,

    LATE_FEE_WAIVER_INSERT,
    LATE_FEE_WAIVER_INSERT_SUCCESS,
    LATE_FEE_WAIVER_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

    MASTER_DATA_VALUE_REASON_LIST,
    MASTER_DATA_VALUE_REASON_LIST_SUCCESS,
    MASTER_DATA_VALUE_REASON_LIST_FAILURE,

} from 'Actions/types';

/*
* Title :- master data value waiver
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const lateFeeWaiverMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    lateFeeWaiverService.getAllLateFeeWaiverMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- master data value reason
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const lateFeeWaiverReasonMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_REASON_LIST });
    lateFeeWaiverService.getAllLateFeeWaiverReasonMasterDataValue(formData)
        .then((masterDataValueReason) => {
            dispatch({ type: MASTER_DATA_VALUE_REASON_LIST_SUCCESS, payload:masterDataValueReason.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_REASON_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- lateFeeWaiverList
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const lateFeeWaiverList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: LATE_FEE_WAIVER_LIST });
    lateFeeWaiverService.getAllLateFeeWaiver(formData)
        .then((lateFeeWaiver) => {
        	dispatch({ type: LATE_FEE_WAIVER_LIST_SUCCESS, payload:lateFeeWaiver.result});
        })
        .catch((error) => {
            dispatch({ type: LATE_FEE_WAIVER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updatelateFeeWaiver
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateLateFeeWaiver = (formData, history) => (dispatch) => {
    dispatch({type: LATE_FEE_WAIVER_UPDATE});
        lateFeeWaiverService.updateLateFeeWaiver(formData)
        .then((lateFeeWaiverUpdate) => {
            dispatch({ type: LATE_FEE_WAIVER_UPDATE_SUCCESS, payload:lateFeeWaiverUpdate.result});
            NotificationManager.success('Late Fee Waiver Updated!');
        })
        .catch((error) => {
            dispatch({ type: LATE_FEE_WAIVER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- insertlateFeeWaiver
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertLateFeeWaiver = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: LATE_FEE_WAIVER_INSERT});
        lateFeeWaiverService.insertLateFeeWaiver(formData)
        .then((lateFeeWaiverInsert) => {
            dispatch({ type: LATE_FEE_WAIVER_INSERT_SUCCESS, payload:lateFeeWaiverInsert});
            NotificationManager.success('Record added successfully!');
        })
        .catch((error) => {
            dispatch({ type: LATE_FEE_WAIVER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

