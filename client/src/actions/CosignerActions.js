import { NotificationManager } from 'react-notifications';
import { cosignerService } from '../apifile';
import {
    CREDIT_APPLICATION_LIST,
    CREDIT_APPLICATION_LIST_SUCCESS,
    CREDIT_APPLICATION_LIST_FAILURE,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,

    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE,

    CUSTOMER_PROFILE_UPDATE_REQUEST,

    VIEW_CO_CUSTOMER_REQUEST,
    VIEW_CO_CUSTOMER_SUCCESS,
    VIEW_CO_CUSTOMER_FAILURE,


    
    CUSTOMER_PROFILE_UPDATE_SUCCESS,
    CUSTOMER_PROFILE_UPDATE_FAILURE
} from 'Actions/types';

/*
* Title :- creditApplicationService
* Descrption :- this function use for get crdit application list for co-signer
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 15, 2019
*/

export const creditApplicationListCosigner = () => (dispatch) => {
    dispatch({ type: CREDIT_APPLICATION_LIST, });
    cosignerService.creditApplicationListCosigner()
        .then((list) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_SUCCESS, payload: list });
        })
        .catch((error) => {
            dispatch({ type: CREDIT_APPLICATION_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- cosignerDashboardInvoiceReportFilter
* Descrption :- this function use for get cosigner invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 19, 2020
*/

export const cosignerDashboardInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST });
    cosignerService.cosignerDashboardInvoiceReportFilter(data)
        .then((invoice) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload: invoice });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const cosignerDashboardInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST });
    cosignerService.cosignerDashboardInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const cosignerDashboardInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST });
    cosignerService.cosignerDashboardInvoiceReportDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- viewCosigner
* Descrption :- this function use to get co-signer details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 19, 2020
*/

export const viewCosigner = (id) => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_UPDATE_REQUEST });
    dispatch({ type: CREDIT_APPLICATION_LIST, });
    dispatch({ type: VIEW_CO_CUSTOMER_REQUEST, });
    cosignerService.viewCosigner(id)
        .then((plan) => {
            dispatch({ type: VIEW_CO_CUSTOMER_SUCCESS, payload: plan });
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: VIEW_CO_CUSTOMER_FAILURE });
            NotificationManager.error(error);
        });
}


export const updateCosignerProfile = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_UPDATE_REQUEST });
    cosignerService.updateCosignerProfile(data)
        .then((submit) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_SUCCESS, payload: submit });
            if (submit.applicationStatus == 0) {
                NotificationManager.error(submit.message);
            } else {
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_FAILURE });
            NotificationManager.error(error);
        });
}


