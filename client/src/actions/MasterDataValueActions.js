import { NotificationManager } from 'react-notifications';
import { userService, masterValuesService } from '../apifile';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,

    MASTER_VALUE_LIST,
    MASTER_VALUE_LIST_SUCCESS,
    MASTER_VALUE_LIST_FAILURE,

    MASTER_VALUE_UPDATE,
    MASTER_VALUE_UPDATE_SUCCESS,
    MASTER_VALUE_UPDATE_FAILURE,

    MASTER_VALUE_DELETE,
    MASTER_VALUE_DELETE_SUCCESS,
    MASTER_VALUE_DELETE_FAILURE,

    MASTER_VALUE_INSERT,
    MASTER_VALUE_INSERT_SUCCESS,
    MASTER_VALUE_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE
} from 'Actions/types';

/*
* Title :- updateMaster
* Descrption :- this function use for update master item
* Author :- CognizSoft & Ramesh Kumar
* Date :- 11 April 2019
*/
export const checkMasterValueExist = (value,md_id,name) => (dispatch) => {
    dispatch({type: EXIST_UPDATE});
    masterValuesService.checkMasterValueExist(value,md_id,name)
        .then((master) => {
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const masterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    masterValuesService.getAllMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const masterValueList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_VALUE_LIST });
    masterValuesService.getAllMasterValue(formData)
        .then((masterValue) => {
        	dispatch({ type: MASTER_VALUE_LIST_SUCCESS, payload:masterValue.result});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateMasterValue
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateMasterValue = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: MASTER_VALUE_UPDATE});
        masterValuesService.updateMasterValues(formData)
        .then((masterValueUpdate) => {
            dispatch({ type: MASTER_VALUE_UPDATE_SUCCESS, payload:masterValueUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_VALUE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteMasterValue = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: MASTER_VALUE_DELETE});
    masterValuesService.deleteMasterValues(formData)
    .then((masterValueDelete) => {
        dispatch({ type: USER_TYPE_DELETE_SUCCESS, payload:masterValueDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: MASTER_VALUE_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertMasterValue = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: MASTER_VALUE_INSERT});
        masterValuesService.insertMasterValues(formData)
        .then((masterValueInsert) => {
            dispatch({ type: MASTER_VALUE_INSERT_SUCCESS, payload:masterValueInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_VALUE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

