import { NotificationManager } from 'react-notifications';
import { masterService } from '../apifile';
import {
    MASTER_INSERT,
    MASTER_INSERT_SUCCESS,
    MASTER_INSERT_FAILURE,

    MASTER_LIST,
    MASTER_LIST_SUCCESS,
    MASTER_LIST_FAILURE,

    MASTER_UPDATE,
    MASTER_UPDATE_SUCCESS,
    MASTER_UPDATE_FAILURE,

    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,
} from 'Actions/types';

/*
* Title :- updateMaster
* Descrption :- this function use for update master item
* Author :- CognizSoft & Ramesh Kumar
* Date :- 12 April 2019
*/

export const changeMasterPage = (page) => (dispatch) => {
	dispatch({ type: MASTER_LIST });
    masterService.changeMasterPage(page)
        .then((list) => {
        	dispatch({ type: MASTER_LIST_SUCCESS, payload:list});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateMaster
* Descrption :- this function use for update master item
* Author :- CognizSoft & Ramesh Kumar
* Date :- 11 April 2019
*/
export const checkNameExist = (value,md_id) => (dispatch) => {
    dispatch({type: EXIST_UPDATE});
    masterService.checkNameExist(value,md_id)
        .then((master) => {
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateMaster
* Descrption :- this function use for update master item
* Author :- CognizSoft & Ramesh Kumar
* Date :- 11 April 2019
*/

export const updateMaster = (formData) => (dispatch) => {
    dispatch({type: MASTER_UPDATE});
    masterService.updateMaster(formData)
        .then((master) => {
            dispatch({ type: MASTER_UPDATE_SUCCESS, payload:master.result});
        })
        .catch((error) => {
            dispatch({ type: MASTER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertUserType
* Descrption :- this function use for insert master
* Author :- Hp & Aman 
* Date :- 04 April 2019
*/

export const insertMaster = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: MASTER_INSERT});
    masterService.insertMaster(formData)
        .then((userTypeInsert) => {
            dispatch({ type: MASTER_INSERT_SUCCESS, payload:userTypeInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- signinUserInApp
* Descrption :- this function use for get master list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 04 April 2019
*/

export const masterList = () => (dispatch) => {
	dispatch({ type: MASTER_LIST });
    masterService.getAll()
        .then((list) => {
        	dispatch({ type: MASTER_LIST_SUCCESS, payload:list});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

