import { NotificationManager } from 'react-notifications';
import { CustomerSupportService } from '../apifile';
import {
    CS_ACCOUNT_FILTER_REQUEST,
    CS_ACCOUNT_FILTER_REQUEST_SUCCESS,
    CS_ACCOUNT_FILTER_REQUEST_FAILURE,

    CS_GET_QUESTION_REQUEST,
    CS_GET_QUESTION_REQUEST_SUCCESS,
    CS_GET_QUESTION_REQUEST_FAILURE,

    CS_CREATE_SUPPORT_TICKET_REQUEST,
    CS_CREATE_SUPPORT_TICKET_SUCCESS,
    CS_CREATE_SUPPORT_TICKET_FAILURE,

    CS_TICKET_LIST_REQUEST,
    CS_TICKET_LIST_REQUEST_SUCCESS,
    CS_TICKET_LIST_REQUEST_FAILURE
} from 'Actions/types';

export const getAllTickets = (appid) => (dispatch) => {
    dispatch({ type: CS_TICKET_LIST_REQUEST, });
    CustomerSupportService.getAllTickets(appid)
        .then((tickets) => {
            dispatch({ type: CS_TICKET_LIST_REQUEST_SUCCESS, payload:tickets});
        })
        .catch((error) => {
            dispatch({ type: CS_TICKET_LIST_REQUEST_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- createSupportTicket
* Descrption :- this function use for create support ticket
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/
export const createSupportTicket = (formData, history) => (dispatch) => {
    dispatch({type: CS_CREATE_SUPPORT_TICKET_REQUEST});
        CustomerSupportService.createSupportTicket(formData)
        .then((ticket) => {
            dispatch({ type: CS_CREATE_SUPPORT_TICKET_SUCCESS, payload:ticket});
        })
        .catch((error) => {
            dispatch({ type: CS_CREATE_SUPPORT_TICKET_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getQuestions
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/
export const getQuestions = (user_id) => (dispatch) => {
    dispatch({ type: CS_GET_QUESTION_REQUEST });
    CustomerSupportService.getQuestions(user_id)
        .then((questions) => {
            dispatch({ type: CS_GET_QUESTION_REQUEST_SUCCESS, payload: questions });
        })
        .catch((error) => {
            dispatch({ type: CS_GET_QUESTION_REQUEST_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- customerAccountFilter
* Descrption :- this function use for get customer account filter details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerAccountFilter = (data) => (dispatch) => {
    dispatch({ type: CS_ACCOUNT_FILTER_REQUEST });
    CustomerSupportService.customerAccountFilter(data)
        .then((accounts) => {
        	dispatch({ type: CS_ACCOUNT_FILTER_REQUEST_SUCCESS, payload:accounts});
        })
        .catch((error) => {
            dispatch({ type: CS_ACCOUNT_FILTER_REQUEST_FAILURE });
            NotificationManager.error(error);
        });
}


