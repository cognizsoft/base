import { NotificationManager } from 'react-notifications';
import { FinancialChargesService } from '../apifile';
import {
    FINANCIAL_CHARGES_LIST,
    FINANCIAL_CHARGES_LIST_SUCCESS,
    FINANCIAL_CHARGES_LIST_FAILURE,

    FINANCIAL_CHARGES_UPDATE,
    FINANCIAL_CHARGES_UPDATE_SUCCESS,
    FINANCIAL_CHARGES_UPDATE_FAILURE,

    FINANCIAL_CHARGES_DELETE,
    FINANCIAL_CHARGES_DELETE_SUCCESS,
    FINANCIAL_CHARGES_DELETE_FAILURE,

    FINANCIAL_CHARGES_INSERT,
    FINANCIAL_CHARGES_INSERT_SUCCESS,
    FINANCIAL_CHARGES_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

} from 'Actions/types';

/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const FinancialChargesMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    FinancialChargesService.getAllFinancialChargesMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- FinancialChargesList
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const FinancialChargesList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: FINANCIAL_CHARGES_LIST });
    FinancialChargesService.getAllFinancialCharges(formData)
        .then((FinancialCharges) => {
        	dispatch({ type: FINANCIAL_CHARGES_LIST_SUCCESS, payload:FinancialCharges.result});
        })
        .catch((error) => {
            dispatch({ type: FINANCIAL_CHARGES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateFinancialCharges
* Descrption :- this function use for update insert Payback data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateFinancialCharges = (formData, history) => (dispatch) => {
    dispatch({type: FINANCIAL_CHARGES_UPDATE});
        FinancialChargesService.updateFinancialCharges(formData)
        .then((FinancialChargesUpdate) => {
            dispatch({ type: FINANCIAL_CHARGES_UPDATE_SUCCESS, payload:FinancialChargesUpdate.result});
        })
        .catch((error) => {
            dispatch({ type: FINANCIAL_CHARGES_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}



/*
* Title :- insertFinancialCharges
* Descrption :- this function use for insert Payback data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertFinancialCharges = (formData, history) => (dispatch) => {
    dispatch({type: FINANCIAL_CHARGES_INSERT});
        FinancialChargesService.insertFinancialCharges(formData)
        .then((FinancialChargesInsert) => {
        	NotificationManager.success('Record added successfully!');
            dispatch({ type: FINANCIAL_CHARGES_INSERT_SUCCESS, payload:FinancialChargesInsert});
        })
        .catch((error) => {
            dispatch({ type: FINANCIAL_CHARGES_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

