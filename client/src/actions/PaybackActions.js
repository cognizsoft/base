import { NotificationManager } from 'react-notifications';
import { PaybackService } from '../apifile';
import {
    PAYBACK_LIST,
    PAYBACK_LIST_SUCCESS,
    PAYBACK_LIST_FAILURE,

    PAYBACK_UPDATE,
    PAYBACK_UPDATE_SUCCESS,
    PAYBACK_UPDATE_FAILURE,

    PAYBACK_DELETE,
    PAYBACK_DELETE_SUCCESS,
    PAYBACK_DELETE_FAILURE,

    PAYBACK_INSERT,
    PAYBACK_INSERT_SUCCESS,
    PAYBACK_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

} from 'Actions/types';

/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const PaybackMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    PaybackService.getAllPaybackMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const PaybackList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: PAYBACK_LIST });
    PaybackService.getAllPayback(formData)
        .then((Payback) => {
        	dispatch({ type: PAYBACK_LIST_SUCCESS, payload:Payback.result});
        })
        .catch((error) => {
            dispatch({ type: PAYBACK_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updatePayback
* Descrption :- this function use for update insert Payback data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updatePayback = (formData, history) => (dispatch) => {
    dispatch({type: PAYBACK_UPDATE});
        PaybackService.updatePayback(formData)
        .then((PaybackUpdate) => {
            dispatch({ type: PAYBACK_UPDATE_SUCCESS, payload:PaybackUpdate.result});
        })
        .catch((error) => {
            dispatch({ type: PAYBACK_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}



/*
* Title :- insertPayback
* Descrption :- this function use for insert Payback data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertPayback = (formData, history) => (dispatch) => {
    dispatch({type: PAYBACK_INSERT});
        PaybackService.insertPayback(formData)
        .then((PaybackInsert) => {
        	NotificationManager.success('Record added successfully!');
            dispatch({ type: PAYBACK_INSERT_SUCCESS, payload:PaybackInsert});
        })
        .catch((error) => {
            dispatch({ type: PAYBACK_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

