import { NotificationManager } from 'react-notifications';
import { termMonthService } from '../apifile';
import {

    TERM_MONTH_LIST,
    TERM_MONTH_LIST_SUCCESS,
    TERM_MONTH_LIST_FAILURE,

    TERM_MONTH_UPDATE,
    TERM_MONTH_UPDATE_SUCCESS,
    TERM_MONTH_UPDATE_FAILURE,

    TERM_MONTH_DELETE,
    TERM_MONTH_DELETE_SUCCESS,
    TERM_MONTH_DELETE_FAILURE,

    TERM_MONTH_INSERT,
    TERM_MONTH_INSERT_SUCCESS,
    TERM_MONTH_INSERT_FAILURE,

    TERM_MONTH_EXIST,
    TERM_MONTH_EXIST_SUCCESS,
    TERM_MONTH_EXIST_FAILURE
} from 'Actions/types';


/*
* Title :- userTypesService
* Descrption :- this function use for name exist or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- May, 05 2019
*/
export const checkTermMonthExist = (value,md_id) => (dispatch) => {
    dispatch({type: TERM_MONTH_EXIST});
    termMonthService.checkTermMonthExist(value,md_id)
        .then((master) => {
            dispatch({ type: TERM_MONTH_EXIST_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: TERM_MONTH_EXIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const listTermMonth = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: TERM_MONTH_LIST });
    termMonthService.listTermMonth(formData)
        .then((userType) => {
        	dispatch({ type: TERM_MONTH_LIST_SUCCESS, payload:userType.result});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: TERM_MONTH_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateTermMonth = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: TERM_MONTH_UPDATE});
        termMonthService.updateTermMonth(formData)
        .then((userTypeUpdate) => {
            dispatch({ type: TERM_MONTH_UPDATE_SUCCESS, payload:userTypeUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: TERM_MONTH_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteTermMonth = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: TERM_MONTH_DELETE});
    termMonthService.deleteTermMonth(formData)
    .then((userTypeDelete) => {
        dispatch({ type: TERM_MONTH_DELETE_SUCCESS, payload:userTypeDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: TERM_MONTH_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertTermMonth = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: TERM_MONTH_INSERT});
        termMonthService.insertTermMonth(formData)
        .then((termMonthInsert) => {
            dispatch({ type: TERM_MONTH_INSERT_SUCCESS, payload:termMonthInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: TERM_MONTH_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

