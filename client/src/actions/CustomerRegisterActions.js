
import { NotificationManager } from 'react-notifications';
import { customerRegisterService } from '../apifile';
import {
   CUSTOMER_REGISTER_REQUEST,
   CUSTOMER_REGISTER_SUCCESS,
   CUSTOMER_REGISTER_FAILURE,

   CUSTOMER_DETAIL_REQUEST,
   CUSTOMER_DETAIL_SUCCESS,
   CUSTOMER_DETAIL_FAILURE,

   CUSTOMER_DETAIL_UPDATE_REQUEST,
   CUSTOMER_DETAIL_UPDATE_SUCCESS,
   CUSTOMER_DETAIL_UPDATE_FAILURE,

   DIRECT_PROVIDER_INSERT,
   DIRECT_PROVIDER_INSERT_SUCCESS,
   DIRECT_PROVIDER_INSERT_FAILURE,

   DIRECT_MASTER_DATA_VALUE_LIST,
   DIRECT_MASTER_DATA_VALUE_LIST_SUCCESS,
   DIRECT_MASTER_DATA_VALUE_LIST_FAILURE,

   DIRECT_STATES_LIST,
   DIRECT_STATES_LIST_SUCCESS,
   DIRECT_STATES_LIST_FAILURE,

   DIRECT_PROVIDER_OPTION_LIST,
   DIRECT_PROVIDER_OPTION_SUCCESS,
   DIRECT_PROVIDER_OPTION_FAILURE,

   DIRECT_ADD_MORE_LIST,
   DIRECT_REMOVE_ADD_MORE_LIST,

   DIRECT_PROVIDER_COUNTRY_LIST,
   DIRECT_PROVIDER_COUNTRY_SUCCESS,
   DIRECT_PROVIDER_COUNTRY_FAILURE,

   DIRECT_EXIST_UPDATE,
   DIRECT_EXIST_UPDATE_SUCCESS,
   DIRECT_EXIST_UPDATE_FAILURE,

   DIRECT_EXIST_SSN_UPDATE,
   DIRECT_EXIST_SSN_UPDATE_SUCCESS,
   DIRECT_EXIST_SSN_UPDATE_FAILURE,

   DIRECT_PROVIDER_SPL_PROCEDURE,
   DIRECT_PROVIDER_SPL_PROCEDURE_SUCCESS,
   DIRECT_PROVIDER_SPL_PROCEDURE_FAILURE,

   ///////////

   DIRECT_APPLICATION_OPTION_LIST,
   DIRECT_APPLICATION_OPTION_SUCCESS,
   DIRECT_APPLICATION_OPTION_FAILURE,

   DIRECT_APPLICATION_STATES_LIST,
   DIRECT_APPLICATION_STATES_LIST_SUCCESS,
   DIRECT_APPLICATION_STATES_LIST_FAILURE,

   DIRECT_APPLICATION_SUBMIT,
   DIRECT_APPLICATION_SUBMIT_SUCCESS,
   DIRECT_APPLICATION_SUBMIT_FAILURE,

   DIRECT_PROVIDER_NAME_EXIST_UPDATE,
   DIRECT_PROVIDER_NAME_EXIST_UPDATE_SUCCESS,
   DIRECT_PROVIDER_NAME_EXIST_UPDATE_FAILURE,

   DIRECT_DATA_ACCESS_REQUEST,
   DIRECT_DATA_ACCESS_REQUEST_SUCCESS,
   DIRECT_DATA_ACCESS_REQUEST_FAILURE,

   DIRECT_EXIST_UPDATE_CO,
   DIRECT_EXIST_UPDATE_SUCCESS_CO,
   DIRECT_EXIST_UPDATE_FAILURE_CO,

} from 'Actions/types';


export const getDataFromAccessRequest = (id) => (dispatch) => {
  
    dispatch({ type: DIRECT_DATA_ACCESS_REQUEST });
    customerRegisterService.getDataFromAccessRequest(id)
        .then((access_request) => {
          dispatch({ type: DIRECT_DATA_ACCESS_REQUEST_SUCCESS, payload:access_request});
        })
        .catch((error) => {
            dispatch({ type: DIRECT_DATA_ACCESS_REQUEST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectSubmitApplicationProvider = (data) => (dispatch) => {
    dispatch({ type: DIRECT_APPLICATION_SUBMIT });
    customerRegisterService.submitApplicationProvider(data)
        .then((submit) => {
            dispatch({ type: DIRECT_APPLICATION_SUBMIT_SUCCESS, payload:submit});
            if(submit.applicationStatus==0){
                NotificationManager.error(submit.message);
            }else{
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: DIRECT_APPLICATION_SUBMIT_FAILURE });
            NotificationManager.error(error);
        });
}

export const DirectGetAppStates = (id,idx,Cidx) => (dispatch) => {
  
    dispatch({ type: DIRECT_APPLICATION_STATES_LIST });
    customerRegisterService.appGetStates(id)
        .then((state) => {
            state.Cidx = Cidx;
            state.idx=idx
          dispatch({ type: DIRECT_APPLICATION_STATES_LIST_SUCCESS, payload:state});
        })
        .catch((error) => {
            dispatch({ type: DIRECT_APPLICATION_STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectApplicationOption = (access_request_id) => (dispatch) => {
    dispatch({ type: DIRECT_APPLICATION_OPTION_LIST });
    customerRegisterService.applicationOption(access_request_id)
        .then((option) => {
            dispatch(DirectGetAppStates(option.countries[0].id, 0));
            dispatch(DirectGetAppStates(option.countries[0].id));
            dispatch(DirectGetAppStates(option.countries[0].id, 0, 1));
          dispatch({ type: DIRECT_APPLICATION_OPTION_SUCCESS, payload:option});
        })
        .catch((error) => {
            dispatch({ type: DIRECT_APPLICATION_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

///////////////////////
//////////////////////

export const DirectCheckSsnExist = (value, md_id) => (dispatch) => {
    dispatch({ type: DIRECT_EXIST_SSN_UPDATE });
    customerRegisterService.checkSsnExist(value, md_id)
        .then((ssn) => {
            ssn.email = 0;
            dispatch({ type: DIRECT_EXIST_SSN_UPDATE_SUCCESS, payload: ssn });
        })
        .catch((error) => {
            dispatch({ type: DIRECT_EXIST_SSN_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectCheckUsernameExist = (value, md_id) => (dispatch) => {
    dispatch({ type: DIRECT_EXIST_UPDATE });
    customerRegisterService.checkUsernameExist(value, md_id)
        .then((user) => {
            user.email = 0;
            dispatch({ type: DIRECT_EXIST_UPDATE_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: DIRECT_EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectcheckUsernameExistCo = (value, md_id) => (dispatch) => {
    dispatch({ type: DIRECT_EXIST_UPDATE_CO });
    customerRegisterService.DirectcheckUsernameExistCo(value, md_id)
        .then((user) => {
            user.email = 0;
            dispatch({ type: DIRECT_EXIST_UPDATE_SUCCESS_CO, payload: user });
        })
        .catch((error) => {
            dispatch({ type: DIRECT_EXIST_UPDATE_FAILURE_CO });
            NotificationManager.error(error.message);
        });
}



export const DirectCheckProviderNameExist = (value, md_id) => (dispatch) => {
    dispatch({ type: DIRECT_PROVIDER_NAME_EXIST_UPDATE });
    customerRegisterService.checkProviderNameExist(value, md_id)
        .then((user) => {
            user.email = 0;
            dispatch({ type: DIRECT_PROVIDER_NAME_EXIST_UPDATE_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: DIRECT_PROVIDER_NAME_EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectGetStates = (id,idx) => (dispatch) => {
  
    dispatch({ type: DIRECT_STATES_LIST });
    customerRegisterService.getStates(id)
        .then((state) => {
            state.idx=idx
          dispatch({ type: DIRECT_STATES_LIST_SUCCESS, payload:state});
        })
        .catch((error) => {
            dispatch({ type: DIRECT_STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectInsertProvider = (formData, history) => (dispatch) => {
    dispatch({type: DIRECT_PROVIDER_INSERT});
        customerRegisterService.insertProvider(formData)
        .then((ProviderInsert) => {
            NotificationManager.success('Profile created successfully!');
            dispatch({ type: DIRECT_PROVIDER_INSERT_SUCCESS, payload:ProviderInsert});
            
        })
        .catch((error) => {
            dispatch({ type: DIRECT_PROVIDER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectGetSpclProcedure = (id, idx) => (dispatch) => {
    
    //dispatch({ type: DIRECT_PROVIDER_SPL_PROCEDURE });
    customerRegisterService.getSpclProcedure(id)
        .then((procedure) => {
            procedure.idx=idx
            dispatch({ type: DIRECT_PROVIDER_SPL_PROCEDURE_SUCCESS, payload:procedure});
        })
        .catch((error) => {
            dispatch({ type: DIRECT_PROVIDER_SPL_PROCEDURE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectGetProviderOption = (chk,access_request_id) => (dispatch) => {
    
    dispatch({ type: DIRECT_PROVIDER_OPTION_LIST });
    customerRegisterService.getProviderOption(access_request_id)
        .then((option) => {
            option.chk = chk;
            dispatch({ type: DIRECT_PROVIDER_OPTION_SUCCESS, payload:option});
            
        })
        .catch((error) => {
            dispatch({ type: DIRECT_PROVIDER_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectGetProviderStates = (id,idx,st, chk) => (dispatch) => {
    
    dispatch({ type: DIRECT_STATES_LIST });
    customerRegisterService.getProviderStates(id)
        .then((state) => {
            state.idx=idx
            state.st=st
            dispatch({ type: DIRECT_STATES_LIST_SUCCESS, payload:state});
            

            if(chk == 1) {
                dispatch(DirectGetProviderStates(id, idx, 0))
            }

        })
        .catch((error) => {
            dispatch({ type: DIRECT_STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const DirectProviderCountry = (formData, history) => (dispatch) => {

    dispatch({type: DIRECT_PROVIDER_COUNTRY_LIST});
    customerRegisterService.getProviderCountry(formData)
        .then((providerCountry) => {
            dispatch({type: DIRECT_PROVIDER_COUNTRY_SUCCESS, payload:providerCountry.result});
            
            dispatch(DirectGetProviderStates(providerCountry.result[0].id, 0, 1));
            dispatch(DirectGetProviderStates(providerCountry.result[0].id, 0, 0));
        })
        .catch((error) => {
            dispatch({type: DIRECT_PROVIDER_COUNTRY_FAILURE});
            NotificationManager.error(error.message);
        })

}

export const DirectProviderTypeMasterDataValueList = (formData, history) => (dispatch) => {
    
    dispatch({ type: DIRECT_MASTER_DATA_VALUE_LIST });
    customerRegisterService.getAllProviderTypeMasterDataValue(formData)
        .then((providerType) => {
            dispatch({ type: DIRECT_MASTER_DATA_VALUE_LIST_SUCCESS, payload:providerType.result});
            
        })
        .catch((error) => {
            dispatch({ type: DIRECT_MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const updateCustomerFlag = (formData, history) => (dispatch) => {
    dispatch({ type: CUSTOMER_DETAIL_UPDATE_REQUEST });
    customerRegisterService.updateCustomerFlag(formData)
        .then((customer_update) => {
            dispatch({ type: CUSTOMER_DETAIL_UPDATE_SUCCESS, payload: customer_update });
            NotificationManager.success('Email Verified');
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DETAIL_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/**
 * Redux Action To Signup User In Firebase
 */
export const customerDetailVerify = (id) => (dispatch) => {
   dispatch({ type: CUSTOMER_DETAIL_REQUEST, });
    customerRegisterService.customerDetailVerify(id)
        .then((verify_detail) => {
         dispatch({ type: CUSTOMER_DETAIL_SUCCESS, payload:verify_detail});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DETAIL_FAILURE });
            NotificationManager.error(error);
        });
}

/**
 * Redux Action To Signup User In Firebase
 */
export const signUpCustomerInApp = (formData, history) => (dispatch) => {

   dispatch({type: CUSTOMER_REGISTER_REQUEST});
        customerRegisterService.signUpCustomerInApp(formData)
        .then((customer_register) => {
            dispatch({ type: CUSTOMER_REGISTER_SUCCESS, payload:customer_register});
            NotificationManager.success('Please verify your email');
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_REGISTER_FAILURE });
            NotificationManager.error(error.message);
        });
}


export const DirectAddMoreProvider = () => (dispatch) => {
    dispatch({ type: DIRECT_ADD_MORE_LIST });
}
/*
* Title :- removeAddMore
* Descrption :- this function use for remove old value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const DirectRemoveAddMoreProvider = (idx) => (dispatch) => {
    dispatch({ type: DIRECT_REMOVE_ADD_MORE_LIST, payload:idx });
}

