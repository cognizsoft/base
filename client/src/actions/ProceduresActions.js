import { NotificationManager } from 'react-notifications';
import { proceduresService } from '../apifile';
import {
    PROCEDURES_LIST,
    PROCEDURES_LIST_SUCCESS,
    PROCEDURES_LIST_FAILURE,
    PROCEDURES_INSERT,
    PROCEDURES_INSERT_SUCCESS,
    PROCEDURES_INSERT_FAILURE,
    PROCEDURES_UPDATE,
    PROCEDURES_UPDATE_SUCCESS,
    PROCEDURES_UPDATE_FAILURE,

} from 'Actions/types';

/*
* Title :- updateProcedures
* Descrption :- this function use for update procedures record
* Author :- CognizSoft & Ramesh Kumar
* Date :- June 26, 2019
*/

export const updateProcedures = (formData) => (dispatch) => {
    dispatch({ type: PROCEDURES_UPDATE });
    proceduresService.updateProcedures(formData)
        .then((data) => {
            dispatch({ type: PROCEDURES_UPDATE_SUCCESS, payload: data.result });
            NotificationManager.success('Procedures Updated!');
        })
        .catch((error) => {
            dispatch({ type: PROCEDURES_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertProcedures
* Descrption :- this function use for insert procedure
* Author :- Hp & Aman 
* Date :- June 26, 2019
*/

export const insertProcedures = (formData) => (dispatch) => {
    dispatch({ type: PROCEDURES_INSERT });
    proceduresService.insertProcedures(formData)
        .then((procedure) => {
            console.log(procedure)
            NotificationManager.success('Record added successfully!');
            dispatch({ type: PROCEDURES_INSERT_SUCCESS, payload: procedure });
        })
        .catch((error) => {
            dispatch({ type: PROCEDURES_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- proceduresList
* Descrption :- this function use procedures list
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 26, 2019
*/

export const proceduresList = () => (dispatch) => {
    dispatch({ type: PROCEDURES_LIST });
    proceduresService.proceduresList()
        .then((data) => {
        	dispatch({ type: PROCEDURES_LIST_SUCCESS, payload:data});
        })
        .catch((error) => {
            dispatch({ type: PROCEDURES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}



