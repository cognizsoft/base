import { NotificationManager } from 'react-notifications';
import { systemModuleService } from '../apifile';
import {
    SYSTEM_MODULE_LIST,
    SYSTEM_MODULE_LIST_SUCCESS,
    SYSTEM_MODULE_LIST_FAILURE,
    SYSTEM_MODULE_INSERT,
    SYSTEM_MODULE_INSERT_SUCCESS,
    SYSTEM_MODULE_INSERT_FAILURE,
    SYSTEM_MODULE_UPDATE,
    SYSTEM_MODULE_UPDATE_SUCCESS,
    SYSTEM_MODULE_UPDATE_FAILURE,
    SYSTEM_MODULE_DELETE,
    SYSTEM_MODULE_DELETE_SUCCESS,
    SYSTEM_MODULE_DELETE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE
} from 'Actions/types';

/*
* Title :- checkSystemModuleExist
* Descrption :- this function use for check System Module exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkSystemModuleExist = (value, sys_mod_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    systemModuleService.checkSystemModuleExist(value, sys_mod_id)
        .then((systemModule) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: systemModule});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllSystemModule
* Descrption :- this function use for get question list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllSystemModule = () => (dispatch) => {
    dispatch({ type: SYSTEM_MODULE_LIST });
    systemModuleService.getAllSystemModule()
        .then((systemModule) => {
            dispatch({ type: SYSTEM_MODULE_LIST_SUCCESS, payload: systemModule.result });
        })
        .catch((error) => {
            dispatch({ type: SYSTEM_MODULE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertSystemModule
* Descrption :- this function use for insert question information
* Author :- Hp & Aman 
* Date :- 24 April 2019
*/

export const insertSystemModule = (formData) => (dispatch) => {
    dispatch({ type: SYSTEM_MODULE_INSERT });
    systemModuleService.insertSystemModule(formData)
        .then((systemModule) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: SYSTEM_MODULE_INSERT_SUCCESS, payload: systemModule });
        })
        .catch((error) => {
            dispatch({ type: SYSTEM_MODULE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateSystemModule
* Descrption :- this function use for update question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateSystemModule = (formData) => (dispatch) => {
    dispatch({ type: SYSTEM_MODULE_UPDATE });
    systemModuleService.updateSystemModule(formData)
        .then((state) => {
            dispatch({ type: SYSTEM_MODULE_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('System Module Updated!');
        })
        .catch((error) => {
            dispatch({ type: SYSTEM_MODULE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

