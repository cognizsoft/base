import { NotificationManager } from 'react-notifications';
import { careCouldService } from '../apifile';
import {
    TOKEN_REQUEST,
    TOKEN_REQUEST_SUCCESS,
    TOKEN_REQUEST_FAILURE,
    SEARCH_APPLICATION_REQUEST,
    SEARCH_APPLICATION_SUCCESS,
    SEARCH_APPLICATION_FAILURE,
    VIEW_SEARCH_PATIENT_REQUEST,
    VIEW_SEARCH_PATIENT_SUCCESS,
    VIEW_SEARCH_PATIENT_FAILURE,
    CARE_CLOUD_REQUEST,
    CARE_CLOUD_SUCCESS,
    CARE_CLOUD_FAILURE,
    MFS_DETAILS_REQUEST,
    MFS_DETAILS_SUCCESS,
    MFS_DETAILS_FAILURE,
} from 'Actions/types';



/*
* Title :- MFSDetails
* Descrption :- this function use for save carecould details
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const MFSDetails = () => (dispatch) => {
    
    dispatch({type: MFS_DETAILS_REQUEST});
    careCouldService.MFSDetails()
        .then((result) => {
            dispatch({ type: MFS_DETAILS_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: MFS_DETAILS_FAILURE });
            NotificationManager.error(error);
        });
}




/*
* Title :- getCareCouldDetails
* Descrption :- this function use for save carecould details
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const getCareCouldDetails = () => (dispatch) => {
    
    dispatch({type: CARE_CLOUD_REQUEST});
    careCouldService.getCareCouldDetails()
        .then((result) => {
            dispatch({ type: CARE_CLOUD_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: CARE_CLOUD_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- careCouldDetails
* Descrption :- this function use for save carecould details
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const careCouldDetails = (data) => (dispatch) => {
    
    dispatch({type: CARE_CLOUD_REQUEST});
    careCouldService.careCouldDetails(data)
        .then((result) => {
            dispatch({ type: CARE_CLOUD_SUCCESS, payload:result});
            NotificationManager.success(result.message);
        })
        .catch((error) => {
            dispatch({ type: CARE_CLOUD_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- viewCareCouldPatient
* Descrption :- this function use for create careCould token
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const viewCareCouldPatient = (id) => (dispatch) => {
    
    dispatch({type: VIEW_SEARCH_PATIENT_REQUEST});
    careCouldService.viewCareCouldPatient(id)
        .then((careView) => {
            dispatch({ type: VIEW_SEARCH_PATIENT_SUCCESS, payload:careView});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: VIEW_SEARCH_PATIENT_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- searchThirdParty
* Descrption :- this function use for create careCould token
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const searchThirdParty = (data) => (dispatch) => {
    dispatch({type: SEARCH_APPLICATION_REQUEST});
    careCouldService.searchThirdParty(data)
        .then((careData) => {
            dispatch({ type: SEARCH_APPLICATION_SUCCESS, payload:careData});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: SEARCH_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- requestToken
* Descrption :- this function use for create careCould token
* Author :- Cogniz software so & Ramesh 
* Date :- Aug 30, 2019
*/

export const requestToken = (code,url,refreshToken) => (dispatch) => {
    dispatch({type: TOKEN_REQUEST});
    careCouldService.requestToken(code,url,refreshToken)
        .then((appExpireDateUpdate) => {
            localStorage.setItem("applicationToken", JSON.stringify(appExpireDateUpdate.body));
            dispatch({ type: TOKEN_REQUEST_SUCCESS, payload:appExpireDateUpdate.body});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: TOKEN_REQUEST_FAILURE });
            NotificationManager.error(error);
        });
}




