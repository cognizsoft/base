import { NotificationManager } from 'react-notifications';
import { cityService } from '../apifile';
import {
    CITY_LIST,
    CITY_LIST_SUCCESS,
    CITY_LIST_FAILURE,
    CITY_INSERT,
    CITY_INSERT_SUCCESS,
    CITY_INSERT_FAILURE,
    CITY_UPDATE,
    CITY_UPDATE_SUCCESS,
    CITY_UPDATE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,

    CITY_SELECTED_COUNTRY_STATE,
    CITY_SELECTED_COUNTRY_STATE_SUCCESS,
    CITY_SELECTED_COUNTRY_STATE_FAILURE,
} from 'Actions/types';


/*
* Title :- selectedCountryState
* Descrption :- this function use for check state name exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const selectedCountryState = (value) => (dispatch) => {
    dispatch({ type: CITY_SELECTED_COUNTRY_STATE });
    cityService.selectedCountryState(value)
        .then((country_state) => {
            //user.abbreviation=0;
            dispatch({ type: CITY_SELECTED_COUNTRY_STATE_SUCCESS, payload: country_state});
        })
        .catch((error) => {
            dispatch({ type: CITY_SELECTED_COUNTRY_STATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkNameExist
* Descrption :- this function use for check state name exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkCityExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    cityService.checkCityExist(value, md_id)
        .then((city) => {
            //user.abbreviation=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: city});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllUsers
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/
export const getAllCity = () => (dispatch) => {
    dispatch({ type: CITY_LIST });
    cityService.getAllCity()
        .then((city) => {
            dispatch({ type: CITY_LIST_SUCCESS, payload: city });
        })
        .catch((error) => {
            dispatch({ type: CITY_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertUser
* Descrption :- this function use for insert master
* Author :- Hp & Aman 
* Date :- 04 April 2019
*/

export const insertCity = (formData) => (dispatch) => {
    dispatch({ type: CITY_INSERT });
    cityService.insertCity(formData)
        .then((city) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: CITY_INSERT_SUCCESS, payload: city });
        })
        .catch((error) => {
            dispatch({ type: CITY_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateCITY
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const updateCity = (formData) => (dispatch) => {
    dispatch({ type: CITY_UPDATE });
    cityService.updateCity(formData)
        .then((city) => {
            dispatch({ type: CITY_UPDATE_SUCCESS, payload: city.result });
            NotificationManager.success('Record Updated!');
        })
        .catch((error) => {
            dispatch({ type: CITY_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}