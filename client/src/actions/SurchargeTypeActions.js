import { NotificationManager } from 'react-notifications';
import { surchargeTypeService } from '../apifile';
import {
    SURCHARGE_TYPE_LIST,
    SURCHARGE_TYPE_LIST_SUCCESS,
    SURCHARGE_TYPE_LIST_FAILURE,
    SURCHARGE_TYPE_INSERT,
    SURCHARGE_TYPE_INSERT_SUCCESS,
    SURCHARGE_TYPE_INSERT_FAILURE,
    SURCHARGE_TYPE_UPDATE,
    SURCHARGE_TYPE_UPDATE_SUCCESS,
    SURCHARGE_TYPE_UPDATE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE
} from 'Actions/types';



/*
* Title :- checkScorThresholdExist
* Descrption :- this function use for check System Module exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkSurchargeTypeExist = (name, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    surchargeTypeService.checkSurchargeTypeExist(name, id)
        .then((surchargeType) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: surchargeType});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllSurchargeType
* Descrption :- this function use for get state list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 22 March 2019
*/
export const getAllSurchargeType = () => (dispatch) => {
    dispatch({ type: SURCHARGE_TYPE_LIST });
    surchargeTypeService.getAllSurchargeType()
        .then((user) => {
            dispatch({ type: SURCHARGE_TYPE_LIST_SUCCESS, payload: user.result });
        })
        .catch((error) => {
            dispatch({ type: SURCHARGE_TYPE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- insertState
* Descrption :- this function use for insert state information
* Author :- Hp & Aman 
* Date :- 22 April 2019
*/

export const insertSurchargeType = (formData) => (dispatch) => {
    dispatch({ type: SURCHARGE_TYPE_INSERT });
    surchargeTypeService.insertSurchargeType(formData)
        .then((user) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: SURCHARGE_TYPE_INSERT_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: SURCHARGE_TYPE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateState
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const updateSurchargeType = (formData) => (dispatch) => {
    dispatch({ type: SURCHARGE_TYPE_UPDATE });
    surchargeTypeService.updateSurchargeType(formData)
        .then((state) => {
            dispatch({ type: SURCHARGE_TYPE_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('Surcharge Type Update');
        })
        .catch((error) => {
            dispatch({ type: SURCHARGE_TYPE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}