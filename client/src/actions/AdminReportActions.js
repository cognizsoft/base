import { NotificationManager } from 'react-notifications';
import { AdminReportService } from '../apifile';
import {
    ADMIN_REPORT_LIST,
    ADMIN_REPORT_LIST_SUCCESS,
    ADMIN_REPORT_LIST_FAILURE,
    REPORT_DOWNLOAD_LIST_REQUEST,
    REPORT_DOWNLOAD_LIST_SUCCESS,
    REPORT_DOWNLOAD_LIST_FAILURE,
    PROVIDER_INVOICE_REPORT_LIST,
    PROVIDER_INVOICE_REPORT_LIST_SUCCESS,
    PROVIDER_INVOICE_REPORT_LIST_FAILURE,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,
    PRINT_INVOICE_REQUEST,
    PRINT_INVOICE_SUCCESS,
    PRINT_INVOICE_FAILURE,

    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_REQUEST,
    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_SUCCESS,
    PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_FAILURE,

    PROCEDURE_DATE_REQUEST,
    PROCEDURE_DATE_SUCCESS,
    PROCEDURE_DATE_FAILURE,
    PROCEDURE_EMAIL_REQUEST,
    PROCEDURE_EMAIL_SUCCESS,
    PROCEDURE_EMAIL_FAILURE,

    REMINDER_EMAIL_REQUEST,
    REMINDER_EMAIL_SUCCESS,
    REMINDER_EMAIL_FAILURE,

    ADMIN_ACC_RECEIVABLES_REPORT_REQUEST,
    ADMIN_ACC_RECEIVABLES_REPORT_SUCCESS,
    ADMIN_ACC_RECEIVABLES_REPORT_FAILURE,

    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_REQUEST,
    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_SUCCESS,
    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_FAILURE,

    SUPPORT_REPORT_REQUEST,
    SUPPORT_REPORT_SUCCESS,
    SUPPORT_REPORT_FAILURE,

    SUPPORT_REPORT_DOWN_REQUEST,
    SUPPORT_REPORT_DOWN_SUCCESS,
    SUPPORT_REPORT_DOWN_FAILURE,

    CUSTOMER_SUPPORT_REPORT_REQUEST,
    CUSTOMER_SUPPORT_REPORT_SUCCESS,
    CUSTOMER_SUPPORT_REPORT_FAILURE,

    CUSTOMER_SUPPORT_REPORT_DOWN_REQUEST,
    CUSTOMER_SUPPORT_REPORT_DOWN_SUCCESS,
    CUSTOMER_SUPPORT_REPORT_DOWN_FAILURE,

    PROVIDER_PAYABLE_REQUEST,
    PROVIDER_PAYABLE_SUCCESS,
    PROVIDER_PAYABLE_FAILURE,

    CREDIT_CHARGES_REQUEST,
    CREDIT_CHARGES_SUCCESS,
    CREDIT_CHARGES_FAILURE,
    
} from 'Actions/types';


/*
* Title :- customerCreditCharge
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- jULY 24, 2020
*/

export const customerCreditChargeDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerCreditChargeDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerCreditChargeDownload
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- jULY 24, 2020
*/

export const customerCreditChargeDownload = (formData) => (dispatch) => {
    console.log('aaaaaaaaaaaaaa')
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerCreditChargeDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerCreditChargeFilter
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 24, 2020
*/

export const customerCreditChargeFilter = (data) => (dispatch) => {
    dispatch({ type: CREDIT_CHARGES_REQUEST });
    AdminReportService.customerCreditChargeFilter(data)
        .then((invoice) => {
            dispatch({ type: CREDIT_CHARGES_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CREDIT_CHARGES_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- providerAccountPayableDownloadXLS
* Descrption :- this function use for download provider account payable reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 30, 2020
*/

export const providerAccountPayableDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerAccountPayableDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- providerAccountPayableDownload
* Descrption :- this function use for download provider account payable reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 30, 2020
*/

export const providerAccountPayableDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerAccountPayableDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerAccountPayableFilter
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 30, 2020
*/

export const providerAccountPayableFilter = (data) => (dispatch) => {
    dispatch({ type: PROVIDER_PAYABLE_REQUEST });
    AdminReportService.providerAccountPayableFilter(data)
        .then((invoice) => {
            dispatch({ type: PROVIDER_PAYABLE_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_PAYABLE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerSupportReportDownloadXLS
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const customerSupportReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_REQUEST });
    AdminReportService.customerSupportReportDownloadXLS(formData)
        .then((downloadList) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerSupportReportDownload
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const customerSupportReportDownload = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_REQUEST });
    AdminReportService.customerSupportReportDownload(formData)
        .then((downloadList) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- customerSupportReportFilter
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const customerSupportReportFilter = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_SUPPORT_REPORT_REQUEST });
    AdminReportService.customerSupportReportFilter(formData)
        .then((filterList) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_SUCCESS, payload:filterList});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- customerSupportReport
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const customerSupportReport = (formData, history) => (dispatch) => {
    dispatch({ type: CUSTOMER_SUPPORT_REPORT_REQUEST });
    AdminReportService.customerSupportReport(formData)
        .then((reportList) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_SUCCESS, payload:reportList});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_SUPPORT_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- supportReportDownloadXLS
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const supportReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: SUPPORT_REPORT_DOWN_REQUEST });
    AdminReportService.supportReportDownloadXLS(formData)
        .then((downloadList) => {
            dispatch({ type: SUPPORT_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: SUPPORT_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- supportReportDownload
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const supportReportDownload = (formData) => (dispatch) => {
    dispatch({ type: SUPPORT_REPORT_DOWN_REQUEST });
    AdminReportService.supportReportDownload(formData)
        .then((downloadList) => {
            dispatch({ type: SUPPORT_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: SUPPORT_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- supportReportFilter
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const supportReportFilter = (formData) => (dispatch) => {
    dispatch({ type: SUPPORT_REPORT_REQUEST });
    AdminReportService.supportReportFilter(formData)
        .then((filterList) => {
            dispatch({ type: SUPPORT_REPORT_SUCCESS, payload:filterList});
        })
        .catch((error) => {
            dispatch({ type: SUPPORT_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}
/*
* Title :- supportReport
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const supportReport = (formData, history) => (dispatch) => {
    dispatch({ type: SUPPORT_REPORT_REQUEST });
    AdminReportService.supportReport(formData)
        .then((reportList) => {
            dispatch({ type: SUPPORT_REPORT_SUCCESS, payload:reportList});
        })
        .catch((error) => {
            dispatch({ type: SUPPORT_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- accountReceivablesReportDownloadXLS
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const accountReceivablesReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_REQUEST });
    AdminReportService.accountReceivablesReportDownloadXLS(formData)
        .then((downloadList) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- accountReceivablesReportDownload
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const accountReceivablesReportDownload = (formData) => (dispatch) => {
    dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_REQUEST });
    AdminReportService.accountReceivablesReportDownload(formData)
        .then((downloadList) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_DOWN_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- accountReceivablesReportFilter
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const accountReceivablesReportFilter = (formData) => (dispatch) => {
    dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_REQUEST });
    AdminReportService.accountReceivablesReportFilter(formData)
        .then((filterList) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_SUCCESS, payload:filterList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- accountReceivablesReport
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const accountReceivablesReport = (formData, history) => (dispatch) => {
    dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_REQUEST });
    AdminReportService.accountReceivablesReport(formData)
        .then((reportList) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_SUCCESS, payload:reportList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_ACC_RECEIVABLES_REPORT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerReminderEmail = (formData) => (dispatch) => {
    dispatch({ type: REMINDER_EMAIL_REQUEST });
    AdminReportService.customerReminderEmail(formData)
        .then((fullReportList) => {
            dispatch({ type: REMINDER_EMAIL_SUCCESS});
            NotificationManager.success(fullReportList.message);
        })
        .catch((error) => {
            dispatch({ type: REMINDER_EMAIL_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerProcedureSingleReportEmail
* Descrption :- this function use for EMAIL Procedure Date noticpation
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

export const customerProcedureSingleReportEmail = (id) => (dispatch) => {
    dispatch({ type: PROCEDURE_EMAIL_REQUEST });
    AdminReportService.customerProcedureSingleReportEmail(id)
        .then((fullReportList) => {
            dispatch({ type: PROCEDURE_EMAIL_SUCCESS});
            NotificationManager.success(fullReportList.message);
        })
        .catch((error) => {
            dispatch({ type: PROCEDURE_EMAIL_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerProcedureReportEmail
* Descrption :- this function use for EMAIL Procedure Date noticpation
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

export const customerProcedureReportEmail = (formData) => (dispatch) => {
    dispatch({ type: PROCEDURE_EMAIL_REQUEST });
    AdminReportService.customerProcedureReportEmail(formData)
        .then((fullReportList) => {
            dispatch({ type: PROCEDURE_EMAIL_SUCCESS});
            NotificationManager.success(fullReportList.message);
        })
        .catch((error) => {
            dispatch({ type: PROCEDURE_EMAIL_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- customerProcedureReportDownloadXLS
* Descrption :- this function use for download Procedure Date Report pdf
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

export const customerProcedureReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerProcedureReportDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- customerProcedureReportDownload
* Descrption :- this function use for download Procedure Date Report pdf
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

export const customerProcedureReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerProcedureReportDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- Procedure Date Report
* Descrption :- this function use for get Procedure Date Report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 6, 2020
*/

export const customerProcedureReportFilter = (data) => (dispatch) => {
    dispatch({ type: PROCEDURE_DATE_REQUEST });
    AdminReportService.customerProcedureReportFilter(data)
        .then((invoice) => {
            dispatch({ type: PROCEDURE_DATE_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROCEDURE_DATE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report XLS
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/

export const customerWithdrawalReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerWithdrawalReportDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report PDF
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/

export const customerWithdrawalReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerWithdrawalReportDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerWithdrawalReportFilter
* Descrption :- this function use for get customer withdrawal report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2020
*/

export const customerWithdrawalReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerWithdrawalReportFilter(data)
        .then((invoice) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const customerfullInvoiceDownload = (data) => (dispatch) => {
    dispatch({ type: PRINT_INVOICE_REQUEST });
    AdminReportService.customerfullInvoiceDownload(data)
        .then((invoice) => {
        	dispatch({ type: PRINT_INVOICE_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PRINT_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerInvoiceReportDownloadXLS
* Descrption :- this function use for download customr weekly & monthly invoice xlsx reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerInvoiceReportDownload
* Descrption :- this function use for download customer weekly & monthly invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.customerInvoiceReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- customerInvoiceReportFilter
* Descrption :- this function use for get customer invoice filter details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerInvoiceReportFilter(data)
        .then((invoice) => {
        	dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerWeekMonthInvoiceReport
* Descrption :- this function use for get customer week month reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

export const customerWeekMonthInvoiceReport = () => (dispatch) => {
    dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST });
    AdminReportService.customerWeekMonthInvoiceReport()
        .then((invoice) => {
        	dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- providerInvoiceReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.providerInvoiceReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerSingleInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const providerSingleInvoiceReportDownload = (invoice_id, provider_id) => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_REQUEST });
    AdminReportService.providerSingleInvoiceReportDownload(invoice_id, provider_id)
        .then((fullReportList) => {
            dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_SINGLE_REPORT_DOWNLOAD_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- providerInvoiceReportFilter
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/

export const providerInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_REPORT_LIST });
    AdminReportService.providerInvoiceReportFilter(data)
        .then((invoice) => {
        	dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerInvoiceReport
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/

export const providerInvoiceReport = (status_id) => (dispatch) => {
    dispatch({ type: PROVIDER_INVOICE_REPORT_LIST });
    AdminReportService.providerInvoiceReport(status_id)
        .then((invoice) => {
        	dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- loanReportDownloadXLS
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.loanReportDownloadXLS(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- loanReportDownload
* Descrption :- this function use for download loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportDownload = (formData) => (dispatch) => {
    dispatch({ type: REPORT_DOWNLOAD_LIST_REQUEST });
    AdminReportService.loanReportDownload(formData)
        .then((fullReportList) => {
        	dispatch({ type: REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- loanReportFilter
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 17, 2019
*/

export const loanReportFilter = (formData) => (dispatch) => {
    dispatch({ type: ADMIN_REPORT_LIST });
    AdminReportService.loanReportFilter(formData)
        .then((fullReportList) => {
        	dispatch({ type: ADMIN_REPORT_LIST_SUCCESS, payload:fullReportList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_REPORT_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- FinancialChargesList
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const fullReportList = (formData, history) => (dispatch) => {
    dispatch({ type: ADMIN_REPORT_LIST });
    AdminReportService.fullReportList(formData)
        .then((fullReportList) => {
        	dispatch({ type: ADMIN_REPORT_LIST_SUCCESS, payload:fullReportList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_REPORT_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


