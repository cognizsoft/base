import { NotificationManager } from 'react-notifications';
import { providerService } from '../apifile';
import {
    PROVIDER_LIST,
    PROVIDER_LIST_SUCCESS,
    PROVIDER_LIST_FAILURE,

    PROVIDER_DETAILS,
    PROVIDER_DETAILS_SUCCESS,
    PROVIDER_DETAILS_FAILURE,

    PROVIDER_UPDATE,
    PROVIDER_UPDATE_SUCCESS,
    PROVIDER_UPDATE_FAILURE,

    PROVIDER_DELETE,
    PROVIDER_DELETE_SUCCESS,
    PROVIDER_DELETE_FAILURE,

    PROVIDER_INSERT,
    PROVIDER_INSERT_SUCCESS,
    PROVIDER_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,

    REGION_LIST,
    REGION_LIST_SUCCESS,
    REGION_LIST_FAILURE,

    PROVIDER_OPTION_LIST,
    PROVIDER_OPTION_SUCCESS,
    PROVIDER_OPTION_FAILURE,

    ADD_MORE_LIST,
    REMOVE_ADD_MORE_LIST,

    PROVIDER_COUNTRY_LIST,
    PROVIDER_COUNTRY_SUCCESS,
    PROVIDER_COUNTRY_FAILURE,

    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,

    PROVIDER_SPL_PROCEDURE,
    PROVIDER_SPL_PROCEDURE_SUCCESS,
    PROVIDER_SPL_PROCEDURE_FAILURE,
    ADD_MORE_SPEC_LIST,

    PROVIDER_CITY_SUGGESTION,
    PROVIDER_CITY_SUGGESTION_SUCCESS,
    PROVIDER_CITY_SUGGESTION_FAILURE,

    PROVIDER_UPLOAD_DOCUMENTS_REQUEST,
    PROVIDER_UPLOAD_DOCUMENTS_SUCCESS,
    PROVIDER_UPLOAD_DOCUMENTS_FAILURE,

    PROVIDER_DOCUMENTS_REQUEST,
    PROVIDER_DOCUMENTS_SUCCESS,
    PROVIDER_DOCUMENTS_FAILURE,
} from 'Actions/types';

/*
* Title :- PROVIDERDocument
* Descrption :- this function use for get PROVIDER documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const providerDocument = (id) => (dispatch) => {
    dispatch({ type: PROVIDER_DOCUMENTS_REQUEST, });
    providerService.providerDocument(id)
        .then((all_doc) => {
            dispatch({ type: PROVIDER_DOCUMENTS_SUCCESS, payload:all_doc});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_DOCUMENTS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerUploadDocument
* Descrption :- this function use for admin upload provider document
* Author :- Cogniz software so & Aman 
* Date :- April 21, 2019
*/
export const providerUploadDocument = (datafrom) => (dispatch) => {
    dispatch({ type: PROVIDER_UPLOAD_DOCUMENTS_REQUEST, });
    providerService.providerUploadDocument(datafrom)
        .then((doc) => {
            dispatch(providerDocument(doc.provider_id));
            dispatch({ type: PROVIDER_UPLOAD_DOCUMENTS_SUCCESS, payload:doc});
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_UPLOAD_DOCUMENTS_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- citySuggestion
* Descrption :- this function use for check username exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const citySuggestion = (country_id, state_id) => (dispatch) => {
    dispatch({ type: PROVIDER_CITY_SUGGESTION });
    providerService.citySuggestion(country_id, state_id)
        .then((result) => {
            dispatch({ type: PROVIDER_CITY_SUGGESTION_SUCCESS, payload: result });
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_CITY_SUGGESTION_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- addMoreSpec
* Descrption :- this function use add new state value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const addMoreSpec = () => (dispatch) => {
    dispatch({ type: ADD_MORE_SPEC_LIST });
}
export const getSpclProcedure = (id, idx) => (dispatch) => {
    //console.log(id+"actionnnnnnnnnn")
    dispatch({ type: PROVIDER_SPL_PROCEDURE });
    providerService.getSpclProcedure(id)
        .then((procedure) => {
            procedure.idx=idx
            dispatch({ type: PROVIDER_SPL_PROCEDURE_SUCCESS, payload:procedure});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_SPL_PROCEDURE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- checkSsnExist
* Descrption :- this function use for check username exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkSsnExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    providerService.checkSsnExist(value, md_id)
        .then((ssn) => {
            ssn.email = 0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: ssn });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUser
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/


export const updateProvider = (id) => (dispatch) => {
    dispatch({ type: PROVIDER_UPDATE });
    providerService.updateProvider(id)
        .then((provider_update) => {
            dispatch({ type: PROVIDER_UPDATE_SUCCESS, payload: provider_update.result });
            NotificationManager.success('Provider Updated!');
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteProvider
* Descrption :- this function use for delete user
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const deleteProvider = (value) => (dispatch) => {
    dispatch({ type: PROVIDER_DELETE });
    providerService.deleteProvider(value)
        .then((provider) => {
            dispatch({ type: PROVIDER_DELETE_SUCCESS, payload: provider.provider_id });
            NotificationManager.success('Provider Deleted!');
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- Provider Details
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const ProviderDetails = (id) => (dispatch) => {
    dispatch({ type: PROVIDER_DETAILS });
    providerService.getProviderDetails(id)
        .then((provider_details) => {
            
            
            dispatch({ type: PROVIDER_DETAILS_SUCCESS, payload:provider_details});

        })
        .catch((error) => {
            dispatch({ type: PROVIDER_DETAILS_FAILURE });
            //NotificationManager.error(error.message);
            
        });
}

export const ProviderDetailsEdit = (id) => (dispatch) => {
    dispatch({ type: PROVIDER_DETAILS });
    providerService.getProviderDetails(id)
        .then((provider_details) => {
            var provider_loc_add_err = [];
            var provider_bank_add_err = [];
            var provider_spl_add_err = [];
            var provider_addi_dis_add_err = [];
            var provider_addi_fee_add_err = [];
            var provider_doc_add_err = [];
            //console.log('provider_details')
            //console.log(provider_details.provider_location)
            provider_details.provider_location && provider_details.provider_location.map(function(opt, idx) {
               
                dispatch(addMoreProvider());
                dispatch(getProviderStates(opt.billing_country_id, idx, 1));
                dispatch(getProviderStates(opt.physical_country_id, idx, 0));

                //dispatch(getProviderRegion(opt.billing_state_id, idx, 1));
                //dispatch(getProviderRegion(opt.physical_state_id, idx, 0));

                

                provider_loc_add_err = provider_loc_add_err.concat([{
                    physical_location_name: "",
                    physical_address1: "",
                    physical_country: "",
                    physical_state: "",
                    physical_region: "",
                    physical_city: "",
                    physical_zip_code: "",
                    physical_phone_no: "",     
                    primary_address_flag: "",     

                    billing_location_name: "",
                    billing_address1: "",
                    billing_city: "",
                    billing_zip_code: "",
                    billing_phone_no: "",
                    billing_state: "",
                    billing_country:"",
                    billing_region: "",
                }]);
            })

            provider_details.provider_bank && provider_details.provider_bank.map(function(opt, idx) {
               //console.log(opt.billing_country_id+"  "+opt.physical_country_id)
                dispatch(getProviderOption(1));
                provider_bank_add_err = provider_bank_add_err.concat([{
                    location_name: "",
                    bank_name: "",
                    bank_address: "",
                    rounting_no: "",
                    bank_accout: "",
                    name_on_account: "",
                    bank_account_type: "",
                }]);

            })

            provider_details.provider_specility && provider_details.provider_specility.map(function(opt, idx) {
               dispatch(getSpclProcedure(opt.specialization, idx));
                provider_spl_add_err = provider_spl_add_err.concat([{
                    spl_location_name: "",
                    specialization: "",
                    procedure: "",
                    doctor_f_name: "",
                    doctor_l_name: "",
                }]);

            })

            provider_details.provider_additional_discount && provider_details.provider_additional_discount.map(function(opt, idx) {
               
                provider_addi_dis_add_err = provider_addi_dis_add_err.concat([{
                    discount_type: "",
                    discount_rate: "",
                    redu_discount_rate: "",
                    loan_term_month: "",
                    discount_from_date: "",
                    discount_to_date: "",
                }]);

            })

            provider_details.provider_additional_fee && provider_details.provider_additional_fee.map(function(opt, idx) {

                opt.additional_fee_field = (opt.additional_fee_type_id && opt.additional_conv_fee) ? "" : 1
                provider_addi_fee_add_err = provider_addi_fee_add_err.concat([{
                    additional_fee_type: "",
                    additional_conv_fee: "",
                    additional_from_date: "",
                    additional_to_date: "",
                }]);
                return opt;
            })

            provider_details.provider_document && provider_details.provider_document.map(function(opt, idx) {
               
                provider_doc_add_err = provider_doc_add_err.concat([{
                    document_type: "",
                    document_name: "",
                    document_upload: "",
                }]);

            })

           
            provider_details.provider_loc_add_err = provider_loc_add_err;
            provider_details.provider_bank_add_err = provider_bank_add_err;
            provider_details.provider_spl_add_err = provider_spl_add_err;
            provider_details.provider_addi_dis_add_err = provider_addi_dis_add_err;
            provider_details.provider_addi_fee_add_err = provider_addi_fee_add_err;
            provider_details.provider_doc_add_err = provider_doc_add_err;

            dispatch({ type: PROVIDER_DETAILS_SUCCESS, payload:provider_details});

            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_DETAILS_FAILURE });
            //NotificationManager.error(error.message);
        });
}

/*
* Title :- Provider List
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const ProviderList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: PROVIDER_LIST });
    providerService.getAllProvider(formData)
        .then((Provider) => {
            dispatch({ type: PROVIDER_LIST_SUCCESS, payload:Provider.result});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- provider type master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const providerTypeMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    providerService.getAllProviderTypeMasterDataValue(formData)
        .then((providerType) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:providerType.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- provider country
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const providerCountry = (formData, history) => (dispatch) => {

    dispatch({type: PROVIDER_COUNTRY_LIST});
    providerService.getProviderCountry(formData)
        .then((providerCountry) => {
            dispatch({type: PROVIDER_COUNTRY_SUCCESS, payload:providerCountry.result});
            
            dispatch(getProviderStates(providerCountry.result[0].id, 0, 1));
            dispatch(getProviderStates(providerCountry.result[0].id, 0, 0));
        })
        .catch((error) => {
            dispatch({type: PROVIDER_COUNTRY_FAILURE});
            NotificationManager.error(error.message);
        })

}

/*
* Title :- provider states
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const getProviderStates = (id,idx,st, chk) => (dispatch) => {
    //console.log('call state')
    dispatch({ type: STATES_LIST });
    providerService.getProviderStates(id)
        .then((state) => {
            state.idx=idx
            state.st=st
            dispatch({ type: STATES_LIST_SUCCESS, payload:state});
            //console.log(userType.result)

            if(chk == 1) {
                dispatch(getProviderStates(id, idx, 0))
            }

        })
        .catch((error) => {
            dispatch({ type: STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- get region list
* Descrption :- this function use get region according to state
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/

export const getProviderRegion = (id,idx,rg) => (dispatch) => {
    dispatch({ type: REGION_LIST });
    providerService.getProviderRegion(id)
        .then((region) => {
            region.idx=idx
            region.rg=rg
            dispatch({ type: REGION_LIST_SUCCESS, payload:region});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: REGION_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const getProviderOption = (chk) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: PROVIDER_OPTION_LIST });
    providerService.getProviderOption()
        .then((option) => {
            option.chk = chk;
            dispatch({ type: PROVIDER_OPTION_SUCCESS, payload:option});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_OPTION_FAILURE });
            NotificationManager.error(error.message);
        });
}
    
/*
* Title :- insert provider
* Descrption :- this function use for insert Provider data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertProvider = (formData, history) => (dispatch) => {
    dispatch({type: PROVIDER_INSERT});
        providerService.insertProvider(formData)
        .then((ProviderInsert) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: PROVIDER_INSERT_SUCCESS, payload:ProviderInsert});
            
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- addMore
* Descrption :- this function use add new state value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const addMoreProvider = () => (dispatch) => {
    dispatch({ type: ADD_MORE_LIST });
}
/*
* Title :- removeAddMore
* Descrption :- this function use for remove old value in props
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
export const removeAddMoreProvider = (idx) => (dispatch) => {
    dispatch({ type: REMOVE_ADD_MORE_LIST, payload:idx });
}