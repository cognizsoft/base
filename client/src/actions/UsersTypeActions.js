import { NotificationManager } from 'react-notifications';
import { userService, userTypesService } from '../apifile';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    USER_TYPE_LIST,
    USER_TYPE_LIST_SUCCESS,
    USER_TYPE_LIST_FAILURE,

    USER_TYPE_UPDATE,
    USER_TYPE_UPDATE_SUCCESS,
    USER_TYPE_UPDATE_FAILURE,

    USER_TYPE_DELETE,
    USER_TYPE_DELETE_SUCCESS,
    USER_TYPE_DELETE_FAILURE,

    USER_TYPE_INSERT,
    USER_TYPE_INSERT_SUCCESS,
    USER_TYPE_INSERT_FAILURE,

    USER_TYPE_LAST_INSERT_ID,
    USER_TYPE_LAST_INSERT_ID_SUCCESS,
    USER_TYPE_LAST_INSERT_ID_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE
} from 'Actions/types';


/*
* Title :- userTypesService
* Descrption :- this function use for name exist or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- May, 05 2019
*/
export const checkUserTypeExist = (value,md_id) => (dispatch) => {
    dispatch({type: EXIST_UPDATE});
    userTypesService.checkUserTypeExist(value,md_id)
        .then((master) => {
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/
export const userTypeList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: USER_TYPE_LIST });
    userTypesService.getAll(formData)
        .then((userType) => {
        	dispatch({ type: USER_TYPE_LIST_SUCCESS, payload:userType.result});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_TYPE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateUserType = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: USER_TYPE_UPDATE});
        userTypesService.updateUserTypes(formData)
        .then((userTypeUpdate) => {
            dispatch({ type: USER_TYPE_UPDATE_SUCCESS, payload:userTypeUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_TYPE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteUserType = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: USER_TYPE_DELETE});
    userTypesService.deleteUserTypes(formData)
    .then((userTypeDelete) => {
        dispatch({ type: USER_TYPE_DELETE_SUCCESS, payload:userTypeDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: USER_TYPE_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertUserType = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: USER_TYPE_INSERT});
        userTypesService.insertUserTypes(formData)
        .then((userTypeInsert) => {
            dispatch({ type: USER_TYPE_INSERT_SUCCESS, payload:userTypeInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_TYPE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const lastInsertIdUserType = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    dispatch({ type: USER_TYPE_LAST_INSERT_ID });
    userTypesService.getLastInsertId(formData)
        .then((userTypeLastId) => {
            dispatch({ type: USER_TYPE_LAST_INSERT_ID_SUCCESS, payload:userTypeLastId.result[0].type_id});
            console.log(userTypeLastId.result[0].type_id)
        })
        .catch((error) => {
            dispatch({ type: USER_TYPE_LAST_INSERT_ID_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllUsers
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/

export const getAllUsersType = (history) => (dispatch) => {
    console.log('this list')
    /*dispatch({ type: LOGIN_USER });
    userService.login(formData)
        .then((user) => {
            //console.log(user)
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem("user", user);
            dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user') });
            history.push('/');
            NotificationManager.success('User Login Successfully!');
        })
        .catch((error) => {
            dispatch({ type: LOGIN_USER_FAILURE });
            NotificationManager.error(error.message);
        });*/
}