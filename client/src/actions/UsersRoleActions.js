import { NotificationManager } from 'react-notifications';
import { userService, userRolesService } from '../apifile';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    USER_ROLE_LIST,
    USER_ROLE_LIST_SUCCESS,
    USER_ROLE_LIST_FAILURE,

    USER_ROLE_UPDATE,
    USER_ROLE_UPDATE_SUCCESS,
    USER_ROLE_UPDATE_FAILURE,

    USER_ROLE_DELETE,
    USER_ROLE_DELETE_SUCCESS,
    USER_ROLE_DELETE_FAILURE,

    USER_ROLE_INSERT,
    USER_ROLE_INSERT_SUCCESS,
    USER_ROLE_INSERT_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,
    USER_ROLE_LAST_INSERT_ID,
    USER_ROLE_LAST_INSERT_ID_SUCCESS,
    USER_ROLE_LAST_INSERT_ID_FAILURE
} from 'Actions/types';


/*
* Title :- checkRoleExist
* Descrption :- this function use for update master item
* Author :- CognizSoft & Ramesh Kumar
* Date :- 11 April 2019
*/
export const checkRoleExist = (value,md_id,name) => (dispatch) => {
    dispatch({type: EXIST_UPDATE});
    userRolesService.checkRoleExist(value,md_id,name)
        .then((master) => {
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload:master});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const userRoleList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: USER_ROLE_LIST });
    userRolesService.getAll(formData)
        .then((userRole) => {
        	dispatch({ type: USER_ROLE_LIST_SUCCESS, payload:userRole});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_ROLE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateUserRole = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: USER_ROLE_UPDATE});
        userRolesService.updateUserRoles(formData)
        .then((userRoleUpdate) => {
            NotificationManager.success('User Role Updated!');
            dispatch({ type: USER_ROLE_UPDATE_SUCCESS, payload:userRoleUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_ROLE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteUserRole = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: USER_ROLE_DELETE});
    userRolesService.deleteUserRoles(formData)
    .then((userRoleDelete) => {
        dispatch({ type: USER_ROLE_DELETE_SUCCESS, payload:userRoleDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: USER_ROLE_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertUserRole = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: USER_ROLE_INSERT});
        userRolesService.insertUserRoles(formData)
        .then((userRoleInsert) => {
            NotificationManager.success('User Role Created!');
            dispatch({ type: USER_ROLE_INSERT_SUCCESS, payload:userRoleInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: USER_ROLE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}
