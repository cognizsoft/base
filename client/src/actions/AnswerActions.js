import { NotificationManager } from 'react-notifications';
import { answerService } from '../apifile';
import {
    ANSWER_LIST,
    ANSWER_LIST_SUCCESS,
    ANSWER_LIST_FAILURE,
    ANSWER_INSERT,
    ANSWER_INSERT_SUCCESS,
    ANSWER_INSERT_FAILURE,
    ANSWER_UPDATE,
    ANSWER_UPDATE_SUCCESS,
    ANSWER_UPDATE_FAILURE,
    ANSWER_DELETE,
    ANSWER_DELETE_SUCCESS,
    ANSWER_DELETE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,

    PROVIDER_ANSWER_LIST,
    PROVIDER_ANSWER_LIST_SUCCESS,
    PROVIDER_ANSWER_LIST_FAILURE,

    PROVIDER_ANSWER_INSERT,
    PROVIDER_ANSWER_INSERT_SUCCESS,
    PROVIDER_ANSWER_INSERT_FAILURE,

    PROVIDER_ANSWER_UPDATE,
    PROVIDER_ANSWER_UPDATE_SUCCESS,
    PROVIDER_ANSWER_UPDATE_FAILURE,

    PROVIDER_ANSWER_DELETE,
    PROVIDER_ANSWER_DELETE_SUCCESS,
    PROVIDER_ANSWER_DELETE_FAILURE,

    CUSTOMER_ANSWER_LIST,
    CUSTOMER_ANSWER_LIST_SUCCESS,
    CUSTOMER_ANSWER_LIST_FAILURE,

    CUSTOMER_ANSWER_INSERT,
    CUSTOMER_ANSWER_INSERT_SUCCESS,
    CUSTOMER_ANSWER_INSERT_FAILURE,

    CUSTOMER_ANSWER_UPDATE,
    CUSTOMER_ANSWER_UPDATE_SUCCESS,
    CUSTOMER_ANSWER_UPDATE_FAILURE,

    CUSTOMER_ANSWER_DELETE,
    CUSTOMER_ANSWER_DELETE_SUCCESS,
    CUSTOMER_ANSWER_DELETE_FAILURE,
} from 'Actions/types';

///////////////CUSTOMER///////////////


/*
* Title :- deleteProviderAnswer
* Descrption :- this function use for delete provider question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const deleteCustomerAnswer = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_ANSWER_DELETE });
    answerService.deleteCustomerAnswer(formData)
        .then((question) => {
            dispatch({ type: CUSTOMER_ANSWER_DELETE_SUCCESS, payload: question.id });
            NotificationManager.success('Answer Deleted!');
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_ANSWER_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateCustomerAnswer
* Descrption :- this function use for update Customer question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateCustomerAnswer = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_ANSWER_UPDATE });
    answerService.updateCustomerAnswer(formData)
        .then((state) => {
            dispatch({ type: CUSTOMER_ANSWER_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('Answer Updated!');
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_ANSWER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertCustomerAnswer
* Descrption :- this function use for insert Customer question information
* Author :- Cogniz software Solution
* Date :- 24 April 2019
*/

export const insertCustomerAnswer = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_ANSWER_INSERT });
    answerService.insertCustomerAnswer(formData)
        .then((question) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: CUSTOMER_ANSWER_INSERT_SUCCESS, payload: question });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_ANSWER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllCustomerAnswer
* Descrption :- this function use for get question list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllCustomerAnswer = () => (dispatch) => {
    dispatch({ type: CUSTOMER_ANSWER_LIST });
    answerService.getAllCustomerAnswer()
        .then((answer) => {
            dispatch({ type: CUSTOMER_ANSWER_LIST_SUCCESS, payload: answer });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_ANSWER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


//////////////PROVIDER////////////////

/*
* Title :- deleteProviderAnswer
* Descrption :- this function use for delete provider question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const deleteProviderAnswer = (formData) => (dispatch) => {
    dispatch({ type: PROVIDER_ANSWER_DELETE });
    answerService.deleteProviderAnswer(formData)
        .then((question) => {
            dispatch({ type: PROVIDER_ANSWER_DELETE_SUCCESS, payload: question.id });
            NotificationManager.success('Answer Deleted!');
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_ANSWER_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateProviderAnswer
* Descrption :- this function use for update provider question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateProviderAnswer = (formData) => (dispatch) => {
    dispatch({ type: PROVIDER_ANSWER_UPDATE });
    answerService.updateProviderAnswer(formData)
        .then((state) => {
            dispatch({ type: PROVIDER_ANSWER_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('Answer Updated!');
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_ANSWER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertProviderAnswer
* Descrption :- this function use for insert question information
* Author :- Cogniz software Solution
* Date :- 24 April 2019
*/

export const insertProviderAnswer = (formData) => (dispatch) => {
    dispatch({ type: PROVIDER_ANSWER_INSERT });
    answerService.insertProviderAnswer(formData)
        .then((question) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: PROVIDER_ANSWER_INSERT_SUCCESS, payload: question });
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_ANSWER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllProviderAnswer
* Descrption :- this function use for get question list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllProviderAnswer = () => (dispatch) => {
    dispatch({ type: PROVIDER_ANSWER_LIST });
    answerService.getAllProviderAnswer()
        .then((answer) => {
            dispatch({ type: PROVIDER_ANSWER_LIST_SUCCESS, payload: answer });
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_ANSWER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


//////////////ADMIN//////////////////

/*
* Title :- getAllAnswer
* Descrption :- this function use for get question list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllAnswer = () => (dispatch) => {
    dispatch({ type: ANSWER_LIST });
    answerService.getAllAnswer()
        .then((answer) => {
            dispatch({ type: ANSWER_LIST_SUCCESS, payload: answer });
        })
        .catch((error) => {
            dispatch({ type: ANSWER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- insertAnswer
* Descrption :- this function use for insert question information
* Author :- Cogniz software Solution
* Date :- 24 April 2019
*/

export const insertAnswer = (formData) => (dispatch) => {
    dispatch({ type: ANSWER_INSERT });
    answerService.insertAnswer(formData)
        .then((question) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: ANSWER_INSERT_SUCCESS, payload: question });
        })
        .catch((error) => {
            dispatch({ type: ANSWER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateAnswer
* Descrption :- this function use for update question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateAnswer = (formData) => (dispatch) => {
    dispatch({ type: ANSWER_UPDATE });
    answerService.updateAnswer(formData)
        .then((state) => {
            dispatch({ type: ANSWER_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('Answer Updated!');
        })
        .catch((error) => {
            dispatch({ type: ANSWER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- deleteAnswer
* Descrption :- this function use for update question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const deleteAnswer = (formData) => (dispatch) => {
    dispatch({ type: ANSWER_DELETE });
    answerService.deleteAnswer(formData)
        .then((question) => {
            NotificationManager.success('Answer Deleted!');
            dispatch({ type: ANSWER_DELETE_SUCCESS, payload: question.id });
        })
        .catch((error) => {
            dispatch({ type: ANSWER_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- checkSecurityAnswerExist
* Descrption :- this function use for check System Module exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkSecurityAnswerExist = (question_id, user_id, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    answerService.checkSecurityAnswerExist(question_id, user_id, id)
        .then((SecurityQuestion) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: SecurityQuestion});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}