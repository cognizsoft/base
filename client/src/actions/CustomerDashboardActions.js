import { NotificationManager } from 'react-notifications';
import { customerDashboardService, cosignerService } from '../apifile';
import {
    CUSTOMER_ACTIVE_PLAN,
    CUSTOMER_ACTIVE_PLAN_SUCCESS,
    CUSTOMER_ACTIVE_PLAN_FAILURE,

    CUSTOMER_INSERT_PAYMENT,
    CUSTOMER_INSERT_PAYMENT_SUCCESS,
    CUSTOMER_INSERT_PAYMENT_FAILURE,

    CUSTOMER_EDIT_PROFILE_DETAILS,
    CUSTOMER_EDIT_PROFILE_DETAILS_SUCCESS,
    CUSTOMER_EDIT_PROFILE_DETAILS_FAILURE,

    CUSTOMER_PROFILE_STATES_LIST,
    CUSTOMER_PROFILE_STATES_LIST_SUCCESS,
    CUSTOMER_PROFILE_STATES_LIST_FAILURE,

    CUSTOMER_PROFILE_ADD_MORE_LIST,

    CUSTOMER_PROFILE_UPDATE_REQUEST,
    CUSTOMER_PROFILE_UPDATE_SUCCESS,
    CUSTOMER_PROFILE_UPDATE_FAILURE,

    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE,

    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,

    CUSTOMER_APP_PLAN_DETAILS_PDF_DOWNLOAD_REQUEST,
    CUSTOMER_APP_PLAN_DETAILS_PDF_DOWNLOAD_SUCCESS,
    CUSTOMER_APP_PLAN_DETAILS_PDF_DOWNLOAD_FAILURE,
    
    CUSTOMER_BANK_VERIFY_ACCOUNT,
    CUSTOMER_BANK_VERIFY_ACCOUNT_SUCCESS,
    CUSTOMER_BANK_VERIFY_ACCOUNT_FAILURE,
} from 'Actions/types';

/*
* Title :- submitVerifyAmount
* Descrption :- this function use for verify bank account number
* Author :- Cogniz software so & Aman 
* Date :- Aug 23, 2019
*/

export const submitCustomerVerifyAmount = (formData) => (dispatch) => {
    dispatch({type: CUSTOMER_BANK_VERIFY_ACCOUNT});
        customerDashboardService.submitCustomerVerifyAmount(formData)
        .then((verifyAmt) => {
            
            /*customerService.viewCustomer(verifyAmt.patient_id)
                .then((customer) => {
                    verifyAmt.customerDetails = customer.customerDetails
                }).catch((e) => {
                    NotificationManager.error("Something wrong with get customer data.");
                })*/


            dispatch({ type: CUSTOMER_BANK_VERIFY_ACCOUNT_SUCCESS, payload:verifyAmt});

            NotificationManager.success(verifyAmt.message);
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_BANK_VERIFY_ACCOUNT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerPlanDetailsDownload = (currentUserId, planid) => (dispatch) => {
    dispatch({ type: CUSTOMER_APP_PLAN_DETAILS_PDF_DOWNLOAD_REQUEST, });
    customerDashboardService.customerPlanDetailsDownload(currentUserId, planid)
        .then((plan_details) => {
            //dispatch({ type: APP_PLAN_DETAILS__PDF_DOWNLOAD_SUCCESS, payload:plan_details});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_APP_PLAN_DETAILS_PDF_DOWNLOAD_FAILURE });
            NotificationManager.error(error);
        });
}

export const customerDashboardInvoiceReportDownloadXLS = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST });
    customerDashboardService.customerDashboardInvoiceReportDownloadXLS(formData)
        .then((fullReportList) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerDashboardInvoiceReportDownload = (formData) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST });
    customerDashboardService.customerDashboardInvoiceReportDownload(formData)
        .then((fullReportList) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerDashboardInvoiceReportFilter = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST });
    customerDashboardService.customerDashboardInvoiceReportFilter(data)
        .then((invoice) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const customerDashboardWeekMonthInvoiceReport = () => (dispatch) => {
    dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST });
    customerDashboardService.customerDashboardWeekMonthInvoiceReport()
        .then((invoice) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS, payload:invoice});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const updateCustomerProfile = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_UPDATE_REQUEST });
    customerDashboardService.updateCustomerProfile(data)
        .then((submit) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_SUCCESS, payload:submit});
            if(submit.applicationStatus==0){
                NotificationManager.error(submit.message);
            }else{
                NotificationManager.success(submit.message);
            }
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PROFILE_UPDATE_FAILURE });
            NotificationManager.error(error);
        });
}

export const editCustomerProfileDetails = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS });
    customerDashboardService.editCustomerProfileDetails(data)
        .then((search) => {
            let addError=[];
            let bankError=[];
            search.customerAddress && search.customerAddress.map(function (item, idx) {
                addError = addError.concat([{
                    address1: "",
                    country: "",
                    state: "",
                    region: "",
                    city: "",
                    zip_code: "",
                    how_long: "",
                    phone_no: "",
                }]);
                dispatch(customerProfileAddMore());
                dispatch(customerProfileGetStates(item.country, idx));
                
            });
            if(search.billing_country != ''){
                dispatch(customerProfileGetStates(search.billing_country));
            }
            search.bankDetails && search.bankDetails.map(function (item, idx) {
                bankError = bankError.concat([{
                    bank_name: "",
                    bank_address: "",
                    rounting_no: "",
                    bank_ac: "",
                    account_name: "",
                    account_type: "",
                }]);
                //dispatch(getRegion(item.state, idx));

            });


            search.bankError = bankError;
            search.addError = addError;
            dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS_SUCCESS, payload:search});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS_FAILURE });
            
            NotificationManager.error(error);
        });
}

export const editCosignerProfileDetails = (data) => (dispatch) => {
    dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS });
    cosignerService.editCosignerProfileDetails(data)
        .then((search) => {
            let addError = [];
            let bankError = [];
            search.customerAddress && search.customerAddress.map(function (item, idx) {
                addError = addError.concat([{
                    address1: "",
                    country: "",
                    state: "",
                    region: "",
                    city: "",
                    zip_code: "",
                    how_long: "",
                    phone_no: "",
                }]);
                dispatch(customerProfileAddMore());
                dispatch(customerProfileGetStates(item.country, idx));

            });
            if (search.billing_country != '') {
                dispatch(customerProfileGetStates(search.billing_country));
            }
            search.bankDetails && search.bankDetails.map(function (item, idx) {
                bankError = bankError.concat([{
                    bank_name: "",
                    bank_address: "",
                    rounting_no: "",
                    bank_ac: "",
                    account_name: "",
                    account_type: "",
                }]);
                //dispatch(getRegion(item.state, idx));

            });


            search.bankError = bankError;
            search.addError = addError;
            dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS_SUCCESS, payload: search });
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_EDIT_PROFILE_DETAILS_FAILURE });

            NotificationManager.error(error);
        });
}

export const customerProfileGetStates = (id,idx) => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_STATES_LIST });
    customerDashboardService.customerProfileGetStates(id)
        .then((state) => {
            state.idx=idx
            dispatch({ type: CUSTOMER_PROFILE_STATES_LIST_SUCCESS, payload:state});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PROFILE_STATES_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerProfileAddMore = () => (dispatch) => {
    dispatch({ type: CUSTOMER_PROFILE_ADD_MORE_LIST });
}

export const insertCustomerPayment = (formData, history) => (dispatch) => {
    dispatch({type: CUSTOMER_INSERT_PAYMENT});
        customerDashboardService.insertCustomerPayment(formData)
        .then((PayInstallment) => {
            NotificationManager.success('Payment successfully!');
            dispatch({ type: CUSTOMER_INSERT_PAYMENT_SUCCESS, payload:PayInstallment});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_INSERT_PAYMENT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerActivePlanDetails = (user_id) => (dispatch) => {
	//console.log(user_id);
	dispatch({ type: CUSTOMER_ACTIVE_PLAN, });
    customerDashboardService.customerActivePlanDetails(user_id)
        .then((plan) => {
        	dispatch({ type: CUSTOMER_ACTIVE_PLAN_SUCCESS, payload:plan});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_ACTIVE_PLAN_FAILURE });
            //NotificationManager.error(error);
        });
}






