import { NotificationManager } from 'react-notifications';
import { adminInvoiceService } from '../apifile';
import {
    ADMIN_OPEN_INVOICE_REQUEST,
    ADMIN_OPEN_INVOICE_SUCCESS,
    ADMIN_OPEN_INVOICE_FAILURE,
    ADMIN_VIEW_INVOICE_REQUEST,
    ADMIN_VIEW_INVOICE_SUCCESS,
    ADMIN_VIEW_INVOICE_FAILURE,
    ADMIN_DELETE_INVOICE_REQUEST,
    ADMIN_DELETE_INVOICE_SUCCESS,
    ADMIN_DELETE_INVOICE_FAILURE,
    ADMIN_ADD_INVOICE_REQUEST,
    ADMIN_ADD_INVOICE_SUCCESS,
    ADMIN_ADD_INVOICE_FAILURE,
    ADMIN_LIST_INVOICE_REQUEST,
    ADMIN_LIST_INVOICE_SUCCESS,
    ADMIN_LIST_INVOICE_FAILURE,
    ADMIN_CANCEL_INVOICE_REQUEST,
    ADMIN_CANCEL_INVOICE_SUCCESS,
    ADMIN_CANCEL_INVOICE_FAILURE,
    CONFIRM_INVOICE_REQUEST,
    CONFIRM_INVOICE_SUCCESS,
    CONFIRM_INVOICE_FAILURE,
    ADMIN_CLOSE_INVOICE_REQUEST,
    ADMIN_CLOSE_INVOICE_SUCCESS,
    ADMIN_CLOSE_INVOICE_FAILURE,
    ADMIN_CANCEL_CONINVOICE_REQUEST,
    ADMIN_CANCEL_CONINVOICE_SUCCESS,
    ADMIN_CANCEL_CONINVOICE_FAILURE,
    ADMIN_NOTE_INVOICE_REQUEST,
    ADMIN_NOTE_INVOICE_SUCCESS,
    ADMIN_NOTE_INVOICE_FAILURE,
    VIEW_INVOICE_REQUEST,
    VIEW_INVOICE_SUCCESS,
    VIEW_INVOICE_FAILURE,
    APPROVE_INVOICE_REQUEST,
    APPROVE_INVOICE_SUCCESS,
    APPROVE_INVOICE_FAILURE,
} from 'Actions/types';

/*
* Title :- approveInvoice
* Descrption :- this function use for approve invoice application
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 22, 2019
*/

export const approveInvoice = (invoice_id) => (dispatch) => {
	dispatch({ type: APPROVE_INVOICE_REQUEST, });
    adminInvoiceService.approveInvoice(invoice_id)
        .then((invoiceList) => {
            dispatch({ type: APPROVE_INVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: APPROVE_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerInvoiceView
* Descrption :- this function use for view sibgle report 
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 19, 2019
*/

export const providerInvoiceView = (id) => (dispatch) => {
    dispatch({ type: VIEW_INVOICE_REQUEST });
    adminInvoiceService.providerInvoiceView(id)
        .then((invoiceList) => {
        	dispatch({ type: VIEW_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: VIEW_INVOICE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- cancelInvoiceConfirmations
* Descrption :- this function use for cancel invoice confirmations
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/

export const viewInvoiceNotes = (invoice_id) => (dispatch) => {
	dispatch({ type: ADMIN_NOTE_INVOICE_REQUEST, });
    adminInvoiceService.viewInvoiceNotes(invoice_id)
        .then((invoiceList) => {
            dispatch({ type: ADMIN_NOTE_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_NOTE_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- cancelInvoiceConfirmations
* Descrption :- this function use for cancel invoice confirmations
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/

export const cancelInvoiceConfirmations = (invoice_id) => (dispatch) => {
	dispatch({ type: ADMIN_CANCEL_CONINVOICE_REQUEST, });
    adminInvoiceService.cancelInvoiceConfirmations(invoice_id)
        .then((invoiceList) => {
            dispatch({ type: ADMIN_CANCEL_CONINVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: ADMIN_CANCEL_CONINVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- invoiceAdminCloseList
* Descrption :- this function use for get all close invoice for admin
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/

export const invoiceAdminCloseList = () => (dispatch) => {
	dispatch({ type: ADMIN_CLOSE_INVOICE_REQUEST, });
    adminInvoiceService.invoiceAdminCloseList()
        .then((invoiceList) => {
        	dispatch({ type: ADMIN_CLOSE_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_CLOSE_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- confirmInvoiceSubmit
* Descrption :- this function use for confirm invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/

export const confirmInvoicesSubmit = (invoiceData) => (dispatch) => {
	dispatch({ type: CONFIRM_INVOICE_REQUEST, });
    adminInvoiceService.confirmInvoicesSubmit(invoiceData)
        .then((invoiceList) => {
            dispatch({ type: CONFIRM_INVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: CONFIRM_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- cancelInvoiceApplication
* Descrption :- this function use for cancel invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 04, 2019
*/

export const cancelInvoiceApplication = (invoice_id,commentNote) => (dispatch) => {
	dispatch({ type: ADMIN_CANCEL_INVOICE_REQUEST, });
    adminInvoiceService.cancelInvoiceApplication(invoice_id,commentNote)
        .then((invoiceList) => {
            dispatch({ type: ADMIN_CANCEL_INVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: ADMIN_CANCEL_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- addInvoiceApplication
* Descrption :- this function use for add new application in invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 04, 2019
*/

export const addInvoiceApplication = (applicationId,invoice_id) => (dispatch) => {
	dispatch({ type: ADMIN_ADD_INVOICE_REQUEST, });
    adminInvoiceService.addInvoiceApplication(applicationId,invoice_id)
        .then((invoiceList) => {
            dispatch({ type: ADMIN_ADD_INVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: ADMIN_ADD_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- listInvoiceApplication
* Descrption :- this function use for add new application in invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 04, 2019
*/

export const listInvoiceApplication = (provider_id,invoice_id) => (dispatch) => {
	dispatch({ type: ADMIN_LIST_INVOICE_REQUEST, });
    adminInvoiceService.listInvoiceApplication(provider_id)
        .then((invoiceList) => {
            invoiceList.invoice_id = invoice_id;
        	dispatch({ type: ADMIN_LIST_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_LIST_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- deleteInvoiceApplication
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/

export const deleteInvoiceApplication = (application_id,invoice_id, provider_id, comment) => (dispatch) => {
	dispatch({ type: ADMIN_DELETE_INVOICE_REQUEST, });
    adminInvoiceService.deleteInvoiceApplication(application_id,invoice_id, provider_id,comment)
        .then((invoiceList) => {
            dispatch({ type: ADMIN_DELETE_INVOICE_SUCCESS, payload:invoiceList});
            NotificationManager.success(invoiceList.message);
        })
        .catch((error) => {
            dispatch({ type: ADMIN_DELETE_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- viewInvoiceDetails
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/

export const viewInvoiceDetails = (invoice_id) => (dispatch) => {
	dispatch({ type: ADMIN_VIEW_INVOICE_REQUEST, });
    adminInvoiceService.viewInvoiceDetails(invoice_id)
        .then((invoiceList) => {
        	dispatch({ type: ADMIN_VIEW_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_VIEW_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- invoiceAdminOpenList
* Descrption :- this function use for get all open invoice for admin
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/

export const invoiceAdminOpenList = () => (dispatch) => {
	dispatch({ type: ADMIN_OPEN_INVOICE_REQUEST, });
    adminInvoiceService.invoiceAdminOpenList()
        .then((invoiceList) => {
        	dispatch({ type: ADMIN_OPEN_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: ADMIN_OPEN_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}




