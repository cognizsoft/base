import { NotificationManager } from 'react-notifications';
import { providerPendingService, userRolesService } from '../apifile';
import {
    PROVIDER_PENDING_REQUEST,
    PROVIDER_PENDING_SUCCESS,
    PROVIDER_PENDING_FAILURE,
    PROVIDER_PREVIEW_INVOICE,
    PROVIDER_PREVIEW_LOAD,
    PROVIDER_SELECTED_REQUEST,
    PROVIDER_SELECTED_SUCCESS,
    PROVIDER_SELECTED_FAILURE,
    PROVIDER_PDF_REQUEST,
    PROVIDER_PDF_SUCCESS,
    PROVIDER_PDF_FAILURE,
    PROVIDER_INVOICE_SUBMIT_REQUEST,
    PROVIDER_INVOICE_SUBMIT_SUCCESS,
    PROVIDER_INVOICE_SUBMIT_FAILURE,
    PROVIDER_SUBMITTED_REQUEST,
    PROVIDER_SUBMITTED_SUCCESS,
    PROVIDER_SUBMITTED_FAILURE,
    PROVIDER_VIEW_INVOICE_REQUEST,
    PROVIDER_VIEW_INVOICE_SUCCESS,
    PROVIDER_VIEW_INVOICE_FAILURE,
    
    PROVIDER_VIEW_INVOICE_SUBMITTED_REQUEST,
    PROVIDER_VIEW_INVOICE_SUBMITTED_SUCCESS,
    PROVIDER_VIEW_INVOICE_SUBMITTED_FAILURE,

    REFUND_INVOICE_REQUEST,
    REFUND_INVOICE_SUCCESS,
    REFUND_INVOICE_FAILURE,
    REFUND_INVOICE_PAY_REQUEST,
    REFUND_INVOICE_PAY_SUCCESS,
    REFUND_INVOICE_PAY_FAILURE,
} from 'Actions/types';

/*
* Title :- adminRefundAmount
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 15, 2020
*/

export const adminRefundAmount= (data) => (dispatch) => {
	dispatch({ type: REFUND_INVOICE_PAY_REQUEST, });
    providerPendingService.adminRefundAmount(data)
        .then((invoiceList) => {
            dispatch(refundAdminInvoiceList())
            NotificationManager.success(invoiceList.message);
        	dispatch({ type: REFUND_INVOICE_PAY_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: REFUND_INVOICE_PAY_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- refundAdminInvoiceList
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 12, 2020
*/

export const refundAdminInvoiceList = (data) => (dispatch) => {
	dispatch({ type: REFUND_INVOICE_REQUEST, });
    providerPendingService.refundAdminInvoiceList(data)
        .then((invoiceList) => {
        	dispatch({ type: REFUND_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: REFUND_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- providerRefundAmount
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 15, 2020
*/

export const providerRefundAmount= (data) => (dispatch) => {
	dispatch({ type: REFUND_INVOICE_PAY_REQUEST, });
    providerPendingService.providerRefundAmount(data)
        .then((invoiceList) => {
            dispatch(refundInvoiceList())
            NotificationManager.success(invoiceList.message);
        	dispatch({ type: REFUND_INVOICE_PAY_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: REFUND_INVOICE_PAY_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- refundInvoiceList
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 12, 2020
*/

export const refundInvoiceList = (data) => (dispatch) => {
	dispatch({ type: REFUND_INVOICE_REQUEST, });
    providerPendingService.refundInvoiceList(data)
        .then((invoiceList) => {
        	dispatch({ type: REFUND_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: REFUND_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- viewSubmittedInvoice
* Descrption :- this function use for submit invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/

export const viewSubmittedInvoice = (invoice_id,provider_id) => (dispatch) => {
    dispatch({ type: PROVIDER_VIEW_INVOICE_SUBMITTED_REQUEST, });
    providerPendingService.viewSubmittedInvoice(invoice_id,provider_id)
        .then((invoiceView) => {
            dispatch({ type: PROVIDER_VIEW_INVOICE_SUBMITTED_SUCCESS, payload:invoiceView});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_VIEW_INVOICE_SUBMITTED_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- viewInvoice
* Descrption :- this function use for submit invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/

export const viewInvoice = (invoice_id,provider_id) => (dispatch) => {
	dispatch({ type: PROVIDER_VIEW_INVOICE_REQUEST, });
    providerPendingService.viewInvoice(invoice_id,provider_id)
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_VIEW_INVOICE_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_VIEW_INVOICE_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- invoiceProviderSubmitList
* Descrption :- this function use for submit invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/

export const invoiceProviderSubmitList = (status_id) => (dispatch) => {
	dispatch({ type: PROVIDER_SUBMITTED_REQUEST, });
    providerPendingService.invoiceProviderSubmitList(status_id)
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_SUBMITTED_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_SUBMITTED_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- invoiceProviderInvoiceSubmit
* Descrption :- this function use for submit invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 01, 2019
*/

export const invoiceProviderInvoiceSubmit = (list) => (dispatch) => {
	dispatch({ type: PROVIDER_INVOICE_SUBMIT_REQUEST, });
    providerPendingService.invoiceProviderInvoiceSubmit(list)
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_INVOICE_SUBMIT_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_INVOICE_SUBMIT_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- invoiceProviderreviewPDF
* Descrption :- this function use for get pdf document
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/

export const invoiceProviderreviewPDF = (list) => (dispatch) => {
	dispatch({ type: PROVIDER_PDF_REQUEST, });
    providerPendingService.invoiceProviderreviewPDF(list)
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_PDF_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_PDF_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- invoiceProviderSelectedPendingList
* Descrption :- this function use for get selected application
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const invoiceProviderSelectedPendingList = (list) => (dispatch) => {
	dispatch({ type: PROVIDER_SELECTED_REQUEST, });
    providerPendingService.invoiceProviderSelectedPendingList(list)
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_SELECTED_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_SELECTED_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- invoiceProviderPreviewLoad
* Descrption :- this function use for loading enable in preview page
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 01, 2019
*/

export const invoiceProviderPreviewLoad = () => (dispatch) => {
	dispatch({ type: PROVIDER_PREVIEW_LOAD});
}

/*
* Title :- invoiceProviderSelected
* Descrption :- this function use for send data on preview invoice page.
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 01, 2019
*/

export const invoiceProviderSelected = (data) => (dispatch) => {
	dispatch({ type: PROVIDER_PREVIEW_INVOICE, payload:data});
}

/*
* Title :- creditApplicationList
* Descrption :- this function use for get crdit application list
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 06, 2019
*/

export const invoiceProviderPendingList = () => (dispatch) => {
	dispatch({ type: PROVIDER_PENDING_REQUEST, });
    providerPendingService.invoiceProviderPendingList()
        .then((invoiceList) => {
        	dispatch({ type: PROVIDER_PENDING_SUCCESS, payload:invoiceList});
        })
        .catch((error) => {
            dispatch({ type: PROVIDER_PENDING_FAILURE });
            NotificationManager.error(error);
        });
}




