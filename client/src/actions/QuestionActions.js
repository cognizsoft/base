import { NotificationManager } from 'react-notifications';
import { questionService } from '../apifile';
import {
    QUESTION_LIST,
    QUESTION_LIST_SUCCESS,
    QUESTION_LIST_FAILURE,
    QUESTION_INSERT,
    QUESTION_INSERT_SUCCESS,
    QUESTION_INSERT_FAILURE,
    QUESTION_UPDATE,
    QUESTION_UPDATE_SUCCESS,
    QUESTION_UPDATE_FAILURE,
    QUESTION_DELETE,
    QUESTION_DELETE_SUCCESS,
    QUESTION_DELETE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE
} from 'Actions/types';



/*
* Title :- checkSecurityQuestionExist
* Descrption :- this function use for check System Module exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const checkSecurityQuestionExist = (value, id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    questionService.checkSecurityQuestionExist(value, id)
        .then((SecurityQuestion) => {
            //user.email=0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: SecurityQuestion});
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllQuestion
* Descrption :- this function use for get question list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 March 2019
*/
export const getAllQuestion = () => (dispatch) => {
    dispatch({ type: QUESTION_LIST });
    questionService.getAllQuestion()
        .then((question) => {
            dispatch({ type: QUESTION_LIST_SUCCESS, payload: question.result });
        })
        .catch((error) => {
            dispatch({ type: QUESTION_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertQuestion
* Descrption :- this function use for insert question information
* Author :- Hp & Aman 
* Date :- 24 April 2019
*/

export const insertQuestion = (formData) => (dispatch) => {
    dispatch({ type: QUESTION_INSERT });
    questionService.insertQuestion(formData)
        .then((question) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: QUESTION_INSERT_SUCCESS, payload: question });
        })
        .catch((error) => {
            dispatch({ type: QUESTION_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateQuestion
* Descrption :- this function use for update question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const updateQuestion = (formData) => (dispatch) => {
    dispatch({ type: QUESTION_UPDATE });
    questionService.updateQuestion(formData)
        .then((state) => {
            dispatch({ type: QUESTION_UPDATE_SUCCESS, payload: state.result });
            NotificationManager.success('Question Updated!');
        })
        .catch((error) => {
            dispatch({ type: QUESTION_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteQuestion
* Descrption :- this function use for update question record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 24 April 2019
*/

export const deleteQuestion = (formData) => (dispatch) => {
    dispatch({ type: QUESTION_DELETE });
    questionService.deleteQuestion(formData)
        .then((question) => {
            NotificationManager.success('Question Deleted!');
            dispatch({ type: QUESTION_DELETE_SUCCESS, payload: question.id });
        })
        .catch((error) => {
            dispatch({ type: QUESTION_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}
