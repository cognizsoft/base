import { NotificationManager } from 'react-notifications';
import { customerService } from '../apifile';
import {
    CUSTOMER_LIST,
    CUSTOMER_LIST_SUCCESS,
    CUSTOMER_LIST_FAILURE,
    VIEW_CUSTOMER_REQUEST,
    VIEW_CUSTOMER_SUCCESS,
    VIEW_CUSTOMER_FAILURE,

    BANK_VERIFY_ACCOUNT,
    BANK_VERIFY_ACCOUNT_SUCCESS,
    BANK_VERIFY_ACCOUNT_FAILURE,

    CUSTOMER_PAUSE_REQUEST,
    CUSTOMER_PAUSE_SUCCESS,
    CUSTOMER_PAUSE_FAILURE,

    CUSTOMER_PAUSE_LIST_REQUEST,
    CUSTOMER_PAUSE_LIST_SUCCESS,
    CUSTOMER_PAUSE_LIST_FAILURE,

    CUSTOMER_UPLOAD_DOCUMENTS_REQUEST,
    CUSTOMER_UPLOAD_DOCUMENTS_SUCCESS,
    CUSTOMER_UPLOAD_DOCUMENTS_FAILURE,

    CUSTOMER_DOCUMENTS_REQUEST,
    CUSTOMER_DOCUMENTS_SUCCESS,
    CUSTOMER_DOCUMENTS_FAILURE,

    CUSTOMER_PROFILE_UPDATE_REQUEST,
    CREDIT_APPLICATION_LIST,
} from 'Actions/types';

/*
* Title :- customerAppUploadDocument
* Descrption :- this function use for provider upload patient document according to application
* Author :- Cogniz software so & Ramesh 
* Date :- Dec 17, 2019
*/
export const customerAppUploadDocument = (datafrom) => (dispatch) => {
    dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_REQUEST, });
    customerService.customerAppUploadDocument(datafrom)
        .then((doc) => {
            //dispatch(customerDocument(doc.customer_id));
            dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_SUCCESS, payload:doc});
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- customerDocument
* Descrption :- this function use for get customer documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const customerDocument = (id) => (dispatch) => {
    dispatch({ type: CUSTOMER_DOCUMENTS_REQUEST, });
    customerService.customerDocument(id)
        .then((all_doc) => {
            dispatch({ type: CUSTOMER_DOCUMENTS_SUCCESS, payload:all_doc});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_DOCUMENTS_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- customerUploadDocument
* Descrption :- this function use for admin upload patient document
* Author :- Cogniz software so & Aman 
* Date :- April 21, 2019
*/
export const customerUploadDocument = (datafrom) => (dispatch) => {
    dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_REQUEST, });
    customerService.customerUploadDocument(datafrom)
        .then((doc) => {
            console.log('doc')
            console.log(doc)
            dispatch(customerDocument(doc.customer_id));
            dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_SUCCESS, payload:doc});
            NotificationManager.success(doc.message);
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_UPLOAD_DOCUMENTS_FAILURE });
            NotificationManager.error(error);
        });
}
/*
* Title :- customerInvoicepPauseUpdate
* Descrption :- this function use for update invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 21, 2019
*/

export const customerInvoicepPauseUpdate = (formData) => (dispatch) => {
    dispatch({type: CUSTOMER_PAUSE_REQUEST});
        customerService.customerInvoicepPauseUpdate(formData)
        .then((result) => {
            dispatch({ type: CUSTOMER_PAUSE_SUCCESS, payload:result});
            NotificationManager.success(result.message);
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PAUSE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- listInvoicepPause
* Descrption :- this function use for add invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 23, 2019
*/

export const listInvoicepPause = (id) => (dispatch) => {
    dispatch({type: CUSTOMER_PAUSE_LIST_REQUEST});
        customerService.listInvoicepPause(id)
        .then((result) => {
            dispatch({ type: CUSTOMER_PAUSE_LIST_SUCCESS, payload:result});
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PAUSE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- customerInvoicepPause
* Descrption :- this function use for add invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 23, 2019
*/

export const customerInvoicepPause = (formData) => (dispatch) => {
    dispatch({type: CUSTOMER_PAUSE_REQUEST});
        customerService.customerInvoicepPause(formData)
        .then((result) => {
            dispatch({ type: CUSTOMER_PAUSE_SUCCESS, payload:result});
            NotificationManager.success(result.message);
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_PAUSE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- submitVerifyAmount
* Descrption :- this function use for verify bank account number
* Author :- Cogniz software so & Aman 
* Date :- Aug 23, 2019
*/

export const submitVerifyAmount = (formData) => (dispatch) => {
    dispatch({type: BANK_VERIFY_ACCOUNT});
        customerService.submitVerifyAmount(formData)
        .then((verifyAmt) => {
            dispatch({ type: BANK_VERIFY_ACCOUNT_SUCCESS, payload:verifyAmt});
            NotificationManager.success(verifyAmt.message);
        })
        .catch((error) => {
            dispatch({ type: BANK_VERIFY_ACCOUNT_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const customerListProvider = () => (dispatch) => {
	dispatch({ type: CUSTOMER_LIST, });
    customerService.customerListProvider()
        .then((list) => {
        	dispatch({ type: CUSTOMER_LIST_SUCCESS, payload:list});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

export const customerList = () => (dispatch) => {
	dispatch({ type: CUSTOMER_LIST, });
    customerService.customerList()
        .then((list) => {
        	dispatch({ type: CUSTOMER_LIST_SUCCESS, payload:list});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: CUSTOMER_LIST_FAILURE });
            NotificationManager.error(error);
        });
}

/*
* Title :- planDetails
* Descrption :- this function use for view application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

export const viewCustomer = (id) => (dispatch) => {
    dispatch({type: CUSTOMER_PROFILE_UPDATE_REQUEST});
    dispatch({ type: CREDIT_APPLICATION_LIST, });
	dispatch({ type: VIEW_CUSTOMER_REQUEST, });
    customerService.viewCustomer(id)
        .then((plan) => {
        	dispatch({ type: VIEW_CUSTOMER_SUCCESS, payload:plan});
        	//console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: VIEW_CUSTOMER_FAILURE });
            NotificationManager.error(error);
        });
}





