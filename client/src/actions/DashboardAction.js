import { NotificationManager } from 'react-notifications';
import { dashboardService } from '../apifile';
import {
    DASHBOARD_APPLICATION_REQUEST,
    DASHBOARD_APPLICATION_SUCCESS,
    DASHBOARD_APPLICATION_FAILURE,
} from 'Actions/types';

/*
* Title :- approveInvoice
* Descrption :- this function use for approve invoice application
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 22, 2019
*/

export const providerDashboardApplication = () => (dispatch) => {
	dispatch({ type: DASHBOARD_APPLICATION_REQUEST, });
    dashboardService.providerDashboardApplication()
        .then((appDetails) => {
            dispatch({ type: DASHBOARD_APPLICATION_SUCCESS, payload:appDetails});
        })
        .catch((error) => {
            dispatch({ type: DASHBOARD_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}


/*
* Title :- hpsDashboardApplication
* Descrption :- this function use for approve invoice application
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 22, 2019
*/

export const hpsDashboardApplication = () => (dispatch) => {
	dispatch({ type: DASHBOARD_APPLICATION_REQUEST, });
    dashboardService.hpsDashboardApplication()
        .then((appDetails) => {
            dispatch({ type: DASHBOARD_APPLICATION_SUCCESS, payload:appDetails});
        })
        .catch((error) => {
            dispatch({ type: DASHBOARD_APPLICATION_FAILURE });
            NotificationManager.error(error);
        });
}




