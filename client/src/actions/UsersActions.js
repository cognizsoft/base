import { NotificationManager } from 'react-notifications';
import { userService } from '../apifile';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,

    USER_LIST,
    USER_LIST_SUCCESS,
    USER_LIST_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE_FAILURE,
    USER_INSERT,
    USER_INSERT_SUCCESS,
    USER_INSERT_FAILURE,
    USER_UPDATE,
    USER_UPDATE_SUCCESS,
    USER_UPDATE_FAILURE,
    USER_DELETE,
    USER_DELETE_SUCCESS,
    USER_DELETE_FAILURE,
    USER_PASSWORD,
    USER_PASSWORD_SUCCESS,
    USER_PASSWORD_FAILURE,
    REFRESH_SIDEBAR,
    USER_ROLE,
    USER_ROLE_SUCCESS,
    USER_ROLE_FAILURE,
    PASSWORD_TOKEN_ACTION_REQUEST,
    PASSWORD_TOKEN_ACTION_SUCCESS,
    PASSWORD_TOKEN_ACTION_FAILURE,    
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE, 
    FORGET_PASSWORD_REQUEST,
    FORGET_PASSWORD_SUCCESS,
    FORGET_PASSWORD_FAILURE, 
    CLEAR_PASSWORD_STATE,

    UPDATE_AGREE_FLAG_REQUEST,
    UPDATE_AGREE_FLAG_SUCCESS,
    UPDATE_AGREE_FLAG_FAILURE,

    USER_UNLOCK,
    USER_UNLOCK_SUCCESS,
    USER_UNLOCK_FAILURE,

    LOCK_USER_DETAILS,
    LOCK_USER_DETAILS_SUCCESS,
    LOCK_USER_DETAILS_FAILURE,

    EXIST_UPDATE_CO,
    EXIST_UPDATE_SUCCESS_CO,
    EXIST_UPDATE_FAILURE_CO,
} from 'Actions/types';


/*
* Title :- getAllUsers
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/
export const getLockUserDetail = (user_id) => (dispatch) => {
    dispatch({ type: LOCK_USER_DETAILS });
    userService.getLockUserDetail(user_id)
        .then((user) => {
            dispatch({ type: LOCK_USER_DETAILS_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: LOCK_USER_DETAILS_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- unlockUserAccount
* Descrption :- this function use for unlock user
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const unlockUserAccount = (uid) => (dispatch) => {
    dispatch({ type: USER_UNLOCK });
    userService.unlockUserAccount(uid)
        .then((user) => {
            dispatch({ type: USER_UNLOCK_SUCCESS, payload: user });
            NotificationManager.success("User's account has been unlocked");
        })
        .catch((error) => {
            dispatch({ type: USER_UNLOCK_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- updateAgreeFlag
* Descrption :- this function use for clear old state
* Author :- CognizSoft & Ramesh Kumar
* Date :- oct 15, 2019
*/

export const updateAgreeFlag = (formData,img) => (dispatch) => {
    let userSt = localStorage.getItem('user');

    dispatch({ type: UPDATE_AGREE_FLAG_REQUEST });
    userService.updateAgreeFlag(formData,img)
        .then((user) => {
            dispatch({ type: UPDATE_AGREE_FLAG_SUCCESS, payload: user });

            userSt = (userSt) ? JSON.parse(userSt) : '';

            userSt['user'].agree_flag = 1
            //user.user.agree_flag = 1;
            localStorage.setItem('user', JSON.stringify(userSt));

            NotificationManager.success('You accepted the terms and conditions agreement.');
        })
        .catch((error) => {
            dispatch({ type: UPDATE_AGREE_FLAG_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- clearForgetState
* Descrption :- this function use for clear old state
* Author :- CognizSoft & Ramesh Kumar
* Date :- oct 15, 2019
*/

export const clearForgetState = (formData) => (dispatch) => {
    dispatch({ type: CLEAR_PASSWORD_STATE });
}

/*
* Title :- resetPasswordInApp
* Descrption :- this function use for forget password
* Author :- CognizSoft & Ramesh Kumar
* Date :- oct 14, 2019
*/

export const resetPasswordInApp = (formData) => (dispatch) => {
    dispatch({ type: RESET_PASSWORD_REQUEST });
    userService.resetPasswordInApp(formData)
        .then((user) => {
            dispatch({ type: RESET_PASSWORD_SUCCESS, payload: user.user_id });
            NotificationManager.success(user.message);
        })
        .catch((error) => {
            dispatch({ type: RESET_PASSWORD_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkTokenInApp
* Descrption :- this function use for forget password
* Author :- CognizSoft & Ramesh Kumar
* Date :- oct 14, 2019
*/

export const checkTokenInApp = (formData) => (dispatch) => {
    dispatch({ type: PASSWORD_TOKEN_ACTION_REQUEST });
    userService.checkTokenInApp(formData)
        .then((user) => {
            dispatch({ type: PASSWORD_TOKEN_ACTION_SUCCESS, payload: user.user_id });
        })
        .catch((error) => {
            dispatch({ type: PASSWORD_TOKEN_ACTION_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- forGetPasswordInApp
* Descrption :- this function use for forget password
* Author :- CognizSoft & Ramesh Kumar
* Date :- oct 14, 2019
*/

export const forGetPasswordInApp = (formData) => (dispatch) => {
    dispatch({ type: FORGET_PASSWORD_REQUEST });
    userService.forGetPasswordInApp(formData)
        .then((user) => {
            dispatch({ type: FORGET_PASSWORD_SUCCESS, payload: user.result });
            NotificationManager.success(user.message);
        })
        .catch((error) => {
            dispatch({ type: FORGET_PASSWORD_FAILURE});            
            NotificationManager.error(error.message);
        });
}



/*
* Title :- updateUserProvider
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- Jun 11, 2019
*/

export const updateUserProvider = (formData) => (dispatch) => {
    dispatch({ type: USER_UPDATE });
    userService.updateUserProvider(formData)
        .then((user) => {
            dispatch({ type: USER_UPDATE_SUCCESS, payload: user.result });
            NotificationManager.success('User Updated!');
        })
        .catch((error) => {
            dispatch({ type: USER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertUserProvider
* Descrption :- this function use for insert master
* Author :- Hp & Aman 
* Date :- Jun 11, 2019
*/

export const insertUserProvider = (formData) => (dispatch) => {
    dispatch({ type: USER_INSERT });
    userService.insertUserProvider(formData)
        .then((user) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: USER_INSERT_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: USER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllUsersProvider
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jun 11, 2019
*/
export const getAllUsersProvider = () => (dispatch) => {
    dispatch({ type: USER_LIST });
    userService.getAllUsersProvider()
        .then((user) => {
            dispatch({ type: USER_LIST_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: USER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getUserRole
* Descrption :- this function use for get user role according to uesr type
* Author :- Cogniz software & ramesh Kumar 
* Date :- 8 May 2019
*/

export const getUserRole = (id) => (dispatch) => {
    dispatch({ type: USER_ROLE });
    userService.getUserRole(id)
        .then((role) => {
            dispatch({ type: USER_ROLE_SUCCESS, payload: role.result });
        })
        .catch((error) => {
            dispatch({ type: USER_ROLE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const signinUserInApp = (formData, history) => (dispatch) => {
    dispatch({ type: LOGIN_USER });
    userService.login(formData)
        .then((user) => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem("user", JSON.stringify(user));
            let userSidebar = {
                role : user.role,
                coSigner : user.user.co_signer_id,
            }
            dispatch({ type: REFRESH_SIDEBAR, payload: userSidebar });
            dispatch({ type: LOGIN_USER_SUCCESS, payload: localStorage.getItem('user') });
            //history.push('/');
            NotificationManager.success('User Login Successfully!');
        })
        .catch((error) => {
            dispatch({ type: LOGIN_USER_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- getAllUsers
* Descrption :- this function use for get user list
* Author :- Cogniz software & ramesh Kumar 
* Date :- 6 March 2019
*/
export const getAllUsers = () => (dispatch) => {
    dispatch({ type: USER_LIST });
    userService.getAllUsers()
        .then((user) => {
            dispatch({ type: USER_LIST_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: USER_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkUsernameExist
* Descrption :- this function use for check username exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkUsernameExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    userService.checkUsernameExist(value, md_id)
        .then((user) => {
            user.email = 0;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkUsernameExistCo
* Descrption :- this function use for check username exits or not for co-signer
* Author :- CognizSoft & Ramesh Kumar
* Date :- 19 Aug 2020
*/
export const checkUsernameExistCo = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE_CO });
    userService.checkUsernameExist(value, md_id)
        .then((user) => {
            user.email = 0;
            dispatch({ type: EXIST_UPDATE_SUCCESS_CO, payload: user });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE_CO });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- checkEmailExist
* Descrption :- this function use for check eamil exits or not
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const checkEmailExist = (value, md_id) => (dispatch) => {
    dispatch({ type: EXIST_UPDATE });
    userService.checkEmailExist(value, md_id)
        .then((user) => {
            user.email = 1;
            dispatch({ type: EXIST_UPDATE_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: EXIST_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- deleteUser
* Descrption :- this function use for delete user
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/
export const deleteUser = (value) => (dispatch) => {
    dispatch({ type: USER_DELETE });
    userService.deleteUser(value)
        .then((user) => {
            dispatch({ type: USER_DELETE_SUCCESS, payload: user.user_id });
            NotificationManager.success('User Deleted!');
        })
        .catch((error) => {
            dispatch({ type: USER_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- insertUser
* Descrption :- this function use for insert master
* Author :- Hp & Aman 
* Date :- 04 April 2019
*/

export const insertUser = (formData) => (dispatch) => {
    dispatch({ type: USER_INSERT });
    userService.insertUser(formData)
        .then((user) => {
            NotificationManager.success('Record added successfully!');
            dispatch({ type: USER_INSERT_SUCCESS, payload: user });
        })
        .catch((error) => {
            dispatch({ type: USER_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateUser
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const updateUser = (formData) => (dispatch) => {
    dispatch({ type: USER_UPDATE });
    userService.updateUser(formData)
        .then((user) => {
            dispatch({ type: USER_UPDATE_SUCCESS, payload: user.result });
            NotificationManager.success('User Updated!');
        })
        .catch((error) => {
            dispatch({ type: USER_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- changeUserPassword
* Descrption :- this function use for update user record
* Author :- CognizSoft & Ramesh Kumar
* Date :- 16 April 2019
*/

export const changeUserPassword = (formData) => (dispatch) => {
    dispatch({ type: USER_PASSWORD });
    userService.changeUserPassword(formData)
        .then((user) => {
            dispatch({ type: USER_PASSWORD_SUCCESS, payload: user.result });
            NotificationManager.success('User Updated!');
        })
        .catch((error) => {
            dispatch({ type: USER_PASSWORD_FAILURE });
            NotificationManager.error(error.message);
        });
}

/**
 * Redux Action To Signout User From  Firebase
 */
export const logoutUserFromApp = (history) => (dispatch) => {
    dispatch({ type: LOGOUT_USER });
    localStorage.removeItem('user');
    localStorage.removeItem('user_permission');
    localStorage.removeItem('role');
    NotificationManager.success('User Logout Successfully');
    history.push('/');
}
