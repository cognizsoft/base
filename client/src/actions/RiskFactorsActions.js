import { NotificationManager } from 'react-notifications';
import { riskFactorsService } from '../apifile';
import {
    RISK_FACTORS_LIST,
    RISK_FACTORS_LIST_SUCCESS,
    RISK_FACTORS_LIST_FAILURE,

    RISK_FACTORS_UPDATE,
    RISK_FACTORS_UPDATE_SUCCESS,
    RISK_FACTORS_UPDATE_FAILURE,

    RISK_FACTORS_DELETE,
    RISK_FACTORS_DELETE_SUCCESS,
    RISK_FACTORS_DELETE_FAILURE,

    RISK_FACTORS_INSERT,
    RISK_FACTORS_INSERT_SUCCESS,
    RISK_FACTORS_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

} from 'Actions/types';

/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const riskFactorMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    riskFactorsService.getAllRiskFactorMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const riskFactorsList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: RISK_FACTORS_LIST });
    riskFactorsService.getAllRiskFactors(formData)
        .then((riskFactors) => {
        	dispatch({ type: RISK_FACTORS_LIST_SUCCESS, payload:riskFactors.result});
        })
        .catch((error) => {
            dispatch({ type: RISK_FACTORS_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateMasterValue
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateRiskFactors = (formData, history) => (dispatch) => {
    dispatch({type: RISK_FACTORS_UPDATE});
        riskFactorsService.updateRiskFactors(formData)
        .then((riskFactorsUpdate) => {
            dispatch({ type: RISK_FACTORS_UPDATE_SUCCESS, payload:riskFactorsUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: RISK_FACTORS_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- deleteUserType
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const deleteRiskFactors = (formData, history) => (dispatch) => {
    //console.log("sfdf");

    dispatch({type: RISK_FACTORS_DELETE});
    riskFactorsService.deleteMasterValues(formData)
    .then((riskFactorsDelete) => {
        dispatch({ type: RISK_FACTORS_DELETE_SUCCESS, payload:riskFactorsDelete.result});
        //console.log(userType.result)
    })
    .catch((error) => {
        dispatch({ type: RISK_FACTORS_DELETE_FAILURE });
        NotificationManager.error(error.message);
    });
}


/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertRiskFactors = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: RISK_FACTORS_INSERT});
        riskFactorsService.insertRiskFactors(formData)
        .then((riskFactorsInsert) => {
        	NotificationManager.success('Record added successfully!');
            dispatch({ type: RISK_FACTORS_INSERT_SUCCESS, payload:riskFactorsInsert});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: RISK_FACTORS_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

