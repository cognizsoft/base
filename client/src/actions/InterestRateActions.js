import { NotificationManager } from 'react-notifications';
import { interestRateService } from '../apifile';
import {
    INTEREST_RATE_LIST,
    INTEREST_RATE_LIST_SUCCESS,
    INTEREST_RATE_LIST_FAILURE,

    INTEREST_RATE_UPDATE,
    INTEREST_RATE_UPDATE_SUCCESS,
    INTEREST_RATE_UPDATE_FAILURE,

    INTEREST_RATE_DELETE,
    INTEREST_RATE_DELETE_SUCCESS,
    INTEREST_RATE_DELETE_FAILURE,

    INTEREST_RATE_INSERT,
    INTEREST_RATE_INSERT_SUCCESS,
    INTEREST_RATE_INSERT_FAILURE,

    MASTER_DATA_VALUE_LIST,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

    INTEREST_RATE_SCORE_EXIST,
    INTEREST_RATE_SCORE_EXIST_SUCCESS,
    INTEREST_RATE_SCORE_EXIST_FAILURE

} from 'Actions/types';



  /////////////////////////////////////////////////////////////////////
 /////////////////////////////START///////////////////////////////////
/////////////////////////////////////////////////////////////////////

export const checkInterest_RateExist = (value,md_id) => (dispatch) => {
    dispatch({type: INTEREST_RATE_SCORE_EXIST});
    interestRateService.checkInterest_RateExist(value,md_id)
        .then((scoreExist) => {
            dispatch({ type: INTEREST_RATE_SCORE_EXIST_SUCCESS, payload:scoreExist});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_SCORE_EXIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const listInterest_Rate = (formData, history) => (dispatch) => {
    dispatch({ type: INTEREST_RATE_LIST });
    interestRateService.listInterest_Rate(formData)
        .then((interestRate) => {
            dispatch({ type: INTEREST_RATE_LIST_SUCCESS, payload:interestRate.result});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const updateInterest_Rate = (formData, history) => (dispatch) => {
    dispatch({type: INTEREST_RATE_UPDATE});
        interestRateService.updateInterest_Rate(formData)
        .then((interestRateUpdate) => {
            dispatch({ type: INTEREST_RATE_UPDATE_SUCCESS, payload:interestRateUpdate.result});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const deleteInterest_Rate = (formData, history) => (dispatch) => {
    dispatch({type: INTEREST_RATE_DELETE});
        interestRateService.deleteInterest_Rate(formData)
        .then((interestRateDelete) => {
            dispatch({ type: INTEREST_RATE_DELETE_SUCCESS, payload:interestRateDelete.result});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_DELETE_FAILURE });
            NotificationManager.error(error.message);
        });
}

export const insertInterest_Rate = (formData, history) => (dispatch) => {
    dispatch({type: INTEREST_RATE_INSERT});
        interestRateService.insertInterest_Rate(formData)
        .then((interestRateInsert) => {
            dispatch({ type: INTEREST_RATE_INSERT_SUCCESS, payload:interestRateInsert});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}



  /////////////////////////////////////////////////////////////////////
 //////////////////////////////END////////////////////////////////////
/////////////////////////////////////////////////////////////////////


/*
* Title :- master data value
* Descrption :- master value for dropdown
* Author :- Aman 
* Date :- 11 April 2019
*/
export const interestRateMasterDataValueList = (formData, history) => (dispatch) => {
    //console.log("dsfddgvddddddddd");
    dispatch({ type: MASTER_DATA_VALUE_LIST });
    interestRateService.getAllInterestRateMasterDataValue(formData)
        .then((masterDataValue) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_SUCCESS, payload:masterDataValue});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: MASTER_DATA_VALUE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for login action
* Author :- Cogniz software & ramesh Kumar 
* Date :- 5 March 2019
*/

export const interestRateList = (formData, history) => (dispatch) => {
	//console.log("dsfddgvddddddddd");
    dispatch({ type: INTEREST_RATE_LIST });
    interestRateService.getAllInterestRate(formData)
        .then((interestRate) => {
        	dispatch({ type: INTEREST_RATE_LIST_SUCCESS, payload:interestRate.result});
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_LIST_FAILURE });
            NotificationManager.error(error.message);
        });
}


/*
* Title :- updateMasterValue
* Descrption :- this function use for update user type data
* Author :- Hp & Aman 
* Date :- 13 March 2019
*/

export const updateInterestRate = (formData, history) => (dispatch) => {
    dispatch({type: INTEREST_RATE_UPDATE});
       interestRateService.updateInterestRate(formData)
        .then((interestRateUpdate) => {
            dispatch({ type: INTEREST_RATE_UPDATE_SUCCESS, payload:interestRateUpdate.result});
            //console.log(userType.result)
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_UPDATE_FAILURE });
            NotificationManager.error(error.message);
        });
}



/*
* Title :- insertUserType
* Descrption :- this function use for insert user type data
* Author :- Hp & Aman 
* Date :- 15 March 2019
*/

export const insertInterestRate = (formData, history) => (dispatch) => {
    //console.log("dvdfd");
    //console.log(formData)
    dispatch({type: INTEREST_RATE_INSERT});
        interestRateService.insertInterestRate(formData)
        .then((interestRateInsert) => {
        	
            dispatch({ type: INTEREST_RATE_INSERT_SUCCESS, payload:interestRateInsert});
            //console.log(userType.result)
            NotificationManager.success('Record added successfully!');
        })
        .catch((error) => {
            dispatch({ type: INTEREST_RATE_INSERT_FAILURE });
            NotificationManager.error(error.message);
        });
}

