/**
 * Chek if email is valid
 * @prop String email
 * @returns Boolean
 */
export const isEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\\]+(\.[^<>()[\]\\.,;:\s@\\]+)*)|(\\.+\\))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**
 * Chek if vatiable is empty
 * @prop String thing
 * @returns Boolean
 */
export const isEmpty = (thing) => {
    let empty = false;
    switch (typeof thing) {
        case 'undefined':
            empty = true;
            break;
        case 'string':
            if (thing.trim().length === 0) {
                empty = true;
            }
            break;
        case 'object':
            if (thing === null) {
                empty = true;
            } else if (Object.keys(thing).length === 0) {
                empty = true;
            }
            break;
        default:
            break;
    }
    return empty;
}
export const isObjectEmpty = (thing) => {
    let empty = false;
    switch (typeof thing) {
        case 'object':
            if (Object.keys(thing).length !== 0) {
                empty = true;
            }
            break;
        default:
            break;
    }
    return empty;
}
/**
 * Check length of the string greater than
 * @prop String|Integer str
 * @prop boolean|options.trim Trim input before validating
 * @prop number|options.lt Check if length less than lt
 * @prop number|options.lte Check if length is less than or equals to lte
 * @prop number|options.gt Check if length is greater than gt
 * @prop number|options.gte Check if length is greater than or equals to gte
 * @returns Boolean
 */
export const isLength = (str, options) => {

    if (isEmpty(options)) {
        throw new Error("Who will provide the options you?");
    }

    let isValid = true;

    if (['string', 'number'].indexOf(typeof str) === -1) {
        isValid = false;
    } else {
        // Convert to string incase it's number
        let len = 0;

        if (options.trim) {
            len = str.toString().trim().length;
        } else {
            len = str.toString().length;
        }

        if (typeof options.lt === 'number' && len >= options.lt) {
            isValid = false;
        } else if (typeof options.lte === 'number' && len > options.lte) {
            isValid = false;
        } else if (typeof options.gt === 'number' && len <= options.gt) {
            isValid = false;
        } else if (typeof options.gte === 'number' && len < options.gte) {
            isValid = false;
        }
    }

    return isValid;
}

export const isPassword = (str, options) => {

    if (isEmpty(options)) {
        throw new Error("Who will provide the options you?");
    }

    let isValid = true;
    let reg = /^(?=.*[0-9])(?=.*[A-Z])([a-zA-Z0-9~!@#$%^&*\(\)_+=\-\[\]\{\}\|:"';<>\\?,./]+)$/
    if (['string', 'number'].indexOf(typeof str) === -1) {
        isValid = false;
    } else {
        // Convert to string incase it's number
        let len = 0;

        if (options.trim) {
            len = str.toString().trim().length;
            str = str.toString().trim();
        } else {
            len = str.toString().length;
            str = str.toString();
        }

        if (typeof options.min === 'number' && len < options.min) {
            isValid = false;
        } else if (!reg.test(str)) {
            isValid = false;
        }
    }

    return isValid;
}

/**
 * Check if string contains whitespaces
 * @prop String str
 * @returns Boolean
 */
export const isContainWhiteSpace = (str) => {

    if (typeof str === 'string' || typeof str === 'number') {
        return str.toString().trim().indexOf(' ') !== -1;
    } else {
        return false;
    }
}

/**
 * Check if string contains only A-Z0-9 and _ -
 * @prop String str
 * @returns Boolean
 */
export const isAlphaDigitUnderscoreDash = (str) => {

    const re = /^[A-Z0-9_-]+$/i;
    return re.test(str);
}
/**
 * Check if string contains only A-Z0-9
 * @prop String str
 * @returns Boolean
 */
export const isAlphaDigit = (str) => {

    const re = /^[A-Z0-9]+$/i;
    return re.test(str);
}
/**
 * Check if string contains only A-Z0-9 
 * @prop String str
 * @returns Boolean
 */
export const isAlphaNumeric = (str) => {

    const re = /^[A-Z0-9]+$/i;
    return re.test(str);
}

/**
 * Check if Allow some spacil character 
 * @prop String str
 * @returns Boolean
 */
export const isMaster = (str) => {
    const re = /^[a-zA-Z0-9 _-]+$/i;
    return re.test(str);
}

/**
 * Check if Allow some spacil character 
 * @prop String str
 * @returns Boolean
 */
export const isNumeric = (str) => {
    const re = /[^0-9\.]/g;
    return re.test(str);
}

export const isNumericDecimal = (str) => {
    const re = /[^0-9]/g;
    return re.test(str);
}
/**
 * Convert phone number in us format
 * @prop String str
 * @returns Boolean
 */
export const formatPhoneNumber = (phoneNumberString) => {
    var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    if (phoneRegex.test(phoneNumberString)) {
        return phoneNumberString.replace(phoneRegex, "($1) $2-$3");
    } else {
        return phoneNumberString;
    }
}

export const isPhone = (str) => {
    const re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return !re.test(str);
}

/**
 * Check if Numeric and Decimal
 */

export const isDecimals = (str) => {

    const re = /^\d+(\.\d{1,2})?$/;
    return re.test(str);
}

export const pointDecimals = (str) => {

    var ex = /^\d*\.?\d{0,2}$/;
    if (ex.test(str) == false) {
        return str.substring(0, str.length - 1);
    }else{
        return str;
    }
}

/**
 * Check if SSN/TaxID
 */
export const isSSN = (str) => {
    const re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})$/;
    return !re.test(str);
}
export const formatSSNNumber = (phoneNumberString) => {
    var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})$/;

    if (phoneRegex.test(phoneNumberString)) {
        return phoneNumberString.replace(phoneRegex, "$1-$2-$3");
    } else {
        return phoneNumberString;
    }
}

/**
 * Check if ZIP
 */
export const isZip = (str) => {

    const re = /^[A-Z0-9 ]+$/i;
    return re.test(str);
}


export const ageVliad = (birthday) => {
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    //console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

export const isAlpha = (inputtxt) => {

    const re = /^[A-Za-z ]+$/;
    return re.test(inputtxt);


}