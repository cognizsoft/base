//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const scoreThresholdService = {
  getAllScoreThreshold,
  getAllScoreThresholdMasterDataValue,
  updateScoreThreshold,
  insertScoreThreshold,
  checkScoreThresholdExist
};

/*
* Title :- checkScoreThresholdExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkScoreThresholdExist(name, id) {
    let cid = currentUserId();
    let formData = {};
    formData['name'] = name;
    formData['id'] = id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/score-threshold-exist`, formData, header)
        .then(handleResponse);
}

function getAllScoreThresholdMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/score-threshold-master-data-value`, header)
    .then(handleResponse);
}

function getAllScoreThreshold(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/score-threshold`, header)
    .then(handleResponse);
}

function updateScoreThreshold(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/score-threshold-update`,formData, header)
    .then(handleResponse);
}

function insertScoreThreshold(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/score-threshold-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
