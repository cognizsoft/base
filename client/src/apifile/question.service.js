//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const questionService = {
    getAllQuestion,
    insertQuestion,
    updateQuestion,
    deleteQuestion,
    checkSecurityQuestionExist
};

/*
* Title :- checkSecurityQuestionExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkSecurityQuestionExist(value, id) {
    let cid = currentUserId();
    let formData = {};
    formData['name'] = value;
    formData['id'] = id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/question-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getAllQuestion
* Descrpation :- This function use in get all question list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllQuestion() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/question-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertQuestion
* Descrpation :- This function use in add question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertQuestion(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/question-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- updateUser
* Descrpation :- This function use in update question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateQuestion(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/question-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- deleteQuestion
* Descrpation :- This function use in delete question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function deleteQuestion(value) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/question-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
