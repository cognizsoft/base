//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const cityService = {
    getAllCity,
    insertCity,
    updateCity,
    checkCityExist,

    selectedCountryState
};

/*
* Title :- selectedCountryState
* Descrpation :- This function use in state name exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function selectedCountryState(value) {
    let cid = currentUserId();
    let formData = {};
    formData['country_id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/city-country-states-list`, formData, header)
        .then(handleResponse);
}


/*
* Title :- checkNameExist
* Descrpation :- This function use in state name exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkCityExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['name'] = value;
    formData['state_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/region-name-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getAllCity() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/region-list`, header)
        .then(handleResponse);
}
/*
* Title :- insertUser
* Descrpation :- This function use in add new user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function insertCity(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/region-insert`, requestOptions, header)
        .then(handleResponse);

}
/*
* Title :- updateRegion
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function updateCity(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/region-update`, formData, header)
        .then(handleResponse);
}
/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
