//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const surchargeTypeService = {
    getAllSurchargeType,
    insertSurchargeType,
    updateSurchargeType,
    checkSurchargeTypeExist
};


/*
* Title :- checkSurchargeTypeExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkSurchargeTypeExist(name, id) {
    let cid = currentUserId();
    let formData = {};
    formData['name'] = name;
    formData['id'] = id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/surcharge-type-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getAllSurchargeType() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/surcharge-type-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertState
* Descrpation :- This function use in add state details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 22,2019
*/
function insertSurchargeType(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/surcharge-type-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- updateUser
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function updateSurchargeType(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/surcharge-type-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
