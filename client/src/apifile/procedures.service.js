//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const proceduresService = {
    proceduresList,
    insertProcedures,
    updateProcedures
};

/*
* Title :- updateProcedures
* Descrpation :- This function use in update procedures details
* Author : Cognizsoft and Ramesh Kumar
* Date :- Jun 11,2019
*/
function updateProcedures(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/procedures-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- insertUserProvider
* Descrpation :- This function use in add new user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- Jun 11,2019
*/
function insertProcedures(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/procedures-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- proceduresList
* Descrpation :- This function use in get all procedures list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function proceduresList() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/procedures-list`, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
