//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import { URL } from './URL';
import moment from 'moment';
import axios from 'axios';
export const AdminReportService = {
  fullReportList,
  loanReportFilter,
  loanReportDownload,
  providerInvoiceReport,
  providerInvoiceReportFilter,
  providerInvoiceReportDownload,
  providerInvoiceReportDownloadXLS,
  loanReportDownloadXLS,
  customerWeekMonthInvoiceReport,
  customerInvoiceReportFilter,
  customerInvoiceReportDownload,
  customerInvoiceReportDownloadXLS,
  customerfullInvoiceDownload,
  providerSingleInvoiceReportDownload,
  customerWithdrawalReportFilter,
  customerWithdrawalReportDownload,
  customerWithdrawalReportDownloadXLS,
  customerProcedureReportFilter,
  customerProcedureReportDownload,
  customerProcedureReportDownloadXLS,
  customerProcedureReportEmail,
  customerProcedureSingleReportEmail,
  customerReminderEmail,
  accountReceivablesReport,
  accountReceivablesReportFilter,
  accountReceivablesReportDownload,
  accountReceivablesReportDownloadXLS,
  supportReport,
  supportReportFilter,
  supportReportDownload,
  supportReportDownloadXLS,
  customerSupportReport,
  customerSupportReportFilter,
  customerSupportReportDownload,
  customerSupportReportDownloadXLS,
  providerAccountPayableFilter,
  providerAccountPayableDownload,
  providerAccountPayableDownloadXLS,
  customerCreditChargeFilter,
  customerCreditChargeDownload,
  customerCreditChargeDownloadXLS,
};

/*
* Title :- customerCreditChargeDownloadXLS
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- jULY 24, 2020
*/
function customerCreditChargeDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-credit-charges-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, 'credit-charges.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- customerCreditChargeDownload
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- jULY 24, 2020
*/
function customerCreditChargeDownload(formData) {
  console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-credit-charges-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'credit-charges.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- customerCreditChargeFilter
* Descrption :- this function use for get customer credit card details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 24, 2020
*/
function customerCreditChargeFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-credit-charges`, formData, header)
    .then(handleResponse);
}

/*
* Title :- providerAccountPayableDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function providerAccountPayableDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-payable-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Accounts Payable ' + dateTime ;
            
            
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName+'.xlsx');
            return result;
          });

      }

    });
}


/*
* Title :- providerAccountPayableDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function providerAccountPayableDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-payable-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Accounts Payable ' + dateTime ;
            
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName + '.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- providerAccountPayableFilter
* Descrption :- this function use for filter provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function providerAccountPayableFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-provider-payable-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerSupportReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerSupportReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-support-report-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, 'customer-support-report.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- supportReportDownload
* Descrption :- this function use for download reposrts loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerSupportReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-support-report-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'customer-support-report.pdf');
            return result;
          });

      }

    });
}
/*
* Title :- customerSupportReportFilter
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerSupportReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-support-report-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerSupportReport
* Descrption :- this function use get all locan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerSupportReport(formData) {
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/customer-support-report`, header)
    .then(handleResponse);
}

/*
* Title :- supportReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function supportReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-support-report-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, 'provider-support-report.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- supportReportDownload
* Descrption :- this function use for download reposrts loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function supportReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-support-report-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'provider-support-report.pdf');
            return result;
          });

      }

    });
}
/*
* Title :- supportReportFilter
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function supportReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-support-report-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- supportReport
* Descrption :- this function use get all locan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function supportReport(formData) {
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/provider-support-report`, header)
    .then(handleResponse);
}

/*
* Title :- accountReceivablesReportXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function accountReceivablesReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-account-receivables-report-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Account Receivables ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName+'.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- accountReceivablesReportDownload
* Descrption :- this function use for download reposrts loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function accountReceivablesReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-account-receivables-report-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Account Receivables ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName+'.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- accountReceivablesReportFilter
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function accountReceivablesReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-account-receivables-report-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- accountReceivablesReport
* Descrption :- this function use get all locan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function accountReceivablesReport(formData) {
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/admin-account-receivables-report`, header)
    .then(handleResponse);
}

function customerReminderEmail(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-reminder-invoice-email`, formData, header)
    .then(handleResponse);
}
/*
* Title :- customerProcedureSingleReportEmail
* Descrption :- this function use for get Procedure Date Report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

function customerProcedureSingleReportEmail(id) {
  let formData = {
    id: id,
  }
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-procedure-single-email`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerProcedureReportEmail
* Descrption :- this function use for get Procedure Date Report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

function customerProcedureReportEmail(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-procedure-date-email`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerProcedureReportDownloadXLS
* Descrption :- this function use for download Procedure Date Report pdf
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/

function customerProcedureReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/procedure-date-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Provider Procedure Date Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName+'.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- customerProcedureReportDownload
* Descrption :- this function use for get Procedure Date Report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 7, 2020
*/
function customerProcedureReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/procedure-date-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Provider Procedure Date Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName+'.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- customerProcedureReportFilter
* Descrption :- this function use for get Procedure Date Report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- April 6, 2020
*/

function customerProcedureReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-procedure-date-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report XLS
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/
function customerWithdrawalReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-withdrawal-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Withdrawal Day Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName+'.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- customerWithdrawalReportDownload
* Descrption :- this function use for download customer withdrawal report PDF
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2019
*/
function customerWithdrawalReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-withdrawal-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Withdrawal Day Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName+'.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- customerWithdrawalReportFilter
* Descrption :- this function use for get customer withdrawal report data
* Author :- Cogniz software & ramesh Kumar 
* Date :- March 24, 2020
*/
function customerWithdrawalReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-customer-withdrawal-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerfullInvoiceDownload
* Descrption :- this function use for download customer weekly & monthly invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerfullInvoiceDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/invoice-print`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'customer-invoice.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- customerInvoiceReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerInvoiceReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-weeklt-monthly-invoice-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, 'customer-invoice.xlsx');
            return result;
          });

      }

    });
}


/*
* Title :- customerInvoiceReportDownload
* Descrption :- this function use for download customer weekly & monthly invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function customerInvoiceReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/customer-weeklt-monthly-invoice-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'customer-invoice.pdf');
            return result;
          });

      }

    });
}


/*
* Title :- customerInvoiceReportFilter
* Descrption :- this function use for filter customer invoice filter details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function customerInvoiceReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-customer-weekmonth-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- customerWeekMonthInvoiceReport
* Descrption :- this function use for get customer week month reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/
function customerWeekMonthInvoiceReport() {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/admin-customer-weekmonth-list`, header)
    .then(handleResponse);
}

/*
* Title :- providerInvoiceReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function providerInvoiceReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-invoice-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Provider Invoices Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName+'.xlsx');
            return result;
          });

      }

    });
}


/*
* Title :- providerInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function providerInvoiceReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-invoice-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Provider Invoices Report ' + dateTime ;
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName+'.pdf');
            return result;
          });

      }

    });
}


/*
* Title :- providerSingleInvoiceReportDownload
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function providerSingleInvoiceReportDownload(invoice_id, provider_id) {
  //console.log(formData)
  let data = {
    invoice_id: invoice_id,
    provider_id: provider_id,
    check: 1,
  }
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/provider-single-invoice-pdf`, data, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'provider-single-invoice.pdf');
            return result;
          });

      }

    });
}


/*
* Title :- providerInvoiceReportFilter
* Descrption :- this function use for filter provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function providerInvoiceReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-provider-invoice-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- providerInvoiceReport
* Descrption :- this function use for get provider invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function providerInvoiceReport(status_id) {
  let formData = {
    params: {
      status_id: status_id,
    },
    headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/admin-provider-invoice-list`, formData)
    .then(handleResponse);
}

/*
* Title :- providerInvoiceReportDownloadXLS
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function loanReportDownloadXLS(formData) {

  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-reposrt-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Loan Report ' + dateTime ;
            
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, fileName + '.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- loanReportDownload
* Descrption :- this function use for download reposrts loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function loanReportDownload(formData) {
  this.formData = formData;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-report-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let FilterData = formData;
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            let fileName = '';
            let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');
            
            fileName = 'Loan Report ' + dateTime ;
            
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, fileName+'.pdf');
            return result;
          });

      }

    });
}

/*
* Title :- viewInvoiceNotes
* Descrption :- this function use for filter loan reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function loanReportFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-report-filter`, formData, header)
    .then(handleResponse);
}

/*
* Title :- fullReportList
* Descrption :- this function use get all locan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function fullReportList(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/admin-report-list`, header)
    .then(handleResponse);
}


function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
