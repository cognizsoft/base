export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.token) {
        return user.user.token ;
    } else {
        return '';
    }
}

export function currentUserId() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.current_user_id) {
        return user.user.current_user_id ;
    } else {
        return {};
    }
}

export function providerID() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.provider_id) {
        return user.user.provider_id ;
    } else {
        return {};
    }
}

export function patientID() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.patient_id) {
        return user.user.patient_id ;
    } else {
        return {};
    }
}

export function welcomeUsername() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.username) {
        return user.user.username ;
    } else {
        return {};
    }
}

export function userName() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.name) {
        return user.user.name ;
    } else {
        return {};
    }
}

export function timezone() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.timezone) {
        return user.user.timezone ;
    } else {
        return {};
    }
}

export function currentUserRole() {
    let user = JSON.parse(localStorage.getItem('user'));
    //console.log(user)
    if (user && user.role) {
        return user.role ;
    } else {
        return '';
    }
}

export function expiresAtM() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.expiresAt) {
        return user.user.expiresAt ;
    } else {
        return '';
    }
}

export function providerAgreeFlag() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user) {
        return user.user.agree_flag ;
    } else {
        return {};
    }
}

export function currentCoSignerId() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.user.co_signer_id) {
        return user.user.co_signer_id ;
    } else {
        return '';
    }
}