//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const lateFeeWaiverService = {
  getAllLateFeeWaiver,
  getAllLateFeeWaiverMasterDataValue,
  getAllLateFeeWaiverReasonMasterDataValue,
  updateLateFeeWaiver,
  insertLateFeeWaiver
};

function getAllLateFeeWaiverMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/late-fee-waiver-master-data-value`, header)
    .then(handleResponse);
}

function getAllLateFeeWaiverReasonMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/late-fee-reason-master-data-value`, header)
    .then(handleResponse);
}

function getAllLateFeeWaiver(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/late-fee-waiver`, header)
    .then(handleResponse);
}

function updateLateFeeWaiver(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/late-fee-waiver-update`,formData, header)
    .then(handleResponse);
}

function insertLateFeeWaiver(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/late-fee-waiver-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
