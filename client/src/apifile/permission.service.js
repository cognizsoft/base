//import config from 'config';


import axios from 'axios';
import {URL} from './URL';
import { authHeader,currentUserId } from '../apifile';
export const permissionService = {
    getPermisionFilter,
    getPermisionModule,
    getPermisionSubmite
};


function getPermisionFilter() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/permision-filter`, header)
        .then(handleResponse);
}

function getPermisionModule(user_role) {
    const header = { 
        headers: { "Authorization": `Bearer ${authHeader()}` },
        params: {
        user_role: user_role
        } 
    };
    return axios.get(`${URL.APIURL}/api/permision-module`, header)
        .then(handleResponse);
}

function getPermisionSubmite(formData) {
    let  cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = {
        headers: { "Authorization": `Bearer ${authHeader()}` }
    };
    return axios.post(`${URL.APIURL}/api/permision-submit`, formData, header)
        .then(handleResponse);
}



function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            // auto logout if 401 response returned from api
            logout();
            //location.reload(true);
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        //const data = JSON.parse(response.data);

        const data = response.data;
        //console.log(data)
        return data;
    }
    //return false;

}
