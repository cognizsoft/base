import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const creditApplicationService = {
    applicationOption,
    getStates,
    getRegion,
    submitApplication,
    creditApplicationList,
    paymentPlan,
    createPaymentPrintPlan,
    createPaymentPlan,
    planDetails,
    viewApplication,
    creditApplicationListProvider,
    submitApplicationProvider,
    submitSearchCustomer,
    creditApplicationReview,
    creditApplicationManualActions,
    rejectApplication,
    creditApplicationUploadDocument,
    creditApplicationReviewDocument,
    getSpeciality,
    uploadAgreement,
    downloadAgrement,
    searchCustomerApplication,
    applicationDetailsData,
    printPlan,
    updateAppExpireDate,
    estimatePlan,
    applicationFinalDetails,
    applicationEmail,
    printEstimatePlan,
    applicationDoc,
    uploadRejectionAgreement,
    eamilPlan,
    sendEstEmail,
    getDoctors,
    updateOverrideAmount,
    unlockApplication,
    downloadOneDrive,
    experianCIR,
    resendApplicationDoc,
    uploadAgreementViewDetails,
    planUploadAgreement,
    planReviewAgreementDetails,
    planRejectOrApprove,
    providerPlanList,
    providerPlanSubmit,
    downloadPlanAgreement,
    downloadDoc,
    creditApplicationListDownload,
    creditApplicationListDownloadXLS,
    cancelRefundDetails,
    cancelRefundDetailsDownload,
    editApplicationProvider,
    applicationDiskcleanup,
    planDoc,
    viewDisk,
    resendMoveDocDrive,
    verifyPlanAction,
    creditApplicationPlanReview,

};

function creditApplicationPlanReview() {

    let formData = {
        
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }

    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-review`, formData)
        .then(handleResponse);
}


function verifyPlanAction(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-verify-plan`, formData)
        .then(handleResponse);
}

/*
* Title :- Resend move doc on one drive
* Descrption :- this function use for resend application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- Desc 10, 2020
*/

function resendMoveDocDrive(id, docs) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = id;
    formData['docs'] = docs;
    formData['current_user_id'] = cid;

    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };

    /*let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }*/
    return axios.post(`${URL.APIURL}/api/creditapplication-move-documents`, formData, header)
        .then(handleResponse);
}

function viewDisk(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-disk-documents`, formData)
        .then(handleResponse);
}

/*
* Title :- PLan Documents
* Descrption :- this function use for get plan documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- Dec 09, 2019
*/

function planDoc(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-plan-documents`, formData)
        .then(handleResponse);
}

/*
* Title :- applicationDiskcleanup
* Descrption :- this function use for get application document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/
function applicationDiskcleanup(id, docs) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = id;
    formData['docs'] = docs;
    formData['current_user_id'] = cid;

    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };

    return axios.post(`${URL.APIURL}/api/creditapplication-disk-cleanup`, formData, header)
        .then(handleResponse);
    /*let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-disk-cleanup`, formData)
        .then(handleResponse);*/
}


/*
* Title :- editApplicationProvider
* Descrption :- this function use for edit application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 07, 2020
*/
function editApplicationProvider(formData) {
    let cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-edit-appliaction/`, requestOptions, header)
        .then(handleResponse);
}

function cancelRefundDetailsDownload(formData) {
    //console.log(formData)
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/cancel-refund-plan-details-pdf`, formData, header)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                        saveAs(pdfBlob, 'plan-details.pdf');
                        return result;
                    });

            }

        });
}

function cancelRefundDetails(ppid) {
    let formData = {
        params: {
            ppid: ppid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/cancel-refund-plan-details`, formData)
        .then(handleResponse);
}

function creditApplicationListDownloadXLS(status_id) {
    //console.log(formData)
    let moment = require('moment');
    let formData2 = {
        params: {
            status_id: status_id.status,
            filter_type: status_id.filter_type,
            year: status_id.year,
            month: status_id.month,
            quarter: status_id.quarter,
            start_date: status_id.created_start_date,
            end_date: status_id.created_end_date,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-list-xls`, formData2)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        let fileName = '';
                        let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');

                        fileName = 'Application List ' + dateTime;
                        const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
                        saveAs(pdfBlob, fileName + '.xlsx');
                        return result;
                    });

            }

        });
}

function creditApplicationListDownload(status_id) {
    //console.log(formData)
    let moment = require('moment');
    let formData2 = {
        params: {
            status_id: status_id.status,
            filter_type: status_id.filter_type,
            year: status_id.year,
            month: status_id.month,
            quarter: status_id.quarter,
            start_date: status_id.created_start_date,
            end_date: status_id.created_end_date,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }

    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-list-pdf`, formData2)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        let fileName = '';
                        let dateTime = moment().format('MM/DD/YYYY h:mm:ss a');

                        fileName = 'Application List ' + dateTime;
                        const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                        saveAs(pdfBlob, fileName + '.pdf');
                        return result;
                    });

            }

        });
}

function downloadDoc(item_id) {
    let cid = currentUserId();
    let formData = {
        item_id: item_id,
        current_user_id: cid
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/download-doc`, formData, header)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.file_path,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/octet-stream;' });
                        var ext = result.file_path.split(".");
                        ext = ext[(ext.length - 1)];
                        saveAs(pdfBlob, result.file_name + '.' + ext);
                        return result;
                    });

            }

        });

}

function downloadPlanAgreement(pp_id) {
    let cid = currentUserId();
    let formData = {
        pp_id: pp_id,
        current_user_id: cid
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/download-plan-agreement`, formData, header)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.file_path,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/octet-stream;' });
                        var ext = result.file_path.split(".");
                        ext = ext[(ext.length - 1)];
                        saveAs(pdfBlob, 'plan-generate-agreement.' + ext);
                        return result;
                    });

            }

        });

}

/*
* Title :- providerPlanSubmit
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/
function providerPlanSubmit(list) {
    let formData = {};
    let cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    formData['pp_id'] = list;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-submit-plan`, formData, header)
        .then(handleResponse);
}
/*
* Title :- providerPlanList
* Descrption :- this function use for get credit applicaton plans
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function providerPlanList() {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/provider-plan-list`, formData)
        .then(handleResponse);
}
/*
* Title :- planRejectOrApprove
* Descrption :- this function use for manual action
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/
function planRejectOrApprove(id, type, addData) {
    let cid = currentUserId();
    let formData = {
        current_user_id: cid,
        id: id,
        type: type,
        comment: addData.comment,/*
        approved_amt: addData.approved_amt,
        credit_score: addData.credit_score,
        manual_term: addData.manual_term,
        manual_interest: addData.manual_interest*/
    }
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/plan-reject-approve-actions/`, requestOptions, header)
        .then(handleResponse);
}

/*
* Title :- planReviewAgreementDetails
* Descrption :- this function use for get plan agreement
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/
function planReviewAgreementDetails(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/review-plan-agreement-detail`, formData)
        .then(handleResponse);
}

/*
* Title :- planUploadAgreement
* Descrption :- this function use for upload  agreement document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

function planUploadAgreement(formData) {
    let cid = currentUserId();
    //formData['current_user_id'] = cid;
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/upload-plan-agreement`, formData, header)
        .then(handleResponse);
}

/*
* Title :- uploadAgreementViewDetails
* Descrption :- this function use for get details for plan agreement
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 18, 2019
*/
function uploadAgreementViewDetails(planid) {
    let formData = {
        params: {
            planid: planid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-plan-agreement-view-details`, formData)
        .then(handleResponse);
}

/*
* Title :- Resend Documents
* Descrption :- this function use for resend application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

function resendApplicationDoc(id, docs) {
    let cid = currentUserId();
    let formData = {};
    formData['app_id'] = id;
    formData['docs'] = docs;
    formData['current_user_id'] = cid;

    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };

    /*let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }*/
    return axios.post(`${URL.APIURL}/api/resend-creditapplication-documents`, formData, header)
        .then(handleResponse);
}

/*
* Title :- experianCIR
* Descrption :- this function use for experian CIR and manual data
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 18, 2019
*/
function experianCIR(id) {
    let cid = currentUserId();
    let formData = {
        params: {
            id: id,
            current_user_id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-cir`, formData)
        .then(handleResponse);
}

/*
* Title :- downloadOneDrive
* Descrption :- this function use for get application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jan 21, 2019
*/

function downloadOneDrive(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-download-documents`, formData)
        .then(handleResponse);
}


function unlockApplication(app_id) {
    let cid = currentUserId();
    let formData = {};
    formData['app_id'] = app_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/application-unlock`, formData, header)
        .then(handleResponse);
}

/*
* Title :- eamilPlan
* Descrption :- this function use for send email to customer
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 16, 2019
*/
function sendEstEmail(data) {
    data.provider_id = providerID();
    /*let formData = {
        amount:loan_amount,
        score:score,
        plan_id : plan_id,
        provider_id : providerID(),
    }*/
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/plan-est-email`, data, header)
        .then(handleResponse);
}

/*
* Title :- eamilPlan
* Descrption :- this function use for send email to customer
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 16, 2019
*/
function eamilPlan(score, loan_amount, plan_id, procedure_date) {
    let formData = {
        amount: loan_amount,
        score: score,
        plan_id: plan_id,
        provider_id: providerID(),
        procedure_date,
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/plan-email`, formData, header)
        .then(handleResponse);
}

/*
* Title :- All Documents
* Descrption :- this function use for get application documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

function applicationDoc(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-all-documents`, formData)
        .then(handleResponse);
}

/*
* Title :- printEstimatePlan
* Descrption :- this function use for download estimate plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 18, 2019
*/
function printEstimatePlan(score, loan_amount, plan_id) {
    let formData = {
        amount: loan_amount,
        score: score,
        plan_id: plan_id,
        provider_id: providerID(),
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/print-estimate-plan`, formData, header)
        .then(handleResponse)
        .then((result) => {

            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.file_path,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/octet-stream;' });
                        var ext = result.file_path.split(".");
                        ext = ext[(ext.length - 1)];
                        saveAs(pdfBlob, 'estimate-payment-plan.' + ext);
                        return result;
                    });

            }

        });
}

/*
* Title :- applicationEmail
* Descrption :- this function use for get customer information according to application
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/
function applicationEmail(id, type) {
    let formData = {
        params: {
            id: id,
            type: type
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/application-email`, formData)
        .then(handleResponse);
}
/*
* Title :- applicationFinalDetails
* Descrption :- this function use for get customer information according to application
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/
function applicationFinalDetails(id) {
    let formData = {
        params: {
            id: id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/application-details`, formData)
        .then(handleResponse);
}

/*
* Title :- estimatePlan
* Descrption :- this function use for get estimate plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Sep 20, 2019
*/
function estimatePlan(score, amount, loan_type, monthly_amount) {
    let formData = {
        params: {
            amount: amount,
            score: score,
            loan_type: loan_type,
            monthly_amount: monthly_amount,
            provider_id: providerID(),
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/estimate-plan`, formData)
        .then(handleResponse);
}

/*
* Title :- updateAppExpireDate
* Descrption :- this function use for chnage application expiry date
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 23, 2019
*/
function updateAppExpireDate(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    console.log('formData')
    console.log(formData)
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/app-expire-date-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- updateOverrideAmount
* Descrption :- this function use for chnage application expiry date
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 23, 2019
*/
function updateOverrideAmount(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/app-overide-amount`, formData, header)
        .then(handleResponse);
}

/*
* Title :- downloadAgrement
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function printPlan(application_id, loan_amount, plan_id, procedure_date) {
    let formData = {
        application_id: application_id,
        loan_amount: loan_amount,
        plan_id: plan_id,
        procedure_date: procedure_date,
        provider_id: providerID(),
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/print-plan`, formData, header)
        .then(handleResponse)
        .then((result) => {

            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.file_path,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/octet-stream;' });
                        var ext = result.file_path.split(".");
                        ext = ext[(ext.length - 1)];
                        saveAs(pdfBlob, 'customer-payment-plan.' + ext);
                        return result;
                    });

            }

        });
}


/*
* Title :- applicationDetailsData
* Descrption :- this function use for get application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 13, 2019
*/
function applicationDetailsData(id) {
    let provider_id = providerID();
    let formData = {
        params: {
            id: id,
            provider_id: provider_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-details/`, formData)
        .then(handleResponse);
}

/*
* Title :- searchCustomerApplication
* Descrption :- this function use for get search customre details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/
function searchCustomerApplication(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customerapplication-search/`, requestOptions, header)
        .then(handleResponse);
}


/*
* Title :- downloadAgrement
* Descrption :- this function use for download provider invoice reports
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function downloadAgrement(data) {
    let formData = {
        application_id: data
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/download-agreement`, formData, header)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.result.file_path,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/octet-stream;' });
                        var ext = result.result.file_path.split(".");
                        ext = ext[(ext.length - 1)];
                        saveAs(pdfBlob, 'customer-agrement.' + ext);
                        return result;
                    });

            }

        });
}

/*
* Title :- uploadAgreement
* Descrption :- this function use for upload application aggrement
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

function uploadAgreement(formData) {
    let cid = currentUserId();
    //formData['current_user_id'] = cid;
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-agrement`, formData, header)
        .then(handleResponse);
}

/*
* Title :- uploadRejectionAgreement
* Descrption :- this function use for upload application aggrement
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 27, 2019
*/

function uploadRejectionAgreement(formData) {
    let cid = currentUserId();
    //formData['current_user_id'] = cid;
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-rejection-agrement`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getDoctors
* Descrption :- this function use for get Speciality according to provider location
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 27, 2019
*/
function getDoctors(id) {
    let provider_id = providerID();
    let formData = {
        params: {
            id: id,
            provider_id: provider_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-doctors/`, formData)
        .then(handleResponse);
}
/*
* Title :- getSpeciality
* Descrption :- this function use for get Speciality according to provider location
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 27, 2019
*/
function getSpeciality(id) {
    let provider_id = providerID();
    let formData = {
        params: {
            id: id,
            provider_id: provider_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-speciality/`, formData)
        .then(handleResponse);
}
/*
* Title :- creditApplicationReviewDocument
* Descrption :- this function use for get application document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/
function creditApplicationReviewDocument(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-getDocument`, formData)
        .then(handleResponse);
}
/*
* Title :- creditApplicationUploadDocument
* Descrption :- this function use for upload credit application document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

function creditApplicationUploadDocument(formData) {
    let cid = currentUserId();
    //formData['current_user_id'] = cid;
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-upload`, formData, header)
        .then(handleResponse);
}

/*
* Title :- rejectApplication
* Descrption :- this function use for get reject application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/
function rejectApplication(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-reject`, formData)
        .then(handleResponse);
}

/*
* Title :- creditApplicationManualActions
* Descrption :- this function use for manual action
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 19, 2019
*/
function creditApplicationManualActions(id, type, addData) {
    let cid = currentUserId();
    let formData = {
        current_user_id: cid,
        id: id,
        type: type,
        comment: addData.comment,
        approved_amt: addData.approved_amt,
        credit_score: addData.credit_score,
        manual_term: addData.manual_term,
        manual_interest: addData.manual_interest
    }
    /*formData['current_user_id'] = cid;
    formData['id'] = id;
    formData['type'] = type;
    formData['addData'] = addData;*/
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-manual-actions/`, requestOptions, header)
        .then(handleResponse);
    /*let formData = {
        params: {
            id: id,
            type: type
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-manual-actions`, formData)
        .then(handleResponse);*/
}

/*
* Title :- creditApplicationReview
* Descrption :- this function use for get details for review application
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 18, 2019
*/
function creditApplicationReview(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-review-application`, formData)
        .then(handleResponse);
}

/*
* Title :- submitSearchCustomer
* Descrption :- this function use for get search customre details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 13, 2019
*/
function submitSearchCustomer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-search/`, requestOptions, header)
        .then(handleResponse);
}

/*
* Title :- submitApplicationProvider
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jun 12, 2019
*/
function submitApplicationProvider(formData) {
    let cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-submit-provider/`, requestOptions, header)
        .then(handleResponse);
}

/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 11, 2019
*/
function creditApplicationListProvider(status_id) {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
            status_id: status_id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-list-provider`, formData)
        .then(handleResponse);
}

/*
* Title :- viewApplication
* Descrption :- this function use for view application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 6, 2019
*/
function viewApplication(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-view-application`, formData)
        .then(handleResponse);
}

/*
* Title :- planDetails
* Descrption :- this function use for get single plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 6, 2019
*/
function planDetails(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-singleplan-details`, formData)
        .then(handleResponse);
}

/*
* Title :- createPaymentPrintPlan
* Descrption :- this function use for create payment print plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function createPaymentPrintPlan(application_id, plan_id, details) {
    let formData = {}
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    formData['application_id'] = application_id;
    formData['plan_id'] = plan_id;
    formData['provider_id'] = providerID();
    formData['details'] = details;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/creditapplication-create-print-plan/`, requestOptions, header)
        .then(handleResponse)
        .then((result) => {
            if (result.file_status == 1) {

                let formData = {
                    params: {
                        file: result.file,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                        saveAs(pdfBlob, 'plan_generate_agreement.pdf');
                        return result;
                    });

            }

        });
}

/*
* Title :- createPaymentPlan
* Descrption :- this function use for create payment plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function createPaymentPlan(application_id, plan_id, details) {
    let formData = {}
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    formData['application_id'] = application_id;
    formData['plan_id'] = plan_id;
    formData['provider_id'] = providerID();
    formData['details'] = details;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/creditapplication-create-plan/`, requestOptions, header)
        .then(handleResponse);
}

/*
* Title :- paymentPlan
* Descrption :- this function use for get payment plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function paymentPlan(id, amount, loan_type, monthly_amount, procedure_date) {
    let formData = {
        params: {
            id: id,
            provider_id: providerID(),
            amount: amount,
            loan_type,
            monthly_amount,
            procedure_date,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-plan`, formData)
        .then(handleResponse);
}

/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function creditApplicationList(status_id) {
    //let formData;
    //if(status_id.filter_type) {
    let formData = {
        params: {
            status_id: status_id.status,
            filter_type: status_id.filter_type,
            year: status_id.year,
            month: status_id.month,
            quarter: status_id.quarter,
            start_date: status_id.created_start_date,
            end_date: status_id.created_end_date,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //} else {
    /*formData = {
      params: {
        status_id: (status_id.status) ? status_id.status : status_id,
      },
      headers: { "Authorization": `Bearer ${authHeader()}` }
    } */
    //}
    /*let formData = {
      params: {
        status_id: status_id,
      },
      headers: { "Authorization": `Bearer ${authHeader()}` }
    }*/

    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-list`, formData)
        .then(handleResponse);
}

/*
* Title :- submitApplication
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function submitApplication(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/creditapplication-submit/`, requestOptions, header)
        .then(handleResponse);
}
/*
* Title :- signinUserInApp
* Descrption :- this function use for get credit application option data
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function applicationOption() {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/creditapplication-option/`, formData)
        .then(handleResponse);
}

/*
* Title :- getStates
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function getStates(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-states/`, formData)
        .then(handleResponse);
}

/*
* Title :- getStates
* Descrption :- this function use for get state list according to country
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function getRegion(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-region/`, formData)
        .then(handleResponse);
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}