//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, providerID } from '../apifile';

export const dashboardService = {
    providerDashboardApplication,
    hpsDashboardApplication,
};

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function providerDashboardApplication() {
    let provider_id = providerID();
    let formData = {
        params: {
            provider_id:provider_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/provider-dashboard/`, formData)
        .then(handleResponse);
}

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function hpsDashboardApplication() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/dashboard/`, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
