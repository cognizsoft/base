//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const userRolesService = {
  getAll,  
  updateUserRoles,
  deleteUserRoles,
  insertUserRoles,
  checkRoleExist
};

function checkRoleExist(value,md_id,name) {
  let  cid = currentUserId();
  let formData = {};
  formData['value'] = value;
  formData['mdv_id'] = md_id;
  formData['name'] = name;
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-roles-check-exist`,formData, header)
    .then(handleResponse);
}


function getAll(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/user-roles`, header)
    .then(handleResponse);
}

function updateUserRoles(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-roles-update`,formData, header)
    .then(handleResponse);
}

function deleteUserRoles(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-roles-delete`,formData, header)
    .then(handleResponse);
}

function insertUserRoles(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/user-roles-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function getLastInsertId(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/user-roles-last-insert-id`, header)
    .then(handleResponse);
}

function getUserType(id){
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = {'user_role_id':id};
  return axios.post(`${URL.APIURL}/api/userroles/getUserRole`, requestOptions, header)
    .then(handleResponse)
    .then(usertypes => {
      return userroles;
    });
}



function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
