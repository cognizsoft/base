//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const statesService = {
    getAllstates,
    insertState,
    checkSNameExist,
    checkAbbreviationExist,
    updateState,
    getAllCountry
};


/*
* Title :- getAllCountry
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getAllCountry() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/state-country-list`, header)
        .then(handleResponse);
}

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getAllstates() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/state-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertState
* Descrpation :- This function use in add state details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 22,2019
*/
function insertState(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/state-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- checkNameExist
* Descrpation :- This function use in state name exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkSNameExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['name'] = value;
    formData['state_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/state-name-exist`, formData, header)
        .then(handleResponse);
}
/*
* Title :- checkAbbreviationExist
* Descrpation :- This function use in check state Abbreviation exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkAbbreviationExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['abbreviation'] = value;
    formData['state_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/state-abbreviation-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- updateUser
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function updateState(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/state-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
