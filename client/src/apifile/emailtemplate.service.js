//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const emailTempService = {
    getAllEmailTemp,
    insertEmailTemp,
    updateEmailTemp,
    deleteEmailTemp,
    checkEmailTempExist,
    getEmailTempVariable
};

/*
* Title :- getEmailTempVariable
* Descrpation :- This function use in get all email template list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getEmailTempVariable(temp_id) {
    let formData = {
        params: {
            temp_id:temp_id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/email-template-variable`, formData)
        .then(handleResponse);
}

/*
* Title :- checkEmailTempExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkEmailTempExist(value, id) {
    let cid = currentUserId();
    let formData = {};
    formData['template_name'] = value;
    formData['id'] = id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/email-template-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getAllEmailTemp
* Descrpation :- This function use in get all email template list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllEmailTemp() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/email-template-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertEmailTemp
* Descrpation :- This function use in add email template details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertEmailTemp(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/email-template-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- updateEmailTemp
* Descrpation :- This function use in update email template details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateEmailTemp(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/email-template-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- deleteEmailTemp
* Descrpation :- This function use in delete email template details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function deleteEmailTemp(value) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/email-template-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
