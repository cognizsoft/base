//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const riskFactorsService = {
  getAllRiskFactors,
  getAllRiskFactorMasterDataValue,
  updateRiskFactors,
  deleteRiskFactors,
  insertRiskFactors
};

function getAllRiskFactorMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/risk-factor-master-data-value`, header)
    .then(handleResponse);
}

function getAllRiskFactors(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/risk-factor`, header)
    .then(handleResponse);
}

function updateRiskFactors(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/risk-factor-update`,formData, header)
    .then(handleResponse);
}

function deleteRiskFactors(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/risk-factor-delete`,formData, header)
    .then(handleResponse); 
}

function insertRiskFactors(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/risk-factor-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
