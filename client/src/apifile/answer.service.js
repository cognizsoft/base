//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const answerService = {
    getAllAnswer,
    insertAnswer,
    updateAnswer,
    deleteAnswer,
    checkSecurityAnswerExist,
    getAllProviderAnswer,
    insertProviderAnswer,
    updateProviderAnswer,
    deleteProviderAnswer
};

////////////CUSTOMER///////////


/*
* Title :- deleteCustomerAnswer
* Descrpation :- This function use in delete question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function deleteCustomerAnswer(value) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-answer-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- updateCustomerAnswer
* Descrpation :- This function use in update question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateCustomerAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-answer-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- insertCustomerAnswer
* Descrpation :- This function use in add question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertCustomerAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/customer-answer-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- getAllCustomerAnswer
* Descrpation :- This function use in get all question list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllCustomerAnswer() {
    let formData = {
        params: {
            user_id: currentUserId()
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/customer-answer-list`, formData)
        .then(handleResponse);
}



///////////PROVIDER////////////

/*
* Title :- deleteProviderAnswer
* Descrpation :- This function use in delete question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function deleteProviderAnswer(value) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-answer-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- updateProviderAnswer
* Descrpation :- This function use in update question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateProviderAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-answer-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- insertProviderAnswer
* Descrpation :- This function use in add question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertProviderAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/provider-answer-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- getAllProviderAnswer
* Descrpation :- This function use in get all question list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllProviderAnswer() {
    let formData = {
        params: {
            user_id: currentUserId()
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/provider-answer-list`, formData)
        .then(handleResponse);
}

////////////ADMIN//////////////

/*
* Title :- getAllQuestion
* Descrpation :- This function use in get all question list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllAnswer() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/answer-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertAnswer
* Descrpation :- This function use in add question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/answer-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- updateAnswer
* Descrpation :- This function use in update question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateAnswer(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/answer-update`, formData, header)
        .then(handleResponse);
}

/*
* Title :- deleteAnswer
* Descrpation :- This function use in delete question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function deleteAnswer(value) {
    let cid = currentUserId();
    let formData = {};
    formData['id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/answer-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- checkSecurityAnswerExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkSecurityAnswerExist(question_id, user_id, id) {
    let cid = currentUserId();
    let formData = {};
    formData['question_id'] = question_id;
    formData['user_id'] = user_id;
    formData['id'] = id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/answer-exist`, formData, header)
        .then(handleResponse);
}


/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}




