//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const masterValuesService = {
  getAllMasterValue,
  getAllMasterDataValue,
  updateMasterValues,
  deleteMasterValues,
  insertMasterValues,
  checkMasterValueExist
};


function checkMasterValueExist(value,md_id,name) {
  let  cid = currentUserId();
  let formData = {};
  formData['value'] = value;
  formData['mdv_id'] = md_id;
  formData['name'] = name;
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/mastervalue-check-exist`,formData, header)
    .then(handleResponse);
}

function getAllMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/master-data-value`, header)
    .then(handleResponse);
}

function getAllMasterValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/master-value`, header)
    .then(handleResponse);
}

function updateMasterValues(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/master-value-update`,formData, header)
    .then(handleResponse);
}

function deleteMasterValues(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/master-value-delete`,formData, header)
    .then(handleResponse); 
}

function insertMasterValues(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/master-value-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
