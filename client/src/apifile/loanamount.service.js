//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const loanAmountService = {
  listLoanAmount,
  updateLoanAmount,
  deleteLoanAmount,
  insertLoanAmount,
  checkLoanAmountExist
};


function checkLoanAmountExist(value,md_id) {
  let  cid = currentUserId();
  let formData = {};
  formData['value'] = value;
  formData['mdv_id'] = md_id;
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-amount-check-exist`,formData, header)
    .then(handleResponse);
}

function listLoanAmount(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/loan-amount-list`, header)
    .then(handleResponse);
}

function updateLoanAmount(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-amount-update`,formData, header)
    .then(handleResponse);
}

function deleteLoanAmount(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/loan-amount-delete`,formData, header)
    .then(handleResponse);
}

function insertLoanAmount(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/loan-amount-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
