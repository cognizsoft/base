import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const paymentPlanService = {
    getPaymentMasterFeeOption,
    getLateFeeWaiverType,
    getFinChargeWaiverType,
    getSingleInstallment,
    insertPayPayment,
    sendReport,
    allPlanDetails,
    invoiceDetails,
    viewReceipt,
    createInvoice,
    adminPlanDetailsDownload,
    regeneratePlan,
    regeneratePlanProcess,
    viewOptionToClose,
    closeCurrentPlan,
    regeneratePrintPlan,
    getClosePlanDetails,
    confirmedCancellation,
};
/*
* Title :- confirmedCancellation
* Descrption :- this function use to confirmed cancellation
* Author :- Cogniz software & ramesh Kumar 
* Date :- Jun 6, 2020
*/
function confirmedCancellation(pp_id,provider_confirm,refund_type) {
    let cid = currentUserId();
    let formData = {
        params: {
            pp_id: pp_id,
            provider_confirm:provider_confirm,
            refund_type:refund_type,
            current_user_id: cid,
            provider_id:providerID(),
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/confirmed-cancellation`, formData)
        .then(handleResponse);
}

/*
* Title :- getClosePlanDetails
* Descrption :- this function use to get plan details
* Author :- Cogniz software & ramesh Kumar 
* Date :- May 5, 2020
*/
function getClosePlanDetails(pp_id) {
    let formData = {
        params: {
            pp_id: pp_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/close-plan-details`, formData)
        .then(handleResponse);
}

/*
* Title :- regeneratePrintPlan
* Descrption :- this function use for regeneratePrintPlan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/

function regeneratePrintPlan(data) {
    let cid = currentUserId();
    var formData = {
        current_user_id: cid,
        data: data,
    }
    //formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/regenerate-print-plan/`, requestOptions, header)
        .then(handleResponse)
        .then((result) => { 
            if(result.file_status == 1){
                
                let formData = {
                    params: {
                        file: result.file,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                .then((download) => { 
                    const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                    saveAs(pdfBlob, 'plan_regenerate_agreement.pdf');
                    return result;
                });
                
            }
            
        });

}

/*
* Title :- closeCurrentPlan
* Descrption :- this function use for close plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 20 April 2020
*/

function closeCurrentPlan(data) {
    let cid = currentUserId();
    var formData = {
        current_user_id: cid,
        data: data,
    }
    //formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/plan-action/`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- regeneratePlanProcess
* Descrption :- this function use for regenerate plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/
function viewOptionToClose(pp_id) {
    let formData = {
        params: {
            pp_id: pp_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/option-to-close-plan`, formData)
        .then(handleResponse);
}

/*
* Title :- regeneratePlanProcess
* Descrption :- this function use for regenerate plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/

function regeneratePlanProcess(data) {
    let cid = currentUserId();
    var formData = {
        current_user_id: cid,
        data: data,
    }
    //formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/regenerate-plan-process/`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- regeneratePlan
* Descrption :- this function use for regenerate plan
* Author :- Cogniz software & ramesh Kumar 
* Date :- 19 March 2020
*/

function regeneratePlan(data) {
    let cid = currentUserId();
    var formData = {
        current_user_id: cid,
        data: data,
    }
    //formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/regenerate-plan/`, requestOptions, header)
        .then(handleResponse);

}


function adminPlanDetailsDownload(appid, planid) {
    let formData = {};
    let cid = providerID();
    formData['provider_id'] = cid;
    formData['appid'] = appid;
    formData['planid'] = planid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/creditapplication-all-plan-details-pdf`, formData, header)
        .then(handleResponse)
        .then((result) => {
            if (result.status == 1) {
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                    .then((download) => {
                        const pdfBlob = new Blob([download.data], { type: 'application/pdf' });

                        if (result.type == 2) {
                            saveAs(pdfBlob, 'app-plan-details.pdf');
                        } else {
                            saveAs(pdfBlob, 'single-plan-details.pdf');
                        }
                        return result;
                    });

            }

        });
}

function createInvoice(date, appid) {
    let cid = currentUserId();
    var formData = {
        current_user_id: cid,
        date: date,
        appid: appid,
    }
    //formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/create-invoice`, requestOptions, header)
        .then(handleResponse);

}

function viewReceipt(app_id) {
    let formData = {
        params: {
            app_id: app_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/view-recepit`, formData)
        .then(handleResponse);
}
function invoiceDetails(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/application-plan-singleInvoice-payment-details`, formData)
        .then(handleResponse);
}

function allPlanDetails(appid) {
    let formData = {
        params: {
            appid: appid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-all-plan-details`, formData)
        .then(handleResponse);
}

function sendReport(invoice_msg, id) {
    let formData = {
        params: {
            invoice_msg: invoice_msg,
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/send-customer-invoice-report`, formData)
        .then(handleResponse);
}

function insertPayPayment(formData) {
    //console.log('service')
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/pay-installment-insert`, requestOptions, header)
        .then(handleResponse);

}

function getSingleInstallment(invoiceId) {
    let formData = {
        params: {
            invoiceId: invoiceId
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/application-plan-singleInstallment-payment-details`, formData)
        .then(handleResponse);
}

function getLateFeeWaiverType(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/payment-late-fee-waiver-type-option`, formData)
        .then(handleResponse);
}

function getFinChargeWaiverType(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/payment-fin-charge-waiver-type-option`, formData)
        .then(handleResponse);
}

function getPaymentMasterFeeOption(formData) {
    //console.log('formData')
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/payment-master-fee-option`, header)
        .then(handleResponse);
}

function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }   //return false;

}
