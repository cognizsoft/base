//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const CustomerSupportService = {
  customerAccountFilter,
  getQuestions,
  createSupportTicket,
  getAllTickets
};

function getAllTickets(data) {
    let formData = {
        params: {
            data: data,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/get-all-tickets`, formData)
        .then(handleResponse);
}
/*
* Title :- createSupportTicket
* Descrpation :- This function use use for create support ticket
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function createSupportTicket(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/create-support-ticket`, requestOptions, header)
    .then(handleResponse);
    
}

/*
* Title :- getQuestions
* Descrpation :- This function use in get getQuestions list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getQuestions(user_id) {
    
    let formData = {
        params: {
            user_id: user_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/get-user-questions`, formData)
        .then(handleResponse);
}
/*
* Title :- customerAccountFilter
* Descrption :- this function use for filter customer account filter details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function customerAccountFilter(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/admin-customer-account-filter`, formData, header)
    .then(handleResponse);
}



function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
