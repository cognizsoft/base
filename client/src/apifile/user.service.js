//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId, providerID } from '../apifile';

export const userService = {
    login,
    logout,
    getAllUsers,
    checkUsernameExist,
    insertUser,
    updateUser,
    checkEmailExist,
    deleteUser,
    changeUserPassword,
    getUserRole,
    getAllUsersProvider,
    insertUserProvider,
    updateUserProvider,
    forGetPasswordInApp,
    checkTokenInApp,
    resetPasswordInApp,
    updateAgreeFlag,
    unlockUserAccount,
    getLockUserDetail
};

/*
* Title :- getLockUserDetail
* Descrpation :- This function use in get getLockUserDetail list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getLockUserDetail(user_id) {
    
    let formData = {
        params: {
            user_id: user_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/lock-user-details`, formData)
        .then(handleResponse);
}

/*
* Title :- unlockUserAccount
* Descrpation :- This function use in unlockUserAccount any user record
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function unlockUserAccount(value) {
    let cid = currentUserId();
    let formData = {};
    formData['user_id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-unlock`, formData, header)
        .then(handleResponse);
}

function updateAgreeFlag(data,img) {
    let cid = currentUserId();
    console.log(cid)
    var formData = {}
    formData['flag'] = data;
    formData['signature'] = img;
    formData['providerID'] = providerID();
    formData['current_user_id'] = cid;
    console.log(formData)

    //return false
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-update-agree-flag`, formData, header)
        .then(handleResponse);
}

/*
* Title :- resetPasswordInApp
* Descrpation :- This function use in update password
* Author : Cognizsoft and Ramesh Kumar
* Date :- Oct 14,2019
*/
function resetPasswordInApp(formData) {
    
    return axios.post(`${URL.APIURL}/api/user-update-password`, formData)
        .then(handleResponse);
}

/*
* Title :- checkTokenInApp
* Descrpation :- This function use in check password token
* Author : Cognizsoft and Ramesh Kumar
* Date :- Oct 14,2019
*/
function checkTokenInApp(formData) {
    
    return axios.post(`${URL.APIURL}/api/user-check-token`, formData)
        .then(handleResponse);
}

/*
* Title :- forGetPasswordInApp
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- Oct 14,2019
*/
function forGetPasswordInApp(formData) {
    formData.base = window.location.origin.toString()+''+URL.SUB_PATH;
    return axios.post(`${URL.APIURL}/api/user-forget-password`, formData)
        .then(handleResponse);
}

/*
* Title :- updateUser
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- Jun 11,2019
*/
function updateUserProvider(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-update-provider`, formData, header)
        .then(handleResponse);
}
/*
* Title :- insertUserProvider
* Descrpation :- This function use in add new user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- Jun 11,2019
*/
function insertUserProvider(formData) {
    let cid = currentUserId();
    let client_id = providerID();
    formData['current_user_id'] = cid;
    formData['client_id'] = client_id;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/user-insert-provider`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- getAllUsersProvider
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- Jun 11,2019
*/
function getAllUsersProvider() {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/user-list-provider`, formData)
        .then(handleResponse);
}

/*
* Title :- getUserRole
* Descrpation :- This function use for get user role according to user type
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getUserRole(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/user-role`, formData)
        .then(handleResponse);

}

/*
* Title :- login
* Descrpation :- This function use in user login
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function login(formData) {
    const serverport = {
        username: formData.username,
        password: formData.password
    }
    return axios.post(`${URL.APIURL}/api/users/login`, serverport)
        .then(handleResponse)
        .then(user => {
            return user;
        });

}
/*
* Title :- logout
* Descrpation :- This function for destory current user session
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function logout() {
    localStorage.removeItem('user');
}

/*
* Title :- getAllUsers
* Descrpation :- This function use in get all user list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function getAllUsers() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/user-list`, header)
        .then(handleResponse);
}
/*
* Title :- checkUsernameExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkUsernameExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['username'] = value;
    formData['user_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-username-exist`, formData, header)
        .then(handleResponse);
}
/*
* Title :- checkEmailExist
* Descrpation :- This function use in check emial exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkEmailExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['username'] = value;
    formData['user_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-eamil-exist`, formData, header)
        .then(handleResponse);
}
/*
* Title :- deleteUser
* Descrpation :- This function use in delete any user record
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function deleteUser(value) {
    let cid = currentUserId();
    let formData = {};
    formData['user_id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-delete`, formData, header)
        .then(handleResponse);
}

/*
* Title :- insertUser
* Descrpation :- This function use in add new user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function insertUser(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/user-insert`, requestOptions, header)
        .then(handleResponse);

}
/*
* Title :- updateUser
* Descrpation :- This function use in update user details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function updateUser(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-update`, formData, header)
        .then(handleResponse);
}
/*
* Title :- changeUserPassword
* Descrpation :- This function use in change user password
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function changeUserPassword(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/user-password`, formData, header)
        .then(handleResponse);
}
/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function handleResponse(response) {

    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
