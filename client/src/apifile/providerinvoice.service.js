import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';
import { saveAs } from 'file-saver';

import axios from 'axios';
export const providerPendingService = {
    invoiceProviderPendingList,
    invoiceProviderSelectedPendingList,
    invoiceProviderreviewPDF,
    invoiceProviderInvoiceSubmit,
    invoiceProviderSubmitList,
    viewInvoice,
    viewSubmittedInvoice,
    refundInvoiceList,
    providerRefundAmount,
    refundAdminInvoiceList,
    adminRefundAmount,
};

/*
* Title :- adminRefundAmount
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 15, 2020
*/
function adminRefundAmount(data) {
    let formData = {};
    let  cid = currentUserId();
    formData['current_user_id'] = cid;
    formData['data'] = data;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/adminrefund-submitted-invoice`, formData, header)
        .then(handleResponse);
}

/*
* Title :- refundAdminInvoiceList
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 15, 2020
*/
function refundAdminInvoiceList(status_id) {
    
    let formData = {
        params: {
            iou_flag: status_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-refundinvoice-list`, formData)
        .then(handleResponse);
}

/*
* Title :- refundInvoiceList
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 12, 2020
*/
function providerRefundAmount(data) {
    let formData = {};
    let  cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    formData['data'] = data;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/refund-submitted-invoice`, formData, header)
        .then(handleResponse);
}

/*
* Title :- refundInvoiceList
* Descrption :- this function use for get refund invoice details
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 12, 2020
*/
function refundInvoiceList(status_id) {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
            iou_flag: status_id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/provider-refundinvoice-list`, formData)
        .then(handleResponse);
}

/*
* Title :- invoiceProviderSubmittedView
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function viewSubmittedInvoice(invoice_id,provider_id) {
    let formData = {};
    formData['provider_id'] = provider_id;
    formData['invoice_id'] = invoice_id;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/view-submitted-invoice`, formData, header)
        .then(handleResponse);
}

/*
* Title :- invoiceProviderSelectedPendingList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function viewInvoice(invoice_id,provider_id) {
    let formData = {};
    formData['provider_id'] = provider_id;
    formData['invoice_id'] = invoice_id;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/view-pdf`, formData, header)
        .then(handleResponse)
        .then((result) => { 
            if(result.status == 1){
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                .then((download) => { 
                    const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                    saveAs(pdfBlob, 'invoice.pdf');
                    return result;
                });
                
            }
            
        });
}

/*
* Title :- invoiceProviderSubmitList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/
function invoiceProviderSubmitList(status_id) {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
            status_id: status_id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/provider-submittedinvoice-list`, formData)
        .then(handleResponse);
}



/*
* Title :- invoiceProviderInvoiceSubmit
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 02, 2019
*/
function invoiceProviderInvoiceSubmit(list) {
    let formData = {};
    let  cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    formData['pp_id'] = list;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-submit-invoice`, formData, header)
        .then(handleResponse);
}



/*
* Title :- invoiceProviderSelectedPendingList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function invoiceProviderreviewPDF(list) {
    let formData = {};
    let cid = providerID();
    formData['provider_id'] = cid;
    formData['pp_id'] = list;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/create-pdf`, formData, header)
        .then(handleResponse)
        .then((result) => { 
            if(result.status == 1){
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                .then((download) => { 
                    const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
                    saveAs(pdfBlob, 'invoice.pdf');
                    return result;
                });
                
            }
            
        });
}

/*
* Title :- invoiceProviderSelectedPendingList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function invoiceProviderSelectedPendingList(list) {
    let formData = {};
    let cid = providerID();
    formData['provider_id'] = cid;
    formData['pp_id'] = list;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-selected-list`, formData, header)
        .then(handleResponse);
}


/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 24, 2019
*/
function invoiceProviderPendingList() {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/provider-invoice-list`, formData)
        .then(handleResponse);
}


/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}