import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const customerDashboardService = {
    customerActivePlanDetails,
    insertCustomerPayment,
    editCustomerProfileDetails,
    customerProfileGetStates,
    updateCustomerProfile,

    customerDashboardWeekMonthInvoiceReport,
    customerDashboardInvoiceReportFilter,
    customerDashboardInvoiceReportDownload,
    customerDashboardInvoiceReportDownloadXLS,
    customerPlanDetailsDownload,

    submitCustomerVerifyAmount
};

/*
* Title :- submitCustomerVerifyAmount
* Descrption :- this function use for verify bank account number
* Author :- Cogniz software & Aman
* Date :- Aug 23, 2019
*/
function submitCustomerVerifyAmount(formData) {
    console.log(formData)
    let  cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-patient-verify-amount`,formData, header)
      .then(handleResponse);
}

function customerPlanDetailsDownload(currentUserId, planid) {
    let formData = {};
    let cid = providerID();
    formData['provider_id'] = cid;
    formData['current_user_id'] = currentUserId;
    formData['planid'] = planid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-creditapplication-all-plan-details-pdf`, formData, header)
        .then(handleResponse)
        .then((result) => { 
            if(result.status == 1){
                let formData = {
                    params: {
                        file: result.result,
                    },
                    responseType: 'blob',
                    headers: { "Authorization": `Bearer ${authHeader()}` }
                }
                return axios.get(`${URL.APIURL}/api/fetch-pdf`, formData)
                .then((download) => { 
                    const pdfBlob = new Blob([download.data], { type: 'application/pdf' });

                    if(result.type == 2) {
                        saveAs(pdfBlob, 'active-app-plan-details.pdf');
                    } else {
                        saveAs(pdfBlob, 'single-active-app-plan-details.pdf');
                    }
                    
                    return result;
                });
                
            }
            
        });
}

function customerDashboardWeekMonthInvoiceReport() {
    //console.log(formData)
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/customer-dashboard-weekmonth-list`, header)
      .then(handleResponse);
  }
  
  function customerDashboardInvoiceReportFilter(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-dashboard-weekmonth-filter`, requestOptions, header)
      .then(handleResponse);
  }
  
  function customerDashboardInvoiceReportDownload(formData) {
    //console.log(formData)
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-dashboard-weeklt-monthly-invoice-pdf`, formData, header)
      .then(handleResponse)
      .then((result) => {
        if (result.status == 1) {
          let formData = {
            params: {
              file: result.result,
            },
            responseType: 'blob',
            headers: { "Authorization": `Bearer ${authHeader()}` }
          }
          return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
            .then((download) => {
              const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
              saveAs(pdfBlob, 'customer-invoice.pdf');
              return result;
            });
  
        }
  
      });
  }
  
  
  function customerDashboardInvoiceReportDownloadXLS(formData) {
    //console.log(formData)
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-dashboard-weeklt-monthly-invoice-xls`, formData, header)
      .then(handleResponse)
      .then((result) => {
        if (result.status == 1) {
          let formData = {
            params: {
              file: result.result,
            },
            responseType: 'blob',
            headers: { "Authorization": `Bearer ${authHeader()}` }
          }
          return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
            .then((download) => {
              const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
              saveAs(pdfBlob, 'customer-invoice.xlsx');
              return result;
            });
  
        }
  
      });
  }
  

function updateCustomerProfile(formData) {
    let cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-profile-update/`, requestOptions, header)
        .then(handleResponse);
}

function customerProfileGetStates(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-states/`, formData)
        .then(handleResponse);
}

function editCustomerProfileDetails(customer_id) {
    let formData = {
        params: {
            id: customer_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/edit-customer-profile-details/`, formData)
        .then(handleResponse);
}

function insertCustomerPayment(formData) {
    console.log('service')
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/pay-customer-installment-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function customerActivePlanDetails(user_id) {
	//console.log("service"+user_id);
	//return false;
    let formData = {
        params: {
            id: user_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/customer-active-plan-details`, formData)
        .then(handleResponse);
}

function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}