//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const providerService = {
  getAllProviderTypeMasterDataValue,
  getProviderCountry,
  getProviderStates,
  getProviderRegion,
  getProviderOption,
  insertProvider,
  getAllProvider,
  getProviderDetails,
  deleteProvider,
  updateProvider,
  checkSsnExist,
  getSpclProcedure,
  citySuggestion,
  providerDocument,
  providerUploadDocument
};

/*
* Title :- providerDocument
* Descrption :- this function use for get provider documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

function providerDocument(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/admin-get-provider-documents`, formData)
        .then(handleResponse);
}

/*
* Title :- providerUploadDocument
* Descrption :- this function use for admin upload provider document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

function providerUploadDocument(formData) {
    let cid = currentUserId();
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-upload-provider-document`, formData, header)
        .then(handleResponse);
}

function citySuggestion(country_id, state_id) {
    let cid = currentUserId();
    let formData = {};
    formData['country_id'] = country_id;
    formData['state_id'] = state_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-city-suggestion`, formData, header)
        .then(handleResponse);
}

function getSpclProcedure(id, idx) {
  let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/provider-spl-procedure/`, formData)
        .then(handleResponse);
}

function checkSsnExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['ssn'] = value;
    formData['provider_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/ssn-exist`, formData, header)
        .then(handleResponse);
}

function updateProvider(id) {

    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/provider-update`, formData)
        .then(handleResponse);
}

function deleteProvider(value) {
    let cid = currentUserId();
    let formData = {};
    formData['provider_id'] = value;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-delete`, formData, header)
        .then(handleResponse);
}


function getProviderDetails(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/provider-details`, formData)
        .then(handleResponse);
}

function getAllProviderTypeMasterDataValue(formData) {
  //console.log('formData')
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/provider-type-master-data-value`, header)
    .then(handleResponse);
}

function getAllProvider(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/provider-list`, header)
    .then(handleResponse);
}

function getProviderCountry(formData) {
	const header = {headers: { "Authorization": `Bearer ${authHeader()}` }};
	return axios.get(`${URL.APIURL}/api/provider-country`, header)
		.then(handleResponse);
}

function getProviderStates(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-states/`, formData)
        .then(handleResponse);
}

function getProviderRegion(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/creditapplication-region/`, formData)
        .then(handleResponse);
}

function getProviderOption() {
  //console.log('formData')
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/provider-option`, header)
    .then(handleResponse);
}

function insertProvider(formData) {
	//console.log('insertProviderService')
  let  cid = currentUserId();
  //formData.current_user_id = cid;
  formData.append('current_user_id', cid);
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/provider-insert`, requestOptions, header)
    .then(handleResponse);
    
}


function updateProvider(formData) {
    let cid = currentUserId();
    //formData['current_user_id'] = cid;
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-update`, formData, header)
        .then(handleResponse);
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
