//import config from 'config';


import axios from 'axios';
import { URL } from './URL';
import { authHeader, currentUserId } from '../apifile';

export const systemModuleService = {
    getAllSystemModule,
    insertSystemModule,
    updateSystemModule,
    checkSystemModuleExist,
};

/*
* Title :- checkSystemModuleExist
* Descrpation :- This function use in username exist or not
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 19,2019
*/
function checkSystemModuleExist(value, sys_mod_id) {
    let cid = currentUserId();
    let formData = {};
    formData['description'] = value;
    formData['sys_mod_id'] = sys_mod_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/system-module-exist`, formData, header)
        .then(handleResponse);
}

/*
* Title :- getAllQuestion
* Descrpation :- This function use in get all question list
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function getAllSystemModule() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/system-module-list`, header)
        .then(handleResponse);
}

/*
* Title :- insertQuestion
* Descrpation :- This function use in add question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function insertSystemModule(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    const requestOptions = formData;
    return axios.post(`${URL.APIURL}/api/system-module-insert`, requestOptions, header)
        .then(handleResponse);

}

/*
* Title :- updateUser
* Descrpation :- This function use in update question details
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function updateSystemModule(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/system-module-update`, formData, header)
        .then(handleResponse);
}



/*
* Title :- handleResponse
* Descrpation :- This function use for handle api response 
* Author : Cognizsoft and Ramesh Kumar
* Date :- April 24,2019
*/
function handleResponse(response) {
    if (response.status !== 200) {
        if (response.status === 401) {
            logout();
        }
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data) || response.data;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }
}
