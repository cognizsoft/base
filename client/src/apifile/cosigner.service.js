import { authHeader, currentUserId, currentCoSignerId } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const cosignerService = {
  creditApplicationListCosigner,
  cosignerDashboardInvoiceReportFilter,
  cosignerDashboardInvoiceReportDownloadXLS,
  cosignerDashboardInvoiceReportDownload,
  viewCosigner,
  editCosignerProfileDetails,
  updateCosignerProfile,
};

/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :-  Oct 11, 2020
*/
function creditApplicationListCosigner() {
  let cid = currentCoSignerId();
  let formData = {
    params: {
      id: cid,
    },
    headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/creditapplication-list-co-signer`, formData)
    .then(handleResponse);
}

function cosignerDashboardInvoiceReportFilter(formData) {
  let cid = currentUserId();
  formData['current_user_id'] = cid;
  const requestOptions = formData;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/cosigner-dashboard-weekmonth-filter`, requestOptions, header)
    .then(handleResponse);
}

function cosignerDashboardInvoiceReportDownload(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/cosigner-dashboard-weeklt-monthly-invoice-pdf`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/pdf' });
            saveAs(pdfBlob, 'customer-invoice.pdf');
            return result;
          });

      }

    });
}


function cosignerDashboardInvoiceReportDownloadXLS(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/cosigner-dashboard-weeklt-monthly-invoice-xls`, formData, header)
    .then(handleResponse)
    .then((result) => {
      if (result.status == 1) {
        let formData = {
          params: {
            file: result.result,
          },
          responseType: 'blob',
          headers: { "Authorization": `Bearer ${authHeader()}` }
        }
        return axios.get(`${URL.APIURL}/api/fetch-loan-pdf`, formData)
          .then((download) => {
            const pdfBlob = new Blob([download.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;' });
            saveAs(pdfBlob, 'customer-invoice.xlsx');
            return result;
          });

      }

    });
}

/*
* Title :- viewCosigner
* Descrption :- this function use get co-signer details
* Author :- Cogniz software & ramesh Kumar 
* Date :- Oct 19, 2020
*/
function viewCosigner(id) {
  let formData = {
    params: {
      id: id,
    },
    headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  return axios.get(`${URL.APIURL}/api/cosigner-view`, formData)
    .then(handleResponse);
}

function editCosignerProfileDetails(customer_id) {
  let formData = {
      params: {
          id: customer_id,
      },
      headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  return axios.get(`${URL.APIURL}/api/edit-cosigner-profile-details/`, formData)
      .then(handleResponse);
}

function updateCosignerProfile(formData) {
  let cid = currentUserId();
  let provider_id = currentCoSignerId();
  formData['current_user_id'] = cid;
  formData['provider_id'] = provider_id;
  const requestOptions = formData;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/cosigner-profile-update/`, requestOptions, header)
      .then(handleResponse);
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :-  Oct 11, 2020
*/
function handleResponse(response) {
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}