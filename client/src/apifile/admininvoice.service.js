import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';
import { saveAs } from 'file-saver';

import axios from 'axios';
export const adminInvoiceService = {
    invoiceAdminOpenList,
    viewInvoiceDetails,
    deleteInvoiceApplication,
    listInvoiceApplication,
    addInvoiceApplication,
    cancelInvoiceApplication,
    confirmInvoicesSubmit,
    invoiceAdminCloseList,
    cancelInvoiceConfirmations,
    viewInvoiceNotes,
    providerInvoiceView,
    approveInvoice
};

/*
* Title :- approveInvoice
* Descrption :- this function use for approve invoice application
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function approveInvoice(invoice_id) {
    let cid = currentUserId();
    let formData = {
        params: {
            invoice_id: invoice_id,
            current_user_id: cid
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-approve-invoice`, formData)
        .then(handleResponse);
}

/*
* Title :- providerInvoiceView
* Descrption :- this function use for view invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 18, 2019
*/
function providerInvoiceView(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/provider-view-invoice`, formData)
        .then(handleResponse);
}

/*
* Title :- viewInvoiceNotes
* Descrption :- this function use for view invoice note
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function viewInvoiceNotes(invoice_id) {
    let cid = currentUserId();
    let formData = {
        params: {
            invoice_id: invoice_id,
            current_user_id: cid
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-invoice-notes/`, formData)
        .then(handleResponse);
}

/*
* Title :- cancelInvoiceConfirmations
* Descrption :- this function use for cancel invoice connfirmations
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function cancelInvoiceConfirmations(invoice_id) {
    let cid = currentUserId();
    let formData = {
        params: {
            invoice_id: invoice_id,
            current_user_id: cid
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-cancel-confirmations`, formData)
        .then(handleResponse);
}


/*
* Title :- invoiceAdminCloseList
* Descrption :- this function use for get all close invoice for admin
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function invoiceAdminCloseList() {
    let cid = providerID();
    let formData = {
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-close-invoice`, formData)
        .then(handleResponse);
}

/*
* Title :- confirmInvoiceSubmit
* Descrption :- this function use for confirm invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 15, 2019
*/
function confirmInvoicesSubmit(invoiceData) {
    let cid = currentUserId();
    invoiceData.current_user_id = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-confirm-invoice`, invoiceData, header)
        .then(handleResponse);
}


/*
* Title :- listInvoiceApplication
* Descrption :- this function use for cancel invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function cancelInvoiceApplication(invoice_id, commentNote) {
    let cid = currentUserId();
    let formData = {
        invoice_id: invoice_id,
        commentNote:commentNote,
        current_user_id: cid
    }
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-cancel-invoice`, formData, header)
        .then(handleResponse);
    /*let formData = {
        params: {
            invoice_id: invoice_id,
            current_user_id: cid
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-cancel-invoice`, formData)
        .then(handleResponse);*/
}

/*
* Title :- deleteInvoiceApplication
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function addInvoiceApplication(applicationId, invoice_id) {
    let formData = {};
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    formData['invoice_id'] = invoice_id;
    formData['pp_id'] = applicationId;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-add-application`, formData, header)
        .then(handleResponse);
}

/*
* Title :- listInvoiceApplication
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function listInvoiceApplication(provider_id) {
    let cid = providerID();
    let formData = {
        params: {
            provider_id: provider_id
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-list-application`, formData)
        .then(handleResponse);
}

/*
* Title :- deleteInvoiceApplication
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function deleteInvoiceApplication(pp_id, invoice_id, provider_id, comment) {
    let cid = currentUserId();
    let formData = {
        pp_id: pp_id,
        invoice_id: invoice_id,
        provider_id: provider_id,
        current_user_id: cid,
        comment: comment
    }

    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-delete-application`, formData, header)
        .then(handleResponse);

    /*let cid = currentUserId();
    let formData = {
        params: {
            pp_id: pp_id,
            invoice_id: invoice_id,
            provider_id: provider_id,
            current_user_id: cid
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-delete-application`, formData)
        .then(handleResponse);*/

}

/*
* Title :- viewInvoiceDetails
* Descrption :- this function use for view particuler invoice
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function viewInvoiceDetails(invoice_id) {
    let cid = providerID();
    let formData = {
        params: {
            invoice_id: invoice_id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-view-invoice`, formData)
        .then(handleResponse);
}

/*
* Title :- invoiceAdminOpenList
* Descrption :- this function use for get all open invoice for admin
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 03, 2019
*/
function invoiceAdminOpenList() {
    let cid = providerID();
    let formData = {
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/admin-open-invoice`, formData)
        .then(handleResponse);
}


/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}