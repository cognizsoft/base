import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const customerService = {
    customerList,
    viewCustomer,
    customerListProvider,
    submitVerifyAmount,
    customerInvoicepPause,
    listInvoicepPause,
    customerInvoicepPauseUpdate,
    customerUploadDocument,
    customerDocument,
    customerAppUploadDocument
};

/*
* Title :- customerAppUploadDocument
* Descrption :- this function use for provider upload patient document according to application
* Author :- Cogniz software so & Ramesh 
* Date :- Dec 17, 2019
*/

function customerAppUploadDocument(formData) {
    let cid = currentUserId();
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/provider-upload-patient-document`, formData, header)
        .then(handleResponse);
}

/*
* Title :- customerDocument
* Descrption :- this function use for get customer documents
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 06, 2019
*/

function customerDocument(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/admin-get-patient-documents`, formData)
        .then(handleResponse);
}

/*
* Title :- customerUploadDocument
* Descrption :- this function use for admin upload patient document
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 20, 2019
*/

function customerUploadDocument(formData) {
    let cid = currentUserId();
    formData.append('current_user_id', cid);
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/admin-upload-patient-document`, formData, header)
        .then(handleResponse);
}

/*
* Title :- customerInvoicepPause
* Descrption :- this function use for add invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 21, 2019
*/
function customerInvoicepPauseUpdate(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-invoice-pause-update/`, formData, header)
        .then(handleResponse);
}

/*
* Title :- customerInvoicepPause
* Descrption :- this function use for add invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 23, 2019
*/
function listInvoicepPause(id) {
    
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/customer-invoice-pause-list/`, formData)
        .then(handleResponse);
}

/*
* Title :- customerInvoicepPause
* Descrption :- this function use for add invoice pause
* Author :- Cogniz software so & Aman 
* Date :- April 23, 2019
*/
function customerInvoicepPause(formData) {
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/customer-invoice-pause/`, formData, header)
        .then(handleResponse);
}


/*
* Title :- submitVerifyAmount
* Descrption :- this function use for verify bank account number
* Author :- Cogniz software & Aman
* Date :- Aug 23, 2019
*/
function submitVerifyAmount(formData) {
    console.log(formData)
    let cid = currentUserId();
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/patient-verify-amount`, formData, header)
        .then(handleResponse);
}

/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function customerListProvider() {
    let cid = providerID();
    let formData = {
        params: {
            id: cid,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/customer-list-provider/`, formData)
        .then(handleResponse);
}

/*
* Title :- creditApplicationList
* Descrption :- this function use for get credit applicaton
* Author :- Cogniz software & ramesh Kumar 
* Date :- June 6, 2019
*/
function customerList() {
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/customer-list`, header)
        .then(handleResponse);
}

/*
* Title :- viewApplication
* Descrption :- this function use for view application details
* Author :- Cogniz software & ramesh Kumar 
* Date :- July 6, 2019
*/
function viewCustomer(id) {
    let formData = {
        params: {
            id: id,
        },
        headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/customer-view`, formData)
        .then(handleResponse);
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}