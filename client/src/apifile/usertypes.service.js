//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const userTypesService = {
  getAll,
  getUserType,
  updateUserTypes,
  deleteUserTypes,
  insertUserTypes,
  getLastInsertId,
  checkUserTypeExist
};


function checkUserTypeExist(value,md_id) {
  let  cid = currentUserId();
  let formData = {};
  formData['value'] = value;
  formData['mdv_id'] = md_id;
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-types-check-exist`,formData, header)
    .then(handleResponse);
}

function getAll(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/user-types`, header)
    .then(handleResponse);
}

function updateUserTypes(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-types-update`,formData, header)
    .then(handleResponse);
}

function deleteUserTypes(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/user-types-delete`,formData, header)
    .then(handleResponse);
}

function insertUserTypes(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/user-types-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function getLastInsertId(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/user-types-last-insert-id`, header)
    .then(handleResponse);
}

function getUserType(id){
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = {'user_type_id':id};
  return axios.post(`${URL.APIURL}/api/usertypes/getUserType`, requestOptions, header)
    .then(handleResponse)
    .then(usertypes => {
      return usertypes;
    });
}



function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
