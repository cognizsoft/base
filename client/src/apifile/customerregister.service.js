import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const customerRegisterService = {
	signUpCustomerInApp,
	customerDetailVerify,
	updateCustomerFlag,

    getAllProviderTypeMasterDataValue,
    getProviderCountry,
    getProviderStates,
    getProviderOption,
    insertProvider,
    getSpclProcedure,
    getStates,
    checkUsernameExist,
    checkProviderNameExist,
    checkSsnExist,

    applicationOption,
    appGetStates,
    submitApplicationProvider,
    getDataFromAccessRequest,
    DirectcheckUsernameExistCo,
};

function getDataFromAccessRequest(id) {
    let formData = {
        params: {
            access_request_id: id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/get-data-access-request/`, formData)
        .then(handleResponse);
}

function submitApplicationProvider(formData) {
    let cid = currentUserId();
    let provider_id = providerID();
    formData['current_user_id'] = cid;
    formData['provider_id'] = provider_id;
    const requestOptions = formData;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/direct-creditapplication-submit-provider/`, requestOptions)
        .then(handleResponse);
}

function appGetStates(id) {
    let formData = {
        params: {
            id: id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/direct-creditapplication-states/`, formData)
        .then(handleResponse);
}

function applicationOption(access_request_id) {
    let cid = providerID();
    let formData = {
        params: {
            access_request_id: access_request_id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.get(`${URL.APIURL}/api/direct-creditapplication-option/`, formData)
        .then(handleResponse);
}

//////////
//////////

function checkSsnExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['ssn'] = value;
    formData['provider_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/direct-ssn-exist`, formData)
        .then(handleResponse);
}

function checkUsernameExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['username'] = value;
    formData['user_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/direct-user-username-exist`, formData)
        .then(handleResponse);
}

function DirectcheckUsernameExistCo(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['username'] = value;
    formData['user_id'] = md_id;
    formData['current_user_id'] = cid;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/direct-user-username-exist`, formData)
        .then(handleResponse);
}

function checkProviderNameExist(value, md_id) {
    let cid = currentUserId();
    let formData = {};
    formData['provider_name'] = value;
    const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
    return axios.post(`${URL.APIURL}/api/direct-provider-name-exist`, formData)
        .then(handleResponse);
}

function getStates(id) {
    let formData = {
        params: {
            id: id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/direct-creditapplication-states/`, formData)
        .then(handleResponse);
}

function insertProvider(formData) {
    
  let  cid = currentUserId();
  //formData.current_user_id = cid;
  formData.append('current_user_id', cid);
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/direct-provider-insert`, requestOptions)
    .then(handleResponse);
    
}

function getSpclProcedure(id, idx) {
  let formData = {
        params: {
            id: id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/direct-provider-spl-procedure/`, formData)
        .then(handleResponse);
}

function getProviderOption(chk, access_request_id) {
  
  let formData = {
        params: {
            access_request_id: access_request_id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
  //const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/direct-provider-option`, formData)
    .then(handleResponse);
}

function getProviderStates(id) {
    let formData = {
        params: {
            id: id,
        },
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/direct-provider-creditapplication-states/`, formData)
        .then(handleResponse);
}

function getProviderCountry(formData) {
    const header = {headers: { "Authorization": `Bearer ${authHeader()}` }};
    return axios.get(`${URL.APIURL}/api/direct-provider-country`)
        .then(handleResponse);
}

function getAllProviderTypeMasterDataValue(formData) {
  
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/direct-provider-type-master-data-value`)
    .then(handleResponse);
}

function updateCustomerFlag(formData) {
    return axios.post(`${URL.APIURL}/api/customer-flag-update`, formData)
        .then(handleResponse);
}

function customerDetailVerify(id) {
    let formData = {
        params: {
            id: id
        }
        //headers: { "Authorization": `Bearer ${authHeader()}` }
    }
    return axios.get(`${URL.APIURL}/api/verify-detail`, formData)
        .then(handleResponse);
}

function signUpCustomerInApp(formData) {
	let  cid = currentUserId();
  	formData['current_user_id'] = cid;

  	const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  	const requestOptions = formData;
  	return axios.post(`${URL.APIURL}/api/customer-signup`, requestOptions)
    .then(handleResponse);
}



function handleResponse(response) {
    if (response.status !== 200) {
        const error = (response.data && response.data.result.error) || response.statusText;
        return Promise.reject(error);
    } else if (response.data.status === 0) {
        const error = (response.data && response.data.message) || response.data.message;
        return Promise.reject(error);
    } else {
        const data = response.data;
        return data;
    }	//return false;

}