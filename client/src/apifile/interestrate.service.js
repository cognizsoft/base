//import config from 'config';
import { authHeader, currentUserId } from '../apifile';
import {URL} from './URL';

import axios from 'axios';
export const interestRateService = {
  getAllInterestRate,
  getAllInterestRateMasterDataValue,
  updateInterestRate,
  insertInterestRate,

  checkInterest_RateExist,
  listInterest_Rate,
  updateInterest_Rate,
  deleteInterest_Rate,
  insertInterest_Rate
};


  /////////////////////////////////////////////////////////////////////
 //////////////////////////////START//////////////////////////////////
/////////////////////////////////////////////////////////////////////


function checkInterest_RateExist(value,id) {
  let  cid = currentUserId();
  let formData = {};
  formData['value'] = value;
  formData['id'] = id;
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/interest_rate_check_exist`,formData, header)
    .then(handleResponse);
}

function listInterest_Rate(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/interest_rate_list`, header)
    .then(handleResponse);
}

function updateInterest_Rate(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/interest_rate_update`,formData, header)
    .then(handleResponse);
}

function deleteInterest_Rate(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/interest_rate_delete`,formData, header)
    .then(handleResponse);
}

function insertInterest_Rate(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/interest_rate_insert`, requestOptions, header)
    .then(handleResponse);
    
}


  /////////////////////////////////////////////////////////////////////
 /////////////////////////////END/////////////////////////////////////
/////////////////////////////////////////////////////////////////////


function getAllInterestRateMasterDataValue(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/interest-rate-master-data-value`, header)
    .then(handleResponse);
}

function getAllInterestRate(formData) {
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.get(`${URL.APIURL}/api/interest-rate`, header)
    .then(handleResponse);
}

function updateInterestRate(formData) {
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  //console.log(formData)
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  return axios.post(`${URL.APIURL}/api/interest-rate-update`,formData, header)
    .then(handleResponse);
}

function insertInterestRate(formData) {
  console.log(formData)
  let  cid = currentUserId();
  formData['current_user_id'] = cid;
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;
  return axios.post(`${URL.APIURL}/api/interest-rate-insert`, requestOptions, header)
    .then(handleResponse);
    
}

function handleResponse(response) {
  //console.log(response)
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}
