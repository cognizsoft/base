import { authHeader, currentUserId, providerID } from '../apifile';
import { URL } from './URL';

import axios from 'axios';
export const careCouldService = {
  requestToken,
  searchThirdParty,
  viewCareCouldPatient,
  careCouldDetails,
  getCareCouldDetails,
  MFSDetails,
};

/*
* Title :- getCareCouldDetails
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function MFSDetails() {
  let cid = providerID();
  let formData = {
    params: {
      id: cid,
    },
    headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  return axios.get(`${URL.APIURL}/api/mfs-cdetails/`, formData)
    .then(handleResponse);
}

/*
* Title :- getCareCouldDetails
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function getCareCouldDetails() {
  let cid = providerID();
  let formData = {
    params: {
      id: cid,
    },
    headers: { "Authorization": `Bearer ${authHeader()}` }
  }
  return axios.get(`${URL.APIURL}/api/provider-caredetails-list/`, formData)
    .then(handleResponse);
}
/*
* Title :- careCouldDetails
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function careCouldDetails(formData) {
  let cid = providerID();
  formData.provider_id = cid;
  formData.current_user_id = currentUserId();
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };


  return axios.post(`${URL.APIURL}/api/provider-caredetails/`, formData, header)
    .then(handleResponse);
}


/*
* Title :- searchThirdParty
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function viewCareCouldPatient(id) {
  var tokenDetails = JSON.parse(localStorage.getItem('applicationToken'));
  const header = { headers: { "Authorization": tokenDetails.access_token, } };
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  return axios.get(`${proxyurl}https://api.carecloud.com/v2/patients/${id}`, header)
    .then(handleResponse);
  /*const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;

  return axios.post(`${URL.APIURL}/api/search-application/`, requestOptions, header)
      .then(handleResponse);*/
}

/*
* Title :- searchThirdParty
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function searchThirdParty(formData) {
  let cid = currentUserId();
  formData['current_user_id'] = cid;
  formData['token_details'] = JSON.parse(localStorage.getItem('applicationToken'));
  var tokenDetails = JSON.parse(localStorage.getItem('applicationToken'));
  const header = { headers: { "Authorization": tokenDetails.access_token, } };
  
  var requestData = { "fields": {} }
  /*"fields": {
    "first_name": "LAUREN",
    "last_name": "TOO",
    "dob": "01-01-1980",
    "gender": "f",
    "phone_number": "561-789-6862"
  }
}*/
  if (formData.exist_first_name) {
    requestData.fields.first_name = formData.exist_first_name;
  }
  if (formData.exist_last_name) {
    requestData.fields.last_name = formData.exist_last_name;
  }
  if (formData.exist_dob) {
    requestData.fields.dob = formData.exist_dob;
  }
  if (formData.gender) {
    requestData.fields.gender = formData.gender;
  }
  if (formData.phone_no) {
    requestData.fields.phone_number = "561-789-6862";//formData.phone_no;
  }
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  return axios.post(`${proxyurl}https://api.carecloud.com/v2/patients/search`, requestData, header)
    .then(handleResponse);
  /*const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;

  return axios.post(`${URL.APIURL}/api/search-application/`, requestOptions, header)
      .then(handleResponse);*/
}

/*
* Title :- requestToken
* Descrption :- this function use for get careCould token
* Author :- Cogniz software & ramesh Kumar 
* Date :- Aug 30, 2019
*/
function requestToken(code, url,refreshToken) {
  let cid = currentUserId();
  var formData = {
    current_user_id: cid,
    provider_id: providerID(),
    code: code,
    url: url,
    refreshToken:refreshToken,
  }
  
  const header = { headers: { "Authorization": `Bearer ${authHeader()}` } };
  const requestOptions = formData;

  return axios.post(`${URL.APIURL}/api/request-token/`, requestOptions, header)
    .then(handleResponse);
}

/*
* Title :- signinUserInApp
* Descrption :- this function use for handle response data 
* Author :- Cogniz software & ramesh Kumar 
* Date :- 24 May 2019
*/
function handleResponse(response) {
  if (response.status !== 200) {
    const error = (response.data && response.data.result.error) || response.statusText;
    return Promise.reject(error);
  } else if (response.data.status === 0) {
    const error = (response.data && response.data.message) || response.data.message;
    return Promise.reject(error);
  } else {
    const data = response.data;
    return data;
  }	//return false;

}