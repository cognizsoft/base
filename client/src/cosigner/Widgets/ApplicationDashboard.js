/**
 * Support Request
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';

// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import DoughnutChart from 'Components/Charts/DoughnutChart';

// intl messagess
import IntlMessages from 'Util/IntlMessages';

class ApplicationDashboard extends Component {
   render() {
      return (
         <div className="support-widget-wrap">
            <div className="text-center py-10">
               <DoughnutChart />
            </div>
            <List className="list-unstyled p-0">
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsAccepted" /></p>
                  <Badge color="primary" className="px-4">570</Badge>
                  <IconButton color="default">
                     <i className="ti-eye"></i>
                  </IconButton>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsDeclined" /></p>
                  <Badge color="danger" className="px-4">50</Badge>
                  <IconButton color="default">
                     <i className="ti-eye"></i>
                  </IconButton>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsPending" /></p>
                  <Badge color="info" className="px-4">345</Badge>
                  <IconButton color="default">
                     <i className="ti-eye"></i>
                  </IconButton>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsNew" /></p>
                  <Badge color="primary" className="px-4">50</Badge>
                  <IconButton color="default">
                     <i className="ti-eye"></i>
                  </IconButton>
               </ListItem>
            </List>
            
         </div>
      );
   }
}

export default ApplicationDashboard;
