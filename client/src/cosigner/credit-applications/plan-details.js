/**
 * Invoice
 */
import React, { Component } from 'react';

import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import Button from '@material-ui/core/Button';
import { RctCard } from 'Components/RctCard/index';
//import VerifyCustomerForm from './verify-customer/VerifyCustomerForm';

import CryptoJS from 'crypto-js';
import Tooltip from '@material-ui/core/Tooltip';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import {
   planDetails, clearRedirectURL, allPlanDetails, adminPlanDetailsDownload, getLockUserDetail, createSupportTicket, getAllTickets,
} from 'Actions';
import MaterialDatatable from "material-datatable";
import AddCallLogButton from './support-history/AddCallLogButton';
import AddCallLogForm from './support-history/AddCallLogForm';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isEmpty, isMaster, isNumeric, isObjectEmpty } from '../../validator/Validator';
class PlanDetails extends Component {

   state = {
      openViewRefund: false,
      refundDetails: null,
      singleView: null,
      openInstallmentDialog: false,
      installmentDetails: null,

      ///Support History///
      addCallLogModal: false,
      addCallLogDetail: {
         ticket_source: 3,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_plan: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         follow_up_date_time: '',
         invoice_check: true
      },
      view_ticket: false,
      ticket_number: '',
      application_id: '',
      add_err: {},
      dateTimeStartDate: '',
      followUpDateTimeStartDate: '',
      invoiceGroup: null,
   }


   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   componentDidMount() {
      var data = {
         appid: this.dec(this.props.match.params.appid),
         filter_by: 1 // for filter in node file
      }
      this.props.clearRedirectURL();
      this.props.allPlanDetails(this.dec(this.props.match.params.appid));
      this.props.getAllTickets(data);
   }


   mainInvoice() {
      this.setState({ singleView: null })
   }

   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   goBack() {
      this.props.history.goBack(-1)
   }

   getMuiTheme = () => createMuiTheme({
      overrides: {

         MuiTooltip: {
            tooltip: {
               fontSize: "15px"
            }
         },
      }
   })

   refundDetails(details) {
      this.setState({ openViewRefund: true, refundDetails: details });
   }
   onViewRefundClose() {
      this.setState({ openViewRefund: false, refundDetails: null })
   }

   ////Support History////
   opnAddCallLogModal() {
      this.setState({ addCallLogModal: true });
   }
   onAddUpdateCallLogModalClose = () => {
      let addR = {}
      let addCallLogDetail = {
         ticket_source: 3,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_plan: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         follow_up_date_time: '',
         invoice_check: true
      };
      this.setState({ addCallLogModal: false, add_err: addR, addCallLogDetail: addCallLogDetail, view_ticket: false, ticket_number: '' })
   }
   addCallLogView() {
      const min = 1;
      const max = 100000000;
      const rand = Math.floor(Math.random() * (max - min + 1) + min)

      this.setState({ view_ticket: true, ticket_number: rand })
   }
   addCallLogViewBack() {
      this.setState({ view_ticket: false })
   }
   addCallLog() {
      this.state.addCallLogDetail.ticket_number = this.state.ticket_number
      this.state.addCallLogDetail.application_id = this.dec(this.props.match.params.appid);
      this.state.addCallLogDetail.caller_name = (this.props.application_details[0].f_name + ' ' + this.props.application_details[0].m_name + ' ' + this.props.application_details[0].l_name)
      this.state.addCallLogDetail.customer_id = this.props.application_details[0].patient_id
      this.state.addCallLogDetail.commented_by = 2 // by customer
      //console.log(this.state.addCallLogDetail)
      //return false;
      //insertTermMonth
      this.props.createSupportTicket(this.state.addCallLogDetail);


      this.setState({ addCallLogModal: false, loading: true });
      let self = this;
      let log = {
         ticket_source: 3,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_plan: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         follow_up_date_time: '',
         view_ticket: false,
         ticket_number: '',
         invoice_check: true
      }

      setTimeout(() => {
         self.setState({ loading: false, addCallLogDetail: log, view_ticket: false, ticket_number: '', dateTimeStartDate: '', followUpDateTimeStartDate: '' });
         NotificationManager.success('Ticket Created!');
      }, 2000);

   }
   onChangeAddCallLogDetails(key, value) {
      let { add_err, addCallLogDetail } = this.state;
      //console.log(value)
      switch (key) {
         case 'ticket_source':

            break;
         case 'call_type':

            break;
         case 'ticket_related_ac':
            value = (this.state.addCallLogDetail.ticket_related_ac) ? 0 : 1;

            break;
         case 'ticket_related_app':
            value = (this.state.addCallLogDetail.ticket_related_app) ? 0 : 1;

            break;
         case 'ticket_related_plan':
            value = (this.state.addCallLogDetail.ticket_related_plan) ? 0 : 1;
            if (value == 0) {
               addCallLogDetail['plan_no'] = ''
               addCallLogDetail['ticket_related_invoice'] = 0
               addCallLogDetail['invoice_check'] = true
               this.setState({ invoiceGroup: null })
            } else {
               addCallLogDetail['invoice_check'] = false
            }
            add_err['plan_no'] = 'Select Plan No.'
            break;
         case 'ticket_related_invoice':
            value = (this.state.addCallLogDetail.ticket_related_invoice) ? 0 : 1;
            if (value == 0) {
               addCallLogDetail['invoice_no'] = ''
               this.setState({ invoiceGroup: null })
            } else {
               addCallLogDetail['plan_no'] = ''
               add_err['plan_no'] = 'Select Plan No.'
            }
            add_err['invoice_no'] = 'Select Invoice No.'
            break;
         case 'plan_no':
            if (isEmpty(value)) {
               add_err[key] = "Select Plan No.";
               add_err['invoice_no'] = 'Select Invoice No.'
               this.setState({ invoiceGroup: null })
            } else {
               add_err[key] = '';
               addCallLogDetail['invoice_no'] = ''
               add_err['invoice_no'] = 'Select Invoice No.'
               var planInvoice = this.props.invoicePlan.filter(x => x.pp_id == value)
               this.setState({ invoiceGroup: planInvoice })
            }
            break;
         case 'invoice_no':
            if (isEmpty(value)) {
               add_err[key] = "Select Invoice No.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'caller_name':
            if (isEmpty(value)) {
               add_err[key] = "Caller Name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'call_date_time':
            if (value == null) {
               add_err[key] = "Select Date";
            } else {
               this.setState({ dateTimeStartDate: value })
               value = moment(value).format('YYYY-MM-DD h:mm:ss');
               add_err[key] = '';
            }
            break;
         case 'subject':
            if (isEmpty(value)) {
               add_err[key] = "Subject can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'hps_agent_name':
            if (isEmpty(value)) {
               add_err[key] = "Agent Name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'description':
            if (isEmpty(value)) {
               add_err[key] = "Description can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'follow_up':
            value = (this.state.addCallLogDetail.follow_up) ? 0 : 1;
            if (value == 0) {
               addCallLogDetail['follow_up_date_time'] = ''
               this.setState({ followUpDateTimeStartDate: '' })
            }
            add_err['follow_up_date_time'] = 'Select date'
            break;
         case 'follow_up_date_time':
            if (value == null) {
               add_err[key] = "Select Date";
            } else {
               this.setState({ followUpDateTimeStartDate: value })
               value = moment(value).format('YYYY-MM-DD h:mm:ss');
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });
      this.setState({
         addCallLogDetail: {
            ...this.state.addCallLogDetail,
            [key]: value
         }
      });
      //console.log(this.state.addCallLogDetail)
   }
   validateAddCallLogSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      let { addCallLogDetail } = this.state
      var common = true;

      common = (
         this.state.add_err.subject === '' &&
         this.state.add_err.description === ''
      ) ? true : false

      var count = 0;
      if (addCallLogDetail.ticket_related_ac == 1) {
         count++
      }
      if (addCallLogDetail.ticket_related_app == 1) {
         count++
      }
      if (addCallLogDetail.ticket_related_plan == 1) {
         var plan_select = (this.state.add_err.plan_no === '') ? true : false
         count++
      } else {
         var plan_select = true
      }
      if (addCallLogDetail.ticket_related_invoice == 1) {
         var invoice_select = (this.state.add_err.invoice_no === '') ? true : false
         count++
      } else {
         var invoice_select = true
      }

      return (count >= 1 && common == true && plan_select == true && invoice_select == true) ? true : false;




      return false
      return (/*
         this.state.add_err.ticket_source === '' &&
         this.state.add_err.call_type === '' &&*/
         this.state.add_err.caller_name === '' &&
         this.state.add_err.call_date_time === '' &&
         this.state.add_err.subject === '' &&
         this.state.add_err.hps_agent_name === '' &&
         this.state.add_err.description === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }
   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateUserDetails(name, value)
      });
   }

   render() {
      //console.log(this.state)
      const { verify_err, selectedVerifyUserQuestions } = this.state;
      const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;

      let totalLateAmount = 0;
      let totalFinCharge = 0;
      //var uniqueLoanAmount = application_plans && [...new Set(application_plans.map(item => item.amount))];
      var uniqueLoanAmount = 0;
      var uniqueRemainingAmount = 0;
      application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
            uniqueLoanAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.loan_amount : 0;
            uniqueRemainingAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.remaining_amount : 0;
         }
         return accumulator;
      }, []);
      //var uniqueLoanAmount = amountDetails && [...new Set(amountDetails.map(item => item.amount_rcvd))];
      var uniqueLateCount = 0;//application_plans && [...new Set(application_plans.map(item => item.late_count))];
      var uniqueMissedCount = application_plans && [...new Set(application_plans.map(item => item.missed_count))];


      // get plan details group by

      var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            // get recived amount
            var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
               if (planindex == 0) {
                  accumulator = 0;
               }
               accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
               return accumulator;
            }, 0);
            accumulator[currentValue.pp_id] = {
               paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.plan_status, invoice_exist: currentValue.invoice_exist, note: currentValue.note, status_name: currentValue.status_name,
               refund_comment: currentValue.refund_comment,
               ach_account_no: currentValue.ach_account_no,
               ach_routing_no: currentValue.ach_routing_no,
               ach_bank_name: currentValue.ach_bank_name,
               check_date: currentValue.check_date,
               check_no: currentValue.check_no,
               status_name: currentValue.status_name,
               refund_amt: currentValue.refund_amt,
               payment_method: currentValue.payment_method,
               refundmethod: currentValue.refundmethod,
               refund_id: currentValue.refund_id,

               pro_refund_comment: currentValue.pro_refund_comment,
               pro_ach_account_no: currentValue.pro_ach_account_no,
               pro_ach_routing_no: currentValue.pro_ach_routing_no,
               pro_ach_bank_name: currentValue.pro_ach_bank_name,
               pro_check_date: currentValue.pro_check_date,
               pro_check_no: currentValue.pro_check_no,
               pro_status_name: currentValue.pro_status_name,
               pro_refund_amt: currentValue.pro_refund_amt,
               pro_payment_method: currentValue.pro_payment_method,
               pro_refundmethod: currentValue.pro_refundmethod,
               iou_flag: currentValue.iou_flag,

            };
         } else {
            //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
         }

         return accumulator;
      }, []);
      var groupArrays = invoiceDetails && invoiceDetails.map((data, idx) => {
         var nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id == data.invoice_id);
         nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id > data.invoice_id && x.pp_id == nextDetailsDetails[0].pp_id);
         data.nextDeferred = 0;
         if (nextDetailsDetails.length > 0) {
            var checkNextInvoice = nextDetailsDetails.reduce(function (a, b) {
               return new Date(a.due_date) < new Date(b.due_date) ? a : b;
            });
            if (Object.keys(checkNextInvoice).length !== 0) {
               data.nextDeferred = 1;
            }
         }
         //console.log(invoiceDetails[idx+1])
         data.checkPartialPaid = (invoiceDetails.length > (idx + 1)) ? 1 : 0
         if (invoiceDetails[idx + 1] !== undefined && invoiceDetails[idx + 1].due_date == data.due_date) {
            data.checkPartialPaid = 0;
         }
         if (invoiceDetails[idx + 2] !== undefined || invoiceDetails[idx + 3] !== undefined || invoiceDetails[idx + 4] !== undefined) {
            data.checkPartialPaid = 1;
         }
         if (data.on_fly == 1 && nextDetailsDetails.length > 0 && data.invoice_status == 3 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else if (invoiceDetails.length > (idx + 1) && data.invoice_status != 1 && data.invoice_status != 4 && data.on_fly != 1 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else {
            data.missed_flag = 0;
         }

         //data.fin_charge_amt = (data.fin_charge_amt != null) ? (data.finpct != null) ? (parseFloat(data.fin_charge_amt) - (parseFloat(data.finpct) * parseFloat(datafin_charge_amt)) / 100) : parseFloat(data.fin_charge_amt) : 0;
         //data.late_fee_received = (data.late_fee_received != null) ? (data.latepct != null) ? (parseFloat(data.late_fee_received) - (parseFloat(data.latepct) * parseFloat(data.late_fee_received)) / 100) : parseFloat(data.late_fee_received) : 0;
         data.fin_charge_received = (data.invoice_status == 3) ? data.fin_charge_due : data.fin_charge_received;
         data.late_fee_received = (data.invoice_status == 3) ? data.late_fee_due : data.late_fee_received;

         //data.previous_fin_charge = (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
         //data.previous_late_fee = (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;
         data.previous_fin_charge = parseFloat(data.previous_fin_charge);
         data.previous_late_fee = parseFloat(data.previous_late_fee);

         data.total_due = data.payment_amount;
         //data.paid_amount1 = (data.invoice_status == 1 || data.invoice_status == 4) ? (data.paid_amount + data.fin_charge_received + data.late_fee_received + data.additional_amount + data.previous_fin_charge + data.previous_late_fee) : 0;
         data.paid_amount1 = (data.invoice_status == 1 || data.invoice_status == 4) ? (data.paid_amount + data.fin_charge_received + data.late_fee_received + data.additional_amount) : 0;
         data.total_due += (data.late_fee_received) ? data.late_fee_received : 0;
         data.total_due += (data.fin_charge_received) ? data.fin_charge_received : 0;

         data.total_due += (data.previous_late_fee && data.invoice_status == 3) ? data.previous_late_fee : 0;
         data.total_due += (data.previous_fin_charge && data.invoice_status == 3) ? data.previous_fin_charge : 0;

         if (idx != 0) {

            data.previous_blc += parseFloat(data.previous_fin_charge);
            data.previous_blc += parseFloat(data.previous_late_fee);
         }
         if (data.invoice_status == 1 || data.invoice_status == 4) {
            totalLateAmount += data.late_fee_received;// + data.previous_late_fee;
            totalFinCharge += data.fin_charge_received;// + data.previous_fin_charge; 
         }
         data.finalAmt = (data.paid_amount1 <= data.total_due) ? data.total_due - data.paid_amount1 : 0;
         if (data.invoice_status == 1 || data.invoice_status == 4) {
            data.paid_amount1 = data.paid_amount1 + data.credit_charge_amt;
         }
         if ((data.invoice_status == 2 || data.invoice_status == 3) && invoiceDetails[invoiceDetails.length - 1].due_date > data.due_date) {
            uniqueLateCount++;
         }
         return data;
      })
      var nextDueDate = '';
      if (invoiceDetails !== undefined && invoiceDetails.length > 0) {
         /*var getTrem = application_plans.reduce(function (a, b) {
            return a.payment_term_month < b.payment_term_month ? b : a;
         });
         if (getTrem.payment_term_month > invoiceDetails.length && (invoiceDetails[invoiceDetails.length - 1].invoice_status == 1 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 4 || moment(moment(invoiceDetails[invoiceDetails.length - 1].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')))) {
            //if (1) {

            var newDate = moment(invoiceDetails[invoiceDetails.length - 1].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
            newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
            nextDueDate = moment(newDate).format('MM/DD/YYYY');
         }*/
         var activePlan = application_plans.filter(function (data, idx) {

            if (data.payment_term_month > data.installments_count && data.plan_status == 1 && (invoiceDetails[invoiceDetails.length - 1].invoice_status == 0 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 1 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 5 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 4 || moment(moment(invoiceDetails[invoiceDetails.length - 1].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')))) {

               if (nextDueDate == '' && data.installments_count == data.payment_term_month) {
                  nextDueDate = '';
               } else if (nextDueDate != '' && new Date(nextDueDate) > new Date(data.last_plan_due) && data.last_plan_due !== null) {
                  nextDueDate = data.last_plan_due;
               } else if (nextDueDate == '' && data.last_plan_due !== null) {
                  nextDueDate = data.last_plan_due;
               }
            }
         });
         //console.log(nextDueDate)
         if (nextDueDate) {
            var newDate = moment(nextDueDate, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
            newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
            nextDueDate = moment(newDate).format('MM/DD/YYYY');
         }

      } else {
         if (application_plans) {
            var activePlan = application_plans.filter(x => x.status == 1 && x.provider_invoice_status == 4);
            if (activePlan.length > 0) {
               var nextInvoice = activePlan.reduce(function (a, b) {
                  return new Date(a.procedure_date) < new Date(b.procedure_date) ? a : b;
               });
               if (moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('DD') > 15) {
                  var newDate = moment(nextInvoice.procedure_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                  newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
                  nextDueDate = newDate;
               } else {
                  var newDate = new Date(moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('YYYY'), moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('MM'), 0);
                  nextDueDate = newDate;
               }
            }

         }

      }

      const supportColumns = [
         {
            name: 'Ticket ID',
            field: 'ticket_id'
         },
         {
            name: 'App No.',
            field: 'application_no',
            options: {
               customBodyRender: (value) => {
                  if (value.application_no && application_details) {
                     return application_details[0].application_no
                  } else {
                     return '-'
                  }
               }
            }
         },
         {
            name: 'Plan No.',
            field: 'plan_number',
            options: {
               customBodyRender: (value) => {
                  if (value.plan_number && this.props.customerPlan) {
                     var ppnp = this.props.customerPlan.filter(x => x.pp_id == value.plan_number)
                     return ppnp[0].plan_number
                  } else {
                     return '-'
                  }
               }
            }
         },
         {
            name: 'Invoice No.',
            field: 'invoice_number',
            options: {
               customBodyRender: (value) => {
                  if (value.invoice_number && this.props.customerInvoice) {
                     var invo = this.props.customerInvoice.filter(x => x.invoice_id == value.invoice_number)

                     return invo[0].invoice_number
                  } else {
                     return '-'
                  }
               }
            }
         },
         {
            name: 'Subject',
            field: 'subject',
         },
         {
            name: 'Status',
            field: 'status',
         },
         {
            name: 'Action',
            field: 'ticket_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {

                  return (
                     <React.Fragment>
                        <span className="list-action">
                           <Link to={`/cosigner/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                        </span>
                     </React.Fragment>
                  )
               },

            }
         }

      ];

      const supportOptions = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
            );
         }
      };

      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
            MuiPaper: {
               root: { boxShadow: "none !important" }
            }
         }
      });

      const { classes } = this.props;
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i> Back</Link>
                           </li>
                        </ul>
                     </div>
                     {this.props.application_plans &&
                        <div className="p-10">

                           <h1 className="text-center mb-20">Customer Account</h1>
                           {this.props.application_plans[0].plan_updated == 1 &&
                              <div className="alert alert-danger alert-dismissible fade show">
                                 Now this plan apply new Interest rate due to missed payment. Available Credit has been locked call HPS at (919) 600-5526 for any information.
                              </div>
                           }
                           <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Customer Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-nowrap"><strong>Account No :</strong></td>
                                          <td>{(application_details) ? application_details[0].patient_ac : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-nowrap"><strong>Application No :</strong></td>
                                          <td>{(application_details) ? application_details[0].application_no : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Name :</strong></td>
                                          <td className="text-capitalize">{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</td>
                                       </tr>

                                       <tr>
                                          <td><strong>Address :</strong></td>
                                          <td>{(application_details) ? (application_details[0].address1 + ', ' + application_details[0].City + ', ' + application_details[0].name + ', ' + application_details[0].zip_code) : '-'}

                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Phone :</strong></td>
                                          <td>{(application_details) ? application_details[0].peimary_phone : '-'}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Loan Information</th>
                                       </tr>
                                       <tr>
                                          <td><strong>Line Of Credit :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].approve_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Override Amount :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].override_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Available Balance :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].remaining_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Principal Amount :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(uniqueLoanAmount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Outstanding Principal :</strong></td>
                                          <td>
                                             ${parseFloat(uniqueRemainingAmount).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Payment Information</th>
                                       </tr>

                                       <tr>
                                          <td><strong>Late/Missed Payments :</strong></td>
                                          <td>
                                             {uniqueLateCount}
                                          </td>
                                       </tr>
                                       {/*<tr>
                                          <td><strong>Payments Missed :</strong></td>
                                          <td>
                                             {uniqueMissedCount.reduce((a, b) => a + b, 0)}
                                          </td>
                                       </tr>*/}
                                       <tr>
                                          <td><strong>Late Fees :</strong></td>
                                          <td>
                                             ${parseFloat(totalLateAmount).toFixed(2)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Financial Charges :</strong></td>
                                          <td>
                                             ${parseFloat(totalFinCharge).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           {(application_details[0].co_patient_id != null) &&
                              <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">
                                 <div className="add-card w-100">

                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="3">Co-signer Information</th>
                                          </tr>
                                          <tr>
                                             <td><strong>Name:</strong> {application_details[0].co_first_name + ' ' + application_details[0].co_middle_name + ' ' + application_details[0].co_last_name}</td>
                                             <td><strong>Address:</strong> {application_details[0].co_address1 + ' ' + application_details[0].co_address2 + ' ' + application_details[0].co_City + ' ' + application_details[0].co_state_name}</td>
                                             <td><strong>Phone:</strong> {application_details[0].co_phone_no}</td>
                                          </tr>

                                       </tbody>
                                    </table>

                                 </div>
                              </div>
                           }
                           <div className="table-responsive mb-5 pymt-history">
                              <h3 className="text-center mb-10">Payment Plans</h3>
                              <MuiThemeProvider theme={this.getMuiTheme()}>
                                 <table className="table table-borderless admin-application-payment-plan">
                                    <thead>
                                       <tr>
                                          <th>Plan ID</th>
                                          <th>Principal Amt</th>
                                          <th>Outstanding Principal Amt</th>
                                          <th>Payment Term</th>
                                          <th>APR(%)</th>
                                          <th>Monthly Payment</th>
                                          <th>Date Created</th>
                                          <th>Procedure Date</th>
                                          <th>Status</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>

                                       {planDetails.map(function (row, idx) {
                                          return (<tr key={idx}>
                                             <td>{row.plan_number}</td>
                                             <td>${(row.loan_amount).toFixed(2)}</td>
                                             <td>${(row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2)}</td>
                                             <td>{row.payment_term_month} Month</td>
                                             <td>{parseFloat(row.discounted_interest_rate).toFixed(2)}%</td>
                                             <td>${parseFloat(row.monthly_amount).toFixed(2)}</td>
                                             <td>{row.date_created}</td>
                                             <td>{row.procedure_date}</td>
                                             <td>{(row.status == 0) ? <span>{row.status_name} <Tooltip id="tooltip-top-end" title={row.note} placement="top-end"><i className="zmdi zmdi-comment-text"></i></Tooltip></span> : row.status_name}</td>
                                             <td>{(row.status == 0 && row.refund_id != null) ?
                                                <a href="javascript:void(0)" title="View Refund details" onClick={() => this.refundDetails(row)}><i className="zmdi zmdi-eye zmdi-hc-lg"></i></a>
                                                :
                                                '-'
                                             }</td>
                                          </tr>)
                                       }.bind(this))
                                       }
                                    </tbody>
                                 </table>

                              </MuiThemeProvider>
                           </div>

                           <div className="table-responsive mb-40 pymt-history">
                              <h3 className="text-center mb-10">Invoice History</h3>
                              <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                 <thead>
                                    <tr>
                                       <th>S. No.</th>
                                       <th>Invoice No.</th>
                                       <th>Payment Date</th>
                                       <th>Due Date</th>
                                       <th>Prev Bal</th>
                                       <th>Monthly Payment</th>
                                       <th>Late Fee</th>
                                       <th>Fin Charge</th>
                                       <th>Total Due</th>
                                       <th>Min Due</th>
                                       <th>Additional Amt</th>
                                       <th>Paid</th>
                                       <th>Balance</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    {groupArrays && groupArrays.map(function (row, idx) {
                                       return <tr key={idx} className={idx}>
                                          <td>{idx + 1}</td>
                                          <td>{row.invoice_number}</td>
                                          <td>{(row.payment_date) ? row.payment_date : '-'}</td>
                                          <td>{row.due_date}</td>
                                          <td>{(row.previous_blc > 0) ? '$' + parseFloat(row.previous_blc).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.payment_amount).toFixed(2)}</td>
                                          <td>{(row.late_fee_received) ? '$' + parseFloat(row.late_fee_received).toFixed(2) : '-'}</td>
                                          <td>{(row.fin_charge_received) ? '$' + parseFloat(row.fin_charge_received).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>{(row.additional_amount > 0) ? '$' + parseFloat(row.additional_amount).toFixed(2) : '-'}</td>
                                          <td>{(row.paid_amount1 > 0) ? '$' + parseFloat(row.paid_amount1).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.finalAmt).toFixed(2)}</td>
                                          <td>

                                             {(row.missed_flag == 1) ?
                                                'Missed'
                                                :
                                                (row.invoice_status == 1) ?
                                                   row.invoice_status_name
                                                   :
                                                   (row.invoice_status == 3) ?
                                                      row.invoice_status_name
                                                      :
                                                      (row.invoice_status == 4) ?
                                                         row.invoice_status_name
                                                         :
                                                         row.invoice_status_name
                                             }
                                          </td>
                                          <td>
                                             {(row.missed_flag == 1) ?
                                                '-'
                                                :
                                                ((row.invoice_status == 3 || row.invoice_status == 2 || (row.invoice_status == 5 && row.nextDeferred == 0)) && application_details[0].primary_bill_pay_flag == 1) ?
                                                   <div><Link to={`/cosigner/credit-application/installment/payment/${this.enc(row.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>
                                                   </div>
                                                   :
                                                   (row.invoice_status == 1 || row.invoice_status == 4) ?
                                                      (row.invoice_status == 1) ?
                                                         <Link to={`/cosigner/credit-application/installment/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>
                                                         :
                                                         (row.checkPartialPaid == 1) ?
                                                            <Link to={`/cosigner/credit-application/installment/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>
                                                            :
                                                            (application_details[0].primary_bill_pay_flag == 1) ?
                                                               <div><Link to={`/cosigner/credit-application/installment/payment/${this.enc(row.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>
                                                                  <Link to={`/cosigner/credit-application/installment/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link></div>
                                                               :
                                                               <div><Link to={`/cosigner/credit-application/installment/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link></div>
                                                      :
                                                      '-'
                                             }</td>
                                       </tr>
                                    }.bind(this))
                                    }
                                 </tbody>
                              </table>
                           </div>


                           <div className="admin-application-list support-history-container">
                              <RctCollapsibleCard
                                 colClasses="col-md-12 w-xs-full support-history"
                                 heading="Support History"
                                 collapsible
                                 fullBlock
                                 customClasses="overflow-hidden d-inline-block w-100"
                              >
                                 <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
                                 <MuiThemeProvider theme={myTheme}>
                                    <MaterialDatatable
                                       data={(this.props.tickets) ? this.props.tickets : ''}
                                       columns={supportColumns}
                                       options={supportOptions}
                                    />
                                 </MuiThemeProvider>
                              </RctCollapsibleCard>
                           </div>
                        </div>
                     }

                     <Modal isOpen={this.state.addCallLogModal} toggle={() => this.onAddUpdateCallLogModalClose()} className="support-history-modal">
                        <ModalHeader toggle={() => this.onAddUpdateCallLogModalClose()}>
                           Add Call Log
                        </ModalHeader>
                        <ModalBody>
                           <AddCallLogForm
                              addErr={this.state.add_err}
                              addCallLogDetails={this.state.addCallLogDetail}
                              onChangeAddCallLogDetails={this.onChangeAddCallLogDetails.bind(this)}
                              allInvoice={this.state.invoiceGroup}
                              allPlan={planDetails}
                              DatePicker={DatePicker}
                              dateTimeStartDate={this.state.dateTimeStartDate}
                              followUpDateTimeStartDate={this.state.followUpDateTimeStartDate}
                              masterTicketSource={this.props.masterTicketSource}
                           />

                        </ModalBody>
                        <ModalFooter>

                           <Button
                              variant="contained"
                              color="primary"
                              className="text-white"
                              onClick={() => this.addCallLog()}
                              disabled={!this.validateAddCallLogSubmit()}
                           >
                              Submit
                           </Button>


                           <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCallLogModalClose()}>Cancel</Button>
                        </ModalFooter>
                     </Modal>

                     {this.props.loading &&
                        <RctSectionLoader />
                     }


                  </RctCard>


               </div>

            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, CustomerSupportReducer, PaymentPlanReducer }) => {
   const { nameExist, isEdit, unlockStatus, userLockDetail } = authUser;
   const { loading, application_details, invoiceDetails, application_plans, amountDetails, userQuestions, invoicePlan } = PaymentPlanReducer;
   const { tickets, masterTicketSource, customerPlan, customerInvoice } = CustomerSupportReducer

   return { loading, application_details, invoiceDetails, application_plans, amountDetails, userQuestions, unlockStatus, userLockDetail, invoicePlan, tickets, masterTicketSource, customerPlan, customerInvoice }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails, createSupportTicket, getAllTickets,
})(PlanDetails);