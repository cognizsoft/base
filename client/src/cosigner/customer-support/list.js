/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import { currentUserId } from '../../apifile';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import Tooltip from '@material-ui/core/Tooltip';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MaterialDatatable from "material-datatable";
import AddCallLogButton from '../credit-applications/support-history/AddCallLogButton';
import AddCallLogForm from '../credit-applications/support-history/AddCallLogForm';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import { isEmpty, isMaster, isNumeric, isObjectEmpty } from '../../validator/Validator';
import {
    planDetails, clearRedirectURL, allPlanDetails, customerActivePlanDetails, customerPlanDetailsDownload, createSupportTicket, getAllTickets
} from 'Actions';
class TicketList extends Component {

    state = {
        openViewRefund: false,
        refundDetails: null,
        singleView: null,
        openInstallmentDialog: false,
        installmentDetails: null,

        
    }
    dec(cipherText) {
        var SECRET = 'rmaeshCSS';
        var reb64 = CryptoJS.enc.Hex.parse(cipherText);
        var bytes = reb64.toString(CryptoJS.enc.Base64);
        var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
        var plain = decrypt.toString(CryptoJS.enc.Utf8);
        return plain;
    }
    componentDidMount() {
        var data = {
            appid: currentUserId(),
            filter_by: 5 // for filter in node file
        }
        this.props.clearRedirectURL();
        //this.props.customerActivePlanDetails(currentUserId());
        this.props.getAllTickets(data);
    }





    enc(plainText) {
        var SECRET = 'rmaeshCSS'
        var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
        var e64 = CryptoJS.enc.Base64.parse(b64);
        var eHex = e64.toString(CryptoJS.enc.Hex);
        return eHex;
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {

            MuiTooltip: {
                tooltip: {
                    fontSize: "15px"
                }
            },
        }
    })

    onViewRefundClose() {
        this.setState({ openViewRefund: false, refundDetails: null })
    }

    render() {
        const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;
        var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
            if (!accumulator[currentValue.pp_id]) {
                // get recived amount
                var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
                    if (planindex == 0) {
                        accumulator = 0;
                    }
                    accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
                    return accumulator;
                }, 0);
                accumulator[currentValue.pp_id] = {
                    paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.plan_status, invoice_exist: currentValue.invoice_exist, note: currentValue.note, status_name: currentValue.status_name,
                    refund_comment: currentValue.refund_comment,
                    ach_account_no: currentValue.ach_account_no,
                    ach_routing_no: currentValue.ach_routing_no,
                    ach_bank_name: currentValue.ach_bank_name,
                    check_date: currentValue.check_date,
                    check_no: currentValue.check_no,
                    status_name: currentValue.status_name,
                    refund_amt: currentValue.refund_amt,
                    payment_method: currentValue.payment_method,
                    refundmethod: currentValue.refundmethod,
                    refund_id: currentValue.refund_id,
                };
            } else {
                //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
            }

            return accumulator;
        }, []);

        /* console.log('this.props.application_plans.length')
         console.log(this.props.application_plans)*/

        /////Support History/////
        const supportColumns = [
            {
                name: 'Ticket ID',
                field: 'ticket_id'
            },
            {
                name: 'App No.',
                field: 'application_no',
                options: {
                    customBodyRender: (value) => {
                        if (value.application_no) {
                            return value.application_no
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Plan No.',
                field: 'plan_number',
                options: {
                    customBodyRender: (value) => {
                        if (value.plan_number && this.props.customerPlan) {
                            var ppnp = this.props.customerPlan.filter(x => x.pp_id == value.plan_number)
                            return ppnp[0].plan_number
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Invoice No.',
                field: 'invoice_number',
                options: {
                    customBodyRender: (value) => {
                        if (value.invoice_number && this.props.customerInvoice) {
                            var invo = this.props.customerInvoice.filter(x => x.invoice_id == value.invoice_number)

                            return invo[0].invoice_number
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Subject',
                field: 'subject',
            },
            {
                name: 'Status',
                field: 'status',
            },
            {
                name: 'Action',
                field: 'ticket_id',
                options: {
                    noHeaderWrap: true,
                    filter: false,
                    sort: false,
                    download: false,
                    customBodyRender: (value, tableMeta, updateValue) => {

                        return (
                            <React.Fragment>
                                <span className="list-action">
                                    <Link to={`/cosigner/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                                </span>
                            </React.Fragment>
                        )
                    },

                }
            }

        ];

        const supportOptions = {
            filterType: 'dropdown',
            selectableRows: false,
            
        };
        const myTheme = createMuiTheme({
            overrides: {
                MaterialDatatableToolbar: {
                    root: { display: "none" }
                },
                MuiTableCell: {
                    footer: { padding: "4px 8px 4px 8px" }
                },
                MuiPaper: {
                    root: { boxShadow: "none !important" }
                }
            }
        });

        return (
            <div className="invoice-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.customerSupportTicket" />} match={this.props.match} />
                <div className="row">

                    <div className="col-sm-12 mx-auto">
                        <RctCard>

                            {this.props.tickets &&
                                <div className="p-10">
                                    <div className="admin-application-list support-history-container">
                                        <RctCollapsibleCard
                                            colClasses="col-md-12 w-xs-full support-history"
                                            heading="Support History"

                                            fullBlock
                                            customClasses="overflow-hidden d-inline-block w-100"
                                        >
                                            
                                            <MuiThemeProvider theme={myTheme}>
                                                <MaterialDatatable
                                                    data={(this.props.tickets) ? this.props.tickets : ''}
                                                    columns={supportColumns}
                                                    options={supportOptions}
                                                />
                                            </MuiThemeProvider>
                                        </RctCollapsibleCard>
                                    </div>
                                </div>
                            }





                            {this.props.loading &&
                                <RctSectionLoader />
                            }

                        </RctCard>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ authUser, creditApplication, CustomerDashboardReducer, CustomerSupportReducer }) => {

    const { nameExist, isEdit } = authUser;
    const { loading, application_details, invoiceDetails, application_plans, amountDetails, invoicePlan, app_id } = CustomerDashboardReducer;
    const { tickets, masterTicketSource, customerPlan, customerInvoice } = CustomerSupportReducer
    return { loading, application_details, invoiceDetails, application_plans, amountDetails, invoicePlan, app_id, tickets, masterTicketSource, customerPlan, customerInvoice }
}

export default connect(mapStateToProps, {
    planDetails, clearRedirectURL, allPlanDetails, customerActivePlanDetails, customerPlanDetailsDownload, createSupportTicket, getAllTickets
})(TicketList);