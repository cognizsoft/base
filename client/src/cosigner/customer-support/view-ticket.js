/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge
} from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IconButton from '@material-ui/core/IconButton';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import UnlockConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty } from '../../validator/Validator';

import { NotificationManager } from 'react-notifications';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import { Link } from 'react-router-dom';
import { Form, Label, FormGroup, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import {
  getAllTickets, createSupportTicket
} from 'Actions';
class ViewTicket extends Component {

  state = {
    logDetails: {
      caller_name: '',
      description: '',
      ticket_status: 1
    },
    add_err: {

    },

  }

 
  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    var data = {
      ticket_id: this.props.match.params.ticket_id,
      filter_by: 3 // for filter in node file
    }
    this.props.getAllTickets(data);

  }

  onChangeLogDetails(key, value) {
      let { add_err, logDetails } = this.state;
      //console.log(value)
      switch (key) {
         
        case 'caller_name':
          if (isEmpty(value)) {
            add_err[key] = "Caller Name can't be blank";
          } else {
            add_err[key] = '';
          }
        break;
         
        case 'description':
          if (isEmpty(value)) {
            add_err[key] = "Description can't be blank";
          } else {
            add_err[key] = '';
          }
        break;
         
        default:
            break;
      }

      this.setState({ add_err: add_err });
      this.setState({
         logDetails: {
            ...this.state.logDetails,
            [key]: value
         }
      });
      //console.log(this.state.logDetails)
  }

  validateAddCallLogSubmit() {
      
      return (
         this.state.add_err.description === ''
      );
   }

   addCallLog() {
      var callName = this.props.tickets.filter(x => x.commented_by == 2)

      this.state.logDetails.ticket_id = this.props.tickets[0].ticket_id
      this.state.logDetails.caller_name = (callName.length > 0) ? callName[0].caller_name : this.props.tickets[0].caller_name
      this.state.logDetails.submit = 1 //for view page reducer result
      this.state.logDetails.commented_by = 2 // for customer 
      //console.log(this.state.logDetails)
      //return false;
      //insertTermMonth
      this.props.createSupportTicket(this.state.logDetails);


      //this.setState({ addCallLogModal: false, loading: true });
      let self = this;
      let log = {
         caller_name: '',
         description: '',
         ticket_status: 1
      }
      setTimeout(() => {
         self.setState({ loading: false, logDetails: log, add_err: {} });
         NotificationManager.success('Call log submitted successfully!');
      }, 2000);
      
   }

   addCallLogCancel() {
      let log = {
         caller_name: '',
         description: '',
      }
      this.setState({ loading: false, logDetails: log });
      
   }

  goBack() {
    this.props.history.goBack(-1)
  }
  render() {
    let { logDetails, add_err } = this.state;

    let { tickets } = this.props;
    return (
      <div className="view-support-ticket">
            <PageTitleBar title={<IntlMessages id="sidebar.viewTicket" />} match={this.props.match} />
            {tickets &&
             <div className="row">

                <div className="col-sm-12 mx-auto">
                   <div className="view-ticket-container">
                      <div className="invoice-head text-right">
                         <ul className="list-inline">
                            <li>
                               <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i> Back</Link>
                            </li>
                            <li>
                               <ReactToPrint
                                  trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                  content={() => this.componentRef}
                               />
                            </li>
                         </ul>
                      </div>
                      <div className="customer-invoice pt-5 pb-5 px-50" ref={el => (this.componentRef = el)}>
                          <div className="ticket-head-container d-flex justify-content-between">
                            <div className="ticket-head mt-30 w-100 border-bottom">
                              <div className="float-left text-left">  
                              <h2>[Ticket #{tickets[0].ticket_id}] {tickets[0].subject} <span className={(tickets[tickets.length -1].status_id == 1) ? "ticket-status ticket-open" : (tickets[tickets.length -1].status_id == 2) ? "ticket-status ticket-hold" : "ticket-status ticket-close"}>{tickets[tickets.length -1].status}</span></h2>
                              <span><strong className="mr-5">Ticket related to:</strong>  
                                {(tickets[0].ac_ticket_flag == 1) ? 'A/C, ' : ''}
                                {(tickets[0].plan_ticket_flag == 1) ? 'Plan, ' : ''}
                                {(tickets[0].app_ticket_flag == 1) ? 'Application, ' : ''}
                                {(tickets[0].invoice_ticket_flag == 1) ? 'Invocie' : ''}
                              </span>
                              <br/>
                              {tickets[0].plan_ticket_flag == 1 &&
                                <span><strong className="mr-5">Plan Number:</strong>  {tickets[0].plan_number} <br/></span>
                              }
                              
                              {tickets[0].app_ticket_flag == 1 &&
                                <span><strong className="mr-5">Application Number:</strong>  {tickets[0].application_no} <br/></span>
                              }
                              
                              {tickets[0].invoice_ticket_flag == 1 &&
                                <span><strong className="mr-5">Invoice Number:</strong>  {tickets[0].invoice_number} </span>
                              }
                              </div>

                              <div className="float-right text-right">
                                <span><strong className="mr-5">From:</strong> {tickets[0].caller_name}</span>
                              </div>

                            </div>

                          </div>

                          {!tickets[0].date_created && <div className="comments ticket-comment-section mt-20 mb-40">
                            <div className="comment">
                              <span className="comment-date">{tickets[0].date_created}</span>
                              <span className="comment-description">{tickets[0].description}</span>
                            </div>
                          </div>
                          }

                          <div className="all-comment-section mt-20 mb-20">
                            <table className="w-100 table">
                              <thead>
                                <tr>
                                    <th>CS Tkt Dtl ID</th>
                                    <th>Call Date</th>
                                    <th>Inc Call Flag</th>
                                    <th>HPS Agent</th>
                                    <th>Caller Name</th>
                                    
                                    <th>Description</th>
                                    <th>Follow Up</th>
                                    <th>Follow Up Sch</th>
                                </tr>
                              </thead>
                              <tbody>
                                {tickets && tickets.map((tkt, key) => {
                                  return (
                                      <tr key={key} className={(tkt.commented_by == 1) ? 'admin_comment comment' : 'customer_comment comment'}> 
                                        
                                        <td>{tkt.cs_tkt_dtl_id}</td>
                                        <td>{tkt.call_date_time}</td>
                                        <td>{(tkt.incoming_call_flag == "Yes") ? "Yes" : "No"}</td>
                                        <td>{tkt.hps_agent_name}</td>
                                        <td>{tkt.caller_name}</td>
                                        
                                        <td>{tkt.description}</td>
                                        <td>{(tkt.follow_up_flag == 1) ? 'Yes' : 'No'}</td>
                                        <td>{(tkt.follow_up_flag == 1) ? tkt.follow_up_sch : '-'}</td>
                                      </tr>
                                    )
                                })}
                              </tbody>
                            </table>
                          </div>

                          {tickets[tickets.length - 1].status_id !== 0 && 
                            <div className="d-flex justify-content-between add-full-card customer-detail-info">

                              <div className="add-cardd w-100">
                                <div className="row">
                                    

                                    <div className="col-md-12">
                                        <FormGroup>
                                            <Input
                                                type="textarea" 
                                                name="description" 
                                                id="description" 
                                                className="ticket-reply-textarea"
                                                placeholder="Enter Desccription"
                                                value={logDetails.description}
                                                onChange={(e) => this.onChangeLogDetails('description', e.target.value)}
                                                />
                                              {(add_err.description) ? <FormHelperText>{add_err.description}</FormHelperText> : ''}
                                        </FormGroup>
                                    </div>

                                </div>

                                <div className="ticket-action-button float-right ">

                                  <Button 
                                    variant="contained"
                                    color="primary" 
                                    className="text-white cancel-btn mr-10"
                                    onClick={() => this.addCallLogCancel()}
                                  >
                                    Cancel
                                  </Button>

                                  <Button
                                    variant="contained"
                                    color="primary"
                                    className="text-white submit-btn"
                                    onClick={() => this.addCallLog()}
                                    disabled={!this.validateAddCallLogSubmit()}
                                  >
                                     Submit
                                  </Button>

                                </div>
                              </div>

                            </div>
                          }
                         
                      </div>
                   </div>
                </div>

             </div>
            }
            {
               this.props.loading &&
               <RctSectionLoader />
            }
         </div >
    );
  }
}
const mapStateToProps = ({ CustomerSupportReducer }) => {
  const { loading, tickets } = CustomerSupportReducer;
  return { loading, tickets }
}
export default connect(mapStateToProps, {
  getAllTickets, createSupportTicket
})(ViewTicket);