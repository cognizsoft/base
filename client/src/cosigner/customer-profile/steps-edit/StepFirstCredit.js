/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import { InputAdornment, withStyles } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
const StepFirstCredit = ({ addErr, addData, onChnagerovider, DatePicker, startDate, existCustomer, onChnageExist, validateSearch, startDateExist, existError, searchCustomerSubmit, searchCustomerReset }) => (

   <div className="modal-body page-form-outer text-left">

      <div className="row">
         <div className="col-md-4">
            <FormGroup>
               <Label for="first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="first_name"
                  id="first_name"
                  fullWidth
                  inputProps={{
                     readOnly: true
                  }}
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.first_name}
                  error={(addErr.first_name) ? true : false}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  helperText={addErr.first_name}
                  onChange={(e) => onChnagerovider('first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="middle_name"
                  id="middle_name"
                  fullWidth
                  inputProps={{
                     readOnly: true
                  }}
                  variant="outlined"
                  placeholder="Middle Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.middle_name}
                  onChange={(e) => onChnagerovider('middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="last_name"
                  id="last_name"
                  fullWidth
                  inputProps={{
                     readOnly: true
                  }}
                  variant="outlined"
                  placeholder="Last Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.last_name}
                  error={(addErr.last_name) ? true : false}
                  helperText={addErr.last_name}
                  onChange={(e) => onChnagerovider('last_name', e.target.value)}
               />
            </FormGroup>
         </div>

      </div>
      <div className="row">

         <div className="col-md-4">
            <FormGroup>
               <Label for="dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="dob"
                  id="dob"
                  inputProps={{
                     readOnly: true
                  }}
                  selected={startDate}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  onChange={(e) => onChnagerovider('dob', e)}
               />
               {(addErr.dob) ? <FormHelperText className="jss116">{addErr.dob}</FormHelperText> : ''}
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup className="ssn-mask">
               <Label for="ssn">Social Security Number<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="ssn"
                  id="ssn"
                  fullWidth
                  inputProps={{
                     readOnly: true
                  }}
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  placeholder="Social Security Number"
                  value={addData.ssn.replace(/.(?=.{4})/g, 'x')}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  error={(addErr.ssn) ? true : false}
                  helperText={addErr.ssn}
                  onChange={(e) => onChnagerovider('ssn', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      
      <p className="customer_profile_fields">You cannot change the value of these fields of section</p> 

      <div className="row d-none">
         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Gender</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'M'}
                        readOnly={true}
                        checked={(addData.gender == 'M') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Male
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'F'}
                        readOnly={true}
                        checked={(addData.gender == 'F') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Female
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>


         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Status</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={1}
                        readOnly={true}
                        checked={(addData.status == 1) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Active
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={0}
                        readOnly={true}
                        checked={(addData.status == 0) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Inactive
                  </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>



   </div>

);

export default StepFirstCredit;