/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Profile from './component/Profile';
import BankDetails from './component/BankDetails';
import EmploymentDetails from './component/EmploymentDetails';
import Address from './component/Address';
import UserBlock from './component/UserBlock';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge,
   Collapse
} from 'reactstrap';
// rct card box
import { RctCard } from 'Components/RctCard';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { NotificationManager } from 'react-notifications';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { currentCoSignerId } from '../../apifile';
import VerifyAmountForm from './VerifyAmountForm';
import { isEmpty, isContainWhiteSpace, pointDecimals, isDecimals, isNumeric, isNumericDecimal, isLength } from '../../validator/Validator';

import {
   viewCosigner, submitCustomerVerifyAmount
} from 'Actions';

function getSteps() {
   return ['My Profile', 'Bank Details', 'Employment Details', 'Address'];
}

class UserProfile extends Component {

   state = {
      activeTab: this.props.location.state ? this.props.location.state.activeTab : 0,
      activeStep: 0,
      completed: {},

      addNewVerifyAmountModal: false,
      loading: false,

      verify_err: {
         verify_amount: ''
      },
      verify_amt: {
         patient_id: '',
         verify_amount: ''
      },
      CoappDetails: {}
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onVerifyAmountDetails(name, value)
      });
   }

   onVerifyAmount() {
      let { verify_amt, verify_err } = this.state;

      var verify_error = {}
      verify_amt.patient_id = currentCoSignerId()

      this.setState({ addNewVerifyAmountModal: true, verify_amt: verify_amt, verify_err: verify_error });
   }

   onVerifyAmountModalClose() {
      let { verify_err } = this.state

      var verify_error = {}

      this.setState({ addNewVerifyAmountModal: false, verify_err: verify_error })
   }

   onVerifyAmountDetails(fieldName, value) {

      let { verify_err } = this.state;
      //console.log(value)
      switch (fieldName) {
         case 'verify_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               verify_err[fieldName] = "Amount can't be blank";
            } else if (!isDecimals(value)) {
               verify_err[fieldName] = "Amount not valid";
            } else if (value <= 0 || value >= 1) {
               verify_err[fieldName] = 'Amount should be between 0.00 - 1.00';
            } else {
               verify_err[fieldName] = '';
            }

            this.setState({
               verify_amt: {
                  ...this.state.verify_amt,
                  [fieldName]: value
               }
            });
            break;
         default:
            break;

      }

      this.setState({ verify_err: verify_err });



   }

   verifyAmount() {
      const { verify_amt } = this.state;
      const { CoappDetails } = this.props;

      //console.log(verify_amt)
      //return false;

      if ((Date.now() / 1000) >= CoappDetails.bank_verify_amt_duration) {
         NotificationManager.error('Time limit is expired. Please contact to admin.');
         //console.log(customerDetails.bank_verify_amt_duration)
         //console.log(Date.now() / 1000)
      } else {

         if (CoappDetails.bank_verify_amt == verify_amt.verify_amount) {

            this.props.submitCustomerVerifyAmount(verify_amt);

            let self = this;
            setTimeout(() => {
               this.setState({ loading: true, addNewVerifyAmountModal: false });
               //NotificationManager.success('Amount Submitted!');
               self.setState({ loading: false });
            }, 2000);

         } else {
            NotificationManager.error('Amount does not match. Please enter correct amount.');
         }

      }

   }

   validateVerifyAmount() {
      return (
         this.state.verify_err.verify_amount === ''
      )
   }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   componentDidMount() {
      this.props.viewCosigner(currentCoSignerId());
   }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   componentWillReceiveProps(nextProps) {

      let { CoappDetails } = this.state;

      if (nextProps.CoappDetails) {
         this.setState({ CoappDetails: nextProps.CoappDetails })
      }
      if (nextProps.verifyAmt !== undefined) {
         this.setState({ CoappDetails: nextProps.verifyAmt.customerDetails })
      }

   }
   onCollapseappInfo() {
      this.setState({ collapse_app_info: !this.state.collapse_app_info });
   }
   onCollapseBankInfo() {
      this.setState({ collapse_bank_info: !this.state.collapse_bank_info });
   }
   onCollapseEmpInfo() {
      this.setState({ collapse_emp_info: !this.state.collapse_emp_info });
   }
   onCollapseAddInfo() {
      this.setState({ collapse_add: !this.state.collapse_add });
   }
   render() {
      const steps = getSteps();
      const { activeStep, verify_err, CoappDetails } = this.state;
      const { loading, CoappAddress } = this.props;


      return (
         <div className="userProfile-wrapper">
            <Helmet>
               <title>User Profile</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.userProfile" />} match={this.props.match} />
            <RctCard>
               {CoappDetails &&
                  <UserBlock
                     customerDetails={CoappDetails}
                     customerID={currentCoSignerId()}
                  />
               }


               {CoappDetails &&
                  <div className="col-sm-12">
                     <div className="rct-block custom-collapse">
                        <div className="rct-block-title">
                           <h4><span>My Profile</span></h4>
                           <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseappInfo()}><i className={(this.state.collapse_app_info) ? "ti-minus" : "ti-plus"}></i></a>
                           </div>
                        </div>
                        <Collapse isOpen={this.state.collapse_app_info}>
                           <div className="rct-block-content">
                              <div className="width-100">
                                 <table className="table">
                                    <tbody>
                                       <tr>
                                          <td className="fw-bold">First Name :</td>
                                          <td className="text-capitalize">{(CoappDetails.f_name) ? CoappDetails.f_name : '-'}</td>
                                          <td className="fw-bold">Other First Name :</td>
                                          <td>{(CoappDetails.other_f_name) ? CoappDetails.other_f_name : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Middle Name :</td>
                                          <td className="text-capitalize">{(CoappDetails.m_name) ? CoappDetails.m_name : '-'}</td>
                                          <td className="fw-bold">Other Middle Name :</td>
                                          <td>{(CoappDetails.other_m_name) ? CoappDetails.other_m_name : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Last Name :</td>
                                          <td className="text-capitalize">{(CoappDetails.l_name) ? CoappDetails.l_name : '-'}</td>
                                          <td className="fw-bold">Other Last Name :</td>
                                          <td>{(CoappDetails.other_l_name) ? CoappDetails.other_l_name : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Date of Birth :</td>
                                          <td>{(CoappDetails.dob) ? CoappDetails.dob : '-'}</td>
                                          <td className="fw-bold">Primary Phone No.:</td>
                                          <td>{(CoappDetails.user_phone1) ? CoappDetails.user_phone1 : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Gender :</td>
                                          <td>{(CoappDetails.gender) ? (CoappDetails.gender == 'F') ? 'Male' : 'Female' : '-'}</td>
                                          <td className="fw-bold">Secondary Phone No.:</td>
                                          <td>{(CoappDetails.user_phone2) ? CoappDetails.user_phone2 : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Primary Email Address :</td>
                                          <td>{(CoappDetails.email) ? CoappDetails.email : '-'}</td>
                                          <td className="fw-bold">Social Security Number :</td>
                                          <td>
                                             {(CoappDetails.ssn) ? CoappDetails.ssn.replace(/.(?=.{4})/g, 'x') : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Secondary Email Address :</td>
                                          <td>{(CoappDetails.secondary_email) ? CoappDetails.secondary_email : '-'}</td>
                                          <td className="fw-bold">Status :</td>
                                          <td>{(CoappDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                                       </tr>
                                       <tr>
                                          <td className="fw-bold">Customer A/C :</td>
                                          <td>{(CoappDetails.patient_ac) ? CoappDetails.patient_ac : '-'}</td>
                                          <td className="fw-bold"></td>
                                          <td></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </Collapse>
                     </div>
                     <div className="rct-block custom-collapse">
                        <div className="rct-block-title">
                           <h4><span>Bank Details</span></h4>
                           <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseBankInfo()}><i className={(this.state.collapse_bank_info) ? "ti-minus" : "ti-plus"}></i></a>
                           </div>
                        </div>
                        <Collapse isOpen={this.state.collapse_bank_info}>
                           <div className="rct-block-content">
                              <div className="view-section-inner">

                                 <div className="view-box">
                                    {this.props.CobankDetails && this.props.CobankDetails.map((bank, idx) => (
                                       <div className="width-100" key={idx}>
                                          <table className="table">
                                             <tbody>
                                                <tr>
                                                   <td className="fw-bold">Bank Name:</td>
                                                   <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                                                   <td className="fw-bold">Name on Account:</td>
                                                   <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                                                </tr>
                                                <tr>
                                                   <td className="fw-bold">Routing Number:</td>
                                                   <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                                                   <td className="fw-bold">Bank A/C Type:</td>
                                                   <td>{(bank.value) ? bank.value : '-'}</td>
                                                </tr>
                                                <tr>
                                                   <td className="fw-bold">Bank A/C#:</td>
                                                   <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                                                   <td className="fw-bold">Bank Address</td>
                                                   <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <hr className="border-dark" />
                                       </div>
                                    ))
                                    }
                                 </div>

                              </div>
                           </div>
                        </Collapse>
                     </div>
                     <div className="rct-block custom-collapse">
                        <div className="rct-block-title">
                           <h4><span>Employment Information</span></h4>
                           <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseEmpInfo()}><i className={(this.state.collapse_emp_info) ? "ti-minus" : "ti-plus"}></i></a>
                           </div>
                        </div>
                        <Collapse isOpen={this.state.collapse_emp_info}>
                           <div className="rct-block-content">
                              <div className="view-section-inner">

                                 <div className="view-box">
                                    <div className="width-50">
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td className="fw-bold">Employed :</td>
                                                <td>{(CoappDetails.employment_status == 0) ? 'No' : 'Yes'}</td>
                                             </tr>
                                             <tr>
                                                <td className="fw-bold">Employer Name :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.employer_name) ? CoappDetails.employer_name : '-'}</td>
                                             </tr>
                                             <tr>
                                                <td className="fw-bold">Employment Type :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.emp_type_name) ? CoappDetails.emp_type_name : '-'}</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>

                                    <div className="width-50">
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td className="fw-bold">Employer Phone No :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.employer_phone) ? CoappDetails.employer_phone : '-'}</td>
                                             </tr>
                                             <tr>
                                                <td className="fw-bold">Annual Income($) :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.annual_income) ? CoappDetails.annual_income : '-'}</td>
                                             </tr>
                                             <tr>
                                                <td className="fw-bold">Employer Email Address :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.employer_email) ? CoappDetails.employer_email : '-'}</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>

                                 <div className="view-box">
                                    <div className="width-50">
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td className="fw-bold">Employed Since :</td>
                                                <td>{(CoappDetails.employment_status !== 0 && CoappDetails.employer_since && CoappDetails.employer_since !== '00/00/0000') ? CoappDetails.employer_since : '-'}</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>

                                    <div className="width-50">
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td></td>
                                                <td></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>

                              </div>

                           </div>
                        </Collapse>
                     </div>
                     <div className="rct-block custom-collapse">
                        <div className="rct-block-title">
                           <h4><span>Address</span></h4>
                           <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseAddInfo()}><i className={(this.state.collapse_add) ? "ti-minus" : "ti-plus"}></i></a>
                           </div>
                        </div>
                        <Collapse isOpen={this.state.collapse_add}>
                           <div className="rct-block-content">
                              {CoappAddress && CoappAddress.map((addRess, idx) => (

                                 <div className="width-100" key={idx}>
                                    <table className="table">
                                       <tbody>
                                          <tr>
                                             <td className="fw-bold">Address1</td>
                                             <td>{(addRess.address1) ? addRess.address1 : '-'}</td>
                                             <td className="fw-bold">State :</td>
                                             <td>{(addRess.state_name) ? addRess.state_name : '-'}</td>
                                          </tr>
                                          <tr>
                                             <td className="fw-bold">Address2 :</td>
                                             <td>{(addRess.address2) ? addRess.address2 : '-'}</td>
                                             <td className="fw-bold">Phone No :</td>
                                             <td>{(addRess.phone_no) ? addRess.phone_no : '-'}</td>
                                          </tr>
                                          <tr>
                                             <td className="fw-bold">Country :</td>
                                             <td>{(addRess.country_name) ? addRess.country_name : '-'}</td>
                                             <td className="fw-bold">Billing and Physical address same :</td>
                                             <td>{(addRess.billing_address == 1) ? 'Yes' : 'No'}</td>
                                          </tr>
                                          <tr>
                                             <td className="fw-bold">City :</td>
                                             <td>{(addRess.city) ? addRess.city : '-'}</td>
                                             <td className="fw-bold">Zip Code :</td>
                                             <td>{(addRess.zip_code) ? addRess.zip_code : '-'}</td>
                                          </tr>
                                          <tr>
                                             <td className="fw-bold">How long at this address? :</td>
                                             <td>{(addRess.address_time_period) ? addRess.address_time_period + 'Yrs.' : '-'}</td>
                                             <td className="fw-bold">Primary Address :</td>
                                             <td>{(addRess.primary_address == 1) ? 'Yes' : 'No'}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <hr className="border-dark" />


                                 </div>
                              ))}
                           </div>
                        </Collapse>
                     </div>


                  </div>
               }

            </RctCard>

            <Modal isOpen={this.state.addNewVerifyAmountModal} toggle={() => this.onVerifyAmountModalClose()}>
               <ModalHeader toggle={() => this.onVerifyAmountModalClose()}>

                  Verify Bank Account

               </ModalHeader>

               <ModalBody>

                  <VerifyAmountForm
                     verifyAmtErr={verify_err}
                     onVerifyAmountDetail={this.onVerifyAmountDetails.bind(this)}
                     verifyAmt={this.state.verify_amt}
                  />

               </ModalBody>
               <ModalFooter>

                  <Button variant="contained" color="primary" className="text-white" onClick={() => this.verifyAmount()} disabled={!this.validateVerifyAmount()}>Verify</Button>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onVerifyAmountModalClose()}>Cancel</Button>

               </ModalFooter>
            </Modal>

         </div>
      );
   }
}
const mapStateToProps = ({ CosignerReducer, CustomerDashboardReducer }) => {

   const { loading, CoappDetails, CoappAddress, CobankDetails } = CosignerReducer;

   const { verifyAmt } = CustomerDashboardReducer
   return { loading, CoappDetails, CoappAddress, verifyAmt, CobankDetails }
}
export default connect(mapStateToProps, {
   viewCosigner, submitCustomerVerifyAmount
})(UserProfile);