/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepThird = ({ addErr, addData, onChnagerovider, handleThirdTabBankNameChange, handleRemoveThirdTabBank, handleAddThirdTabBank, thirdTabBank, handleThirdTabSplNameChange, handleRemoveThirdTabSpl, handleAddThirdTabSpl, thirdTabSpl, provider_option_bank, provider_option_location }) => (
   <div className="table-responsive">
         <div className="modal-body page-form-outer text-left third-tab-container">
              <div className="third-tab-bank-inner-container">
                {addData.bank_details.map((thirdtab_bank, idx) => (
                  
                   <div className="row"  key={idx}>

                        {(() => {
                            if (idx != 0) {
                                return (
                                    <React.Fragment>
                                        <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                                        <div className="col-md-2">
                                            <a href="#" onClick={(e) => handleRemoveThirdTabBank(idx)}>Remove Bank (-)</a>
                                        </div>
                                    </React.Fragment>
                                )
                            }

                        })()}

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="location_name">Location Name<span className="required-field">*</span></Label>
                              <Input
                                  type="select"
                                  name="location_name"
                                  id="location_name"
                                  placeholder=""
                                  value={addData.bank_details[idx].location_name}
                                  onChange={(e) => onChnagerovider('location_name', e.target.value, idx)}
                              >
                                  <option value="">Select</option>
                                  {addData.location && addData.location.map((opt, key) => (
                                      <option value={opt.physical_location_name} key={key}>{opt.physical_location_name}</option>
                                  ))}
                                  
                              </Input>
                              {(addErr.bank_details[idx].location_name != '') ? <FormHelperText>{addErr.bank_details[idx].location_name}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="bank_name">Bank Name<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="bank_name"
                                 id="bank_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Bank Name"
                                 value={(addData.bank_details[idx].bank_name != '') ? addData.bank_details[idx].bank_name : ''}
                                 error={(addErr.bank_details[idx].bank_name) ? true : false}
                                 helperText={(addErr.bank_details[idx].bank_name != '') ? addErr.bank_details[idx].bank_name : ''}
                                 onChange={(e) => onChnagerovider('bank_name', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="bank_address">Bank Address</Label><br/>
                                 <TextField
                                 type="text"
                                 name="bank_address"
                                 id="bank_address"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Bank Address"
                                 value={(addData.bank_details[idx].bank_address != '') ? addData.bank_details[idx].bank_address : ''}
                                 onChange={(e) => onChnagerovider('bank_address', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="rounting_no">Routing Number<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="rounting_no"
                                 id="rounting_no"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Routing Number"
                                 value={(addData.bank_details[idx].rounting_no != '') ? addData.bank_details[idx].rounting_no : ''}
                                 error={(addErr.bank_details[idx].rounting_no) ? true : false}
                                 helperText={(addErr.bank_details[idx].rounting_no != '') ? addErr.bank_details[idx].rounting_no : ''}
                                 onChange={(e) => onChnagerovider('rounting_no', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="bank_accout">Bank A/C#<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="bank_accout"
                                 id="bank_accout"
                                 fullWidth
                                 inputProps={{ maxLength: 17 }}
                                 variant="outlined"
                                 placeholder="Bank A/C#"
                                 value={(addData.bank_details[idx].bank_accout != '') ? addData.bank_details[idx].bank_accout : ''}
                                 error={(addErr.bank_details[idx].bank_accout) ? true : false}
                                 helperText={(addErr.bank_details[idx].bank_accout != '') ? addErr.bank_details[idx].bank_accout : ''}
                                 onChange={(e) => onChnagerovider('bank_accout', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="name_on_account">Name on Account</Label><br/>
                                 <TextField
                                 type="text"
                                 name="name_on_account"
                                 id="name_on_account"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Name on Account"
                                 value={(addData.bank_details[idx].name_on_account != '') ? addData.bank_details[idx].name_on_account : ''}
                                 error={(addErr.bank_details[idx].name_on_account) ? true : false}
                                 helperText={(addErr.bank_details[idx].name_on_account != '') ? addErr.bank_details[idx].name_on_account : ''}
                                 onChange={(e) => onChnagerovider('name_on_account', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="bank_account_type">Bank A/C Type<span className="required-field">*</span></Label>
                              <Input
                                  type="select"
                                  name="bank_account_type"
                                  id="bank_account_type"
                                  placeholder=""
                                  value={(addData.bank_details[idx].bank_account_type_id != '') ? addData.bank_details[idx].bank_account_type_id : ''}
                                  onChange={(e) => onChnagerovider('bank_account_type', e.target.value, idx)}
                              >
                                  <option value="">Select</option>
                                  {provider_option_bank && provider_option_bank.map((opt, key) => (
                                      <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                  ))}
                                  
                              </Input>
                              {(addErr.bank_details[idx].bank_account_type != '') ? <FormHelperText>{addErr.bank_details[idx].bank_account_type}</FormHelperText> : ''}
                           </FormGroup>
                        </div>
                   </div>
                  
                 ))}

                  <div className="row">
                   <div className="col-md-12">
                      <a href="#" onClick={(e) => handleAddThirdTabBank()}>Add More Bank (+)</a>
                   </div>
                  </div>

              </div>
         </div>
   </div>
);

export default StepThird;