/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Provider from './component/Provider';
import Locations from './component/Locations';
import Banks from './component/Banks';
import AdditionalInfo from './component/AdditionalInfo';
import UserBlock from './component/UserBlock';
import Documents from './component/Documents';
import Users from './component/Users';

// rct card box
import { RctCard } from 'Components/RctCard';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
  ProviderDetails
} from 'Actions';
import {patientID} from '../../apifile';
// For Tab Content
function TabContainer(props) {
   return (
      <Typography component="div" style={{ padding: 8 * 3 }}>
         {props.children}
      </Typography>
   );
}

class ProviderView extends Component {

   state = {
      activeTab: this.props.location.state ? this.props.location.state.activeTab : 0,
      opnDocFileModal: false,
      imgPath:'',
      imgName:'',
   }

   componentDidMount() {
    //console.log(patientID())
    //this.props.viewCustomer(2);
    this.props.ProviderDetails(this.props.match.params.id);
  }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   opnDocFileModal(path, name) {
      this.setState({ opnDocFileModal: true, imgPath:path, imgName:name})
   }
   
   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }

   render() {
      const { activeTab } = this.state;
      const { loading } = this.props;

      const path = require('path');
      const providerDetails = this.props.provider_details;
      const providerBank = this.props.provider_bank;
      const providerLocation = this.props.provider_location;
      const providerUser = this.props.provider_user;
      const providerAdditionalDiscount = this.props.provider_additional_discount;
      const providerAdditionalFee = this.props.provider_additional_fee;
      const providerDocument = this.props.provider_document;

      return (
         <div className="providerView-wrapper">
            <Helmet>
               <title>Provider View</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.providerView" />} match={this.props.match} />
            <RctCard>
            
               <div className="rct-tabs">
                  <AppBar position="static">
                     <Tabs
                        value={activeTab}
                        onChange={this.handleChange}
                        variant="scrollable"
                        scrollButtons="off"
                        indicatorColor="primary"
                     >
                        <Tab
                           icon={<i className="ti-user"></i>}
                           label="Provider"
                        />
                        <Tab
                           icon={<i className="ti-location-pin"></i>}
                           label="Locations"
                        />
                        <Tab
                           icon={<i className="ti-home"></i>}
                           label="Banks"
                        />
                        <Tab
                           icon={<i className="zmdi zmdi-info-outline"></i>}
                           label="Additional Info"
                        />
                        <Tab
                           icon={<i className="ti-files"></i>}
                           label="Documents"
                        />
                        <Tab
                           icon={<i className="zmdi zmdi-accounts-outline"></i>}
                           label="Users"
                        />
                     </Tabs>
                  </AppBar>
                  {activeTab === 0 && providerDetails &&
                     <TabContainer>
                        <Provider 
                           providerDetails={providerDetails}
                        />
                     </TabContainer>}
                  {activeTab === 1 && providerLocation &&
                     <TabContainer>
                        <Locations 
                           providerLocation={providerLocation}
                        />
                     </TabContainer>}
                  {activeTab === 2 && providerBank &&
                     <TabContainer>
                        <Banks 
                           providerBank={providerBank}
                        />
                     </TabContainer>}
                  {activeTab === 3 && providerAdditionalDiscount && providerAdditionalFee &&
                     <TabContainer>
                        <AdditionalInfo 
                           providerAdditionalDiscount={providerAdditionalDiscount}
                           providerAdditionalFee={providerAdditionalFee}
                        />
                     </TabContainer>}
                  {activeTab === 4 && providerDocument &&
                     <TabContainer>
                        <Documents 
                           providerDocument={providerDocument}
                           opnDocFileModal={this.opnDocFileModal.bind(this)}
                        />
                     </TabContainer>}
                  {activeTab === 5 && providerUser &&
                     <TabContainer>
                        <Users 
                           providerUser={providerUser}
                        />
                     </TabContainer>}
               </div>
            </RctCard>

            <Modal className="p-view-img" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>
                  
               <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
                  <span className="float-left>">Name: {this.state.imgName}</span>
                  <span className="float-right"><a href={this.state.imgPath} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span> 
               </ModalHeader>

               <ModalBody>
                  {this.state.imgPath &&
                     <embed src={this.state.imgPath} width="100%" download/>
                
                  }

               </ModalBody>
               
            </Modal>

         </div>
      );
   }
}
const mapStateToProps = ({ Provider }) => {
   console.log(provider_details)
  const { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document } = Provider;
  return { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document }
}
export default connect(mapStateToProps, {
  ProviderDetails
})(ProviderView);