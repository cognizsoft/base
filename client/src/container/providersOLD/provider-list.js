/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../validator/Validator';
import {
ProviderList, deleteProvider
} from 'Actions'; 

//import AddNewButton from './AddNewButton';

class Provider extends Component {

   state = {
      currentModule: 24,
      cancelState: 0,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      Provider: null, // initial user data
      selectedProvider: null, // selected user to perform operations
      loading: false, // loading activity
      addNewProviderModal: false, // add new user form modal
      addNewProviderDetail: {
         payback_type: '',
         min_volume: '',
         max_volume: '',
         rate: '',
         status: 1,
         checked: false 
      },
      openViewProviderDialog: false, // view user dialog box
      editProvider: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         payback_id:'',
         min_volume:'',
         max_volume:'',
         rate:''
      },
      add_err: {
         
      },
      ruleExist:false,
   }

   validateAddSubmit() {
      return (
         this.state.add_err.payback_type === '' &&
         this.state.add_err.min_volume === '' &&
         this.state.add_err.max_volume === '' &&
         this.state.add_err.rate === ''
      );
   }

   validateUpdateSubmit() {
      return (
         this.state.err.payback_id === '' &&
         this.state.err.min_volume === '' &&
         this.state.err.max_volume === '' &&
         this.state.err.rate === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateProviderDetails(name, value)
      });
   }

   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.ProviderList();      
   }

   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { addData } = this.state;
      (nextProps.redirectURL != '') ? this.setState({changeURL: nextProps.redirectURL}) : '';
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewProviderModal() {
      this.setState({ addNewProviderModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedProvider: data });
   }

   /*
   * Title :- deleteUserPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   deleteUserPermanently() {
      const { selectedProvider } = this.state;
      let users = this.props.provider_list;
      let indexOfDeleteProvider = users.indexOf(selectedProvider);
      this.props.deleteProvider(users[indexOfDeleteProvider].provider_id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedProvider: null });
   }
    /**
    * View Master Value Detail Hanlder
    */
   viewProviderDetail(data) {
      this.setState({ openViewProviderDialog: true, selectedProvider: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewProviderModalClose() {
      this.setState({ openViewProviderDialog: false, selectedProvider: null })
   }

   /**
    * On Edit Master Value
    */
   onEditProvider(editProvider) {
        //console.log(editPayback)
      this.setState({ addNewProviderModal: true, editProvider: editProvider });
   }


   render() {
      const { add_err, err, Provider, loading, editProvider, allSelected, selectedProvider } = this.state;
      const ProviderProp = this.props.provider_list;

      const columns = [
         
         {
            name: 'ID', 
            field: 'provider_id'
         },
         {   
            name: 'Provider Type', 
            field: 'provider_type',
         },
         {
            name: 'Provider Name', 
            field: 'name',
         },
         {
            name: 'Addresss', 
            field: 'address1',
         },
         {
            name: 'City', 
            field: 'city',
         },
         {
            name: 'State', 
            field: 'state_name',
         },
         {
            name: 'Zip', 
            field: 'zip_code',
         },
         {
            name: 'In-Network', 
            field: 'member_flag',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  (value.member_flag == 1) ? 'Yes' : 'No'
               );
             }
           }
         },
         {   
            name: 'Primary Contact', 
            field: 'primary_contact',
         },
         {   
            name: 'Email', 
            field: 'email',
         },
         {   
            name: 'Phone No', 
            field: 'phone',
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               //console.log(value)
               return (
                 <div className="list-action">
                  {(this.state.currentPermision.view)?<Link to={"/admin/providers/provider-view/"+value.provider_id}><i className="ti-eye"></i></Link>:''}
                  {(this.state.currentPermision.edit)?<Link to={"/admin/providers/provider-edit/"+value.provider_id}><i className="ti-pencil"></i></Link>:''}
                  {(this.state.currentPermision.delete)?<a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a>:''}
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
             (this.state.currentPermision.add)?<Link to="/admin/providers/add-new" color="primary" className="caret btn-sm mr-10">Add Providers <i className="zmdi zmdi-plus"></i></Link>:''
           );
         }
         
      };
      return (
         <div className="others admin-provider-list">
            <Helmet>
               <title>Health Partner | Providers</title>
               <meta name="description" content="Provider" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.providerList" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   className={'abc'}
                   data={ProviderProp}
                   columns={columns}
                   options={options}
                   
               />
            </RctCollapsibleCard>
            
           <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete user permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ Provider, authUser }) => {
   //console.log(Provider)
   
   const { loading, provider_list, redirectURL } = Provider;
   const user = authUser.user;
   return { loading, user, provider_list, redirectURL }
   
}

export default connect(mapStateToProps, {
ProviderList, deleteProvider
})(Provider);