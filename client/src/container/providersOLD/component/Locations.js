/**
 * Email Prefrences Page
 */
import React, { Component } from 'react';
import Switch from 'react-toggle-switch';
import Button from '@material-ui/core/Button';
import { FormGroup, Input } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';

// intl messages
import IntlMessages from 'Util/IntlMessages';

const Locations = ({ providerLocation }) => (
         <div className="prefrences-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">

                 {providerLocation && providerLocation.map((location, key) => (
                    <div className="p-view-location  mb-15" key={key}>
                    <div className="view-box" >

                      <div className="physical-address-box">
                        <h3 className="p-view-title">Physical Address</h3>
                        <div className={(location.billing_address_flag == 1) ? 'width-50' : 'width-50 mb-15'}>
                          <table>
                              <tbody>
                              <tr>
                                <td className="fw-bold">Location Name:</td>
                                <td>{location.physical_location_name}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Address1:</td>
                                <td>{location.physical_address1}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Address2:</td>
                                <td>{location.physical_address2}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">City:</td>
                                <td>{location.physical_city}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">State:</td>
                                <td>{location.physical_state}</td>
                              </tr>
                              </tbody>
                          </table>
                        </div>
                        <div className="width-50">
                          <table>
                              <tbody>
                              <tr>
                                <td className="fw-bold">Zip Code:</td>
                                <td>{location.physical_zip_code}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Country:</td>
                                <td>{location.physical_country}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Phone No:</td>
                                <td>{location.physical_phone_no}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Billing and physical address same:</td>
                                <td>{(location.billing_address_flag == 1) ? 'Yes' : 'No'}</td>
                              </tr>
                              </tbody>
                          </table>
                        </div>
                      </div>

                      {location.billing_address_flag !== 1 &&
                        <div className="billing-address-box">
                          <h3 className="p-view-title">Billing Address</h3>
                          <div className="width-50 mb-15">
                            <table>
                                <tbody>
                                <tr>
                                  <td className="fw-bold">Location Name:</td>
                                  <td>{location.billing_location_name}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address1:</td>
                                  <td>{location.billing_address1}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address2:</td>
                                  <td>{location.billing_address2}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">City:</td>
                                  <td>{location.billing_city}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">State:</td>
                                  <td>{location.billing_state}</td>
                                </tr>
                                </tbody>
                            </table>
                          </div>
                          <div className="width-50 mb-15">
                            <table>
                                <tbody>
                                <tr>
                                  <td className="fw-bold">Zip Code:</td>
                                  <td>{location.billing_zip_code}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Region:</td>
                                  <td>{location.billing_region}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Country:</td>
                                  <td>{location.billing_country}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Phone No:</td>
                                  <td>{location.billing_phone_no}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                          </div>
                        </div>
                      }

                    </div>
                    </div>
                  ))}

               </div>

            </div>
         </div>
      );
export default Locations;