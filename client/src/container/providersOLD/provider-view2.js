/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   ProviderDetails
} from 'Actions';

class ViewProvider extends Component {
    
   state = {
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0
   }


   componentDidMount() {
      this.props.ProviderDetails(this.props.match.params.id);  
      //console.log(this.props);    
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   render() {
      const { users, loading } = this.state;
      const path = require('path');
      const provider_details = this.props.provider_details;
      const provider_bank = this.props.provider_bank;
      const provider_location = this.props.provider_location;
      const provider_user = this.props.provider_user;
      const provider_additional_discount = this.props.provider_additional_discount;
      const provider_additional_fee = this.props.provider_additional_fee;
      const provider_document = this.props.provider_document;

      
      //console.log('ProviderDetailsProp')
      //console.log(provider_details)
      const data = [
         { state_name: "North Carolina", region_name: "Region-1", serving: "Yes", description: "description here", status: "Active"},
         { state_name: "North Carolina", region_name: "Region-3", serving: "No", description: "description here", status: "Inactive"},
      ];
      const options = {
         filterType: 'dropdown',
      };
      return (
         <div className="country regions">
            <Helmet>
               <title>Health Partner | Providers | Add New</title>
               <meta name="description" content="Regions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.providerView" />}
               match={this.props.match}
            />
            {/*this.props.match.params.id*/}
            <RctCollapsibleCard fullBlock>
                {this.props.loading &&
                  <RctSectionLoader/>
                }
               <div className="table-responsive">
                 <div className="modal-body page-form-outer view-section">
                    
                       <div className="view-section-inner">
                          <h4>Providers </h4>
                          <div className="view-box">
                            <div className="width-50">
                              <table>
                                <tbody>
                                <tr>
                                  <td className="fw-bold">Proiver Name:</td>
                                  <td>{provider_details ? provider_details[0].provider_name : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Provider Type:</td>
                                  <td>{provider_details ? provider_details[0].provider_type : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Primary Contact Phone:</td>
                                  <td>{provider_details ? provider_details[0].provider_phone_no : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Email Address: </td>
                                  <td>{provider_details ? provider_details[0].provider_email : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Fax No:</td>
                                  <td>{provider_details ? provider_details[0].provider_fax_no : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Tax ID/SSN:</td>
                                  <td>{provider_details ? provider_details[0].provider_taxid_ssn : ''}</td>
                                </tr>
                                </tbody>
                              </table>
                            </div>

                            <div className="width-50">
                              <table>
                                <tbody>
                                <tr>
                                  <td className="fw-bold">Primary Contact:</td>
                                  <td>{provider_details ? provider_details[0].provider_primary_contact : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Website:</td>
                                  <td>{provider_details ? provider_details[0].provider_website : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">In Network:</td>
                                  <td>{provider_details ? (provider_details[0].provider_network == 1) ? 'Yes' : 'No' : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold"> Phone No:</td>
                                  <td>{provider_details ? provider_details[0].provider_primary_contact_phone : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Status:</td>
                                  <td>{provider_details ? (provider_details[0].provider_status == 1) ? 'Active' : 'Inactive' : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold"></td>
                                  <td></td>
                                </tr>
                                </tbody>
                              </table>
                            </div>

                            

                          </div>
                       </div>

                       <div className="view-section-inner">
                          <h4>Locations</h4>
                          
                          {provider_location && provider_location.map((location, key) => (
                            <div key={key}>
                            <h3 className="location-title">Location {key+1}</h3>
                            <div className="view-box mb-10" >
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Location Name:</td>
                                      <td>{location.physical_location_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Address1:</td>
                                      <td>{location.physical_address1}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Address2:</td>
                                      <td>{location.physical_address2}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">City:</td>
                                      <td>{location.physical_city}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">State:</td>
                                      <td>{location.physical_state}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Zip Code:</td>
                                      <td>{location.physical_zip_code}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Region:</td>
                                      <td>{location.physical_region}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Country:</td>
                                      <td>{location.physical_country}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Phone No:</td>
                                      <td>{location.physical_phone_no}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                              <h3 className="billing-title">Billing Address</h3>
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Location Name:</td>
                                      <td>{location.billing_location_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Address1:</td>
                                      <td>{location.billing_address1}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Address2:</td>
                                      <td>{location.billing_address2}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">City:</td>
                                      <td>{location.billing_city}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">State:</td>
                                      <td>{location.billing_state}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Zip Code:</td>
                                      <td>{location.billing_zip_code}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Region:</td>
                                      <td>{location.billing_region}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Country:</td>
                                      <td>{location.billing_country}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Phone No:</td>
                                      <td>{location.billing_phone_no}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>

                            </div>
                            </div>
                          ))}
                       </div>

                       <div className="view-section-inner">
                          <h4>Bank Details</h4>
                          {provider_bank && provider_bank.map((bank, key) => (
                                                        <div key={key}>
                                                        <h3 className="location-title">Bank Detail {key+1}</h3>
                          <div className="view-box mb-10">
                            <div className="width-50">
                              <table>
                                  <tbody>
                                  <tr>
                                    <td className="fw-bold">Location Name:</td>
                                    <td>{bank.location_name}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Bank Name: </td>
                                    <td>{bank.bank_name}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Bank Address: </td>
                                    <td>{bank.bank_address}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Routing Number:</td>
                                    <td>{bank.rounting_no}</td>
                                  </tr>
                                  </tbody>
                              </table>
                            </div>
                            <div className="width-50">
                              <table>
                                  <tbody>
                                  <tr>
                                    <td className="fw-bold">Bank A/C#:</td>
                                    <td>{bank.bank_accout}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Name on Account:</td>
                                    <td>{bank.name_on_account}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Bank A/C Type:</td>
                                    <td>{bank.bank_account_type}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold"></td>
                                    <td></td>
                                  </tr>
                                  
                                  </tbody>
                              </table>
                            </div>

                          </div>
                         </div>
                          ))}
                       </div>

                       <div className="view-section-inner">
                          <h4>Additional Informations</h4>
                            <div className="view-box">
                             

                              {provider_additional_discount && provider_additional_discount.map((additional, key) => (
                                
                                <div className="width-50" key={key}>
                                 {(() => {
                                      
                                      return(   
                                         
                                          <table>
                                              <tbody>
                                              <tr>
                                                <td className="fw-bold">Discount Type:</td>
                                                <td>{additional.discount_type}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">Discount Rate:</td>
                                                <td>{additional.discount_rate+'%'}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">From Date:</td>
                                                <td>{additional.discount_from_date}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">To Date:</td>
                                                <td>{additional.discount_to_date}</td>
                                              </tr>
                                              </tbody>
                                          </table>
                                          
                                      )
                                   
                                  })()}
                                 </div>
                                  
                              ))}

                              {provider_additional_fee && provider_additional_fee.map((additional, key) => (
                                
                                <div className="width-50" key={key}>
                                 {(() => {
                                      
                                      return(   
                                         
                                          <table>
                                              <tbody>
                                              <tr>
                                                <td className="fw-bold">Discount Type:</td>
                                                <td>{additional.additional_fee_type}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">Discount Rate:</td>
                                                <td>{'$'+additional.additional_conv_fee}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">From Date:</td>
                                                <td>{additional.additional_from_date}</td>
                                              </tr>
                                              <tr>
                                                <td className="fw-bold">To Date:</td>
                                                <td>{additional.additional_to_date}</td>
                                              </tr>
                                              </tbody>
                                          </table>
                                          
                                      )
                                   
                                  })()}
                                 </div>
                                  
                              ))}

                              

                            </div>
                          
                       </div>


                       <div className="view-section-inner">
                          <h4>Documents</h4>
                          {provider_document && provider_document.map((document, key) => (
                            
                            <div className="view-box" key={key}>
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Document Type:</td>
                                      <td>{document.file_type}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Document Name:</td>
                                      <td>{document.document_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Document:</td>
                                      <td>{document.document_upload}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                               
                            </div>
                            
                          ))}
                       </div>

                       <div className="view-section-inner">
                          <h4>Users</h4>
                          {provider_user && provider_user.map((user, key) => (
                            <div key={key}>
                            <h3 className="location-title">User {key+1}</h3>
                            <div className="view-box" key={key}>
                              <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">First Name:</td>
                                      <td>{user.user_first_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Middle Name:</td>
                                      <td>{user.user_middle_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Last Name:</td>
                                      <td>{user.user_last_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Phone No:</td>
                                      <td>{user.user_phone_no}</td>
                                    </tr>
                                    </tbody>
                                </table>
                              </div>
                               <div className="width-50">
                                <table>
                                    <tbody>
                                    <tr>
                                      <td className="fw-bold">Location</td>
                                      <td>{user.user_location}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Email Address:</td>
                                      <td>{user.user_email}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">User Role:</td>
                                      <td>{user.user_role}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">User Name:</td>
                                      <td>{user.username}</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                              </div>
                            </div>
                            </div>
                          ))}
                       </div>

                       





                  </div>
               </div>
               {loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
           
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ Provider, authUser }) => {
   console.log(Provider)
   
   const { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document } = Provider;
   const user = authUser.user;
   return { loading, user, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document }
   
}

export default connect(mapStateToProps, {
ProviderDetails
})(ViewProvider);