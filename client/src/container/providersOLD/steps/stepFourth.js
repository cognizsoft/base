/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import moment from 'moment';

const StepFourth = ({ addErr, addData, onChnagerovider, handleFourthTabDiscountNameChange, handleRemoveFourthTabDiscount, handleAddFourthTabDiscount, fourthTabDiscount, handleFourthTabFeeNameChange, handleRemoveFourthTabFee, handleAddFourthTabFee, fourthTabFee, handleFourthTabDocNameChange, handleRemoveFourthTabDoc, handleAddFourthTabDoc, fourthTabDoc, DatePicker, provider_option_discount_type, provider_option_fee_type, provider_option_document_type, provider_payment_term, provider_loan_term_months, provider_option_spl, splProcedure, handleRemoveThirdTabSpl, handleAddThirdTabSpl, }) => (
  <div className="table-responsive">
    <div className="modal-body page-form-outer text-left fourth-tab-container">


      <div className="fourth-tab-spl-container">
      <h3>Procedure Speciality</h3>
        {addData.speciality.map((thirdtab_spl, idx) => (

          <div className="row" key={idx}>

            {(() => {
              if (idx != 0) {
                return (
                  <React.Fragment>
                    <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                    <div className="col-md-2">
                      <a href="#" onClick={(e) => handleRemoveThirdTabSpl(idx)}>Remove Speciality (-)</a>
                    </div>
                  </React.Fragment>
                )
              }

            })()}



            <div className="col-md-3">
              <FormGroup>
                <Label for="spl_location_name">Location Name<span className="required-field">*</span></Label>
                <Input
                  type="select"
                  name="spl_location_name"
                  id="spl_location_name"
                  placeholder=""
                  onChange={(e) => onChnagerovider('spl_location_name', e.target.value, idx)}
                >
                  <option value="">Select</option>
                  {addData.location && addData.location.map((opt, key) => (
                    <option value={opt.physical_location_name} key={key}>{opt.physical_location_name}</option>
                  ))}

                </Input>
                {(addErr.speciality[idx].spl_location_name != '') ? <FormHelperText>{addErr.speciality[idx].spl_location_name}</FormHelperText> : ''}
              </FormGroup>

            </div>

            <div className="col-md-3">
              <FormGroup>
                <Label for="specialization">Speciality Name<span className="required-field">*</span></Label>
                <Input
                  type="select"
                  name="specialization"
                  id="specialization"
                  placeholder=""
                  value={(addData.speciality[idx].specialization != '') ? addData.speciality[idx].specialization : ''}
                  onChange={(e) => onChnagerovider('specialization', e.target.value, idx)}
                >
                  <option value="">Select</option>
                  {provider_option_spl && provider_option_spl.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                  ))}
                </Input>
                {(addErr.speciality[idx].specialization != '') ? <FormHelperText>{addErr.speciality[idx].specialization}</FormHelperText> : ''}
              </FormGroup>
            </div>

            <div className="col-md-3">
            
              <FormGroup>
                <Label for="procedure">Procedure</Label>
                <Input
                  type="select"
                  name="procedure"
                  id="procedure"
                  placeholder=""
                  value={(addData.speciality[idx].procedure != '') ? addData.speciality[idx].procedure : ''}
                  onChange={(e) => onChnagerovider('procedure', e.target.value, idx)}
                >
                  <option value="">Select</option>
                  {splProcedure[idx] && splProcedure[idx].map((opt, key) => (
                    <option value={opt.procedure_id} key={key}>{opt.procedure_name}</option>
                  ))}
                </Input>
                {(addErr.speciality[idx].procedure != '') ? <FormHelperText>{addErr.speciality[idx].procedure}</FormHelperText> : ''}
              </FormGroup>
            </div>

          </div>

        ))}


        <div className="row">
          <div className="col-md-12">
            <a href="#" onClick={(e) => handleAddThirdTabSpl()}>Add More Speciality (+)</a>
          </div>
        </div>
      </div>
      <br/>
      <div className="fourth-tab-discount-rate-container">
        <h3>Discount Rate</h3>
        {addData.discount_rate.map((fourthtab_discount, idx) => (
          <div className="row" key={idx}>

            {(() => {
              if (idx != 0) {
                return (
                  <React.Fragment>
                    <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                    <div className="col-md-3 text-right">
                      <a href="#" onClick={(e) => handleRemoveFourthTabDiscount(idx)}>Remove Discount Rate (-)</a>
                    </div>
                  </React.Fragment>
                )
              }

            })()}

            <div className="col-md-4">
              <FormGroup>
                <Label for="discount_type">Discount Type<span className="required-field">*</span></Label>
                <Input
                  type="select"
                  name="discount_type"
                  id="discount_type"
                  placeholder=""
                  value={(addData.discount_rate[idx].discount_type != '') ? addData.discount_rate[idx].discount_type : ''}
                  onChange={(e) => onChnagerovider('discount_type', e, idx)}
                >
                  <option value="">Select</option>
                  {provider_option_discount_type && provider_option_discount_type.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                  ))}

                </Input>
                {(addErr.discount_rate[idx].discount_type != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_type}</FormHelperText> : ''}
              </FormGroup>
            </div>

            <div className="col-md-4">
              <FormGroup>
                <Label for="loan_term_month">Loan Term(months)</Label>
                <Input
                  type="select"
                  name="loan_term_month"
                  id="loan_term_month"
                  placeholder=""
                  value={(addData.discount_rate[idx].loan_term_month != '') ? addData.discount_rate[idx].loan_term_month : ''}
                  onChange={(e) => onChnagerovider('loan_term_month', e.target.value, idx)}
                >
                  <option value="">Select</option>
                  {provider_loan_term_months && provider_loan_term_months.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                  ))}

                </Input>
                {(addErr.discount_rate[idx].loan_term_month != '') ? <FormHelperText>{addErr.discount_rate[idx].loan_term_month}</FormHelperText> : ''}
              </FormGroup>
            </div>

            <div className="col-md-4">
              <FormGroup>
                <Label for="discount_rate">Discount PCT<span className="required-field">*</span> (%)</Label><br />
                <TextField
                  type="text"
                  name="discount_rate"
                  id="discount_rate"
                  fullWidth
                  variant="outlined"
                  placeholder="Discount PCT (%)"
                  value={(addData.discount_rate[idx].discount_rate != '') ? addData.discount_rate[idx].discount_rate : ''}
                  error={(addErr.discount_rate[idx].discount_rate) ? true : false}
                  helperText={(addErr.discount_rate[idx].discount_rate != '') ? addErr.discount_rate[idx].discount_rate : ''}
                  onChange={(e) => onChnagerovider('discount_rate', e.target.value, idx)}
                />
              </FormGroup>
            </div>

            <div className="col-md-4">
              <FormGroup>
                <Label for="redu_discount_rate">Reduced Interest PCT (%)</Label><br />
                <TextField
                  type="text"
                  name="redu_discount_rate"
                  id="redu_discount_rate"
                  fullWidth
                  variant="outlined"
                  placeholder="Reduced Discount PCT (%)"
                  value={(addData.discount_rate[idx].redu_discount_rate != '') ? addData.discount_rate[idx].redu_discount_rate : ''}
                  error={(addErr.discount_rate[idx].redu_discount_rate) ? true : false}
                  helperText={(addErr.discount_rate[idx].redu_discount_rate != '') ? addErr.discount_rate[idx].redu_discount_rate : ''}
                  onChange={(e) => onChnagerovider('redu_discount_rate', e.target.value, idx)}
                />
              </FormGroup>
            </div>

            {addData.discount_rate[idx].discount_type_filter &&
              <div className="col-md-4">
                <FormGroup>
                  <Label for="discount_from_date">From Date<span className="required-field">*</span></Label>
                  <DatePicker
                    name="discount_from_date"
                    id="discount_from_date"
                    selected={(addData.discount_rate[idx].discount_from_date) ? new Date(addData.discount_rate[idx].discount_from_date) : ''}
                    placeholderText="MM/DD/YYYY"
                    maxDate={(addData.discount_rate[idx].discount_to_date) ? new Date(addData.discount_rate[idx].discount_to_date) : ''}
                    autocomplete={false}
                    onChange={(e) => onChnagerovider('discount_from_date', e, idx)}
                  />
                  {(addErr.discount_rate[idx].discount_from_date != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_from_date}</FormHelperText> : ''}
                </FormGroup>
              </div>
            }

            {addData.discount_rate[idx].discount_type_filter &&
              <div className="col-md-4">
                <FormGroup>
                  <Label for="discount_to_date">Expire Date<span className="required-field">*</span></Label>
                  <DatePicker
                    name="discount_to_date"
                    id="discount_to_date"
                    selected={(addData.discount_rate[idx].discount_to_date) ? new Date(addData.discount_rate[idx].discount_to_date) : ''}
                    placeholderText="MM/DD/YYYY"
                    minDate={(addData.discount_rate[idx].discount_from_date) ? new Date(addData.discount_rate[idx].discount_from_date) : ''}
                    autocomplete={false}
                    onChange={(e) => onChnagerovider('discount_to_date', e, idx)}
                  />
                  {(addErr.discount_rate[idx].discount_to_date != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_to_date}</FormHelperText> : ''}
                </FormGroup>
              </div>
            }
          </div>
        ))}
        <div className="row">
          <div className="col-md-12 text-left">
            <a href="#" onClick={(e) => handleAddFourthTabDiscount()}>Add More Discount Rate (+)</a>
          </div>
        </div>
      </div>

      <br />


      <div className="fourth-tab-fee-rate-container">
        <h3>Additional Fee</h3>
        {addData.additional_fee.map((fourthtab_fee, idx) => (
          <div className="row" key={idx}>

            {(() => {
              if (idx != 0) {
                return (
                  <React.Fragment>
                    <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                    <div className="col-md-3 text-right">
                      <a href="#" onClick={(e) => handleRemoveFourthTabFee(idx)}>Remove Additional Fee (-)</a>
                    </div>
                  </React.Fragment>
                )
              }

            })()}

            <div className="col-md-3">
              <FormGroup>
                <Label for="additional_fee_type">Fee Type</Label>
                <Input
                  type="select"
                  name="additional_fee_type"
                  id="additional_fee_type"
                  placeholder=""
                  value={(addData.additional_fee[idx].additional_fee_type != '') ? addData.additional_fee[idx].additional_fee_type : ''}
                  onChange={(e) => onChnagerovider('additional_fee_type', e.target.value, idx)}
                >
                  <option value="">Select</option>
                  {provider_option_fee_type && provider_option_fee_type.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                  ))}

                </Input>
                {(addErr.additional_fee[idx].additional_fee_type != '') ? <FormHelperText>{addErr.additional_fee[idx].additional_fee_type}</FormHelperText> : ''}
              </FormGroup>
            </div>

            <div className="col-md-3">
              <FormGroup>
                <Label for="additional_conv_fee">Convenience Fee ($)</Label><br />
                <TextField
                  type="text"
                  name="additional_conv_fee"
                  id="additional_conv_fee"
                  fullWidth
                  variant="outlined"
                  placeholder="Convenience Fee"
                  value={(addData.additional_fee[idx].additional_conv_fee != '') ? addData.additional_fee[idx].additional_conv_fee : ''}
                  error={(addErr.additional_fee[idx].additional_conv_fee) ? true : false}
                  helperText={(addErr.additional_fee[idx].additional_conv_fee != '') ? addErr.additional_fee[idx].additional_conv_fee : ''}
                  onChange={(e) => onChnagerovider('additional_conv_fee', e.target.value, idx)}
                />
              </FormGroup>
            </div>

            <div className="col-md-3">
              <FormGroup>
                <Label for="additional_from_date">From Date</Label>
                <DatePicker
                  name="additional_from_date"
                  id="additional_from_date"
                  selected={(addData.additional_fee[idx].additional_from_date) ? new Date(addData.additional_fee[idx].additional_from_date) : ''}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  onChange={(e) => onChnagerovider('additional_from_date', e, idx)}
                />

              </FormGroup>
            </div>

            <div className="col-md-3">
              <FormGroup>
                <Label for="additional_to_date">To Date</Label>
                <DatePicker
                  name="additional_to_date"
                  id="additional_to_date"
                  selected={(addData.additional_fee[idx].additional_to_date) ? new Date(addData.additional_fee[idx].additional_to_date) : ''}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  onChange={(e) => onChnagerovider('additional_to_date', e, idx)}
                />
              </FormGroup>
            </div>

          </div>
        ))}
        <div className="row">
          <div className="col-md-12 text-left">
            <a href="#" onClick={(e) => handleAddFourthTabFee()}>Add More Additional Fee (+)</a>
          </div>
        </div>
      </div>

      <br />


      <div className="fourth-tab-upload-document-container">
        <h3>Documents</h3>
        {addData.documents.map((fourthtab_doc, idx) => (
          <div className="row" key={idx}>
            {(() => {
              if (idx != 0) {
                return (
                  <React.Fragment>
                    <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                    <div className="col-md-3 text-right">
                      <a href="#" onClick={(e) => handleRemoveFourthTabDoc(idx)}>Remove Document (-)</a>
                    </div>
                  </React.Fragment>
                )
              }

            })()}
            <div className="col-md-3">
              <FormGroup>
                <Label for="document_type">Document Type</Label>
                <Input
                  type="select"
                  name="document_type"
                  id="document_type"
                  placeholder=""
                  value={(addData.documents[idx].document_type != '') ? addData.documents[idx].document_type : ''}
                  onChange={(e) => onChnagerovider('document_type', e.target.value, idx)}

                >
                  <option value="">Select</option>
                  {provider_option_document_type && provider_option_document_type.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                  ))}

                </Input>
                {(addErr.documents[idx].document_type != '') ? <FormHelperText>{addErr.documents[idx].document_type}</FormHelperText> : ''}

              </FormGroup>
            </div>

            <div className="col-md-3">
              <FormGroup>
                <Label for="document_name">Document Name</Label><br />
                <TextField
                  type="text"
                  name="document_name"
                  id="document_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Document Name"
                  value={(addData.documents[idx].document_name != '') ? addData.documents[idx].document_name : ''}
                  error={(addErr.documents[idx].document_name) ? true : false}
                  helperText={(addErr.documents[idx].document_name != '') ? addErr.documents[idx].document_name : ''}
                  onChange={(e) => onChnagerovider('document_name', e.target.value, idx)}
                />
              </FormGroup>
            </div>

            <div className="col-md-3 upload-doc-input">
              <FormGroup>
                <Label for="document_upload">Upload Document</Label><br />
                <Input
                  type="file"
                  name="document_upload"
                  id="document_upload"
                  placeholder=""

                  onChange={(e) => onChnagerovider('document_upload', e, idx)}
                >

                </Input>
              </FormGroup>
            </div>


          </div>
        ))}
        <div className="row">
          <div className="col-md-12 text-left">
            <a href="#" onClick={(e) => handleAddFourthTabDoc()}>Add More Documents (+)</a>
          </div>
        </div>
      </div>



    </div>
  </div>
);

export default StepFourth;