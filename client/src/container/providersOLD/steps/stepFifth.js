/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepFifth = ({ addErr, addData, onChnagerovider, provider_option_user_role, checkUsernameExist }) => (
    <div className="table-responsive">
         <div className="modal-body page-form-outer text-left fifth-tab-container">
            
               <div className="row">
                  <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_first_name">First Name<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_first_name"
                                 id="user_first_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="First Name"
                                 value={(addData.user_first_name != '') ? addData.user_first_name : ''}
                                 error={(addErr.user_first_name) ? true : false}
                                 helperText={(addErr.user_first_name != '') ? addErr.user_first_name : ''}
                                 onChange={(e) => onChnagerovider('user_first_name', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_middle_name">Middle Name</Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_middle_name"
                                 id="user_middle_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Middle Name"
                                 value={(addData.user_middle_name != '') ? addData.user_middle_name : ''}
                                 onChange={(e) => onChnagerovider('user_middle_name', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_last_name">Last Name<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_last_name"
                                 id="user_last_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Last Name"
                                 value={(addData.user_last_name != '') ? addData.user_last_name : ''}
                                 error={(addErr.user_last_name) ? true : false}
                                 helperText={(addErr.user_last_name != '') ? addErr.user_last_name : ''}
                                 onChange={(e) => onChnagerovider('user_last_name', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_email">Email Address<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_email"
                                 id="user_email"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Email Address"
                                 value={(addData.user_email != '') ? addData.user_email : ''}
                                 error={(addErr.user_email) ? true : false}
                                 helperText={(addErr.user_email != '') ? addErr.user_email : ''}
                                 onChange={(e) => onChnagerovider('user_email', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_role">User Role<span className="required-field">*</span></Label>
                              <Input
                                  type="select"
                                  name="user_role"
                                  id="user_role"
                                  placeholder=""
                                  value={(addData.user_role != '') ? addData.user_role : ''}
                                  onChange={(e) => onChnagerovider('user_role', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {provider_option_user_role && provider_option_user_role.map((opt, key) => (
                                   <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                   ))}
                                  
                              </Input>
                                  {(addErr.user_role != '') ? <FormHelperText>{addErr.user_role}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                        
                        <div className="col-md-4 bg-grey-input">
                           <FormGroup>
                              <Label for="user_location">Location</Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_location"
                                 id="user_location"
                                 fullWidth
                                 readOnly
                                 variant="outlined"
                                 placeholder="Location"
                                 value={(addData.location[0].physical_location_name != '') ? addData.location[0].physical_location_name : ''}
                                 onChange={(e) => onChnagerovider('user_location', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_phone_no">Phone No<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="user_phone_no"
                                 id="user_phone_no"
                                 fullWidth
                                 inputProps={{ maxLength: 14 }}
                                 variant="outlined"
                                 placeholder="Phone No"
                                 value={(addData.user_phone_no != '') ? addData.user_phone_no : ''}
                                 error={(addErr.user_phone_no) ? true : false}
                                 helperText={(addErr.user_phone_no != '') ? addErr.user_phone_no : ''}
                                 onChange={(e) => onChnagerovider('user_phone_no', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                  
                          <FormGroup check>
                          <br/>
                          <Label check>
                            <Input 
                              type="checkbox" 
                              value="1"
                              name="user_primary_contact_flag"
                              onChange={(e) => onChnagerovider('user_primary_contact_flag', e.target.checked)}
                              />{' '}
                             Set as primary contact
                              </Label>
                        </FormGroup>
                       </div>


                       </div>

                       <div className="row">

                         <div className="col-md-4">
                           <FormGroup>
                              <Label for="username">User Name<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="username"
                                 id="username"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="User Name"
                                 value={(addData.username != '') ? addData.username : ''}
                                 error={(addErr.username) ? true : false}
                                 helperText={(addErr.username != '') ? addErr.username : ''}
                                 onChange={(e) => onChnagerovider('username', e.target.value)}
                                 onKeyUp={(e) => checkUsernameExist(e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_password">Password<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="password"
                                 name="user_password"
                                 id="user_password"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Password"
                                 value={(addData.user_password != '') ? addData.user_password : ''}
                                 error={(addErr.user_password) ? true : false}
                                 helperText={(addErr.user_password != '') ? addErr.user_password : ''}
                                 onChange={(e) => onChnagerovider('user_password', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="user_confirm_password">Confirm Password<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="password"
                                 name="user_confirm_password"
                                 id="user_confirm_password"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Confirm Password"
                                 value={(addData.user_confirm_password != '') ? addData.user_confirm_password : ''}
                                 error={(addErr.user_confirm_password) ? true : false}
                                 helperText={(addErr.user_confirm_password != '') ? addErr.user_confirm_password : ''}
                                 onChange={(e) => onChnagerovider('user_confirm_password', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

               </div>
                  
         </div>
      </div>
);

export default StepFifth;