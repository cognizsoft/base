/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

// rct theme provider
import RctThemeProvider from './RctThemeProvider';

//Horizontal Layout
import HorizontalLayout from './HorizontalLayout';

//Agency Layout
import AgencyLayout from './AgencyLayout';

//Main App
import RctDefaultLayout from './DefaultLayout';

// boxed layout
import RctBoxedLayout from './RctBoxedLayout';

// app signin
import AppSignIn from './Signin';
import SignupVerifyEmail from './SignupVerifyEmail';
import AppSignUp from './SignupFirebase';
import AppProviderCreate from './providers/add-new';
import ApplicationCreate from './credit-applications/add-new';
import ApplicationForgetPassword from './ApplicationForgetPassword';
import ApplicationResetPassword from './ApplicationResetPassword';


import { currentUserRole, currentCoSignerId } from '../apifile';

// async components
import {
	AsyncSessionLoginComponent,
	AsyncSessionRegisterComponent,
	AsyncSessionLockScreenComponent,
	AsyncSessionForgotPasswordComponent,
	AsyncSessionPage404Component,
	AsyncSessionPage500Component,
	AsyncTermsConditionComponent
} from 'Components/AsyncComponent/AsyncComponent';
import { NotificationManager } from 'react-notifications';
//Auth0
import Auth from '../Auth/Auth';

// callback component
//import Callback from "Components/Callback/Callback";

//Auth0 Handle Authentication
const auth = new Auth();

/*const handleAuthentication = ({ location }) => {
	console.log('eeeeeeeeeeeeeeeeeee')
	if (/access_token|id_token|error/.test(location.hash)) {
		auth.handleAuthentication();
	}
}*/

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, ...rest, authUser }) =>
	<Route
		{...rest}
		render={props =>
			authUser
				? <Component {...props} />
				: <Redirect
					to={{
						pathname: '/signin',
						state: { from: props.location }
					}}
				/>}
	/>;
/*const InitialPath = ({ component: Component, ...rest, authUser }) =>
	<Route
		{...rest}
		render={props =>
			authUser
				? <Redirect
				to={{
					pathname: '/signin',
					state: { from: props.location }
				}}
			/>
				: <Component {...props} />}
	/>;*/

class App extends Component {
	render() {
		
		const { location, match, user } = this.props;

		/*if(location.pathname === '/provider-register') {
			return (<Redirect to={'/provider-register'} />);
		} else if(!auth.isAuthenticated() && location.pathname != '/signin') {
			return <Redirect to='/signin' />
		}
*/

		if (location.pathname === '/') {
			if (user === null || !auth.isAuthenticated()) {
				return (<Redirect to={'/signin'} />);
			} else {
				if (currentUserRole() == 'HPS') {
					return (<Redirect to={'/admin/dashboard'} />);
				} else if (currentUserRole() == 'Provider') {
					return (<Redirect to={'/provider/dashboard'} />);
				} else if (currentUserRole() == 'Customers' && currentCoSignerId() == '') {
					return (<Redirect to={'/customer/dashboard'} />);
				} else if (currentCoSignerId() != '') {
					return (<Redirect to={'/cosigner/dashboard'} />);
				}
				//return (currentUserRole() != 'HPS') ? (<Redirect to={'/provider/dashboard'} />) : (<Redirect to={'/admin/dashboard'} />);
				//return (<Redirect to={'/admin/dashboard'} />);
			}
		} else if (!auth.isAuthenticated() && location.pathname != '/signin' && location.pathname != '/signup' && location.pathname != '/verify-email' && location.pathname != '/provider/create' && location.pathname != '/application/create' && location.pathname != '/application/forgot-password' && location.pathname != '/application/reset') {
			return <Redirect to='/signin' />
		} else if (auth.isAuthenticated() && location.pathname == '/signin') {
			if (currentUserRole() == 'HPS') {
				return (<Redirect to={'/admin/dashboard'} />);
			} else if (currentUserRole() == 'Provider') {
				return (<Redirect to={'/provider/dashboard'} />);
			} else if (currentUserRole() == 'Customers' && currentCoSignerId() == '') {
				return (<Redirect to={'/customer/dashboard'} />);
			} else if (currentCoSignerId() != '') {
				return (<Redirect to={'/cosigner/dashboard'} />);
			}
		}
		let userPath = `${match.url}admin`;
		if (currentUserRole() == 'Provider') {
			if (!location.pathname.includes('admin')) {
				userPath = `${match.url}provider`;
			}
		} else if (currentUserRole() == 'Customers' && currentCoSignerId() == '') {
			if (!location.pathname.includes('admin')) {
				userPath = `${match.url}customer`;
			}
		} else if (currentCoSignerId() != '') {
			if (!location.pathname.includes('admin')) {
				userPath = `${match.url}cosigner`;
			}
		}
		return (
			<RctThemeProvider>

				<InitialPath
					path={userPath}
					authUser={user}
					component={RctDefaultLayout}
				/>

				<Route path="/signin" component={AppSignIn} />
				<Route path="/verify-email" component={SignupVerifyEmail} />
				<Route path="/signup" component={AppSignUp} />
				<Route path="/provider/create" component={AppProviderCreate} />
				<Route path="/application/create" component={ApplicationCreate} />
				<Route path="/application/forgot-password" component={ApplicationForgetPassword} />
				<Route path="/application/reset" component={ApplicationResetPassword} />

				<Route path="/session/login" component={AsyncSessionLoginComponent} />
				<Route path="/session/register" component={AsyncSessionRegisterComponent} />
				<Route path="/session/lock-screen" component={AsyncSessionLockScreenComponent} />
				<Route
					path="/session/forgot-password"
					component={AsyncSessionForgotPasswordComponent}
				/>
				<Route path="/session/404" component={AsyncSessionPage404Component} />
				<Route path="/session/500" component={AsyncSessionPage500Component} />
				<Route path="/terms-condition" component={AsyncTermsConditionComponent} />

			</RctThemeProvider>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const { user } = authUser;
	return { user };
};

export default connect(mapStateToProps)(App);
