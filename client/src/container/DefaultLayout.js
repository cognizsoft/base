/**
 * App Routes
 */
import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

// app default layout
import RctAppLayout from 'Components/RctAppLayout';
import { ProviderAgreement } from "./agreement_index";
// router service
import routerService from "../services/_routerService";
import providerRouterService from "../services/_providerRouterService";
import customerRouterService from "../services/_customerRouterService";
import cosignerRouterService from "../services/_cosignerRouterService";
import { currentUserRole, currentCoSignerId } from '../apifile';
class DefaultLayout extends Component {
	render() {

		const { match } = this.props;
		return (
			(currentUserRole() == 'HPS') ?
				<RctAppLayout>
					{routerService && routerService.map((route, key) =>
						<Route exact key={key} path={`${match.url}/${route.path}`} component={route.component} />
					)}
				</RctAppLayout>
				:
				((currentUserRole() == 'Provider') ?
					<RctAppLayout>
						<div>
							<ProviderAgreement />
							{providerRouterService && providerRouterService.map((route, key) =>
								<Route exact key={key} path={`${match.url}/${route.path}`} component={route.component} />
							)}
						</div>
					</RctAppLayout>
					:
					(currentCoSignerId() == '') ?
						<RctAppLayout>
							{customerRouterService && customerRouterService.map((route, key) =>
								<Route exact key={key} path={`${match.url}/${route.path}`} component={route.component} />
							)}
						</RctAppLayout>
						:
						<RctAppLayout>
							{cosignerRouterService && cosignerRouterService.map((route, key) =>
								<Route exact key={key} path={`${match.url}/${route.path}`} component={route.component} />
							)}
						</RctAppLayout>

				)


		);
	}
}

export default withRouter(connect(null)(DefaultLayout));
