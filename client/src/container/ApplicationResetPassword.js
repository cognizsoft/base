/**
 * Sign Up With Firebase
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import { Fab } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import CryptoJS from 'crypto-js';
// components
import { SessionSlider } from 'Components/Widgets';
import { isEmpty, isContainWhiteSpace, isPassword } from '../validator/Validator';
// app config
import AppConfig from 'Constants/AppConfig';
import queryString from 'query-string';

// redux action
import {
   checkTokenInApp, resetPasswordInApp
} from 'Actions';

class ApplicationResetPassword extends Component {

   constructor(props) {
      super(props)
      // reset login status
      //this.props.dispatch(userActions.logout());
      this.state = {

         customerTypeDetail: {
            password: '',
            confirm_password: ''
         },

         errors: {},

         formData: {}, // Contains login form data
         formSubmitted: false, // Indicates submit status of login form
         loading: false // Indicates in progress state of login form
      }
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Oct 14,2019
   */
   componentDidMount() {
      //console.log(queryString.parse(this.props.location.search).check)
      if (queryString.parse(this.props.location.search).check != '') {
         var data = {
            'token': queryString.parse(this.props.location.search).check
         }
         this.props.checkTokenInApp(data);
      }
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { formData } = this.state;
      formData[name] = value;

      this.setState({
         formData: formData
      }, function () {
         this.validateSignUpForm(name, value)
      });
   }

   validateSubmit() {
      return (
         this.state.errors.confirm_password === '' &&
         this.state.errors.password === ''
      );
   }

   validateSignUpForm = (fieldName, value) => {
      let { errors } = this.state;
      //console.log(value)
      switch (fieldName) {
         case 'password':
            if (isEmpty(value)) {
               errors[fieldName] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               passwordError[fieldName] = "Password should not contain white spaces";
            } /*else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
                  passwordError[key] = "Password's length must between 6 to 16";
               } */else if (!isPassword(value, { min: 8, trim: true })) {
               errors[fieldName] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (this.state.customerTypeDetail.confirm_password !== '' && this.state.customerTypeDetail.confirm_password !== value) {
               errors[fieldName] = "Password and confirm password not match";
            } else if (this.state.customerTypeDetail.confirm_password !== '' && this.state.customerTypeDetail.confirm_password === value) {
               errors[fieldName] = "";
               errors['confirm_password'] = "";
            } else {
               errors[fieldName] = '';
            }
            break;
         case 'confirm_password':
            if (isEmpty(value)) {
               errors[fieldName] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               errors[fieldName] = "Password should not contain white spaces";
            } /*else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
                  passwordError[key] = "Password's length must between 6 to 16";
               } */else if (!isPassword(value, { min: 8, trim: true })) {
               errors[fieldName] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (value !== this.state.customerTypeDetail.password) {
               errors[fieldName] = "Password and confirm password not match";
            } else if (value === this.state.customerTypeDetail.password) {
               errors[fieldName] = "";
               errors['password'] = "";
            } else {
               errors[fieldName] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ errors: errors });

      this.setState({
         customerTypeDetail: {
            ...this.state.customerTypeDetail,
            [fieldName]: value
         }
      });
   }

   customerSignUp = (e) => {

      const { customerTypeDetail } = this.state;
      customerTypeDetail.user_id = this.props.passwordRestID;
      this.props.resetPasswordInApp(customerTypeDetail);

   }

   encryption(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   render() {
      const { errors } = this.state;
      const { loading } = this.props;
      
      if (this.props.redirectReset == 1) {
         return (<Redirect to={"/signin"} />);
      } else if (this.props.redirectReset == 2) {
         return (<Redirect to={"/application/forgot-password"} />);
      }
      //console.log(this.state)

      return (
         <QueueAnim type="bottom" duration={2000}>
            <div className="rct-session-wrapper">
               {loading &&
                  <LinearProgress />
               }

               <div className="session-inner-wrapper">
                  <div className="container">
                     <div className="row row-eq-height h-100 justify-content-center align-items-center">
                        <div className="health-login">
                           <div className="col-sm-3 col-md-3 col-lg-3">

                           </div>

                           <div className="col-sm-6 col-md-6 col-lg-6 center-login">
                              <div className="session-logo">
                                 <Link to="/">
                                    <img src={AppConfig.loginLogo} alt="Health Partner" className="img-fluid" />
                                 </Link>
                              </div>

                              {/*<div className="heading-container">
                                <h2>Health Financing Made Easy</h2>
                                <p>Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum</p>
                           </div>*/}
                              {(this.props.passwordRestID > 0) &&
                                 <div className="session-body text-center">

                                    <div className="signup-form">


                                       <div className="provider_fields">
                                          <FormGroup className="has-wrapper login-password">
                                             <TextField
                                                id="Password"
                                                fullWidth
                                                label="Password"
                                                name="password"
                                                type="password"
                                                placeholder="Enter password"
                                                error={(errors.password) ? true : false}
                                                helperText={errors.password}
                                                onChange={(e) => this.validateSignUpForm('password', e.target.value)}
                                             />
                                             <span className="has-icon"><i className="ti-lock"></i></span>
                                          </FormGroup>
                                          <FormGroup className="has-wrapper login-password">
                                             <TextField
                                                id="confirm_password"
                                                fullWidth
                                                label="Confirm Password"
                                                name="confirm_password"
                                                type="password"
                                                placeholder="Enter Confirm Password"
                                                error={(errors.confirm_password) ? true : false}
                                                helperText={errors.confirm_password}
                                                onChange={(e) => this.validateSignUpForm('confirm_password', e.target.value)}
                                             />
                                             <span className="has-icon"><i className="ti-lock"></i></span>
                                          </FormGroup>




                                          <FormGroup className="mb-15 mt-20">
                                             <Button
                                                color="primary"
                                                className={(this.validateSubmit()) ? "text-white btn-primary" : "text-white btn-error"}
                                                variant="contained"
                                                size="large"
                                                disabled={!this.validateSubmit()}
                                                onClick={this.customerSignUp}
                                             >
                                                Reset Password
                                       </Button>
                                          </FormGroup>
                                          <Link to="/signin" className="mb-10">Already have an account?</Link>
                                       </div>

                                    </div>
                                 </div>
                              }
                              {(this.props.passwordRestID == 0)?
                                 <div className="session-body text-center">

                                    <div className="signup-form">


                                       <div className="provider_fields">
                                          Your password reset link is not valid <br/>
                                          <Link to="/signin" className="mb-10">Already have an account?</Link>
                                       </div>

                                    </div>
                                 </div>
                              :''}
                           </div>

                           <div className="col-sm-3 col-md-3 col-lg-3">

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </QueueAnim>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
   //console.log('CustomerRegisterReducer')
   //console.log(CustomerRegisterReducer)
   const { loading, passwordRestID, redirectReset } = authUser;
   return { loading, passwordRestID, redirectReset };
};

export default connect(mapStateToProps, {
   checkTokenInApp, resetPasswordInApp
})(ApplicationResetPassword);
