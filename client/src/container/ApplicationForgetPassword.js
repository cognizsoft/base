/**
 * Sign Up With Firebase
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import { Fab } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import CryptoJS from 'crypto-js';
// components
import { SessionSlider } from 'Components/Widgets';
import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isContainWhiteSpace, isEmail, isPhone, formatPhoneNumber } from '../validator/Validator';
// app config
import AppConfig from 'Constants/AppConfig';

// redux action
import {
   forGetPasswordInApp
} from 'Actions';

class ApplicationForgetPassword extends Component {

   constructor(props) {
      super(props)
      // reset login status
      //this.props.dispatch(userActions.logout());
      this.state = {

         customerTypeDetail: {
            email: ''
         },

         errors: {},

         formData: {}, // Contains login form data
         formSubmitted: false, // Indicates submit status of login form
         loading: false // Indicates in progress state of login form
      }
   }


   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { formData } = this.state;
      formData[name] = value;

      this.setState({
         formData: formData
      }, function () {
         this.validateSignUpForm(name, value)
      });
   }

   validateSubmit() {
      return (
         this.state.errors.email === ''
      );
   }

   validateSignUpForm = (fieldName, value) => {
      let { errors } = this.state;
      //console.log(value)
      switch (fieldName) {
         case 'email':
            if (isEmpty(value)) {
               errors[fieldName] = "Email can't be blank";
            } else if (!isEmail(value) && value != '') {
               errors[fieldName] = "Please enter a valid email address.";
            } else {
               errors[fieldName] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ errors: errors });

      this.setState({
         customerTypeDetail: {
            ...this.state.customerTypeDetail,
            [fieldName]: value
         }
      });
   }

   customerSignUp = (e) => {

      const { customerTypeDetail } = this.state;
      //console.log(customerTypeDetail)
      this.props.forGetPasswordInApp(customerTypeDetail);

   }

   encryption(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   render() {
      const { customerTypeDetail, errors } = this.state;
      const { loading, redirectURL, last_insert_id } = this.props;

      if (this.props.redirectLink == 1) {
         return (<Redirect to={"/signin"} />);
      }
      //console.log(this.state)

      return (
         <QueueAnim type="bottom" duration={2000}>
            <div className="rct-session-wrapper">
               {loading &&
                  <LinearProgress />
               }

               <div className="session-inner-wrapper">
                  <div className="container">
                     <div className="row row-eq-height h-100 justify-content-center align-items-center">
                        <div className="health-login">
                           <div className="col-sm-3 col-md-3 col-lg-3">

                           </div>

                           <div className="col-sm-6 col-md-6 col-lg-6 center-login">
                              <div className="session-logo">
                                 <Link to="/">
                                    <img src={AppConfig.loginLogo} alt="Health Partner" className="img-fluid" />
                                 </Link>
                              </div>

                              {/*<div className="heading-container">
                                <h2>Health Financing Made Easy</h2>
                                <p>Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum</p>
                           </div>*/}

                              <div className="session-body text-center">

                                 <div className="signup-form">


                                    <div className="provider_fields">

                                       <FormGroup className="has-wrapper login-password">
                                          <TextField
                                             id="email"
                                             fullWidth
                                             label="Email"
                                             name="email"
                                             type="email"
                                             placeholder="Email"
                                             error={(errors.email) ? true : false}
                                             value={(customerTypeDetail.email != '') ? customerTypeDetail.email : ''}
                                             helperText={errors.email}
                                             onChange={(e) => this.validateSignUpForm('email', e.target.value)}
                                          />
                                          <span className="has-icon"><i className="ti-email"></i></span>
                                       </FormGroup>






                                       <FormGroup className="mb-15 mt-20">
                                          <Button
                                             color="primary"
                                             className={(this.validateSubmit()) ? "text-white btn-primary" : "text-white btn-error"}
                                             variant="contained"
                                             size="large"
                                             disabled={!this.validateSubmit()}
                                             onClick={this.customerSignUp}
                                          >
                                             Reset Password
                                       </Button>
                                       </FormGroup>
                                       <Link to="/signin" className="mb-10">Already have an account?</Link>
                                    </div>

                                 </div>
                              </div>

                           </div>

                           <div className="col-sm-3 col-md-3 col-lg-3">

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </QueueAnim>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser, CustomerRegisterReducer }) => {
   //console.log('CustomerRegisterReducer')
   //console.log(CustomerRegisterReducer)
   const { loading, redirectLink } = authUser;
   return { loading, redirectLink };
};

export default connect(mapStateToProps, {
   forGetPasswordInApp
})(ApplicationForgetPassword);
