/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import Button from '@material-ui/core/Button';
import "react-datepicker/dist/react-datepicker.css";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { RctCard } from 'Components/RctCard/index';
import CryptoJS from 'crypto-js';
import { isObjectEmpty } from '../../validator/Validator';

import {
   applicationFinalDetails, applicationEmail, uploadRejectionAgreement
} from 'Actions';
class applicationDeclined extends Component {
   state = {
      addData: {
         document: ''
      },
      add_err: {},
   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      //console.log(this.dec(this.props.match.params.id))
      this.props.applicationFinalDetails(this.dec(this.props.match.params.id));
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for send mail
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.applicationEmail(type, 0);
   }

   uploadCallAction(application_id, e) {


      let datafrom = new FormData();
      datafrom.append('application_id', application_id);
      datafrom.append('imgedata', this.state.addData.document);
      this.props.uploadRejectionAgreement(datafrom);

     
   }

   validateSubmit() {
      return (this.state.add_err.document === '');
   }

   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'document':
            value = value.target.files[0];
            if (isObjectEmpty(value)) {
               add_err[key] = "Please upload document before submitting.";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }

   
   render() {
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="credit-declined" />}
               match={this.props.match}
            />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     {this.props.finalDetails &&
                        <React.Fragment>
                           <div className="customer-invoice pt-10 pb-10 pl-50 pr-50" ref={el => (this.componentRef = el)}>
                              <div className="text-center">
                                 <div className="sender-address">
                                    <div className="invoice-logo mb-30">
                                       <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid" />
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div className="text-center">
                                    <h2><b>LINE OF CREDIT DECLINED</b></h2>
                                 </div>
                                 <div className="text-left">

                                    <p><strong>Name:</strong> {this.props.finalDetails.f_name + ' ' + this.props.finalDetails.m_name + ' ' + this.props.finalDetails.l_name}</p>
                                    <p><strong>Address:</strong> {this.props.finalDetails.address1 + ' ' + this.props.finalDetails.address2}</p>
                                    <p>{this.props.finalDetails.City + ', ' + this.props.finalDetails.name + ' - ' + this.props.finalDetails.zip_code}</p>
                                    <p><strong>Phone:</strong>{this.props.finalDetails.phone_no}</p>

                                 </div>
                                 <hr />
                                 <div>
                                    <p>
                                       We want to thank you applying for line of created. Based on the information provided, your application was evaluated and reviewed based on many criteria including your credit history.
                              </p>
                                    <p>
                                       We regret to inform you that we are unable to extend line of created at this time. We look forward to serve you in the future.
                              </p>
                                    <p>
                                       If you have any additional question, our friendly customer service associate are eager to help.
                              </p>
                                    <br />
                                    <p>
                                       Thanks, you very much for considering Health partner for your medical needs.
                              </p>
                                    <br />
                                    <br />
                                    <p>
                                       <strong>Sincerely</strong>
                                    </p>
                                    <p>
                                       <strong>Health Partner Inc.</strong>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           

                           <div className="invoice-head">
                              <div className="row">
                                 <div className="col-sm-6">
                                    <div className="row agreement_doc">
                                       <div className="col-md-6 upload-doc-input">
                                          <FormGroup>
                                             <Label for="document_upload">Upload Document<span className="required-field">*</span></Label><br />
                                             <Input
                                                type="file"
                                                name="document"
                                                id="document"
                                                onChange={(e) => this.onChnage('document', e)}
                                             >
                                             </Input>
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-2">
                                          <Label for="document_upload"></Label>
                                          
                                             <Button variant="contained" color="primary" className="text-white mr-10 mb-10" size="small" onClick={this.uploadCallAction.bind(this, this.dec(this.props.match.params.id))} disabled={!this.validateSubmit()}>
                                                Upload
                                             </Button>
                                         

                                       </div>
                                    </div>
                                 </div>
                                 <div className="col-sm-6 text-right">
                                    <ul className="list-inline agreement_print">

                                    <li><a href="javascript:void(0);" onClick={this.callAction.bind(this, this.dec(this.props.match.params.id))}><i className="mr-10 ti-email"></i> Email</a></li>
                                    <li>
                                       <ReactToPrint
                                          trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                          content={() => this.componentRef}
                                       />
                                    </li>
                                 </ul>
                                 </div>
                              </div>
                           </div>
                        </React.Fragment>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                  </RctCard>
               </div>
            </div>


         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, finalDetails } = creditApplication;
   return { loading, finalDetails }
}

export default connect(mapStateToProps, {
   applicationFinalDetails,applicationEmail,uploadRejectionAgreement
})(applicationDeclined); 