/**
 * Profile Page
 */
import React, { Component } from 'react';
import { FormGroup, Input, Form, Label, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';
import { formatPhoneNumber} from '../../../validator/Validator';
import moment from 'moment';
// intlmessages
import IntlMessages from 'Util/IntlMessages';

const Profile = ({ customerDetails }) => (
   <div className="profile-wrapper d-flex">
      <div className="modal-body page-form-outer view-section p-0">
         
         <div className="view-section-inner">

            <div className="view-box">
               {customerDetails &&
                  <React.Fragment>
                     <div className="width-50">
                        <table>
                           <tbody>
                              <tr>
                                 <td className="fw-bold">First Name : {customerDetails.first_name}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Middle Name : {(customerDetails.middle_initial)?customerDetails.middle_initial:'-'}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Last Name : {customerDetails.last_name}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Date of Birth : {moment(customerDetails.dob).format('MM/DD/YYYY')}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Phone No. : {(customerDetails.phones.length > 0) ? formatPhoneNumber(customerDetails.phones.filter(function (per) { return per.is_primary === true })[0].phone_number) : '-'}</td>
                              </tr>
                              
                              
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                              <tr>
                                 <td className="fw-bold">Account Number : {(customerDetails.account_number) ? customerDetails.account_number : '-'}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Email Address : {customerDetails.email_address}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Social Security Number : {customerDetails.ssn}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Gender : {customerDetails.gender}</td>
                              </tr>
                              <tr>
                                 <td className="fw-bold">Status : {customerDetails.status}</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </React.Fragment>
               }
            </div>

         </div>

      </div>
   </div>
);
export default Profile;