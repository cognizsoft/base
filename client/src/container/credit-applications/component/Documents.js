/**
 * Messages Page
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Input,
   InputGroup,
   InputGroupAddon,
   FormGroup,
   Label
} from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

const Documents = ({ providerDocument }) => (
         <div className="messages-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">

                 <div className="view-box">
                     <div className="width-100">
                        <table>
                          <thead>
                            <tr>
                              <th>Document Type</th>
                              <th>Document Name</th>
                              <th>Documents</th>
                            </tr>
                          </thead>
                           <tbody>
                             <tr>
                              
                               <td>{(providerDocument.file_type) ? providerDocument.file_type : '-'}</td>
                               
                               <td>{(providerDocument.document_name) ? providerDocument.document_name : '-'}</td>
                               
                               <td>{(providerDocument.document_upload) ? providerDocument.document_upload : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                 </div>

               </div>

            </div>
         </div>
      );
export default Documents;