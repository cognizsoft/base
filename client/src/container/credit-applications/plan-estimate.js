/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isSSN, isEmpty, formatSSNNumber, isDecimals } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


import {
   estimatePlan, estimatePlanClear, printEstimatePlan
} from 'Actions';
class planEstimate extends Component {
   state = {
      procedure_date: '',
      selectedPlan: null,
      loan: {
         loan_amount: "",
         score: "",
      },
      loanError: {},
   }



   /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.estimatePlanClear();
   }


   /*
   * Title :- onChnagePlanExist
   * Descrpation :- This function use for plan onchnage 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnagePlanExist(key, value) {
      let { loanError } = this.state;
      switch (key) {
         case 'loan_amount':
            if (isEmpty(value)) {
               loanError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Loan amount not valid";
            } else {
               loanError[key] = "";
            }
            break;
         case 'score':
            if (isEmpty(value)) {
               loanError[key] = "Credit score can't be blank";
            } else {
               loanError[key] = "";
            }
            break;

      }

      this.setState({
         loan: {
            ...this.state.loan,
            [key]: value
         }
      });
      this.setState({ loanError: loanError });
   }
   /*
   * Title :- validateSearchPlan
   * Descrpation :- This function use for validate data for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   validateSearchPlan() {
      return (
         this.state.loanError.loan_amount === '' &&
         this.state.loanError.score === ''
      )
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan according to customer
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   viewPlan() {
      this.props.estimatePlan(this.state.loan.score, this.state.loan.loan_amount);
      //this.props.searchCustomerApplication(this.state.existApplication);
   }
   printPlan() {
      //console.log(this.state.current_application_id)
      //console.log(this.state.loan.loan_amount)
      //console.log(this.state.selectedPlan)
      this.props.printEstimatePlan(this.state.loan.score, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   checkboxAction = (id, e) => {
      this.setState({ selectedPlan: (e.target.checked) ? id : null });
   }
   render() {
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.planEstimate" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left" ref={el => (this.componentRef = el)}>


                  <React.Fragment >


                     <div className="row" >
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="score">Credit Score<span className="required-field">*</span></Label><br />
                              <TextField
                                 type="text"
                                 name="score"
                                 id="score"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Enter Loan Amount"
                                 value={this.state.loan.score}
                                 error={(this.state.loanError.score) ? true : false}
                                 helperText={this.state.loanError.score}
                                 onChange={(e) => this.onChnagePlanExist('score', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="loan_amount">Enter Loan Amount($)<span className="required-field">*</span></Label><br />
                              <TextField
                                 type="text"
                                 name="loan_amount"
                                 id="loan_amount"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Enter Loan Amount"
                                 value={this.state.loan.loan_amount}
                                 error={(this.state.loanError.loan_amount) ? true : false}
                                 helperText={this.state.loanError.loan_amount}
                                 onChange={(e) => this.onChnagePlanExist('loan_amount', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">

                           <div className="mt-30">
                              <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 View Plan
                              </Button>
                              <Button onClick={this.printPlan.bind(this)} disabled={(this.state.selectedPlan == null) ? true : false} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 Print Plan
                              </Button>

                           </div>


                        </div>
                     </div>
                  </React.Fragment>

                  {this.props.paymentPlanAllowed &&
                     <div className="price-list user-permission" >
                        <div className="row row-eq-height">
                           {this.props.paymentPlanAllowed.map((plan, idx) => (
                              <div className="col-md-4" key={idx} >
                                 <div className="price-plan plan1 rct-block text-center">
                                    <div className="header">
                                       <h2 className="pricing-title">
                                          <span className="price">{plan.interest_rate}</span><span>%</span>
                                       </h2>
                                       <div className="description-text">Principal Amount: <span className="t-price">${this.props.estDetails.amount}</span></div>
                                       <label className="check-container">
                                          <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                          <span className="checkmark"></span>
                                       </label>
                                    </div>
                                    <div className="content ">
                                       <div className="page-form-outer select-plan-detail">
                                          <div className="row">

                                             <div className="col-md-10">Payment Plans</div>
                                             <div className="col-md-2">{idx + 1}</div>

                                             <div className="col-md-10">Payment Term (months)</div>
                                             <div className="col-md-2">{plan.term_month}</div>
                                          </div>

                                          <div className="price-list table-responsive">
                                             <table className="table table-bordered table-striped table-hover">
                                                <thead>
                                                   <tr className="table-head">
                                                      <th>SN</th>
                                                      <th>Date</th>
                                                      <th>Amount</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   {
                                                      plan.monthlyPlan.map((month, idm) => (

                                                         <tr key={idm}>
                                                            <td>{idm + 1}</td>
                                                            <td>{month.next_month}</td>
                                                            <td>${month.perMonth}</td>
                                                         </tr>
                                                      ))
                                                   }



                                                </tbody>
                                             </table>

                                          </div>
                                          {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                       </div>


                                    </div>
                                 </div>
                              </div>
                           ))}


                        </div>
                        {this.state.generatePlan &&
                           <div className="text-right">
                              <button disabled={!this.validatePlan()} className="btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, this.state.selectedPlan)}><span>Generate Invoice</span></button>
                           </div>
                        }
                     </div>

                  }
               </div>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>


         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, paymentPlanAllowed, estDetails } = creditApplication;
   return { loading, paymentPlanAllowed, estDetails }



}

export default connect(mapStateToProps, {
   estimatePlan, estimatePlanClear, printEstimatePlan
})(planEstimate); 