/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import {
   rejectApplication
} from 'Actions';

class rejectLatter extends Component {

   state = {

   }
   /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.rejectApplication(this.props.match.params.id);
   }
   printLatter() {
      window.print();
   }
   render() {
      const { loading, appRejectDetails, appRejectDetailsAddress } = this.props;

      //const addressDetails = (appDetailsAddress != '')?appDetailsAddress.filter(x => x.primary_address==1):'';

      console.log(appRejectDetailsAddress);
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.invoice" />} match={this.props.match} />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                              
                           </li>
                        </ul>

                     </div>
                     {appRejectDetails &&
                        <div className="rejection-letter p-50" ref={el => (this.componentRef = el)}>
                           <p>
                              {(appRejectDetails.gender == 'M') ? 'Mr.' : 'Mrs.'} {appRejectDetails.f_name + ' ' + appRejectDetails.m_name + ' ' + appRejectDetails.l_name}<br />
                              {appRejectDetailsAddress[0].region_name},<br />
                              {appRejectDetailsAddress[0].state_name},<br />
                              {appRejectDetailsAddress[0].country_name}<br /><br />
                           </p>

                           <p><strong>Subject: Credit Application Rejection Letter</strong></p>
                           <p>Dear {appRejectDetails.f_name + ' ' + appRejectDetails.m_name + ' ' + appRejectDetails.l_name},<br />
                              {appRejectDetails.note}
                              <br /><br />
                           </p>
                           <p>With Regards,</p>
                           <p>Director,<br />
                              Health Partner
                      </p>
                        </div>
                     }
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, appRejectDetails, appRejectDetailsAddress } = creditApplication;
   return { loading, appRejectDetails, appRejectDetailsAddress }

}

export default connect(mapStateToProps, {
   rejectApplication
})(rejectLatter);