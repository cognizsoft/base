/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import {
   planDetails, clearRedirectURL, allPlanDetails
} from 'Actions';
class PlanDetails extends Component {

   state = {
      payment_history: '',
      payment_history_data: false,
      nested_arr_row: '',
      nested_arr_row_data: false,
      plan_row: '',
      plan_row_data: false,
      paid_status: '',
      current_month_invoice_sum: 0,
      current_month_invoice: '',
      current_month_invoice_data: false,
      overdue_month_invoice_grp: '',
      overdue_month_invoice_grp_data: false,
      invoice_last_index_value: 0,
      all_plans_total_amount: 0,
      single_plan_total: 0
   }

   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      //console.log(this.props.match.params.id)
      this.props.clearRedirectURL();
      this.props.allPlanDetails(this.props.match.params.appid);
     // console.log(this.props.pp_details)
   }

   

   componentWillReceiveProps(nextProps) {
      //let { payment_history } = this.state;
      let pp_details = nextProps.pp_details;
      

      //CURRENT MONTH INSTALLMENT TOTAL SUM
    if(pp_details) {

      var currentdate = new Date();
      var cur_month = currentdate.getMonth() + 1;
      var cur_year = currentdate.getFullYear();

      var current_month_invoice_sum = 0;
      var current_month_invoice = [];
      var paid_status = 0;
      pp_details.forEach(function(data) {

        data.forEach(function(dat) {

          var date = dat.due_date;
          var date_parts = date.split("/");
          //console.log(date_parts)

          var month = date_parts[0];
          var year = date_parts[2];
          dat['month'] = month;
          dat['year'] = year;

          if(cur_month == month && cur_year == year) {
            current_month_invoice_sum += dat.installment_amt;
            current_month_invoice.push(dat);

            if(dat.paid_flag == 1) {
              paid_status = 1
            } else if(dat.paid_flag == 2) {
              paid_status = 2
            } else {
              paid_status = 0;
            }

          }

        })

      })
      /*console.log('current_month_invoice')
      console.log(current_month_invoice)*/

    }

    //OVERDUE MONTH INSTALLMENT TOTAL SUM
    if(pp_details) {

      var currentdate = new Date();
      var cur_month = currentdate.getMonth() + 1;
      var cur_year = currentdate.getFullYear();
      
      var overdue_month_invoice = [];
      pp_details.forEach(function(data) {

        data.forEach(function(dat) {

          var date = dat.due_date;
          var date_parts = date.split("/");
          
          var month = date_parts[0];
          var year = date_parts[2];
          dat['month'] = month;
          dat['year'] = year;

          if((month < cur_month && year <= cur_year) && dat.paid_flag !== 1) {
            overdue_month_invoice.push(dat);
            
          }

          

        })
        

      })

     var overdue_month_invoice_grp = [];
      overdue_month_invoice.reduce(function(res, value) {
        if (!res[value.due_date]) {
          
          res[value.due_date] = { due_date: value.due_date, installment_amt: 0, month: value.month, year: value.year, paid_flag: value.paid_flag };
          
          overdue_month_invoice_grp.push(res[value.due_date])
        }
        res[value.due_date].installment_amt += value.installment_amt;
        return res;
      }, {});

      //console.log(overdue_month_invoice_grp)


      //console.log(overdue_month_invoice)
      //console.log(dataa)

    }

      //PLANS DETAIL LIST ROW
    //PLAN LOAN AMOUNT
    if(pp_details) {

      var ppd_count = pp_details.length;
      
      var plan_row = [];

      for(var i=0;i<ppd_count;i++) {

        var ppd_countI = pp_details[i].length;
        
        plan_row[i]=[];

        var plan_loan_amount_sum = 0;
        var plan_paid_emi = 0;
        for(var j=0;j<ppd_countI;j++) {

          if(pp_details[i][j].installment_amt !== '' ) {
            plan_loan_amount_sum += pp_details[i][j].installment_amt;
          }
          if(pp_details[i][j].paid_flag == 1 ) {
            plan_paid_emi += pp_details[i][j].installment_amt;
          }
          

        }

        var plan_balance = plan_loan_amount_sum - plan_paid_emi;

        plan_row[i]['pp_id']=pp_details[i][0].pp_id;
        plan_row[i]['loan_amount']=plan_loan_amount_sum;
        plan_row[i]['paid_emi']=plan_paid_emi;
        plan_row[i]['balance']=plan_balance;
        plan_row[i]['emi']=pp_details[i][0].installment_amt;

      }
      //console.log(plan_row)

    }

      ////////////////////////////

      //INSTALLMENT LIST INVOICE HISTORY
    if(pp_details) {
      //pp_details = pp_details.sort()
      

      var nestedArrRow = [];
      var tpp = 0
      pp_details.forEach(function(data) {

        data.forEach(function(dat) {

          var datR = nestedArrRow.filter(x => x.due_date == dat.due_date );
          
          var dd = nestedArrRow.findIndex(x => x.due_date == dat.due_date );
          //console.log(datR)
          if(dd != -1) {
          //console.log(datR[0].installment_amt)
          //console.log(parseFloat(dat.installment_amt)+parseFloat(datR[0].installment_amt))
          nestedArrRow[dd].installment_amt = parseFloat(dat.installment_amt)+parseFloat(datR[0].installment_amt);

          //nestedArrRow[dd].total_plans_pay = tpp;
            

          } else {
            nestedArrRow.push(dat)
          }
          
        })
          
      })
      

      var getPaidBalncAll = (nestedArrRow) ? nestedArrRow.filter(x => x.installment_amt !== '' ) : '';
      var getTotal = (nestedArrRow) ? nestedArrRow.filter(x => x.installment_amt !== '' ) : '';
      if(getPaidBalncAll) {
         var paidInsTotal = 0;
         for (var i = 0; i<getPaidBalncAll.length; i++) {
           if(nestedArrRow[i].paid_flag == 1) {
              var dd = 0
              paidInsTotal += parseFloat(getPaidBalncAll[i].installment_amt+getPaidBalncAll[i].additional_amount);
              nestedArrRow[i].total_paid = paidInsTotal
           }

           // console.log('paidInsTotal')
           // console.log(paidInsTotal)
         }
      }

      if(getTotal) {
         var InsTotal = 0;
         for (var i = 0; i<getTotal.length; i++) {
           InsTotal += parseFloat(getTotal[i].installment_amt);
         }
      }

      
      // console.log('InsTotal')
      // console.log(InsTotal)

      // console.log('nestedArrRow-----')
      // console.log(nestedArrRow)
          
      }

      ////////////////////////////

      
        
      this.setState({
        plan_row: plan_row,
        plan_row_data: true,
        paid_status: paid_status,
        current_month_invoice_sum: current_month_invoice_sum,
        current_month_invoice: current_month_invoice,
        current_month_invoice_data: true,
        overdue_month_invoice_grp: overdue_month_invoice_grp,
        overdue_month_invoice_grp_data: true,
        all_plans_total_amount: InsTotal,
        nested_arr_row: nestedArrRow,
        nested_arr_row_data: true,
      })
        

   }

   //////////////////
   /////////////////
   ///////////////

   invoice_history_details() {

    this.setState({
      payment_history_data: false,
      nested_arr_row_data: true
    })

   }

   payment_history_details(pp_details, pp_id) {

      if(pp_details) {
        console.log('pp_details')
        console.log(pp_details)
        var sixdata = [];
        var selected_plan_length = '';

        pp_details.forEach(function(data) {

          if(data[0].pp_id == pp_id) {
            selected_plan_length = data.length;
            sixdata.push(data.filter(x => x.pp_id == pp_id))
          }
          
        })
        // $(this.refs.slide).slideUp();
        // $(this.refs.slide).slideDown();
        

        

        ///////////

        var getPaidBalncPlan = (sixdata) ? sixdata[0].filter(x => x.installment_amt !== '' ) : '';
        var getTotalPlan = (sixdata) ? sixdata[0].filter(x => x.installment_amount !== '' ) : '';
        if(getPaidBalncPlan) {
           var paidInsTotal = 0;
           for (var i = 0; i<getPaidBalncPlan.length; i++) {
             if(sixdata[0][i].paid_flag == 1) {
                var dd = 0
                paidInsTotal += parseFloat(getPaidBalncPlan[i].installment_amount);
                sixdata[0][i].total_plan_paid = paidInsTotal
             }

             // console.log('paidInsTotal')
             // console.log(paidInsTotal)
           }
        }

        if(getTotalPlan) {
           var InsTotalPlan = 0;
           for (var i = 0; i<getTotalPlan.length; i++) {
             InsTotalPlan += parseFloat(getTotalPlan[i].installment_amount);
           }
        }
        
        //////////

        if(sixdata[0].length === selected_plan_length) {  
          this.setState({
            payment_history: sixdata[0],
            payment_history_data: true,
            nested_arr_row_data: false,
            single_plan_total: InsTotalPlan
          })
        }

      }

   }

   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails, application_details, pp_details, application_plans } = this.props;
      let { payment_history, payment_history_data, nested_arr_row, nested_arr_row_data, plan_row, plan_row_data, paid_status, current_month_invoice_sum, current_month_invoice, current_month_invoice_data, overdue_month_invoice_grp, overdue_month_invoice_grp_data, invoice_last_index_value, all_plans_total_amount, single_plan_total } = this.state;
      
      console.log('pp_details-------')
      console.log(pp_details)

      var appId = this.props.match.params.appid
       ////////////////////////////
      ////////NEW CHANGES/////////
     ////////////////////////////

    //LOAN AMOUNT//
    if(application_plans) {

      var loan_amount_sum = 0;
      application_plans.forEach(function(data) {

        if(data.amount !== '') {
          loan_amount_sum += data.amount;
        }

      })

    }

    //CURRENT BALANCE//
    if(pp_details) {

      var paid_balance = 0;
      var paid_balance_arr = [];
      pp_details.forEach(function(data) {
        
        data.forEach(function(dat) {

          if(dat.paid_flag == 1) {
            paid_balance += dat.installment_amt;
          }
          if(dat.amount_paid !== 0) {
             paid_balance_arr.push(dat);
          }

        })

      })

      var paid_balance_arr_sum = 0;
      if(paid_balance_arr) {
        paid_balance_arr.forEach(function(data) {
            paid_balance_arr_sum += data.amount_paid;
        })
      }

      if(application_details) {
        var current_balance = (loan_amount_sum - paid_balance_arr_sum).toFixed(2);
      }
      
    }

    //LATE FEE COUNT//
    if(pp_details) {

      var late_fee_count = 0;
      pp_details.forEach(function(data) {
        
        data.forEach(function(dat) {

          if(dat.late_flag == 1) {
            late_fee_count += dat.late_flag;
          }

        })

      })
      //console.log(late_fee_count)
    }

    //MISSED PAYMENT COUNT//
    if(pp_details) {
      
      var missed_payment_count = 0;
      pp_details.forEach(function(data) {
        
        data.forEach(function(dat) {

          if(dat.missed_flag == 1) {
            missed_payment_count += dat.missed_flag;
          }

        })

      })
      //console.log(missed_payment_count)
    }

    //LATE FEE RECIEVED SUM//
    if(pp_details) {
      
      var late_fee_received_sum = 0;
      pp_details.forEach(function(data) {
        
        data.forEach(function(dat) {

          if(dat.late_fee_received !== null) {
            late_fee_received_sum += dat.late_fee_received;
          }

        })

      })
      //console.log(missed_payment_count)
    }


    //FINANCIAL CHARGE RECIEVED SUM//
    if(pp_details) {
      
      var financial_charge_recieved_sum = 0;
      pp_details.forEach(function(data) {
        
        data.forEach(function(dat) {

          if(dat.fin_charge_amt !== null) {
            financial_charge_recieved_sum += dat.fin_charge_amt;
          }

        })

      })
      //console.log(missed_payment_count)
    }
    
    

    

    
       
      //last payment rcvd
      var lastPymntRcvd = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).pop() : '-';
      
     //next pymnt due date
      var nxtPymntDueDate = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 0 ) : '';

      //payment missed count
      var countPymntMis = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.missed_flag == 1 ) : '';
      
      //last payment rcvd
      var pastPymntDue = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).reverse() : '-';
      
      //late fee amount
      var lateFeeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.late_flag == 1 ) : '';

      if(lateFeeAmt) {
         var lateFeeTotal = 0;
         for (var i = 0; i<lateFeeAmt.length; i++) {
           lateFeeTotal += parseFloat(lateFeeAmt[i].late_fee_received);
         }
      }

      //fin charge amount
      var finChargeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.fin_charge_amt !== 0 && x.fin_charge_amt !== null ) : '';
      //console.log(finChargeAmt);
      if(finChargeAmt) {
         var finChargeTotal = 0;
         for(var i=0; i<finChargeAmt.length; i++) {
            finChargeTotal += parseFloat(finChargeAmt[i].fin_charge_amt);
         }
      }

      //current balance
      /*var getPaidBalnc = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ) : '';
      if(getPaidBalnc) {
         var paidInsTotal = 0;
         for (var i = 0; i<getPaidBalnc.length; i++) {
           paidInsTotal += parseFloat(getPaidBalnc[i].installment_amt);
         }
      }*/
      
      //balnce for table row
      var crdtBalnce = (mainPlan) ? mainPlan[0].credit_amount : 0;
      var blnc = (mainPlan) ? mainPlan[0].credit_amount : 0;

      //total additional amount
      var addiAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.additional_amount !== 0 && x.additional_amount !== null ) : '';
      if(addiAmt) {
         var addiAmtTotal = 0;
         for(var i=0; i<addiAmt.length; i++) {
            addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
         }
      }
      
      //var appId = (mainPlan) ? mainPlan[0].application_id : '';

      var countPaymentDetailRow = (paymentPlanDetails) ? paymentPlanDetails.length : 0;
      //console.log(countPaymentDetailRow);
      //check payment is due
      //console.log(appId);
      //var app
      //console.log(nxtPymntDueDat.due_date)
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">
            
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                           </li>
                        </ul>
                     </div>
                     <div className="p-10" ref={el => (this.componentRef = el)}>
                        <h1 className="text-center mb-20">Customer Account</h1>
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Customer Information</th>
                                 </tr>
                                 <tr>
                                    <td><strong>Application No :</strong></td>
                                    <td>{(application_details) ? application_details[0].application_no : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Name :</strong></td>
                                    <td className="text-capitalize">{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Address :</strong></td>
                                    <td>{(application_details) ? (application_details[0].address1 + ', ' + application_details[0].City + ', ' + application_details[0].state_id) : '-'}
                                    
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Phone :</strong></td>
                                    <td>{(application_details) ? application_details[0].peimary_phone : '-'}</td>
                                 </tr>
                              
                                 </tbody>
                              </table>
                           
                           </div>
                          
                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Loan Information</th>
                                 </tr>
                                 <tr>
                                    <td><strong>Approved Amount :</strong></td>
                                    <td>{(application_details) ? '$'+parseFloat(application_details[0].approve_amount).toFixed(2) : '$0.00'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Loan Amount :</strong></td>
                                    <td>{(loan_amount_sum) ? '$'+parseFloat(loan_amount_sum).toFixed(2) : '$0.00'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Outstanding Balance :</strong></td>
                                    <td>{'$'+parseFloat(current_balance).toFixed(2)}</td>
                                 </tr>
                                 </tbody>
                              </table>
                           
                           </div>

                            <div className="add-card">
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Payment Information</th>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Late Fee Payments :</strong></td>
                                    <td>
                                       {late_fee_count}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Payments Missed :</strong></td>
                                    <td>
                                       {missed_payment_count}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Late Fees :</strong></td>
                                    <td>
                                       {(late_fee_received_sum) ? '$'+parseFloat(late_fee_received_sum).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Financial Charges :</strong></td>
                                    <td>
                                       {(financial_charge_recieved_sum) ? '$'+parseFloat(financial_charge_recieved_sum).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>

                        <div className="table-responsive mb-40 pymt-history">
                        <h2 className="text-center mb-10">Outstanding Invoice</h2>
                           <table className="table table-borderless admin-application-payment-plan">
                              <thead>
                                <tr>
                                  <th>Invoice of</th>
                                  <th>Amount</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                  <td>Current Month</td>
                                  <td>{(current_month_invoice_sum) ? '$'+parseFloat(current_month_invoice_sum).toFixed(2) : '$0.00'}</td>
                                  <td>
                                    {(paid_status == 1) ? 'Paid' : ((paid_status == 0) ? 'Unpaid' : 'In Progress')}
                                  </td>
                                 </tr>

                                 {overdue_month_invoice_grp_data && overdue_month_invoice_grp.map((invoice, idx) => {
                                    return (
                                       <tr key={idx}>
                                        <td>Overdue ({invoice.due_date})</td>
                                        <td>{'$'+parseFloat(invoice.installment_amt).toFixed(2)}</td>
                                        <td>
                                          {
                                             (invoice.paid_flag == 1) ? 'Paid' : (invoice.paid_flag == 0) ? 'Unpaid' : 'In Progress' 
                                          }
                                        </td>
                                       </tr> 
                                    )
                                 })}
                                 
                              </tbody>
                           </table>
                        </div>

                        <div className="table-responsive mb-40 pymt-history">
                        <h2 className="text-center mb-10">Payment Plans <i className="ti-arrow-right"></i> <a href="javascript:void(0);" onClick={() => this.invoice_history_details()}>View Invoice History</a></h2>
                           <table className="table table-borderless admin-application-payment-plan">
                              <thead>
                                 <tr>
                                    <th>Plan ID</th>
                                    <th>Loan Amount</th>
                                    <th>Balance Amount</th>
                                    <th>EMI</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {plan_row_data && plan_row.map((plan, idx) => {
                                    return (
                                       <tr key={idx} onClick={() => this.payment_history_details(pp_details, plan.pp_id)}>
                                       <td>{plan.pp_id}</td>
                                       <td>
                                          {'$'+parseFloat(plan.loan_amount).toFixed(2)}
                                       </td>
                                       <td>
                                          {'$'+parseFloat(plan.balance).toFixed(2)}
                                       </td>
                                       <td>
                                          {'$'+parseFloat(plan.emi).toFixed(2)}
                                       </td>
                                       <td>
                                          <a href="javascript:void(0);" onClick={() => this.payment_history_details(pp_details, plan.pp_id)}><i className="zmdi zmdi-eye icon-fr"></i></a>
                                       </td>
                                       </tr>
                                    );
                                 })}
                              </tbody>
                           </table>
                        </div>

                        {nested_arr_row_data &&
                          <div className="table-responsive mb-40 pymt-history">
                          <h2 className="text-center mb-10">Invoice History</h2>
                           <table className="table table-borderless admin-application-payment-plan" ref="slide">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Payment Date</th>
                                    <th>Due Date</th>
                                    <th>Amount Paid</th>
                                    <th>Additional Amount</th>
                                    <th>EMI</th>
                                    <th>Late Fee</th>
                                    <th>Finacial Charge</th>
                                    <th>Total Paid</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {nested_arr_row_data && nested_arr_row.map(function(row, idx) {
                                    return <tr 
                                       key={idx} 
                                       className={new Date(row.comparison_date)<new Date() && row.paid_flag == 0 ? "late-fee-payment" : ""}
                                       >
                                       <td>{idx + 1}</td>
                                       <td>
                                          {(row.payment_date) ? row.payment_date : '-'}
                                       </td>
                                       <td>
                                          {(row.due_date) ? row.due_date : '-'}
                                       </td>
                                       <td>
                                          {(row.paid_flag == 1) ? '$'+parseFloat(row.installment_amt).toFixed(2) : ((row.partial_paid == 1 && row.paid_flag == 2) ? '$'+parseFloat(row.additional_amt).toFixed(2) : ((row.paid_flag == 2) ? '$'+parseFloat(row.installment_amt).toFixed(2) : (row.partial_paid == 1) ? '$'+parseFloat(row.amount_paid).toFixed(2) : '-'))}
                                       </td>
                                       <td>
                                          {(row.additional_amount) ? '$'+parseFloat(row.additional_amount).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(row.installment_amt) ? '$'+parseFloat(row.installment_amt).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(row.late_fee_received) ? '$'+parseFloat(row.late_fee_received).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(row.fin_charge_amt) ? '$'+parseFloat(row.fin_charge_amt).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(row.paid_flag == 1) ? ((row.installment_total || row.partial_total !== null) ? parseFloat(row.installment_amt + row.additional_amount).toFixed(2) : '-') : '-'}
                                       </td>
                                       <td>
                                          {(row.paid_flag == 1) ? ((row.total_paid && row.additional_amount) ? '$'+parseFloat(all_plans_total_amount - row.total_paid).toFixed(2) : '-') : '-'}
                                       </td>
                                       <td>{(row.paid_flag == 1) ? 'Paid' : ((row.partial_paid == 1 && row.paid_flag == 2) ? 'Partial Paid' : (row.paid_flag == 2) ? 'In progress' : (row.partial_paid == 1) ? 'Partial Paid' : 'Unpaid')}</td>
                                    </tr>
                                 })}
                                 
                              </tbody>
                           </table>
                          </div>
                        }

                        {payment_history_data && 
                          <div className="table-responsive mb-40 pymt-history">
                          <h2 className="text-center mb-10">Payment History</h2>
                           <table className="table table-borderless admin-application-payment-plan" ref="slide">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Payment Date</th>
                                    <th>Due Date</th>
                                    <th>Amount Paid</th>
                                    <th>EMI</th>
                                    <th>Total Paid</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {payment_history_data && payment_history.map(function(plan, idx) {
                                    return <tr 
                                       key={idx} 
                                       className={new Date(plan.comparison_date)<new Date() && plan.paid_flag == 0 ? "late-fee-payment" : ""}
                                       >
                                       <td>{idx + 1}</td>
                                       <td>
                                          {(plan.payment_date) ? plan.payment_date : '-'}
                                       </td>
                                       <td>
                                          {(plan.due_date) ? plan.due_date : '-'}
                                       </td>
                                       <td>
                                          {(plan.paid_flag == 1) ? '$'+parseFloat(plan.amount_paid).toFixed(2) : ((plan.partial_paid == 1 && plan.paid_flag == 2) ? '$'+parseFloat(plan.additional_amt).toFixed(2) : ((plan.paid_flag == 2) ? '$'+parseFloat(plan.installment_amount).toFixed(2) : ((plan.partial_paid == 1) ? '$'+parseFloat(plan.amount_paid).toFixed(2) : '-')))}
                                          
                                       </td>
                                       <td>
                                          {(plan.installment_amount) ? '$'+parseFloat(plan.installment_amount).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(plan.paid_flag == 1) ? ((plan.installment_total || plan.partial_total !== null) ? '$'+parseFloat(plan.installment_amount).toFixed(2) : '-') : '-'}
                                       </td>
                                       <td>
                                          {(plan.paid_flag == 1) ? ((plan.total_plan_paid) ? '$'+parseFloat(single_plan_total - plan.total_plan_paid).toFixed(2) : '$'+parseFloat(single_plan_total - plan.total_plan_paid).toFixed(2)) : '-'}
                                       </td>
                                       <td>{(plan.paid_flag == 1) ? 'Paid' : ((plan.partial_paid == 1 && plan.paid_flag == 2) ? 'Partial Paid' : (plan.paid_flag == 2) ? 'In progress' : ((plan.partial_paid == 1) ? 'Partial Paid' : 'Unpaid'))}</td>
                                    </tr>
                                 })}
                                 
                              </tbody>
                           </table>
                          </div>
                        }
                        {/**<div className="note-wrapper row">
                           <div className="invoice-note col-sm-12 col-md-8">
                              <h2 className="invoice-title">Note</h2>
                              <p className="fs-14">Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.</p>
                           </div>

                        </div>**/}
                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer }) => {
   console.log(PaymentPlanReducer);
   const { nameExist, isEdit } = authUser;
   const { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL } = creditApplication;
   const {application_details, application_plan_details, pp_details, application_plans} = PaymentPlanReducer;
   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, application_details, application_plan_details, pp_details, application_plans, redirectURL }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails
})(PlanDetails);