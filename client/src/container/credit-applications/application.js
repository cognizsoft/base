/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


import {
  viewApplication
} from 'Actions';
class ApplicationView extends Component {

  state = {
    all: false,
    users: null, // initial user data

  }


  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    //this.permissionFilter(this.state.currentModule);
    this.props.viewApplication(this.props.match.params.id);
  }




  render() {
    const { loading, appDetails, appDetailsAddress } = this.props;
    return (
      <div className="country regions">
        <Helmet>
          <title>Health Partner | Providers | Add New</title>
          <meta name="description" content="Regions" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.viewApplication" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          {appDetails &&
            <div className="table-responsive">
              <div className="modal-body page-form-outer view-section">

                <div className="view-section-inner">
                  <h4>Customer Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">First Name:</td>
                            <td>{appDetails.f_name}</td>
                            <td className="fw-bold">Phone No. 2:</td>
                            <td>{(appDetails.alternative_phone) ? appDetails.alternative_phone : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Middle Name:</td>
                            <td>{(appDetails.m_name != '') ? appDetails.m_name : '-'}</td>
                            <td className="fw-bold">Email Address:</td>
                            <td>{appDetails.email}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Last Name:</td>
                            <td>{appDetails.l_name}</td>
                            <td className="fw-bold">Social Security Number:</td>
                            <td>{appDetails.ssn}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Date of Birth:</td>
                            <td>{appDetails.dob}</td>
                            <td className="fw-bold">Gender:</td>
                            <td>{(appDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Phone No. 1:</td>
                            <td>{appDetails.peimary_phone}</td>
                            <td className="fw-bold">Customer A/C:</td>
                            <td>{appDetails.patient_ac}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Status:</td>
                            <td>{(appDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                            <td className="fw-bold">Application No</td>
                            <td>{appDetails.application_no}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="view-section-inner">
                  <h4>Address</h4>

                  {appDetailsAddress && appDetailsAddress.map((address, idx) => (
                    <div key={idx}>
                      <h3 className="location-title">Address {idx + 1}</h3>
                      <div className="view-box mb-10" >
                        <div className="width-100">
                          <table>
                            <tbody>
                              <tr>
                                <td className="fw-bold">Address1:</td>
                                <td>{address.address1}</td>
                                <td className="fw-bold">City:</td>
                                <td>{address.City}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Address2:</td>
                                <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                <td className="fw-bold">Zip Code:</td>
                                <td>{address.zip_code}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Country:</td>
                                <td>{address.country_name}</td>
                                <td className="fw-bold">How long at this address?:</td>
                                <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">State:</td>
                                <td>{address.state_name}</td>
                                <td className="fw-bold">Phone No:</td>
                                <td>{address.phone_no}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Primary Address:</td>
                                <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                <td className="fw-bold">Billing and physical address same:</td>
                                <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                              </tr>
                              {/*<tr>
                              <td className="fw-bold">County:</td>
                              <td>{(address.county != '') ? address.county : '-'}</td>
                              <td className="fw-bold">Region:</td>
                              <td>{address.region_name}</td>
                              
                            </tr>*/}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  ))
                  }


                </div>

                <div className="view-section-inner">
                  <h4>Employment Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Employed:</td>
                            <td>{(appDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                            <td className="fw-bold">Employer Name:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_name : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Employment Type:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.emp_type_name : '-'}</td>
                            <td className="fw-bold">Employer Phone No:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_phone : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Annual Income($):</td>
                            <td>{(appDetails.employment_status == 1) ? '$' + appDetails.annual_income : '-'}</td>
                            <td className="fw-bold">Employer Email Address:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_email : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Employed Since:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_since : '-'}</td>
                            <td className="fw-bold">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="view-section-inner">
                  <h4>Bank Details</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Bank Name:</td>
                            <td>{appDetails.bank_name}</td>
                            <td className="fw-bold">Name on Account:</td>
                            <td>{appDetails.account_name}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Routing Number:</td>
                            <td>{appDetails.rounting_no}</td>
                            <td className="fw-bold">Bank A/C Type:</td>
                            <td>{appDetails.account_type}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Bank A/C#:</td>
                            <td>{appDetails.account_number}</td>
                            <td className="fw-bold">Bank Address</td>
                            <td>{(appDetails.bank_address != '') ? appDetails.bank_address : '-'}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="view-section-inner">
                  <h4>User Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Username:</td>
                            <td>{appDetails.username}</td>
                            <td className="fw-bold">Phone 1:</td>
                            <td>{appDetails.user_phone1}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Email Address:</td>
                            <td>{appDetails.user_eamil}</td>
                            <td className="fw-bold">Phone 2:</td>
                            <td>{(appDetails.user_phone2 != '') ? appDetails.user_phone2 : '-'}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>




              </div>
            </div>
          }

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication }) => {
  const { loading, appDetails, appDetailsAddress } = creditApplication;
  return { loading, appDetails, appDetailsAddress }
}
export default connect(mapStateToProps, {
  viewApplication
})(ApplicationView);