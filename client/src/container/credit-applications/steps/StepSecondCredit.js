/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepSecondCredit = ({
   addErr,
   addData,
   onChnagerovider,
   handleShareholderNameChange,
   handleRemoveShareholder,
   handleAddShareholder,
   shareholders,
   sameBilling,
   countriesList,
   stateType,
   regionType,
   blillingStateType,
   blillingRegionType,
   handleRemoveShareholderCo,
   handleAddShareholderCo,
   stateTypeCo,
   co_sameBilling,
   CoStateType,
}) => (

      <div className="modal-body page-form-outer text-left">
         <h4 className="blue-small-title">Customer Address</h4>
         {addData.location.map((shareholder, idx) => (
            <div key={idx} aa={idx}>
               {(() => {
                  if (idx != 0) {
                     return (
                        <React.Fragment>
                           <div className="row">
                              <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                              <div className="col-md-2">
                                 <a href="#" onClick={(e) => handleRemoveShareholder(idx)}>Remove Address (-)</a>
                              </div>
                           </div>
                        </React.Fragment>
                     )
                  }

               })()}
               <div className="row">


                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="address1">Address1<span className="required-field">*</span></Label><br />
                        <TextField
                           type="text"
                           name="address1"
                           id="address1"
                           fullWidth
                           variant="outlined"
                           placeholder="Address1"
                           value={(addData.location[idx].address1 != '') ? addData.location[idx].address1 : ''}
                           error={(addErr.location[idx].address1) ? true : false}
                           helperText={(addErr.location[idx].address1 != '') ? addErr.location[idx].address1 : ''}
                           onChange={(e) => onChnagerovider('address1', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="address2">Address2</Label><br />
                        <TextField
                           type="text"
                           name="address2"
                           id="address2"
                           fullWidth
                           variant="outlined"
                           placeholder="Address2"
                           value={(addData.location[idx].address2 != '') ? addData.location[idx].address2 : ''}
                           onChange={(e) => onChnagerovider('address2', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="city">City<span className="required-field">*</span></Label><br />
                        <TextField
                           type="text"
                           name="city"
                           id="city"
                           fullWidth
                           variant="outlined"
                           placeholder="City"
                           value={(addData.location[idx].city != '') ? addData.location[idx].city : ''}
                           error={(addErr.location[idx].city) ? true : false}
                           helperText={(addErr.location[idx].city != '') ? addErr.location[idx].city : ''}
                           onChange={(e) => onChnagerovider('city', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="state">State<span className="required-field">*</span></Label>
                        <Input
                           type="select"
                           name="state"
                           id="state"
                           placeholder=""
                           value={addData.location[idx].state}
                           onChange={(e) => onChnagerovider('state', e.target.value, idx)}
                        >
                           <option value="">Select</option>
                           {stateType[idx] && stateType[idx].map((state, key) => (
                              <option value={state.state_id} key={key}>{state.name}</option>
                           ))}

                        </Input>
                        {(addErr.location[idx].state != '' && addErr.location[idx].state !== undefined) ? <FormHelperText>{addErr.location[idx].state}</FormHelperText> : ''}
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="country">Country<span className="required-field">*</span></Label>
                        <Input
                           type="select"
                           name="country"
                           id="country"
                           value={addData.location[idx].country}
                           onChange={(e) => onChnagerovider('country', e.target.value, idx)}
                        >

                           {countriesList && countriesList.map((country, key) => (
                              <option value={country.id} key={key}>{country.name}</option>
                           ))}

                        </Input>
                        {(addErr.location[idx].country != '' && addErr.location[idx].country !== undefined) ? <FormHelperText>{addErr.location[idx].country}</FormHelperText> : ''}
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="zip_code">Zip Code<span className="required-field">*</span></Label><br />
                        <TextField
                           type="text"
                           name="zip_code"
                           id="zip_code"
                           fullWidth
                           variant="outlined"
                           placeholder="Zip Code"
                           value={(addData.location[idx].zip_code != '') ? addData.location[idx].zip_code : ''}
                           error={(addErr.location[idx].zip_code) ? true : false}
                           helperText={(addErr.location[idx].zip_code != '') ? addErr.location[idx].zip_code : ''}
                           onChange={(e) => onChnagerovider('zip_code', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="phone_no">Phone No<span className="required-field">*</span></Label><br />
                        <TextField
                           type="text"
                           name="phone_no"
                           id="phone_no"
                           fullWidth
                           variant="outlined"
                           inputProps={{ maxLength: 14 }}
                           placeholder="Phone No"
                           value={(addData.location[idx].phone_no != '') ? addData.location[idx].phone_no : ''}
                           error={(addErr.location[idx].phone_no) ? true : false}
                           helperText={(addErr.location[idx].phone_no != '') ? addErr.location[idx].phone_no : ''}
                           onChange={(e) => onChnagerovider('phone_no', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  {/*<div className="col-md-4">
                        <FormGroup>
                           <Label for="region">Region<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="region"
                              id="region"
                              placeholder=""
                              value={addData.location[idx].region}
                              onChange={(e) => onChnagerovider('region', e.target.value, idx)}
                           >
                              <option value="">Select</option>
                              {regionType[idx] && regionType[idx].map((region, key) => (
                                 <option value={region.region_id} key={key}>{region.name}</option>
                              ))}

                           </Input>
                           {(addErr.location[idx].region != '' && addErr.location[idx].region !== undefined) ? <FormHelperText>{addErr.location[idx].region}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="county">County</Label>
                           <TextField
                              type="text"
                              name="county"
                              id="county"
                              fullWidth
                              variant="outlined"
                              placeholder="county"
                              value={(addData.location[idx].county != '') ? addData.location[idx].county : ''}
                              error={(addErr.location[idx].county) ? true : false}
                              helperText={(addErr.location[idx].county != '') ? addErr.location[idx].county : ''}
                              onChange={(e) => onChnagerovider('county', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     */}









                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="how_long">How long at this address? (Years)</Label><br />
                        <TextField
                           type="text"
                           name="how_long"
                           id="how_long"
                           fullWidth
                           variant="outlined"
                           placeholder="How long at this address? (Years)"
                           error={(addErr.location[idx].how_long) ? true : false}
                           helperText={(addErr.location[idx].how_long != '') ? addErr.location[idx].how_long : ''}
                           value={(addData.location[idx].how_long != '') ? addData.location[idx].how_long : ''}
                           onChange={(e) => onChnagerovider('how_long', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>


                  <div className="col-md-12">
                     <FormGroup tag="fieldset">
                        <FormGroup check>
                           <Label check>
                              <Input
                                 type="checkbox"
                                 name="primary_address"
                                 value={1}
                                 checked={(addData.location[idx].primary_address == 1) ? true : false}
                                 onChange={(e) => onChnagerovider('primary_address', e.target.value, idx)}
                              />
                              Set as primary address
              </Label>
                        </FormGroup>
                        <FormGroup check>
                           <Label check>
                              <Input
                                 type="checkbox"
                                 name="billing_address"
                                 value={1}
                                 checked={(addData.location[idx].billing_address == 1) ? true : false}
                                 onChange={(e) => onChnagerovider('billing_address', e.target.value, idx)}
                              />
                              Billing and physical address same
              </Label>
                        </FormGroup>
                        {(addErr.primary_address != '') ? <FormHelperText>{addErr.primary_address}</FormHelperText> : ''}
                     </FormGroup>
                  </div>
               </div>
               <br />
               {(() => {
                  if (idx == 0) {
                     return (
                        <React.Fragment>

                           <h4 className={(sameBilling) ? "blue-small-title d-none" : "blue-small-title"}>Billing Address</h4>

                           <div className={(sameBilling) ? "row d-none" : "row"}>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_address1">Address1<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_address1"
                                       id="billing_address1"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="Address1"
                                       value={(addData.billing_address1 != '') ? addData.billing_address1 : ''}
                                       error={(addErr.billing_address1) ? true : false}
                                       helperText={(addErr.billing_address1 != '') ? addErr.billing_address1 : ''}
                                       onChange={(e) => onChnagerovider('billing_address1', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>

                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_address2">Address2</Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_address2"
                                       id="billing_address2"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="Address2"
                                       value={(addData.billing_address2 != '') ? addData.billing_address2 : ''}
                                       onChange={(e) => onChnagerovider('billing_address2', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_city">City<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_city"
                                       id="billing_city"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="City"
                                       value={(addData.billing_city != '') ? addData.billing_city : ''}
                                       error={(addErr.billing_city) ? true : false}
                                       helperText={(addErr.billing_city != '') ? addErr.billing_city : ''}
                                       onChange={(e) => onChnagerovider('billing_city', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_state">State<span className="required-field">*</span></Label>
                                    <Input
                                       type="select"
                                       name="billing_state"
                                       id="billing_state"
                                       placeholder=""
                                       value={addData.billing_state}
                                       onChange={(e) => onChnagerovider('billing_state', e.target.value)}
                                    >
                                       <option value="">Select</option>
                                       {blillingStateType && blillingStateType.map((state, key) => (
                                          <option value={state.state_id} key={key}>{state.name}</option>
                                       ))}

                                    </Input>
                                    {(addErr.billing_state != '' && addErr.billing_state !== undefined) ? <FormHelperText>{addErr.billing_state}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_country">Country<span className="required-field">*</span></Label>
                                    <Input
                                       type="select"
                                       name="billing_country"
                                       id="billing_country"
                                       value={addData.billing_country}
                                       onChange={(e) => onChnagerovider('billing_country', e.target.value)}
                                    >

                                       {countriesList && countriesList.map((country, key) => (
                                          <option value={country.id} key={key}>{country.name}</option>
                                       ))}

                                    </Input>
                                    {(addErr.billing_country != '' && addErr.billing_country !== undefined) ? <FormHelperText>{addErr.billing_country}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_zip_code"
                                       id="billing_zip_code"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="Zip Code"
                                       value={(addData.billing_zip_code != '') ? addData.billing_zip_code : ''}
                                       error={(addErr.billing_zip_code) ? true : false}
                                       helperText={(addErr.billing_zip_code != '') ? addErr.billing_zip_code : ''}
                                       onChange={(e) => onChnagerovider('billing_zip_code', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>

                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_phone_no">Phone No<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_phone_no"
                                       id="billing_phone_no"
                                       fullWidth
                                       variant="outlined"
                                       inputProps={{ maxLength: 14 }}
                                       placeholder="Phone No"
                                       value={(addData.billing_phone_no != '') ? addData.billing_phone_no : ''}
                                       error={(addErr.billing_phone_no) ? true : false}
                                       helperText={(addErr.billing_phone_no != '') ? addErr.billing_phone_no : ''}
                                       onChange={(e) => onChnagerovider('billing_phone_no', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>

                              {/*<div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_region">Region<span className="required-field">*</span></Label>
                                    <Input
                                       type="select"
                                       name="billing_region"
                                       id="billing_region"
                                       placeholder=""
                                       value={addData.billing_region}
                                       onChange={(e) => onChnagerovider('billing_region', e.target.value)}
                                    >
                                       <option value="">Select</option>
                                       {blillingRegionType && blillingRegionType.map((region, key) => (
                                          <option value={region.region_id} key={key}>{region.name}</option>
                                       ))}

                                    </Input>
                                    {(addErr.billing_region != '' && addErr.billing_region !== undefined) ? <FormHelperText>{addErr.billing_region}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_county">County</Label>
                                    <TextField
                                       type="text"
                                       name="billing_county"
                                       id="billing_county"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="county"
                                       value={(addData.billing_county != '') ? addData.billing_county : ''}
                                       error={(addErr.billing_county) ? true : false}
                                       helperText={(addErr.billing_county != '') ? addErr.billing_county : ''}
                                       onChange={(e) => onChnagerovider('billing_county', e.target.value)}
                                    />
                                 </FormGroup>
                                       </div>*/}











                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="billing_how_long">How long at this address? (Years)</Label><br />
                                    <TextField
                                       type="text"
                                       name="billing_how_long"
                                       id="billing_how_long"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="How long at this address? (Years)"
                                       error={(addErr.billing_how_long) ? true : false}
                                       helperText={(addErr.billing_how_long != '') ? addErr.billing_how_long : ''}
                                       value={(addData.billing_how_long != '') ? addData.billing_how_long : ''}
                                       onChange={(e) => onChnagerovider('billing_how_long', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>


                           </div>
                        </React.Fragment>
                     )
                  }

               })()}
            </div>
         ))}
         <div className="row">
            <div className="col-md-12">
               <a href="#" onClick={(e) => handleAddShareholder()}>Add more Address (+)</a>
            </div>
         </div>
         <div>
            <hr className="border-dark" />
            <div className="row">
               <div className="col-md-12"><h4 className="blue-small-title">Secondary Contact Address</h4></div>
            </div>
            <div className="row">
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_address1">Address1<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="sd_address1"
                        id="sd_address1"
                        fullWidth
                        variant="outlined"
                        placeholder="Address1"
                        value={(addData.sd_address1 != '') ? addData.sd_address1 : ''}
                        error={(addErr.sd_address1) ? true : false}
                        helperText={(addErr.sd_address1 != '') ? addErr.sd_address1 : ''}
                        onChange={(e) => onChnagerovider('sd_address1', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_address2">Address2</Label><br />
                     <TextField
                        type="text"
                        name="sd_address2"
                        id="sd_address2"
                        fullWidth
                        variant="outlined"
                        placeholder="Address2"
                        value={(addData.sd_address2 != '') ? addData.sd_address2 : ''}
                        onChange={(e) => onChnagerovider('sd_address2', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_city">City<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="sd_city"
                        id="sd_city"
                        fullWidth
                        variant="outlined"
                        placeholder="City"
                        value={(addData.sd_city != '') ? addData.sd_city : ''}
                        error={(addErr.sd_city) ? true : false}
                        helperText={(addErr.sd_city != '') ? addErr.sd_city : ''}
                        onChange={(e) => onChnagerovider('sd_city', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_state">State<span className="required-field">*</span></Label>
                     <Input
                        type="select"
                        name="sd_state"
                        id="sd_state"
                        placeholder=""
                        value={addData.sd_state}
                        onChange={(e) => onChnagerovider('sd_state', e.target.value)}
                     >
                        <option value="">Select</option>
                        {CoStateType && CoStateType.map((state, key) => (
                           <option value={state.state_id} key={key}>{state.name}</option>
                        ))}

                     </Input>
                     {(addErr.sd_state != '' && addErr.sd_state !== undefined) ? <FormHelperText>{addErr.sd_state}</FormHelperText> : ''}
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_country">Country<span className="required-field">*</span></Label>
                     <Input
                        type="select"
                        name="sd_country"
                        id="sd_country"
                        value={addData.sd_country}
                        onChange={(e) => onChnagerovider('sd_country', e.target.value)}
                     >

                        {countriesList && countriesList.map((country, key) => (
                           <option value={country.id} key={key}>{country.name}</option>
                        ))}

                     </Input>
                     {(addErr.sd_country != '' && addErr.sd_country !== undefined) ? <FormHelperText>{addErr.sd_country}</FormHelperText> : ''}
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="sd_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="sd_zip_code"
                        id="sd_zip_code"
                        fullWidth
                        variant="outlined"
                        placeholder="Zip Code"
                        value={(addData.sd_zip_code != '') ? addData.sd_zip_code : ''}
                        error={(addErr.sd_zip_code) ? true : false}
                        helperText={(addErr.sd_zip_code != '') ? addErr.sd_zip_code : ''}
                        onChange={(e) => onChnagerovider('sd_zip_code', e.target.value)}
                     />
                  </FormGroup>
               </div>
            </div>
         </div>
         <div className={(addData.co_signer != 1) ? 'd-none' : ''}>
            <hr className="border-dark" />
            <div className="row">
               <div className="col-md-8"><h4 className="blue-small-title">Co-signer Address</h4></div>
               <div className="col-md-4">
                  <FormGroup tag="fieldset">
                     <FormGroup check>
                        <Label check>
                           <Input
                              type="checkbox"
                              name="co_signer_same_add"
                              value={1}
                              checked={(addData.co_signer_same_add == 1) ? true : false}
                              onChange={(e) => onChnagerovider('co_signer_same_add', e)}
                           />
                           <strong>Co-signer and secondary contact are same?</strong>
                        </Label>
                     </FormGroup>
                  </FormGroup>
               </div>
            </div>
            {addData.co_location.map((shareholder, idx) => (
               <div key={idx} aa={idx}>
                  {(() => {
                     if (idx != 0) {
                        return (
                           <React.Fragment>
                              <div className="row">
                                 <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                                 <div className="col-md-2">
                                    <a href="#" onClick={(e) => handleRemoveShareholderCo(idx)}>Remove Address (-)</a>
                                 </div>
                              </div>
                           </React.Fragment>
                        )
                     }

                  })()}
                  <div className="row">


                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_address1">Address1<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="co_address1"
                              id="co_address1"
                              fullWidth
                              variant="outlined"
                              placeholder="Address1"
                              value={(addData.co_location[idx].co_address1 != '') ? addData.co_location[idx].co_address1 : ''}
                              error={(addErr.co_location[idx].co_address1) ? true : false}
                              helperText={(addErr.co_location[idx].co_address1 != '') ? addErr.co_location[idx].co_address1 : ''}
                              onChange={(e) => onChnagerovider('co_address1', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>

                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_address2">Address2</Label><br />
                           <TextField
                              type="text"
                              name="co_address2"
                              id="co_address2"
                              fullWidth
                              variant="outlined"
                              placeholder="Address2"
                              value={(addData.co_location[idx].co_address2 != '') ? addData.co_location[idx].co_address2 : ''}
                              onChange={(e) => onChnagerovider('co_address2', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_city">City<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="co_city"
                              id="co_city"
                              fullWidth
                              variant="outlined"
                              placeholder="City"
                              value={(addData.co_location[idx].co_city != '') ? addData.co_location[idx].co_city : ''}
                              error={(addErr.co_location[idx].co_city) ? true : false}
                              helperText={(addErr.co_location[idx].co_city != '') ? addErr.co_location[idx].co_city : ''}
                              onChange={(e) => onChnagerovider('co_city', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_state">State<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="co_state"
                              id="co_state"
                              placeholder=""
                              value={addData.co_location[idx].co_state}
                              onChange={(e) => onChnagerovider('co_state', e.target.value, idx)}
                           >
                              <option value="">Select</option>
                              {stateTypeCo[idx] && stateTypeCo[idx].map((state, key) => (
                                 <option value={state.state_id} key={key}>{state.name}</option>
                              ))}

                           </Input>
                           {(addErr.co_location[idx].co_state != '' && addErr.co_location[idx].co_state !== undefined) ? <FormHelperText>{addErr.co_location[idx].co_state}</FormHelperText> : ''}
                        </FormGroup>
                     </div>

                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_country">Country<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="co_country"
                              id="co_country"
                              value={addData.co_location[idx].co_country}
                              onChange={(e) => onChnagerovider('co_country', e.target.value, idx)}
                           >

                              {countriesList && countriesList.map((country, key) => (
                                 <option value={country.id} key={key}>{country.name}</option>
                              ))}

                           </Input>
                           {(addErr.co_location[idx].co_country != '' && addErr.co_location[idx].co_country !== undefined) ? <FormHelperText>{addErr.co_location[idx].co_country}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="co_zip_code"
                              id="co_zip_code"
                              fullWidth
                              variant="outlined"
                              placeholder="Zip Code"
                              value={(addData.co_location[idx].co_zip_code != '') ? addData.co_location[idx].co_zip_code : ''}
                              error={(addErr.co_location[idx].co_zip_code) ? true : false}
                              helperText={(addErr.co_location[idx].co_zip_code != '') ? addErr.co_location[idx].co_zip_code : ''}
                              onChange={(e) => onChnagerovider('co_zip_code', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_phone_no">Phone No<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="co_phone_no"
                              id="co_phone_no"
                              fullWidth
                              variant="outlined"
                              inputProps={{ maxLength: 14 }}
                              placeholder="Phone No"
                              value={(addData.co_location[idx].co_phone_no != '') ? addData.co_location[idx].co_phone_no : ''}
                              error={(addErr.co_location[idx].co_phone_no) ? true : false}
                              helperText={(addErr.co_location[idx].co_phone_no != '') ? addErr.co_location[idx].co_phone_no : ''}
                              onChange={(e) => onChnagerovider('co_phone_no', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>


                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="co_how_long">How long at this address? (Years)</Label><br />
                           <TextField
                              type="text"
                              name="co_how_long"
                              id="co_how_long"
                              fullWidth
                              variant="outlined"
                              placeholder="How long at this address? (Years)"
                              error={(addErr.co_location[idx].co_how_long) ? true : false}
                              helperText={(addErr.co_location[idx].co_how_long != '') ? addErr.co_location[idx].co_how_long : ''}
                              value={(addData.co_location[idx].co_how_long != '') ? addData.co_location[idx].co_how_long : ''}
                              onChange={(e) => onChnagerovider('co_how_long', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>


                     <div className="col-md-12">
                        <FormGroup tag="fieldset">
                           <FormGroup check>
                              <Label check>
                                 <Input
                                    type="checkbox"
                                    name="co_primary_address"
                                    value={1}
                                    checked={(addData.co_location[idx].co_primary_address == 1) ? true : false}
                                    onChange={(e) => onChnagerovider('co_primary_address', e.target.value, idx)}
                                 />
                              Set as primary address
              </Label>
                           </FormGroup>
                           <FormGroup check>
                              <Label check>
                                 <Input
                                    type="checkbox"
                                    name="co_billing_address"
                                    value={1}
                                    checked={(addData.co_location[idx].co_billing_address == 1) ? true : false}
                                    onChange={(e) => onChnagerovider('co_billing_address', e.target.value, idx)}
                                 />
                              Billing and physical address same
              </Label>
                           </FormGroup>
                           {(addErr.co_primary_address != '') ? <FormHelperText>{addErr.co_primary_address}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                  </div>
                  <br />
                  {(() => {
                     if (idx == 0) {
                        return (
                           <React.Fragment>

                              <h4 className={(co_sameBilling) ? "blue-small-title d-none" : "blue-small-title"}>Billing Address</h4>

                              <div className={(co_sameBilling) ? "row d-none" : "row"}>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_address1">Address1<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_address1"
                                          id="co_billing_address1"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Address1"
                                          value={(addData.co_billing_address1 != '') ? addData.co_billing_address1 : ''}
                                          error={(addErr.co_billing_address1) ? true : false}
                                          helperText={(addErr.co_billing_address1 != '') ? addErr.co_billing_address1 : ''}
                                          onChange={(e) => onChnagerovider('co_billing_address1', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>

                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_address2">Address2</Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_address2"
                                          id="co_billing_address2"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Address2"
                                          value={(addData.co_billing_address2 != '') ? addData.co_billing_address2 : ''}
                                          onChange={(e) => onChnagerovider('co_billing_address2', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_city">City<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_city"
                                          id="co_billing_city"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="City"
                                          value={(addData.co_billing_city != '') ? addData.co_billing_city : ''}
                                          error={(addErr.co_billing_city) ? true : false}
                                          helperText={(addErr.co_billing_city != '') ? addErr.co_billing_city : ''}
                                          onChange={(e) => onChnagerovider('co_billing_city', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_state">State<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="co_billing_state"
                                          id="co_billing_state"
                                          placeholder=""
                                          value={addData.co_billing_state}
                                          onChange={(e) => onChnagerovider('co_billing_state', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {blillingStateType && blillingStateType.map((state, key) => (
                                             <option value={state.state_id} key={key}>{state.name}</option>
                                          ))}

                                       </Input>
                                       {(addErr.co_billing_state != '' && addErr.co_billing_state !== undefined) ? <FormHelperText>{addErr.co_billing_state}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_country">Country<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="co_billing_country"
                                          id="co_billing_country"
                                          value={addData.co_billing_country}
                                          onChange={(e) => onChnagerovider('co_billing_country', e.target.value)}
                                       >

                                          {countriesList && countriesList.map((country, key) => (
                                             <option value={country.id} key={key}>{country.name}</option>
                                          ))}

                                       </Input>
                                       {(addErr.co_billing_country != '' && addErr.co_billing_country !== undefined) ? <FormHelperText>{addErr.co_billing_country}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_zip_code"
                                          id="co_billing_zip_code"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Zip Code"
                                          value={(addData.co_billing_zip_code != '') ? addData.co_billing_zip_code : ''}
                                          error={(addErr.co_billing_zip_code) ? true : false}
                                          helperText={(addErr.co_billing_zip_code != '') ? addErr.co_billing_zip_code : ''}
                                          onChange={(e) => onChnagerovider('co_billing_zip_code', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>

                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_phone_no">Phone No<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_phone_no"
                                          id="co_billing_phone_no"
                                          fullWidth
                                          variant="outlined"
                                          inputProps={{ maxLength: 14 }}
                                          placeholder="Phone No"
                                          value={(addData.co_billing_phone_no != '') ? addData.co_billing_phone_no : ''}
                                          error={(addErr.co_billing_phone_no) ? true : false}
                                          helperText={(addErr.co_billing_phone_no != '') ? addErr.co_billing_phone_no : ''}
                                          onChange={(e) => onChnagerovider('co_billing_phone_no', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>


                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="co_billing_how_long">How long at this address? (Years)</Label><br />
                                       <TextField
                                          type="text"
                                          name="co_billing_how_long"
                                          id="co_billing_how_long"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="How long at this address? (Years)"
                                          error={(addErr.co_billing_how_long) ? true : false}
                                          helperText={(addErr.co_billing_how_long != '') ? addErr.co_billing_how_long : ''}
                                          value={(addData.co_billing_how_long != '') ? addData.co_billing_how_long : ''}
                                          onChange={(e) => onChnagerovider('co_billing_how_long', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>


                              </div>
                           </React.Fragment>
                        )
                     }

                  })()}
               </div>
            ))}
            <div className="row">
               <div className="col-md-12">
                  <a href="#" onClick={(e) => handleAddShareholderCo()}>Add more Address (+)</a>
               </div>
            </div>
         </div>

      </div>

   );

export default StepSecondCredit;