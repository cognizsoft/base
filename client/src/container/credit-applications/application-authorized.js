/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';

import "react-datepicker/dist/react-datepicker.css";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { RctCard } from 'Components/RctCard/index';
import CryptoJS from 'crypto-js';


import {
   applicationFinalDetails,applicationEmail
} from 'Actions';
class applicationAuthorized extends Component {
   state = {

   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      this.props.applicationFinalDetails(this.dec(this.props.match.params.id));
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for send mail
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.applicationEmail(type,1);
   }
   render() {
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="credit-authorized" />}
               match={this.props.match}
            />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     {this.props.finalDetails &&
                        <React.Fragment>
                           <div className="customer-invoice pt-10 pb-10 pl-50 pr-50" ref={el => (this.componentRef = el)}>
                              <div className="text-center">
                                 <div className="sender-address">
                                    <div className="invoice-logo mb-30">
                                       <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid" />
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div className="text-center">
                                    <h2><b>LINE OF CREDIT APPROVAL</b></h2>
                                 </div>
                                 <div className="text-left">

                                    <p><strong>Name:</strong> {this.props.finalDetails.f_name + ' ' + this.props.finalDetails.m_name + ' ' + this.props.finalDetails.l_name}</p>
                                    <p><strong>Address:</strong> {this.props.finalDetails.address1 + ' ' + this.props.finalDetails.address2}</p>
                                    <p>{this.props.finalDetails.City + ', ' + this.props.finalDetails.name + ' - ' + this.props.finalDetails.zip_code}</p>
                                    <p><strong>Phone:</strong>{this.props.finalDetails.phone_no}</p>

                                 </div>
                                 <hr />
                                 <div>
                                    <p><strong>Congratulation, your credit application has been approved. The following is your line of credit details:</strong></p>
                                    <br />
                                    <p>
                                       <strong>Account Number:</strong> {this.props.finalDetails.patient_ac}
                                    </p>
                                    <p>
                                       <strong>Application Number:</strong> {this.props.finalDetails.application_no}
                                    </p>
                                    <p>
                                       <strong>Amount Approved:</strong> ${this.props.finalDetails.approve_amount}
                                    </p>
                                    <p>
                                       <strong>Authorization date:</strong> {this.props.finalDetails.authorization_date}
                                    </p>
                                    <p>
                                       <strong>Authorization expiration:</strong> {this.props.finalDetails.expiry_date}
                                    </p>
                                    <br />
                                    <p>
                                       To take advantage of the line of credit, please present this letter of authorization to any medical practice or doctor in our network.
                              </p>
                                    <p>
                                       If you have any additional question, our friendly customer service associate are eager to help.
                              </p>
                                    <br />
                                    <p>
                                       Thanks, you very much for your business!
                              </p>
                                    <br />
                                    <br />
                                    <p>
                                       <strong>Sincerely</strong>
                                    </p>
                                    <p>
                                       <strong>Health Partner Inc.</strong>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div className="invoice-head text-right">
                              <ul className="list-inline">

                                 <li><a href="javascript:void(0);" onClick={this.callAction.bind(this, this.dec(this.props.match.params.id))} ><i className="mr-10 ti-email"></i> Email</a></li>
                                 <li>
                                    <ReactToPrint
                                       trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                       content={() => this.componentRef}
                                    />
                                 </li>
                              </ul>
                           </div>
                        </React.Fragment>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                  </RctCard>
               </div>
            </div>


         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, finalDetails } = creditApplication;
   return { loading, finalDetails }
}

export default connect(mapStateToProps, {
   applicationFinalDetails,applicationEmail
})(applicationAuthorized); 