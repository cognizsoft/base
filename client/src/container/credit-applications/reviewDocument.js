import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import {
   Button,
   Form,
   FormGroup,
   Label,
   Input,
   FormText,
   Col,
   FormFeedback
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { URL } from './../../apifile/URL';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isEmpty } from '../../validator/Validator';
import {
   creditApplicationReviewDocument, creditApplicationManualActions, creditApplicationRemoveManualActions
} from 'Actions';

class uploadDocument extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         comment: ''
      },
      add_err: {},
      uploadDocumentRedirect: 0,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.creditApplicationReviewDocument(this.props.match.params.id);
   }
   /*
   * Title :- onChnage
   * Descrpation :- This function use check on change value
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   /*
   * Title :- validateSubmit
   * Descrpation :- This function use for check validation
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   validateSubmit() {
      return (this.state.add_err.comment === '');
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for submit appication status
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.creditApplicationManualActions(this.props.match.params.id, type, this.state.addData.comment);

   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check updated props
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentWillReceiveProps(nextProps) {
      if (nextProps.manualType && nextProps.manualType != '') {
         this.props.creditApplicationRemoveManualActions();
         this.setState({ uploadDocumentRedirect: nextProps.manualType })
      }
   }

   render() {
      if (this.state.uploadDocumentRedirect == 1) {
         return (<Redirect to={`/admin/customers/customerPayment-plan/${this.props.match.params.id}`} />);
      } else if (this.state.uploadDocumentRedirect == 2) {
         return (<Redirect to={`/admin/credit-application/reject/${this.props.match.params.id}`} />);
      }
      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsreviewDocCreditApplications" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">

                     {this.props.appDocDetails &&
                        <Form>
                           <h4>Credit Application Information</h4>
                           <div className="row">
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Application No:</strong> {this.props.appDocDetails.application_no}</div>
                              </div>
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Amount:</strong> ${this.props.appDocDetails.amount}</div>
                              </div>
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Credit Score :</strong> {this.props.appDocDetails.score}</div>
                              </div>
                           </div>
                           <hr />
                           <h4>Credit Application Document</h4>
                           <div className="row">
                              {this.props.docDetails && this.props.docDetails.map((doc, idx) => (
                                 <div className="col-md-6" key={idx}>
                                    <div>{doc.name}</div>
                                    {(() => {
                                       var docType = doc.file_path.split(".");
                                       if (docType[docType.length - 1] == 'pdf' || docType[docType.length - 1] == 'png' || docType[docType.length - 1] == 'jpg' || docType[docType.length - 1] == 'jpeg') {
                                          return (
                                             <embed src={`${URL.APIURL + '/' + doc.file_path}`} width="500px" height="400px" />
                                          )
                                       } else {
                                          return (
                                             <div>Preview not available <a href={`${URL.APIURL + '/' + doc.file_path}`}>click here</a> to download </div>
                                          )
                                       }

                                    })()}

                                 </div>

                              ))
                              }
                           </div>
                           <hr />
                           <div className="row">
                              <div className="col-md-12">
                                 <FormGroup>
                                    <Label for="comment">Your Comment <span className="required-field">*</span></Label><br />
                                    <Input
                                       type="textarea"
                                       name="comment"
                                       id="comment"
                                       variant="outlined"
                                       placeholder="Enter your comment before any action"
                                       defaultValue={this.state.addData.comment}
                                       onChange={(e) => this.onChnage('comment', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.add_err.comment) ? <FormHelperText>{this.state.add_err.comment}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-12">
                                 <div className="mps-submt-btn text-right">
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 1)} disabled={!this.validateSubmit()}>Approve</Button>
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 2)} disabled={!this.validateSubmit()}>Reject</Button>
                                 </div>
                              </div>
                           </div>
                        </Form>

                     }
                  </div>


               </div>


               {this.props.loading &&
                  <RctSectionLoader />
               }     


            </RctCollapsibleCard>

         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, appDocDetails, docDetails, manualType } = creditApplication;
   return { loading, appDocDetails, docDetails, manualType }

}

export default connect(mapStateToProps, {
   creditApplicationReviewDocument, creditApplicationManualActions, creditApplicationRemoveManualActions
})(uploadDocument);