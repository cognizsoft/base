/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isSSN, isEmpty, formatSSNNumber, isDecimals } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';


import {
   searchCustomerApplication, paymentPlan, applicationOption, getSpeciality, planReset, createPaymentPlan, printPlan
} from 'Actions';
class searchApplication extends Component {
   state = {
      currentModule: 26,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      existApplication: {
         exist_first_name: "",
         exist_last_name: "",
         exist_dob: "",
         exist_ssn: "",
         application_no: "",
      },
      existError: {},
      startDateExist: "",
      changeURL: 0,
      showSearchPlan: false,
      current_application_id: null,
      generatePlan: false,
      selectedPlan: null,
      remainingAmount: 0.00,
      procedure_date: '',
      loan: {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         speciality_type: "",
      },
      loanError: {},
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.applicationOption();
      this.props.planReset(1);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   /*
   * Title :- onChnageExist
   * Descrpation :- This function use for onchnage
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnageExist(key, value) {
      let { existError } = this.state;
      switch (key) {
         case 'exist_first_name':
            if (isEmpty(value)) {
               existError[key] = "First Name can't be blank";
            } else {
               existError[key] = '';
            }

            break;
         case 'exist_last_name':
            if (isEmpty(value)) {
               existError[key] = "Last Name can't be blank";
            } else {
               existError[key] = '';
            }

            break;
         case 'exist_ssn':
            if (isEmpty(value)) {
               existError[key] = "SSN can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               existError[key] = "SSN not valid";
            } else {
               value = formatSSNNumber(value)
               existError[key] = '';
            }
            //delete existError.application_no;
            break;
         case 'exist_dob':
            if (value == null) {
               existError[key] = "Select Date of Birth";
               this.setState({ startDateExist: '' })
            } else {
               this.setState({ startDateExist: value })
               value = moment(value).format('YYYY-MM-DD');
               existError[key] = '';
            }

            break;
         case 'application_no':
            if (isEmpty(value)) {
               existError[key] = "Application number can't be blank";
            } else {
               existError[key] = '';
            }
            //delete existError.exist_ssn;
            break;
      }

      this.setState({
         existApplication: {
            ...this.state.existApplication,
            [key]: value
         }
      });
      this.setState({ existError: existError });
   }
   /*
   * Title :- searchCustomer
   * Descrpation :- This function use for search custoemr details
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerSubmit() {
      this.props.searchCustomerApplication(this.state.existApplication);
      this.props.planReset(0);
      const loan = {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         speciality_type: "",
      }
      const loanError = {}
      this.setState({ showSearchPlan: false, current_application_id: null, generatePlan: false, loan: loan, procedure_date: '', selectedPlan: null, loanError: loanError });
   }
   /*
   * Title :- searchCustomerReset
   * Descrpation :- This function use for reset page
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerReset() {
      /*let { existError } = this.state;
      existError['exist_first_name'] = "";
      existError['exist_last_name'] = "";
      existError['exist_ssn'] = "";
      existError['exist_dob'] = "";
      existError['application_no'] = "";*/

      this.props.planReset(1);
      const loan = {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         speciality_type: "",
      }
      const loanError = {}
      this.setState({
         existApplication: {
            exist_first_name: "",
            exist_last_name: "",
            exist_dob: "",
            exist_ssn: "",
            application_no: "",
         },
         existError: {},
         startDateExist: "",
         showSearchPlan: false,
         current_application_id: null,
         generatePlan: false,
         loan: loan, procedure_date: '',
         selectedPlan: null,
         loanError: loanError,
      }, () => {
         //this.props.removeSearchCustomer();
      })
   }
   /*
   * Title :- validateSearch
   * Descrpation :- This function use for validate data
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   validateSearch() {
      return (
         (
            this.state.existError.exist_first_name === '' &&
            this.state.existError.exist_last_name === '' &&
            this.state.existError.exist_dob === ''
         ) ||
         (
            this.state.existError.exist_ssn === ''
         ) ||
         (
            this.state.existError.application_no === ''
         )


      )
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   searchPlan(patient_id, application_id, type, remainingAmount, e) {
      this.props.planReset(0);
      const loan = {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         speciality_type: "",
      }
      const loanError = {}
      this.setState({ showSearchPlan: true, current_application_id: application_id, generatePlan: (type) ? true : false, loan: loan, procedure_date: '', selectedPlan: null, remainingAmount: remainingAmount, loanError: loanError });
      //return (
      // this.state.loanError.loan_amount === ''
      //)
   }
   /*
   * Title :- onChnagePlanExist
   * Descrpation :- This function use for plan onchnage 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnagePlanExist(key, value) {
      let { loanError } = this.state;
      switch (key) {
         case 'loan_amount':
            if (isEmpty(value)) {
               loanError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Loan amount not valid";
            } else if (parseFloat(this.state.remainingAmount) < parseFloat(value)) {
               loanError[key] = "Loan amount less than or equal to remaining amount";
            } else if (this.state.loan.procedure_amount !== '' && this.state.loan.procedure_amount < value) {
               loanError[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.loan.procedure_amount !== '' && this.state.loan.procedure_amount >= value) {
               loanError[key] = "";
               loanError['loan_amount'] = "";
            } else {
               loanError[key] = "";
            }
            break;
         case 'procedure_date':
            if (value == null) {
               loanError[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               loanError[key] = '';
            }
            break;
         case 'procedure_amount':
            if (isEmpty(value)) {
               loanError[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Procedure amount not valid";
            }
            else if (this.state.loan.loan_amount !== '' && this.state.loan.loan_amount > value) {
               loanError[key] = "Procedure amount greater than or equal to procedure amount";
            } else if (this.state.loan.loan_amount !== '' && this.state.loan.loan_amount <= value) {
               loanError[key] = "";
               loanError['procedure_amount'] = "";
            } else {
               loanError[key] = '';
            }
            break;
         case 'provider_location':
            if (isEmpty(value)) {
               loanError[key] = "Location can't be blank";
            } else {
               this.props.getSpeciality(value);
               loanError[key] = '';
            }
            loanError['speciality_type'] = "Speciality can't be blank";
            break;
         case 'speciality_type':
            if (isEmpty(value)) {
               loanError[key] = "Speciality can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
      }

      this.setState({
         loan: {
            ...this.state.loan,
            [key]: value
         }
      });
      this.setState({ loanError: loanError });
   }
   /*
   * Title :- validateSearchPlan
   * Descrpation :- This function use for validate data for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   validateSearchPlan() {

      if (this.state.generatePlan) {
         return (
            this.state.loanError.loan_amount === '' &&
            this.state.loanError.procedure_date === '' &&
            this.state.loanError.procedure_amount === '' &&
            this.state.loanError.provider_location === '' &&
            this.state.loanError.speciality_type === ''
         )
      } else {
         return (
            this.state.loanError.loan_amount === '' &&
            this.state.loanError.procedure_date === '' &&
            this.state.loanError.procedure_amount === ''
         )
      }
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan according to customer
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   viewPlan() {
      this.props.paymentPlan(this.state.current_application_id, this.state.loan.loan_amount);
      //this.props.searchCustomerApplication(this.state.existApplication);
   }
   printPlan() {
      //console.log(this.state.current_application_id)
      //console.log(this.state.loan.loan_amount)
      //console.log(this.state.selectedPlan)
      this.props.printPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   validatePlan() {
      return (
         this.state.loanError.loan_amount === '' &&
         this.state.loanError.procedure_date === '' &&
         this.state.loanError.procedure_amount === '' &&
         this.state.loanError.provider_location === '' &&
         this.state.loanError.speciality_type === '' &&
         this.state.selectedPlan !== null
         /*this.state.add_err.provider_location === '' &&
         //this.state.add_err.speciality_type === '' &&
         this.state.add_err.procedure_date === '' &&
         this.state.add_err.amount === '' &&
         this.state.add_err.procedure_amount === '' &&
         this.state.add_err.comment === ''*/
      )
   }
   checkboxAction = (id, e) => {
      this.setState({ selectedPlan: (e.target.checked) ? id : null });
   }
   /*
   * Title :- processPlan
   * Descrpation :- This function use for process selected plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   processPlan(application_id, plan_id) {
      this.props.createPaymentPlan(application_id, plan_id, this.state.loan);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check state updated or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentWillReceiveProps(nextProps) {
      (nextProps.rediretPlanURL != '') ? this.setState({ changeURL: nextProps.rediretPlanURL }) : '';
   }
   render() {
      if (this.state.changeURL === 1) {
         return (<Redirect to={`/provider/customers/agreement-plan/${this.props.paymentPlanID}`} />);
      }
      const paymentPlanAllow = this.props.paymentPlanAllow;

      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.searchApplication" />}
               match={this.props.match}
            />

            {this.state.currentPermision.view != 1 &&
               <RctCollapsibleCard heading="" fullBlock >
               <div className="col-sm-12 p-20">
                  You are not authorized to access this page.
               </div>
               </RctCollapsibleCard>
            }

            {this.state.currentPermision.view == 1 &&
               <RctCollapsibleCard heading="" fullBlock>
                  <div className="modal-body page-form-outer text-left" ref={el => (this.componentRef = el)}>
                     <div className="row">
                        <div className="col-md-12 text-danger">
                           Following criteria to search application :- First name, Last Name, DOB or SSN or Application Number
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_first_name">First Name</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_first_name"
                                 id="exist_first_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="First Name"
                                 value={this.state.existApplication.exist_first_name}
                                 error={(this.state.existError.exist_first_name) ? true : false}
                                 helperText={this.state.existError.exist_first_name}
                                 onChange={(e) => this.onChnageExist('exist_first_name', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_last_name">Last Name</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_last_name"
                                 id="exist_last_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Last Name"
                                 value={this.state.existApplication.exist_last_name}
                                 error={(this.state.existError.exist_last_name) ? true : false}
                                 helperText={this.state.existError.exist_last_name}
                                 onChange={(e) => this.onChnageExist('exist_last_name', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_dob">Date of Birth</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="exist_dob"
                                 id="exist_dob"
                                 selected={this.state.startDateExist}
                                 inputProps={{ maxLength: 10 }}
                                 placeholderText="MM/DD/YYYY"
                                 autocomplete={false}
                                 onChange={(e) => this.onChnageExist('exist_dob', e)}
                              />
                              {(this.state.existError.exist_dob) ? <FormHelperText>{this.state.existError.exist_dob}</FormHelperText> : ''}
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_ssn">SSN</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_ssn"
                                 id="exist_ssn"
                                 fullWidth
                                 variant="outlined"
                                 inputProps={{ maxLength: 11 }}
                                 placeholder="Social Security Number"
                                 value={this.state.existApplication.exist_ssn}
                                 error={(this.state.existError.exist_ssn) ? true : false}
                                 helperText={this.state.existError.exist_ssn}
                                 onChange={(e) => this.onChnageExist('exist_ssn', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="application_no">Application No</Label><br />
                              <TextField
                                 type="text"
                                 name="application_no"
                                 id="application_no"
                                 fullWidth
                                 variant="outlined"
                                 inputProps={{ maxLength: 11 }}
                                 placeholder="Application Number"
                                 value={this.state.existApplication.application_no}
                                 error={(this.state.existError.application_no) ? true : false}
                                 helperText={this.state.existError.application_no}
                                 onChange={(e) => this.onChnageExist('application_no', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="mt-30">
                              <Button onClick={this.searchCustomerSubmit.bind(this)} disabled={!this.validateSearch()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 Search
                              </Button>
                              <Button onClick={this.searchCustomerReset.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 Reset
                              </Button>
                           </div>
                        </div>
                     </div>

                     {this.props.searchCustomer &&

                        <div className="table-responsive mb-40 pymt-history">
                           <h2 className="text-center mb-10">Customer Detail</h2>
                           <div className="table-responsive">
                              <table className="table table-borderless admin-account-view-invoice">
                                 <thead>
                                    <tr>
                                       <th>A/C Number</th>
                                       <th>Application No</th>
                                       <th>Full Name</th>
                                       <th>Address1</th>
                                       <th>City</th>
                                       <th>State</th>
                                       <th>Zip</th>
                                       <th>Phone</th>
                                       <th>Expiry Date</th>
                                       <th>Approved Amount</th>
                                       <th>Remaining Amount</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {this.props.searchCustomer && this.props.searchCustomer.map((cusomer, idx) => (
                                       <tr key={idx}>
                                          <td>{cusomer.patient_ac}</td>
                                          <td>{cusomer.application_no}</td>
                                          <td>{cusomer.first_name + ' ' + cusomer.middle_name + ' ' + cusomer.last_name}</td>
                                          <td>{cusomer.address1}</td>
                                          <td>{cusomer.City}</td>
                                          <td>{cusomer.state_name}</td>
                                          <td>{cusomer.zip_code}</td>
                                          <td>{cusomer.peimary_phone}</td>
                                          <td>{cusomer.expiry_date}</td>
                                          <td>${(cusomer.approve_amount > 0) ? parseFloat(cusomer.approve_amount).toFixed(2) : '0.00'}</td>
                                          <td>${(cusomer.remaining_amount > 0) ? parseFloat(cusomer.remaining_amount).toFixed(2) : '0.00'}</td>
                                          <td>
                                             {/*<Link to={`/provider/customers/customerPayment-plan/${this.props.searchCustomer.patient_id}/${this.props.searchCustomer.application_id}`} title="Process for next step"><i className="material-icons mr-10">visibility</i></Link>*/}
                                             <span className="list-action">
                                                {

                                                   (cusomer.expiry_date > moment().format('MM/DD/YYYY')) ?
                                                      (cusomer.remaining_amount > 0) ?
                                                         <React.Fragment>
                                                            <a href="javascript:void(0)" onClick={this.searchPlan.bind(this, cusomer.patient_id, cusomer.application_id, 0, cusomer.remaining_amount.toFixed(2))} title="View Plan Details"><i className="ti-eye mr-10"></i></a>
                                                            <a href="javascript:void(0)" onClick={this.searchPlan.bind(this, cusomer.patient_id, cusomer.application_id, 1, cusomer.remaining_amount.toFixed(2))} title="Generate Payment Plan"><i className="ti-money mr-10"></i></a>
                                                         </React.Fragment>
                                                         :
                                                         <a href="javascript:void(0)" title="Remaining amount not sufficient"><i className="ti-na mr-10"></i></a>
                                                      :
                                                      <a href="javascript:void(0)" title="Application expired please contact your system administrator"><i className="ti-na mr-10"></i></a>

                                                }
                                             </span>
                                          </td>
                                       </tr>
                                    ))}

                                 </tbody>
                              </table>

                           </div>
                        </div>
                     }
                     {this.state.showSearchPlan &&
                        <React.Fragment >
                           <div className="table-responsive mb-20 pymt-history select-customer-plan">
                              <table className="table">
                                 <thead>
                                    <tr>
                                       <th>{(this.state.generatePlan) ? 'Generate Payment Plan and Process Payement' : 'View Payment Plan'}</th>
                                    </tr>
                                 </thead>
                              </table>
                           </div>
                           {this.state.generatePlan &&
                              <div className="row">
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="provider_location">Location<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="provider_location"
                                          id="provider_location"
                                          placeholder=""
                                          onChange={(e) => this.onChnagePlanExist('provider_location', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {this.props.providerLocation && this.props.providerLocation.map((loc, key) => (
                                             <option value={loc.provider_location_id} key={key}>{loc.location_name}</option>
                                          ))}

                                       </Input>
                                       {(this.state.loanError.provider_location != '' && this.state.loanError.provider_location !== undefined) ? <FormHelperText>{this.state.loanError.provider_location}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="speciality_type">Speciality<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="speciality_type"
                                          id="speciality_type"
                                          onChange={(e) => this.onChnagePlanExist('speciality_type', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {this.props.providerSpeciality && this.props.providerSpeciality.map((spc, key) => (
                                             <option value={spc.mdv_id + '-' + spc.procedure_id} key={key}>{spc.value}</option>
                                          ))}
                                       </Input>
                                       {(this.state.loanError.speciality_type != '' && this.state.loanError.speciality_type !== undefined) ? <FormHelperText>{this.state.loanError.speciality_type}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                              </div>
                           }
                           <div className="row" >
                              <div className="col-md-3">
                                 <FormGroup>
                                    <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="procedure_amount"
                                       id="procedure_amount"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="($)"
                                       value={this.state.loan.procedure_amount}
                                       error={(this.state.loanError.procedure_amount) ? true : false}
                                       helperText={(this.state.loanError.procedure_amount != '') ? this.state.loanError.procedure_amount : ''}
                                       onChange={(e) => this.onChnagePlanExist('procedure_amount', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>
                              <div className="col-md-3">
                                 <FormGroup>
                                    <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                                    <DatePicker
                                       dateFormat="MM/dd/yyyy"
                                       name="procedure_date"
                                       id="procedure_date"
                                       selected={this.state.procedure_date}
                                       placeholderText="MM/DD/YYYY"
                                       autocomplete={false}
                                       onChange={(e) => this.onChnagePlanExist('procedure_date', e)}
                                    />
                                    {(this.state.loanError.procedure_date) ? <FormHelperText>{this.state.loanError.procedure_date}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-3">
                                 <FormGroup>
                                    <Label for="loan_amount">Enter Loan Amount($)<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="loan_amount"
                                       id="loan_amount"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="Enter Loan Amount"
                                       value={this.state.loan.loan_amount}
                                       error={(this.state.loanError.loan_amount) ? true : false}
                                       helperText={this.state.loanError.loan_amount}
                                       onChange={(e) => this.onChnagePlanExist('loan_amount', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>
                              <div className="col-md-3">
                                 {(this.state.generatePlan) ?
                                    <div className="mt-30">
                                       <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                          Generate Plan
                                    </Button>
                                    </div>
                                    :
                                    <div className="mt-30">
                                       <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                          View Plan
                                       </Button>
                                       <Button onClick={this.printPlan.bind(this)} disabled={(this.state.selectedPlan == null) ? true : false} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                          Print Plan
                                       </Button>
                                    </div>

                                 }
                              </div>
                           </div>
                        </React.Fragment>
                     }
                     {paymentPlanAllow &&
                        <div className="price-list user-permission" >
                           <div className="row row-eq-height">
                              {paymentPlanAllow.map((plan, idx) => (
                                 <div className="col-md-4" key={idx} >
                                    <div className="price-plan plan1 rct-block text-center">
                                       <div className="header">
                                          <h2 className="pricing-title">
                                             <span className="price">{plan.interest_rate}</span><span>%</span></h2>
                                          <div className="description-text">Principal Amount: <span className="t-price">${this.props.applicationDetails[0].amount}</span></div>
                                          {/*<button disabled={!this.validatePlan()} className="btn-block btn-medium btn btn-primary top-select-btn" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}
                                          <label className="check-container">
                                             <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                             <span className="checkmark"></span>
                                          </label>
                                       </div>
                                       <div className="content ">
                                          <div className="page-form-outer select-plan-detail">
                                             <div className="row">

                                                <div className="col-md-10">Payment Plans</div>
                                                <div className="col-md-2">{idx + 1}</div>

                                                <div className="col-md-10">Payment Term (months)</div>
                                                <div className="col-md-2">{plan.term_month}</div>
                                             </div>

                                             <div className="price-list table-responsive">
                                                <table className="table table-bordered table-striped table-hover">
                                                   <thead>
                                                      <tr className="table-head">
                                                         <th>SN</th>
                                                         <th>Date</th>
                                                         <th>Amount</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {
                                                         plan.monthlyPlan.map((month, idm) => (

                                                            <tr key={idm}>
                                                               <td>{idm + 1}</td>
                                                               <td>{month.next_month}</td>
                                                               <td>${month.perMonth}</td>
                                                            </tr>
                                                         ))
                                                      }



                                                   </tbody>
                                                </table>

                                             </div>
                                             {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                          </div>


                                       </div>
                                    </div>
                                 </div>
                              ))}


                           </div>
                           {this.state.generatePlan &&
                              <div className="text-right">
                                 <button disabled={!this.validatePlan()} className="btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, this.state.selectedPlan)}><span>Generate Invoice</span></button>
                              </div>
                           }
                        </div>

                     }
                  </div>

                  {this.props.loading &&
                     <RctSectionLoader />
                  }
               </RctCollapsibleCard>
            }

         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, searchCustomer, paymentPlanAllow, applicationDetails, providerLocation, providerSpeciality, rediretPlanURL, paymentPlanID } = creditApplication;
   return { loading, searchCustomer, paymentPlanAllow, applicationDetails, providerLocation, providerSpeciality, rediretPlanURL, paymentPlanID, user }



}

export default connect(mapStateToProps, {
   searchCustomerApplication, paymentPlan, applicationOption, getSpeciality, planReset, createPaymentPlan, printPlan
})(searchApplication); 