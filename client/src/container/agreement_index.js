/**
 * App Widgets
 */
import React from 'react';
import Loadable from 'react-loadable';
import PreloadWidget from 'Components/PreloadLayout/PreloadWidget';

const MyLoadingComponent = () => (
   <PreloadWidget />
)


const ProviderAgreement = Loadable({
   loader: () => import("./agreement"),
   loading: MyLoadingComponent
})


export {
   ProviderAgreement
}