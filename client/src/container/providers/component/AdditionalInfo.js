/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
   FormGroup,
   Form,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';


// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const AdditionalInfo = ({ providerAdditionalDiscount, providerAdditionalFee }) => (
         <div className="address-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
               
                  <div className="view-section-inner">

                    <div className="view-box">

                      <div className="discount-box mb-15">
                        <h3 className="p-view-title">Additional Discount</h3>

                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                                <tr>
                                  <th>Discount Type</th>
                                  <th>Discount PCT(%)</th>
                                  <th>Reduced Interest PCT(%)</th>
                                  <th>Loan Term (months)</th>
                                  <th>From Date</th>
                                  <th>To Date</th>
                                </tr>
                              </thead>
                              <tbody>
                              {providerAdditionalDiscount && providerAdditionalDiscount.map((additional, key) => (
                                
                                  <tr key={key}>
                                    <td>{additional.discount_type}</td>
                                    <td>{additional.discount_rate+'%'}</td>
                                    <td>{additional.redu_discount_rate+'%'}</td>
                                    <td>{(additional.loan_term_month_value) ? additional.loan_term_month_value : '-'}</td>
                                    <td>{(additional.discount_from_date && additional.discount_from_date != '00/00/0000') ? additional.discount_from_date : '-'}</td>
                                    <td>{(additional.discount_to_date && additional.discount_to_date != '00/00/0000') ? additional.discount_to_date : '-'}</td>
                                  </tr>
                                          
                              ))}
                                </tbody>
                            </table>
                          </div>
                                  
                      </div>

                      <div className="fee-box mb-15">
                        <h3 className="p-view-title">Additional Fee</h3>
                        <div className="width-100">
                        
                        <table>
                            <thead>
                              <tr>
                                <th>Fee Type</th>
                                <th>Convenience Fee</th>
                                <th>From Date</th>
                                <th>To Date</th>
                              </tr>
                            </thead>
                            <tbody>
                        {providerAdditionalFee && providerAdditionalFee.map((additional, key) => (
                          
                          <tr key={key}>
                            <td>{additional.additional_fee_type}</td>
                            <td>{(additional.additional_conv_fee) ? '$'+parseFloat(additional.additional_conv_fee).toFixed(2) : '-'}</td>
                            <td>{(additional.additional_from_date) ? additional.additional_from_date : '-'}</td>
                            <td>{(additional.additional_to_date) ? additional.additional_to_date : '-'}</td>
                          </tr>
                            
                            
                        ))}
                            </tbody>
                        </table>
                        </div>
                      </div>

                    </div>

                  </div>
              
            </div>
         </div>
      );
export default AdditionalInfo;
