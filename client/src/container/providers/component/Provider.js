/**
 * Profile Page
 */
import React, { Component } from 'react';
import { FormGroup, Input, Form, Label, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';

// intlmessages
import IntlMessages from 'Util/IntlMessages';

const Provider = ({ providerDetails }) => (
         <div className="profile-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">
                <h3 className="p-view-title">Provider Details</h3>
                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                                  <td className="fw-bold">Provider Name:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_name : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Provider Type:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_type : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Primary Contact Phone:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_phone_no : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Email Address: </td>
                                  <td>{providerDetails ? providerDetails[0].provider_email : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Fax No:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_fax_no : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Tax ID/SSN:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_taxid_ssn : ''}</td>
                                </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                                  <td className="fw-bold">Primary Contact:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_primary_contact : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Website:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_website : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">In Network:</td>
                                  <td>{providerDetails ? (providerDetails[0].provider_network == 1) ? 'Yes' : 'No' : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold"> Phone No:</td>
                                  <td>{providerDetails ? providerDetails[0].provider_primary_contact_phone : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Status:</td>
                                  <td>{providerDetails ? (providerDetails[0].provider_status == 1) ? 'Active' : 'Inactive' : ''}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold"></td>
                                  <td></td>
                                </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      );
export default Provider;