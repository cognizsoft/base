/**
 * Messages Page
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Input,
   InputGroup,
   InputGroupAddon,
   FormGroup,
   Label
} from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

const UsersDetails = ({ providerUser }) => (
         <div className="messages-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">
                    <h3 className="p-view-title">Users</h3>

               		{providerUser && providerUser.map((user, key) => (
	                    <div className="p-view-user mb-15" key={key}>
		                    <div className="view-box">
		                      <div className="width-50">
		                        <table>
		                            <tbody>
		                            <tr>
		                              <td className="fw-bold">First Name:</td>
		                              <td>{user.user_first_name}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">Middle Name:</td>
		                              <td>{user.user_middle_name}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">Last Name:</td>
		                              <td>{user.user_last_name}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">Phone No:</td>
		                              <td>{user.user_phone_no}</td>
		                            </tr>
		                            </tbody>
		                        </table>
		                      </div>
		                       <div className="width-50">
		                        <table>
		                            <tbody>
		                            <tr>
		                              <td className="fw-bold">Location</td>
		                              <td>{user.user_location}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">Email Address:</td>
		                              <td>{user.user_email}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">User Role:</td>
		                              <td>{user.user_role}</td>
		                            </tr>
		                            <tr>
		                              <td className="fw-bold">User Name:</td>
		                              <td>{user.username}</td>
		                            </tr>
		                            
		                            </tbody>
		                        </table>
		                      </div>
		                    </div>
	                    </div>
	                  ))}

               </div>

            </div>
         </div>
      );
export default UsersDetails;