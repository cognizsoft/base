/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Provider from './component/Provider';
import Locations from './component/Locations';
import Banks from './component/Banks';
import AdditionalInfo from './component/AdditionalInfo';
import Physician from './component/Physician';
import UserBlock from './component/UserBlock';
import Documents from './component/Documents';
import Users from './component/Users';

// rct card box
import { RctCard } from 'Components/RctCard';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
  ProviderDetails
} from 'Actions';
import {patientID} from '../../apifile';

function getSteps() {
   return ['Provider', 'Location', 'Bank', 'Additional Info', 'Physician', 'Documents', 'Users'];
}

class ProviderView extends Component {

   state = {
      opnDocFileModal: false,
      imgPath:'',
      imgName:'',
      activeStep: 0,
      completed: {},
   }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   componentDidMount() {
      this.props.ProviderDetails(this.props.match.params.id);
  }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   opnDocFileModal(path, name) {
      this.setState({ opnDocFileModal: true, imgPath:path, imgName:name})
   }
   
   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }

   render() {
      const steps = getSteps();
      const { activeStep } = this.state;
      const { loading } = this.props;

      const path = require('path');
      const providerDetails = this.props.provider_details;
      const providerBank = this.props.provider_bank;
      const providerLocation = this.props.provider_location;
      const providerUser = this.props.provider_user;
      const providerAdditionalDiscount = this.props.provider_additional_discount;
      const providerAdditionalFee = this.props.provider_additional_fee;
      const providerDocument = this.props.provider_document;
      const providerSpecility = this.props.provider_specility;
      const providerSpecilityDoctors = this.props.provider_specility_doctors;

      if(providerSpecility) {
         var checkSpc = 0;
         providerSpecility.forEach(function(item, idx) {
            //console.log('mergedList')
            //console.log(idx)
            //console.log(item)
            if(checkSpc == idx){
               var splc = providerSpecilityDoctors.filter((s, idx) => s.location_id == item.provider_location_provider_location_id);
               splc.forEach(function(doc,id){
                  providerSpecility[checkSpc].doctor_f_name = doc.f_name;
                  providerSpecility[checkSpc].doctor_l_name = doc.l_name;
                  providerSpecility[checkSpc].procedure_name = doc.procedure_name;
                  providerSpecility[checkSpc].speciality = doc.speciality;

                  checkSpc++;
               })
               
            }
            
            //console.log(splc)
         })
      }

      

      return (
         <div className="providerView-wrapper">
            <Helmet>
               <title>Provider View</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.providerView" />} match={this.props.match} />
            <RctCard>

               <Stepper nonLinear activeStep={activeStep}>
                  {steps.map((label, index) => {
                     return (
                        <Step key={label}>
                           <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                              {label}
                           </StepButton>
                        </Step>
                     );
                  })}
               </Stepper>
               <div>
                  {this.allStepsCompleted() ? (
                     <div className="pl-40">
                        <p>All steps completed - you&quot;re finished</p>
                        <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                     </div>
                  ) : (
                        <div className="pl-40">
                           {(() => {
                              switch (activeStep) {
                                 case 0:
                                    return <Provider 
                                             providerDetails={providerDetails}
                                           />
                                    break;
                                 case 1:
                                    return <Locations 
                                             providerLocation={providerLocation}
                                           />
                                    break;
                                 case 2:
                                    return <Banks 
                                             providerBank={providerBank}
                                           />
                                    break;
                                 case 3:
                                    return <AdditionalInfo 
                                             providerAdditionalDiscount={providerAdditionalDiscount}
                                             providerAdditionalFee={providerAdditionalFee}
                                           />
                                    break;
                                 case 4:
                                    return <Physician
                                             providerPhysician={providerSpecility}
                                           />
                                    break;
                                 case 5:
                                    return <Documents 
                                             providerDocument={providerDocument}
                                             opnDocFileModal={this.opnDocFileModal.bind(this)}
                                           />
                                    break;
                                 case 6: 
                                    return <Users 
                                             providerUser={providerUser}
                                           />
                                 default:
                                    return (<div></div>)
                              }
                           })()}

                        </div>
                     )}
               </div>

            </RctCard>

            <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>
                  
               <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
                  <span className="float-left>">Name: {this.state.imgName}</span>
                  <span className="float-right"><a href={this.state.imgPath} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span> 
               </ModalHeader>

               <ModalBody>
                  {this.state.imgPath &&
                     <embed src={this.state.imgPath} width="100%" height="350" download/>
                
                  }

               </ModalBody>
               
            </Modal>

         </div>
      );
   }
}
const mapStateToProps = ({ Provider }) => {
  const { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_specility, provider_specility_doctors } = Provider;
  return { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_specility, provider_specility_doctors }
}
export default connect(mapStateToProps, {
  ProviderDetails
})(ProviderView);