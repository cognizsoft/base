/**
* Add New User Form
*/
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import ReactAutoSuggest from './ReactAutoSuggest.js';
const StepSecond = ({ addErr, addData, onChnagerovider, handleShareholderNameChange, handleRemoveShareholder, handleAddShareholder, shareholders, providerCountry, stateType, regionType, stateType2, regionType2, onCheckedClick, citySuggestion }) => (
<div className="table-responsive">
   <div className="modal-body page-form-outer text-left second-tab-container">
      {addData.location.map((shareholder, idx) => (
      <div className="second-tab-inner-container" key={idx} aa={idx}>
         <div className="row">
            {(() => {
            if (idx != 0) {
            return (
            <React.Fragment>
               <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
               <div className="col-md-2">
                  <a href="#" onClick={(e) => handleRemoveShareholder(idx)}>Remove locations (-)</a>
               </div>
            </React.Fragment>
            )
            }
            })()}
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_location_name">Location Name<span className="required-field">*</span></Label><br />
                  <TextField
                  type="text"
                  name="physical_location_name"
                  id="physical_location_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Location Name"
                  value={(addData.location[idx].physical_location_name != '') ? addData.location[idx].physical_location_name : ''}
                  error={(addErr.location[idx].physical_location_name) ? true : false}
                  helperText={(addErr.location[idx].physical_location_name != '') ? addErr.location[idx].physical_location_name : ''}
                  onChange={(e) => onChnagerovider('physical_location_name', e.target.value, idx)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_address1">Address1<span className="required-field">*</span></Label><br />
                  <TextField
                  type="text"
                  name="physical_address1"
                  id="physical_address1"
                  fullWidth
                  variant="outlined"
                  placeholder="Address1"
                  value={(addData.location[idx].physical_address1 != '') ? addData.location[idx].physical_address1 : ''}
                  error={(addErr.location[idx].physical_address1) ? true : false}
                  helperText={(addErr.location[idx].physical_address1 != '') ? addErr.location[idx].physical_address1 : ''}
                  onChange={(e) => onChnagerovider('physical_address1', e.target.value, idx)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_address2">Address2</Label><br />
                  <TextField
                  type="text"
                  name="physical_address2"
                  id="physical_address2"
                  fullWidth
                  variant="outlined"
                  placeholder="Address2"
                  value={(addData.location[idx].physical_address2 != '') ? addData.location[idx].physical_address2 : ''}
                  onChange={(e) => onChnagerovider('physical_address2', e.target.value, idx)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
              <FormGroup>
                <Label for="physical_city">City<span className="required-field">*</span></Label><br />
                 <ReactAutoSuggest 
                    citySuggestion={citySuggestion} 
                    onChnagerovider={onChnagerovider}
                    index={idx}
                    type={'physical'}
                  />
                </FormGroup>
            </div>
            {/*<div className="col-md-4">
                           <FormGroup>
                              <Label for="physical_city">City<span className="required-field">*</span></Label><br />
                              <TextField
                              type="text"
                              name="physical_city"
                              id="physical_city"
                              fullWidth
                              variant="outlined"
                              placeholder="City"
                              value={(addData.location[idx].physical_city != '') ? addData.location[idx].physical_city : ''}
                              error={(addErr.location[idx].physical_city) ? true : false}
                              helperText={(addErr.location[idx].physical_city != '') ? addErr.location[idx].physical_city : ''}
                              onChange={(e) => onChnagerovider('physical_city', e.target.value, idx)}
                              />
                           </FormGroup>
                        </div>*/}
            <div className="col-md-4">
               <FormGroup className="phy_state_grp">
                  <Label for="physical_state">State<span className="required-field">*</span></Label>
                  <Input
                     type="select"
                     name="physical_state"
                     id="physical_state"
                     placeholder=""
                     value={addData.location[idx].physical_state}
                     onChange={(e) => onChnagerovider('physical_state', e.target.value, idx)}
                  >
                  <option value="">Select</option>
                  {stateType[idx] && stateType[idx].map((state, key) => (
                  <option value={state.state_id} key={key}>{state.name}</option>
                  ))}
                  </Input>
                  {(addErr.location[idx].physical_state != '') ? 
                  <FormHelperText>{addErr.location[idx].physical_state}</FormHelperText>
                  : ''}
               </FormGroup>
            </div>
            
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_country">Country<span className="required-field">*</span></Label>
                  <Input
                     type="select"
                     name="physical_country"
                     id="physical_country"
                     placeholder=""
                     value={addData.location[idx].physical_country}
                     onChange={(e) => onChnagerovider('physical_country', e.target.value, idx)}
                  >
                  
                  {providerCountry && providerCountry.map((opt, key) => (
                  <option value={opt.id} key={key}>{opt.name}</option>
                  ))}
                  </Input>
                  {(addErr.location[idx].physical_country != '') ? 
                  <FormHelperText>{addErr.location[idx].physical_country}</FormHelperText>
                  : ''}
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                  <TextField
                  type="text"
                  name="physical_zip_code"
                  id="physical_zip_code"
                  fullWidth
                  variant="outlined"
                  placeholder="Zip Code"
                  value={(addData.location[idx].physical_zip_code != '') ? addData.location[idx].physical_zip_code : ''}
                  error={(addErr.location[idx].physical_zip_code) ? true : false}
                  helperText={(addErr.location[idx].physical_zip_code != '') ? addErr.location[idx].physical_zip_code : ''}
                  onChange={(e) => onChnagerovider('physical_zip_code', e.target.value, idx)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="physical_phone_no">Phone No<span className="required-field">*</span></Label><br />
                  <TextField
                  type="text"
                  name="physical_phone_no"
                  id="physical_phone_no"
                  fullWidth
                  variant="outlined"
                  placeholder="Phone No"
                  inputProps={{ maxLength: 14 }}
                  value={(addData.location[idx].physical_phone_no != '') ? addData.location[idx].physical_phone_no : ''}
                  error={(addErr.location[idx].physical_phone_no) ? true : false}
                  helperText={(addErr.location[idx].physical_phone_no != '') ? addErr.location[idx].physical_phone_no : ''}
                  onChange={(e) => onChnagerovider('physical_phone_no', e.target.value, idx)}
                  />
               </FormGroup>
            </div>            

            <div className="col-md-12">
               <FormGroup tag="fieldset" className="prmry_adrs_grp">
                  <FormGroup check>
                     <Label check>
                     <Input 
                     type="checkbox" 
                     checked={(addData.location[idx].primary_address_flag == 1) ? true : false}
                     value={(addData.location[idx].primary_address_flag != '') ? addData.location[idx].primary_address_flag : ''}
                     name="primary_address_flag" 
                     onChange={(e) => onChnagerovider('primary_address_flag', e.target.value, idx)}
                     />{' '}
                     Set as primary address
                     </Label>
                     {(addErr.primary_flag != '') ? 
                      <FormHelperText>{addErr.primary_flag}</FormHelperText>
                      : ''}
                  </FormGroup>
                  <FormGroup check>
                     <Label check>
                     <Input 
                     type="checkbox" 
                     name="billing_address_flag"
                     checked={(addData.location[idx].billing_address_flag == 1) ? true : false}
                     value={(addData.location[idx].billing_address_flag != '') ? addData.location[idx].billing_address_flag : ''}
                     onChange={(e) => onChnagerovider('billing_address_flag', e.target.checked, idx)}
                     />{' '}
                     Billing and physical address same
                     </Label>
                  </FormGroup>
               </FormGroup>
            </div>
         </div>
         <br />
         <h4 className={(addData.location[idx].billing_address_flag == 1) ? 'row blue-small-title d-none' : 'blue-small-title'}>Billing Address</h4>
         <div className={(addData.location[idx].billing_address_flag == 1) ? 'row billing_address_container d-none' : 'row billing_address_container'}>
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_location_name">Location Name<span className="required-field">*</span></Label><br />
                 <TextField
                 type="text"
                 name="billing_location_name"
                 id="billing_location_name"
                 fullWidth
                 variant="outlined"
                 placeholder="Location Name"
                 value={(addData.location[idx].billing_location_name != '') ? addData.location[idx].billing_location_name : ''}
                 error={(addErr.location[idx].billing_location_name) ? true : false}
                 helperText={(addErr.location[idx].billing_location_name != '') ? addErr.location[idx].billing_location_name : ''}
                 onChange={(e) => onChnagerovider('billing_location_name', e.target.value, idx)}
                 />
              </FormGroup>
           </div>
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_address1">Address1<span className="required-field">*</span></Label><br />
                 <TextField
                 type="text"
                 name="billing_address1"
                 id="billing_address1"
                 fullWidth
                 variant="outlined"
                 placeholder="Address1"
                 value={(addData.location[idx].billing_address1 != '') ? addData.location[idx].billing_address1 : ''}
                 error={(addErr.location[idx].billing_address1) ? true : false}
                 helperText={(addErr.location[idx].billing_address1 != '') ? addErr.location[idx].billing_address1 : ''}
                 onChange={(e) => onChnagerovider('billing_address1', e.target.value, idx)}
                 />
              </FormGroup>
           </div>
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_address2">Address2</Label><br />
                 <TextField
                 type="text"
                 name="billing_address2"
                 id="billing_address2"
                 fullWidth
                 variant="outlined"
                 placeholder="Address2"
                 value={(addData.location[idx].billing_address2 != '') ? addData.location[idx].billing_address2 : ''}
                 onChange={(e) => onChnagerovider('billing_address2', e.target.value, idx)}
                 />
              </FormGroup>
           </div>


           <div className="col-md-4">
              <FormGroup>
                <Label for="billing_city">City<span className="required-field">*</span></Label><br />
                 <ReactAutoSuggest 
                    citySuggestion={citySuggestion} 
                    onChnagerovider={onChnagerovider}
                    index={idx}
                    type={'billing'}
                  />
                </FormGroup>
            </div>
           {/*<div className="col-md-4">
                         <FormGroup>
                            <Label for="billing_city">City<span className="required-field">*</span></Label><br />
                            <TextField
                            type="text"
                            name="billing_city"
                            id="billing_city"
                            fullWidth
                            variant="outlined"
                            placeholder="City"
                            value={(addData.location[idx].billing_city != '') ? addData.location[idx].billing_city : ''}
                            error={(addErr.location[idx].billing_city) ? true : false}
                            helperText={(addErr.location[idx].billing_city != '') ? addErr.location[idx].billing_city : ''}
                            onChange={(e) => onChnagerovider('billing_city', e.target.value, idx)}
                            />
                         </FormGroup>
                      </div>*/}
           <div className="col-md-4">
              <FormGroup className="bill_state_grp">
                 <Label for="billing_state">State<span className="required-field">*</span></Label>
                 <Input
                    type="select"
                    name="billing_state"
                    id="billing_state"
                    placeholder="Enter Type"
                    value={addData.location[idx].billing_state}
                    onChange={(e) => onChnagerovider('billing_state', e.target.value, idx)}
                 >
                 <option value="">Select</option>
                 {stateType2[idx] && stateType2[idx].map((state, key) => (
                 <option value={state.state_id} key={key}>{state.name}</option>
                 ))} 
                 </Input>
                 {(addErr.location[idx].billing_state != '') ? 
                 <FormHelperText>{addErr.location[idx].billing_state}</FormHelperText>
                 : ''}
              </FormGroup>
           </div>
           
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_country">Country<span className="required-field">*</span></Label>
                 <Input
                    type="select"
                    name="billing_country"
                    id="billing_country"
                    placeholder="Enter Type"
                    value={addData.location[idx].billing_country}
                    onChange={(e) => onChnagerovider('billing_country', e.target.value, idx)}
                 >
                 
                 {providerCountry && providerCountry.map((opt, key) => (
                 <option value={opt.id} key={key}>{opt.name}</option>
                 ))}
                 </Input>
                 {(addErr.location[idx].billing_country != '') ? 
                 <FormHelperText>{addErr.location[idx].billing_country}</FormHelperText>
                 : ''}
              </FormGroup>
           </div>
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                 <TextField
                 type="text"
                 name="billing_zip_code"
                 id="billing_zip_code"
                 fullWidth
                 variant="outlined"
                 placeholder="Zip Code"
                 value={(addData.location[idx].billing_zip_code != '') ? addData.location[idx].billing_zip_code : ''}
                 error={(addErr.location[idx].billing_zip_code) ? true : false}
                 helperText={(addErr.location[idx].billing_zip_code != '') ? addErr.location[idx].billing_zip_code : ''}
                 onChange={(e) => onChnagerovider('billing_zip_code', e.target.value, idx)}
                 />
              </FormGroup>
           </div>
           <div className="col-md-4">
              <FormGroup>
                 <Label for="billing_phone_no">Phone No<span className="required-field">*</span></Label><br />
                 <TextField
                 type="text"
                 name="billing_phone_no"
                 id="billing_phone_no"
                 fullWidth
                 variant="outlined"
                 placeholder="Phone No"
                 inputProps={{ maxLength: 14 }}
                 value={(addData.location[idx].billing_phone_no != '') ? addData.location[idx].billing_phone_no : ''}
                 error={(addErr.location[idx].billing_phone_no) ? true : false}
                 helperText={(addErr.location[idx].billing_phone_no != '') ? addErr.location[idx].billing_phone_no : ''}
                 onChange={(e) => onChnagerovider('billing_phone_no', e.target.value, idx)}
                 />
              </FormGroup>
           </div>
           <div className="col-md-4">
               <FormGroup tag="fieldset">
                  <br />
                  <FormGroup check>
                     <Label check>
                     <Input 
                     type="checkbox" 
                     checked={(addData.location[idx].primary_billing_address_flag == 1) ? true : false}
                     value={(addData.location[idx].primary_billing_address_flag != '') ? addData.location[idx].primary_billing_address_flag : ''}
                     name="primary_billing_address_flag" 
                     onChange={(e) => onChnagerovider('primary_billing_address_flag', e.target.value, idx)}
                     />{' '}
                     Set primary billing address
                     </Label>
                  </FormGroup>  
               </FormGroup>
            </div>

         </div>
      </div>
      ))}
    <div className="row">
   <div className="col-md-12">
      <a href="#" onClick={(e) => handleAddShareholder()}>Add more locations (+)</a>
   </div>
   </div>
</div>
</div>
);
export default StepSecond;