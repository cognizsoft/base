/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { Link } from 'react-router-dom';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllUsers
} from 'Actions';

export default class InvoiceList extends Component {

   state = {
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0
   }


	
	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/**
	 * On Reload
	 */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

	/**
	 * On Select User
	 */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }

	
   /**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
       //console.log(this.state.openViewUserDialog);
      this.setState({ openViewUserDialog: true, selectedUser: data });
     // console.log(this.state.openViewUserDialog);
   }

	/**
	 * On Edit User
	 */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose() {
      // console.log(this.state.addNewUserModal);
      this.setState({ addNewUserModal: false, editUser: null })
      //console.log(this.state.addNewUserModal);
   }

    /**
	 * On View User Modal Close
	 */
   onViewUserModalClose = () => {
       //console.log(this.state.addViewUserModal);
      this.setState({ openViewUserDialog: false, selectedUser: null })
      //console.log(this.state.addViewUserModal);
   }
   
   
	

   render() {
      const { users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      
      const data = [
         { invoice_id: "4587", provider_name: "Ally Jake", total_amount: "$4780", submit_date: "10/05/2018", status: "Active"},
         { invoice_id: "6735", provider_name: "Brandon Oxadien", total_amount: "$3660", submit_date: "02/11/2018", status: "Inactive"},
      ];
      const options = {
         filterType: 'dropdown',
      };
      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Customers | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.invoiceList" />}
               match={this.props.match}
            />
            
            <RctCollapsibleCard fullBlock>
               <div className="table-responsive">
                  <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                     <div>
                        
                        <a href="javascript:void(0)" onClick={() => this.onReload()} className="btn-outline-default mr-10"><i className="ti-reload"></i></a>
                     </div>
                     
                  </div>
                  <table className="table table-middle table-hover mb-0">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Invoice ID</th>
                           <th>Provider Name</th>
                           <th>Total Amount</th>
                           <th>Submit Date</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        {data && data.map((user, key) => (
                           <tr key={key}>
                              <td>{key +1}</td>
                              <td>
                                 <div className="media">
                                    <div className="media-body">
                                       <h5 className="mb-5 fw-bold">{user.invoice_id}</h5>
                                    </div>
                                 </div>
                              </td>
                              <td><span className="">{user.provider_name}</span></td>
                              <td><span className="">{user.total_amount}</span></td>
                              <td><span className="">{user.submit_date}</span></td>
                              <td className="d-flex justify-content-start">
                               <div className="status">
                                    <span className="d-block">{user.status}</span>
                                 </div>
                              </td>
                              <td className="list-action">
                                <Link to={"/admin/providers/invoice/"+user.invoice_id}><i className="ti-eye"></i></Link>
                              </td>
                           </tr>
                        ))}
                     </tbody>
                     <tfoot className="border-top">
                        <tr>
                           <td colSpan="100%">
                              <Pagination className="mb-0 py-10 px-10">
                                 <PaginationItem>
                                    <PaginationLink previous href="#" />
                                 </PaginationItem>
                                 <PaginationItem active>
                                    <PaginationLink href="javascript:void(0)">1</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink href="javascript:void(0)">2</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink href="javascript:void(0)">3</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink next href="javascript:void(0)" />
                                 </PaginationItem>
                              </Pagination>
                           </td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               {loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'Risk Factor View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left">
                              
                              <div className="media-body">
                                 <p>Invoice ID: <span className="fw-bold">{selectedUser.invoice_id}</span></p>
                                 <p>Provider Name: <span className="fw-bold">{selectedUser.provider_name}</span></p>
                                 <p>Total Amount: <span className="fw-bold">{selectedUser.total_amount}</span></p>
                                 <p>Submit Date: <span className="fw-bold">{selectedUser.submit_date}</span></p>
                                 <p>Status: <span className="fw-bold">{selectedUser.status}</span></p>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
              
            </Modal> 
            
         </div>
      );
   }
}
