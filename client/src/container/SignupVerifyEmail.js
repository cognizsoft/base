/**
 * Sign Up With Firebase
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import { Fab } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
// components
import { SessionSlider } from 'Components/Widgets';
import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isContainWhiteSpace } from '../validator/Validator';
import queryString from 'query-string';
import CryptoJS from 'crypto-js';
// app config
import AppConfig from 'Constants/AppConfig';

// redux action
import {
   customerDetailVerify, updateCustomerFlag
} from 'Actions';

class SignupFirebase extends Component {

   constructor(props) {
      super(props)
      // reset login status
      //this.props.dispatch(userActions.logout());
      this.state = {

         customerTypeDetail: {
            access_token: ''
         },

         customerDetailVerify: {
            id: '',
            access_token: ''
         },

         errors: {
            
         },

         formData: {}, // Contains login form data
         formSubmitted: false, // Indicates submit status of login form
         loading: false // Indicates in progress state of login form
      }
   }

   decryption(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }

   componentDidMount() {
      //this.decryption(this.props.match.params.id)
      if(queryString.parse(this.props.location.search).id !== undefined) {
         this.props.customerDetailVerify(this.decryption(queryString.parse(this.props.location.search).id));  
      }

   }

   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { customerDetailVerify } = this.state;
      
      if(nextProps.customer_detail_verify != '' && nextProps.customer_detail_verify !== undefined) {

         
         customerDetailVerify.id = nextProps.customer_detail_verify[0].id;
         customerDetailVerify.access_token = nextProps.customer_detail_verify[0].access_token;
         
         this.setState({
            customerDetailVerify: customerDetailVerify
         });
      }

   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { formData } = this.state;
      formData[name] = value;

      this.setState({
         formData: formData
      }, function () {
         this.validateSignUpForm(name, value)
      });
   }

   validateSubmit() {
      return (
         this.state.errors.access_token === ''
      );
   }

   validateSignUpForm = (fieldName, value) => {
      let { errors } = this.state;
      //console.log(value)
      switch (fieldName) {
         case 'access_token':
            if (isEmpty(value)) {
               errors[fieldName] = "Access Token can't be blank";
            } else {
               errors[fieldName] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ errors: errors });

      this.setState({
         customerTypeDetail: {
            ...this.state.customerTypeDetail,
            [fieldName]: value
         }
      });
   }

   customerSignUp = (e) => {

      const { customerTypeDetail, customerDetailVerify, errors } = this.state;
      
      if(customerTypeDetail.access_token == customerDetailVerify.access_token) {
         var id = {
            id: this.decryption(queryString.parse(this.props.location.search).id)
         }
         this.props.updateCustomerFlag(id);
      } else {
         var err = {
            access_token: "Access Token not matched"
         }
         this.setState({
            errors: err
         })
      }
   }


   
   render() {
      const { customerTypeDetail, errors } = this.state;
      const { loading, redirectURL, customer_detail_verify, customer_redirect } = this.props;

      //console.log('customer_detail_verify')
      //console.log(customer_detail_verify)

      if(customer_detail_verify) {
         if(customer_detail_verify[0].access_flag == 1 && customer_detail_verify[0].profile_created == 0 && customer_detail_verify[0].customer_type == 'Provider') {
            return (<Redirect to={"/provider/create?id="+queryString.parse(this.props.location.search).id}/>);
         } else if(customer_detail_verify[0].access_flag == 1 && customer_detail_verify[0].profile_created == 0 && customer_detail_verify[0].customer_type == 'Customer') {
            return (<Redirect to={"/application/create?id="+queryString.parse(this.props.location.search).id}/>);
         } else if(customer_detail_verify[0].access_flag == 1 && customer_detail_verify[0].profile_created == 1) {
            return (<Redirect to={"/signin"}/>);
         } else {
            
         }
      }

      if(customer_redirect == 1) {
         return (<Redirect to={"/provider/create?id="+queryString.parse(this.props.location.search).id}/>);
      } else if(customer_redirect == 2) {
         return (<Redirect to={"/application/create?id="+queryString.parse(this.props.location.search).id}/>);
      } else {
         //return (<Redirect to={"/signin"}/>);
      }

      //console.log('queryString.parse(this.props.location.search).id')
      //console.log(queryString.parse(this.props.location.search).id)

      if(queryString.parse(this.props.location.search).id == undefined) {
         return (<Redirect to={"/signin"}/>);
      }

      if(this.decryption(queryString.parse(this.props.location.search).id) !== '') {
         var cus_id = this.decryption(queryString.parse(this.props.location.search).id)
         if(cus_id === '') {
            return (<Redirect to={"/signin"}/>);
            //console.log('cus_id blank')
         }
      } else {
         return (<Redirect to={"/signin"}/>);
         //console.log('query blank')
      }

      //console.log('-=-=-=-=-=-')
      //console.log(this.decryption(queryString.parse(this.props.location.search).id))
      return (
         <QueueAnim type="bottom" duration={2000}>
            <div className="rct-session-wrapper">
               {loading &&
                  <LinearProgress />
               }
              
               <div className="session-inner-wrapper">
                  <div className="container">
                     <div className="row row-eq-height h-100 justify-content-center align-items-center">
                     <div className="health-login">
                        <div className="col-sm-3 col-md-3 col-lg-3">
                        
                        </div>
                     
                        <div className="col-sm-6 col-md-6 col-lg-6 center-login">
                            <div className="session-logo">
                              <Link to="/">
                                 <img src={AppConfig.loginLogo} alt="Health Partner" className="img-fluid" />
                              </Link>
                           </div>
                           
                           {/*<div className="heading-container">
                                <h2>Health Financing Made Easy</h2>
                                <p>Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum</p>
                           </div>*/}
                           
                           <div className="session-body text-center">
                              <div className="session-head-radio text-left">
                                 <h4><strong>Please check your email and verify access token.</strong></h4>
                              </div>
                              <div className="verify-email-form">

                                 
                                 <div className="provider_fields">

                                    
                                    <FormGroup className="has-wrapper login-username">
                                       <TextField 
                                          id="access_token" 
                                          fullWidth 
                                          label="Access Token" 
                                          name="access_token" 
                                          type="string"
                                          placeholder="Access Token"
                                          error={(errors.access_token)?true:false}
                                          helperText={errors.access_token}
                                          onChange={(e) => this.validateSignUpForm('access_token', e.target.value)} 
                                       />
                                       <span className="has-icon"><i className="ti-eye"></i></span>
                                    </FormGroup>
                                    

                                    <FormGroup className="mb-15 mt-20">
                                       <Button
                                          color="primary"
                                          className={(this.validateSubmit()) ? "text-white btn-primary" : "text-white btn-error"}
                                          variant="contained"
                                          size="large"
                                          disabled={!this.validateSubmit()}
                                          onClick={this.customerSignUp}
                                       >
                                          VERIFY
                                       </Button>
                                    </FormGroup>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </div>
                        
                        <div className="col-sm-3 col-md-3 col-lg-3">
                           
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
         </QueueAnim>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser, CustomerRegisterReducer }) => {
   console.log('CustomerRegisterReducer')
   console.log(CustomerRegisterReducer)
   const { loading, redirectURL, customer_detail_verify, customer_redirect } = CustomerRegisterReducer;
   return { loading, redirectURL, customer_detail_verify, customer_redirect };
};

export default connect(mapStateToProps, {
   customerDetailVerify, updateCustomerFlag
})(SignupFirebase);
