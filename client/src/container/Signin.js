/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import TextField from '@material-ui/core/TextField';


import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isContainWhiteSpace } from '../validator/Validator';


// app config
import AppConfig from 'Constants/AppConfig';

// redux action
import {
   signinUserInApp,clearForgetState
} from 'Actions';



class Signin extends Component {

   constructor(props) {
      super(props)
      // reset login status
      //this.props.dispatch(userActions.logout());
      this.state = {
         formData: {}, // Contains login form data
         errors: {}, // Contains login field errors
         formSubmitted: false, // Indicates submit status of login form
         loading: false // Indicates in progress state of login form
      }
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Oct 14,2019
   */
   componentDidMount() {
      this.props.clearForgetState();
   }
   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { formData } = this.state;
      formData[name] = value;

      this.setState({
         formData: formData
      }, function () {
         this.validateLoginForm(name, value)
      });
   }
   validateSubmit() {
      return (
         this.state.errors.username === ''
         && this.state.errors.password === ''
      );
   }
   validateLoginForm = (fieldName, value) => {
      let { errors } = this.state;
      switch (fieldName) {
         case 'username':
            if (isEmpty(value)) {
               errors[fieldName] = "Username can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Username should not contain white spaces";
            } else {
               errors[fieldName] = '';
            }
            break;
         case 'password':
            if (isEmpty(value)) {
               errors[fieldName] = "Password can't be blank";
            } /*else if (isContainWhiteSpace(value)) {
               errors[fieldName] = "Password should not contain white spaces";
            } else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
               errors[fieldName] = "Password's length must between 6 to 16";
            } */else {
               errors[fieldName] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ errors: errors });
   }

   login = (e) => {
      e.preventDefault();
      const { username, password } = this.state.formData;
      //const { dispatch } = this.props;
      if (username && password) {
         //dispatch(signinUserInApp.login(username, password));
         this.props.signinUserInApp(this.state.formData, this.props.history);
      }
      //alert("You are successfully signed in...");


   }
   /**
    * On User Login
    */
   onUserLogin() {
      if (this.state.email !== '' && this.state.password !== '') {
         this.props.signinUserInFirebase(this.state, this.props.history);
      }
   }

   /**
    * On User Sign Up
    */
   onUserSignUp() {
      this.props.history.push('/signup');
   }


   render() {
      const { errors } = this.state;
      const { loading } = this.props;

      return (
         <QueueAnim type="bottom" duration={2000}>
            <div className="rct-session-wrapper">
               {loading &&
                  <LinearProgress />
               }

               <div className="session-inner-wrapper">
                  <div className="container">
                     <div className="row row-eq-height h-100 justify-content-center align-items-center">
                        <div className="health-login">
                           <div className="col-sm-4 col-md-4 col-lg-4">

                           </div>

                           <div className="col-sm-4 col-md-4 col-lg-4 center-login">
                              <div className="session-logo">
                                 <Link to="/">
                                    <img src={AppConfig.loginLogo} alt="Health Partner" className="img-fluid" />
                                 </Link>
                              </div>

                              {/*<div className="heading-container">
                                <h2>Health Financing Made Easy</h2>
                                <p>Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum Lorum ipsum</p>
                           </div>*/}

                              <div className="session-body text-center">
                                 <div className="session-head mb-30">

                                 </div>
                                 <Form onSubmit={this.login} className="login-form">
                                    <FormGroup className="has-wrapper login-username">
                                       <TextField
                                          id="name"
                                          fullWidth
                                          label="Username"
                                          name="username"
                                          type="string"
                                          placeholder="Enter user name"
                                          error={(errors.username) ? true : false}
                                          helperText={errors.username}
                                          onChange={this.handleInputChange}
                                       />
                                       <span className="has-icon"><i className="ti-email"></i></span>
                                    </FormGroup>
                                    <FormGroup className="has-wrapper login-password">
                                       <TextField
                                          id="Password"
                                          fullWidth
                                          label="Password"
                                          name="password"
                                          type="password"
                                          placeholder="Enter password"
                                          autoComplete="current-password"
                                          error={(errors.password) ? true : false}
                                          helperText={errors.password}
                                          onChange={this.handleInputChange}
                                       />
                                       <span className="has-icon"><i className="ti-lock"></i></span>
                                    </FormGroup>

                                    <Link to="/application/forgot-password" className="mb-10">Forget Password?</Link>
                                    <Link to="/signup" className="mb-10 ml-20">Create New Account</Link>
                                    <FormGroup className="mb-15">
                                       <Button
                                          color="primary"
                                          className="btn-block text-white w-100"
                                          variant="contained"
                                          size="large"
                                          type="submit"
                                          disabled={!this.validateSubmit()}
                                       >
                                          Sign In
                                    </Button>
                                    </FormGroup>

                                 </Form>
                              </div>

                           </div>

                           <div className="col-sm-4 col-md-4 col-lg-4">

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </QueueAnim>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
   const { user, loading } = authUser;
   return { user, loading }
}

export default connect(mapStateToProps, {
   signinUserInApp,clearForgetState
})(Signin);
