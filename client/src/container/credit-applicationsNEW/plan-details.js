/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import moment from 'moment';
import CryptoJS from 'crypto-js';

import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';


import {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice,adminPlanDetailsDownload
} from 'Actions';
class PlanDetails extends Component {

   state = {
      singleView: null,
      openInstallmentDialog: false,
      installmentDetails: null,
   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   componentDidMount() {
      this.props.clearRedirectURL();
      this.props.allPlanDetails(this.dec(this.props.match.params.appid));
   }
   /*
   * Title :- viewSinglePlan
   * Descrpation :- This function use for create invoice according to due date
   * Date :- Dec 5, 2019
   * Author :- Ramesh Kumar
   */
   createInvoce(date, appid) {
      date = moment(date, 'MM/DD/YYYY', true).format('YYYY-MM-DD');
      this.props.createInvoice(date, appid)
   }

   downloadSinglePlan(appid, planid) {
      this.props.adminPlanDetailsDownload(appid, planid)
   }
   /*
   * Title :- viewSinglePlan
   * Descrpation :- This function use for get single plan information
   * Date :- Dec 5, 2019
   * Author :- Ramesh Kumar
   */
   viewSinglePlan(id) {
      var action = 0;
      var totalPay = 0;
      var blacAmt = 0;
      const { amountDetails } = this.props;
      var data = this.props.application_plans.filter(function (item) {
         return item.pp_id == id;
      }).map(function ({ pp_id, pp_installment_id, amount, amount_rcvd, due_date, installment_amt, paid_flag, partial_paid, invoice_status, payment_date, paidOld }) {
         if (new Date() > new Date(due_date) || invoice_status == 1) {
            action = 0;
         } else if (action == 0) {
            action = 1;
            action = 1;
         } else {
            action = 2;
         }
         var current_installment = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
            if (planindex == 0) {
               accumulator['amountrcvd'] = 0;
               accumulator['paid_date'] = '-';
            }
            if (currentplan.pp_installment_id == pp_installment_id) {
               accumulator['amountrcvd'] += currentplan.amount_rcvd;
               accumulator['paid_date'] = currentplan.payment_date;
            }

            return accumulator;
         }, []);

         amount_rcvd = current_installment.amountrcvd;
         payment_date = current_installment.paid_date;
         totalPay += (amount_rcvd != null) ? amount_rcvd : 0;
         //blacAmt = amount.toFixed(2) - totalPay.toFixed(2);

         blacAmt = parseFloat(amount.toFixed(2)) - parseFloat(totalPay.toFixed(2));
         return { pp_id, pp_installment_id, amount, amount_rcvd, due_date, installment_amt, paid_flag, partial_paid, invoice_status, payment_date, action, totalPay, blacAmt, paidOld };
      });
      this.setState({ singleView: data })
   }
   /*
   * Title :- viewInstallmentDetails
   * Descrpation :- This function use for view particular installment details
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Dec 19,2019
   */
   viewInstallmentDetails(id) {
      var installmentDetails = this.props.amountDetails.filter(function (item) {
         return item.pp_installment_id == id;
      });
      console.log(installmentDetails)
      this.setState({ openInstallmentDialog: true, installmentDetails: installmentDetails });
   }
   /*
   * Title :- closeInstallmentDialog
   * Descrpation :- This function use for close view popup box
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Dec 19,2019
   */
   closeInstallmentDialog = () => {
      this.setState({ openInstallmentDialog: false, installmentDetails: null })
   }

   mainInvoice() {
      this.setState({ singleView: null })
   }

   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   adminPlanDetails() {
      this.props.adminPlanDetailsDownload(this.dec(this.props.match.params.appid));
   }
   render() {
      const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;

      let totalLateAmount = 0;
      let totalFinCharge = 0;
      //var uniqueLoanAmount = application_plans && [...new Set(application_plans.map(item => item.amount))];
      var uniqueLoanAmount = 0;
      application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
            uniqueLoanAmount += currentValue.loan_amount;
         }
         return accumulator;
      }, []);
      //var uniqueLoanAmount = amountDetails && [...new Set(amountDetails.map(item => item.amount_rcvd))];
      var uniqueLateCount = application_plans && [...new Set(application_plans.map(item => item.late_count))];
      var uniqueMissedCount = application_plans && [...new Set(application_plans.map(item => item.missed_count))];

      // get recived amount according to invoice
      var UniqueReceived = amountDetails && amountDetails.reduce(function (accumulator, currentValue, currentindex) {
         if (currentindex == 0) {
            accumulator = 0;
         }
         accumulator += currentValue.amount_rcvd;
         return accumulator;
      }, []);

      // get plan details group by
      
      var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            // get recived amount
            var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
               if (planindex == 0) {
                  accumulator = 0;
               }

               accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
               return accumulator;
            }, 0);
            accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.amount, emi: currentValue.installment_amt, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created };
         } else {
            //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
         }

         return accumulator;
      }, []);
      var groupArrays = invoiceDetails && invoiceDetails.map((data, idx) => {
         //console.log(moment().format('YYYY-MM-DD'))
         //data.due_date = '01/31/2019';
         //console.log(moment(data.due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD'))
         //if (new Date() > new Date(data.due_date)) {
         /*if (moment(moment(data.due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')) && data.paid_flag != 1 && data.paid_flag != 4) {
            data.missed_flag = 1;
         } else {
            data.missed_flag = 0;
         }*/
         if(invoiceDetails.length > (idx+1) && data.paid_flag != 1 && data.paid_flag != 4){
            data.missed_flag = 1;
         }else{
            data.missed_flag = 0;
         }
         data.fin_charge_amt = (data.fin_charge_amt != null) ? (data.finpct != null) ? (parseFloat(data.fin_charge_amt) - (parseFloat(data.finpct) * parseFloat(data.fin_charge_amt)) / 100) : parseFloat(data.fin_charge_amt) : 0;
         data.late_fee_received = (data.late_fee_received != null) ? (data.latepct != null) ? (parseFloat(data.late_fee_received) - (parseFloat(data.latepct) * parseFloat(data.late_fee_received)) / 100) : parseFloat(data.late_fee_received) : 0;

         data.previous_fin_charge = (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
         data.previous_late_fee = (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;

         data.total_due = data.payment_amount;
         data.paid_amount1 = (data.paid_flag == 1 || data.paid_flag == 4) ? (data.paid_amount + data.fin_charge_amt + data.late_fee_received + data.additional_amount + data.previous_fin_charge + data.previous_late_fee) : 0;
         data.total_due += (data.late_fee_received) ? data.late_fee_received : 0;
         data.total_due += (data.fin_charge_amt) ? data.fin_charge_amt : 0;
         data.total_due += (data.previous_late_fee) ? data.previous_late_fee : 0;
         data.total_due += (data.previous_fin_charge) ? data.previous_fin_charge : 0;

         if (idx != 0) {

            data.previous_blc += (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
            data.previous_blc += (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;
         }
         if (data.paid_flag == 1 || data.paid_flag == 4) {
            totalLateAmount += data.late_fee_received + data.previous_late_fee;
            totalFinCharge += data.fin_charge_amt + data.previous_fin_charge;
         }
         data.finalAmt = (data.paid_amount1 <= data.total_due) ? data.total_due - data.paid_amount1 : 0;
         return data;
      })
      var nextDueDate = '';
      if (invoiceDetails !== undefined && invoiceDetails.length > 0) {

         //if (invoiceDetails[invoiceDetails.length - 1].paid_flag == 1 || new Date() > new Date(invoiceDetails[invoiceDetails.length - 1].due_date)) {
         if (invoiceDetails[invoiceDetails.length - 1].paid_flag == 1 || invoiceDetails[invoiceDetails.length - 1].paid_flag == 4 || moment(moment(invoiceDetails[invoiceDetails.length - 1].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD'))) {
            var currentDate = invoiceDetails[invoiceDetails.length - 1].due_date;
            var nextInvoice = application_plans.filter(function (e) {
               return (new Date(e.due_date) > new Date(currentDate));
            });
            if (nextInvoice.length > 0) {
               nextInvoice = nextInvoice.reduce(function (a, b) {
                  return new Date(a.due_date) < new Date(b.due_date) ? a : b;
               });
               nextDueDate = nextInvoice.due_date;
            }
         }

      } else {
         var nextInvoice = application_plans && application_plans.filter(function (e) {
            return (new Date(e.due_date) > new Date());
         });
         if (application_plans && nextInvoice.length > 0) {
            nextInvoice = nextInvoice.reduce(function (a, b) {
               return new Date(a.due_date) < new Date(b.due_date) ? a : b;
            });
            nextDueDate = nextInvoice.due_date;
         }
      }

      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
                           </li>
                           <li>
                              <a href="javascript:void(0)" onClick={this.adminPlanDetails.bind(this)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                           </li>
                        </ul>
                     </div>
                     {this.props.application_plans &&
                        <div className="p-10">

                           <h1 className="text-center mb-20">Customer Account</h1>
                           {this.props.application_plans[0].plan_updated == 1 &&
                              <div class="alert alert-danger alert-dismissible fade show">
                                 Now this plan apply new Interest rate due to missed payment. Available Credit has been locked call HPS at (919) 600-5526 for any information.
                              </div>
                           }
                           <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Customer Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-nowrap"><strong>Application No :</strong></td>
                                          <td>{(application_details) ? application_details[0].application_no : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Name :</strong></td>
                                          <td className="text-capitalize">{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</td>
                                       </tr>

                                       <tr>
                                          <td><strong>Address :</strong></td>
                                          <td>{(application_details) ? (application_details[0].address1 + ', ' + application_details[0].City + ', ' + application_details[0].name + ', ' + application_details[0].zip_code) : '-'}

                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Phone :</strong></td>
                                          <td>{(application_details) ? application_details[0].peimary_phone : '-'}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Loan Information</th>
                                       </tr>
                                       <tr>
                                          <td><strong>Line Of Credit :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].approve_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Available Balance :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].remaining_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Principal Amount :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(uniqueLoanAmount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Outstanding Principal :</strong></td>
                                          <td>
                                             ${parseFloat(uniqueLoanAmount - UniqueReceived).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Payment Information</th>
                                       </tr>

                                       <tr>
                                          <td><strong>Late Payments :</strong></td>
                                          <td>
                                             {uniqueLateCount.reduce((a, b) => a + b, 0)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Payments Missed :</strong></td>
                                          <td>
                                             {uniqueMissedCount.reduce((a, b) => a + b, 0)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Late Fees :</strong></td>
                                          <td>
                                             ${parseFloat(totalLateAmount).toFixed(2)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Financial Charges :</strong></td>
                                          <td>
                                             ${parseFloat(totalFinCharge).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>

                           <div className="table-responsive mb-5 pymt-history">
                              <h3 className="text-center mb-10">Payment Plans <i className="ti-arrow-right"></i> <a href="javascript:void(0);" onClick={() => this.mainInvoice()}>View Invoice History</a></h3>
                              <table className="table table-borderless admin-application-payment-plan">
                                 <thead>
                                    <tr>
                                       <th>Plan ID</th>
                                       <th>Principal Amt</th>
                                       <th>Outstanding Principal Amt</th>
                                       <th>Payment Term</th>
                                       <th>APR(%)</th>
                                       <th>Monthly Payment</th>
                                       <th>Date Created</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    {planDetails.map(function (row, idx) {
                                       return (<tr key={idx}>
                                          <td>{row.pp_id}</td>
                                          <td>${(row.loan_amount).toFixed(2)}</td>
                                          <td>${(row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2)}</td>
                                          <td>{row.payment_term_month} Month</td>
                                          <td>{parseFloat(row.discounted_interest_rate).toFixed(2)}%</td>
                                          <td>${parseFloat(row.emi).toFixed(2)}</td>
                                          <td>{row.date_created}</td>
                                          <td><a href="javascript:void(0);" onClick={() => this.viewSinglePlan(row.pp_id)}><i className="zmdi zmdi-eye icon-fr"></i></a> <a href="javascript:void(0);" onClick={() => this.downloadSinglePlan(this.dec(this.props.match.params.appid), row.pp_id)}><i className="zmdi zmdi-download icon-fr"></i></a></td>
                                       </tr>)
                                    }.bind(this))
                                    }
                                 </tbody>
                              </table>
                           </div>

                           <div className={(this.state.singleView != null) ? 'table-responsive mb-40 pymt-history d-none' : 'table-responsive mb-40 pymt-history'}>
                              <h3 className="text-center mb-10">Invoice History {(nextDueDate != '') ? <a href="javascript:void(0);" onClick={() => this.createInvoce(nextDueDate, this.dec(this.props.match.params.appid))} title="Create Invoice"><i className="zmdi zmdi-file-plus icon-fr"></i></a> : ''}</h3>
                              <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                 <thead>
                                    <tr>
                                       <th>S. No.</th>
                                       <th>Invoice No.</th>
                                       <th>Payment Date</th>
                                       <th>Due Date</th>
                                       <th>Prev Bal</th>
                                       <th>Monthly Payment</th>
                                       <th>Late Fee</th>
                                       <th>Fin Charge</th>
                                       <th>Total Due</th>
                                       <th>Min Due</th>
                                       <th>Additional Amt</th>
                                       <th>Paid</th>
                                       <th>Balance</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    {groupArrays && groupArrays.map(function (row, idx) {
                                       return <tr key={idx} className={idx}>
                                          <td>{idx + 1}</td>
                                          <td>{row.invoice_number}</td>
                                          <td>{(row.payment_date) ? row.payment_date : '-'}</td>
                                          <td>{row.due_date}</td>
                                          <td>{(row.previous_blc > 0) ? '$' + parseFloat(row.previous_blc).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.payment_amount).toFixed(2)}</td>
                                          <td>{(row.late_fee_received) ? '$' + parseFloat(row.late_fee_received).toFixed(2) : '-'}</td>
                                          <td>{(row.fin_charge_amt) ? '$' + parseFloat(row.fin_charge_amt).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>{(row.additional_amount > 0) ? '$' + parseFloat(row.additional_amount).toFixed(2) : '-'}</td>
                                          <td>{(row.paid_amount1 > 0) ? '$' + parseFloat(row.paid_amount1).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.finalAmt).toFixed(2)}</td>
                                          <td>

                                             {(row.missed_flag == 1) ?
                                                'Missed'
                                                :
                                                (row.paid_flag == 1) ?
                                                   'Paid'
                                                   :
                                                   (row.paid_flag == 3) ?
                                                      'Due'
                                                      :
                                                      (row.paid_flag == 4) ?
                                                         'Partial Paid'
                                                         :
                                                         'In progress'
                                             }
                                          </td>
                                          <td>
                                             {(row.missed_flag == 1) ?
                                                '-'
                                                :
                                                (row.paid_flag == 3 || row.paid_flag == 2) ?
                                                   <div><Link to={`/admin/credit-application/payment/${this.enc(row.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>
                                                      <Link to={`/admin/credit-application/invoice/${this.enc(row.invoice_id.toString())}`} title="Invoice"><i className="zmdi zmdi-file-text icon-fr"></i></Link></div>
                                                   :
                                                   (row.paid_flag == 1 || row.paid_flag == 4) ?
                                                      <Link to={`/admin/credit-application/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>
                                                      :
                                                      '-'
                                             }</td>
                                       </tr>
                                    }.bind(this))
                                    }
                                 </tbody>
                              </table>
                           </div>

                           <div className={(this.state.singleView != null) ? 'table-responsive mb-40 pymt-history' : 'table-responsive mb-40 pymt-history d-none'}>
                              <h3 className="text-center mb-10">Payment History</h3>
                              <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                 <thead>
                                    <tr>
                                       <th>S. No.</th>
                                       <th>Amount Due</th>
                                       <th>Due Date</th>
                                       <th>Amount Paid</th>
                                       <th>Payment Date</th>
                                       <th>Total Paid</th>
                                       <th>Balance</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {this.state.singleView && this.state.singleView.map(function (row, idx) {
                                       return <tr key={idx}>
                                          <td>{idx + 1}</td>
                                          <td>${parseFloat(row.installment_amt).toFixed(2)}</td>
                                          <td>{row.due_date}</td>
                                          <td>{(row.amount_rcvd) ? '$' + parseFloat(row.amount_rcvd).toFixed(2) : '-'}</td>
                                          <td>{(row.payment_date) ? row.payment_date : '-'}</td>
                                          <td>{(row.totalPay > 0) ? '$' + parseFloat(row.totalPay).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.blacAmt).toFixed(2)}</td>
                                          <td>
                                             {(row.paid_flag == 1) ? 'Paid' : (row.partial_paid == 1) ? 'Partial Paid' : 'Unpaid'
                                             }
                                          </td>
                                          <td>
                                             {(row.paid_flag == 1 || row.partial_paid == 1) ? <a href="javascript:void(0)" onClick={() => this.viewInstallmentDetails(row.pp_installment_id)}><i className="zmdi zmdi-eye icon-fr"></i></a> : '-'}
                                          </td>
                                       </tr>
                                    }.bind(this))
                                    }

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                     <Modal isOpen={this.state.openInstallmentDialog} toggle={() => this.closeInstallmentDialog()}>
                        <ModalHeader toggle={() => this.closeInstallmentDialog()}>
                           Payment Details
                        </ModalHeader>
                        <ModalBody>
                           {this.state.installmentDetails !== null &&
                              <div className={'table-responsive mb-10 table-sm'}>
                                 <table className="table">
                                    <thead>
                                       <tr>
                                          <th>S. No.</th>
                                          <th>Plan Number</th>
                                          <th>Invoice Number</th>
                                          <th>Amount Paid</th>
                                          <th>Payment Date</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       {this.state.installmentDetails.map(function (row, idx) {
                                          return <tr key={idx}>
                                             <td>{idx + 1}</td>
                                             <td>{row.plan_number}</td>
                                             <td>{row.invoice_number}</td>
                                             <td>{(row.amount_rcvd) ? '$' + parseFloat(row.amount_rcvd).toFixed(2) : '-'}</td>
                                             <td>{row.payment_date}</td>
                                          </tr>
                                       })
                                       }

                                    </tbody>
                                 </table>
                              </div>
                           }
                        </ModalBody>
                        <ModalFooter>

                        </ModalFooter>
                     </Modal>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer }) => {

   const { nameExist, isEdit } = authUser;
   const { loading, application_details, invoiceDetails, application_plans, amountDetails } = PaymentPlanReducer;
   return { loading, application_details, invoiceDetails, application_plans, amountDetails }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice, adminPlanDetailsDownload
})(PlanDetails);