/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Link } from 'react-router-dom';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import {
   rejectApplication
} from 'Actions';

class rejectLatter extends Component {

   state = {

   }
   /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.rejectApplication(this.props.match.params.id);
   }
   printLatter() {
      window.print();
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   render() {
      const { loading, appRejectDetails, appRejectDetailsAddress } = this.props;

      //const addressDetails = (appDetailsAddress != '')?appDetailsAddress.filter(x => x.primary_address==1):'';

      console.log(appRejectDetailsAddress);
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.invoice" />} match={this.props.match} />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
                           </li>
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />

                           </li>
                        </ul>

                     </div>
                     {appRejectDetails &&
                        <div className="rejection-letter p-50" ref={el => (this.componentRef = el)}>
                           <p>
                              {(appRejectDetails.gender == 'M') ? 'Mr.' : 'Mrs.'} {appRejectDetails.f_name + ' ' + appRejectDetails.m_name + ' ' + appRejectDetails.l_name}<br />
                              {appRejectDetailsAddress[0].address1 + ' ' + appRejectDetailsAddress[0].address2},<br />
                              {appRejectDetailsAddress[0].City},<br />
                              {appRejectDetailsAddress[0].state_name},<br />
                              {appRejectDetailsAddress[0].country_name}<br /><br />
                           </p>

                           <p><strong>Subject: Credit application – Line of credit not approved</strong></p>
                           <p>Dear {appRejectDetails.f_name + ' ' + appRejectDetails.m_name + ' ' + appRejectDetails.l_name},
                           </p>
                           <p>
                              We want to thank you for submitting the credit application. Based on the information provided, your application was evaluated and reviewed on many criteria’s including your employment history, credit score and credit history. We regret to inform you that we are unable to extend line of credit at this time.
                              </p>
                           <p>
                              We look forward to serve you in the future. If you have any additional question, don’t hesitate to call us at (919) 600-5526. Our friendly customer service associates are eager to help.
                              </p>
                           <br />
                           <p>
                              Thanks, you very much for considering Health partner for your medical needs.
                              </p>
                           <br />
                           <br />
                           <p>
                              <strong>Sincerely</strong>
                           </p>
                           <p>
                              <strong>Health Partner Inc.</strong>
                           </p>
                        </div>
                     }
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, appRejectDetails, appRejectDetailsAddress } = creditApplication;
   return { loading, appRejectDetails, appRejectDetailsAddress }

}

export default connect(mapStateToProps, {
   rejectApplication
})(rejectLatter);