/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import {
   viewReceipt
} from 'Actions';
class Receipt extends Component {

   state = {
      payment_history: '',
      payment_history_data: false,
      nested_arr_row: '',
      nested_arr_row_data: false,
      plan_row: '',
      plan_row_data: false,
      paid_status: '',
      current_month_invoice_sum: 0,
      current_month_invoice: '',
      current_month_invoice_data: false,
      overdue_month_invoice_grp: '',
      overdue_month_invoice_grp_data: false,
      invoice_last_index_value: 0,
      all_plans_total_amount: 0,
      single_plan_total: 0
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }

   componentDidMount() {
      console.log(this.dec(this.props.match.params.appid))
      this.props.viewReceipt(this.dec(this.props.match.params.appid));
   }



   checkLateDiscount() {
      if (this.props.recipitDetails.latepct != null) {
         if ((this.props.recipitDetails.previous_late_fee != null)) {

            var newLateFee = '$' + (((parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.late_fee_received)) - (parseFloat(this.props.recipitDetails.latepct) * (parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.late_fee_received))) / 100)).toFixed(2);
         } else {
            var newLateFee = '$' + ((parseFloat(this.props.recipitDetails.late_fee_received) - (parseFloat(this.props.recipitDetails.latepct) * parseFloat(this.props.recipitDetails.late_fee_received)) / 100)).toFixed(2)
         }
         return <tr>
            <td><strong>Late Fee After Discount :</strong></td>
            <td>{newLateFee}</td>
         </tr>
      } else {
         return <tr></tr>;
      }
   }
   checkFinDiscount() {
      if (this.props.recipitDetails.finpct != null) {
         if ((this.props.recipitDetails.previous_fin_charge != null)) {
            var newLateFin = '$' + (((parseFloat(this.props.recipitDetails.previous_fin_charge) + parseFloat(this.props.recipitDetails.fin_charge_amt)) - (parseFloat(this.props.recipitDetails.finpct) * (parseFloat(this.props.recipitDetails.previous_fin_charge) + parseFloat(this.props.recipitDetails.fin_charge_amt))) / 100)).toFixed(2);
         } else {
            var newLateFin = '$' + ((parseFloat(this.props.recipitDetails.fin_charge_amt) - (parseFloat(this.props.recipitDetails.finpct) * parseFloat(this.props.recipitDetails.fin_charge_amt)) / 100)).toFixed(2);
         }
         return <tr>
            <td><strong>Late Fee After Discount :</strong></td>
            <td>{newLateFin}</td>
         </tr>
      } else {
         return <tr></tr>;
      }
   }

   checkTotalAmount() {
      // get late fin
      var newLateFin = 0;
      var newLateFee = 0;
      if (this.props.recipitDetails.finpct != null) {
         if ((this.props.recipitDetails.previous_fin_charge != null)) {
            newLateFin = (((parseFloat(this.props.recipitDetails.previous_fin_charge) + parseFloat(this.props.recipitDetails.fin_charge_amt)) - (parseFloat(this.props.recipitDetails.finpct) * (parseFloat(this.props.recipitDetails.previous_fin_charge) + parseFloat(this.props.recipitDetails.fin_charge_amt))) / 100)).toFixed(2);
         } else {
            newLateFin = ((parseFloat(this.props.recipitDetails.fin_charge_amt) - (parseFloat(this.props.recipitDetails.finpct) * parseFloat(this.props.recipitDetails.fin_charge_amt)) / 100)).toFixed(2);
         }
      } else {
         if ((this.props.recipitDetails.previous_fin_charge != null)) {
            newLateFin = parseFloat(this.props.recipitDetails.previous_fin_charge) + parseFloat(this.props.recipitDetails.fin_charge_amt);
         } else {
            newLateFin = parseFloat(this.props.recipitDetails.fin_charge_amt);
         }
      }
      // get late feee
      if (this.props.recipitDetails.latepct != null) {
         if ((this.props.recipitDetails.previous_late_fee != null)) {
            newLateFee = (((parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.late_fee_received)) - (parseFloat(this.props.recipitDetails.latepct) * (parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.late_fee_received))) / 100)).toFixed(2);
         } else {
            newLateFee = ((parseFloat(this.props.recipitDetails.previous_late_fee) - (parseFloat(this.props.recipitDetails.latepct) * parseFloat(this.props.recipitDetails.previous_late_fee)) / 100)).toFixed(2)
         }
      } else {
         if ((this.props.recipitDetails.previous_late_fee != null)) {
            newLateFee = parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.late_fee_received);
         } else {
            newLateFee = parseFloat(this.props.recipitDetails.previous_late_fee);
         }
      }
      var addAmt = (this.props.recipitDetails.additional_amount != null) ? this.props.recipitDetails.additional_amount : 0;

      addAmt = (isNaN(addAmt)) ? 0 : addAmt;
      newLateFee = (isNaN(newLateFee)) ? 0 : newLateFee;
      newLateFin = (isNaN(newLateFin)) ? 0 : newLateFin;

      return '$' + (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(this.props.recipitDetails.paid_amount) + parseFloat(addAmt)).toFixed(2)
   }

   //${parseFloat(parseFloat(this.props.recipitDetails.payment_amount) + parseFloat(this.props.recipitDetails.late_fee_received) + parseFloat(this.props.recipitDetails.fin_charge_amt) + parseFloat(this.props.recipitDetails.additional_amount) + parseFloat(this.props.recipitDetails.previous_late_fee) + parseFloat(this.props.recipitDetails.previous_fin_charge)).toFixed(2)}
   goBack(){
      this.props.history.goBack(-1)
   }
   render() {

      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.receipt" />} match={this.props.match} />
            {this.props.recipitDetails &&
               <div className="row">

                  <div className="col-sm-12 mx-auto">
                     <RctCard>
                        <div className="invoice-head text-right">
                           <ul className="list-inline">
                              <li>
                                 <Link to="#" onClick={this.goBack.bind(this)} title="Cancel"><i className="mr-10 material-icons">arrow_back</i></Link>
                              </li>
                              <li>
                                 <ReactToPrint
                                    trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                    content={() => this.componentRef}
                                 />
                              </li>
                           </ul>
                        </div>
                        <div className="customer-invoice pt-5 pb-5 px-50" ref={el => (this.componentRef = el)}>
                           <div className="d-flex justify-content-between">
                              <div className="sender-address">
                                 <div className="invoice-logo mb-10">
                                    <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid" /><br />
                                    <span>Health Partner Inc.</span><br />
                                    <span>5720 Creedmoor Rd, Suite 103</span><br />
                                    <span>Raleigh, NC 27612</span>
                                 </div>

                              </div>
                              <div className="invoice-address text-right">
                                 <span>Invoice: #{this.props.recipitDetails.invoice_number}</span>
                                 <span>{this.props.recipitDetails.payment_date}</span>
                                 <span className="invoice_status">{(this.props.recipitDetails.paid_flag == 1) ? 'PAID' : (this.props.recipitDetails.paid_flag == 4) ? 'Partial Paid' : ''}</span>
                              </div>
                           </div>

                           <div className="d-flex justify-content-between mb-10 add-full-card customer-detail-info">

                              <div className="add-card">
                                 <table>
                                    <tbody>

                                       <tr>
                                          <td className="text-capitalize text-left">
                                             {this.props.reciptCustomer.f_name + ' ' + this.props.reciptCustomer.m_name + ' ' + this.props.reciptCustomer.l_name}
                                          </td>
                                       </tr>

                                       <tr>
                                          <td className="text-left">
                                             {this.props.reciptCustomer.address1 + ' ' + this.props.reciptCustomer.address2}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className="text-left">
                                             {this.props.reciptCustomer.City + ' ' + this.props.reciptCustomer.state_name + '-' + this.props.reciptCustomer.zip_code}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className="text-left">
                                             {this.props.reciptCustomer.peimary_phone}
                                          </td>
                                       </tr>

                                    </tbody>
                                 </table>
                              </div>

                              <div className="add-card">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <td><strong>Invoice Date :</strong></td>
                                          <td>{this.props.recipitDetails.due_date}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Account Number :</strong></td>
                                          <td>
                                             {this.props.reciptCustomer.patient_ac}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Payment Amount :</strong></td>
                                          <td>
                                             ${parseFloat(this.props.recipitDetails.payment_amount).toFixed(2)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Late Fee
                                             {
                                                (this.props.recipitDetails.previous_late_fee != null && this.props.recipitDetails.previous_late_fee > 0) ?
                                                   ' ($' + parseFloat(this.props.recipitDetails.previous_late_fee).toFixed(2) + ' + $' + parseFloat(this.props.recipitDetails.late_fee_received).toFixed(2) + ')'
                                                   :
                                                   ''
                                             } :</strong></td>
                                          <td>
                                             ${
                                                (parseFloat(this.props.recipitDetails.late_fee_received) > 0) ?
                                                   (this.props.recipitDetails.previous_late_fee != null) ?
                                                      (parseFloat(this.props.recipitDetails.late_fee_received) + parseFloat(this.props.recipitDetails.previous_late_fee)).toFixed(2)
                                                      :
                                                      parseFloat(this.props.recipitDetails.late_fee_received).toFixed(2)
                                                   :
                                                   '0.00'
                                             }
                                          </td>
                                       </tr>
                                       {this.checkLateDiscount()}
                                       <tr>
                                          <td><strong>Finance Charge
                                             {
                                                (this.props.recipitDetails.previous_fin_charge != null && this.props.recipitDetails.previous_fin_charge > 0) ?
                                                   ' ($' + parseFloat(this.props.recipitDetails.previous_fin_charge).toFixed(2) + ' + $' + parseFloat(this.props.recipitDetails.fin_charge_amt).toFixed(2) + ')'
                                                   :
                                                   ''
                                             } :</strong></td>
                                          <td>
                                             ${
                                                (parseFloat(this.props.recipitDetails.fin_charge_amt) > 0) ?
                                                   (this.props.recipitDetails.previous_fin_charge != null) ?
                                                      (parseFloat(this.props.recipitDetails.fin_charge_amt) + parseFloat(this.props.recipitDetails.previous_fin_charge)).toFixed(2)
                                                      :
                                                      parseFloat(this.props.recipitDetails.fin_charge_amt).toFixed(2)
                                                   :
                                                   '0.00'
                                             }
                                          </td>
                                       </tr>
                                       {this.checkFinDiscount()}

                                       <tr>
                                          <td><strong>Additional Amount :</strong></td>
                                          <td>
                                             ${(parseFloat(this.props.recipitDetails.additional_amount) > 0) ? parseFloat(this.props.recipitDetails.additional_amount).toFixed(2) : '0.00'}
                                          </td>
                                       </tr>

                                       <tr>
                                          <td><strong>Total Amount :</strong></td>
                                          <td>
                                             {this.checkTotalAmount()}

                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <div className="paymt-voucher">
                              <h4 className="text-center mb-10">PAYMENT METHODS</h4>
                              <div className="d-flex justify-content-between mb-10 add-full-card customer-detail-info">

                                 <div className="add-card w-100 mw-100">
                                    <table>
                                       <tbody>

                                          <tr>
                                             <td>Payment Method :</td>
                                             <td>
                                                {(this.props.recipitDetails.payment_method == 1) ? 'ACH / eCheck' : ''}
                                                {(this.props.recipitDetails.payment_method == 2) ? 'Credit Card' : ''}
                                                {(this.props.recipitDetails.payment_method == 3) ? 'Check / Via Mail' : ''}
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>Paid Date :</td>
                                             <td>
                                                {(this.props.recipitDetails.payment_date) ? this.props.recipitDetails.payment_date : ''}
                                             </td>
                                          </tr>
                                          {(this.props.recipitDetails.payment_method == 1) &&
                                             <React.Fragment>
                                                <tr>
                                                   <td>Bank Name :</td>
                                                   <td>
                                                      {(this.props.recipitDetails.ach_bank_name) ? this.props.recipitDetails.ach_bank_name : '-'}
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>Routing No. :</td>
                                                   <td>
                                                      {(this.props.recipitDetails.ach_routing_no) ? this.props.recipitDetails.ach_routing_no : '-'}
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>Account No. :</td>
                                                   <td>
                                                      {(this.props.recipitDetails.ach_account_no) ? this.props.recipitDetails.ach_account_no : '-'}
                                                   </td>
                                                </tr>
                                             </React.Fragment>
                                          }
                                          <tr>
                                             <td>Note :</td>
                                             <td>
                                                {(this.props.recipitDetails.comments) ? this.props.recipitDetails.comments : '-'}
                                             </td>
                                          </tr>

                                       </tbody>
                                    </table>
                                 </div>
                              </div>



                           </div>
                           <div className="mb-30 table-responsive mb-40 pymt-history">
                              <h4 className="text-center mb-10">PAYMENT DETAILS</h4>
                              <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                 <thead>
                                    <tr>
                                       <th>#</th>
                                       <th>Plan Number</th>
                                       <th>Due Date</th>
                                       <th>Monthly Payment</th>
                                       <th>Paid Amount</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {this.props.installmentDetails && this.props.installmentDetails.map(function (row, idx) {
                                       return <tr key={idx}>
                                          <td>{idx + 1}</td>
                                          <td>{row.plan_number}</td>
                                          <td>{row.due_date}</td>
                                          <td>${parseFloat(row.installment_amt).toFixed(2)}</td>
                                          <td>${parseFloat(row.amount_rcvd).toFixed(2)}</td>
                                          <td>{(row.partial_paid == 1) ? 'Partial Paid' : (row.paid_flag == 1) ? 'Paid' : 'Unpaid'}
                                          </td>
                                       </tr>
                                    })
                                    }
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </RctCard>
                  </div>
               </div>
            }
            {this.props.loading &&
               <RctSectionLoader />
            }
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer }) => {

   const { loading, recipitDetails, reciptCustomer, installmentDetails } = PaymentPlanReducer;
   return { loading, recipitDetails, reciptCustomer, installmentDetails }
}

export default connect(mapStateToProps, {
   viewReceipt
})(Receipt);