/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ MFSScoreList, interestRateMList, loanAmountList, paymentTermMonthList, addErr, addNewInterestRateDetails, onChangeaddNewInterestRateDetail,DatePicker,startDate }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-6">

	        <FormGroup>
            <Label for="score_range">MFS Score<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="score_range"
                id="score_range"
                placeholder=""
                onChange={(e) => onChangeaddNewInterestRateDetail('score_range', e.target.value)}
            >
                <option value="">Select</option>
                {MFSScoreList && MFSScoreList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(addErr.score_range) ? <FormHelperText>{addErr.score_range}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="interest_rate">Interest Rate<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="interest_rate"
                id="interest_rate"
                placeholder=""
                onChange={(e) => onChangeaddNewInterestRateDetail('interest_rate', e.target.value)}
            >
                <option value="">Select</option>
                {interestRateMList && interestRateMList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(addErr.interest_rate) ? <FormHelperText>{addErr.interest_rate}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="loan_amount">Loan Amount<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="loan_amount"
                id="loan_amount"
                placeholder=""
                onChange={(e) => onChangeaddNewInterestRateDetail('loan_amount', e.target.value)}
            >
                <option value="">Select</option>
                {loanAmountList && loanAmountList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(addErr.loan_amount) ? <FormHelperText>{addErr.loan_amount}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="payment_term_month">Payment Term Month<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="payment_term_month"
                id="payment_term_month"
                placeholder=""
                onChange={(e) => onChangeaddNewInterestRateDetail('payment_term_month', e.target.value)}
            >
                <option value="">Select</option>
                {paymentTermMonthList && paymentTermMonthList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(addErr.payment_term_month) ? <FormHelperText>{addErr.payment_term_month}</FormHelperText> : ''}
        	</FormGroup>

        </div>

        

        
        <div className="col-md-6">
        <FormGroup>
			<Label for="effective_date">Effective Date<span className="required-field">*</span></Label>
			<DatePicker
                dateFormat="MM/dd/yyyy"
                name="effective_date"
                id="effective_date"
                selected={startDate}
                placeholderText="MM/DD/YYYY"
                autocomplete={false}
                onChange={(e) => onChangeaddNewInterestRateDetail('effective_date', e)}
                />
                {(addErr.effective_date) ? <FormHelperText>{addErr.effective_date}</FormHelperText> : ''}
            
		</FormGroup>
        </div>
        <div className="col-md-6">
	        <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(addNewInterestRateDetails.status == 1) ? true : false}
                            onChange={(e) => onChangeaddNewInterestRateDetail('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(addNewInterestRateDetails.status == 0) ? true : false}
                            onChange={(e) => onChangeaddNewInterestRateDetail('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>   
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
