/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';

const UpdateAmountForm = ({ updateAmtErr, updateAmountDetails, onUpdateAmountDetail, selectedOvrAmt, selectedOvrAmtCmt }) => (
    <Form>
    <div className="row">
        
        
        <div className="col-md-12">
            <FormGroup>
               <Label for="overide_amount">Amount<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="overide_amount"
                  id="overide_amount"
                  fullWidth
                  variant="outlined"
                  placeholder="Enter the amount"
                  value={(selectedOvrAmt != '') ? selectedOvrAmt : ''}
                  error={(updateAmtErr.overide_amount) ? true : false}
                  helperText={updateAmtErr.overide_amount}
                  onChange={(e) => onUpdateAmountDetail('overide_amount', e.target.value)}
               />
            </FormGroup>
        </div>

        <div className="col-md-12">
            <FormGroup>
                <Label for="overide_amount_cmt">Comment</Label>
                <Input
                    type="textarea"
                    name="overide_amount_cmt"
                    id="overide_amount_cmt"
                    value={(selectedOvrAmtCmt != '') ? selectedOvrAmtCmt : ''}
                    onChange={(e) => onUpdateAmountDetail('overide_amount_cmt', e.target.value)}
                    />
            </FormGroup>
        </div>

    </div>
    </Form>
);

export default UpdateAmountForm;
