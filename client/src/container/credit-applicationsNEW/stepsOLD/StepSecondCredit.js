/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepSecondCredit = ({ addErr, addData, onChnagerovider, handleShareholderNameChange, handleRemoveShareholder, handleAddShareholder, shareholders, sameBilling, countriesList, stateType, regionType, blillingStateType, blillingRegionType }) => (
   
      <div className="modal-body page-form-outer text-left">
         <Form>
            {addData.location.map((shareholder, idx) => (
               <div key={idx} aa={idx}>
                  {(() => {
                     if (idx != 0) {
                        return (
                           <React.Fragment>
                              <div className="row">
                                 <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                                 <div className="col-md-2">
                                    <a href="#" onClick={(e) => handleRemoveShareholder(idx)}>Remove Address (-)</a>
                                 </div>
                              </div>
                           </React.Fragment>
                        )
                     }

                  })()}
                  <div className="row">


                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="address1">Address1<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="address1"
                              id="address1"
                              fullWidth
                              variant="outlined"
                              placeholder="Address1"
                              value={(addData.location[idx].address1 != '') ? addData.location[idx].address1 : ''}
                              error={(addErr.location[idx].address1) ? true : false}
                              helperText={(addErr.location[idx].address1 != '') ? addErr.location[idx].address1 : ''}
                              onChange={(e) => onChnagerovider('address1', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>

                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="address2">Address2</Label><br />
                           <TextField
                              type="text"
                              name="address2"
                              id="address2"
                              fullWidth
                              variant="outlined"
                              placeholder="Address2"
                              value={(addData.location[idx].address2 != '') ? addData.location[idx].address2 : ''}
                              onChange={(e) => onChnagerovider('address2', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="country">Country<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="country"
                              id="country"
                              value={addData.location[idx].country}
                              onChange={(e) => onChnagerovider('country', e.target.value, idx)}
                           >
                              <option value="">Select</option>
                              {countriesList && countriesList.map((country, key) => (
                                 <option value={country.id} key={key}>{country.name}</option>
                              ))}

                           </Input>
                           {(addErr.location[idx].country != '' && addErr.location[idx].country !== undefined) ? <FormHelperText>{addErr.location[idx].country}</FormHelperText> : ''}
                        </FormGroup>
                     </div>

                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="state">State<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="state"
                              id="state"
                              placeholder=""
                              value={addData.location[idx].state}
                              onChange={(e) => onChnagerovider('state', e.target.value, idx)}
                           >
                              <option value="">Select</option>
                              {stateType[idx] && stateType[idx].map((state, key) => (
                                 <option value={state.state_id} key={key}>{state.name}</option>
                              ))}

                           </Input>
                           {(addErr.location[idx].state != '' && addErr.location[idx].state !== undefined) ? <FormHelperText>{addErr.location[idx].state}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="region">Region<span className="required-field">*</span></Label>
                           <Input
                              type="select"
                              name="region"
                              id="region"
                              placeholder=""
                              value={addData.location[idx].region}
                              onChange={(e) => onChnagerovider('region', e.target.value, idx)}
                           >
                              <option value="">Select</option>
                              {regionType[idx] && regionType[idx].map((region, key) => (
                                 <option value={region.region_id} key={key}>{region.name}</option>
                              ))}

                           </Input>
                           {(addErr.location[idx].region != '' && addErr.location[idx].region !== undefined) ? <FormHelperText>{addErr.location[idx].region}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="county">County</Label>
                           <TextField
                              type="text"
                              name="county"
                              id="county"
                              fullWidth
                              variant="outlined"
                              placeholder="county"
                              value={(addData.location[idx].county != '') ? addData.location[idx].county : ''}
                              error={(addErr.location[idx].county) ? true : false}
                              helperText={(addErr.location[idx].county != '') ? addErr.location[idx].county : ''}
                              onChange={(e) => onChnagerovider('county', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="city">City<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="city"
                              id="city"
                              fullWidth
                              variant="outlined"
                              placeholder="City"
                              value={(addData.location[idx].city != '') ? addData.location[idx].city : ''}
                              error={(addErr.location[idx].city) ? true : false}
                              helperText={(addErr.location[idx].city != '') ? addErr.location[idx].city : ''}
                              onChange={(e) => onChnagerovider('city', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>



                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="zip_code">Zip Code<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="zip_code"
                              id="zip_code"
                              fullWidth
                              variant="outlined"
                              placeholder="Zip Code"
                              value={(addData.location[idx].zip_code != '') ? addData.location[idx].zip_code : ''}
                              error={(addErr.location[idx].zip_code) ? true : false}
                              helperText={(addErr.location[idx].zip_code != '') ? addErr.location[idx].zip_code : ''}
                              onChange={(e) => onChnagerovider('zip_code', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>






                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="how_long">How long at this address?</Label><br />
                           <TextField
                              type="text"
                              name="how_long"
                              id="how_long"
                              fullWidth
                              variant="outlined"
                              placeholder="How long at this address?"
                              value={(addData.location[idx].how_long != '') ? addData.location[idx].how_long : ''}
                              onChange={(e) => onChnagerovider('how_long', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>

                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="phone_no">Phone No<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="phone_no"
                              id="phone_no"
                              fullWidth
                              variant="outlined"
                              inputProps={{ maxLength: 14 }}
                              placeholder="Phone No"
                              value={(addData.location[idx].phone_no != '') ? addData.location[idx].phone_no : ''}
                              error={(addErr.location[idx].phone_no) ? true : false}
                              helperText={(addErr.location[idx].phone_no != '') ? addErr.location[idx].phone_no : ''}
                              onChange={(e) => onChnagerovider('phone_no', e.target.value, idx)}
                           />
                        </FormGroup>
                     </div>

                     <div className="col-md-12">
                        <FormGroup tag="fieldset">
                           <FormGroup check>
                              <Label check>
                                 <Input
                                    type="checkbox"
                                    name="primary_address"
                                    value={1}
                                    checked={(addData.location[idx].primary_address == 1) ? true : false}
                                    onChange={(e) => onChnagerovider('primary_address', e.target.value, idx)}
                                 />
                                 Set as primary address
              </Label>
                           </FormGroup>
                           <FormGroup check>
                              <Label check>
                                 <Input
                                    type="checkbox"
                                    name="billing_address"
                                    value={1}
                                    checked={(addData.location[idx].billing_address == 1) ? true : false}
                                    onChange={(e) => onChnagerovider('billing_address', e.target.value, idx)}
                                 />
                                 Billing and physical address same
              </Label>
                           </FormGroup>
                        </FormGroup>
                     </div>
                  </div>
                  <br />
                  {(() => {
                     if (idx == 0) {
                        return (
                           <React.Fragment>

                              <h4 className={(sameBilling) ? "blue-small-title d-none" : "blue-small-title"}>Billing Address</h4>

                              <div className={(sameBilling) ? "row d-none" : "row"}>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_address1">Address1<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_address1"
                                          id="billing_address1"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Address1"
                                          value={(addData.billing_address1 != '') ? addData.billing_address1 : ''}
                                          error={(addErr.billing_address1) ? true : false}
                                          helperText={(addErr.billing_address1 != '') ? addErr.billing_address1 : ''}
                                          onChange={(e) => onChnagerovider('billing_address1', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>

                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_address2">Address2</Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_address2"
                                          id="billing_address2"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Address2"
                                          value={(addData.billing_address2 != '') ? addData.billing_address2 : ''}
                                          onChange={(e) => onChnagerovider('billing_address2', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_country">Country<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="billing_country"
                                          id="billing_country"
                                          value={addData.billing_country}
                                          onChange={(e) => onChnagerovider('billing_country', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {countriesList && countriesList.map((country, key) => (
                                             <option value={country.id} key={key}>{country.name}</option>
                                          ))}

                                       </Input>
                                       {(addErr.billing_country != '' && addErr.billing_country !== undefined) ? <FormHelperText>{addErr.billing_country}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>

                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_state">State<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="billing_state"
                                          id="billing_state"
                                          placeholder=""
                                          value={addData.billing_state}
                                          onChange={(e) => onChnagerovider('billing_state', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {blillingStateType && blillingStateType.map((state, key) => (
                                             <option value={state.state_id} key={key}>{state.name}</option>
                                          ))}

                                       </Input>
                                       {(addErr.billing_state != '' && addErr.billing_state !== undefined) ? <FormHelperText>{addErr.billing_state}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_region">Region<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="billing_region"
                                          id="billing_region"
                                          placeholder=""
                                          value={addData.billing_region}
                                          onChange={(e) => onChnagerovider('billing_region', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {blillingRegionType && blillingRegionType.map((region, key) => (
                                             <option value={region.region_id} key={key}>{region.name}</option>
                                          ))}

                                       </Input>
                                       {(addErr.billing_region != '' && addErr.billing_region !== undefined) ? <FormHelperText>{addErr.billing_region}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_county">County</Label>
                                       <TextField
                                          type="text"
                                          name="billing_county"
                                          id="billing_county"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="county"
                                          value={(addData.billing_county != '') ? addData.billing_county : ''}
                                          error={(addErr.billing_county) ? true : false}
                                          helperText={(addErr.billing_county != '') ? addErr.billing_county : ''}
                                          onChange={(e) => onChnagerovider('billing_county', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_city">City<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_city"
                                          id="billing_city"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="City"
                                          value={(addData.billing_city != '') ? addData.billing_city : ''}
                                          error={(addErr.billing_city) ? true : false}
                                          helperText={(addErr.billing_city != '') ? addErr.billing_city : ''}
                                          onChange={(e) => onChnagerovider('billing_city', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>



                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_zip_code">Zip Code<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_zip_code"
                                          id="billing_zip_code"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Zip Code"
                                          value={(addData.billing_zip_code != '') ? addData.billing_zip_code : ''}
                                          error={(addErr.billing_zip_code) ? true : false}
                                          helperText={(addErr.billing_zip_code != '') ? addErr.billing_zip_code : ''}
                                          onChange={(e) => onChnagerovider('billing_zip_code', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>






                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_how_long">How long at this address?</Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_how_long"
                                          id="billing_how_long"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="How long at this address?"
                                          value={(addData.billing_how_long != '') ? addData.billing_how_long : ''}
                                          onChange={(e) => onChnagerovider('billing_how_long', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>

                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="billing_phone_no">Phone No<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="billing_phone_no"
                                          id="billing_phone_no"
                                          fullWidth
                                          variant="outlined"
                                          inputProps={{ maxLength: 14 }}
                                          placeholder="Phone No"
                                          value={(addData.billing_phone_no != '') ? addData.billing_phone_no : ''}
                                          error={(addErr.billing_phone_no) ? true : false}
                                          helperText={(addErr.billing_phone_no != '') ? addErr.billing_phone_no : ''}
                                          onChange={(e) => onChnagerovider('billing_phone_no', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                              </div>
                           </React.Fragment>
                        )
                     }

                  })()}
               </div>
            ))}


         </Form>
         <div className="col-md-12">
            <a href="#" onClick={(e) => handleAddShareholder()}>Add more Address (+)</a>
         </div>
      </div>
   
);

export default StepSecondCredit;