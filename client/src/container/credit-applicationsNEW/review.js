/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { Form, FormGroup, Label, Input } from 'reactstrap';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isEmpty } from '../../validator/Validator';

import {
   creditApplicationReview, creditApplicationManualActions, creditApplicationRemoveManualActions
} from 'Actions';

class ReviewApplication extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         comment: '',
         approved_amt: '',
         credit_score: ''
      },
      add_err: {},
      uploadDocumentRedirect: 0,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.creditApplicationReview(this.props.match.params.id);
   }
   /*
   * Title :- onChnage
   * Descrpation :- This function use check on change value
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'approved_amt':
            if (isEmpty(value)) {
               add_err[key] = "Approve Amount can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'credit_score':
            if (isEmpty(value)) {
               add_err[key] = "Credit Score can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   /*
   * Title :- validateSubmit
   * Descrpation :- This function use for check validation
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   validateSubmit() {
      return (this.state.add_err.comment === '' && this.state.add_err.approved_amt === '' && this.state.add_err.credit_score === '');
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for submit appication status
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
     // return false;
      this.props.creditApplicationManualActions(this.props.match.params.id, type, this.state.addData);

   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check updated props
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentWillReceiveProps(nextProps) {
      let { addData,add_err } = this.state
      if (nextProps.manualType && nextProps.manualType != '') {
         this.props.creditApplicationRemoveManualActions();
         this.setState({ uploadDocumentRedirect: nextProps.manualType })
      }
      //this.props.reviewAppDetails[0].approve_amount
      //this.props.reviewAppDetails[0].score
      if(nextProps.reviewAppDetails && nextProps.reviewAppDetails !== '') {
         addData.approved_amt = nextProps.reviewAppDetails[0].approve_amount
         addData.credit_score = nextProps.reviewAppDetails[0].score
         add_err.approved_amt='';
         add_err.credit_score='';
         this.setState({
            addData: addData,add_err: add_err
         })
      }

   }
   goBack() {
      this.props.history.goBack(-1)
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      
      if (this.state.uploadDocumentRedirect == 1) {
         //return (<Redirect to={`/admin/credit-applications`} />);
         return (<Redirect to={`/admin/credit-application/credit-authorized/${this.enc(this.props.match.params.id.toString())}`} />);
      } else if (this.state.uploadDocumentRedirect == 2) {
         return (<Redirect to={`/admin/credit-application/reject/${this.props.match.params.id}`} />);
      } else if (this.state.uploadDocumentRedirect == 3) {
         if (this.props.reviewAppDetails[0].provider_id == null) {
            //return (<Redirect to={`/admin/credit-application/upload/${this.props.reviewAppDetails[0].patient_id}/${this.props.match.params.id}`} />);
            return (<Redirect to={`/admin/credit-applications`} />);
         } else {
            return (<Redirect to={`/admin/credit-applications`} />);
         }
      }


      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsReviewCreditApplications" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">

                     {this.props.reviewAppDetails &&
                        <Form className="application-review-process">

                           <div className="table-responsive mb-40 pymt-history review-app-info">
                           <h3 className="app-review-info">Credit Application Information</h3>
                           <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Application No : <span>{this.props.reviewAppDetails[0].application_no}</span></th>
                                       <th>
                                          <span className="label-heading d-inline-block align-middle">Approved Amount :</span>
                                             <span className="label-input d-inline-block align-middle ml-2">
                                                <FormGroup className="m-0">
                                                   <Input
                                                      type="text"
                                                      name="approved_amt"
                                                      id="approved_amt"
                                                      variant="outlined"
                                                      defaultValue={this.state.addData.approved_amt}
                                                      onChange={(e) => this.onChnage('approved_amt', e.target.value)}
                                                   >
                                                   </Input>
                                                   {(this.state.add_err.approved_amt) ? <FormHelperText>{this.state.add_err.approved_amt}</FormHelperText> : ''}
                                                </FormGroup>

                                             </span>
                                       </th>
                                       <th>
                                          <span className="label-heading d-inline-block align-middle">Credit Score :</span>
                                             <span className="label-input d-inline-block align-middle ml-2">
                                                <FormGroup className="m-0">
                                                   <Input
                                                      type="text"
                                                      name="credit_score"
                                                      id="credit_score"
                                                      variant="outlined"
                                                      defaultValue={this.state.addData.credit_score}
                                                      onChange={(e) => this.onChnage('credit_score', e.target.value)}
                                                   >
                                                   </Input>
                                                   {(this.state.add_err.credit_score) ? <FormHelperText>{this.state.add_err.credit_score}</FormHelperText> : ''}
                                                </FormGroup>
                                             </span>
                                       </th>
                                    </tr>
                                 </thead>
                              </table>
                           </div>

                           
                           <div className="table-responsive mb-40 experian-risk-modal">
                              <h3 className="text-left mb-10">Experian Risk Model</h3>
                              <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Model Indicator</th>
                                       <th>Score</th>
                                       <th>Evaluation</th>
                                       <th>Importance</th>
                                       <th>Code</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    {this.props.reviewApiRiskFactor && this.props.reviewApiRiskFactor.map((risk, idx) => (
                                    <tr key={idx}>
                                       <td>
                                          {risk.modelIndicator}
                                       </td>
                                       <td>
                                          {risk.score}
                                       </td>
                                       <td>
                                          {risk.evaluation}
                                       </td>
                                       <td>
                                          {risk.importance}
                                       </td>
                                       <td>
                                          {risk.code}
                                       </td>
                                    </tr>
                                 ))
                                 }
                                 {this.props.reviewApiRiskFactor == '' &&
                                 <tr>
                                    <td colSpan="5">No risk factor retrieved</td>
                                 </tr>
                                 }
                                   
                                 </tbody>
                              </table>
                           </div>
                           
                           <div className="table-responsive mb-40 hps-risk-modal">
                              <h3 className="text-left mb-10">HPS Risk Model</h3>
                              <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Risk Factors</th>
                                       <th>Weight</th>
                                       <th>Weight Type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                 {this.props.reviewHPSRiskFactor && this.props.reviewHPSRiskFactor.map((risk, idx) => (
                                    <tr key={idx}>
                                       <td>
                                          {risk.value}
                                       </td>
                                       <td>
                                          {risk.weight}
                                       </td>
                                       <td>
                                          {(risk.weight_type == 1) ? '+ (Positive)' : '- (Negative)'}
                                       </td>
                                    </tr>
                                 ))}
                                   
                                 </tbody>
                              </table>
                           </div>

                           
                           <div className="row">
                              <hr />
                              <div className="col-md-12">
                                 <FormGroup>
                                    <h3 className="text-left mb-10">Your Comment<span className="required-field">*</span></h3>
                                    <Input
                                       type="textarea"
                                       name="comment"
                                       id="comment"
                                       variant="outlined"
                                       placeholder="Enter your comment before any action"
                                       defaultValue={this.state.addData.comment}
                                       onChange={(e) => this.onChnage('comment', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.add_err.comment) ? <FormHelperText>{this.state.add_err.comment}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-12">
                                 <div className="mps-submt-btn text-right">
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.goBack.bind(this)}>Cancel</Button>
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 1)} disabled={!this.validateSubmit()}>Approve</Button>
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 2)} disabled={!this.validateSubmit()}>Reject</Button>
                                    {/*<Link to={"/admin/credit-application/reject/"}><Button variant="contained" color="primary" className="text-white mr-10 mb-10" >Reject</Button></Link>*/}
                                    <Button variant="contained" color="primary" className="text-white mb-10" onClick={this.callAction.bind(this, 3)} disabled={!this.validateSubmit()}>Upload Document</Button>
                                 </div>
                              </div>
                           </div>
                        </Form>
                     }

                  </div>


               </div>


               {this.props.loading &&
                  <RctSectionLoader />
               }


            </RctCollapsibleCard>

         </div>
      );
   }
}

const mapStateToProps = ({ creditApplication }) => {
   const { loading, reviewAppDetails, reviewApiRiskFactor, reviewHPSRiskFactor, manualType } = creditApplication;
   return { loading, reviewAppDetails, reviewApiRiskFactor, reviewHPSRiskFactor, manualType }

}

export default connect(mapStateToProps, {
   creditApplicationReview, creditApplicationManualActions, creditApplicationRemoveManualActions
})(ReviewApplication);