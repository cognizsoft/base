/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';

import { URL } from '../../apifile/URL';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';

import {
  applicationDoc,downloadOneDrive
} from 'Actions';
class AllDocumentsApplication extends Component {

  state = {
    all: false,
    users: null, // initial user data
    opnDocFileModal: false,
    imgPath:'',
    imgPlanNumber:'',
  }


  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    //this.permissionFilter(this.state.currentModule);
    this.props.applicationDoc(this.props.match.params.id);
  }

  opnDocFileModal(item_id, plan_number) {
      this.props.downloadOneDrive(item_id)
      this.setState({ opnDocFileModal: true, imgPlanNumber:plan_number})
   }
   
   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }


  render() {
    const { loading, allDocuments } = this.props;
    return (
      <div className="all-documents">
        <Helmet>
          <title>Health Partner | Admin | Credit Application | All Documents</title>
          <meta name="description" content="All Documents" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.allDocuments" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
            <div className="table-responsive">
              <div className="modal-body page-form-outer view-section">

               <div className="row">
                  {allDocuments && allDocuments.map((img, key) => (
                    <div className="col-sm-6 col-md-4 col-lg-4 col-xl-3" key={key}>
                      <figure className="img-wrapper border border-secondary" onClick={()=> this.opnDocFileModal(img.item_id, img.plan_number)}>
                        
                        <img src={`${img.file_path}`} width="250" height="400" />
                        {/*(() => {
                            var docType = img.file_path.split(".");
                            if (docType[docType.length - 1] == 'pdf' || docType[docType.length - 1] == 'png' || docType[docType.length - 1] == 'jpg' || docType[docType.length - 1] == 'jpeg') {
                              return (
                                  <embed src={`${URL.APIURL + '/' + img.file_path}`} width="500px" height="400px" />
                              )
                            } else {
                              return (
                                  <div>Preview not available <a href={`${URL.APIURL + '/' + img.file_path}`}>click here</a> to download </div>
                              )
                            }

                        })()*/}
                        
                        <figcaption>
                          <h4>View and Download</h4>
                        </figcaption>
                        <a href="javascript:void(0);">&nbsp;</a>
                      </figure>
                    </div>
                  ))}
                </div> 

              </div>
            </div>

            <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>
                  
               <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
                  <span className="float-left>">{(this.state.imgPlanNumber)?'Plan Number: '+this.state.imgPlanNumber:''}</span>
                  <span className="float-right"><a href={this.props.oneDrivePreview} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span> 
               </ModalHeader>

               <ModalBody>
                  {this.props.oneDrivePreview &&
                     <embed src={this.props.oneDrivePreview} width="100%" height="500" download/>
                
                  }
                  {this.props.loading &&
                     <div className="p-50 text-center">Loading, Please Wait...</div>
                  }

               </ModalBody>
               
            </Modal>

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication }) => {
  const { loading, allDocuments,oneDrivePreview } = creditApplication;
  return { loading, allDocuments,oneDrivePreview }
}
export default connect(mapStateToProps, {
  applicationDoc,downloadOneDrive
})(AllDocumentsApplication);