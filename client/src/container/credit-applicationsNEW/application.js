/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge
} from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import UnlockConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// update user form
import UpdateUserForm from './expire-date/UpdateUserForm';
import UpdateAmountForm from './overide-amount/UpdateAmountForm';
import { NotificationManager } from 'react-notifications';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
  viewApplication, updateAppExpireDate, updateOverrideAmount, unlockApplication
} from 'Actions';
class ApplicationView extends Component {

  state = {
    all: false,
    users: null, // initial user data
    addNewUserModal: false, // add new user form modal
    addNewAmountModal: false, // add new user form modal
    startDate: '',
    editUser: null,

    err: {
      expiry_date: '',
      expiry_date_cmt: ''
    },
    exp_date: {
      application_id: '',
      expiry_date: '',
      expiry_date_cmt: ''
    },

    amt_err: {
      overide_amount: '',
      overide_amount_cmt: ''
    },
    ovr_amt: {
      application_id: '',
      overide_amount: '',
      overide_amount_cmt: ''
    },
    ovr_amt_selected: '',
    ovr_amt_selected_cmt: ''

  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let { updateForm } = this.state;
    updateForm[name] = value;

    this.setState({
      updateForm: updateForm
    }, function () {
      this.onUpdateUserDetails(name, value)
      this.onUpdateAmountDetails(name, value)
    });
  }
  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    console.log('this.props.match.params.id');
    this.props.viewApplication(this.props.match.params.id);

  }

  onEditUser(expiry_date, expiry_date_cmt) {
    //console.log(editRfactor)
    let { exp_date } = this.state;
    exp_date.expiry_date = expiry_date;
    exp_date.expiry_date_cmt = expiry_date_cmt;
    exp_date.application_id = this.props.match.params.id
    this.setState({ addNewUserModal: true, exp_date: exp_date, startDate: new Date(expiry_date) });

  }

  onAddUpdateUserModalClose() {
    let { exp_date, startDate } = this.state;
    //exp_date.expiry_date = moment(startDate).format('MM/DD/YYYY');
    this.setState({ addNewUserModal: false })
  }

  onUpdateUserDetails(fieldName, value) {

    let { err } = this.state;
    switch (fieldName) {

      case 'expiry_date':
        if (value == null) {
          err[fieldName] = "Select Expiry Date";
          this.setState({ startDate: '' })
        } else {
          this.setState({ startDate: value })
          value = moment(value).format('YYYY-MM-DD');
          err[fieldName] = '';
        }
        break;
      case 'expiry_date_cmt':
        if (value == '') {
          err[fieldName] = '';
        }
        break;
      default:
        break;

    }

    this.setState({ err: err });
    //console.log(this.state.err)

    this.setState({
      exp_date: {
        ...this.state.exp_date,
        [fieldName]: value
      }
    });
  }

  updateUser() {
    const { exp_date } = this.state;


    exp_date.expiry_date = moment(exp_date.expiry_date).format('YYYY-MM-DD');
    // console.log('exp_date======')
    //console.log(exp_date)
    //return false;
    this.props.updateAppExpireDate(exp_date);
    //this.onEditUser(exp_date.expiry_date)
    let self = this;
    setTimeout(() => {
      this.setState({ loading: true, addNewUserModal: false });
      NotificationManager.success('Date Updated!');
      self.setState({ loading: false });
    }, 2000);
  }

  /* componentWillReceiveProps(nextProps) {
       let { exp_date } = this.state;
 
       if(nextProps.appDetails) {
           exp_date.expiry_date = nextProps.appDetails.expiry_date;
       }
 
       this.setState({exp_date: exp_date})
 
   }*/

  onEditAmount(ov_amt, ov_amt_cmt) {
    let { ovr_amt } = this.state;
    if (ov_amt_cmt == null) {
      ov_amt_cmt = ''
    }
    ovr_amt.application_id = this.props.match.params.id

    this.setState({ addNewAmountModal: true, ovr_amt: ovr_amt, ovr_amt_selected: ov_amt, ovr_amt_selected_cmt: ov_amt_cmt });
  }

  onAddUpdateAmountModalClose() {
    this.setState({ addNewAmountModal: false })
  }

  onUpdateAmountDetails(fieldName, value) {

    let { amt_err } = this.state;
    switch (fieldName) {
      case 'overide_amount':
        if (value == '') {
          amt_err[fieldName] = "Amount can't be blank";
          this.setState({ ovr_amt_selected: '' })
        } else {
          this.setState({ ovr_amt_selected: value })
          amt_err[fieldName] = '';
        }
        break;
      case 'overide_amount_cmt':
        if (value == '') {
          this.setState({ ovr_amt_selected_cmt: '' })
        } else {
          this.setState({ ovr_amt_selected_cmt: value })
        }
        break;
      default:
        break;

    }

    this.setState({ amt_err: amt_err });
    //console.log(this.state.err)

    this.setState({
      ovr_amt: {
        ...this.state.ovr_amt,
        [fieldName]: value
      }
    });

  }

  updateAmount() {
    const { ovr_amt } = this.state;

    console.log(ovr_amt)
    //return false;

    this.props.updateOverrideAmount(ovr_amt);
    //this.onEditUser(exp_date.expiry_date)
    let self = this;
    setTimeout(() => {
      this.setState({ loading: true, addNewAmountModal: false });
      NotificationManager.success('Amount Updated!');
      self.setState({ loading: false });
    }, 2000);
  }

  validateOverideAmount() {
    return (
      this.state.amt_err.overide_amount === ''
    )
  }

  onUnlockApplication() {
    this.refs.unlockConfirmationDialog.open();
  }

  unlockApplication() {

    this.props.unlockApplication(this.props.match.params.id);
    this.refs.unlockConfirmationDialog.close();
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  render() {
    const { loading, appDetails, appDetailsAddress } = this.props;
    const { err, amt_err } = this.state;
    return (
      <div className="country regions">
        <Helmet>
          <title>Health Partner | Providers | Add New</title>
          <meta name="description" content="Regions" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.viewApplication" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          {appDetails &&
            <div className="table-responsive">
              <div className="ovride-action-btn mt-5 text-right mr-15">
                {(appDetails.application_status == 1 || appDetails.application_status == 6) &&
                  <div className="d-inline">
                    <span className="overide-btn oe-btn" onClick={() => this.onEditAmount(appDetails.override_amount, appDetails.override_amount_cmt)}><i className="zmdi zmdi-money"></i> Override Amount</span>

                    <span className=" mr-10  ml-10"> / </span>

                    <span className="expire-btn oe-btn" onClick={() => this.onEditUser(appDetails.expiry_date, appDetails.expiry_date_cmt)}><i className="zmdi zmdi-calendar-alt"></i> Expiry Date</span>
                  </div>
                }
                {appDetails.application_status == 6 &&
                  <div className="d-inline">
                    <span className=" mr-10  ml-10"> / </span>

                    <span className="expire-btn oe-btn" onClick={() => this.onUnlockApplication()}><i className="zmdi zmdi-lock-open"></i> Unlock Application</span>
                  </div>
                }
                <div className="d-inline">
                  <span className=" mr-10  ml-10"> / </span>

                  <span className="expire-btn oe-btn" onClick={this.goBack.bind(this)} title="Back"><i className="zmdi zmdi-arrow-left"></i></span>
                </div>
              </div>
              <div className="modal-body page-form-outer view-section pt-5">

                <div className="view-section-inner">
                  <h4>Customer Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">First Name:</td>
                            <td>{appDetails.f_name}</td>
                            <td className="fw-bold">Phone No. 2:</td>
                            <td>{(appDetails.alternative_phone) ? appDetails.alternative_phone : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Middle Name:</td>
                            <td>{(appDetails.m_name != '') ? appDetails.m_name : '-'}</td>
                            <td className="fw-bold">Email Address:</td>
                            <td>{appDetails.email}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Last Name:</td>
                            <td>{appDetails.l_name}</td>
                            <td className="fw-bold">SSN / Tax ID:</td>
                            <td>{(appDetails.ssn !== undefined) ? appDetails.ssn.replace(/.(?=.{4})/g, 'x') : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Date of Birth:</td>
                            <td>{appDetails.dob}</td>
                            <td className="fw-bold">Gender:</td>
                            <td>{(appDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Phone No. 1:</td>
                            <td>{appDetails.peimary_phone}</td>
                            <td className="fw-bold">Customer A/C:</td>
                            <td>{appDetails.patient_ac}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Status:</td>
                            <td>{(appDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                            <td className="fw-bold">Application No</td>
                            <td>{appDetails.application_no}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="view-section-inner">
                  <h4>Application Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Application No:</td>
                            <td>{appDetails.application_no}</td>
                            <td className="fw-bold">Credit Score:</td>
                            <td>{(appDetails.score) ? appDetails.score : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Authorization date:</td>
                            <td>{(appDetails.application_status == 1) ? appDetails.authorization_date : '-'}</td>
                            <td className="fw-bold">Authorization expiration:</td>
                            <td>{(appDetails.expiry_date) ? appDetails.expiry_date : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Amount Approved:</td>
                            <td>{(appDetails.application_status == 1) ? '$' + appDetails.approve_amount.toFixed(2) : '-'}</td>
                            <td className="fw-bold">Expiration Comment:</td>
                            <td>{(appDetails.expiry_date_cmt) ? appDetails.expiry_date_cmt : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Override Amount:</td>
                            <td>{(appDetails.override_amount !== '') ? '$' + parseFloat(appDetails.override_amount).toFixed(2) : '$0.00'}</td>
                            <td className="fw-bold">Remaining Amount:</td>
                            <td>{(appDetails.application_status == 1) ? '$' + (parseFloat(appDetails.remaining_amount) + parseFloat(appDetails.override_amount)).toFixed(2) : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Override Amount Comment:</td>
                            <td>{(appDetails.override_amount_cmt !== '') ? appDetails.override_amount_cmt : '-'}</td>
                            <td className="fw-bold"></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="view-section-inner">
                  <h4>Address</h4>

                  {appDetailsAddress && appDetailsAddress.map((address, idx) => (
                    <div key={idx} className="mb-10">
                      <div className="view-box" >
                        <div className="width-100">
                          <table>
                            <tbody>
                              <tr>
                                <td className="fw-bold">Address1:</td>
                                <td>{address.address1}</td>
                                <td className="fw-bold">City:</td>
                                <td>{address.City}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Address2:</td>
                                <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                <td className="fw-bold">Zip Code:</td>
                                <td>{address.zip_code}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Country:</td>
                                <td>{address.country_name}</td>
                                <td className="fw-bold">How long at this address?:</td>
                                <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">State:</td>
                                <td>{address.state_name}</td>
                                <td className="fw-bold">Phone No:</td>
                                <td>{address.phone_no}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Primary Address:</td>
                                <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                <td className="fw-bold">Billing and Physical address same:</td>
                                <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                              </tr>
                              {/*<tr>
                              <td className="fw-bold">County:</td>
                              <td>{(address.county != '') ? address.county : '-'}</td>
                              <td className="fw-bold">Region:</td>
                              <td>{address.region_name}</td>
                              
                            </tr>*/}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  ))
                  }


                </div>

                <div className="view-section-inner">
                  <h4>Employment Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Employed:</td>
                            <td>{(appDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                            <td className="fw-bold">Employer Name:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_name : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Employment Type:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.emp_type_name : '-'}</td>
                            <td className="fw-bold">Employer Phone No:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_phone : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Annual Income($):</td>
                            <td>{(appDetails.employment_status == 1) ? '$' + appDetails.annual_income : '-'}</td>
                            <td className="fw-bold">Employer Email Address:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_email : '-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Employed Since:</td>
                            <td>{(appDetails.employment_status == 1) ? appDetails.employer_since : '-'}</td>
                            <td className="fw-bold">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="view-section-inner">
                  <h4>Bank Details</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Bank Name:</td>
                            <td>{(appDetails.bank_name)?appDetails.bank_name:'-'}</td>
                            <td className="fw-bold">Name on Account:</td>
                            <td>{(appDetails.account_name)?appDetails.account_name:'-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Routing Number:</td>
                            <td>{(appDetails.rounting_no>0)?appDetails.rounting_no:'-'}</td>
                            <td className="fw-bold">Bank A/C Type:</td>
                            <td>{(appDetails.account_type)?appDetails.account_type:'-'}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Bank A/C#:</td>
                            <td>{(appDetails.account_number)?appDetails.account_number:'-'}</td>
                            <td className="fw-bold">Bank Address</td>
                            <td>{(appDetails.bank_address != '') ? appDetails.bank_address : '-'}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div className="view-section-inner">
                  <h4>User Information</h4>
                  <div className="view-box">
                    <div className="width-100">
                      <table>
                        <tbody>
                          <tr>
                            <td className="fw-bold">Username:</td>
                            <td>{appDetails.username}</td>
                            <td className="fw-bold">Phone 1:</td>
                            <td>{appDetails.user_phone1}</td>
                          </tr>
                          <tr>
                            <td className="fw-bold">Email Address:</td>
                            <td>{appDetails.user_eamil}</td>
                            <td className="fw-bold">Phone 2:</td>
                            <td>{(appDetails.user_phone2 != '') ? appDetails.user_phone2 : '-'}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>




              </div>
            </div>
          }

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

        <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
          <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>

            Change Expire Date

               </ModalHeader>
          <ModalBody>

            <UpdateUserForm
              updateErr={err}
              updateUserDetails={(this.state.exp_date.expiry_date) ? this.state.exp_date.expiry_date : ''}
              onUpdateUserDetail={this.onUpdateUserDetails.bind(this)}
              DatePicker={DatePicker}
              startDate={this.state.startDate}
              selectedExpDatCmt={this.state.exp_date.expiry_date_cmt}
            />

          </ModalBody>
          <ModalFooter>

            <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateUser()}>Update</Button>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>

          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.addNewAmountModal} toggle={() => this.onAddUpdateAmountModalClose()}>
          <ModalHeader toggle={() => this.onAddUpdateAmountModalClose()}>

            Override Amount

               </ModalHeader>
          <ModalBody>

            <UpdateAmountForm
              updateAmtErr={amt_err}
              updateAmountDetails={(this.state.overide_amount) ? this.state.overide_amount : ''}
              onUpdateAmountDetail={this.onUpdateAmountDetails.bind(this)}
              selectedOvrAmt={this.state.ovr_amt_selected}
              selectedOvrAmtCmt={this.state.ovr_amt_selected_cmt}
            />

          </ModalBody>
          <ModalFooter>

            <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateAmount()} disabled={!this.validateOverideAmount()}>Update</Button>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateAmountModalClose()}>Cancel</Button>

          </ModalFooter>
        </Modal>

        <UnlockConfirmationDialog
          ref="unlockConfirmationDialog"
          title="Are You Sure Want To Unlock?"
          message="This will unlock user's Application."
          onConfirm={() => this.unlockApplication()}
        />

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication }) => {
  const { loading, appDetails, appDetailsAddress } = creditApplication;
  return { loading, appDetails, appDetailsAddress }
}
export default connect(mapStateToProps, {
  viewApplication, updateAppExpireDate, updateOverrideAmount, unlockApplication
})(ApplicationView);