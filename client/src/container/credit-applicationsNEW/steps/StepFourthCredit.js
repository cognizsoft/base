/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepFourthCredit = ({ addErr, addData, onChnagerovider, bankType }) => (
   
      <div className="modal-body page-form-outer text-left">
         <Form>
            <div className="row">
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="bank_name">Bank Name</Label><br />
                     <TextField
                        type="text"
                        name="bank_name"
                        id="bank_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Bank Name"
                        value={(addData.bank_name != '') ? addData.bank_name : ''}
                        error={(addErr.bank_name) ? true : false}
                        helperText={(addErr.bank_name != '') ? addErr.bank_name : ''}
                        onChange={(e) => onChnagerovider('bank_name', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="bank_address">Bank Address</Label><br />
                     <TextField
                        type="text"
                        name="bank_address"
                        id="bank_address"
                        fullWidth
                        variant="outlined"
                        placeholder="Bank Address"
                        value={(addData.bank_address != '') ? addData.bank_address : ''}
                        error={(addErr.bank_address) ? true : false}
                        helperText={(addErr.bank_address != '') ? addErr.bank_address : ''}
                        onChange={(e) => onChnagerovider('bank_address', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="rounting_no">Routing Number</Label><br />
                     <TextField
                        type="text"
                        name="rounting_no"
                        id="rounting_no"
                        fullWidth
                        variant="outlined"
                        placeholder="Routing Number"
                        value={(addData.rounting_no != '') ? addData.rounting_no : ''}
                        error={(addErr.rounting_no) ? true : false}
                        helperText={(addErr.rounting_no != '') ? addErr.rounting_no : ''}
                        onChange={(e) => onChnagerovider('rounting_no', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="bank_ac">Bank A/C#</Label><br />
                     <TextField
                        type="text"
                        name="bank_ac"
                        id="bank_ac"
                        fullWidth
                        variant="outlined"
                        inputProps={{ maxLength: 17 }}
                        placeholder="Bank A/C#"
                        value={(addData.bank_ac != '') ? addData.bank_ac : ''}
                        error={(addErr.bank_ac) ? true : false}
                        helperText={(addErr.bank_ac != '') ? addErr.bank_ac : ''}
                        onChange={(e) => onChnagerovider('bank_ac', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="account_name">Name on Account</Label><br />
                     <TextField
                        type="text"
                        name="account_name"
                        id="account_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Name on Account"
                        value={(addData.account_name != '') ? addData.account_name : ''}
                        error={(addErr.account_name) ? true : false}
                        helperText={(addErr.account_name != '') ? addErr.account_name : ''}
                        onChange={(e) => onChnagerovider('account_name', e.target.value)}
                     />
                  </FormGroup>
               </div>


               <div className="col-md-4">
                  <FormGroup>
                     <Label for="account_type">Bank A/C Type</Label>
                     <Input
                        type="select"
                        name="account_type"
                        id="account_type"
                        placeholder=""
                        value={addData.account_type}
                        onChange={(e) => onChnagerovider('account_type', e.target.value)}
                     >
                        <option value="">Select</option>
                        {bankType && bankType.map((bank, key) => (
                            <option value={bank.mdv_id} key={key}>{bank.value}</option>
                        ))}
                        
                     </Input>
                     {(addErr.account_type != '' && addErr.account_type !== undefined) ? <FormHelperText>{addErr.account_type}</FormHelperText> : ''}
                  </FormGroup>
               </div>





            </div>
         </Form>
      </div>
   
);

export default StepFourthCredit;