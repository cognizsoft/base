/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddInvoiceMsgForm = ({ addErr, addInvoiceMsgDetails, onChangeaddInvoiceMsgDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
            <FormGroup>
                <Label for="invoice_msg">Message</Label>
                <Input
                    type="textarea" 
                    name="invoice_msg" 
                    id="invoice_msg" 
                    placeholder="Enter Message"
                    value={addInvoiceMsgDetails}
                    onChange={(e) => onChangeaddInvoiceMsgDetail('invoice_msg', e.target.value)}
                    />
                {(addErr.invoice_msg) ? <FormHelperText>{addErr.invoice_msg}</FormHelperText> : ''}
            </FormGroup>
        </div>
        
    </div>
    </Form>
);

export default AddInvoiceMsgForm;
