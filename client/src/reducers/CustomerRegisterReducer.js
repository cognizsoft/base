import {
    CUSTOMER_REGISTER_REQUEST,
    CUSTOMER_REGISTER_SUCCESS,
    CUSTOMER_REGISTER_FAILURE,

    CUSTOMER_DETAIL_REQUEST,
    CUSTOMER_DETAIL_SUCCESS,
    CUSTOMER_DETAIL_FAILURE,

    CUSTOMER_DETAIL_UPDATE_REQUEST,
    CUSTOMER_DETAIL_UPDATE_SUCCESS,
    CUSTOMER_DETAIL_UPDATE_FAILURE,

    DIRECT_PROVIDER_INSERT,
    DIRECT_PROVIDER_INSERT_SUCCESS,
    DIRECT_PROVIDER_INSERT_FAILURE,

    DIRECT_MASTER_DATA_VALUE_LIST,
    DIRECT_MASTER_DATA_VALUE_LIST_SUCCESS,
    DIRECT_MASTER_DATA_VALUE_LIST_FAILURE,

    DIRECT_STATES_LIST,
    DIRECT_STATES_LIST_SUCCESS,
    DIRECT_STATES_LIST_FAILURE,

    DIRECT_PROVIDER_OPTION_LIST,
    DIRECT_PROVIDER_OPTION_SUCCESS,
    DIRECT_PROVIDER_OPTION_FAILURE,

    DIRECT_PROVIDER_SPL_PROCEDURE,
    DIRECT_PROVIDER_SPL_PROCEDURE_SUCCESS,
    DIRECT_PROVIDER_SPL_PROCEDURE_FAILURE,

    DIRECT_ADD_MORE_LIST,
    DIRECT_REMOVE_ADD_MORE_LIST,

    DIRECT_PROVIDER_COUNTRY_LIST,
    DIRECT_PROVIDER_COUNTRY_SUCCESS,
    DIRECT_PROVIDER_COUNTRY_FAILURE,

    DIRECT_EXIST_UPDATE,
    DIRECT_EXIST_UPDATE_SUCCESS,
    DIRECT_EXIST_UPDATE_FAILURE,

    DIRECT_EXIST_SSN_UPDATE,
    DIRECT_EXIST_SSN_UPDATE_SUCCESS,
    DIRECT_EXIST_SSN_UPDATE_FAILURE,

    DIRECT_APPLICATION_OPTION_LIST,
    DIRECT_APPLICATION_OPTION_SUCCESS,
    DIRECT_APPLICATION_OPTION_FAILURE,

    DIRECT_APPLICATION_STATES_LIST,
    DIRECT_APPLICATION_STATES_LIST_SUCCESS,
    DIRECT_APPLICATION_STATES_LIST_FAILURE,

    DIRECT_APPLICATION_SUBMIT,
    DIRECT_APPLICATION_SUBMIT_SUCCESS,
    DIRECT_APPLICATION_SUBMIT_FAILURE,

    DIRECT_PROVIDER_NAME_EXIST_UPDATE,
    DIRECT_PROVIDER_NAME_EXIST_UPDATE_SUCCESS,
    DIRECT_PROVIDER_NAME_EXIST_UPDATE_FAILURE,

    DIRECT_DATA_ACCESS_REQUEST,
    DIRECT_DATA_ACCESS_REQUEST_SUCCESS,
    DIRECT_DATA_ACCESS_REQUEST_FAILURE,

    DIRECT_EXIST_UPDATE_CO,
    DIRECT_EXIST_UPDATE_SUCCESS_CO,
    DIRECT_EXIST_UPDATE_FAILURE_CO,

} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    stateList: [],
    stateListCo: [],
    stateRegion: [],
    stateList2: [],
    stateRegion2: [],
    splProcedure2: [],
    redirectURL: 0,

    stateListApp: [],
};

export default (state = INIT_STATE, action) => {
    // console.log('action.payload')
    // console.log(action.type)
    switch (action.type) {

        case DIRECT_DATA_ACCESS_REQUEST:
            return { ...state, loading: true };

        case DIRECT_DATA_ACCESS_REQUEST_SUCCESS:
            return { ...state, loading: false, access_request_row: action.payload.access_request_row[0] };

        case DIRECT_DATA_ACCESS_REQUEST_FAILURE:
            return { ...state, loading: false };

        case DIRECT_APPLICATION_SUBMIT:
            return { ...state, loading: true };
        case DIRECT_APPLICATION_SUBMIT_SUCCESS:
            var redirectURL = (action.payload.applicationStatus == 0) ? -1 : action.payload.applicationStatus;
            return { ...state, loading: false, rediretURL: redirectURL, applicationID: action.payload.applicationId, customer_redirect_app_success: 1, customer_detail_verify: '' };

        case DIRECT_APPLICATION_SUBMIT_FAILURE:
            return { ...state, loading: false };

        case DIRECT_APPLICATION_STATES_LIST:
            return { ...state, loading: true };

        case DIRECT_APPLICATION_STATES_LIST_SUCCESS:
            if (action.payload.idx === undefined) {
                return { ...state, loading: false, blillingStateType: action.payload.result, CoStateType: action.payload.result };
            }
            if (action.payload.Cidx !== undefined) {
                var newState = state.stateListCo;
                newState[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, stateTypeCo: newState };
            }
            var newStateApp = state.stateListApp;
            newStateApp[action.payload.idx] = action.payload.result;
            return { ...state, loading: false, stateTypeApp: newStateApp };

        case DIRECT_APPLICATION_STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case DIRECT_APPLICATION_OPTION_LIST:
            return { ...state, loading: true, rediretURL: 0, appSearchDetails: '', appSearchDetailsAddress: '', appaddError: '' };

        case DIRECT_APPLICATION_OPTION_SUCCESS:
            var newStateApp = state.stateListApp;
            newStateApp[0] = '';
            var newRegion = state.stateRegion;
            newRegion[0] = '';
            return { ...state, loading: false, bankType: action.payload.bank_type, countries: action.payload.countries, employmentType: action.payload.employment, stateTypeApp: newStateApp, regionType: newRegion, rediretURL: 0, providerLocation: action.payload.location, access_request_data: action.payload.access_request_data[0], relationship: action.payload.relationship };

        case DIRECT_APPLICATION_OPTION_FAILURE:
            return { ...state, loading: false, rediretURL: 0 };

        case DIRECT_EXIST_SSN_UPDATE:
            return { ...state, loading: false, ssnExist: 0 };
        case DIRECT_EXIST_SSN_UPDATE_SUCCESS:
            return { ...state, loading: false, ssnExist: action.payload.exist, isEdit: action.payload.edit };


        case DIRECT_EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0 };
        case DIRECT_EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, userNameExist: action.payload.exist, isEdit: action.payload.edit };

        case DIRECT_EXIST_UPDATE_CO:
            return { ...state, loading: false, nameExist: 0 };
        case DIRECT_EXIST_UPDATE_SUCCESS_CO:
            return { ...state, loading: false, nameExistCO: action.payload.exist, isEditCO: action.payload.edit };


        case DIRECT_PROVIDER_NAME_EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0 };
        case DIRECT_PROVIDER_NAME_EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, ProviderNameExist: action.payload.exist };


        case DIRECT_APPLICATION_STATES_LIST:
            return { ...state, loading: true };

        case DIRECT_APPLICATION_STATES_LIST_SUCCESS:
            if (action.payload.idx === undefined) {
                //return { ...state, loading: false, blillingStateType: action.payload.result };
                return { ...state, loading: false, StateType: action.payload.result };
            }
            var newState = state.stateList;
            newState[action.payload.idx] = action.payload.result;
            return { ...state, loading: false, stateType: newState };

        case DIRECT_APPLICATION_STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case DIRECT_PROVIDER_SPL_PROCEDURE:
            return { ...state, loading: false }

        case DIRECT_PROVIDER_SPL_PROCEDURE_SUCCESS:
            var newsplProcedure = state.splProcedure2;

            newsplProcedure[action.payload.idx] = action.payload.result;

            state.splProcedure2 = newsplProcedure;
            //console.log()
            return { ...state, loading: false, splProcedure: newsplProcedure }

        case DIRECT_PROVIDER_SPL_PROCEDURE_FAILURE:
            return { ...state, loading: false }

        case DIRECT_PROVIDER_OPTION_SUCCESS:
            //console.log(action.payload)
            if (action.payload.chk == 0) {
                var newsplProcedure = state.splProcedure2;
                newsplProcedure[0] = [];
                state.splProcedure2 = newsplProcedure;
                return { ...state, loading: false, redirectURL: 0, provider_option_bank: action.payload.bank_account_type, provider_payment_term: action.payload.pvd_payment_term, provider_loan_term_months: action.payload.pvd_loan_term_month, provider_option_spl: action.payload.speciality, provider_option_discount_type: action.payload.discount_type, provider_option_fee_type: action.payload.fee_type, provider_option_document_type: action.payload.document_type, provider_option_user_role: action.payload.user_role, splProcedure: newsplProcedure };
            } else {
                return { ...state, loading: false, redirectURL: 0, provider_option_bank: action.payload.bank_account_type, provider_payment_term: action.payload.pvd_payment_term, provider_loan_term_months: action.payload.pvd_loan_term_month, provider_option_spl: action.payload.speciality, provider_option_discount_type: action.payload.discount_type, provider_option_fee_type: action.payload.fee_type, provider_option_document_type: action.payload.document_type, provider_option_user_role: action.payload.user_role };
            }

        case DIRECT_PROVIDER_OPTION_FAILURE:
            return { ...state, loading: false };

        case DIRECT_MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, provider_type_master_data_value: action.payload };

        case DIRECT_MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case DIRECT_PROVIDER_COUNTRY_SUCCESS:
            //console.log('action.payload')
            //console.log(action.payload)
            //console.log('action.payload')

            var newState = state.stateList;
            newState[0] = '';
            var newState2 = state.stateList2;
            newState2[0] = '';
            var newRegion = state.stateRegion;
            newRegion[0] = '';
            var newRegion2 = state.stateRegion2;
            newRegion2[0] = '';
            return { ...state, loading: false, redirectURL: 0, provider_country: action.payload, stateType: newState, regionType: newRegion, stateType2: newState2, regionType2: newRegion2 };

        case DIRECT_PROVIDER_COUNTRY_FAILURE:
            return { ...state, loading: false };

        case DIRECT_STATES_LIST:
            return { ...state, loading: true };

        case DIRECT_STATES_LIST_SUCCESS:
            //console.log('action.payload')
            //console.log(action.payload)
            if (action.payload.st == 0) {
                var newState = state.stateList;
                newState[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, redirectURL: 0, stateType: newState };
            } else {
                var newState2 = state.stateList2;
                newState2[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, redirectURL: 0, stateType2: newState2 };
            }


        //return { ...state, loading: false, stateType: newState, stateType2: newState2 };

        case DIRECT_STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case DIRECT_ADD_MORE_LIST:
            //var newsplProcedure = state.splProcedure;
            //newsplProcedure[newsplProcedure.length] = '';

            var newState = state.stateList;
            newState[newState.length] = '';
            var newState2 = state.stateList2;
            newState2[newState2.length] = '';

            var newRegion = state.stateRegion;
            newRegion[newState.length] = '';
            var newRegion2 = state.stateRegion2;
            newRegion2[newState2.length] = '';

            return { ...state, loading: false, stateType: newState, stateType2: newState2, regionType: newRegion, regionType2: newRegion2 };

        case DIRECT_REMOVE_ADD_MORE_LIST:
            var newsplProcedure = state.splProcedure2;
            newsplProcedure = newsplProcedure.filter((s, sidx) => action.payload !== sidx)
            state.splProcedure2 = newsplProcedure;

            var newState = state.stateList;
            newState = newState.filter((s, sidx) => action.payload !== sidx)
            state.stateList = newState;
            var newState2 = state.stateList2;
            newState2 = newState2.filter((s, sidx) => action.payload !== sidx)
            state.stateList2 = newState2;

            var newRegion = state.stateRegion;
            newRegion = newRegion.filter((s, sidx) => action.payload !== sidx)
            state.stateRegion = newRegion;
            var newRegion2 = state.stateRegion2;
            newRegion2 = newRegion2.filter((s, sidx) => action.payload !== sidx)
            state.stateRegion2 = newRegion2;
            //newState[newState.length] = '';
            return { ...state, loading: false, stateType: newState, stateType2: newState2, regionType: newRegion, regionType2: newRegion2, splProcedure: newsplProcedure };

        case DIRECT_PROVIDER_INSERT:
            return { ...state, loading: false, aloading: true };

        case DIRECT_PROVIDER_INSERT_SUCCESS:
            return { ...state, loading: false, aloading: false, redirectURL: 1, customer_redirect_pi: 1, provider_details: '', provider_location: '', provider_bank: '', provider_specility: '', provider_user: '', provider_additional_discount: '', provider_additional_fee: '', provider_document: '', provider_additional_discount: '', customer_detail_verify: '' };

        case DIRECT_PROVIDER_INSERT_FAILURE:
            return { ...state, loading: false, aloading: false, redirectURL: 1, customer_redirect_pi: 0 };

        //////////\\\\\\\\\

        case CUSTOMER_DETAIL_UPDATE_REQUEST:
            return { ...state, loading: false, aloading: true };

        case CUSTOMER_DETAIL_UPDATE_SUCCESS:
            var customer_redURL;

            if (action.payload.customer_type == 'Provider') {
                customer_redURL = 1;
            } else {
                customer_redURL = 2;
            }

            return { ...state, loading: false, redirectURL: 1, customer_redirect: customer_redURL, customer_detail_verify: '' };

        case CUSTOMER_DETAIL_UPDATE_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_DETAIL_REQUEST:
            return { ...state, loading: false, aloading: true };

        case CUSTOMER_DETAIL_SUCCESS:
            return { ...state, loading: false, redirectURL: 1, customer_detail_verify: action.payload.customer_detail_verify };

        case CUSTOMER_DETAIL_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_REGISTER_REQUEST:
            return { ...state, loading: false, aloading: true };

        case CUSTOMER_REGISTER_SUCCESS:
            return { ...state, loading: false, redirectURL: 1, last_insert_id: action.payload.last_insert_id };

        case CUSTOMER_REGISTER_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
