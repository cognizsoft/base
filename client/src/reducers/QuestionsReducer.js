/**
 * Auth User Reducers
 */
import {
    QUESTION_LIST,
    QUESTION_LIST_SUCCESS,
    QUESTION_LIST_FAILURE,
    QUESTION_INSERT,
    QUESTION_INSERT_SUCCESS,
    QUESTION_INSERT_FAILURE,
    QUESTION_DELETE,
    QUESTION_DELETE_SUCCESS,
    QUESTION_DELETE_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case QUESTION_LIST:
            return { ...state, loading: true };

        case QUESTION_LIST_SUCCESS:
            return { ...state, loading: false, questionlist: action.payload };

        case QUESTION_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, questionExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, questionExist: action.payload.exist, isEdit: action.payload.edit };

        case QUESTION_INSERT:
            return { ...state, loading: true };

        case QUESTION_INSERT_SUCCESS:
            let newQuestion = {
                id: action.payload.last_question_id,
                name: action.payload.name,
                status: action.payload.question_status
            }
            let stateupdated = state.questionlist;
            stateupdated.reverse();
            stateupdated.push(newQuestion);
            stateupdated.reverse();
            
            return { ...state, loading: false, questionlist: stateupdated };

        case QUESTION_INSERT_FAILURE:
            return { ...state, loading: false };

        case QUESTION_DELETE:
            return {...state, loading: true}
        case QUESTION_DELETE_SUCCESS:
            const deleteQuestion = state.questionlist.filter(x => x.id == action.payload );
            let indexOfDeleteQuestion = state.questionlist.indexOf(deleteQuestion[0]);
            state.questionlist.splice(indexOfDeleteQuestion, 1);
            return { ...state, loading: false };
        
        case QUESTION_DELETE_FAILURE:
             return {...state, loading: false}

        default: return { ...state };
    }
}
