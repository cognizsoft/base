/**
 * Auth User Reducers
 */
import {
    COUNTRY_LIST,
    COUNTRY_LIST_SUCCESS,
    COUNTRY_LIST_FAILURE,
    COUNTRY_INSERT,
    COUNTRY_INSERT_SUCCESS,
    COUNTRY_INSERT_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case COUNTRY_LIST:
            return { ...state, loading: true };

        case COUNTRY_LIST_SUCCESS:
            return { ...state, loading: false, countrylist: action.payload };

        case COUNTRY_LIST_FAILURE:
            return { ...state, loading: false };
        
        case COUNTRY_INSERT:
            return { ...state, loading: true };

        case COUNTRY_INSERT_SUCCESS:
            let newCountry = {
                id:action.payload.last_country_id,
                name: action.payload.name,
                abbreviation: action.payload.abbreviation,
                status: action.payload.country_status
            }
            let countryupdated = state.countrylist;
            countryupdated.reverse();
            countryupdated.push(newCountry);
            countryupdated.reverse();
            return { ...state, loading: false, countrylist: countryupdated };

        case COUNTRY_INSERT_FAILURE:
            return { ...state, loading: false };

        
        case EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, nameExist: action.payload.exist, isEdit: action.payload.edit };

        default: return { ...state };
    }
}
