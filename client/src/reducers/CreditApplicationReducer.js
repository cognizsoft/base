/**
 * Auth User Reducers
 */
import {
    APPLICATION_OPTION_LIST,
    APPLICATION_OPTION_SUCCESS,
    APPLICATION_OPTION_FAILURE,
    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,
    ADD_MORE_LIST,
    REMOVE_ADD_MORE_LIST,
    REGION_LIST,
    REGION_LIST_SUCCESS,
    REGION_LIST_FAILURE,
    APPLICATION_SUBMIT,
    APPLICATION_SUBMIT_SUCCESS,
    APPLICATION_SUBMIT_FAILURE,
    CREDIT_APPLICATION_LIST,
    CREDIT_APPLICATION_LIST_SUCCESS,
    CREDIT_APPLICATION_LIST_FAILURE,
    PAYMENT_PLAN_REQUEST,
    PAYMENT_PLAN_SUCCESS,
    PAYMENT_PLAN_FAILURE,
    CREATE_PLAN_REQUEST,
    CREATE_PLAN_SUCCESS,
    CREATE_PLAN_FAILURE,
    SINGLE_PLAN_REQUEST,
    SINGLE_PLAN_SUCCESS,
    SINGLE_PLAN_FAILURE,
    VIEW_APPLICATION_REQUEST,
    VIEW_APPLICATION_SUCCESS,
    VIEW_APPLICATION_FAILURE,
    CUSTOMER_SEARCH_REQUEST,
    CUSTOMER_SEARCH_SUCCESS,
    CUSTOMER_SEARCH_FAILURE,
    REMOVE_CUSTOMER_SEARCH,
    REVIEW_APPLICATION_REQUEST,
    REVIEW_APPLICATION_SUCCESS,
    REVIEW_APPLICATION_FAILURE,
    MANUAL_ACTION_REQUEST,
    MANUAL_ACTION_SUCCESS,
    MANUAL_ACTION_FAILURE,
    REJECT_ACTION_REQUEST,
    REJECT_ACTION_SUCCESS,
    REJECT_ACTION_FAILURE,
    UPLOAD_DOCUMENT_REQUEST,
    UPLOAD_DOCUMENT_SUCCESS,
    UPLOAD_DOCUMENT_FAILURE,
    REVIEW_DOCUMENT_REQUEST,
    REVIEW_DOCUMENT_SUCCESS,
    REVIEW_DOCUMENT_FAILURE,
    REVIEW_MANUAL_ACTION,
    SPECIALITY_LIST,
    SPECIALITY_LIST_SUCCESS,
    SPECIALITY_LIST_FAILURE,
    CUSTOMER_SEARCH_APPlICTION_REQUEST,
    CUSTOMER_SEARCH_APPlICTION_SUCCESS,
    CUSTOMER_SEARCH_APPlICTION_FAILURE,
    APPlICTION_DETAILS_REQUEST,
    APPlICTION_DETAILS_SUCCESS,
    APPlICTION_DETAILS_FAILURE,
    APPlICTION_PLAN_RESET,
    ESTIMATE_DETAILS_REQUEST,
    ESTIMATE_DETAILS_SUCCESS,
    ESTIMATE_DETAILS_FAILURE,
    CUSTOMER_INFORMATION_REQUEST,
    CUSTOMER_INFORMATION_SUCCESS,
    CUSTOMER_INFORMATION_FAILURE,

    ALL_APPLICATION_DOC_DETAILS,
    ALL_APPLICATION_DOC_DETAILS_SUCCESS,
    ALL_APPLICATION_DOC_DETAILS_FAILURE,

    UPLOAD_REJECTION_DOCUMENT_REQUEST,
    UPLOAD_REJECTION_DOCUMENT_SUCCESS,
    UPLOAD_REJECTION_DOCUMENT_FAILURE,

    DOCTORS_LIST,
    DOCTORS_LIST_SUCCESS,
    DOCTORS_LIST_FAILURE,

    APP_EXPIRE_DATE_UPDATE,
    APP_EXPIRE_DATE_UPDATE_SUCCESS,
    APP_EXPIRE_DATE_UPDATE_FAILURE,

    APP_OVERRIDE_AMOUNT_UPDATE,
    APP_OVERRIDE_AMOUNT_UPDATE_SUCCESS,
    APP_OVERRIDE_AMOUNT_UPDATE_FAILURE,

    UNLOCK_APPLICATION,
    UNLOCK_APPLICATION_SUCCESS,
    UNLOCK_APPLICATION_FAILURE,

    DOWNLOAD_FILE_REQUEST,
    DOWNLOAD_FILE_SUCCESS,
    DOWNLOAD_FILE_FAILURE,

    APP_DOC_RESEND_REQUEST,
    APP_DOC_RESEND_SUCCESS,
    APP_DOC_RESEND_FAILURE,

    CREATE_PRINT_PLAN_REQUEST,
    CREATE_PRINT_PLAN_SUCCESS,
    CREATE_PRINT_PLAN_FAILURE,

    UPLOAD_AGREEMENT_VIEW_DETAILS_REQUEST,
    UPLOAD_AGREEMENT_VIEW_DETAILS_SUCCESS,
    UPLOAD_AGREEMENT_VIEW_DETAILS_FAILURE,

    UPLOAD_PLAN_AGREEMENT_REQUEST,
    UPLOAD_PLAN_AGREEMENT_SUCCESS,
    UPLOAD_PLAN_AGREEMENT_FAILURE,

    PLAN_AGREEMENT_REVIEW_DETAILS_REQUEST,
    PLAN_AGREEMENT_REVIEW_DETAILS_SUCCESS,
    PLAN_AGREEMENT_REVIEW_DETAILS_FAILURE,

    PLAN_REJECT_APPROVE_ACTION_REQUEST,
    PLAN_REJECT_APPROVE_ACTION_SUCCESS,
    PLAN_REJECT_APPROVE_ACTION_FAILURE,

    PROVIDER_PLAN_LIST_REQUEST,
    PROVIDER_PLAN_LIST_SUCCESS,
    PROVIDER_PLAN_LIST_FAILURE,

    PROVIDER_PLAN_SUBMIT_REQUEST,
    PROVIDER_PLAN_SUBMIT_SUCCESS,
    PROVIDER_PLAN_SUBMIT_FAILURE,

    DOWNLOAD_PLAN_AGREEMENT_REQUEST,
    DOWNLOAD_PLAN_AGREEMENT_SUCCESS,
    DOWNLOAD_PLAN_AGREEMENT_FAILURE,

    DOWNLOAD_DOC_REQUEST,
    DOWNLOAD_DOC_SUCCESS,
    DOWNLOAD_DOC_FAILURE,

    CREDIT_APPLICATION_LIST_DOWN_REQUEST,
    CREDIT_APPLICATION_LIST_DOWN_SUCCESS,
    CREDIT_APPLICATION_LIST_DOWN_FAILURE,

    CANCEL_REFUND_PLAN_DETAILS_REQUEST,
    CANCEL_REFUND_PLAN_DETAILS_SUCCESS,
    CANCEL_REFUND_PLAN_DETAILS_FAILURE,

    CANCEL_REFUND_PLAN_DETAILS_DOWN_REQUEST,
    CANCEL_REFUND_PLAN_DETAILS_DOWN_SUCCESS,
    CANCEL_REFUND_PLAN_DETAILS_DOWN_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    stateList: [],
    stateListCo: [],
    stateRegion: [],
    rediretURL: 0,
    rediretPlanURL: 0,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case CANCEL_REFUND_PLAN_DETAILS_DOWN_REQUEST:
            return { ...state, loading: true };

        case CANCEL_REFUND_PLAN_DETAILS_DOWN_SUCCESS:
            return { ...state, loading: false };

        case CANCEL_REFUND_PLAN_DETAILS_DOWN_FAILURE:
            return { ...state, loading: false };

        case CANCEL_REFUND_PLAN_DETAILS_REQUEST:
            return { ...state, loading: true, redirectURL: 0 };

        case CANCEL_REFUND_PLAN_DETAILS_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, customer_refund: action.payload.customer_refund, provider_refund: action.payload.provider_refund, sttlement_detail: action.payload.sttlement_detail, customer_detail: action.payload.customer_detail, provider_detail: action.payload.provider_detail };

        case CANCEL_REFUND_PLAN_DETAILS_FAILURE:
            return { ...state, loading: false };

        /*case CREDIT_APPLICATION_LIST_DOWN_REQUEST:
            return { ...state, loading: true };
        case CREDIT_APPLICATION_LIST_DOWN_SUCCESS:
            return { ...state, loading: false, applicationList: action.payload.result, careCould: action.payload.careCould, yearStart: action.payload.yearStart, appStatus: action.payload.app_status };
        case CREDIT_APPLICATION_LIST_DOWN_FAILURE:
            return { ...state, loading: false };*/

        case DOWNLOAD_DOC_REQUEST:
            return { ...state, doc_loading: true }
        case DOWNLOAD_DOC_SUCCESS:
            return { ...state, doc_loading: false }
        case DOWNLOAD_DOC_FAILURE:
            return { ...state, doc_loading: false }

        case DOWNLOAD_PLAN_AGREEMENT_REQUEST:
            return { ...state, down_loading: true }
        case DOWNLOAD_PLAN_AGREEMENT_SUCCESS:
            return { ...state, down_loading: false }
        case DOWNLOAD_PLAN_AGREEMENT_FAILURE:
            return { ...state, down_loading: false }

        case PROVIDER_PLAN_SUBMIT_REQUEST:
            return { ...state, loading: true, submitPlan: false }
        case PROVIDER_PLAN_SUBMIT_SUCCESS:
            return { ...state, loading: false, submitPlan: true }
        case PROVIDER_PLAN_SUBMIT_FAILURE:
            return { ...state, loading: false, submitPlan: false }

        case PROVIDER_PLAN_LIST_REQUEST:
            return { ...state, loading: true, submitInvoice: false, previewInvoice: undefined };
        case PROVIDER_PLAN_LIST_SUCCESS:
            return { ...state, loading: false, planList: action.payload.result };
        case PROVIDER_PLAN_LIST_FAILURE:
            return { ...state, loading: false };

        case PLAN_REJECT_APPROVE_ACTION_REQUEST:
            return { ...state, loading: true, manualType: '' };
        case PLAN_REJECT_APPROVE_ACTION_SUCCESS:
            return { ...state, loading: false, manualType: action.payload.type };
        case PLAN_REJECT_APPROVE_ACTION_FAILURE:
            return { ...state, loading: false };

        case PLAN_AGREEMENT_REVIEW_DETAILS_REQUEST:
            return { ...state, loading: true, appDetails: '', docDetails: '' };
        case PLAN_AGREEMENT_REVIEW_DETAILS_SUCCESS:
            return { ...state, loading: false, appDocDetails: action.payload.appDocDetails, docDetails: action.payload.docDetails };
        case PLAN_AGREEMENT_REVIEW_DETAILS_FAILURE:
            return { ...state, loading: false };

        case UPLOAD_PLAN_AGREEMENT_REQUEST:
            return { ...state, loading: true, uploadAgreementRedirect: '' };
        case UPLOAD_PLAN_AGREEMENT_SUCCESS:
            return { ...state, loading: false, uploadAgreementRedirect: 1 };
        case UPLOAD_PLAN_AGREEMENT_FAILURE:
            return { ...state, loading: false, uploadAgreementRedirect: '' };

        case UPLOAD_AGREEMENT_VIEW_DETAILS_REQUEST:
            return { ...state, loading: true, uploadAgreementRedirect: '' };
        case UPLOAD_AGREEMENT_VIEW_DETAILS_SUCCESS:
            return { ...state, loading: false, planDetails: action.payload };
        case UPLOAD_AGREEMENT_VIEW_DETAILS_FAILURE:
            return { ...state, loading: false, planDetails: '' };

        case CREATE_PRINT_PLAN_REQUEST:
            return { ...state, loading: true, rediretPlanURL: 0 };
        case CREATE_PRINT_PLAN_SUCCESS:
            return { ...state, loading: false, rediretPlanURL: 1, paymentPlanID: action.payload.paymentPlan };
        case CREATE_PRINT_PLAN_FAILURE:
            return { ...state, loading: false, rediretPlanURL: 0 };

        case APP_DOC_RESEND_REQUEST:
            return { ...state, loading: true };
        case APP_DOC_RESEND_SUCCESS:
            return { ...state, loading: false, docStatus: action.payload };
        case APP_DOC_RESEND_FAILURE:
            return { ...state, loading: false };


        case DOWNLOAD_FILE_REQUEST:
            return { ...state, loading: true, oneDrivePreview: '' };
        case DOWNLOAD_FILE_SUCCESS:
            return { ...state, loading: false, oneDrivePreview: action.payload.file };
        case DOWNLOAD_FILE_FAILURE:
            return { ...state, loading: false };


        case UNLOCK_APPLICATION:
            return { ...state, loading: true };
        case UNLOCK_APPLICATION_SUCCESS:
            return { ...state, loading: false, appDetails: action.payload.appDetails, appDetailsAddress: action.payload.appAddress };
        case UNLOCK_APPLICATION_FAILURE:
            return { ...state, loading: false };

        case APP_OVERRIDE_AMOUNT_UPDATE:
            return { ...state, loading: true };
        case APP_OVERRIDE_AMOUNT_UPDATE_SUCCESS:
            return { ...state, loading: false, appDetails: action.payload.appDetails, appDetailsAddress: action.payload.appAddress };
        case APP_OVERRIDE_AMOUNT_UPDATE_FAILURE:
            return { ...state, loading: false };

        case APP_EXPIRE_DATE_UPDATE:
            return { ...state, loading: true };
        case APP_EXPIRE_DATE_UPDATE_SUCCESS:
            return { ...state, loading: false, appDetails: action.payload.appDetails, appDetailsAddress: action.payload.appAddress };
        case APP_EXPIRE_DATE_UPDATE_FAILURE:
            return { ...state, loading: false };

        // get doctors code start here
        case DOCTORS_LIST:
            return { ...state, loading: true, appDetails: '', docDetails: '' };
        case DOCTORS_LIST_SUCCESS:
            return { ...state, loading: false, providerDoctors: action.payload.doctors };
        case DOCTORS_LIST_FAILURE:
            return { ...state, loading: false };
        // get doctors code end here
        //upload rejection document
        case UPLOAD_REJECTION_DOCUMENT_REQUEST:
            return { ...state, loading: true, redirectUpload: '' };
        case UPLOAD_REJECTION_DOCUMENT_SUCCESS:
            return { ...state, loading: false, redirectUpload: '' };
        case UPLOAD_REJECTION_DOCUMENT_FAILURE:
            return { ...state, loading: false, redirectUpload: '' };
        // all application documents
        case ALL_APPLICATION_DOC_DETAILS:
            return { ...state, loading: true, allDocuments: undefined,planDocuments:undefined};
        case ALL_APPLICATION_DOC_DETAILS_SUCCESS:
            return { ...state, loading: false, allDocuments: action.payload.allDocuments,planDocuments:action.payload.planDocuments };
        case ALL_APPLICATION_DOC_DETAILS_FAILURE:
            return { ...state, loading: false };
        // application details start here
        case CUSTOMER_INFORMATION_REQUEST:
            return { ...state, loading: true };
        case CUSTOMER_INFORMATION_SUCCESS:
            return { ...state, loading: false, finalDetails: action.payload.application };
        case CUSTOMER_INFORMATION_FAILURE:
            return { ...state, loading: false };
        // application details end here
        // Estimate plan start here
        case ESTIMATE_DETAILS_REQUEST:
            return { ...state, loading: true };
        case ESTIMATE_DETAILS_SUCCESS:
            var paymentPlanAllowed = action.payload.result.reverse();
            return { ...state, loading: false, paymentPlanAllowed: paymentPlanAllowed, estDetails: action.payload.applcation_details, appProviderDetails: action.payload.providerDetails };
        case ESTIMATE_DETAILS_FAILURE:
            return { ...state, loading: false, paymentPlanAllowed: undefined };
        // Estimate plan end here
        // reset plan start here
        case APPlICTION_PLAN_RESET:
            if (action.payload) {
                return { ...state, rediretPlanURL: 0, paymentPlanID: undefined, paymentPlanAllow: undefined, applicationDetails: undefined, searchCustomer: undefined };
            } else {
                return { ...state, rediretPlanURL: 0, paymentPlanID: undefined, paymentPlanAllow: undefined, applicationDetails: undefined };
            }
        // reset pan end herer
        // get search custormer details start here
        case APPlICTION_DETAILS_REQUEST:
            return { ...state, loading: true };
        case APPlICTION_DETAILS_SUCCESS:
            return { ...state, loading: false, appSummery: action.payload.rows };
        case APPlICTION_DETAILS_FAILURE:
            return { ...state, loading: false };
        // get search customer details end here
        // get search custormer details start here
        case CUSTOMER_SEARCH_APPlICTION_REQUEST:
            return { ...state, loading: true };
        case CUSTOMER_SEARCH_APPlICTION_SUCCESS:
            return { ...state, loading: false, searchCustomer: action.payload.customerDetails };
        case CUSTOMER_SEARCH_APPlICTION_FAILURE:
            return { ...state, loading: false };
        // get search customer details end here
        // get specility code start here
        case SPECIALITY_LIST:
            return { ...state, loading: true, appDetails: '', docDetails: '' };
        case SPECIALITY_LIST_SUCCESS:
            return { ...state, loading: false, providerSpeciality: action.payload.speciality };
        case SPECIALITY_LIST_FAILURE:
            return { ...state, loading: false };
        // get specility code end here
        case REVIEW_MANUAL_ACTION:
            return { ...state, manualType: '' };
        // review application document start here
        case REVIEW_DOCUMENT_REQUEST:
            return { ...state, loading: true, appDetails: '', docDetails: '' };
        case REVIEW_DOCUMENT_SUCCESS:
            return { ...state, loading: false, appDocDetails: action.payload.appDocDetails, docDetails: action.payload.docDetails };
        case REVIEW_DOCUMENT_FAILURE:
            return { ...state, loading: false };
        // review application document start here
        // upload document start here
        case UPLOAD_DOCUMENT_REQUEST:
            return { ...state, loading: true, redirectUpload: '' };
        case UPLOAD_DOCUMENT_SUCCESS:
            return { ...state, loading: false, redirectUpload: 1 };
        case UPLOAD_DOCUMENT_FAILURE:
            return { ...state, loading: false, redirectUpload: '' };
        // upload document end here
        // rejact application code start here
        case REJECT_ACTION_REQUEST:
            return { ...state, loading: true };
        case REJECT_ACTION_SUCCESS:
            return { ...state, loading: false, appRejectDetails: action.payload.appDetails, appRejectDetailsAddress: action.payload.appAddress };
        case REJECT_ACTION_FAILURE:
            return { ...state, loading: false };
        // rejact application code end here
        // manual process code start here
        case MANUAL_ACTION_REQUEST:
            return { ...state, loading: true, manualType: '' };
        case MANUAL_ACTION_SUCCESS:
            return { ...state, loading: false, manualType: action.payload.type };
        case MANUAL_ACTION_FAILURE:
            return { ...state, loading: false };
        // manual process code end here
        // review credit application start here
        case REVIEW_APPLICATION_REQUEST:
            return { ...state, loading: true, manualType: '', reviewAppDetails: '', reviewApiRiskFactor: '', reviewHPSRiskFactor: '' };
        case REVIEW_APPLICATION_SUCCESS:
            return { ...state, loading: false, reviewAppDetails: action.payload.appDetails, reviewApiRiskFactor: action.payload.apiRiskFactor, reviewHPSRiskFactor: action.payload.hpsRiskFactor, termMonth: action.payload.term_month };
        case REVIEW_APPLICATION_FAILURE:
            return { ...state, loading: false };
        // review credit application end here
        // remove exits state
        case REMOVE_CUSTOMER_SEARCH:
            return { ...state, appSearchDetails: '', appSearchDetailsAddress: '', appaddError: '' };
        // search code start hare
        case CUSTOMER_SEARCH_REQUEST:
            return { ...state, loading: true, appSearchDetails: '', appSearchDetailsAddress: '', appaddError: '' };
        case CUSTOMER_SEARCH_SUCCESS:
            return { ...state, loading: false, appSearchDetails: action.payload.customerDetails, appSearchDetailsAddress: action.payload.customerAddress, appaddError: action.payload.addError, bankDetails: action.payload.bankDetails, bankError: action.payload.bankError, secDetails: action.payload.secDetails };
        case CUSTOMER_SEARCH_FAILURE:
            return { ...state, loading: false };
        // search code end hare
        // view application details start here
        case VIEW_APPLICATION_REQUEST:
            return { ...state, loading: true,appDetails: undefined };
        case VIEW_APPLICATION_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                appDetails: action.payload.appDetails, 
                appDetailsAddress: action.payload.appAddress, 
                experianAddress: action.payload.experianAddress, 
                experianEmployment: action.payload.experianEmployment,
                secDetails: action.payload.secDetails,
                bankDetails: action.payload.bankDetails,
                CoappDetails: action.payload.CoappDetails, 
                CoappDetailsAddress: action.payload.CoappAddress, 
                CoexperianAddress: action.payload.CoexperianAddress, 
                CoexperianEmployment: action.payload.CoexperianEmployment,
                CosecDetails: action.payload.CosecDetails,
                coDetails: action.payload.coDetails,
                CobankDetails: action.payload.CobankDetails,
            };
        case VIEW_APPLICATION_FAILURE:
            return { ...state, loading: false };
        // view application details end here
        // single plan details start here
        case SINGLE_PLAN_REQUEST:
            return { ...state, loading: true, redirectURL: 0 };
        case SINGLE_PLAN_SUCCESS:
            /*var planD = action.payload.payment_plan_details.reverse();
            //var paymentPlanDetails = action.payload.payment_plan_details;
            var addiAmt = (planD) ? planD.filter(x => x.additional_amount !== 0 && x.additional_amount !== null) : '';

            var addiAmtTotal = 0;
            if (addiAmt) {
                for (var i = 0; i < addiAmt.length; i++) {
                    addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
                }
            }

            for (var k = 0; k < planD.length; k++) {
                if (addiAmtTotal > planD[k].installment_amt) {
                    addiAmtTotal = addiAmtTotal - planD[k].installment_amt;
                    planD[k].installment_amt = 0;
                } else {
                    planD[k].installment_amt = planD[k].installment_amt - addiAmtTotal;
                    addiAmtTotal = 0
                }
            }

            planD.reverse();*/

            return { ...state, loading: false, redirectURL: 0, mainPlan: action.payload.plan, singlePlanDetails: action.payload.plan_details, paymentPlanDetails: action.payload.payment_plan_details, appProviderDetails: action.payload.providerDetails };
        case SINGLE_PLAN_FAILURE:
            return { ...state, loading: false };
        // single plan details end here
        // payemnt paln list start here
        case CREATE_PLAN_REQUEST:
            return { ...state, loading: true, rediretPlanURL: 0 };
        case CREATE_PLAN_SUCCESS:
            return { ...state, loading: false, rediretPlanURL: 1, paymentPlanID: action.payload.paymentPlan };
        case CREATE_PLAN_FAILURE:
            return { ...state, loading: false, rediretPlanURL: 0 };
        // payment plan list end here
        // payemnt paln list start here
        case PAYMENT_PLAN_REQUEST:
            return { ...state, loading: true };
        case PAYMENT_PLAN_SUCCESS:
            var paymentPlanAllow = action.payload.result.reverse();
            return { ...state, loading: false, paymentPlanAllow: paymentPlanAllow, applicationDetails: action.payload.applcation_details, appProviderDetails: action.payload.providerDetails };
        case PAYMENT_PLAN_FAILURE:
            return { ...state, loading: false };
        // payment plan list end here

        case CREDIT_APPLICATION_LIST:
            return { ...state, loading: true };
        case CREDIT_APPLICATION_LIST_SUCCESS:
            return { ...state, loading: false, applicationList: action.payload.result, careCould: action.payload.careCould, yearStart: action.payload.yearStart, appStatus: action.payload.app_status };
        case CREDIT_APPLICATION_LIST_FAILURE:
            return { ...state, loading: false };

        case APPLICATION_SUBMIT:
            return { ...state, loading: true };
        case APPLICATION_SUBMIT_SUCCESS:
            var redirectURL = (action.payload.applicationStatus == 0) ? -1 : action.payload.applicationStatus;
            return { ...state, loading: false, rediretURL: redirectURL, applicationID: action.payload.applicationId };

        case APPLICATION_SUBMIT_FAILURE:
            return { ...state, loading: false };

        case APPLICATION_OPTION_LIST:
            return { ...state, loading: true, rediretURL: 0, appSearchDetails: '', appSearchDetailsAddress: '', appaddError: '' };

        case APPLICATION_OPTION_SUCCESS:
            var newState = state.stateList;
            newState[0] = '';
            var newRegion = state.stateRegion;
            newRegion[0] = '';
            return { ...state, loading: false, bankType: action.payload.bank_type, countries: action.payload.countries, employmentType: action.payload.employment, stateType: newState, regionType: newRegion, rediretURL: 0, providerLocation: action.payload.location, relationship:action.payload.relationship };

        case APPLICATION_OPTION_FAILURE:
            return { ...state, loading: false, rediretURL: 0 };

        case REGION_LIST:
            return { ...state, loading: true };
        case REGION_LIST_SUCCESS:
            if (action.payload.idx === undefined) {
                return { ...state, loading: false, blillingRegionType: action.payload.result };
            }
            var newRegion = state.stateRegion;
            newRegion[action.payload.idx] = action.payload.result;
            return { ...state, loading: false, regionType: newRegion };

        case REGION_LIST_FAILURE:
            return { ...state, loading: false };

        case STATES_LIST:
            return { ...state, loading: true };

        case STATES_LIST_SUCCESS:
            if (action.payload.idx === undefined) {
                return { ...state, loading: false, blillingStateType: action.payload.result, CoStateType: action.payload.result };
            }
            if (action.payload.Cidx !== undefined) {
                var newState = state.stateListCo;
                newState[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, stateTypeCo: newState };
            }
            var newState = state.stateList;
            newState[action.payload.idx] = action.payload.result;
            return { ...state, loading: false, stateType: newState };

        case STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case ADD_MORE_LIST:
            var newState = state.stateList;
            newState[newState.length] = '';
            var newRegion = state.stateRegion;
            newRegion[newState.length] = '';
            return { ...state, loading: false, stateType: newState, regionType: newRegion };
        case REMOVE_ADD_MORE_LIST:
            if (action.payload.Cidx !== undefined) {
                var newState = state.stateListCo;
                newState = newState.filter((s, sidx) => action.payload.idx !== sidx)
                state.stateListCo = newState;
                return { ...state, loading: false, stateTypeCo: newState };
            }
            var newState = state.stateList;
            newState = newState.filter((s, sidx) => action.payload.idx !== sidx)
            state.stateList = newState;
            var newRegion = state.stateRegion;
            newRegion = newRegion.filter((s, sidx) => action.payload.idx !== sidx)
            state.stateRegion = newRegion;
            //newState[newState.length] = '';
            return { ...state, loading: false, stateType: newState, regionType: newRegion };
        default: return { ...state };
    }
}
