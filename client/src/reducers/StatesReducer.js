/**
 * Auth User Reducers
 */
import {
    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,
    STATES_INSERT,
    STATES_INSERT_SUCCESS,
    STATES_INSERT_FAILURE,
    COUNTRY_LIST,
    COUNTRY_LIST_SUCCESS,
    COUNTRY_LIST_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case STATES_LIST:
            return { ...state, loading: true };

        case STATES_LIST_SUCCESS:
            return { ...state, loading: false, statelist: action.payload };

        case STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case COUNTRY_LIST_SUCCESS:
            return { ...state, loading: false, countrylist: action.payload };

        case COUNTRY_LIST_FAILURE:
            return { ...state, loading: false };

        

        case EXIST_UPDATE:
            return { ...state, loading: false, stateExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            if(action.payload.abbreviation === 1){
                return { ...state, loading: false, abbreviationExist: action.payload.exist, isEdit: action.payload.edit };
            }else{
                return { ...state, loading: false, stateExist: action.payload.exist, isEditS: action.payload.edit };
            }
            
        
        case STATES_INSERT:
            return { ...state, loading: true };

        case STATES_INSERT_SUCCESS:
            const filterType = state.countrylist.filter(x => x.id == action.payload.country );
            //console.log(filterType)
            let newState = {
                state_id:action.payload.last_state_id,
                name: action.payload.name,
                abbreviation: action.payload.abbreviation,
                serving: action.payload.serving,
                status: action.payload.state_status,
                country_id: action.payload.country
            }

            newState.country = filterType[0].name;

            let stateupdated = state.statelist;
            //console.log(stateupdated)
            stateupdated.reverse();
            stateupdated.push(newState);
            stateupdated.reverse();
            return { ...state, loading: false, statelist: stateupdated };

        case STATES_INSERT_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
