/**
 * Sidebar Reducers
 */
import update from 'react-addons-update';
import { TOGGLE_MENU, AGENCY_TOGGLE_MENU, REFRESH_SIDEBAR } from 'Actions/types';

// nav links
import navLinks from 'Components/Sidebar/NavLinks';
import providerNavLinks from 'Components/Sidebar/ProviderNavLinks';
import customerNavLinks from 'Components/Sidebar/CustomerNavLinks';
import CosignerNavLinks from 'Components/Sidebar/CosignerNavLinks';
import { currentUserRole, currentCoSignerId } from '../apifile';
import agencyNavLinks from 'Components/AgencyMenu/NavLinks';

let userSidebar = navLinks;
if (currentUserRole() == 'Provider') {
	userSidebar = providerNavLinks;
} else if (currentUserRole() == 'Customers' && currentCoSignerId() == '') {
	userSidebar = customerNavLinks;
} else if (currentUserRole() == 'Customers' && currentCoSignerId() != ''){
	userSidebar = CosignerNavLinks;
}

const INIT_STATE = {
	sidebarMenus: userSidebar,
	agencySidebarMenu: agencyNavLinks,
};

export default (state = INIT_STATE, action) => {
	switch (action.type) {
		case TOGGLE_MENU:
			let index = state.sidebarMenus[action.payload.stateCategory].indexOf(action.payload.menu);
			for (var key in state.sidebarMenus) {
				var obj = state.sidebarMenus[key];
				for (let i = 0; i < obj.length; i++) {
					const element = obj[i];
					if (element.open) {
						if (key === action.payload.stateCategory) {
							return update(state, {
								sidebarMenus: {
									[key]: {
										[i]: {
											open: { $set: false }
										},
										[index]: {
											open: { $set: !action.payload.menu.open }
										}
									}
								}
							});
						} else {
							return update(state, {
								sidebarMenus: {
									[key]: {
										[i]: {
											open: { $set: false }
										}
									},
									[action.payload.stateCategory]: {
										[index]: {
											open: { $set: !action.payload.menu.open }
										}
									}
								}
							});
						}
					}
				}
			}
			return update(state, {
				sidebarMenus: {
					[action.payload.stateCategory]: {
						[index]: {
							open: { $set: !action.payload.menu.open }
						}
					}
				}
			});
		case AGENCY_TOGGLE_MENU:
			let agencyMenuIndex = state.agencySidebarMenu[action.payload.stateCategory].indexOf(action.payload.menu);
			for (var id in state.agencySidebarMenu) {
				var object = state.agencySidebarMenu[id];
				for (let i = 0; i < object.length; i++) {
					const element = object[i];
					if (element.open) {
						if (id === action.payload.stateCategory) {
							return update(state, {
								agencySidebarMenu: {
									[id]: {
										[i]: {
											open: { $set: false }
										},
										[agencyMenuIndex]: {
											open: { $set: !action.payload.menu.open }
										}
									}
								}
							});
						} else {
							return update(state, {
								agencySidebarMenu: {
									[id]: {
										[i]: {
											open: { $set: false }
										}
									},
									[action.payload.stateCategory]: {
										[agencyMenuIndex]: {
											open: { $set: !action.payload.menu.open }
										}
									}
								}
							});
						}
					}
				}
			}
			return update(state, {
				agencySidebarMenu: {
					[action.payload.stateCategory]: {
						[agencyMenuIndex]: {
							open: { $set: !action.payload.menu.open }
						}
					}
				}
			});
		case REFRESH_SIDEBAR:
			let userActionSidebar = navLinks;
			if (action.payload.role == 'Provider') {
				userActionSidebar = providerNavLinks;
			} else if (action.payload.coSigner != null && action.payload.role == 'Customers') {
				userActionSidebar = CosignerNavLinks;
			} else if (action.payload.role == 'Customers') {
				userActionSidebar = customerNavLinks;
			}
			const INIT_STATE_UPDATE = {
				sidebarMenus: userActionSidebar,
				agencySidebarMenu: agencyNavLinks,
			};
			state = INIT_STATE_UPDATE;
			return { ...state };
		default:
			return { ...state };
	}
}
