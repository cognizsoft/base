/**
 * Auth User Reducers
 */
import {
    CUSTOMER_LIST,
    CUSTOMER_LIST_SUCCESS,
    CUSTOMER_LIST_FAILURE,
    VIEW_CUSTOMER_REQUEST,
    VIEW_CUSTOMER_SUCCESS,
    VIEW_CUSTOMER_FAILURE,

    BANK_VERIFY_ACCOUNT,
    BANK_VERIFY_ACCOUNT_SUCCESS,
    BANK_VERIFY_ACCOUNT_FAILURE,

    CUSTOMER_UPLOAD_DOCUMENTS_REQUEST,
    CUSTOMER_UPLOAD_DOCUMENTS_SUCCESS,
    CUSTOMER_UPLOAD_DOCUMENTS_FAILURE,

    CUSTOMER_DOCUMENTS_REQUEST,
    CUSTOMER_DOCUMENTS_SUCCESS,
    CUSTOMER_DOCUMENTS_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        // all application documents
        case CUSTOMER_DOCUMENTS_REQUEST:
            return { ...state, loading: true };
        case CUSTOMER_DOCUMENTS_SUCCESS:
            return { ...state, loading: false, allDocuments: action.payload.allDocuments, planDocuments:action.payload.planDocuments};
        case CUSTOMER_DOCUMENTS_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_UPLOAD_DOCUMENTS_REQUEST:
            return { ...state, upl_loading: true };
        case CUSTOMER_UPLOAD_DOCUMENTS_SUCCESS:
            return { ...state, upl_loading: false };
        case CUSTOMER_UPLOAD_DOCUMENTS_FAILURE:
            return { ...state, upl_loading: false };

        case BANK_VERIFY_ACCOUNT:
            return { ...state, loading: true };
        case BANK_VERIFY_ACCOUNT_SUCCESS:
            return { ...state, loading: false, verifyAmt: action.payload };
        case BANK_VERIFY_ACCOUNT_FAILURE:
            return { ...state, loading: false };

        // view customer details start here
        case VIEW_CUSTOMER_REQUEST:
            return { ...state, loading: true};
        case VIEW_CUSTOMER_SUCCESS:
            return { ...state, loading: false, customerDetails:action.payload.customerDetails,appDetailsAddress:action.payload.appAddress,experianEmployment:action.payload.experianEmployment,experianAddress:action.payload.experianAddress,secDetails:action.payload.secDetails,bankDetails:action.payload.bankDetails,applicationDetails:action.payload.applicationDetails};
        case VIEW_CUSTOMER_FAILURE:
            return { ...state, loading: false};
        // view customer details end here
        case CUSTOMER_LIST:
            return { ...state, loading: true };
        case CUSTOMER_LIST_SUCCESS:
            return { ...state, loading: false, customerDetails:action.payload.result};
        case CUSTOMER_LIST_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
