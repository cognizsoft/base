/**
 * Auth User Reducers
 */
import {
    PROCEDURES_LIST,
    PROCEDURES_LIST_SUCCESS,
    PROCEDURES_LIST_FAILURE,
    PROCEDURES_INSERT,
    PROCEDURES_INSERT_SUCCESS,
    PROCEDURES_INSERT_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {
        case PROCEDURES_LIST:
            return { ...state, loading: true };
        case PROCEDURES_LIST_SUCCESS:
            return { ...state, loading: false, proceduresData: action.payload.result, specialityData: action.payload.speciality };
        case PROCEDURES_LIST_FAILURE:
            return { ...state, loading: false };
        case PROCEDURES_INSERT:
            return { ...state, loading: true };
        case PROCEDURES_INSERT_SUCCESS:
            const filterType = state.specialityData.filter(x => x.mdv_id == action.payload.speciality_type);
            console.log(filterType)
            let newProvedures = {
                procedure_id: action.payload.procedure_id,
                spec_id: action.payload.speciality_type,
                procedure_name: action.payload.name,
                procedure_code: action.payload.code,
                status: action.payload.data_status,
            }
            newProvedures.value = filterType[0].value;
            let provedures = state.proceduresData;
            provedures.reverse();
            provedures.push(newProvedures);
            provedures.reverse();
            return { ...state, loading: false, proceduresData: provedures };
        case PROCEDURES_INSERT_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
