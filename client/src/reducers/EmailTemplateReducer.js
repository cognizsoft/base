/**
 * Auth User Reducers
 */
import {
    EMAIL_TEMPLATE_LIST,
    EMAIL_TEMPLATE_LIST_SUCCESS,
    EMAIL_TEMPLATE_LIST_FAILURE,
    EMAIL_TEMPLATE_INSERT,
    EMAIL_TEMPLATE_INSERT_SUCCESS,
    EMAIL_TEMPLATE_INSERT_FAILURE,
    EMAIL_TEMPLATE_DELETE,
    EMAIL_TEMPLATE_DELETE_SUCCESS,
    EMAIL_TEMPLATE_DELETE_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE,

    EMAIL_TEMPLATE_VARIABLE,
    EMAIL_TEMPLATE_VARIABLE_SUCCESS,
    EMAIL_TEMPLATE_VARIABLE_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case EMAIL_TEMPLATE_VARIABLE:
            return { ...state, loading: true };

        case EMAIL_TEMPLATE_VARIABLE_SUCCESS:
            return { ...state, loading: false, emailTempVariable: action.payload };

        case EMAIL_TEMPLATE_VARIABLE_FAILURE:
            return { ...state, loading: false };

        case EMAIL_TEMPLATE_LIST:
            return { ...state, loading: true };

        case EMAIL_TEMPLATE_LIST_SUCCESS:
            return { ...state, loading: false, emailTemplist: action.payload.result, masterTempType: action.payload.template_type };

        case EMAIL_TEMPLATE_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, emailTempExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, emailTempExist: action.payload.exist, isEdit: action.payload.edit };

        case EMAIL_TEMPLATE_INSERT:
            return { ...state, loading: true };

        case EMAIL_TEMPLATE_INSERT_SUCCESS:
            let newEmailTemp = {
                id: action.payload.last_temp_id,
                template_type: action.payload.template_type,
                template_name: action.payload.template_name,
                template_subject: action.payload.template_subject,
                template_content: action.payload.template_content,
                status: action.payload.temp_status
            }
            let stateupdated = state.emailTemplist;
            stateupdated.reverse();
            stateupdated.push(newEmailTemp);
            stateupdated.reverse();
            
            return { ...state, loading: false, emailTemplist: stateupdated };

        case EMAIL_TEMPLATE_INSERT_FAILURE:
            return { ...state, loading: false };

        case EMAIL_TEMPLATE_DELETE:
            return {...state, loading: true}
        case EMAIL_TEMPLATE_DELETE_SUCCESS:
            const deleteEmailTemp = state.emailTemplist.filter(x => x.id == action.payload );
            let indexOfDeleteEmailTemp = state.emailTemplist.indexOf(deleteEmailTemp[0]);
            state.emailTemplist.splice(indexOfDeleteEmailTemp, 1);
            return { ...state, loading: false };
        
        case EMAIL_TEMPLATE_DELETE_FAILURE:
             return {...state, loading: false}

        default: return { ...state };
    }
}
