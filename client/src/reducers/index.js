/**
 * App Reducers
 */
import { combineReducers } from 'redux';
import settings from './settings';
import chatAppReducer from './ChatAppReducer';
import emailAppReducer from './EmailAppReducer';
import sidebarReducer from './SidebarReducer';
import todoAppReducer from './TodoAppReducer';
//import authUserReducer from './AuthUserReducer';
import UsersReducer from './UsersReducer';
import UsersTypeReducer from './UsersTypeReducer';
import UsersRoleReducer from './UsersRoleReducer';
import feedbacksReducer from './FeedbacksReducer';
import ecommerceReducer from './EcommerceReducer';
import PermissionReducer from './PermissionReducer';
import MasterValueReducer from './MasterValueReducer';
import MasterReducer from './MasterReducer';
import StatesReducer from './StatesReducer';
import RegionsReducer from './RegionsReducer';
import QuestionsReducer from './QuestionsReducer';
import SurchargeTypeReducer from './SurchargeTypeReducer';
import RiskFactorReducer from './RiskFactorReducer';
import LateFeeWaiverReducer from './LateFeeWaiverReducer';
import ScoreThresholdReducer from './ScoreThresholdReducer';
import SystemModuleReducer from './SystemModuleReducer';
import PaybackReducer from './PaybackReducer';
import InterestRateReducer from './InterestRateReducer';
import FinancialChargesReducer from './FinancialChargesReducer';
import CountryReducer from './CountryReducer';
import ProviderReducer from './ProviderReducer';
import CreditApplicationReducer from './CreditApplicationReducer';
import CustomerReducer from './CustomerReducer';
import ProviderInvoiceReducer from './ProviderInvoiceReducer';
import ProceduresReducer from './ProceduresReducer';
import PaymentPlanReducer from './PaymentPlanReducer';
import AdminInvoiceReducer from './AdminInvoiceReducer';
import AdminReportReducer from './AdminReportReducer';
import CustomerDashboardReducer from './CustomerDashboardReducer';
import ScoreRangeReducer from './ScoreRangeReducer';
import CareCouldReducer from './CareCouldReducer';
import DashboardReducer from './DashboardReducer';
import CustomerRegisterReducer from './CustomerRegisterReducer';
import TermMonthReducer from './TermMonthReducer';
import LoanAmountReducer from './LoanAmountReducer';
import AnswerReducer from './AnswerReducer';
import EmailTemplateReducer from './EmailTemplateReducer';
import CustomerSupportReducer from './CustomerSupportReducer';
import CosignerReducer from './CosignerReducer.js';

const reducers = combineReducers({
  settings,
  chatAppReducer,
  emailApp: emailAppReducer,
  sidebar: sidebarReducer,
  todoApp: todoAppReducer,
  authUser: UsersReducer,
  userType : UsersTypeReducer,
  userRole : UsersRoleReducer,
  feedback: feedbacksReducer,
  ecommerce: ecommerceReducer,
  permissionFilter : PermissionReducer,
  masterValue : MasterValueReducer,
  masterList : MasterReducer,
  statesList : StatesReducer,
  regionDetails: RegionsReducer,
  questionDetails: QuestionsReducer,
  surchargeTypeDetails: SurchargeTypeReducer,
  riskFactor: RiskFactorReducer,
  lateFeeWaiver: LateFeeWaiverReducer,
  scoreThreshold: ScoreThresholdReducer,
  systemModuleDetails: SystemModuleReducer,
  Payback: PaybackReducer,
  interestRate: InterestRateReducer,
  FinancialCharges: FinancialChargesReducer,
  countryList: CountryReducer,
  Provider: ProviderReducer,
  creditApplication: CreditApplicationReducer,
  Customer: CustomerReducer,
  providerInvoice:ProviderInvoiceReducer,
  Procedures:ProceduresReducer,
  PaymentPlanReducer:PaymentPlanReducer,
  adminInvoice:AdminInvoiceReducer,
  AdminReportReducer:AdminReportReducer,
  CustomerDashboardReducer:CustomerDashboardReducer,
  ScoreRangeReducer:ScoreRangeReducer,
  CareCouldReducer:CareCouldReducer,
  DashboardDetails:DashboardReducer,
  CustomerRegisterReducer:CustomerRegisterReducer,
  TermMonthReducer:TermMonthReducer,
  LoanAmountReducer:LoanAmountReducer,
  AnswerDetails:AnswerReducer,
  EmailTemplateReducer: EmailTemplateReducer,
  CustomerSupportReducer: CustomerSupportReducer,
  CosignerReducer: CosignerReducer,
});

export default reducers;
