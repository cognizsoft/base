/**
 * Auth User Reducers
 */
import {
    INTEREST_RATE_LIST_SUCCESS,
    INTEREST_RATE_LIST_FAILURE,
    INTEREST_RATE_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

    INTEREST_RATE_SCORE_EXIST,
    INTEREST_RATE_SCORE_EXIST_SUCCESS,
    INTEREST_RATE_SCORE_EXIST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

          /////////////////////////////////////////////////////////////////////
         /////////////////////////////START///////////////////////////////////
        /////////////////////////////////////////////////////////////////////

        case INTEREST_RATE_LIST_SUCCESS:
            return { ...state, loading: false, interest_score_list: action.payload };

        case INTEREST_RATE_LIST_FAILURE:
            return { ...state, loading: false };

        case INTEREST_RATE_INSERT_SUCCESS:
            let newUser = {
                id: action.payload.last_mfs_apr_id,
                score: action.payload.score,
                apr: action.payload.apr,
                risk_factor: action.payload.risk_factor,
                status: action.payload.int_score_status,
            }


            let users = state.interest_score_list;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, user_type_insert_id: action.payload };
        case INTEREST_RATE_SCORE_EXIST:
            return { ...state, loading: false, typeExist: 0 };
        case INTEREST_RATE_SCORE_EXIST_SUCCESS:
            return { ...state, loading: false, typeExist: action.payload.exist, isEdit: action.payload.edit };
        default: return { ...state };

          /////////////////////////////////////////////////////////////////////
         /////////////////////////////END/////////////////////////////////////
        /////////////////////////////////////////////////////////////////////

        /*case INTEREST_RATE_LIST_SUCCESS:
            return { ...state, loading: false, interest_rate: action.payload };

        case INTEREST_RATE_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, MFSScoreList: action.payload.mfs_score,interestRateMList:action.payload.interest_rate,loanAmountList:action.payload.loan_amount,paymentTermMonthList:action.payload.payment_term_month };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case INTEREST_RATE_INSERT_SUCCESS:
            const scroeType = state.MFSScoreList.filter(x => x.mdv_id == action.payload.score_range );
            const rateType = state.interestRateMList.filter(x => x.mdv_id == action.payload.rate_range );
            const loanType = state.loanAmountList.filter(x => x.mdv_id == action.payload.loan_range );
            const monthType = state.paymentTermMonthList.filter(x => x.mdv_id == action.payload.term_range );
            
            let newInterestRate = {
                id: action.payload.last_value_id,
                effective_date: action.payload.effective_date,
                status: action.payload.score_status,
             }
            newInterestRate.score_range = scroeType[0].value;
            newInterestRate.score_range_id = scroeType[0].mdv_id;
            newInterestRate.rate_range = rateType[0].value;
            newInterestRate.interest_rate_id = rateType[0].mdv_id;
            newInterestRate.loan_range = loanType[0].value;
            newInterestRate.loan_amount_id = loanType[0].mdv_id;
            newInterestRate.term_range = monthType[0].value;
            newInterestRate.payment_term_month_id = monthType[0].mdv_id;
            let users = state.interest_rate;
            users.reverse();
            users.push(newInterestRate); 
            users.reverse();
            
            return { ...state, loading: false };

        default: return { ...state };*/
    }
}
