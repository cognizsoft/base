/**
 * Auth User Reducers
 */
import {
        ADMIN_OPEN_INVOICE_REQUEST,
        ADMIN_OPEN_INVOICE_SUCCESS,
        ADMIN_OPEN_INVOICE_FAILURE,
        ADMIN_VIEW_INVOICE_REQUEST,
        ADMIN_VIEW_INVOICE_SUCCESS,
        ADMIN_VIEW_INVOICE_FAILURE,
        ADMIN_DELETE_INVOICE_REQUEST,
        ADMIN_DELETE_INVOICE_SUCCESS,
        ADMIN_DELETE_INVOICE_FAILURE,
        ADMIN_LIST_INVOICE_REQUEST,
        ADMIN_LIST_INVOICE_SUCCESS,
        ADMIN_LIST_INVOICE_FAILURE,
        ADMIN_ADD_INVOICE_REQUEST,
        ADMIN_ADD_INVOICE_SUCCESS,
        ADMIN_ADD_INVOICE_FAILURE,
        ADMIN_CANCEL_INVOICE_REQUEST,
        ADMIN_CANCEL_INVOICE_SUCCESS,
        ADMIN_CANCEL_INVOICE_FAILURE,
        CONFIRM_INVOICE_REQUEST,
        CONFIRM_INVOICE_SUCCESS,
        CONFIRM_INVOICE_FAILURE,
        ADMIN_CLOSE_INVOICE_REQUEST,
        ADMIN_CLOSE_INVOICE_SUCCESS,
        ADMIN_CLOSE_INVOICE_FAILURE,
        ADMIN_CANCEL_CONINVOICE_REQUEST,
        ADMIN_CANCEL_CONINVOICE_SUCCESS,
        ADMIN_CANCEL_CONINVOICE_FAILURE,
        ADMIN_NOTE_INVOICE_REQUEST,
        ADMIN_NOTE_INVOICE_SUCCESS,
        ADMIN_NOTE_INVOICE_FAILURE,
        VIEW_INVOICE_REQUEST,
        VIEW_INVOICE_SUCCESS,
        VIEW_INVOICE_FAILURE,
        APPROVE_INVOICE_REQUEST,
        APPROVE_INVOICE_SUCCESS,
        APPROVE_INVOICE_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
        loading: false,
        stateList: [],
        stateRegion: [],
        rediretURL: 0,
        rediretPlanURL: 0
};

export default (state = INIT_STATE, action) => {
        switch (action.type) {
                // approve invoice start here
                case APPROVE_INVOICE_REQUEST:
                        return { ...state, loading: true, approveRedirect:false }
                case APPROVE_INVOICE_SUCCESS:
                        return { ...state, loading: false, approveRedirect:true}
                case APPROVE_INVOICE_FAILURE:
                        return { ...state, loading: false, approveRedirect:false }
                // approve invoice end here
                // view invoice start here
                case VIEW_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case VIEW_INVOICE_SUCCESS:
                        var amount = action.payload.result.reduce(function (accumulator, currentValue, currentindex) {
                                if (currentindex == 0) {
                                        accumulator['total_amount'] = currentValue.loan_amount;
                                        accumulator['discount_amount'] = currentValue.hps_discount_amt;
                                } else {
                                        accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                                        accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
                                }
                                return accumulator
                        }, []);
                        return { ...state, loading: false, invoicePreviewList: action.payload.result, providerDetails: action.payload.provider, totalAmount: amount }
                case VIEW_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // view invoice end here
                // invoice note start here
                case ADMIN_NOTE_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_NOTE_INVOICE_SUCCESS:
                        return { ...state, loading: false, noteDetails: action.payload.result }
                case ADMIN_NOTE_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice note end here
                // invoice cancel start here
                case ADMIN_CANCEL_CONINVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_CANCEL_CONINVOICE_SUCCESS:
                        return { ...state, loading: false, closeInvoiceList: action.payload.result }
                case ADMIN_CANCEL_CONINVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice cancel end here
                // invoice list start here
                case ADMIN_CLOSE_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_CLOSE_INVOICE_SUCCESS:
                        return { ...state, loading: false, closeInvoiceList: action.payload.result }
                case ADMIN_CLOSE_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice list end here
                // invoice confirm start here
                case CONFIRM_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case CONFIRM_INVOICE_SUCCESS:
                        return { ...state, loading: false, openInvoiceList: action.payload.result }
                case CONFIRM_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice confirm end here
                // invoice cancel start here
                case ADMIN_CANCEL_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_CANCEL_INVOICE_SUCCESS:
                        return { ...state, loading: false, openInvoiceList: action.payload.result }
                case ADMIN_CANCEL_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice cancel end here
                // invoice add start here
                case ADMIN_ADD_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_ADD_INVOICE_SUCCESS:
                        return { ...state, loading: false, openInvoiceList: action.payload.result }
                case ADMIN_ADD_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice add end here
                // invoice list start here
                case ADMIN_LIST_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_LIST_INVOICE_SUCCESS:
                        return { ...state, loading: false, listInvoiceView: action.payload.result, currentInvoiceID: action.payload.invoice_id }
                case ADMIN_LIST_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice list end here
                // invoice delete start here
                case ADMIN_DELETE_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_DELETE_INVOICE_SUCCESS:
                        var amount = action.payload.result.reduce(function (accumulator, currentValue, currentindex) {
                                if (currentindex == 0) {
                                        accumulator['total_amount'] = currentValue.loan_amount;
                                        accumulator['discount_amount'] = currentValue.hps_discount_amt;
                                } else {
                                        accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                                        accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
                                }
                                return accumulator
                        }, []);
                        return { ...state, loading: false, invoicePreviewList: action.payload.result, providerDetails: action.payload.provider, totalAmount: amount }
                case ADMIN_DELETE_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice delete end here
                // invoice view start here
                case ADMIN_VIEW_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case ADMIN_VIEW_INVOICE_SUCCESS:
                        return { ...state, loading: false, openInvoiceView: action.payload.result }
                case ADMIN_VIEW_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice view end here
                // invoice list start here
                case ADMIN_OPEN_INVOICE_REQUEST:
                        return { ...state, loading: true, approveRedirect:false }
                case ADMIN_OPEN_INVOICE_SUCCESS:
                        return { ...state, loading: false, openInvoiceList: action.payload.result,providerBankDetails: action.payload.providerBank, iouDetails:action.payload.iouDetails }
                case ADMIN_OPEN_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice list end here

                default: return { ...state };
        }
}
