/**
 * Auth User Reducers
 */
import {
    SYSTEM_MODULE_LIST,
    SYSTEM_MODULE_LIST_SUCCESS,
    SYSTEM_MODULE_LIST_FAILURE,
    SYSTEM_MODULE_INSERT,
    SYSTEM_MODULE_INSERT_SUCCESS,
    SYSTEM_MODULE_INSERT_FAILURE,
    SYSTEM_MODULE_DELETE,
    SYSTEM_MODULE_DELETE_SUCCESS,
    SYSTEM_MODULE_DELETE_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case SYSTEM_MODULE_LIST:
            return { ...state, loading: true };

        case SYSTEM_MODULE_LIST_SUCCESS:
            return { ...state, loading: false, systemmodulelist: action.payload };

        case SYSTEM_MODULE_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, moduleExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, moduleExist: action.payload.exist, isEdit: action.payload.edit };
            

        case SYSTEM_MODULE_INSERT:
            return { ...state, loading: true };

        case SYSTEM_MODULE_INSERT_SUCCESS:
            let newSystemModule = {
                sys_mod_id: action.payload.last_system_module_id,
                description: action.payload.description,
                status: action.payload.system_module_status
            }
            let stateupdated = state.systemmodulelist;
            stateupdated.reverse();
            stateupdated.push(newSystemModule);
            stateupdated.reverse();
            
            return { ...state, loading: false, systemmodulelist: stateupdated };

        case SYSTEM_MODULE_INSERT_FAILURE:
            return { ...state, loading: false };

        
        default: return { ...state };
    }
}
