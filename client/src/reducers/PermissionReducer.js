/**
 * Auth User Reducers
 */
import {
    PERMISSION_REQUEST,
    PERMISSION_SUCCESS,
    PERMISSION_FAILURE,
    PERMISSION_MODULE_REQUEST,
    PERMISSION_MODULE_SUCCESS,
    PERMISSION_MODULE_FAILURE,
    PERMISSION_MODULE_RESET,
    USER_ROLE,
    USER_ROLE_SUCCESS,
    USER_ROLE_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case PERMISSION_REQUEST:
            return { ...state, loading: true };

        case PERMISSION_SUCCESS:
            return { ...state, loading: false, usertypes: action.payload.UserType, userrole: action.payload.userRole, permissiondata:action.payload.perresult };

        case PERMISSION_FAILURE:
            return { ...state, loading: false };

        case PERMISSION_MODULE_REQUEST:
            return { ...state, loading: true };

        case PERMISSION_MODULE_SUCCESS:
            return { ...state, loading: false, permissiondata: action.payload };

        case PERMISSION_MODULE_FAILURE:
            return { ...state, loading: false };

        case PERMISSION_MODULE_RESET:
            if (action.payload != undefined) {
                return { ...state, loading: false, permissiondata: undefined };
            }else{
                return { ...state, loading: false, permissiondata: undefined, userrole:''};
            }
            
        case USER_ROLE:
            return { ...state, loading: true, userrole:'' };
        case USER_ROLE_SUCCESS:
            return { ...state, loading: false, userrole: action.payload };
        case USER_ROLE_FAILURE:
            return { ...state, loading: false, userrole:'' };
        default: return { ...state };
    }
}
