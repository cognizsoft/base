/**
 * Auth User Reducers
 */
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    USER_LIST_SUCCESS,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,
    USER_INSERT,
    USER_INSERT_SUCCESS,
    USER_INSERT_FAILURE,
    USER_UPDATE,
    USER_UPDATE_SUCCESS,
    USER_UPDATE_FAILURE,
    USER_DELETE,
    USER_DELETE_SUCCESS,
    USER_DELETE_FAILURE,
    USER_PASSWORD,
    USER_PASSWORD_SUCCESS,
    USER_PASSWORD_FAILURE,
    USER_ROLE,
    USER_ROLE_SUCCESS,
    USER_ROLE_FAILURE,
    PASSWORD_TOKEN_ACTION_SUCCESS,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE,
    FORGET_PASSWORD_SUCCESS,
    FORGET_PASSWORD_FAILURE,
    CLEAR_PASSWORD_STATE,
    PASSWORD_TOKEN_ACTION_FAILURE,

    USER_UNLOCK,
    USER_UNLOCK_SUCCESS,
    USER_UNLOCK_FAILURE,

    LOCK_USER_DETAILS,
    LOCK_USER_DETAILS_SUCCESS,
    LOCK_USER_DETAILS_FAILURE,

    EXIST_UPDATE_CO,
    EXIST_UPDATE_SUCCESS_CO,
    EXIST_UPDATE_FAILURE_CO
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    user: localStorage.getItem('user'),
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOCK_USER_DETAILS_SUCCESS:
            //console.log(action.payload)
            return { ...state, loading: false, userLockDetail: action.payload.result };
        case CLEAR_PASSWORD_STATE:
            return { ...state, redirectLink: undefined };
        // reset rest link
        case FORGET_PASSWORD_SUCCESS:
            return { ...state, redirectLink: 1 };
        case FORGET_PASSWORD_FAILURE:
            return { ...state, redirectLink: 0 };
        // update password
        case RESET_PASSWORD_SUCCESS:
            return { ...state, redirectReset: 1 };
        case RESET_PASSWORD_FAILURE:
            return { ...state, redirectReset: 2 };
        // for password rest token check
        case PASSWORD_TOKEN_ACTION_FAILURE:
            return { ...state, passwordRestID: 0 };
        case PASSWORD_TOKEN_ACTION_SUCCESS:
            return { ...state, passwordRestID: action.payload };
        case LOGIN_USER:
            return { ...state, loading: true };

        case LOGIN_USER_SUCCESS:
            return { ...state, loading: false, user: action.payload };

        case LOGIN_USER_FAILURE:
            return { ...state, loading: false };

        case LOGOUT_USER:
            return { ...state, user: null };
        case USER_ROLE:
            return { ...state, loading: true, userRole: '', isEdit: undefined };
        case USER_ROLE_SUCCESS:
            return { ...state, loading: false, userRole: action.payload };
        case USER_ROLE_FAILURE:
            return { ...state, loading: false, userRole: '' };
        case USER_LIST_SUCCESS:
            return { ...state, loading: false, userList: action.payload.result, userType: action.payload.userType, providerList: action.payload.provider, userQuestions: action.payload.user_questions };

        case EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0 };
        case EXIST_UPDATE_SUCCESS:
            if (action.payload.email === 1) {
                return { ...state, loading: false, emailExist: action.payload.exist, isEdit: action.payload.edit };
            } else {
                return { ...state, loading: false, nameExist: action.payload.exist, isEdit: action.payload.edit };
            }

        case EXIST_UPDATE_CO:
            return { ...state, loading: false, nameExist: 0 };
        case EXIST_UPDATE_SUCCESS_CO:
            if (action.payload.email === 1) {
                return { ...state, loading: false, emailExistCO: action.payload.exist, isEditCO: action.payload.edit };
            } else {
                return { ...state, loading: false, nameExistCO: action.payload.exist, isEditCO: action.payload.edit };
            }


        case USER_INSERT_SUCCESS:
            const filterType = state.userType.filter(x => x.mdv_id == action.payload.mdv_user_type_id);
            const filterRole = state.userRole.filter(x => x.mdv_id == action.payload.mdv_role_id);
            const filterProvider = state.providerList.filter(x => x.provider_id == action.payload.provider_id);

            let newUser = {
                user_id: action.payload.user_id,
                username: action.payload.username,
                email_id: action.payload.email_id,
                status: action.payload.userstatus,
                mdv_user_type_id: action.payload.mdv_user_type_id,
                mdv_role_id: action.payload.mdv_role_id,
                f_name: action.payload.f_name,
                m_name: action.payload.m_name,
                l_name: action.payload.l_name,
                phone: action.payload.phone,
                provider_id: action.payload.provider_id,
            }
            newUser.user_type = filterType[0].value;
            newUser.user_role = filterRole[0].value;
            newUser.name = (action.payload.provider_id) ? filterProvider[0].name : '';
            let users = state.userList;
            users.reverse();
            users.push(newUser);
            users.reverse();

            return { ...state, loading: false, userList: users };

        case USER_INSERT:
            return { ...state, loading: true };
        case USER_INSERT_FAILURE:
            return { ...state, loading: false };
        case USER_UPDATE_SUCCESS:
            return { ...state, loading: false };

        case USER_UPDATE_FAILURE:
            return { ...state, loading: false };

        case USER_UPDATE:
            return { ...state, loading: true };

        case USER_PASSWORD_SUCCESS:
            return { ...state, loading: false };

        case USER_PASSWORD_FAILURE:
            return { ...state, loading: false };

        case USER_PASSWORD:
            return { ...state, loading: true };

        case USER_DELETE_SUCCESS:
            const deleteUser = state.userList.filter(x => x.user_id == action.payload);
            let indexOfDeleteUser = state.userList.indexOf(deleteUser[0]);
            state.userList.splice(indexOfDeleteUser, 1);
            return { ...state, loading: false };
        case USER_DELETE:
            return { ...state, loading: true }
        case USER_DELETE_FAILURE:
            return { ...state, loading: false }

        case USER_UNLOCK:
            return { ...state, loading: true }
        case USER_UNLOCK_SUCCESS:
            const newList = state.userList.filter(x => x.user_id == action.payload.user_id);
            //console.log(newList)
            newList[0].lock_account = 0;
            let indexOfUpdateUser = '';
            for (let i = 0; i < state.userList.length; i++) {
                const user = state.userList[i];
                if (user.user_id === newList[0].user_id) {
                    indexOfUpdateUser = i
                }
            }

            state.userList[indexOfUpdateUser] = newList[0];
            return { ...state, loading: false, unlockStatus: action.payload };
        case USER_UNLOCK_FAILURE:
            return { ...state, loading: false }
        default: return { ...state };
    }
}
