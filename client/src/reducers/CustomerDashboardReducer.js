/**
 * Auth User Reducers
 */
import {
    CUSTOMER_ACTIVE_PLAN,
    CUSTOMER_ACTIVE_PLAN_SUCCESS,
    CUSTOMER_ACTIVE_PLAN_FAILURE,

    CUSTOMER_INSERT_PAYMENT,
    CUSTOMER_INSERT_PAYMENT_SUCCESS,
    CUSTOMER_INSERT_PAYMENT_FAILURE,

    ALL_PLAN_DETAILS,
    ALL_PLAN_DETAILS_SUCCESS,
    ALL_PLAN_DETAILS_FAILURE,

    CUSTOMER_EDIT_PROFILE_DETAILS,
    CUSTOMER_EDIT_PROFILE_DETAILS_SUCCESS,
    CUSTOMER_EDIT_PROFILE_DETAILS_FAILURE,

    CUSTOMER_PROFILE_ADD_MORE_LIST,
    REMOVE_CUSTOMER_PROFILE_ADD_MORE_LIST,

    CUSTOMER_PROFILE_STATES_LIST,
    CUSTOMER_PROFILE_STATES_LIST_SUCCESS,
    CUSTOMER_PROFILE_STATES_LIST_FAILURE,

    CUSTOMER_PROFILE_UPDATE_REQUEST,
    CUSTOMER_PROFILE_UPDATE_SUCCESS,
    CUSTOMER_PROFILE_UPDATE_FAILURE,

    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE,

    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,

    CUSTOMER_BANK_VERIFY_ACCOUNT,
    CUSTOMER_BANK_VERIFY_ACCOUNT_SUCCESS,
    CUSTOMER_BANK_VERIFY_ACCOUNT_FAILURE,

} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    stateList: [],
    stateRegion: [],
    rediretURL: 0,
    rediretPlanURL: 0
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case CUSTOMER_BANK_VERIFY_ACCOUNT:
            return { ...state, loading: true };
        case CUSTOMER_BANK_VERIFY_ACCOUNT_SUCCESS:
            return { ...state, loading: false, verifyAmt: action.payload };
        case CUSTOMER_BANK_VERIFY_ACCOUNT_FAILURE:
            return { ...state, loading: false };

        // customr week month invoice report start here
        case CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST:
            return { ...state, loading: true }
        case CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS:
            return { ...state, loading: false, customerInvoiceList: action.payload.result };
        case CUSTOMER_DASHBOARD_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_REQUEST:
            return { ...state, loading: true }
        case CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_SUCCESS:
            return { ...state, loading: false };
        case CUSTOMER_DASHBOARD_REPORT_DOWNLOAD_LIST_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_PROFILE_UPDATE_REQUEST:
            return { ...state, loading: true, rediretURL:0 };
        case CUSTOMER_PROFILE_UPDATE_SUCCESS:
            //var customer_redirect_to_profile = 1;

            return { ...state, loading: false, rediretURL: 1, applicationID: action.payload.applicationId };


        case CUSTOMER_PROFILE_UPDATE_REQUEST:
            return { ...state, loading: true };
        case CUSTOMER_PROFILE_UPDATE_SUCCESS:
            //var customer_redirect_to_profile = 1;

            return { ...state, loading: false, rediretURL: 1, applicationID: action.payload.applicationId };

        case CUSTOMER_PROFILE_UPDATE_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_PROFILE_STATES_LIST:
            return { ...state, loading: true };

        case CUSTOMER_PROFILE_STATES_LIST_SUCCESS:
            if (action.payload.idx === undefined) {
                return { ...state, loading: false, blillingStateType: action.payload.result };
            }
            var newState = state.stateList;
            newState[action.payload.idx] = action.payload.result;
            return { ...state, loading: false, stateType: newState };

        case CUSTOMER_PROFILE_STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_PROFILE_ADD_MORE_LIST:
            var newState = state.stateList;
            newState[newState.length] = '';
            var newRegion = state.stateRegion;
            newRegion[newState.length] = '';
            return { ...state, loading: false, stateType: newState, regionType: newRegion };
        case REMOVE_CUSTOMER_PROFILE_ADD_MORE_LIST:
            var newState = state.stateList;
            newState = newState.filter((s, sidx) => action.payload !== sidx)
            state.stateList = newState;
            var newRegion = state.stateRegion;
            newRegion = newRegion.filter((s, sidx) => action.payload !== sidx)
            state.stateRegion = newRegion;
            //newState[newState.length] = '';
            return { ...state, loading: false, stateType: newState, regionType: newRegion };
        // search code start hare
        case CUSTOMER_EDIT_PROFILE_DETAILS:
            return { ...state, loading: true, rediretURL: 0, appSearchDetails: '', appSearchDetailsAddress: '', appaddError: '' };
        case CUSTOMER_EDIT_PROFILE_DETAILS_SUCCESS:
            return { ...state, loading: false, rediretURL: 0, appSearchDetails: action.payload.customerDetails, appSearchDetailsAddress: action.payload.customerAddress, appaddError: action.payload.addError, bankDetails: action.payload.bankDetails, bankError: action.payload.bankError };
        case CUSTOMER_EDIT_PROFILE_DETAILS_FAILURE:
            return { ...state, loading: false };

        // view customer details start here
        case CUSTOMER_INSERT_PAYMENT:
            return { ...state, loading: true }

        case CUSTOMER_INSERT_PAYMENT_SUCCESS:
            //console.log(action)
            var redirectURI;
            if (action.payload.redirectReport == 1) {
                redirectURI = 1;
            } else {
                redirectURI = 0;
            }
            return { ...state, loading: false, redirectURL: redirectURI, single_installment_payment: '', payment_billing_late_fee: '', payment_invoice_detail: '' };

        case CUSTOMER_INSERT_PAYMENT_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_ACTIVE_PLAN:
            return { ...state, loading: true, redirectURL: 0 };
        case CUSTOMER_ACTIVE_PLAN_SUCCESS:
            /*if(action.payload.application_plans.length == 0) {
                action.payload.application_plans['plan_updated'] = 0
            }
            console.log('action.payload.application_plans')
            console.log(action.payload.application_plans)*/

            return { ...state, loading: false, redirectURL: 0, application_details: action.payload.application_details, application_plans: action.payload.application_plans, invoiceDetails: action.payload.invoiceDetails, amountDetails: action.payload.amountDetails, invoicePlan: action.payload.invoicePlan, app_id: action.payload.app_id };
        //return { ...state, loading: false, redirectURL: 0, application_details: action.payload.application_details, application_plans: action.payload.application_plans, pp_details: action.payload.pp_details, app_id: action.payload.app_id };

        case CUSTOMER_ACTIVE_PLAN_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
