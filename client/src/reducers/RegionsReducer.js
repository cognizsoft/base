/**
 * Auth User Reducers
 */
import {
    CITY_LIST,
    CITY_LIST_SUCCESS,
    CITY_LIST_FAILURE,
    CITY_INSERT,
    CITY_INSERT_SUCCESS,
    CITY_INSERT_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,

    CITY_SELECTED_COUNTRY_STATE,
    CITY_SELECTED_COUNTRY_STATE_SUCCESS,
    CITY_SELECTED_COUNTRY_STATE_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) { 

        case CITY_SELECTED_COUNTRY_STATE:
            return { ...state, loading: true, statelist: '' };

        case CITY_SELECTED_COUNTRY_STATE_SUCCESS:
            return { ...state, loading: false, statelist: action.payload.country_states };

        case CITY_SELECTED_COUNTRY_STATE_FAILURE:
            return { ...state, loading: false, statelist: '' };

        case CITY_LIST:
            return { ...state, loading: true };

        case CITY_LIST_SUCCESS:
            return { ...state, loading: false, citylist: action.payload.result, statelist: action.payload.state, countrylist: action.payload.countries };

        case CITY_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, cityExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, cityExist: action.payload.exist, isEdit: action.payload.edit };
            
        
        case CITY_INSERT:
            return { ...state, loading: true };

        case CITY_INSERT_SUCCESS:
            const filterstate = state.statelist.filter(x => x.state_id == action.payload.state_id );
            const filtercountry = state.countrylist.filter(x => x.id == action.payload.country_id );
            let newCity = {
                country_id: action.payload.country_id,
                state_id: action.payload.state_id,
                city_id:action.payload.last_city_id,
                name: action.payload.name,
                serving: action.payload.serving,
                status: action.payload.city_status
            }
            let cityupdated = state.citylist;
            newCity.state_name = filterstate[0].name;
            newCity.country_name = filtercountry[0].name;
            cityupdated.reverse();
            cityupdated.push(newCity);
            cityupdated.reverse();
            return { ...state, loading: false, citylist: cityupdated };

        case CITY_INSERT_FAILURE:
            return { ...state, loading: false };
        
        default: return { ...state };
    }
}
