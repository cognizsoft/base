/**
 * Auth User Reducers
 */
import {
    providerTypeMasterDataValueList, ProviderDetails, updateProvider, providerCountry, getProviderStates, getProviderRegion, getProviderOption
} from 'Actions';

import {
    PROVIDER_LIST_SUCCESS,
    PROVIDER_LIST_FAILURE,

    PROVIDER_INSERT,
    PROVIDER_INSERT_SUCCESS,
    PROVIDER_INSERT_FAILURE,

    PROVIDER_DELETE,
    PROVIDER_DELETE_SUCCESS,
    PROVIDER_DELETE_FAILURE,

    PROVIDER_DETAILS,
    PROVIDER_DETAILS_SUCCESS,
    PROVIDER_DETAILS_FAILURE,

    PROVIDER_UPDATE,
    PROVIDER_UPDATE_SUCCESS,
    PROVIDER_UPDATE_FAILURE,

    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,

    PROVIDER_COUNTRY_SUCCESS,
    PROVIDER_COUNTRY_FAILURE,

    STATES_LIST,
    STATES_LIST_SUCCESS,
    STATES_LIST_FAILURE,

    REGION_LIST,
    REGION_LIST_SUCCESS,
    REGION_LIST_FAILURE,

    ADD_MORE_LIST,
    REMOVE_ADD_MORE_LIST,

    PROVIDER_OPTION_LIST,
    PROVIDER_OPTION_SUCCESS,
    PROVIDER_OPTION_FAILURE,

    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS,

    PROVIDER_SPL_PROCEDURE,
    PROVIDER_SPL_PROCEDURE_SUCCESS,
    PROVIDER_SPL_PROCEDURE_FAILURE,
    ADD_MORE_SPEC_LIST,

    PROVIDER_CITY_SUGGESTION,
    PROVIDER_CITY_SUGGESTION_SUCCESS,
    PROVIDER_CITY_SUGGESTION_FAILURE,

    PROVIDER_UPLOAD_DOCUMENTS_REQUEST,
    PROVIDER_UPLOAD_DOCUMENTS_SUCCESS,
    PROVIDER_UPLOAD_DOCUMENTS_FAILURE,

    PROVIDER_DOCUMENTS_REQUEST,
    PROVIDER_DOCUMENTS_SUCCESS,
    PROVIDER_DOCUMENTS_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    stateList: [],
    stateRegion: [],
    stateList2: [],
    stateRegion2: [],
    splProcedure2: [],
    redirectURL: 0,
};

export default (state = INIT_STATE, action) => {
    
    switch (action.type) {

        // all provider documents
        case PROVIDER_DOCUMENTS_REQUEST:
            return { ...state, loading: true };
        case PROVIDER_DOCUMENTS_SUCCESS:
            return { ...state, loading: false, allDocuments: action.payload.allDocuments};
        case PROVIDER_DOCUMENTS_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_UPLOAD_DOCUMENTS_REQUEST:
            return { ...state, upl_loading: true };
        case PROVIDER_UPLOAD_DOCUMENTS_SUCCESS:
            return { ...state, upl_loading: false };
        case PROVIDER_UPLOAD_DOCUMENTS_FAILURE:
            return { ...state, upl_loading: false };

        case PROVIDER_CITY_SUGGESTION:
            return { ...state, loading: false, nameExist: 0 };
        case PROVIDER_CITY_SUGGESTION_SUCCESS:
            return { ...state, loading: false, city_suggestion: action.payload.suggestions };

        case ADD_MORE_SPEC_LIST:
            var newsplProcedure = state.splProcedure2;
            newsplProcedure[newsplProcedure.length] = '';
            state.splProcedure2 = newsplProcedure;
            return { ...state, loading: false, splProcedure: newsplProcedure }

        case PROVIDER_SPL_PROCEDURE:
            return { ...state, loading: false }

        case PROVIDER_SPL_PROCEDURE_SUCCESS:
            var newsplProcedure = state.splProcedure2;
            
            newsplProcedure[action.payload.idx] = action.payload.result;
            
            state.splProcedure2 = newsplProcedure;
            //console.log()
            return { ...state, loading: false, splProcedure: newsplProcedure }

        case PROVIDER_SPL_PROCEDURE_FAILURE:
            return { ...state, loading: false }

        case EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0 };
        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, nameExist: action.payload.exist, isEdit: action.payload.edit };


        case PROVIDER_OPTION_SUCCESS:
            //console.log(action.payload)
            if(action.payload.chk == 0) {
                var newsplProcedure = state.splProcedure2;
                newsplProcedure[0] = [];
                state.splProcedure2 = newsplProcedure;
                return { ...state, loading: false, redirectURL: 0, provider_option_bank: action.payload.bank_account_type, provider_payment_term: action.payload.pvd_payment_term, provider_loan_term_months: action.payload.pvd_loan_term_month, provider_option_spl: action.payload.speciality, provider_option_discount_type: action.payload.discount_type, provider_option_fee_type: action.payload.fee_type, provider_option_document_type: action.payload.document_type, provider_option_user_role: action.payload.user_role, splProcedure: newsplProcedure };
            } else {
                return { ...state, loading: false, redirectURL: 0, provider_option_bank: action.payload.bank_account_type, provider_payment_term: action.payload.pvd_payment_term, provider_loan_term_months: action.payload.pvd_loan_term_month, provider_option_spl: action.payload.speciality, provider_option_discount_type: action.payload.discount_type, provider_option_fee_type: action.payload.fee_type, provider_option_document_type: action.payload.document_type, provider_option_user_role: action.payload.user_role};
            }
            

        case PROVIDER_OPTION_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_LIST_SUCCESS:
            //console.log(action.payload)
            return { ...state, loading: true, redirectURL: 0, provider_list: action.payload,
                provider_details: '', provider_location: '', provider_bank: '', provider_specility: '', provider_specility_doctors: '', provider_user: '', provider_additional_discount: '', provider_additional_fee: '', provider_document: '', provider_agreement: '', provider_term: '', provider_details_loc_add_err: '', provider_bank_add_err: '', provider_spl_add_err: '', provider_addi_dis_add_err: '', provider_addi_fee_add_err: '', provider_doc_add_err: ''
             };

        case PROVIDER_LIST_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_DETAILS:
            
            return { ...state, aloading: true, loading: true }

        case PROVIDER_DETAILS_SUCCESS:
            var newsplProcedure = state.splProcedure2;
            newsplProcedure[0] = '';
            //var newState = state.stateList;
            
            return { ...state, aloading: false, loading: false, redirectURL: 0, provider_details: action.payload.provider_detail, provider_location: action.payload.provider_location, provider_bank: action.payload.provider_bank, provider_specility: action.payload.provider_specility, provider_specility_doctors: action.payload.provider_specility_doctors, provider_user: action.payload.provider_user, provider_additional_discount: action.payload.provider_additional_discount, provider_additional_fee: action.payload.provider_additional_fee, provider_document: action.payload.provider_document, provider_agreement: action.payload.provider_agreement, provider_term: action.payload.provider_term, provider_details_loc_add_err: action.payload.provider_loc_add_err, provider_bank_add_err: action.payload.provider_bank_add_err, provider_spl_add_err: action.payload.provider_spl_add_err, provider_addi_dis_add_err: action.payload.provider_addi_dis_add_err, provider_addi_fee_add_err: action.payload.provider_addi_fee_add_err, provider_doc_add_err: action.payload.provider_doc_add_err, newsplProcedure };

        case PROVIDER_DETAILS_FAILURE:
            
            return { ...state, aloading: false, loading: false };

        case PROVIDER_UPDATE:
            return { ...state, aloading: true };

        case PROVIDER_UPDATE_SUCCESS:
            //console.log(action.payload)
            return { ...state, loading: true, aloading: false, redirectURL: 1, provider_details: '', provider_location: '', provider_bank: '', provider_specility: '', provider_user: '', provider_additional_discount: '', provider_additional_fee: '', provider_document: '', provider_additional_discount: '' };

        case PROVIDER_UPDATE_FAILURE:
            return { ...state, loading: false, aloading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, provider_type_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_COUNTRY_SUCCESS:
            //console.log('action.payload')
            //console.log(action.payload)
            //console.log('action.payload')

            var newState = state.stateList;
            newState[0] = '';
            var newState2 = state.stateList2;
            newState2[0] = '';
            var newRegion = state.stateRegion;
            newRegion[0] = '';
            var newRegion2 = state.stateRegion2;
            newRegion2[0] = '';
            return { ...state, loading: false, redirectURL: 0, provider_country: action.payload, stateType: newState, regionType: newRegion, stateType2: newState2, regionType2: newRegion2 };

        case PROVIDER_COUNTRY_FAILURE:
            return { ...state, loading: false };

        case STATES_LIST:
            return { ...state, loading: true };

        case STATES_LIST_SUCCESS:
            //console.log('action.payload')
            //console.log(action.payload)
            if (action.payload.st == 0) {
                var newState = state.stateList;
                newState[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, redirectURL: 0, stateType: newState };
            } else {
                var newState2 = state.stateList2;
                newState2[action.payload.idx] = action.payload.result;
                return { ...state, loading: false, redirectURL: 0, stateType2: newState2 };
            }


        //return { ...state, loading: false, stateType: newState, stateType2: newState2 };

        case STATES_LIST_FAILURE:
            return { ...state, loading: false };

        case REGION_LIST:
            return { ...state, loading: true };
        case REGION_LIST_SUCCESS:
            if (action.payload.rg == 0) {
                var newRegion = state.stateRegion;
                newRegion[action.payload.idx] = action.payload.result;
                // console.log('newRegion')
                // console.log(newRegion)
                return { ...state, loading: false, redirectURL: 0, regionType: newRegion };
            } else {
                var newRegion2 = state.stateRegion2;
                newRegion2[action.payload.idx] = action.payload.result;
                // console.log('newRegion2')
                // console.log(action.payload.result)
                // console.log(newRegion2)
                return { ...state, loading: false, redirectURL: 0, regionType2: newRegion2 };
            }

        //return { ...state, loading: false, regionType: newRegion, regionType2: newRegion2 };

        case REGION_LIST_FAILURE:
            return { ...state, loading: false };

        case ADD_MORE_LIST:
            //var newsplProcedure = state.splProcedure;
            //newsplProcedure[newsplProcedure.length] = '';

            var newState = state.stateList;
            newState[newState.length] = '';
            var newState2 = state.stateList2;
            newState2[newState2.length] = '';

            var newRegion = state.stateRegion;
            newRegion[newState.length] = '';
            var newRegion2 = state.stateRegion2;
            newRegion2[newState2.length] = '';

            return { ...state, loading: false, stateType: newState, stateType2: newState2, regionType: newRegion, regionType2: newRegion2 };

        case REMOVE_ADD_MORE_LIST:
            var newsplProcedure = state.splProcedure2;
            newsplProcedure = newsplProcedure.filter((s, sidx) => action.payload !== sidx)
            state.splProcedure2 = newsplProcedure;

            var newState = state.stateList;
            newState = newState.filter((s, sidx) => action.payload !== sidx)
            state.stateList = newState;
            var newState2 = state.stateList2;
            newState2 = newState2.filter((s, sidx) => action.payload !== sidx)
            state.stateList2 = newState2;

            var newRegion = state.stateRegion;
            newRegion = newRegion.filter((s, sidx) => action.payload !== sidx)
            state.stateRegion = newRegion;
            var newRegion2 = state.stateRegion2;
            newRegion2 = newRegion2.filter((s, sidx) => action.payload !== sidx)
            state.stateRegion2 = newRegion2;
            //newState[newState.length] = '';
            return { ...state, loading: false, stateType: newState, stateType2: newState2, regionType: newRegion, regionType2: newRegion2, splProcedure: newsplProcedure };

        case PROVIDER_INSERT:
            return { ...state, loading: false, aloading: true };

        case PROVIDER_INSERT_SUCCESS:
            return { ...state, loading: false, aloading: false, redirectURL: 1, provider_details: '', provider_location: '', provider_bank: '', provider_specility: '', provider_user: '', provider_additional_discount: '', provider_additional_fee: '', provider_document: '', provider_additional_discount: '' };

        case PROVIDER_INSERT_FAILURE:
            return { ...state, loading: false, aloading: false, redirectURL: 1 };

        case PROVIDER_DELETE_SUCCESS:
            const deleteProvider = state.provider_list.filter(x => x.provider_id == action.payload);
            let indexOfDeleteProvider = state.provider_list.indexOf(deleteProvider[0]);



            state.provider_list.splice(indexOfDeleteProvider, 1);
            return { ...state, loading: false };
        case PROVIDER_DELETE:
            return { ...state, loading: true }
        case PROVIDER_DELETE_FAILURE:
            return { ...state, loading: false }

        default: return { ...state };
    }
}
