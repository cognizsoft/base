/**
 * Auth User Reducers
 */
import {
    ANSWER_LIST,
    ANSWER_LIST_SUCCESS,
    ANSWER_LIST_FAILURE,
    ANSWER_INSERT,
    ANSWER_INSERT_SUCCESS,
    ANSWER_INSERT_FAILURE,
    ANSWER_DELETE,
    ANSWER_DELETE_SUCCESS,
    ANSWER_DELETE_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE,

    PROVIDER_ANSWER_LIST,
    PROVIDER_ANSWER_LIST_SUCCESS,
    PROVIDER_ANSWER_LIST_FAILURE,

    PROVIDER_ANSWER_INSERT,
    PROVIDER_ANSWER_INSERT_SUCCESS,
    PROVIDER_ANSWER_INSERT_FAILURE,

    PROVIDER_ANSWER_DELETE,
    PROVIDER_ANSWER_DELETE_SUCCESS,
    PROVIDER_ANSWER_DELETE_FAILURE,

    CUSTOMER_ANSWER_LIST,
    CUSTOMER_ANSWER_LIST_SUCCESS,
    CUSTOMER_ANSWER_LIST_FAILURE,

    CUSTOMER_ANSWER_INSERT,
    CUSTOMER_ANSWER_INSERT_SUCCESS,
    CUSTOMER_ANSWER_INSERT_FAILURE,

    CUSTOMER_ANSWER_DELETE,
    CUSTOMER_ANSWER_DELETE_SUCCESS,
    CUSTOMER_ANSWER_DELETE_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        ///////CUSTOMER///////
        case CUSTOMER_ANSWER_DELETE:
            return {...state, loading: true}
        case CUSTOMER_ANSWER_DELETE_SUCCESS:
            const deleteCustomerAnswer = state.answerlist.filter(x => x.id == action.payload );
            let indexOfDeleteCustomerAnswer = state.answerlist.indexOf(deleteCustomerAnswer[0]);
            state.answerlist.splice(indexOfDeleteCustomerAnswer, 1);
            return { ...state, loading: false };
        
        case CUSTOMER_ANSWER_DELETE_FAILURE:
             return {...state, loading: false}

        case CUSTOMER_ANSWER_INSERT:
            return { ...state, loading: true };

        case CUSTOMER_ANSWER_INSERT_SUCCESS:
            const userNameCus = state.userslist.filter(x => x.user_id == action.payload.user_id);
            const questionNameCus = state.questionlist.filter(x => x.security_questions_id == action.payload.security_questions_id);
            let newAnswerCus = {
                id: action.payload.last_answer_id,
                answers: action.payload.answers,
                security_questions_id: action.payload.security_questions_id,
                user_id: action.payload.user_id,

                status: action.payload.answer_status
            }
            newAnswerCus.f_name = userNameCus[0].f_name;
            newAnswerCus.m_name = userNameCus[0].m_name;
            newAnswerCus.l_name = userNameCus[0].l_name;
            newAnswerCus.name = questionNameCus[0].name;
            let stateupdatedCus = state.answerlist;
            stateupdatedCus.reverse();
            stateupdatedCus.push(newAnswerCus);
            stateupdatedCus.reverse();
            
            return { ...state, loading: false, answerlist: stateupdatedCus };

        case CUSTOMER_ANSWER_INSERT_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_ANSWER_LIST:
            return { ...state, loading: true };

        case CUSTOMER_ANSWER_LIST_SUCCESS:
            return { ...state, loading: false, answerlist: action.payload.result, questionlist: action.payload.question, userslist: action.payload.users };

        case CUSTOMER_ANSWER_LIST_FAILURE:
            return { ...state, loading: false };

        //////PROVIDER///////
        case PROVIDER_ANSWER_DELETE:
            return {...state, loading: true}
        case PROVIDER_ANSWER_DELETE_SUCCESS:
            const deleteProviderAnswer = state.answerlist.filter(x => x.id == action.payload );
            let indexOfDeleteProviderAnswer = state.answerlist.indexOf(deleteProviderAnswer[0]);
            state.answerlist.splice(indexOfDeleteProviderAnswer, 1);
            return { ...state, loading: false };
        
        case PROVIDER_ANSWER_DELETE_FAILURE:
             return {...state, loading: false}

        case PROVIDER_ANSWER_INSERT:
            return { ...state, loading: true };

        case PROVIDER_ANSWER_INSERT_SUCCESS:
            const userNamePro = state.userslist.filter(x => x.user_id == action.payload.user_id);
            const questionNamePro = state.questionlist.filter(x => x.security_questions_id == action.payload.security_questions_id);
            let newAnswerPro = {
                id: action.payload.last_answer_id,
                answers: action.payload.answers,
                security_questions_id: action.payload.security_questions_id,
                user_id: action.payload.user_id,

                status: action.payload.answer_status
            }
            newAnswerPro.f_name = userNamePro[0].f_name;
            newAnswerPro.m_name = userNamePro[0].m_name;
            newAnswerPro.l_name = userNamePro[0].l_name;
            newAnswerPro.name = questionNamePro[0].name;
            let stateupdatedPro = state.answerlist;
            stateupdatedPro.reverse();
            stateupdatedPro.push(newAnswerPro);
            stateupdatedPro.reverse();
            
            return { ...state, loading: false, answerlist: stateupdatedPro };

        case PROVIDER_ANSWER_INSERT_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_ANSWER_LIST:
            return { ...state, loading: true };

        case PROVIDER_ANSWER_LIST_SUCCESS:
            return { ...state, loading: false, answerlist: action.payload.result, questionlist: action.payload.question, userslist: action.payload.users };

        case PROVIDER_ANSWER_LIST_FAILURE:
            return { ...state, loading: false };

        //////ADMIN//////
        case ANSWER_LIST:
            return { ...state, loading: true };

        case ANSWER_LIST_SUCCESS:
            return { ...state, loading: false, answerlist: action.payload.result, questionlist: action.payload.question, userslist: action.payload.users };

        case ANSWER_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, answerExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, answerExist: action.payload.exist, isEdit: action.payload.edit };

        case ANSWER_INSERT:
            return { ...state, loading: true };

        case ANSWER_INSERT_SUCCESS:
            const userName = state.userslist.filter(x => x.user_id == action.payload.user_id);
            const questionName = state.questionlist.filter(x => x.security_questions_id == action.payload.security_questions_id);
            let newAnswer = {
                id: action.payload.last_answer_id,
                answers: action.payload.answers,
                security_questions_id: action.payload.security_questions_id,
                user_id: action.payload.user_id,

                status: action.payload.answer_status
            }
            newAnswer.f_name = userName[0].f_name;
            newAnswer.m_name = userName[0].m_name;
            newAnswer.l_name = userName[0].l_name;
            newAnswer.name = questionName[0].name;
            let stateupdated = state.answerlist;
            stateupdated.reverse();
            stateupdated.push(newAnswer);
            stateupdated.reverse();
            
            return { ...state, loading: false, answerlist: stateupdated };

        case ANSWER_INSERT_FAILURE:
            return { ...state, loading: false };

        case ANSWER_DELETE:
            return {...state, loading: true}
        case ANSWER_DELETE_SUCCESS:
            const deleteAnswer = state.answerlist.filter(x => x.id == action.payload );
            let indexOfDeleteAnswer = state.answerlist.indexOf(deleteAnswer[0]);
            state.answerlist.splice(indexOfDeleteAnswer, 1);
            return { ...state, loading: false };
        
        case ANSWER_DELETE_FAILURE:
             return {...state, loading: false}

        default: return { ...state };
    }
}
