/**
 * Auth User Reducers
 */
import {
    SCORE_THRESHOLD_LIST_SUCCESS,
    SCORE_THRESHOLD_LIST_FAILURE,
    SCORE_THRESHOLD_INSERT_SUCCESS,
    SCORE_THRESHOLD_INSERT,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case SCORE_THRESHOLD_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, score_threshold: action.payload };

        case SCORE_THRESHOLD_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, score_threshold_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, scoreExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, scoreExist: action.payload.exist, isEdit: action.payload.edit };

        case SCORE_THRESHOLD_INSERT_SUCCESS:

            let newScoreThreshold = {
                id: action.payload.last_value_id,
                name: action.payload.name,
                value: action.payload.value,
                description: action.payload.description,
                effective_date: action.payload.effective_date,
                status: action.payload.st_status,
                type: action.payload.type,
             }
            let users = state.score_threshold;
            users.reverse();
            users.push(newScoreThreshold);
            users.reverse();
            
            return { ...state, loading: false, score_threshold:users};

        default: return { ...state };
    }
}
