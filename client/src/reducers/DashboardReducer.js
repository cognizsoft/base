/**
 * Auth User Reducers
 */
import {
    DASHBOARD_APPLICATION_REQUEST,
    DASHBOARD_APPLICATION_SUCCESS,
    DASHBOARD_APPLICATION_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        // get search custormer details start here
        case DASHBOARD_APPLICATION_REQUEST:
            return { ...state, loading: true };
        case DASHBOARD_APPLICATION_SUCCESS:
            if (action.payload.application.length > 0) {
                var application = action.payload.application.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['approved'] = 0;
                        accumulator['rejected'] = 0;
                        accumulator['pending'] = 0;
                        accumulator['manual'] = 0;
                    }
                    if (currentValue.status == 1) {
                        accumulator['approved'] += 1;
                    } else if (currentValue.status == 0) {
                        accumulator['rejected'] += 1;
                    } else if (currentValue.status == 4 || currentValue.status == 5) {
                        accumulator['pending'] += 1;
                    } else if (currentValue.status == 2) {
                        accumulator['manual'] += 1;
                    }
                    return accumulator;
                }, []);
            } else {
                var application = [];
                application['approved'] = 0;
                application['rejected'] = 0;
                application['pending'] = 0;
                application['manual'] = 0;
            }
            if (action.payload.invoiceStatus.length > 0) {
                var invoiceStatus = action.payload.invoiceStatus.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['submitted'] = 0;
                        accumulator['cancel'] = 0;
                        accumulator['approved'] = 0;
                        accumulator['confirm_payment'] = 0;
                        accumulator['received'] = 0;
                        accumulator['total_amount'] = 0;
                        accumulator['due'] = 0;
                    }
                    accumulator['total_amount'] = accumulator['total_amount'] + currentValue.total_amt;
                    if (currentValue.mdv_invoice_status_id == 1) {
                        accumulator['submitted'] += 1;
                    } else if (currentValue.mdv_invoice_status_id == 2) {
                        accumulator['cancel'] += 1;
                    } else if (currentValue.mdv_invoice_status_id == 3) {
                        accumulator['approved'] += 1;
                    } else if (currentValue.mdv_invoice_status_id == 4) {
                        accumulator['confirm_payment'] += 1;
                        accumulator['received'] = accumulator['received'] + currentValue.check_amount;
                    }
                    accumulator['due'] = accumulator['total_amount'] - accumulator['received'];
                    return accumulator;
                }, []);
            } else {
                var invoiceStatus = [];
                invoiceStatus['submitted'] = 0;
                invoiceStatus['cancel'] = 0;
                invoiceStatus['approved'] = 0;
                invoiceStatus['confirm_payment'] = 0;
                invoiceStatus['received'] = 0;
                invoiceStatus['total_amount'] = 0;
                invoiceStatus['due'] = 0;
            }

            if (action.payload.refundStatus != '' && action.payload.refundStatus.length > 0) {
                var refundStatus = action.payload.refundStatus.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['refund'] = 0;
                        accumulator['iou'] = 0;
                    }

                    if (currentValue.iou_flag == 1) {
                        accumulator['iou'] += currentValue.refund_due;
                    } else {
                        accumulator['refund'] += currentValue.refund_due;
                    }

                    return accumulator;
                }, []);

                
            } else {
                var refundStatus = [];
                refundStatus['refund'] = 0;
                refundStatus['iou'] = 0;
            }

            if (action.payload.customerInvoice != '' && action.payload.customerInvoice.length > 0) {
                var customerStatus = action.payload.customerInvoice.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['paid'] = 0;
                        accumulator['progress'] = 0;
                        accumulator['partial'] = 0;
                        accumulator['due'] = 0;
                        accumulator['deferred'] = 0;
                        accumulator['cancelled'] = 0;
                        accumulator['refunded'] = 0;
                    }
                    if (currentValue.paid_flag == 1) {
                        accumulator['paid'] += 1;
                    } else if (currentValue.paid_flag == 2) {
                        accumulator['progress'] += 1;
                    } else if (currentValue.paid_flag == 4) {
                        accumulator['partial'] += 1;
                    } else if (currentValue.paid_flag == 3) {
                        accumulator['due'] += 1;
                    } else if (currentValue.paid_flag == 5) {
                        accumulator['deferred'] += 1;
                    } else if (currentValue.paid_flag == 0) {
                        accumulator['cancelled'] += 1;
                    } else if (currentValue.paid_flag == 6) {
                        accumulator['refunded'] += 1;
                    }
                    return accumulator;
                }, []);
            } else {
                var customerStatus = [];
                customerStatus['paid'] = 0;
                customerStatus['progress'] = 0;
                customerStatus['partial'] = 0;
                customerStatus['due'] = 0;
                customerStatus['deferred'] = 0;
                customerStatus['cancelled'] = 0;
                customerStatus['refunded'] = 0;
            }
            
            if (action.payload.customerSupport != '' && action.payload.customerSupport.length > 0) {
                var supportStatus = action.payload.customerSupport.reduce(function (accumulator, currentValue, currentindex) {
                    if (currentindex == 0) {
                        accumulator['open'] = 0;
                        accumulator['Hold'] = 0;
                        accumulator['Closed'] = 0;
                        accumulator['FollowUp'] = 0;
                    }
                    if (currentValue.status_id == 1) {
                        accumulator['open'] += 1;
                    } else if (currentValue.status_id == 2) {
                        accumulator['Hold'] += 1;
                    } else if (currentValue.status_id == 0) {
                        accumulator['Closed'] += 1;
                    } else if (currentValue.status_id == 3) {
                        accumulator['follow_up_admin'] += 1;
                    }
                    return accumulator;
                }, []);
            } else {
                var supportStatus = [];
                supportStatus['open'] = 0;
                supportStatus['Hold'] = 0;
                supportStatus['Closed'] = 0;
                supportStatus['FollowUp'] = 0;
            }
            action.payload.customerSupport = action.payload.customerSupport.filter((s, idx) => s.status_id != 0)
            action.payload.customerSupport = action.payload.customerSupport.slice(0, 10).map(i => {
                return i;
            })
            
            return { ...state, loading: false, applicationDetails: application, invoiceStatus: invoiceStatus, customerStatus: customerStatus, refundStatus: refundStatus, customerSupport: action.payload.customerSupport, supportStatus: supportStatus };
        case DASHBOARD_APPLICATION_FAILURE:
            return { ...state, loading: false };
        default: return { ...state };
    }
}
