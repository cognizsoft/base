/**
 * Auth User Reducers
 */
import {
    MASTER_VALUE_LIST_SUCCESS,
    MASTER_VALUE_LIST_FAILURE,
    MASTER_VALUE_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case MASTER_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, master_value: action.payload };

        case MASTER_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_VALUE_INSERT_SUCCESS:
            let newUser = {
                mdv_id: action.payload.last_value_id,
                md_id: action.payload.master_value_name,
                value: action.payload.master_value,
                description: action.payload.master_value_desc,
                status: action.payload.master_value_status,
             }

            console.log(newUser)
            let users = state.master_value;
            users.reverse();
            users.push(newUser);
            users.reverse();
             
            return { ...state, loading: false, master_value_insert_id: action.payload};
        case EXIST_UPDATE:
            return { ...state, valueExist: 0};
        case EXIST_UPDATE_SUCCESS:            
            return { ...state, valueExist: action.payload.exist, isEdit: action.payload.edit};
        default: return { ...state };
    }
}
