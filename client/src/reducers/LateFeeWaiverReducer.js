/**
 * Auth User Reducers
 */
import {
    LATE_FEE_WAIVER_LIST_SUCCESS,
    LATE_FEE_WAIVER_LIST_FAILURE,
    LATE_FEE_WAIVER_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE,
    MASTER_DATA_VALUE_REASON_LIST_SUCCESS,
    MASTER_DATA_VALUE_REASON_LIST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case LATE_FEE_WAIVER_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, late_fee_waiver: action.payload };

        case LATE_FEE_WAIVER_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, late_fee_waiver_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_REASON_LIST_SUCCESS:
            return { ...state, loading: false, late_fee_reason_master_data_value: action.payload };

        case MASTER_DATA_VALUE_REASON_LIST_FAILURE:
            return { ...state, loading: false };

        case LATE_FEE_WAIVER_INSERT_SUCCESS:
        	const filterType = state.late_fee_waiver_master_data_value.filter(x => x.mdv_id == action.payload.waiver_type );

            //const filterType1 = state.late_fee_reason_master_data_value.filter(x => x.mdv_id == action.payload.reason_type );

            let newLateFeeWaiver = {
                id: action.payload.last_value_id,
                waiver_type_id: action.payload.waiver_type,
                reason_type_id: action.payload.reason_type,
                status: action.payload.factor_status,
             }
             newLateFeeWaiver.waiver_type = filterType[0].md_id+' - '+filterType[0].value+'%';
             console.log(filterType)
             //newLateFeeWaiver.reason_type = filterType1[0].value;
            //console.log(newLateFeeWaiver)
            let users = state.late_fee_waiver;
            users.reverse();
            users.push(newLateFeeWaiver); 
            users.reverse();
            
            return { ...state, loading: false};

        default: return { ...state };
    }
}
