/**
 * Auth User Reducers
 */
import {
    SURCHARGE_TYPE_LIST,
    SURCHARGE_TYPE_LIST_SUCCESS,
    SURCHARGE_TYPE_LIST_FAILURE,
    SURCHARGE_TYPE_INSERT,
    SURCHARGE_TYPE_INSERT_SUCCESS,
    SURCHARGE_TYPE_INSERT_FAILURE,
    SURCHARGE_TYPE_UPDATE,
    SURCHARGE_TYPE_UPDATE_SUCCESS,
    SURCHARGE_TYPE_UPDATE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case SURCHARGE_TYPE_LIST:
            return { ...state, loading: true };

        case SURCHARGE_TYPE_LIST_SUCCESS:
            return { ...state, loading: false, surchargelist: action.payload };

        case SURCHARGE_TYPE_LIST_FAILURE:
            return { ...state, loading: false };

        case EXIST_UPDATE:
            return { ...state, loading: false, surchargeExist: 0 };

        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, surchargeExist: action.payload.exist, isEdit: action.payload.edit };
        
        case SURCHARGE_TYPE_INSERT:
            return { ...state, loading: true };

        case SURCHARGE_TYPE_INSERT_SUCCESS:
            let newState = {
                id: action.payload.last_state_id,
                name: action.payload.surcharge_type,
                amount: action.payload.surcharge_amount,
                effective_date: action.payload.date,
                status: action.payload.state_status
            }
            let stateupdated = state.surchargelist;
            //console.log(stateupdated)
            stateupdated.reverse();
            stateupdated.push(newState);
            stateupdated.reverse();
            
            return { ...state, loading: false, surchargelist: stateupdated };

        case SURCHARGE_TYPE_INSERT_FAILURE:
            return { ...state, loading: false };

        

        default: return { ...state };
    }
}
