/**
 * Auth User Reducers
 */
import {
    FINANCIAL_CHARGES_LIST_SUCCESS,
    FINANCIAL_CHARGES_LIST_FAILURE,
    FINANCIAL_CHARGES_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case FINANCIAL_CHARGES_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, financial_charges: action.payload };

        case FINANCIAL_CHARGES_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, financial_charges_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case FINANCIAL_CHARGES_INSERT_SUCCESS:
        	const filterType = state.financial_charges_master_data_value.filter(x => x.mdv_id == action.payload.financial_charges_type );


            let newFinancialCharges = {
                id: action.payload.last_value_id,
                financial_charges_type_id: action.payload.financial_charges_type,
                charge: action.payload.charge,
                status: action.payload.fc_status,
             }
             newFinancialCharges.financial_charges_type = filterType[0].value;
            //console.log('newPayback')
            //console.log(newPayback)
            let users = state.financial_charges;
            users.reverse();
            users.push(newFinancialCharges); 
            users.reverse();
            
            return { ...state, loading: false};

        default: return { ...state };
    }
}
