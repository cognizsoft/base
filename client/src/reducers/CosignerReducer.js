/**
 * Auth User Reducers
 */
import {
    CREDIT_APPLICATION_LIST,
    CREDIT_APPLICATION_LIST_SUCCESS,
    CREDIT_APPLICATION_LIST_FAILURE,
    VIEW_CO_CUSTOMER_REQUEST,
    VIEW_CO_CUSTOMER_SUCCESS,
    VIEW_CO_CUSTOMER_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case CREDIT_APPLICATION_LIST:
            return { ...state, loading: true };
        case CREDIT_APPLICATION_LIST_SUCCESS:
            return { ...state, loading: false, applicationList: action.payload.result };
        case CREDIT_APPLICATION_LIST_FAILURE:
            return { ...state, loading: false };
        // view customer details start here
        case VIEW_CO_CUSTOMER_REQUEST:
            return { ...state, loading: true};
        case VIEW_CO_CUSTOMER_SUCCESS:
            return { ...state, loading: false, CoappDetails:action.payload.CoappDetails,CoappAddress:action.payload.CoappAddress,CobankDetails:action.payload.CobankDetails};
        case VIEW_CO_CUSTOMER_FAILURE:
            return { ...state, loading: false};
        // view customer details end here

        
        default: return { ...state };
    }
}
