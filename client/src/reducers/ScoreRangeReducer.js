/**
 * Auth User Reducers
 */
import {
    SCORE_RANGE_LIST,
    SCORE_RANGE_LIST_SUCCESS,
    SCORE_RANGE_LIST_FAILURE,

    SCORE_RANGE_INSERT,
    SCORE_RANGE_INSERT_SUCCESS,
    SCORE_RANGE_INSERT_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case SCORE_RANGE_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, scorerange: action.payload };

        case SCORE_RANGE_LIST_FAILURE:
            return { ...state, loading: false };

        case SCORE_RANGE_INSERT_SUCCESS:
        	//const filterType = state.payback_master_data_value.filter(x => x.mdv_id == action.payload.payback_type );


            let newScoreRange = {
                id: action.payload.last_value_id,
                amount: action.payload.amount,
                min_score: action.payload.min_score,
                max_score: action.payload.max_score,
                status: action.payload.score_status,
             }
             //newPayback.payback_type = filterType[0].value;
            //console.log('newPayback')
            //console.log(newPayback)
            let users = state.scorerange;
            users.reverse();
            users.push(newScoreRange); 
            users.reverse();
            return { ...state, loading: false};

        default: return { ...state };
    }
}
