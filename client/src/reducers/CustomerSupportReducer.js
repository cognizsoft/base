/**
 * Auth User Reducers
 */
import {
    CS_ACCOUNT_FILTER_REQUEST,
    CS_ACCOUNT_FILTER_REQUEST_SUCCESS,
    CS_ACCOUNT_FILTER_REQUEST_FAILURE,

    CS_GET_QUESTION_REQUEST,
    CS_GET_QUESTION_REQUEST_SUCCESS,
    CS_GET_QUESTION_REQUEST_FAILURE,

    CS_CREATE_SUPPORT_TICKET_REQUEST,
    CS_CREATE_SUPPORT_TICKET_SUCCESS,
    CS_CREATE_SUPPORT_TICKET_FAILURE,

    CS_TICKET_LIST_REQUEST,
    CS_TICKET_LIST_REQUEST_SUCCESS,
    CS_TICKET_LIST_REQUEST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {
        case CS_TICKET_LIST_REQUEST:
            return { ...state, loading: true, redirectURL: 0 };

        case CS_TICKET_LIST_REQUEST_SUCCESS:
            return { ...state, loading: false, tickets: action.payload.tickets, masterTicketSource: action.payload.masterTicketSource, providerApplication: action.payload.providerApplication, providerInvoice: action.payload.providerInvoice, customerPlan: action.payload.customerPlan, customerInvoice: action.payload.customerInvoice };

        case CS_TICKET_LIST_REQUEST_FAILURE:
            return { ...state, loading: false };

        case CS_CREATE_SUPPORT_TICKET_REQUEST:
            return { ...state, loading: true };

        case CS_CREATE_SUPPORT_TICKET_SUCCESS:
            /*console.log('state.providerApplication')
            console.log(state.providerApplication)
            console.log(state.providerInvoice)*/
            if (action.payload.ticket_insert.ticket_submit == 1) {
                
                
                for (let i = 0; i < state.tickets.length; i++) {
                    state.tickets[i].follow_up_flag = 0;
                    
                }
                
                let newLog = {
                    ticket_id: action.payload.ticket_insert.ticket_id,
                    cs_tkt_dtl_id: action.payload.ticket_insert.cs_tkt_dtl_id,
                    call_date_time: action.payload.ticket_insert.call_date_time,
                    caller_name: action.payload.ticket_insert.caller_name,
                    description: action.payload.ticket_insert.description,
                    follow_up_flag: action.payload.ticket_insert.follow_up,
                    follow_up_sch: action.payload.ticket_insert.follow_up_sch,
                    commented_by: action.payload.ticket_insert.commented_by,
                    hps_agent_name: action.payload.ticket_insert.hps_agent_name,
                    status: action.payload.ticket_insert.ticket_status,
                    status_id: action.payload.ticket_insert.status,
                    incoming_call_flag: action.payload.ticket_insert.call_type,
                }

                let logs = state.tickets;
                
                //logs.reverse();
                logs.push(newLog);
                //logs.reverse();
            } else {
                var app_no;
                if (action.payload.ticket_insert.provider_id && action.payload.ticket_insert.application_no) {
                    var appl_no = state.providerApplication.filter(x => x.application_id == action.payload.ticket_insert.application_no)
                    app_no = appl_no[0].application_no
                } else {
                    app_no = action.payload.ticket_insert.application_no
                }

                var inv_no;
                if (action.payload.ticket_insert.provider_id && action.payload.ticket_insert.invoice_no) {
                    var invc_no = state.providerInvoice.filter(x => x.provider_invoice_id == action.payload.ticket_insert.invoice_no)
                    inv_no = invc_no[0].invoice_number
                } else {
                    //inv_no = action.payload.ticket_insert.invoice_no
                    //var invc_no = state.customerInvoice.filter(x => x.invoice_id == action.payload.ticket_insert.invoice_no)
                    inv_no = action.payload.ticket_insert.invoice_no
                }

                let newLog = {
                    ticket_id: action.payload.ticket_insert.ticket_id,
                    ticket_number: action.payload.ticket_insert.ticket_number,
                    call_date_time: action.payload.ticket_insert.call_date_time,
                    ticket_source: (action.payload.ticket_insert.ticket_source == 1) ? 'Phone Call' : 'Email',

                    application_no: app_no,

                    plan_number: (action.payload.ticket_insert.plan_no) ? action.payload.ticket_insert.plan_no : '',

                    invoice_number: inv_no,

                    caller_name: action.payload.ticket_insert.caller_name,
                    subject: action.payload.ticket_insert.subject,
                    description: action.payload.ticket_insert.description,
                    status: action.payload.ticket_insert.status,
                }

                let logs = state.tickets;
                logs.reverse();
                logs.push(newLog);
                logs.reverse();
            }

            return { ...state, loading: false };

        case CS_CREATE_SUPPORT_TICKET_FAILURE:
            return { ...state, loading: false };

        case CS_GET_QUESTION_REQUEST:
            return { ...state, loading: true }
        case CS_GET_QUESTION_REQUEST_SUCCESS:
            return { ...state, loading: false, customerQuestionsList: action.payload.questions };
        case CS_GET_QUESTION_REQUEST_FAILURE:
            return { ...state, loading: false };

        case CS_ACCOUNT_FILTER_REQUEST:
            return { ...state, loading: true }
        case CS_ACCOUNT_FILTER_REQUEST_SUCCESS:
            if (action.payload.filter_by == 1) {
                var providerList = action.payload.result;
            } else {
                var customerList = action.payload.result;
            }

            return { ...state, loading: false, customerAccountList: customerList, providerAccountList: providerList };
        case CS_ACCOUNT_FILTER_REQUEST_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
