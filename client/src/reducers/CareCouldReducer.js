/**
 * Auth User Reducers
 */
import {
    TOKEN_REQUEST,
    TOKEN_REQUEST_SUCCESS,
    TOKEN_REQUEST_FAILURE,
    SEARCH_APPLICATION_REQUEST,
    SEARCH_APPLICATION_SUCCESS,
    SEARCH_APPLICATION_FAILURE,
    VIEW_SEARCH_PATIENT_REQUEST,
    VIEW_SEARCH_PATIENT_SUCCESS,
    VIEW_SEARCH_PATIENT_FAILURE,
    CARE_CLOUD_REQUEST,
    CARE_CLOUD_SUCCESS,
    CARE_CLOUD_FAILURE,
    MFS_DETAILS_REQUEST,
    MFS_DETAILS_SUCCESS,
    MFS_DETAILS_FAILURE,
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        // get calient id start here
        case MFS_DETAILS_REQUEST:
            return { ...state, loading: true};
        case MFS_DETAILS_SUCCESS:
            return { ...state, loading: false, careCould: action.payload.careCould };
        case MFS_DETAILS_FAILURE:
            return { ...state, loading: false };
        // get calient id end here
        // provider care cpuld details start here
        case CARE_CLOUD_REQUEST:
            return { ...state, loading: true};
        case CARE_CLOUD_SUCCESS:
            return { ...state, loading: false, careCouldSecret: action.payload.result };
        case CARE_CLOUD_FAILURE:
            return { ...state, loading: false };
        // provider care cpuld details end here
        // view care could result start here
        case VIEW_SEARCH_PATIENT_REQUEST:
            return { ...state, loading: true};
        case VIEW_SEARCH_PATIENT_SUCCESS:
            return { ...state, loading: false, careCouldView: action.payload.patient };
        case VIEW_SEARCH_PATIENT_FAILURE:
            return { ...state, loading: false };
        // view care could result end here
        // get care could result start here
        case SEARCH_APPLICATION_REQUEST:
            return { ...state, loading: true};
        case SEARCH_APPLICATION_SUCCESS:
            return { ...state, loading: false, careCouldSearch: action.payload.patients };
        case SEARCH_APPLICATION_FAILURE:
            return { ...state, loading: false };
        // get care could result end here
        // get search custormer details start here
        case TOKEN_REQUEST:
            return { ...state, loading: true};
        case TOKEN_REQUEST_SUCCESS:
            return { ...state, loading: false, careCouldToken: action.payload };
        case TOKEN_REQUEST_FAILURE:
            return { ...state, loading: false };
        // get search custormer details end here
        default: return { ...state };
    }
}
