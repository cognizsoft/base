/**
 * Auth User Reducers
 */
import {
    TERM_MONTH_LIST_SUCCESS,
    TERM_MONTH_LIST_FAILURE,
    TERM_MONTH_INSERT_SUCCESS,
    TERM_MONTH_EXIST,
    TERM_MONTH_EXIST_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case TERM_MONTH_LIST_SUCCESS:
            return { ...state, loading: false, term_month_list: action.payload };

        case TERM_MONTH_LIST_FAILURE:
            return { ...state, loading: false };

        case TERM_MONTH_INSERT_SUCCESS:
            let newUser = {
                mdv_id: action.payload.last_type_id,
                term_month: action.payload.term_month,
                risk_factor: action.payload.risk_factor,
                term_month_desc: action.payload.term_month_desc,
                status: action.payload.term_month_status,
            }


            let users = state.term_month_list;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, user_type_insert_id: action.payload };
        case TERM_MONTH_EXIST:
            return { ...state, loading: false, typeExist: 0 };
        case TERM_MONTH_EXIST_SUCCESS:
            return { ...state, loading: false, typeExist: action.payload.exist, isEdit: action.payload.edit };
        default: return { ...state };
    }
}
