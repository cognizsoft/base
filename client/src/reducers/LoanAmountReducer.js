/**
 * Auth User Reducers
 */
import {
    LOAN_AMOUNT_LIST_SUCCESS,
    LOAN_AMOUNT_LIST_FAILURE,
    LOAN_AMOUNT_INSERT_SUCCESS,
    LOAN_AMOUNT_EXIST,
    LOAN_AMOUNT_EXIST_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case LOAN_AMOUNT_LIST_SUCCESS:
            return { ...state, loading: false, loan_amount_list: action.payload };

        case LOAN_AMOUNT_LIST_FAILURE:
            return { ...state, loading: false };

        case LOAN_AMOUNT_INSERT_SUCCESS:
            let newUser = {
                mdv_id: action.payload.last_type_id,
                loan_amount: action.payload.loan_amount,
                risk_factor: action.payload.risk_factor,
                loan_amount_desc: action.payload.loan_amount_desc,
                status: action.payload.loan_amount_status,
            }


            let users = state.loan_amount_list;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, user_type_insert_id: action.payload };
        case LOAN_AMOUNT_EXIST:
            return { ...state, loading: false, typeExist: 0 };
        case LOAN_AMOUNT_EXIST_SUCCESS:
            return { ...state, loading: false, typeExist: action.payload.exist, isEdit: action.payload.edit };
        default: return { ...state };
    }
}
