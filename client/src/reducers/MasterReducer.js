/**
 * Auth User Reducers
 */
import {
    MASTER_LIST,
    MASTER_INSERT_SUCCESS,
    MASTER_LIST_SUCCESS,
    MASTER_LIST_FAILURE,
    MASTER_UPDATE_SUCCESS,
    MASTER_UPDATE_FAILURE,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    nameExist: false,
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {
        case MASTER_LIST:
        return { ...state, loading: true };
        case MASTER_LIST_SUCCESS:
            if(action.payload.total !== undefined)
                return { ...state, loading: false, master: action.payload.result, total:action.payload.total };
            else
                return { ...state, loading: false, master: action.payload.result};
        case MASTER_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_UPDATE_SUCCESS:
            return { ...state, loading: false };
        
        case MASTER_UPDATE_FAILURE:
            return { ...state, loading: false };
        case EXIST_UPDATE:
            return { ...state, loading: false, nameExist: 0};
        case EXIST_UPDATE_SUCCESS:            
            return { ...state, loading: false, nameExist: action.payload.exist, isEdit: action.payload.edit};
            
        case MASTER_INSERT_SUCCESS:
            let newUser = {
                md_id: action.payload.last_type_id,
                md_name: action.payload.md_name,
                edit_allowed: action.payload.edit_allowed,
                status: action.payload.data_status,
             }
            let users = state.master;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, insert_id: action.payload};

        default: return { ...state };
    }
}
