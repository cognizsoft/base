/**
 * Auth User Reducers
 */
import {
    USER_ROLE_LIST_SUCCESS,
    USER_ROLE_LIST_FAILURE,
    USER_ROLE_INSERT_SUCCESS,
    EXIST_UPDATE_SUCCESS,
    EXIST_UPDATE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case USER_ROLE_LIST_SUCCESS:
            return { ...state, loading: false, user_role: action.payload.result, userType: action.payload.userType };

        case USER_ROLE_LIST_FAILURE:
            return { ...state, loading: false };

        case USER_ROLE_INSERT_SUCCESS:
            const filterType = state.userType.filter(x => x.mdv_id == action.payload.userType);
            let newUser = {
                user_type_role_id: action.payload.user_type_role_id,
                type_id: action.payload.userType,
                mdv_id: action.payload.last_role_id,
                value: action.payload.user_role,
                description: action.payload.user_role_desc,
                status: action.payload.user_role_status,
            }
            newUser.userType = filterType[0].value;
            let users = state.user_role;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, user_role_insert_id: action.payload };
            case EXIST_UPDATE:
            return { ...state, roleExist: 0};
        case EXIST_UPDATE_SUCCESS:            
            return { ...state, roleExist: action.payload.exist, isEdit: action.payload.edit};
        default: return { ...state };
    }
}
