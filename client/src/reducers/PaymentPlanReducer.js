/**
 * Auth User Reducers
 */
import {
    PAYMENT_MASTER_FEE_OPTION,
    PAYMENT_MASTER_FEE_OPTION_SUCCESS,
    PAYMENT_MASTER_FEE_OPTION_FAILURE,

    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION,
    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_SUCCESS,
    PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_FAILURE,

    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION,
    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_SUCCESS,
    PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_FAILURE,

    PAYMENT_SINGLE_INSTALLMENT,
    PAYMENT_SINGLE_INSTALLMENT_SUCCESS,
    PAYMENT_SINGLE_INSTALLMENT_FAILURE,

    PAY_INSTALLMENT_INSERT,
    PAY_INSTALLMENT_INSERT_SUCCESS,
    PAY_INSTALLMENT_INSERT_FAILURE,

    CLEAR_REDIRECT_URL,

    SEND_EMAIL_REPORT,
    SEND_EMAIL_REPORT_SUCCESS,
    SEND_EMAIL_REPORT_FAILURE,

    ALL_PLAN_DETAILS,
    ALL_PLAN_DETAILS_SUCCESS,
    ALL_PLAN_DETAILS_FAILURE,

    INVOICE_PLAN_REQUEST,
    INVOICE_PLAN_SUCCESS,
    INVOICE_PLAN_FAILURE,

    VIEW_RECIPIT_REQUEST,
    VIEW_RECIPIT_SUCCESS,
    VIEW_RECIPIT_FAILURE,

    CREATE_INVOICE_REQUEST,
    CREATE_INVOICE_SUCCESS,
    CREATE_INVOICE_FAILURE,

    REGENERATE_PROCESS_REQUEST,
    REGENERATE_PROCESS_SUCCESS,
    REGENERATE_PROCESS_FAILURE,
    REGENERATE_PLAN_PROCESS,

    OPTION_TO_CLOSE_REQUEST,
    OPTION_TO_CLOSE_SUCCESS,
    OPTION_TO_CLOSE_FAILURE,

    OPTION_TO_CLOSE_ACTION_REQUEST,
    OPTION_TO_CLOSE_ACTION_SUCCESS,
    OPTION_TO_CLOSE_ACTION_FAILURE,

    CUSTOMER_PAUSE_REQUEST,
    CUSTOMER_PAUSE_SUCCESS,
    CUSTOMER_PAUSE_FAILURE,
    
    CUSTOMER_PAUSE_LIST_REQUEST,
    CUSTOMER_PAUSE_LIST_SUCCESS,
    CUSTOMER_PAUSE_LIST_FAILURE,

    REGENERATE_PRINT_PROCESS_REQUEST,
    REGENERATE_PRINT_PROCESS_SUCCESS,
    REGENERATE_PRINT_PROCESS_FAILURE,
    REGENERATE_PRINT_PLAN_PROCESS,

    CURRENT_PLAN_DETAILS_REQUEST,
    CURRENT_PLAN_DETAILS_SUCCESS,
    CURRENT_PLAN_DETAILS_FAILURE,

    CONFIRMED_CANCELLATION_PLAN_REQUEST,
    CONFIRMED_CANCELLATION_PLAN_SUCCESS,
    CONFIRMED_CANCELLATION_PLAN_FAILURE,

} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false,
    rediretPlanURL: 0
};

export default (state = INIT_STATE, action) => {
    //console.log('action.type')
    //console.log(action.type)
    switch (action.type) {
        // close plan details start here
        case CONFIRMED_CANCELLATION_PLAN_REQUEST:
            return { ...state, loading: true, };
        case CONFIRMED_CANCELLATION_PLAN_SUCCESS:
            return { ...state, loading: false};
        case CONFIRMED_CANCELLATION_PLAN_FAILURE:
            return { ...state, loading: false, };
        // close plan details end here

        // close plan details start here
        case CURRENT_PLAN_DETAILS_REQUEST:
            return { ...state, loading: true, };
        case CURRENT_PLAN_DETAILS_SUCCESS:
            return { ...state, loading: false, planInvoiceDetails: action.payload.invoiceDetails, planDetails: action.payload.planDetails, closeInt: action.payload.closeInt, closeTerm:action.payload.closeTerm, customerPaid:action.payload.customerPaid, providerInvoiceDetails:action.payload.providerInvoiceDetails, paymentMethod:action.payload.paymentMethod, providerRefundDetails:action.payload.providerRefundDetails};
        case CURRENT_PLAN_DETAILS_FAILURE:
            return { ...state, loading: false, };
        // close plan details end here

        case REGENERATE_PRINT_PLAN_PROCESS:
            var generateArea = (action.payload !== undefined && action.payload.status == 1) ? 1 : 0;
            return { ...state, loading: false, generateArea: generateArea }
        case REGENERATE_PRINT_PROCESS_REQUEST:
            return { ...state, loading: true }
        case REGENERATE_PRINT_PROCESS_SUCCESS:
            return { ...state, loading: false, rePlanDetails: action.payload.planDetails, reProviderDetails: action.payload.providerDetails }
        case REGENERATE_PRINT_PROCESS_FAILURE:
            return { ...state, loading: false }
            
        // view option to close start here
        case CUSTOMER_PAUSE_LIST_REQUEST:
            return { ...state, loading: true, };
        case CUSTOMER_PAUSE_LIST_SUCCESS:
            return { ...state, loading: false, listInvoicePause: action.payload.result };
        case CUSTOMER_PAUSE_LIST_FAILURE:
            return { ...state, loading: false, };

        case CUSTOMER_PAUSE_REQUEST:
            return { ...state, loading: true, };
        case CUSTOMER_PAUSE_SUCCESS:
            return { ...state, loading: false, listInvoicePause: action.payload.result };
        case CUSTOMER_PAUSE_FAILURE:
            return { ...state, loading: false, };



        case OPTION_TO_CLOSE_ACTION_REQUEST:
            return { ...state, loading: true, };
        case OPTION_TO_CLOSE_ACTION_SUCCESS:
            return { ...state, loading: false, planCloseStatus: 1 };
        case OPTION_TO_CLOSE_ACTION_FAILURE:
            return { ...state, loading: false, };

        case OPTION_TO_CLOSE_REQUEST:
            return { ...state, loading: true, };
        case OPTION_TO_CLOSE_SUCCESS:
            return { ...state, loading: false, planCloseOption: action.payload.result, planInvoiceExist: action.payload.invoiceExits,customerDetails:action.payload.customerDetails,providerDetails:action.payload.providerDetails };
        case OPTION_TO_CLOSE_FAILURE:
            return { ...state, loading: false, };
        // view option to cloase plan end here


        case REGENERATE_PLAN_PROCESS:
            var generateArea = (action.payload !== undefined && action.payload.status == 1) ? 1 : 0;
            return { ...state, loading: false, generateArea: generateArea }
        case REGENERATE_PROCESS_REQUEST:
            return { ...state, loading: true }
        case REGENERATE_PROCESS_SUCCESS:
            return { ...state, loading: false, rePlanDetails: action.payload.planDetails, reProviderDetails: action.payload.providerDetails }
        case REGENERATE_PROCESS_FAILURE:
            return { ...state, loading: false }

        case CREATE_INVOICE_REQUEST:
            return { ...state, loading: true, };
        case CREATE_INVOICE_SUCCESS:
            return { ...state, loading: false, application_details: action.payload.application_details, application_plans: action.payload.application_plans, invoiceDetails: action.payload.invoiceDetails };
        case CREATE_INVOICE_FAILURE:
            return { ...state, loading: false };

        // view recipit start here
        case VIEW_RECIPIT_REQUEST:
            return { ...state, loading: true, };
        case VIEW_RECIPIT_SUCCESS:
            return { ...state, loading: false, recipitDetails: action.payload.result, reciptCustomer: action.payload.customer, installmentDetails: action.payload.installmentDetails, invoicePaymentDetails: action.payload.invoicePaymentDetails };
        case VIEW_RECIPIT_FAILURE:
            return { ...state, loading: false, };
        // view recipit end here
        case INVOICE_PLAN_REQUEST:
            return { ...state, loading: true, redirectURL: 0 };
        case INVOICE_PLAN_SUCCESS:
            var planD = action.payload.payment_plan_details.reverse();
            //var paymentPlanDetails = action.payload.payment_plan_details;
            var addiAmt = (planD) ? planD.filter(x => x.additional_amount !== 0 && x.additional_amount !== null) : '';

            var addiAmtTotal = 0;
            if (addiAmt) {
                for (var i = 0; i < addiAmt.length; i++) {
                    addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
                }
            }

            for (var k = 0; k < planD.length; k++) {
                if (addiAmtTotal > planD[k].installment_amt) {
                    addiAmtTotal = addiAmtTotal - planD[k].installment_amt;
                    planD[k].installment_amt = 0;
                } else {
                    planD[k].installment_amt = planD[k].installment_amt - addiAmtTotal;
                    addiAmtTotal = 0
                }
            }

            planD.reverse();

            var invoice_dtl = action.payload.invoice_dtl;
            var late_fee = action.payload.late_fee;
            var fin_charge = action.payload.fin_charge;
            invoice_dtl.forEach(v => { v.master_late_fee = late_fee; v.master_fin_charge = fin_charge; });

            return { ...state, loading: false, redirectURL: 0, mainPlan: action.payload.plan, singlePlanDetails: action.payload.plan_details, paymentPlanDetails: planD, last_payment_info: action.payload.last_payment_info, invoice_dtl: action.payload.invoice_dtl, invoice_last_pymt: action.payload.invoice_last_pymt, amountDetailsInv: action.payload.amountDetailsInv, credit_charge: action.payload.credit_charge };
        case INVOICE_PLAN_FAILURE:
            return { ...state, loading: false };

        case ALL_PLAN_DETAILS:
            return { ...state, loading: true, redirectURL: 0 };

        case ALL_PLAN_DETAILS_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, application_details: action.payload.application_details, application_plans: action.payload.application_plans, invoiceDetails: action.payload.invoiceDetails, amountDetails: action.payload.amountDetails, userQuestions: action.payload.user_questions, invoicePlan: action.payload.invoicePlan, tickets: action.payload.tickets, masterTicketSource: action.payload.masterTicketSource };

        case ALL_PLAN_DETAILS_FAILURE:
            return { ...state, loading: false };

        case SEND_EMAIL_REPORT:
            return { ...state, redirectURL: 0 }

        case SEND_EMAIL_REPORT_SUCCESS:
            //console.log(action)
            var sendReportSuccess;
            if (action.payload.sendReport == 1) {
                sendReportSuccess = 1;
            } else {
                sendReportSuccess = 0;
            }
            return { ...state, loading: false, sendReport: sendReportSuccess };

        case SEND_EMAIL_REPORT_FAILURE:
            return { ...state, loading: false };

        case CLEAR_REDIRECT_URL:
            //console.log('new case');
            return { ...state, redirectURL: 0, planCloseStatus: 0 }

        case PAY_INSTALLMENT_INSERT:
            return { ...state, loading: true }

        case PAY_INSTALLMENT_INSERT_SUCCESS:
            //console.log(action)
            var redirectURI;
            if (action.payload.redirectReport == 1) {
                redirectURI = 1;
            } else {
                redirectURI = 0;
            }
            return { ...state, loading: false, redirectURL: redirectURI, single_installment_payment: '', single_installment_payment_edit: '', single_invoice_rows: '', invoice_rows_appid: '', payment_billing_late_fee: '', payment_invoice_detail: '' };

        case PAY_INSTALLMENT_INSERT_FAILURE:
            return { ...state, loading: false };

        case PAYMENT_SINGLE_INSTALLMENT:
            return { ...state, loading: true, single_installment_payment: undefined, single_installment_payment_edit: undefined, single_invoice_rows: undefined, invoice_rows_appid: undefined }

        case PAYMENT_SINGLE_INSTALLMENT_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, single_installment_payment: action.payload.single_installment_payment, single_installment_payment_edit: action.payload.single_installment_payment_edit, single_invoice_rows: action.payload.single_invoice_rows, invoice_rows_appid: action.payload.invoice_rows_appid, payment_billing_late_fee: action.payload.payment_billing_late_fee, payment_invoice_detail: action.payload.payment_invoice_detail, invoice_detail: action.payload.payment_invoice_detail, credit_charge: action.payload.credit_charge, customerBank: action.payload.customerBank,customerCoBank: action.payload.customerCoBank };

        case PAYMENT_SINGLE_INSTALLMENT_FAILURE:
            return { ...state, loading: false };

        case PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION:
            return { ...state, loading: true }

        case PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, fin_charge_waiver_type_reason_option: action.payload.fin_charge_waiver_type_reason_option };

        case PAYMENT_FIN_CHARGE_WAIVER_TYPE_OPTION_FAILURE:
            return { ...state, loading: false };

        case PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION:
            return { ...state, loading: true }

        case PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, late_fee_waiver_type_reason_option: action.payload.late_fee_waiver_type_reason_option };

        case PAYMENT_LATE_FEE_WAIVER_TYPE_OPTION_FAILURE:
            return { ...state, loading: false };

        case PAYMENT_MASTER_FEE_OPTION:
            return { ...state, loading: true, single_installment_payment: undefined, single_installment_payment_edit: undefined }

        case PAYMENT_MASTER_FEE_OPTION_SUCCESS:
            return { ...state, loading: false, redirectURL: 0, late_fee_waivers_option: action.payload.late_fee_waivers_option, financial_charges_waiver_option: action.payload.financial_charges_waiver_option, financial_charges_option: action.payload.financial_charges_option, payment_method_option: action.payload.payment_method_option };

        case PAYMENT_MASTER_FEE_OPTION_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
