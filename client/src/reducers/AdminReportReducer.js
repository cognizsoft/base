/**
 * Auth User Reducers
 */
import {
    ADMIN_REPORT_LIST,
    ADMIN_REPORT_LIST_SUCCESS,
    ADMIN_REPORT_LIST_FAILURE,
    REPORT_DOWNLOAD_LIST_REQUEST,
    REPORT_DOWNLOAD_LIST_SUCCESS,
    REPORT_DOWNLOAD_LIST_FAILURE,
    PROVIDER_INVOICE_REPORT_LIST,
    PROVIDER_INVOICE_REPORT_LIST_SUCCESS,
    PROVIDER_INVOICE_REPORT_LIST_FAILURE,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS,
    CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE,

    PROCEDURE_DATE_REQUEST,
    PROCEDURE_DATE_SUCCESS,
    PROCEDURE_DATE_FAILURE,

    ADMIN_ACC_RECEIVABLES_REPORT_REQUEST,
    ADMIN_ACC_RECEIVABLES_REPORT_SUCCESS,
    ADMIN_ACC_RECEIVABLES_REPORT_FAILURE,

    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_REQUEST,
    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_SUCCESS,
    ADMIN_ACC_RECEIVABLES_REPORT_DOWN_FAILURE,

    SUPPORT_REPORT_REQUEST,
    SUPPORT_REPORT_SUCCESS,
    SUPPORT_REPORT_FAILURE,

    SUPPORT_REPORT_DOWN_REQUEST,
    SUPPORT_REPORT_DOWN_SUCCESS,
    SUPPORT_REPORT_DOWN_FAILURE,

    CUSTOMER_SUPPORT_REPORT_REQUEST,
    CUSTOMER_SUPPORT_REPORT_SUCCESS,
    CUSTOMER_SUPPORT_REPORT_FAILURE,

    CUSTOMER_SUPPORT_REPORT_DOWN_REQUEST,
    CUSTOMER_SUPPORT_REPORT_DOWN_SUCCESS,
    CUSTOMER_SUPPORT_REPORT_DOWN_FAILURE,

    PROVIDER_PAYABLE_REQUEST,
    PROVIDER_PAYABLE_SUCCESS,
    PROVIDER_PAYABLE_FAILURE,

    CREDIT_CHARGES_REQUEST,
    CREDIT_CHARGES_SUCCESS,
    CREDIT_CHARGES_FAILURE,

} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {

    switch (action.type) {

        case CREDIT_CHARGES_REQUEST:
            return { ...state, loading: true }
        case CREDIT_CHARGES_SUCCESS:
            return { ...state, loading: false, creditCharesList: action.payload.result, yearStart : action.payload.yearStart };
        case CREDIT_CHARGES_FAILURE:
            return { ...state, loading: false };

        case PROVIDER_PAYABLE_REQUEST:
            return { ...state, loading: true }
        case PROVIDER_PAYABLE_SUCCESS:
            return { ...state, loading: false, providerPayableList: action.payload.result, yearStart : action.payload.yearStart };

        case PROVIDER_PAYABLE_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_SUPPORT_REPORT_DOWN_REQUEST:
            return { ...state, loading: true }
        case CUSTOMER_SUPPORT_REPORT_DOWN_SUCCESS:
            return { ...state, loading: false};
        case CUSTOMER_SUPPORT_REPORT_DOWN_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_SUPPORT_REPORT_REQUEST:
            return { ...state, loading: true }
        case CUSTOMER_SUPPORT_REPORT_SUCCESS:
            return { ...state, loading: false, customer_support_report_list: action.payload.result };
        case CUSTOMER_SUPPORT_REPORT_FAILURE:
            return { ...state, loading: false };

        case SUPPORT_REPORT_DOWN_REQUEST:
            return { ...state, loading: true }
        case SUPPORT_REPORT_DOWN_SUCCESS:
            return { ...state, loading: false};
        case SUPPORT_REPORT_DOWN_FAILURE:
            return { ...state, loading: false };

        case SUPPORT_REPORT_REQUEST:
            return { ...state, loading: true }
        case SUPPORT_REPORT_SUCCESS:
            return { ...state, loading: false, support_report_list: action.payload.result };
        case SUPPORT_REPORT_FAILURE:
            return { ...state, loading: false };

        case ADMIN_ACC_RECEIVABLES_REPORT_DOWN_REQUEST:
            return { ...state, loading: true }
        case ADMIN_ACC_RECEIVABLES_REPORT_DOWN_SUCCESS:
            return { ...state, loading: false};
        case ADMIN_ACC_RECEIVABLES_REPORT_DOWN_FAILURE:
            return { ...state, loading: false };

        case ADMIN_ACC_RECEIVABLES_REPORT_REQUEST:
            return { ...state, loading: true }
        case ADMIN_ACC_RECEIVABLES_REPORT_SUCCESS:
            return { ...state, loading: false, acc_rcv_report_list: action.payload.result, yearStart: action.payload.year_start, lateFinCharge: action.payload.charge };
        case ADMIN_ACC_RECEIVABLES_REPORT_FAILURE:
            return { ...state, loading: false };

        // Procedure Date Report
        case PROCEDURE_DATE_REQUEST:
            return { ...state, loading: true }
        case PROCEDURE_DATE_SUCCESS:
            return { ...state, loading: false, procedureDateList:action.payload.result};
        case PROCEDURE_DATE_FAILURE:
            return { ...state, loading: false };

        case CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST:
            return { ...state, loading: true }
        case CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_SUCCESS:
            return { ...state, loading: false, customerInvoiceList:action.payload.result, customerInvoiceStatus: action.payload.inv_status};
        case CUSTOMER_WEEK_MONTH_INVOICE_REPORT_LIST_FAILURE:
            return { ...state, loading: false };
        // customr week month invoice report end here
        // provider invoice report start here
        case PROVIDER_INVOICE_REPORT_LIST:
            return { ...state, loading: true }
        case PROVIDER_INVOICE_REPORT_LIST_SUCCESS:
            if(action.payload.statusname === undefined){
                return { ...state, loading: false, providerInvoiceList:action.payload.result};
            }else{
                return { ...state, loading: false, providerInvoiceList:action.payload.result, statusList:action.payload.statusname, providerList:action.payload.provider, iouAmt: action.payload.iouAmt};
            }            
        case PROVIDER_INVOICE_REPORT_LIST_FAILURE:
            return { ...state, loading: false };
        // provider invoice report end here
        // download loan report start here
        case REPORT_DOWNLOAD_LIST_REQUEST:
            return { ...state, loading: true }
        case REPORT_DOWNLOAD_LIST_SUCCESS:
            return { ...state, loading: false};
        case REPORT_DOWNLOAD_LIST_FAILURE:
            return { ...state, loading: false };
        // doenload loan report end here
        case ADMIN_REPORT_LIST:
            return { ...state, loading: true }

        case ADMIN_REPORT_LIST_SUCCESS:
            return { ...state, loading: false, admin_report_list: action.payload.result, customerPlanStatus: action.payload.plan_status, lateFinCharge: action.payload.charge, yearStart : action.payload.yearStart };

        case ADMIN_REPORT_LIST_FAILURE:
            return { ...state, loading: false };

        default: return { ...state };
    }
}
