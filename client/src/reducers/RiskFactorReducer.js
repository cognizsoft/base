/**
 * Auth User Reducers
 */
import {
    RISK_FACTORS_LIST_SUCCESS,
    RISK_FACTORS_LIST_FAILURE,
    RISK_FACTORS_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case RISK_FACTORS_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, risk_factor: action.payload };

        case RISK_FACTORS_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, risk_factor_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case RISK_FACTORS_INSERT_SUCCESS:
        	const filterType = state.risk_factor_master_data_value.filter(x => x.mdv_id == action.payload.risk_factor_type );


            let newRiskFactor = {
                id: action.payload.last_value_id,
                weight: action.payload.risk_factor_weight,
                weight_type: action.payload.risk_factor_weight_type,
                effective_date: action.payload.effective_date,
                status: action.payload.factor_status,
             }
             newRiskFactor.factor_type = filterType[0].value;
            //console.log(newRiskFactor)
            let users = state.risk_factor;
            users.reverse();
            users.push(newRiskFactor); 
            users.reverse();
            
            return { ...state, loading: false, risk_factor_insert_id: action.payload};

        default: return { ...state };
    }
}
