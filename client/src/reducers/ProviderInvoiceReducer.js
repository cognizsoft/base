/**
 * Auth User Reducers
 */
import {
        PROVIDER_PENDING_REQUEST,
        PROVIDER_PENDING_SUCCESS,
        PROVIDER_PENDING_FAILURE,
        PROVIDER_PREVIEW_INVOICE,
        PROVIDER_PREVIEW_LOAD,
        PROVIDER_SELECTED_REQUEST,
        PROVIDER_SELECTED_SUCCESS,
        PROVIDER_SELECTED_FAILURE,
        PROVIDER_PDF_REQUEST,
        PROVIDER_PDF_SUCCESS,
        PROVIDER_PDF_FAILURE,
        PROVIDER_INVOICE_SUBMIT_REQUEST,
        PROVIDER_INVOICE_SUBMIT_SUCCESS,
        PROVIDER_INVOICE_SUBMIT_FAILURE,
        PROVIDER_SUBMITTED_REQUEST,
        PROVIDER_SUBMITTED_SUCCESS,
        PROVIDER_SUBMITTED_FAILURE,
        PROVIDER_VIEW_INVOICE_REQUEST,
        PROVIDER_VIEW_INVOICE_SUCCESS,
        PROVIDER_VIEW_INVOICE_FAILURE,

        PROVIDER_VIEW_INVOICE_SUBMITTED_REQUEST,
        PROVIDER_VIEW_INVOICE_SUBMITTED_SUCCESS,
        PROVIDER_VIEW_INVOICE_SUBMITTED_FAILURE,

        REFUND_INVOICE_REQUEST,
        REFUND_INVOICE_SUCCESS,
        REFUND_INVOICE_FAILURE,

} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
        loading: false,
        stateList: [],
        stateRegion: [],
        rediretURL: 0,
        rediretPlanURL: 0
};

export default (state = INIT_STATE, action) => {
        //console.log(action.type)
        switch (action.type) {
                // provider refund invoice
                case REFUND_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case REFUND_INVOICE_SUCCESS:
                        return { ...state, loading: false, refundInvoices: action.payload.result, paymentMethod: action.payload.paymentMethod, invoiceDetails:action.payload.invoiceDetails }
                case REFUND_INVOICE_FAILURE:
                        return { ...state, loading: false }

                //view submitted invoice
                case PROVIDER_VIEW_INVOICE_SUBMITTED_REQUEST:
                        return { ...state, loading: true }
                case PROVIDER_VIEW_INVOICE_SUBMITTED_SUCCESS:
                        return { ...state, loading: false, selectedApplication: action.payload.selectedApplication, getInvoiceApplication: action.payload.getInvoiceApplication }
                case PROVIDER_VIEW_INVOICE_SUBMITTED_FAILURE:
                        return { ...state, loading: false }

                // invoice view start here
                case PROVIDER_VIEW_INVOICE_REQUEST:
                        return { ...state, loading: true }
                case PROVIDER_VIEW_INVOICE_SUCCESS:
                        return { ...state, loading: false }
                case PROVIDER_VIEW_INVOICE_FAILURE:
                        return { ...state, loading: false }
                // invoice view end here
                // submitted invoice start here
                case PROVIDER_SUBMITTED_REQUEST:
                        return { ...state, loading: true }
                case PROVIDER_SUBMITTED_SUCCESS:
                        return { ...state, loading: false, invoiceSubmittedList: action.payload.result }
                case PROVIDER_SUBMITTED_FAILURE:
                        return { ...state, loading: false }
                // submitted invoice end here
                // submit invoice start here
                case PROVIDER_INVOICE_SUBMIT_REQUEST:
                        return { ...state, loading: true, submitInvoice: false }
                case PROVIDER_INVOICE_SUBMIT_SUCCESS:
                        return { ...state, loading: false, submitInvoice: true, previewInvoice: undefined }
                case PROVIDER_INVOICE_SUBMIT_FAILURE:
                        return { ...state, loading: false, submitInvoice: false }
                // submit invoice end here
                // pdf document
                case PROVIDER_PDF_REQUEST:
                        return { ...state, loading: true }
                case PROVIDER_PDF_SUCCESS:
                        return { ...state, loading: false }
                case PROVIDER_PDF_FAILURE:
                        return { ...state, loading: false }
                // case get details for preview page
                case PROVIDER_SELECTED_SUCCESS:
                        var amount = action.payload.result.reduce(function (accumulator, currentValue, currentindex) {
                                if (currentindex == 0) {
                                        accumulator['total_amount'] = currentValue.loan_amount;
                                        accumulator['discount_amount'] = currentValue.hps_discount_amt;
                                } else {
                                        accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                                        accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
                                }
                                return accumulator
                        }, []);
                        return { ...state, loading: false, invoicePreviewList: action.payload.result, providerDetails: action.payload.provider, totalAmount: amount }
                case PROVIDER_SELECTED_FAILURE:
                        return { ...state, loading: false }
                case PROVIDER_PREVIEW_LOAD:
                        var checkSelected = (state.previewInvoice === undefined) ? true : false;
                        return { ...state, loading: true, submitInvoice: checkSelected }
                // send data on provide invoice page
                case PROVIDER_PREVIEW_INVOICE:
                        return { ...state, loading: false, previewInvoice: action.payload }
                // review application document start here
                case PROVIDER_PENDING_REQUEST:
                        return { ...state, loading: true, submitInvoice: false, previewInvoice: undefined };
                case PROVIDER_PENDING_SUCCESS:
                        return { ...state, loading: false, invoiceList: action.payload.result };
                case PROVIDER_PENDING_FAILURE:
                        return { ...state, loading: false };
                // review application document start here
                default: return { ...state };
        }
}
