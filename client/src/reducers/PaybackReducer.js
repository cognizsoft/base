/**
 * Auth User Reducers
 */
import {
    PAYBACK_LIST_SUCCESS,
    PAYBACK_LIST_FAILURE,
    PAYBACK_INSERT_SUCCESS,
    MASTER_DATA_VALUE_LIST_SUCCESS,
    MASTER_DATA_VALUE_LIST_FAILURE
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case PAYBACK_LIST_SUCCESS:
        	//console.log(action.payload)
            return { ...state, loading: false, payback: action.payload };

        case PAYBACK_LIST_FAILURE:
            return { ...state, loading: false };

        case MASTER_DATA_VALUE_LIST_SUCCESS:
            return { ...state, loading: false, payback_master_data_value: action.payload };

        case MASTER_DATA_VALUE_LIST_FAILURE:
            return { ...state, loading: false };

        case PAYBACK_INSERT_SUCCESS:
        	const filterType = state.payback_master_data_value.filter(x => x.mdv_id == action.payload.payback_type );


            let newPayback = {
                id: action.payload.last_value_id,
                payback_id: action.payload.payback_type,
                min_volume: action.payload.min_volume,
                max_volume: action.payload.max_volume,
                rate: action.payload.rate,
                status: action.payload.payback_status,
             }
             newPayback.payback_type = filterType[0].value;
            //console.log('newPayback')
            //console.log(newPayback)
            let users = state.payback;
            users.reverse();
            users.push(newPayback); 
            users.reverse();
            return { ...state, loading: false};

        default: return { ...state };
    }
}
