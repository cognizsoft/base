/**
 * Auth User Reducers
 */
import {
    USER_TYPE_LIST_SUCCESS,
    USER_TYPE_LIST_FAILURE,
    USER_TYPE_INSERT_SUCCESS,
    EXIST_UPDATE,
    EXIST_UPDATE_SUCCESS
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    loading: false
};

export default (state = INIT_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {

        case USER_TYPE_LIST_SUCCESS:
            return { ...state, loading: false, user_type: action.payload };

        case USER_TYPE_LIST_FAILURE:
            return { ...state, loading: false };

        case USER_TYPE_INSERT_SUCCESS:
            let newUser = {
                mdv_id: action.payload.last_type_id,
                value: action.payload.user_type,
                description: action.payload.user_type_desc,
                status: action.payload.user_type_status,
            }


            let users = state.user_type;
            users.reverse();
            users.push(newUser);
            users.reverse();
            
            return { ...state, loading: false, user_type_insert_id: action.payload };
        case EXIST_UPDATE:
            return { ...state, loading: false, typeExist: 0 };
        case EXIST_UPDATE_SUCCESS:
            return { ...state, loading: false, typeExist: action.payload.exist, isEdit: action.payload.edit };
        default: return { ...state };
    }
}
