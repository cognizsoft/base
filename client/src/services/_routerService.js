// routes
import Widgets from 'Routes/widgets';
import Pages from 'Routes/pages';
import AdvanceUIComponents from 'Routes/advance-ui-components';
import CalendarComponents from 'Routes/calendar';
import ChartsComponents from 'Routes/charts';
import FormElements from 'Routes/forms';
//import Users from 'Routes/users';
import Users from 'Admin/user-management';
import Components from 'Routes/components';
import Tables from 'Routes/tables';


import Icons from 'Routes/icons';
import Maps from 'Routes/maps';
import DragAndDrop from 'Routes/drag-drop';
import Editor from 'Routes/editor';
import Ecommerce from 'Routes/ecommerce';
import Dashboard from 'Routes/dashboard';
import ImageCropper from 'Routes/image-cropper';
import VideoPlayer from 'Routes/video-player';
import Dropzone from 'Routes/dropzone';

// async component
import {
	AsyncAboutUsComponent,
	AsyncChatComponent,
	AsyncMailComponent,
	AsyncTodoComponent,
	AsyncPermissionComponent,
	AsyncMasterDataComponent,
	AsyncMasterDataValueComponent,
	AsyncFeeDiscountRateComponent,
	AsyncFeeFinancialChargeComponent,
	AsyncFeeInterestRateComponent,
	AsyncFeeLateFeeWaiversComponent,
	AsyncFeeLateFeeAmountComponent,
	AsyncFeeSurchargeTypeComponent,
	AsyncSystemSettingSystemModuleComponent,
	AsyncSystemSettingSecurityQuestionsComponent,
	AsyncSystemSettingSecurityAnswersComponent,
	AsyncCountryCityComponent,
	AsyncCountryStatesComponent,
	AsyncCountryCountryComponent,
	AsyncOthersRiskFactorComponent,
	AsyncOthersScoreThresholdsComponent,
	AsyncProceduresComponent,
	AsyncProvidersAddNewComponent,
	AsyncProvidersListComponent,
	AsyncProvidersEditComponent,
	AsyncProvidersInvoiceListComponent,
	AsyncProvidersSingleInvoiceComponent,
	AsyncProvidersReportListComponent,
	AsyncPatientsAddNewComponent,
	AsyncPatientsListComponent,
	AsyncPatientsEditComponent,
	AsyncAdminCustomerEditComponent,
	AsyncCustomerInvoiceListComponent,
	AsyncCustomerSingleInvoiceComponent,
	AsyncCustomerReportListComponent,
	AsyncCreditApplicationsComponent,
	AsyncPaymentsOpenInvoiceComponent,
	AsyncPaymentsCloseInvoiceComponent,
	AsyncPaymentsViewInvoiceComponent,
	AsyncPaymentsSingleInvoiceComponent,
	AsyncViewCreditApplicationsComponent,
	AsyncEditCreditApplicationsComponent,
	AsyncViewCustomerComponent,
	AsyncViewProviderComponent,
	AsyncPaymenPlanComponent,
	AsyncCustomerPaymentPlanComponent,
	AsyncAddNewCreditApplicationComponent,
	AsyncEditCreditApplicationComponent,
	AsyncAddNewMPSCreditApplicationComponent,
	AsyncReviewCreditApplicationComponent,
	AsyncRejectCreditApplicationComponent,
	AsyncUploadCreditApplicationComponent,
	AsyncReviewDocumentCreditApplicationComponent,
	AsyncPaymentCreditApplicationComponent,
	AsyncAccountsLoanReportComponent,
	AsyncAccountsProviderInvoiceReportComponent,
	AsyncInstallmentInvoiceCreditApplicationComponent,
	AsyncInstallmentViewCreditApplicationComponent,
	AsyncAccountsWeekMonthInvoiceReportComponent,
	AsyncScoreRangeComponent,
	AsyncAdminApplicationPlanDetailsComponent,
	AsyncAdminApplicationAllDocumentsComponent,
	AsyncInvoiceViewComponent,
	AsyncPaymentReceiptComponent,
	AsyncAccountsPrintMultipalInvoicesComponent,

	AsyncAdminTermMonthComponent,
	AsyncAdminLoanAmountComponent,
	AsyncAdminInterestRateComponent,
	AsyncAdminApplicationAuthorizedComponent,
	AsyncAdminWithdrawalComponent,
	AsyncAdminProcedureDateComponent,
	AsyncAdminEmailTemplatesComponent,
	AsyncReviewPlanAgreementCreditApplicationComponent,
	AsyncPaymentOptionToCloseComponent,
	AsyncPatientsInvoicePauseComponent,

	AsyncAdminUploadPlanAgreementComponent,

	AsyncAdminCustomerSupportComponent,
	AsyncAdminProviderSupportComponent,
	AsyncAdminCustomerSupportViewTicketComponent,

	AsyncProRefundAdminInvoiceListComponent,

	AsyncUserManagementComponent,
	AsyncUsersTypeComponent,
	AsyncUsersRoleComponent,

	AsyncAdminAccountReceivablesComponent,
	AsyncAdminProviderSupportReportComponent,
	AsyncAdminCustomerSupportReportComponent,

	AsyncAdminAccountPayableComponent,
	AsyncAdminCustomerDocumentsComponent,
	AsyncAdminProviderDocumentsComponent,

	AsyncAdminCreditChargesComponent,
	AsyncAdminCancelPlanDetailComponent,
	AsyncAdminApplicationPlanDocumentsComponent,
	AsyncAdminApplicationDiskDocumentsComponent,
	AsyncCreditApplicationsReviewComponent,
} from 'Components/AsyncComponent/AsyncComponent';

export default [
	{
		path: 'dashboard',
		component: Dashboard
	},
	{
		path: 'user-management',
		component: Users
	},
	{
		path: 'user-management/users',
		component: AsyncUserManagementComponent
	},
	{
		path: 'user-management/users-type',
		component: AsyncUsersTypeComponent
	},
	{
		path: 'user-management/users-role',
		component: AsyncUsersRoleComponent
	},
	{
		path: 'permission',
		component: AsyncPermissionComponent
	},
	{
		path: 'system/master/master-data',
		component: AsyncMasterDataComponent
	},
	{
		path: 'system/master/master-data-value',
		component: AsyncMasterDataValueComponent
	},
	{
		path: 'system/fee/discount-rate',
		component: AsyncFeeDiscountRateComponent
	},
	{
		path: 'system/fee/financial-charges',
		component: AsyncFeeFinancialChargeComponent
	},
	{
		path: 'system/fee/interest-rate',
		component: AsyncFeeInterestRateComponent
	},
	{
		path: 'system/fee/late-fee-waivers',
		component: AsyncFeeLateFeeWaiversComponent
	},
	{
		path: 'system/fee/late-fee-amount',
		component: AsyncFeeLateFeeAmountComponent
	},
	{
		path: 'system/fee/surcharge-type',
		component: AsyncFeeSurchargeTypeComponent
	},
	{
		path: 'system/system-settings/system-module',
		component: AsyncSystemSettingSystemModuleComponent
	},
	{
		path: 'system/system-settings/security-questions',
		component: AsyncSystemSettingSecurityQuestionsComponent
	},
	{
		path: 'system/system-settings/security-answers',
		component: AsyncSystemSettingSecurityAnswersComponent
	},
	{
		path: 'system/country/city',
		component: AsyncCountryCityComponent
	},
	{
		path: 'system/country/states',
		component: AsyncCountryStatesComponent
	},
	{
		path: 'system/country/countries',
		component: AsyncCountryCountryComponent 
	},
	{
		path: 'system/others/risk-factor',
		component: AsyncOthersRiskFactorComponent
	},
	{
		path: 'system/others/score-thresholds',
		component: AsyncOthersScoreThresholdsComponent
	},
	{
		path: 'system/procedures',
		component: AsyncProceduresComponent
	},
	{
		path: 'providers/add-new',
		component: AsyncProvidersAddNewComponent
	},
	{
		path: 'providers/provider-list',
		component: AsyncProvidersListComponent
	},
	{
		path: 'providers/provider-edit/:id',
		component: AsyncProvidersEditComponent
	},
	{
		path: 'providers/invoice-list',
		component: AsyncProvidersInvoiceListComponent
	},
	{
		path: 'providers/invoice/:id',
		component: AsyncProvidersSingleInvoiceComponent
	},
	{
		path: 'providers/report-list',
		component: AsyncProvidersReportListComponent
	},
	{
		path: 'credit-application/edit/:app_id',
		component: AsyncPatientsAddNewComponent
	},
	{
		path: 'customers/customers-list',
		component: AsyncPatientsListComponent
	},
	{
		path: 'customers/edit',
		component: AsyncPatientsEditComponent
	},
	{
		path: 'customers/customer-edit/:id',
		component: AsyncAdminCustomerEditComponent
	},
	{
		path: 'customers/invoice-list',
		component: AsyncCustomerInvoiceListComponent
	},
	{
		path: 'customers/invoice/:id',
		component: AsyncCustomerSingleInvoiceComponent
	},
	{
		path: 'customers/report-list',
		component: AsyncCustomerReportListComponent
	},
	{
		path: 'credit-applications/:status_id',
		component: AsyncCreditApplicationsComponent
	},
	{
		path: 'accounts/open-invoice',
		component: AsyncPaymentsOpenInvoiceComponent
	},
	{
		path: 'accounts/close-invoice',
		component: AsyncPaymentsCloseInvoiceComponent
	},
	{
		path: 'accounts/view-invoice/:id',
		component: AsyncPaymentsViewInvoiceComponent
	},
	{
        path: 'accounts/invoice/:id',
        component: AsyncPaymentsSingleInvoiceComponent
    },
	{
		path: 'credit-application/edit-application',
		component: AsyncEditCreditApplicationsComponent
	},
	{
		path: 'credit-application/application/:id',
		component: AsyncViewCreditApplicationsComponent
	},
	{
		path: 'customers/view-customer/:id',
		component: AsyncViewCustomerComponent
	},
	{
		path: 'providers/provider-view/:id',
		component: AsyncViewProviderComponent
	},
	{
		path: 'credit-application/customer-plan/:id',
		component: AsyncPaymenPlanComponent
	},
	/*{
		path: 'credit-application/payment-plan/:id',
		component: AsyncPaymenPlanComponent
	},*/
	{
		path: 'customers/customerPayment-plan/:id',
		component: AsyncCustomerPaymentPlanComponent
	},
	{
		path: 'credit-application/add-new',
		component: AsyncAddNewCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/edit',
		component: AsyncEditCreditApplicationComponent
	},
	{
		path: 'credit-application/add-new-MPS',
		component: AsyncAddNewMPSCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/review/:id',
		component: AsyncReviewCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/reject/:id',
		component: AsyncRejectCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/credit-authorized/:id',
		component: AsyncAdminApplicationAuthorizedComponent
	},
	{
		path: 'credit-application/upload/:customer_id/:id',
		component: AsyncUploadCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/review-document/:id',
		component: AsyncReviewDocumentCreditApplicationComponent
	},
	{
		path: 'credit-application/payment/:invoice_id',
		component: AsyncPaymentCreditApplicationComponent
	},
	{
		path: 'accounts/loan-reports',
		component: AsyncAccountsLoanReportComponent
	},
	{
		path: 'accounts/provider-invoice-reports/:status_id',
		component: AsyncAccountsProviderInvoiceReportComponent
	},
	{
		path: 'credit-application/invoice/:invoice_id',
		component: AsyncInstallmentInvoiceCreditApplicationComponent
	},
	{
		path: 'credit-application/installment/view/:appid/:planid/:instlmntid',
		component: AsyncInstallmentViewCreditApplicationComponent
	},
	{
		path: 'accounts/week-month-reports/:appid',
		component: AsyncAccountsWeekMonthInvoiceReportComponent
	},
	{
		path: 'system/fee/score-range',
		component: AsyncScoreRangeComponent
	},
	{
		path: 'credit-application/plan-details/:appid',
		component: AsyncAdminApplicationPlanDetailsComponent
	},
	{
		path: 'credit-application/all-documents/:id',
		component: AsyncAdminApplicationAllDocumentsComponent
	},
	{
		path: 'provider-invoice-reports/view/:invoice_id/:provider_id',
		component: AsyncInvoiceViewComponent
	},
	{
		path: 'credit-application/receipt/:appid',
		component: AsyncPaymentReceiptComponent
	},
	{
		path: 'accounts/print-multiple-invoices',
		component: AsyncAccountsPrintMultipalInvoicesComponent
	},
	{
		path: 'system/term-month',
		component: AsyncAdminTermMonthComponent
	},
	{
		path: 'system/loan-amount',
		component: AsyncAdminLoanAmountComponent
	},
	{
		path: 'system/interest-rate',
		component: AsyncAdminInterestRateComponent
	},
	{
		path: 'accounts/customer-withdrawal-reports',
		component: AsyncAdminWithdrawalComponent
	},
	{
		path: 'system/email-templates',
		component: AsyncAdminEmailTemplatesComponent
	},
	{
		path: 'accounts/procedure-date-reports',
		component: AsyncAdminProcedureDateComponent
	},
	{
		path: 'credit-application/plan/review-agreement/:planid',
		component: AsyncReviewPlanAgreementCreditApplicationComponent
	},
	{
		path: 'credit-application/option-to-close/:ppid',
		component: AsyncPaymentOptionToCloseComponent
	},
	{
		path: 'customers/customers-invoice-pause/:patient_id',
		component: AsyncPatientsInvoicePauseComponent
	},
	{
		path: 'credit-application/upload-agreement/:planid',
		component: AsyncAdminUploadPlanAgreementComponent
	},
	{
		path: 'customer-support/all/:statusType',
		component: AsyncAdminCustomerSupportComponent
	},
	{
		path: 'customer-support/view-ticket/:ticket_id',
		component: AsyncAdminCustomerSupportViewTicketComponent
	},
	{
		path: 'invoice/refund-invoices/:id',
		component: AsyncProRefundAdminInvoiceListComponent
	},
	{
		path: 'accounts/account-receivables',
		component: AsyncAdminAccountReceivablesComponent
	},
	{
		path: 'customer-support/provider-support-reports',
		component: AsyncAdminProviderSupportReportComponent
	},
	{
		path: 'customer-support/customers-support-reports',
		component: AsyncAdminCustomerSupportReportComponent
	},
	{
		path: 'accounts/accout-payable',
		component: AsyncAdminAccountPayableComponent
	},
	{
		path: 'customers/all-documents/:patient_id',
		component: AsyncAdminCustomerDocumentsComponent
	},
	{
		path: 'providers/all-documents/:provider_id',
		component: AsyncAdminProviderDocumentsComponent
	},
	{
		path: 'accounts/credit-charges',
		component: AsyncAdminCreditChargesComponent
	},
	{
		path: 'credit-application/plan/details/:ppid',
		component: AsyncAdminCancelPlanDetailComponent
	},
	{
		path: 'provider-support/all/:statusType',
		component: AsyncAdminProviderSupportComponent
	},
	{
		path: 'credit-application/plan-documents/:id',
		component: AsyncAdminApplicationPlanDocumentsComponent
	},
	{
		path: 'credit-application/view-disk/:id',
		component: AsyncAdminApplicationDiskDocumentsComponent
	},
	{
		path: 'application/review',
		component: AsyncCreditApplicationsReviewComponent
	},
]
