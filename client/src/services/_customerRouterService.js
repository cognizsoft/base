
import Dashboard from 'Routes/dashboard';

import {
	//AsyncProviderDashboardComponent,
	AsyncCustomerProfileComponent,
	AsyncCustomerCreditApplicationComponent,
	AsyncCustomerInstallmentViewComponent,
	AsyncCustomerInstallmentReceiptComponent,
	AsyncCustomerInstallmentPaymentComponent,
	AsyncCustomerEditProfileComponent,
	AsyncCustomerInvoiceComponent,
	AsyncCustomerSecurityAnswerComponent,
	AsyncCustomerSupportViewTicketComponent,
	AsyncCustomerSupportTicketComponent

} from 'Components/AsyncComponent/AsyncComponent';

export default [
	{
		path: 'dashboard',
		component: AsyncCustomerCreditApplicationComponent
	},
	{
		path: 'profile',
		component: AsyncCustomerProfileComponent
	},
	{
		path: 'credit-application/installment/view/:appid/:planid/:instlmntid',
		component: AsyncCustomerInstallmentViewComponent
	},
	{
		path: 'credit-application/installment/receipt/:invoice_id',
		component: AsyncCustomerInstallmentReceiptComponent
	},
	{
		path: 'credit-application/installment/payment/:invoice_id',
		component: AsyncCustomerInstallmentPaymentComponent
	},
	{
		path: 'edit-profile/:id',
		component: AsyncCustomerEditProfileComponent
	},
	{
		path: 'invoice/week-month-reports/:appid',
		component: AsyncCustomerInvoiceComponent
	},
	{
		path: 'security-answers',
		component: AsyncCustomerSecurityAnswerComponent
	},
	{
		path: 'customer-support/view-ticket/:ticket_id',
		component: AsyncCustomerSupportViewTicketComponent
	},
	{
		path: 'support-ticket',
		component: AsyncCustomerSupportTicketComponent
	},
]

