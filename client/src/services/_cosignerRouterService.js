
import Dashboard from 'Routes/dashboard';

import {
	//AsyncProviderDashboardComponent,
	AsyncCosignerProfileComponent,
	AsyncCosignerCreditApplicationComponent,
	AsyncCosignerViewCreditApplicationsComponent,
	AsyncCosignerInstallmentPaymentComponent,
	AsyncCosignerInstallmentReceiptComponent,
	AsyncCosignerSecurityAnswerComponent,
	AsyncCosignerInvoiceComponent,
	AsyncCosignerEditProfileComponent,
	AsyncCosignerSupportTicketComponent,
	AsyncCosignerSupportViewTicketComponent,
	AsyncCustomerSecurityAnswerComponent,
	AsyncCustomerSupportViewTicketComponent

} from 'Components/AsyncComponent/AsyncComponent';

export default [
	{
		path: 'dashboard',
		component: AsyncCosignerCreditApplicationComponent
	},
	{
		path: 'credit-application/plan-details/:appid',
		component: AsyncCosignerViewCreditApplicationsComponent
	},
	{
		path: 'credit-application/installment/payment/:invoice_id',
		component: AsyncCosignerInstallmentPaymentComponent
	},
	{
		path: 'credit-application/installment/receipt/:invoice_id',
		component: AsyncCosignerInstallmentReceiptComponent
	},
	{
		path: 'security-answers',
		component: AsyncCosignerSecurityAnswerComponent
	},
	{
		path: 'invoice/week-month-reports/:appid',
		component: AsyncCosignerInvoiceComponent
	},
	{
		path: 'profile',
		component: AsyncCosignerProfileComponent
	},
	{
		path: 'edit-profile/:id',
		component: AsyncCosignerEditProfileComponent
	},
	{
		path: 'support-ticket',
		component: AsyncCosignerSupportTicketComponent
	},
	{
		path: 'customer-support/view-ticket/:ticket_id',
		component: AsyncCosignerSupportViewTicketComponent
	},
]

