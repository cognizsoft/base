
import Dashboard from 'Routes/dashboard';

import {
	AsyncProviderDashboardComponent,
	AsyncProviderUsersComponent,
	AsyncProviderCreditApplicationsComponent,
	AsyncProivderViewCreditApplicationsComponent,
	AsyncProviderEditCreditApplicationsComponent,
	AsyncProviderPaymenPlanComponent,
	AsyncProPatientsAddNewComponent,
	AsyncProPatientsListComponent,
	AsyncProPatientsViewComponent,
	AsyncProPatientsEditComponent,
	AsyncProCreateInvoiceListComponent,
	AsyncProPreviewInvoiceListComponent,
	AsyncProSubmittedInvoiceListComponent,
	AsyncProInvoiceComponent,
	AsyncProCreditApplicationAddNewComponent,
	AsyncProCreditApplicationEditComponent,
	AsyncProviderCustomerPaymentPlanComponent,
	AsyncProviderReviewCreditApplicationComponent,
	AsyncProviderRejectCreditApplicationComponent,
	AsyncProviderUploadCreditApplicationComponent,
	AsyncProviderReviewDocumentCreditApplicationComponent,
	AsyncProviderCustomerAgreementComponent,
    AsyncProviderCustomerAgreementViewComponent,
	AsyncProviderProfileComponent,
	AsyncProviderSearchApplicationComponent,
	AsyncProviderSearchCustomerComponent,
	AsyncProviderPlanEstimateComponent,
	AsyncProviderApplicationPlanDetailsComponent,
	AsyncProviderApplicationAuthorizedComponent,
	AsyncProviderApplicationDeclinedComponent,
	AsyncProviderApplicationManualComponent,
	AsyncProviderApplicationAllDocumentsComponent,

	AsyncProviderEditProfileComponent,
	AsyncProviderCareCloudComponent,
	AsyncProviderInvoiceViewComponent,
	AsyncProviderSecurityAnswerComponent,

	AsyncProviderUploadPlanAgreementComponent,
	AsyncProviderPlanListComponent,
	AsyncProviderCustomerSupportViewTicketComponent,
	AsyncProRefundInvoiceListComponent,
} from 'Components/AsyncComponent/AsyncComponent';

export default [
	{
		path: 'dashboard',
		component: AsyncProviderDashboardComponent
	},
	{
		path: 'users',
		component: AsyncProviderUsersComponent
	},
	{
		path: 'credit-applications/:status_id',
		component: AsyncProviderCreditApplicationsComponent
	},
	
	{
		path: 'credit-application/application/:id',
		component: AsyncProivderViewCreditApplicationsComponent
	},
	{
		path: 'credit-applications/edit-application',
		component: AsyncProviderEditCreditApplicationsComponent
	},
	{
		path: 'credit-application/payment-plan/:id',
		component: AsyncProviderPaymenPlanComponent
	},
	{
		path: 'customers/customerPayment-plan/:customer_id/:application_id',
		component: AsyncProviderCustomerPaymentPlanComponent
	},
	{
		path: 'customers/add-new',
		component: AsyncProPatientsAddNewComponent
	},
	{
		path: 'customers/customers-list',
		component: AsyncProPatientsListComponent
	},
	{
		path: 'customers/view-customer/:id',
		component: AsyncProPatientsViewComponent
	},
	{
		path: 'customers/edit',
		component: AsyncProPatientsEditComponent
	},
	{
		path: 'invoice/create-invoice',
		component: AsyncProCreateInvoiceListComponent
	},
	{
		path: 'invoice/preview-invoice',
		component: AsyncProPreviewInvoiceListComponent
	},
	{
		path: 'invoice/submitted-invoices/:status_id',
		component: AsyncProSubmittedInvoiceListComponent
	},
	{
		path: 'invoice/invoice',
		component: AsyncProInvoiceComponent
	},
	{
		path: 'credit-application/add-new',
		component: AsyncProCreditApplicationAddNewComponent
	},
	{
		path: 'credit-applications/edit',
		component: AsyncProCreditApplicationEditComponent
	},
	{
		path: 'credit-application/review/:id',
		component: AsyncProviderReviewCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/reject/:id',
		component: AsyncProviderRejectCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/upload/:customer_id/:id',
		component: AsyncProviderUploadCreditApplicationComponent
	}
	,
	{
		path: 'credit-application/review-document/:id',
		component: AsyncProviderReviewDocumentCreditApplicationComponent
	},
	{
		path: 'customers/agreement-plan/:id',
		component: AsyncProviderCustomerAgreementComponent
	},
	{
		path: 'agreement',
		component: AsyncProviderCustomerAgreementViewComponent
	},
	{
		path: 'profile',
		component: AsyncProviderProfileComponent
	},
	{
		path: 'search-application',
		component: AsyncProviderSearchApplicationComponent
	},
	{
		path: 'search-customer',
		component: AsyncProviderSearchCustomerComponent
	},
	{
		path: 'plan-estimate',
		component: AsyncProviderPlanEstimateComponent
	},
	{
		path: 'credit-application/plan-details/:appid',
		component: AsyncProviderApplicationPlanDetailsComponent
	},
	{
		path: 'credit-application/credit-authorized/:id',
		component: AsyncProviderApplicationAuthorizedComponent
	},
	{
		path: 'credit-application/credit-declined/:id',
		component: AsyncProviderApplicationDeclinedComponent
	},
	{
		path: 'credit-application/credit-manual/:id',
		component: AsyncProviderApplicationManualComponent
	},
	{
		path: 'credit-application/all-documents/:id',
		component: AsyncProviderApplicationAllDocumentsComponent
	},
	{
		path: 'edit-profile/:id',
		component: AsyncProviderEditProfileComponent
	},
	{
		path: 'care-cloud',
		component: AsyncProviderCareCloudComponent
	},
	{
		path: 'invoice/submitted/view/:invoice_id/:provider_id',
		component: AsyncProviderInvoiceViewComponent
	},
	{
		path: 'security-answers',
		component: AsyncProviderSecurityAnswerComponent
	},
	{
		path: 'credit-application/upload-agreement/:planid',
		component: AsyncProviderUploadPlanAgreementComponent
	},
	{
		path: 'credit-application/surgery-status-update',
		component: AsyncProviderPlanListComponent
	},
	{
		path: 'customer-support/view-ticket/:ticket_id',
		component: AsyncProviderCustomerSupportViewTicketComponent
	},
	{
		path: 'invoice/refund-invoices/:id',
		component: AsyncProRefundInvoiceListComponent
	},
	
]

