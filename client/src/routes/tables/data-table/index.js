/**
 * Data Table
 */
import React from 'react';
import MUIDataTable from "mui-datatables";
import MaterialDatatable from "material-datatable";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// intl messages
import IntlMessages from 'Util/IntlMessages';

class DataTable extends React.Component {
	render() {
		const columns = [
			{   
				name: 'Name', 
				field: 'name',
			},
			{
				name: 'Title', 
				field: 'title'
			},
			{
				name: 'Location', 
				field: 'location',
			},
			{
				name: 'Age', 
				field: 'age'
			},
			{
				name: 'Salary', 
				field: 'salary'
			},
			
		];
		const data = [
			{name: "Name 1", title: "Title 1", location: "Location 1", age: 30, salary: 10},
			{name: "Name 2", title: "Title 2", location: "Location 2", age: 31, salary: 11},
		];
		
		const options = {
			filterType: 'dropdown',
		};
		return (
			<div className="data-table-wrapper">
				<PageTitleBar title={<IntlMessages id="sidebar.dataTable" />} match={this.props.match} />
				<div className="alert alert-info">
					<p>MUI-Datatables is a data tables component built on Material-UI V1.
            It comes with features like filtering, view/hide columns, search, export to CSV download, printing, pagination, and sorting.
            On top of the ability to customize styling on most views, there are two responsive modes "stacked" and "scroll" for mobile/tablet
            devices. If you want more customize option please <a href="https://github.com/gregnb/mui-datatables" className="btn btn-danger btn-small mx-10">Click </a> here</p>
				</div>
				<RctCollapsibleCard heading="Data Table" fullBlock>
				<MaterialDatatable
    title={"Employee List"}
    data={data}
    columns={columns}
    options={options}
/>
				</RctCollapsibleCard>
			</div>
		);
	}
}

export default DataTable;
