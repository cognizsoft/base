/**
 * Ecommerce Dashboard
 */

import React, { Component } from 'react'
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct collapsible card
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import {
	SupportOrdersWidget,
	InvoiceDashboard,
	ApplicationDashboard,
	PaymentDashboard,
	SupportChartDashboard,
} from "Admin/Widgets";



export default class healthDashboard extends Component {
	getMuiTheme = () => createMuiTheme({
		overrides: {
			MuiIconButton: {
				root: {
					'padding': '5px',
				}
			},
		}
	})
	render() {
		const { match } = this.props;
		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>Health Partner Dashboard</title>
					<meta name="description" content="Health Partner Dashboard" />
				</Helmet>
				<PageTitleBar title={<IntlMessages id="sidebar.dashboard" />} match={match} />
				<MuiThemeProvider theme={this.getMuiTheme()}>
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
							heading={<IntlMessages id="widgets.dashboardInvoiceOrder" />}
							collapsible
							reloadable
							closeable
							fullBlock
							customClasses="overflow-hidden"
						>
							<InvoiceDashboard />
						</RctCollapsibleCard>
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
							heading={<IntlMessages id="widgets.dashboardAdminPayment" />}
							collapsible
							reloadable
							closeable
							fullBlock
							customClasses="overflow-hidden"
						>
							<PaymentDashboard />
						</RctCollapsibleCard>
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
							heading={<IntlMessages id="widgets.dashboardCreditApplication" />}
							collapsible
							reloadable
							closeable
							fullBlock
							customClasses="overflow-hidden"
						>
							<ApplicationDashboard />
						</RctCollapsibleCard>
						
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
							heading={<IntlMessages id="widgets.dashboardSupportChart" />}
							collapsible
							reloadable
							closeable
							fullBlock
						>
							<SupportChartDashboard />
						</RctCollapsibleCard>

						{/*<RctCollapsibleCard
						colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardCreditApplication" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						<RecentOrdersWidget />
					</RctCollapsibleCard>*/}
					</div>
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12 w-xs-full"
							heading={<IntlMessages id="widgets.dashboardSupportOrders" />}
							collapsible
							reloadable
							closeable
							fullBlock
						>
							<SupportOrdersWidget />
						</RctCollapsibleCard>
					</div>
				</MuiThemeProvider>

			</div>
		)
	}
}
