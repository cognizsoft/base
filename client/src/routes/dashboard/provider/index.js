/**
 * Ecommerce Dashboard
 */

import React, { Component } from 'react'
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct collapsible card
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import {
	SupportOrdersWidget,
	InvoiceDashboard,
	ApplicationDashboard,
	PaymentDashboard,
} from "Provider/Widgets";



export default class providerDashboard extends Component {

	render() {
		const { match } = this.props;
		return (
			<div className="ecom-dashboard-wrapper">
				<Helmet>
					<title>Health Partner Provider Dashboard</title>
					<meta name="description" content="Health Partner Dashboard" />
				</Helmet>
				<PageTitleBar title={<IntlMessages id="sidebar.dashboard" />} match={match} />
				
				<div className="row">
					
					
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardProviderInvoiceOrder" />}
						collapsible
						reloadable
						closeable
						fullBlock
						customClasses="overflow-hidden"
					>
						<InvoiceDashboard />
					</RctCollapsibleCard>
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardCreditApplication" />}
						collapsible
						reloadable
						closeable
						fullBlock
						customClasses="overflow-hidden"
					>
						<ApplicationDashboard />
					</RctCollapsibleCard>
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardPayment" />}
						collapsible
						reloadable
						closeable
						fullBlock
						customClasses="overflow-hidden"
					>
						<PaymentDashboard />
					</RctCollapsibleCard>
					{/*<RctCollapsibleCard
						colClasses="col-sm-12 col-md-4 col-lg-4 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardCreditApplication" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						<RecentOrdersWidget />
					</RctCollapsibleCard>*/}
				</div>
				<div className="row">
					{/*<RctCollapsibleCard
						colClasses="col-sm-12 col-md-6 col-lg-6 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardInvoiceOrder" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						<InvoiceOrdersWidget />
					</RctCollapsibleCard>*/}

					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-12 w-xs-full"
						heading={<IntlMessages id="widgets.dashboardSupportOrders" />}
						collapsible
						reloadable
						closeable
						fullBlock
					>
						<SupportOrdersWidget />
					</RctCollapsibleCard>
				</div>
				
				
				
			</div>
		)
	}
}
