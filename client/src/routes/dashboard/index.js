/**
 * Dasboard Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// async components
import {
   AsyncEcommerceDashboardComponent,
   AsyncSaasDashboardComponent,
   AsyncAgencyDashboardComponent,
   AsyncNewsDashboardComponent,
   AsyncHealthDashboardComponent
} from 'Components/AsyncComponent/AsyncComponent';
import {currentUserRole} from '../../apifile';

const Dashboard = ({ match }) => (
   <div className="dashboard-wrapper">
      <Switch>
         {(currentUserRole() != 'HPS')?
         <Route path={`${match.url}`} component={AsyncAgencyDashboardComponent} />
         :
         <Route path={`${match.url}`} component={AsyncHealthDashboardComponent} />
         }
         <Route path={`${match.url}/saas`} component={AsyncSaasDashboardComponent} />
         <Route path={`${match.url}/agency`} component={AsyncAgencyDashboardComponent} />
         <Route path={`${match.url}/news`} component={AsyncNewsDashboardComponent} />
      </Switch>
   </div>
);

export default Dashboard;
