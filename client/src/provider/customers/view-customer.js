/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import {
  Collapse
} from 'reactstrap';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
  viewCustomer
} from 'Actions';
class PatientView extends Component {

  state = {
    all: false,
    collapse_sec: false,
    collapse_cus_info: false,
    collapse_app_info: false,
    collapse_add: false,
    collapse_emp_info: false,
    collapse_bank_info: false,
    collapse_user_info: false,
  }


  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 13,2019
  */
  componentDidMount() {
    //this.permissionFilter(this.state.currentModule);
    this.props.viewCustomer(this.props.match.params.id);
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  /****Collpase secondary contact details****/
  onCollapse() {
    this.setState({ collapse_sec: !this.state.collapse_sec });
  }
  onCollapseCusInfo() {
    this.setState({ collapse_cus_info: !this.state.collapse_cus_info });
  }
  onCollapseappInfo() {
    this.setState({ collapse_app_info: !this.state.collapse_app_info });
  }
  onCollapseAddInfo() {
    this.setState({ collapse_add: !this.state.collapse_add });
  }
  onCollapseEmpInfo() {
    this.setState({ collapse_emp_info: !this.state.collapse_emp_info });
  }
  onCollapseBankInfo() {
    this.setState({ collapse_bank_info: !this.state.collapse_bank_info });
  }
  onCollapseUserInfo() {
    this.setState({ collapse_user_info: !this.state.collapse_user_info });
  }
  /****Collpase secondary contact details****/
  render() {
    const { loading, customerDetails, customerDetailsAddress } = this.props;
    return (
      <div className="country regions">
        <Helmet>
          <title>Health Partner | Providers | Add New</title>
          <meta name="description" content="Regions" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.viewCustomer" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          {customerDetails &&
            <div className="table-responsive">
              <div className="text-right pr-10 pt-10">
                <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
              </div>
              <div className="modal-body page-form-outer view-section pt-0">
                <div className="col-sm-12 w-xs-full">
                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>Customer Information</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapseCusInfo()}><i className={(this.state.collapse_cus_info) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_cus_info}>
                      <div className="rct-block-content">
                        <div className="width-100">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td className="fw-bold">First Name:</td>
                                <td>{customerDetails.f_name}</td>
                                <td className="fw-bold">Status:</td>
                                <td>{(customerDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Middle Name:</td>
                                <td>{(customerDetails.m_name != '') ? customerDetails.m_name : '-'}</td>
                                <td className="fw-bold">Primary Email Address:</td>
                                <td>{customerDetails.email}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Last Name:</td>
                                <td>{customerDetails.l_name}</td>
                                <td className="fw-bold">Secondary Email Address:</td>
                                <td>{(customerDetails.secondary_email) ? customerDetails.secondary_email : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Date of Birth:</td>
                                <td>{customerDetails.dob}</td>
                                <td className="fw-bold">Gender:</td>
                                <td>{(customerDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Primary Phone No.:</td>
                                <td>{customerDetails.peimary_phone}</td>
                                <td className="fw-bold">Customer A/C:</td>
                                <td>{customerDetails.patient_ac}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Secondary Phone No.:</td>
                                <td>{(customerDetails.alternative_phone) ? customerDetails.alternative_phone : '-'}</td>
                                <td className="fw-bold">SSN / Tax ID:</td>
                                <td>
                                  {/*(customerDetails.ssn !== undefined) ? (!this.state.showSsn) ? customerDetails.ssn.replace(/.(?=.{4})/g, 'x') : customerDetails.ssn : '-'*/}
                                  xxxxxxxxxxx
                                </td>
                              </tr>
                            </tbody>

                          </table>
                        </div>
                      </div>
                    </Collapse>
                  </div>

                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>Bank Details</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapseBankInfo()}><i className={(this.state.collapse_bank_info) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_bank_info}>
                      <div className="rct-block-content">
                        {this.props.bankDetails && this.props.bankDetails.map((bank, idx) => (
                          <div className="width-100" key={idx}>
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Bank Name:</td>
                                  <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                                  <td className="fw-bold">Name on Account:</td>
                                  <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Routing Number:</td>
                                  <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                                  <td className="fw-bold">Bank A/C Type:</td>
                                  <td>{(bank.value) ? bank.value : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Bank A/C#:</td>
                                  <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                                  <td className="fw-bold">Bank Address</td>
                                  <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                                </tr>
                              </tbody>
                            </table>
                            <hr className="border-dark" />
                          </div>
                        ))
                        }
                      </div>

                    </Collapse>
                  </div>

                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>Secondary Contact Details</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapse()}><i className={(this.state.collapse_sec) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_sec}>
                      <div className="rct-block-content">
                        {this.props.secDetails && this.props.secDetails.map((secDetails, idx) => (
                          <div className="width-100 mb-10" key={idx}>
                            <table className="table">
                              {this.props.secDetails &&
                                <tbody>
                                  <tr>
                                    <td className="fw-bold">First Name:</td>
                                    <td>{secDetails.f_name}</td>
                                    <td className="fw-bold">Middle Name:</td>
                                    <td>{(secDetails.m_name != '') ? secDetails.m_name : '-'}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Last Name:</td>
                                    <td>{secDetails.l_name}</td>
                                    <td className="fw-bold">Relationship:</td>
                                    <td>{secDetails.relationship_name}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Phone:</td>
                                    <td>{secDetails.phone}</td>
                                    <td className="fw-bold">Email:</td>
                                    <td>{secDetails.email}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Address:</td>
                                    <td>{secDetails.address1 + ' ' + secDetails.address2}</td>
                                    <td className="fw-bold">City:</td>
                                    <td>{secDetails.city}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">State:</td>
                                    <td>{secDetails.state_name}</td>
                                    <td className="fw-bold">Country</td>
                                    <td>{secDetails.country_name}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Zip Code:</td>
                                    <td>{secDetails.zip_code}</td>
                                    <td className="fw-bold"></td>
                                    <td></td>
                                  </tr>
                                </tbody>
                              }

                            </table>
                            <hr className="border-dark" />
                          </div>
                        ))
                        }
                        {(this.props.secDetails == "") &&
                          <div className="width-100 mb-10">
                            <table className="table">
                              <tbody><tr><td colSpan="4">Secondary contact details not available.</td></tr></tbody>
                            </table>
                          </div>
                        }
                      </div>
                    </Collapse>
                  </div>


                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>Locations</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapseAddInfo()}><i className={(this.state.collapse_add) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_add}>
                      <div className="rct-block-content">
                        {this.props.appDetailsAddress && this.props.appDetailsAddress.map((address, idx) => (
                          <div className="width-100 mb-10" key={idx}>
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Address1:</td>
                                  <td>{address.address1}</td>
                                  <td className="fw-bold">City:</td>
                                  <td>{address.city}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address2:</td>
                                  <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                  <td className="fw-bold">Zip Code:</td>
                                  <td>{address.zip_code}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Country:</td>
                                  <td>{address.country_name}</td>
                                  <td className="fw-bold">How long at this address?:</td>
                                  <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">State:</td>
                                  <td>{address.state_name}</td>
                                  <td className="fw-bold">Phone No:</td>
                                  <td>{address.phone_no}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Primary Address:</td>
                                  <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                  <td className="fw-bold">Billing and Physical address same:</td>
                                  <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                                </tr>
                                {/*<tr>
                              <td className="fw-bold">County:</td>
                              <td>{(address.county != '') ? address.county : '-'}</td>
                              <td className="fw-bold">Region:</td>
                              <td>{address.region_name}</td>                              
                            </tr>*/}
                              </tbody>
                            </table>
                            <hr className="border-dark" />
                          </div>
                        ))
                        }
                      </div>
                    </Collapse>
                  </div>

                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>Employment Information</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapseEmpInfo()}><i className={(this.state.collapse_emp_info) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_emp_info}>
                      <div className="rct-block-content">
                        <div className="width-100">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td className="fw-bold">Employed:</td>
                                <td>{(customerDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                                <td className="fw-bold">Employer Name:</td>
                                <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_name : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Employment Type:</td>
                                <td>{(customerDetails.employment_status == 1) ? customerDetails.emp_type_name : '-'}</td>
                                <td className="fw-bold">Employer Phone No:</td>
                                <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_phone : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Annual Income($):</td>
                                <td>{(customerDetails.employment_status == 1) ? '$' + customerDetails.annual_income : '-'}</td>
                                <td className="fw-bold">Employer Email Address:</td>
                                <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_email : '-'}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Employed Since:</td>
                                <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_since : '-'}</td>
                                <td className="fw-bold">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                            </tbody>

                          </table>
                        </div>
                      </div>
                    </Collapse>
                  </div>


                  <div className="rct-block custom-collapse">
                    <div className="rct-block-title">
                      <h4><span>User Information</span></h4>
                      <div className="contextual-link">
                        <a href="javascript:void(0)" onClick={() => this.onCollapseUserInfo()}><i className={(this.state.collapse_user_info) ? "ti-minus" : "ti-plus"}></i></a>
                      </div>
                    </div>
                    <Collapse isOpen={this.state.collapse_user_info}>
                      <div className="rct-block-content">
                        <div className="width-100">
                          <table className="table">
                            <tbody>
                              <tr>
                                <td className="fw-bold">Username:</td>
                                <td>{customerDetails.username}</td>
                                <td className="fw-bold">Primary Phone No.:</td>
                                <td>{customerDetails.user_phone1}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Email Address:</td>
                                <td>{customerDetails.user_eamil}</td>
                                <td className="fw-bold">Secondary Phone No.:</td>
                                <td>{(customerDetails.user_phone2 != '') ? customerDetails.user_phone2 : '-'}</td>
                              </tr>
                            </tbody>

                          </table>
                        </div>
                      </div>
                    </Collapse>
                  </div>


                </div>
              </div>
            </div>
          }

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div>
    );
  }
}
const mapStateToProps = ({ Customer }) => {
  const {
    loading,
    customerDetails,
    appDetailsAddress,
    secDetails,
    experianEmployment,
    experianAddress,
    bankDetails,
  } = Customer;
  return {
    loading,
    customerDetails,
    appDetailsAddress,
    secDetails,
    experianEmployment,
    experianAddress,
    bankDetails,
  }
}
export default connect(mapStateToProps, {
  viewCustomer
})(PatientView);