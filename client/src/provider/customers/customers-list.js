/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import MaterialDatatable from "material-datatable";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import {
   customerListProvider
} from 'Actions';

class ProviderPatientList extends Component {

   state = {
      currentModule: 25,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 06,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.customerListProvider();
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      const customerDetails = this.props.customerDetails;
      const columns = [
         { name: 'ID', field: 'patient_id', },
         { name: 'A/C No', field: 'patient_ac', },
         {
            name: 'Full Name',
            field: 'f_name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  )
               },
            }
         },
         { name: 'Address', field: 'address1', },
         { name: 'City', field: 'City', },
         { name: 'State', field: 'state_name', },
         { name: 'Zip', field: 'zip_code', },
         /*{
            name: 'Provider',
            field: 'name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.name != null) ? value.name : '-'
                  )
               },
            }
         },*/
         
         { name: 'Email', field: 'email', },
         { name: 'Primary Phone', field: 'peimary_phone', },
         {
            name: 'Status',
            field: 'status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  
                  return (
                     <React.Fragment>
                        <span className="d-flex justify-content-start">
                           <div className="status">
                              <span className="d-block">{(value.status == '1') ? 'Active' : 'Inactive'}</span>
                           </div>
                        </span>
                     </React.Fragment>
                  )
               },
               customValue: (value, tableMeta, updateValue) => {
                  return (value.status == '1') ? 'Active' : 'Inactive';
               },
               customSortValue: (value, tableMeta, updateValue)  => {
                  return (value.status == '1') ? 'Active' : 'Inactive';
               },
            }
         },
         {
            name: 'Action',
            field: 'md_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  if(value.application_id){
                     return (
                        <React.Fragment>
                           <span className="list-action">
                           {(this.state.currentPermision.view) ? <Link to={`/provider/customers/view-customer/${value.patient_id}`} title="View customer details"><i className="ti-eye"></i></Link> : ''}
                           {(this.state.currentPermision.view && value.plan_exists>0)?<Link to={`/admin/credit-application/plan-details/${this.enc(value.application_id.toString())}`} title="View account details"><i className="zmdi zmdi-card"></i></Link>:''}
                           {(value.plan_status == 9)?<Link to="#" title="Need attention"><i className="zmdi zmdi-alert-triangle text-danger"></i></Link>:''}
                           {(this.state.currentPermision.view && value.surgery_exits == 4) ? <Link to={`/provider/credit-application/surgery-status-update`} title="View customer surgery"><img src={require('Assets/img/surgery.png')} width="20" /></Link> : ''}
                           </span>
                        </React.Fragment>
                     )
                  }else{
                     return (
                        <React.Fragment>
                           <span className="list-action">
                           {(this.state.currentPermision.view) ? <Link to={`/admin/customers/view-customer/${value.patient_id}`} title="View Customer Details"><i className="ti-eye"></i></Link> : ''}
                           </span>
                        </React.Fragment>
                     )
                  }
                  
               },

            }
         },
         


      ];
      const options = {

         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         downloadOptions: { filename: 'creditApplication.csv' },
      };
      return (
         <div className="provider-customer-list">
            <Helmet>
               <title>Health Partner | Credit Application | Application List</title>
               <meta name="description" content="Application List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.customers" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <MaterialDatatable
                  data={customerDetails}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

           
         </div>
      );
   }
}
const mapStateToProps = ({ authUser, Customer }) => {
   const { user } = authUser;
   const { loading, customerDetails } = Customer;
   return { loading, customerDetails, user }

}

export default connect(mapStateToProps, {
   customerListProvider
})(ProviderPatientList);

