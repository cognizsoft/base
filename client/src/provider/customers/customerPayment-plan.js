/**
 * Pricing
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Switch from 'react-toggle-switch';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
// components
import PricingBlockV1 from 'Components/Pricing/PricingBlockV1';
import PricingBlockV2 from 'Components/Pricing/PricingBlockV2';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import FormHelperText from '@material-ui/core/FormHelperText';

import { isEmpty, isDecimals } from '../../validator/Validator';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
   paymentPlan, createPaymentPlan, applicationDetailsData, applicationOption, getSpeciality
} from 'Actions';
class CustomerPaymentPlan extends Component {

   state = {
      monthlyPlan: true,
      businessPlan: 30,
      enterprisePlan: 59,
      changeURL: 0,
      loan: {
         loan_amount: "",
      },
      loanError: {},
      add_err: {},
      procedure_date: "",
      addData: {
         provider_location:"",
         speciality_type:"",
         
         procedure_amount:"",
         comment:"",
         procedure_status: 1,
      }
   }
   /*
   * Title :- onChnageExist
   * Descrpation :- This function use for onchnage
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnageExist(key, value) {
      let { loanError } = this.state;
      switch (key) {
         case 'loan_amount':
            if (isEmpty(value)) {
               loanError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Loan amount not valid";
            } else {
               loanError[key] = "";
            }
            break;
      }

      this.setState({
         loan: {
            ...this.state.loan,
            [key]: value
         }
      });
      this.setState({ loanError: loanError });
   }
   onChnageProcedure(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'provider_location':
            if (isEmpty(value)) {
               add_err[key] = "Location can't be blank";
            } else {
               this.props.getSpeciality(value);
               add_err[key] = '';
            }
            add_err['speciality_type'] = "Speciality can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'speciality_type':
            if (isEmpty(value)) {
               add_err[key] = "Speciality can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_date':
            if (value == null) {
               add_err[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'amount':
            if (isEmpty(value)) {
               add_err[key] = "Amount can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Amount not valid";
            }
            else if (this.state.addData.procedure_amount !== '' && this.state.addData.procedure_amount < value) {
               add_err[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.addData.procedure_amount !== '' && this.state.addData.procedure_amount >= value) {
               add_err[key] = "";
               add_err['procedure_amount'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_amount':
            if (isEmpty(value)) {
               add_err[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Procedure amount not valid";
            } else if (value < this.state.addData.amount) {
               add_err[key] = "Procedure amount greater than or equal to loan amount";
            } else if (value >= this.state.addData.amount) {
               add_err[key] = "";
               add_err['amount'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_status':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
      }
      this.setState({ add_err: add_err });
   }
   /*
   * Title :- validateSearch
   * Descrpation :- This function use for validate data for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   validateSearch() {
      return (
         this.state.loanError.loan_amount === ''
      )
   }
   validatePlan() {
      return (
         this.state.add_err.provider_location === '' &&
         //this.state.add_err.speciality_type === '' &&
         this.state.add_err.procedure_date === '' &&
         this.state.add_err.amount === '' &&
         this.state.add_err.procedure_amount === '' &&
         this.state.add_err.comment === ''
      )
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan according to customer
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   searchPlan() {
      this.props.paymentPlan(this.props.match.params.application_id, this.state.loan.loan_amount);
      //this.props.searchCustomerApplication(this.state.existApplication);
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.applicationDetailsData(this.props.match.params.application_id);
      this.props.applicationOption();
      //this.props.paymentPlan(this.props.match.params.application_id);
   }
   /*
   * Title :- processPlan
   * Descrpation :- This function use for process selected plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   processPlan(application_id, plan_id) {
      this.props.createPaymentPlan(application_id, plan_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check state updated or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentWillReceiveProps(nextProps) {
      (nextProps.rediretPlanURL != '') ? this.setState({ changeURL: nextProps.rediretPlanURL }) : '';
   }
   // on plan change
   onPlanChange(isMonthly) {
      this.setState({ monthlyPlan: !isMonthly });
      if (!isMonthly) {
         this.setState({ businessPlan: 30, enterprisePlan: 59 });
      } else {
         this.setState({ businessPlan: 35, enterprisePlan: 70 });
      }
   }


   render() {
      
      if (this.state.changeURL === 1) {
         //return (<Redirect to={`/provider/customers/agreement-plan/${this.props.match.params.id}`} />);
      }
      const paymentPlanAllow = this.props.paymentPlanAllow;
      return (
         <div className="pricing-wrapper">
            <Helmet>
               <title>Health Partner | Customers | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsPaymentPlans" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               {this.props.appSummery &&
                  <React.Fragment>
                     <div className="table-responsive mb-20 pymt-history select-customer-plan">
                        <table className="table">
                           <thead>
                              <tr>
                                 <th>Date : {this.props.appSummery[0].date_created}</th>
                                 <th>Credit Score : ${this.props.appSummery[0].score}</th>
                                 <th>Approved Amount : ${this.props.appSummery[0].approve_amount.toFixed(2)}</th>
                                 <th>Remaining Amount : ${(this.props.appSummery[0].remaining_amount) ? this.props.appSummery[0].remaining_amount.toFixed(2) : '0.00'}</th>
                              </tr>
                           </thead>
                        </table>
                     </div>
                     <div className="modal-body page-form-outer text-left">
                        <div className="row">
                           <div className="col-md-2">
                              <FormGroup>
                                 <Label for="loan_amount">Enter Loan Amount<span className="required-field">*</span></Label><br />
                                 <TextField
                                    type="text"
                                    name="loan_amount"
                                    id="loan_amount"
                                    fullWidth
                                    variant="outlined"
                                    placeholder="Enter Loan Amount"
                                    value={this.state.loan.loan_amount}
                                    error={(this.state.loanError.loan_amount) ? true : false}
                                    helperText={this.state.loanError.loan_amount}
                                    onChange={(e) => this.onChnageExist('loan_amount', e.target.value)}
                                 />
                              </FormGroup>
                           </div>
                           <div className="col-md-2">
                              <div className="mt-30">
                                 <Button onClick={this.searchPlan.bind(this)} disabled={!this.validateSearch()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Check Plan
                                 </Button>
                              </div>
                           </div>
                        </div>
                        {paymentPlanAllow &&
                           <div className="row">
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="procedure_amount"
                                       id="procedure_amount"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="($)"
                                       error={(this.state.add_err.procedure_amount) ? true : false}
                                       helperText={(this.state.add_err.procedure_amount != '') ? this.state.add_err.procedure_amount : ''}
                                       onChange={(e) => this.onChnageProcedure('procedure_amount', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="provider_location">Location<span className="required-field">*</span></Label>
                                    <Input
                                       type="select"
                                       name="provider_location"
                                       id="provider_location"
                                       placeholder=""
                                       onChange={(e) => this.onChnageProcedure('provider_location', e.target.value)}
                                    >
                                       <option value="">Select</option>
                                       {this.props.providerLocation && this.props.providerLocation.map((loc, key) => (
                                          <option value={loc.provider_location_id} key={key}>{loc.location_name}</option>
                                       ))}

                                    </Input>
                                    {(this.state.add_err.provider_location != '' && this.state.add_err.provider_location !== undefined) ? <FormHelperText>{this.state.add_err.provider_location}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="speciality_type">Speciality<span className="required-field">*</span></Label>
                                    <Input
                                       type="select"
                                       name="speciality_type"
                                       id="speciality_type"
                                       onChange={(e) => this.onChnageProcedure('speciality_type', e.target.value)}
                                    >
                                       <option value="">Select</option>
                                       {this.props.providerSpeciality && this.props.providerSpeciality.map((spc, key) => (
                                          <option value={spc.mdv_id + '-' + spc.procedure_id} key={key}>{spc.value}</option>
                                       ))}
                                    </Input>
                                    {(this.state.add_err.speciality_type != '' && this.state.add_err.speciality_type !== undefined) ? <FormHelperText>{this.state.add_err.speciality_type}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                                    <DatePicker
                                       dateFormat="MM/dd/yyyy"
                                       name="procedure_date"
                                       id="procedure_date"
                                       selected={this.state.procedure_date}
                                       placeholderText="MM/DD/YYYY"
                                       autocomplete={false}
                                       onChange={(e) => this.onChnageProcedure('procedure_date', e)}
                                    />
                                    {(this.state.add_err.procedure_date) ? <FormHelperText>{this.state.add_err.procedure_date}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>

                              <div className="col-md-4">
                                 <FormGroup tag="fieldset">
                                    <Label>Procedure Status</Label>
                                    <FormGroup check>
                                       <Label check>
                                          <Input
                                             type="radio"
                                             name="procedure_status"
                                             value={1}
                                             checked={(this.state.addData.procedure_status == 1) ? true : false}
                                             onChange={(e) => this.onChnageProcedure('procedure_status', e.target.value)}
                                          />
                                          Active
                                 </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                       <Label check>
                                          <Input
                                             type="radio"
                                             name="procedure_status"
                                             value={0}
                                             checked={(this.state.addData.procedure_status == 0) ? true : false}
                                             onChange={(e) => this.onChnageProcedure('procedure_status', e.target.value)}
                                          />
                                          Inactive
                                 </Label>
                                    </FormGroup>
                                 </FormGroup>
                              </div>
                              <div className="col-md-12">
                                 <FormGroup>
                                    <Label for="comment">Your Comment <span className="required-field">*</span></Label><br />
                                    <Input
                                       type="textarea"
                                       name="comment"
                                       id="comment"
                                       variant="outlined"
                                       placeholder="Enter your comment"
                                       onChange={(e) => this.onChnageProcedure('comment', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.add_err.comment) ? <FormHelperText>{this.state.add_err.comment}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                           </div>
                        }
                     </div>
                  </React.Fragment>
               }

               <div className="price-list">
                  <div className="row row-eq-height">
                     {paymentPlanAllow && paymentPlanAllow.map((plan, idx) => (
                        <div className="col-md-4" key={idx} >
                           <div className="price-plan plan1 rct-block text-center">
                              <div className="header">
                                 <h2 className="pricing-title">
                                    <span className="price">{plan.interest_rate}</span><span>%</span></h2>
                                 <div className="description-text">Principal Amount: <span className="t-price">${this.props.applicationDetails[0].amount}</span></div>
                                 <button disabled={!this.validatePlan()} className="btn-block btn-medium btn btn-primary top-select-btn" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>
                              </div>
                              <div className="content ">
                                 <div className="page-form-outer select-plan-detail">
                                    <div className="row">

                                       <div className="col-md-10">Payment Plans</div>
                                       <div className="col-md-2">{idx + 1}</div>

                                       <div className="col-md-10">Payment Term (months)</div>
                                       <div className="col-md-2">{plan.term_month}</div>
                                    </div>

                                    <div className="price-list table-responsive">
                                       <table className="table table-bordered table-striped table-hover">
                                          <thead>
                                             <tr className="table-head">
                                                <th>SN</th>
                                                <th>Date</th>
                                                <th>Amount</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             {
                                                plan.monthlyPlan.map((month, idm) => (

                                                   <tr key={idm}>
                                                      <td>{idm + 1}</td>
                                                      <td>{month.next_month}</td>
                                                      <td>${month.perMonth}</td>
                                                   </tr>
                                                ))
                                             }



                                          </tbody>
                                       </table>

                                    </div>
                                    <button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>

                                 </div>


                              </div>
                           </div>
                        </div>
                     ))}


                  </div>
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
         </div>
      );
   }
}
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { nameExist, isEdit } = authUser;
   const { loading, paymentPlanAllow, applicationDetails, rediretPlanURL, appSummery, providerLocation, providerSpeciality } = creditApplication;
   return { loading, paymentPlanAllow, applicationDetails, rediretPlanURL, appSummery, providerLocation, providerSpeciality }
}

export default connect(mapStateToProps, {
   paymentPlan, createPaymentPlan, applicationDetailsData, applicationOption, getSpeciality
})(CustomerPaymentPlan);
