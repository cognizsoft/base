/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import FormHelperText from '@material-ui/core/FormHelperText';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import { isObjectEmpty } from '../../validator/Validator';
import moment from 'moment';
import AppConfig from 'Constants/AppConfig';
import {
   planDetails, clearRedirectURL, uploadAgreement
} from 'Actions';
class agreementPlan extends Component {

   state = {
      addData: {
         document: ''
      },
      add_err: {},
   }
   /*
  * Title :- onChnage
  * Descrpation :- This function use for set value and error in state according to user action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'document':
            value = value.target.files[0];
            if (isObjectEmpty(value)) {
               add_err[key] = "Please upload document before submitting.";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   /*
  * Title :- validateSubmit
  * Descrpation :- This function use for check validation
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   validateSubmit() {
      return (this.state.add_err.document === '');
   }
   /*
  * Title :- callAction
  * Descrpation :- This function use for submit uploaded document
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   callAction(customer_id, application_id, e) {
      let datafrom = new FormData();
      datafrom.append('pp_id', this.props.match.params.id);
      datafrom.append('customer_id', customer_id);
      datafrom.append('application_id', application_id);
      datafrom.append('imgedata', this.state.addData.document);
      this.props.uploadAgreement(datafrom);
   }
   componentDidMount() {
      this.props.clearRedirectURL();
      this.props.planDetails(this.props.match.params.id);
   }

   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails } = this.props;
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.agreementPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="p-50" ref={el => (this.componentRef = el)}>
                        <div className="text-center mb-20">
                           <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                        </div>
                        <h1 className="text-center mb-20">Terms of Agreement for Health Partner, Inc</h1>
                        <div className="terms-condition-agreement">
                           <div>Date <strong><u>{moment(Date.now()).format('MM/DD/YYYY')}</u></strong></div>
                           <div>Location _____________</div>
                           <p>On or before Date, for value received, the undersigned <strong><u>{(mainPlan) ? (mainPlan[0].f_name + ' ' + mainPlan[0].m_name + ' ' + mainPlan[0].l_name) : '-'}</u></strong> (the "Borrower") promises to pay to the order of HEALTH PARTNER INC. (the "Holder"), in the manner and at the place provided below, the principal sum of <strong><u>{(mainPlan) ? '$'+(mainPlan[0].loan_amount.toFixed(2)) : '-'}</u></strong>.</p>

                           <h4><strong>1. PAYMENT :</strong></h4>
                           <p>All payments of principal and interest under this note will be made in lawful money of the United States, without offset, deduction, or counterclaim, by wire transfer of immediately available funds to an account designated by the Holder in writing at least 7 days after the effective date of this note or, if this designation is not made, by check mailed to the Holder at 5720 Creedmoor Road, Suite 103, Raleigh, North Carolina, 27612, or at such other place as the Holder may designate in writing.</p>

                           <h4><strong>2. INTEREST :</strong></h4>
                           <p>Interest on the unpaid principal balance of this note is payable from the date of this note until this note is paid in full, at the rate of <strong><u>{(mainPlan) ? parseFloat(mainPlan[0].discounted_interest_rate).toFixed(2) + "%" : '-'}</u></strong> per year, or the maximum amount allowed by applicable law, whichever is less. Accrued interest will be computed on the basis of a 365-day or 366-day year, as the case may be, based on the actual number of days elapsed in the period in which it accrues.</p>

                           <h4><strong>3. PREPAYMENT :</strong></h4>
                           <p>The Borrower may prepay this note, in whole or in part, at any time before maturity without penalty or premium. Any partial prepayment will be credited first to accrued interest, then to principal. No prepayment extends or postpones the maturity date of this note.</p>

                           <h4><strong>4. EVENTS OF DEFAULT :</strong></h4>
                           <p>Each of the following constitutes an <strong>"Event of Default"</strong> under this note:</p>
                           <ul>
                              <li>The Borrower's failure to make any payment when due under the terms of this note, including the lump-sum payment due under this note at its maturity;</li>
                              <li>The filing of any voluntary or involuntary petition in bankruptcy by or regarding the Borrower or the initiation of any proceeding under bankruptcy or insolvency laws against the Borrower;</li>
                              <li>An assignment made by the Borrower for the benefit of creditors;</li>
                              <li>The appointment of a receiver, custodian, trustee, or similar party to take possession of the Borrower's assets or property; or</li>
                              <li>The death of the Borrower.</li>
                           </ul>

                           <h4><strong>5. ACCELERATION; REMEDIES ON DEFAULT :</strong></h4>
                           <p>If any Event of Default occurs, all principal and other amounts owed under this note will become immediately due and payable without any action by the Holder, the Borrower, or any other person. The Holder, in addition to any rights and remedies available to the Holder under this note, may, in its sole discretion, pursue any legal or equitable remedies available to it under applicable law or in equity, including taking any of the following actions:</p>
                           <ul>
                              <li>Personally, or by agents or attorneys (in compliance with applicable law), take immediate possession of the collateral. To that end, the Holder may pursue the collateral where it may be found, and enter the Borrower's premises, with or without notice, demand, process of law, or legal procedure if this can be done without breach of the peace. If the premises on which any part of the collateral is located are not under the Borrower's direct control, the Borrower will exercise its best efforts to ensure that the Holder is promptly provided right of access to those premises. To the extent that the Borrower's consent would otherwise be required before a right of access could be granted, the Borrower hereby irrevocably grants that consent;</li>
                              <li>Require the Borrower to assemble the collateral and make it available to the Holder at a place to be designated by the Holder that is reasonably convenient to both parties (it being acknowledged that the Borrower's premises are reasonably convenient to the Borrower);</li>
                              <li>Sell, lease, or dispose of the collateral or any part of it in any manner permitted by applicable law or by contract; and</li>
                              <li>Exercise all rights and remedies of a secured party under applicable law.</li>
                           </ul>

                           <h4><strong>6. WAIVER OF PRESENTMENT; DEMAND :</strong></h4>
                           <p>The Borrower hereby waives presentment, demand, notice of dishonor, notice of default or delinquency, notice of protest and nonpayment, notice of costs, expenses or losses and interest on those, notice of interest on interest and late charges, and diligence in taking any action to collect any sums owing under this note, including (to the extent permitted by law) waiving the pleading of any statute of limitations as a defense to any demand against the undersigned. Acceptance by the Holder or any other holder of this note of any payment differing from the designated lump-sum payment listed above does not relieve the undersigned of the obligation to honor the requirements of this note.</p>

                           <h4><strong>7. GOVERNING LAW :</strong></h4>
                           <ul>
                              <li><strong>Choice of La.</strong> The laws of the state of North Carolina govern this note (without giving effect to its conflicts of law principles).</li>
                              <li><strong>Choice of Forum.</strong> Both parties consent to the personal jurisdiction of the state and federal courts in Wake, North Carolina.</li>
                           </ul>

                           <h4><strong>8. COLLECTION COSTS AND ATTORNEYS' FEES :</strong></h4>
                           <p>The Borrower shall pay all expenses of the collection of indebtedness evidenced by this note, including reasonable attorneys' fees and court costs in addition to other amounts due.</p>

                           <h4><strong>9. ASSIGNMENT AND DELEGATION :</strong></h4>
                           <ul>
                              <li><strong>No Assignment.</strong> The Borrower may not assign any of its rights under this note. All voluntary assignments of rights are limited by this subsection.</li>
                              <li><strong>No Delegation.</strong> The Borrower may not delegate any performance under this note.</li>
                              <li><strong>Enforceability of an Assignment or Delegation.</strong> If a purported assignment or purported delegation is made in violation of this section, it is void.</li>
                           </ul>

                           <h4><strong>10. SEVERABILITY :</strong></h4>
                           <p>If any one or more of the provisions contained in this note is, for any reason, held to be invalid, illegal, or unenforceable in any respect, that invalidity, illegality, or unenforceability will not affect any other provisions of this note, but this note will be construed as if those invalid, illegal, or unenforceable provisions had never been contained in it, unless the deletion of those provisions would result in such a material change so as to cause completion of the transactions contemplated by this note to be unreasonable.</p>

                           <h4><strong>11. NOTICES :</strong></h4>
                           <ul>
                              <li>Writing; Permitted Delivery Methods. Each party giving or making any notice, request, demand, or other communication required or permitted by this note shall give that notice in writing and use one of the following types of delivery, each of which is a writing for purposes of this note: personal delivery, mail (registered or certified mail, postage prepaid, return-receipt requested), nationally recognized overnight courier (fees prepaid), facsimile, or email.</li>
                              <li>Addresses. A party shall address notices under this section to a party at the following addresses:<br/><br/>
                              <p><strong>If to the Borrower :</strong></p>
                              <p>_____________________________________</p>
                              <p>_____________________________________</p>
                              <p>_____________________________________</p>

                              <strong>If to the Holder:</strong>
                              <p>Health Partner Inc.</p>
                              <p>5720 Creedmoor Road, Suite 103</p>
                              <p>Raleigh, North Carolina 27612</p>

                              </li>
                              <li><strong>Effectiveness.</strong> A notice is effective only if the party giving notice complies with subsections (a) and</li>
                              <li>and if the recipient receives the notice</li>
                           </ul>

                           <h4><strong>12. WAIVER :</strong></h4>
                           <p>No waiver of a breach, failure of any condition, or any right or remedy contained in or granted by the provisions of this note will be effective unless it is in writing and signed by the party waiving the breach, failure, right, or remedy. No waiver of any breach, failure, right, or remedy will be deemed a waiver of any other breach, failure, right, or remedy, whether or not similar, and no waiver will constitute a continuing waiver, unless the writing so specifies.</p>

                           <h4><strong>13. HEADINGS :</strong></h4>
                           <p>The descriptive headings of the sections and subsections of this note are for convenience only, and do not affect this note's construction or interpretation.</p>

                           <p>By signing below, you, the Borrower, agree to all terms listed in the above Agreement.</p>

                           <p>By : ____________________________ Date : <strong><u>{moment(Date.now()).format('MM/DD/YYYY')}</u></strong></p>
                           <p className="name-borrower"><em>Name of Borrower</em></p>
                           <br/><br/>
                        </div>
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                           <div className="add-card">

                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Customer Information</th>
                                    </tr>
                                    <tr>
                                       <td><strong>Account No :</strong></td>
                                       <td>{(mainPlan) ? mainPlan[0].patient_ac : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Loan No :</strong></td>
                                       <td>{(mainPlan) ? mainPlan[0].plan_number : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Name :</strong></td>
                                       <td>{(mainPlan) ? (mainPlan[0].f_name + ' ' + mainPlan[0].m_name + ' ' + mainPlan[0].l_name) : '-'}</td>
                                    </tr>

                                    <tr>
                                       <td><strong>Address :</strong></td>
                                       <td>{(mainPlan) ? (mainPlan[0].address1 + ' ' + mainPlan[0].address2 + ', ' + mainPlan[0].City + ', ' + mainPlan[0].region_name + ' - ' + mainPlan[0].zip_code) : '-'}

                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Phone :</strong></td>
                                       <td>{(mainPlan) ? mainPlan[0].peimary_phone : '-'}</td>
                                    </tr>

                                 </tbody>
                              </table>

                           </div>
                           <div className="add-card">

                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Provider Information</th>
                                    </tr>
                                    <tr>
                                       <td><strong>Account No:</strong> {(this.props.appProviderDetails) ? this.props.appProviderDetails.provider_ac : '-'}</td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td><strong>Name:</strong> {(this.props.appProviderDetails) ? this.props.appProviderDetails.name : '-'}</td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td><strong>Address:</strong> {(this.props.appProviderDetails) ? this.props.appProviderDetails.address1 + ' ' + this.props.appProviderDetails.address2 + ' ' + this.props.appProviderDetails.city + ' ' + this.props.appProviderDetails.state_name : '-'}</td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td><strong>Phone:</strong> {(this.props.appProviderDetails) ? this.props.appProviderDetails.primary_phone : '-'}</td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td><strong>Doctor:</strong> {(mainPlan) ? mainPlan[0].doctors_name : '-'}</td>
                                       <td></td>
                                    </tr>


                                 </tbody>
                              </table>

                           </div>
                           <div className="add-card">

                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Loan Information</th>
                                    </tr>
                                    <tr>
                                       <td><strong>Loan Amount :</strong></td>
                                       <td>{(mainPlan) ? '$' + parseFloat(mainPlan[0].credit_amount).toFixed(2) : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Payment Term :</strong></td>
                                       <td>{(mainPlan) ? mainPlan[0].payment_term + ' Month' : '-'}</td>
                                    </tr>

                                    <tr>
                                       <td><strong>Interest Rate(%) :</strong></td>
                                       <td>{(mainPlan) ? parseFloat(mainPlan[0].discounted_interest_rate).toFixed(2) + "%" : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Monthly Payment :</strong></td>
                                       <td>{(mainPlan) ? '$' + parseFloat(mainPlan[0].monthly_payment).toFixed(2) : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Current Balance :</strong></td>
                                       <td>{(mainPlan) ? '$' + parseFloat(mainPlan[0].credit_amount).toFixed(2) : '-'}</td>
                                    </tr>
                                 </tbody>
                              </table>

                           </div>

                        </div>
                        <div className="table-responsive mb-40 pymt-history">
                           <h2 className="text-center mb-10">Payment Details</h2>
                           <table className="table-borderless agreement_plan">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>EMI</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {paymentPlanDetails && paymentPlanDetails.map(function (plan, idx) {
                                    return <tr
                                       key={idx}
                                       className={new Date(plan.comparison_date) < new Date() && plan.paid_flag == 0 ? "late-fee-payment" : ""}
                                    >
                                       <td>{idx + 1}</td>
                                       <td>{(plan.installment_amt) ? '$' + parseFloat(plan.installment_amt).toFixed(2) : '$0.00'}</td>
                                       <td>{plan.due_date}</td>
                                       <td>{(!plan.paid_flag) ? 'Pending' : ''}</td>
                                    </tr>
                                 })}
                              </tbody>
                           </table>
                        </div>
                        <div className="p-50">
                           <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                              <div className="add-card sing_box">

                                 <b>Customer Signature</b>

                              </div>
                              <div className="add-card sing_box">

                                 <b>Witness Signature</b>

                              </div>



                           </div>
                        </div>
                     </div>
                     <div className="invoice-head">
                        <div className="row">
                           <div className="col-sm-6">
                              <div className="row agreement_doc">
                                 <div className="col-md-6 upload-doc-input">
                                    <FormGroup>
                                       <Label for="document_upload">Upload Agreement Document<span className="required-field">*</span></Label><br />
                                       <Input
                                          type="file"
                                          name="document"
                                          id="document"
                                          onChange={(e) => this.onChnage('document', e)}
                                       >
                                       </Input>
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-2">
                                    <Label for="document_upload"></Label>
                                    {mainPlan &&
                                       <Button variant="contained" color="primary" className="text-white mr-10 mb-10" size="small" onClick={this.callAction.bind(this, mainPlan[0].patient_id, mainPlan[0].application_id)} disabled={!this.validateSubmit()}>
                                          Upload
                                       </Button>
                                    }

                                 </div>
                              </div>
                           </div>
                           <div className="col-sm-6 text-right">
                              <ul className="list-inline agreement_print">
                                 <li>
                                    <ReactToPrint
                                       trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                       content={() => this.componentRef}
                                    />
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication }) => {
   //console.log(creditApplication);
   const { nameExist, isEdit } = authUser;
   const { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL, appProviderDetails } = creditApplication;

   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL, appProviderDetails }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, uploadAgreement
})(agreementPlan);