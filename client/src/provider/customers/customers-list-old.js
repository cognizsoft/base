/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import MaterialDatatable from "material-datatable";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   customerListProvider
} from 'Actions';

class ProviderPatientList extends Component {

   state = {
      currentModule: 25,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 06,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.customerListProvider();
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   render() {
      const customerDetails = this.props.customerDetails;
      const columns = [
         { name: 'ID', field: 'patient_id', },
         {
            name: 'Full Name',
            field: 'f_name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  )
               },
            }
         },
         { name: 'Address', field: 'address1', },
         { name: 'City', field: 'City', },
         { name: 'State', field: 'state_name', },
         { name: 'Zip', field: 'zip_code', },
         /*{
            name: 'Provider',
            field: 'name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.name != null) ? value.name : '-'
                  )
               },
            }
         },*/
         
         { name: 'Email', field: 'email', },
         { name: 'Primary Phone', field: 'peimary_phone', },
         {
            name: 'Status',
            field: 'status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  
                  return (
                     <React.Fragment>
                        <span className="d-flex justify-content-start">
                           <div className="status">
                              <span className="d-block">{(value.status == '1') ? 'Active' : 'Inactive'}</span>
                           </div>
                        </span>
                     </React.Fragment>
                  )
               },
               customValue: (value, tableMeta, updateValue) => value.status,
               customSortValue: (value, tableMeta, updateValue) => value.status,
            }
         },
         {
            name: 'Action',
            field: 'md_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  if(value.application_id){
                     return (
                        <React.Fragment>
                           <span className="list-action">
                           {(this.state.currentPermision.view) ? <Link to={`/provider/customers/view-customer/${value.patient_id}`}><i className="ti-eye"></i></Link> : ''}
                              {/*<Link to={`/provider/credit-application/payment-plan/${value.application_id}`}><i className="zmdi zmdi-card"></i></Link>*/}
                           </span>
                        </React.Fragment>
                     )
                  }else{
                     return (
                        <React.Fragment>
                           <span className="list-action">
                              <Link to={`/provider/customers/view-customer/${value.patient_id}`}><i className="ti-eye"></i></Link>
                           </span>
                        </React.Fragment>
                     )
                  }
                  
               },

            }
         },


      ];
      const options = {

         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         downloadOptions: { filename: 'creditApplication.csv' },
      };
      return (
         <div className="provider-customer-list">
            <Helmet>
               <title>Health Partner | Credit Application | Application List</title>
               <meta name="description" content="Application List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.customers" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <MaterialDatatable
                  data={customerDetails}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

           
         </div>
      );
   }
}
const mapStateToProps = ({ authUser, Customer }) => {
   const { user } = authUser;
   const { loading, customerDetails } = Customer;
   return { loading, customerDetails, user }

}

export default connect(mapStateToProps, {
   customerListProvider
})(ProviderPatientList);

