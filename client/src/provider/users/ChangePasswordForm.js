/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const ChangePasswordForm = ({ passwordError, onChangePasswordDetail }) => (
    <Form>
        <div className="row">
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userName">Password<span className="required-field">*</span></Label>
                    <TextField
                        type="password"
                        name="password"
                        id="password"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Password"
                        error={(passwordError.password) ? true : false}
                        helperText={passwordError.password}
                        onChange={(e) => onChangePasswordDetail('password', e.target.value)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userName">Confirm Password<span className="required-field">*</span></Label>
                    <TextField
                        type="password"
                        name="confirm_password"
                        id="confirm_password"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Confirm Password"
                        error={(passwordError.confirm_password) ? true : false}
                        helperText={passwordError.confirm_password}
                        onChange={(e) => onChangePasswordDetail('confirm_password', e.target.value)}
                    />
                </FormGroup>
            </div>
                        
        </div>
    </Form>
);

export default ChangePasswordForm;
