/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';
// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isEmail, isMaster, isContainWhiteSpace, isPassword, formatPhoneNumber, isPhone } from '../../validator/Validator';



import AddNewButton from './AddNewButton';
import CustomToolbar from "./CustomToolbar";
import ChangePasswordForm from './ChangePasswordForm';
import {
   getAllUsersProvider, checkUsernameExist, insertUserProvider, updateUserProvider, checkEmailExist, deleteUser, changeUserPassword, getUserRole
} from 'Actions';
class ProviderUser extends Component {

   state = {
      currentModule: 1,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         phone: '',
         username: '',
         email_id: '',
         password: '',
         client_id: '',
         f_name: '',
         m_name: '',
         l_name: '',
         mdv_user_type_id: 2,
         mdv_role_id: '',
         status: 1
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {
         m_name: '',
         l_name: '',
         mdv_user_type_id: '',
      },
      udpate_err: {
         username: '',
         email_id: '',
         provider_id: '',
         phone: '',
         f_name: '',
         m_name: '',
         l_name: '',
         mdv_user_type_id: '',
         mdv_role_id: '',
      },
      passwordError: {},
      editPassword: {
         password: '',
         confirm_password: '',
         password_id: ''
      },
      password_id: null,
      chnagePassword: false,
      showProvider: false,
      userRoledefault: '',
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 19,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllUsersProvider();
      this.props.getUserRole(2);
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 19,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
	/*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }

	/*
   * Title :- deleteUserPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   deleteUserPermanently() {
      const { selectedUser } = this.state;
      let users = this.props.userList;
      let indexOfDeleteUser = users.indexOf(selectedUser);
      this.props.deleteUser(users[indexOfDeleteUser].user_id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedUser: null });
   }

	/*
   * Title :- opnAddNewUserModal
   * Descrpation :- This function use for open add user popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/*
   * Title :- onChangeAddNewUserDetails
   * Descrpation :- This function use for check field validation on add new user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onChangeAddNewUserDetails(key, value, text) {
      let { add_err } = this.state;
      switch (key) {
         case 'username':
            if (isEmpty(value)) {
               add_err[key] = "Username can't be blank";
            } else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid username. only allow a-z A-Z 0-9 _ -";
            } else {
               add_err[key] = '';
            }
            break;
         case 'email_id':
            if (isEmpty(value)) {
               add_err[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'f_name':
            if (isEmpty(value)) {
               add_err[key] = "First name can't be blank";
            } /*else if (!isMaster(value)) {
               add_err[key] = "Please enter a valid first name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
            break;
         case 'm_name':
            //if (!isMaster(value)) {
            // add_err[key] = "Please enter a valid middle name. only allow a-z A-Z 0-9 _ -";
            //} else {
            add_err[key] = '';
            //}
            break;
         case 'l_name':
            //if (!isMaster(value)) {
            // add_err[key] = "Please enter a valid last name. only allow a-z A-Z 0-9 _ -";
            //} else {
            add_err[key] = '';
            //}
            break;
         case 'mdv_user_type_id':
            if (isEmpty(value)) {
               add_err[key] = "User type can't be blank";
            } else {
               add_err[key] = '';
               //this.props.getUserRole(value);
            }
            add_err['mdv_role_id'] = "User role can't be blank";
            break;
         case 'mdv_role_id':
            if (isEmpty(value)) {
               add_err[key] = "User role can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'password':
            if (isEmpty(value)) {
               add_err[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Password should not contain white spaces";
            } /*else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
               add_err[key] = "Password's length must between 6 to 16";
            }*/
            else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else {
               add_err[key] = '';
            }
            break;
         case 'phone':
            if (isEmpty(value)) {
               add_err[key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            break;
         case 'client_id':
            if (isEmpty(value)) {
               add_err[key] = "Provider can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewUserDetail: {
            ...this.state.addNewUserDetail,
            [key]: value
         }
      });
   }

	/*
   * Title :- addNewUser
   * Descrpation :- This function use for add new user action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   addNewUser() {
      this.state.addNewUserDetail.client_id = (this.state.addNewUserDetail.mdv_user_type_id == 2) ? this.state.addNewUserDetail.client_id : '';
      this.props.insertUserProvider(this.state.addNewUserDetail);
      this.setState({ addNewUserModal: false });
      let self = this;
      let emptyState = {
         phone: '',
         username: '',
         email_id: '',
         password: '',
         f_name: '',
         m_name: '',
         l_name: '',
         mdv_user_type_id: 2,
         mdv_role_id: '',
         status: 1
      }
      setTimeout(() => {
         self.setState({ addNewUserDetail: emptyState });
      }, 2000);
   }
   /*
   * Title :- changeUserPassword
   * Descrpation :- This function use for call change password action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   changeUserPassword() {
      this.props.changeUserPassword(this.state.editPassword);
      let emptyPassword = {
         password: '',
         confirm_password: ''
      }
      this.setState({ editPassword: emptyPassword, chnagePassword: false, });
   }

	/*
   * Title :- viewUserDetail
   * Descrpation :- This function use for open user details popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   viewUserDetail(data) {
      this.setState({ openViewUserDialog: true, selectedUser: data });
   }

	/*
   * Title :- onEditUser
   * Descrpation :- This function use for open edit user popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onEditUser(user) {
      this.props.getUserRole(user.mdv_user_type_id);
      let showProvider = (user.provider_id) ? true : false;
      this.setState({ addNewUserModal: true, editUser: user, showProvider: showProvider, userRoledefault: user.mdv_role_id });
   }
   /*
   * Title :- onPassword
   * Descrpation :- This function use for open chnage password popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onPassword(id) {
      const { editPassword } = this.state;
      editPassword.password_id = id;
      this.setState({ chnagePassword: true, editPassword: editPassword });
   }


	/*
   * Title :- onAddUpdateUserModalClose
   * Descrpation :- This function use for close add edit user popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onAddUpdateUserModalClose() {
      let emptyError = {
         username: '',
         email_id: '',
         provider_id: '',
         phone: '',
         f_name: '',
         m_name: '',
         l_name: '',
         mdv_user_type_id: '',
         mdv_role_id: '',
      }

      let passError = {
         password: '',
         confirm_password: '',
      }

      let addNewUserDetail = {
         phone: '',
         username: '',
         email_id: '',
         password: '',
         client_id: '',
         f_name: '',
         m_name: '',
         l_name: '',
         mdv_user_type_id: '',
         mdv_role_id: '',
         status: 1
      };
      let add_err = {
         m_name: '',
         l_name: ''
      };
      this.setState({ addNewUserModal: false, editUser: null, udpate_err: emptyError, chnagePassword: false, passwordError: passError, showProvider: false, addNewUserDetail: addNewUserDetail, add_err: add_err })
   }

   /*
   * Title :- onViewUserModalClose
   * Descrpation :- This function use for close view user details popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onViewUserModalClose = () => {
      this.setState({ openViewUserDialog: false, selectedUser: null })
   }


	/*
   * Title :- onUpdateUserDetails
   * Descrpation :- This function use for check filed validation on edit user case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onUpdateUserDetails(key, value, text) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'username':
            if (isEmpty(value)) {
               udpate_err[key] = "Username can't be blank";
            } else if (!isAlphaDigitUnderscoreDash(value)) {
               udpate_err[key] = "Please enter a valid username. only allow a-z A-Z 0-9 _ -";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'email_id':
            if (isEmpty(value)) {
               udpate_err[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               udpate_err[key] = "Please enter a valid email address.";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'f_name':
            if (isEmpty(value)) {
               udpate_err[key] = "First name can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'm_name':
            //if (!isMaster(value)) {
            // udpate_err[key] = "Please enter a valid middle name. only allow a-z A-Z 0-9 _ -";
            //} else {
            udpate_err[key] = '';
            //}
            break;
         case 'l_name':
            //if (!isMaster(value)) {
            // udpate_err[key] = "Please enter a valid last name. only allow a-z A-Z 0-9 _ -";
            //} else {
            udpate_err[key] = '';
            //}
            break;
         case 'mdv_user_type_id':
            if (isEmpty(value)) {
               udpate_err[key] = "User type can't be blank";
               this.setState({ showProvider: false });
            } else {
               udpate_err[key] = '';
               if (text === 'Provider') {
                  this.setState({ showProvider: true });
               } else {
                  this.setState({ showProvider: false });
               }
               this.props.getUserRole(value);
            }
            this.setState({ userRoledefault: '' });
            //console.log(this.state.editUser.provider_id)
            udpate_err['mdv_role_id'] = "User role can't be blank";
            udpate_err['provider_id'] = (this.state.editUser.provider_id == '') ? "Provider can't be blank" : "";
            break;
         case 'mdv_role_id':
            if (isEmpty(value)) {
               udpate_err[key] = "User role can't be blank";
            } else {
               this.setState({ userRoledefault: value });
               udpate_err[key] = '';
            }
            break;
         case 'phone':
            if (isEmpty(value)) {
               udpate_err[key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               udpate_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               udpate_err[key] = '';
            }
            break;
         case 'provider_id':
            if (isEmpty(value)) {
               udpate_err[key] = "Provider can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editUser: {
            ...this.state.editUser,
            [key]: value
         }
      });
   }
   /*
   * Title :- onChangePasswordDetail
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onChangePasswordDetail(key, value) {
      let { passwordError } = this.state;
      switch (key) {
         case 'password':
            if (isEmpty(value)) {
               passwordError[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               passwordError[key] = "Password should not contain white spaces";
            } /*else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
               passwordError[key] = "Password's length must between 6 to 16";
            } */else if (!isPassword(value, { min: 8, trim: true })) {
               passwordError[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (this.state.editPassword.confirm_password !== '' && this.state.editPassword.confirm_password !== value) {
               passwordError[key] = "Password and confirm password not match";
            } else if (this.state.editPassword.confirm_password !== '' && this.state.editPassword.confirm_password === value) {
               passwordError[key] = "";
               passwordError['confirm_password'] = "";
            } else {
               passwordError[key] = '';
            }
            break;
         case 'confirm_password':
            if (isEmpty(value)) {
               passwordError[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               passwordError[key] = "Password should not contain white spaces";
            } /*else if (!isLength(value, { gte: 6, lte: 16, trim: true })) {
               passwordError[key] = "Password's length must between 6 to 16";
            } */else if (!isPassword(value, { min: 8, trim: true })) {
               passwordError[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (value !== this.state.editPassword.password) {
               passwordError[key] = "Password and confirm password not match";
            } else if (value === this.state.editPassword.password) {
               passwordError[key] = "";
               passwordError['password'] = "";
            } else {
               passwordError[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ passwordError: passwordError });

      this.setState({
         editPassword: {
            ...this.state.editPassword,
            [key]: value
         }
      });
   }
	/*
   * Title :- updateUser
   * Descrpation :- This function use call user update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   updateUser() {
      const { editUser } = this.state;
      editUser.provider_id = (editUser.mdv_user_type_id == 2) ? editUser.provider_id : '';
      const filterType = this.props.userType.filter(x => x.mdv_id == editUser.mdv_user_type_id);
      const filterRole = this.props.userRole.filter(x => x.mdv_id == editUser.mdv_role_id);
      const filterProvider = this.props.providerList.filter(x => x.provider_id == editUser.provider_id);
      editUser.name = (editUser.provider_id) ? filterProvider[0].name : '';
      editUser.provider_id = (editUser.provider_id) ? filterProvider[0].provider_id : '';
      editUser.user_type = filterType[0].value;
      editUser.user_role = filterRole[0].value;

      let indexOfUpdateUser = '';
      let users = this.props.userList;
      for (let i = 0; i < users.length; i++) {
         const user = users[i];
         if (user.user_id === editUser.user_id) {
            indexOfUpdateUser = i
         }
      }
      this.props.updateUserProvider(editUser);

      users[indexOfUpdateUser] = editUser;

      this.setState({ editUser: null, addNewUserModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ users });
      //}, 2000);
   }


   /*
   * Title :- opnAddNewUserModal
   * Descrpation :- This function use for open popup for add new record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   validateAddSubmit() {
      if (this.state.showProvider) {
         return (
            this.state.add_err.username === '' &&
            this.state.add_err.password === '' &&
            this.state.add_err.phone === '' &&
            this.state.add_err.email_id === '' &&
            this.state.add_err.f_name === '' &&
            this.state.add_err.m_name === '' &&
            this.state.add_err.l_name === '' &&
            this.state.add_err.mdv_user_type_id === '' &&
            this.state.add_err.mdv_role_id === ''
         );
      } else {
         return (
            this.state.add_err.username === '' &&
            this.state.add_err.password === '' &&
            this.state.add_err.phone === '' &&
            this.state.add_err.email_id === '' &&
            this.state.add_err.f_name === '' &&
            this.state.add_err.m_name === '' &&
            this.state.add_err.l_name === '' &&
            this.state.add_err.mdv_user_type_id === '' &&
            this.state.add_err.mdv_role_id === ''
         );
      }
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   validateUpdateSubmit() {
      if (this.state.showProvider) {
         return (
            this.state.udpate_err.username === '' &&
            this.state.udpate_err.email_id === '' &&
            this.state.udpate_err.phone === '' &&
            this.state.udpate_err.f_name === '' &&
            this.state.udpate_err.f_name === '' &&
            this.state.udpate_err.m_name === '' &&
            this.state.udpate_err.l_name === '' &&
            this.state.udpate_err.mdv_user_type_id === '' &&
            this.state.udpate_err.mdv_role_id === ''
         );
      } else {
         return (
            this.state.udpate_err.username === '' &&
            this.state.udpate_err.email_id === '' &&
            this.state.udpate_err.phone === '' &&
            this.state.udpate_err.f_name === '' &&
            this.state.udpate_err.m_name === '' &&
            this.state.udpate_err.l_name === '' &&
            this.state.udpate_err.mdv_user_type_id === '' &&
            this.state.udpate_err.mdv_role_id === ''
         );
      }
   }
   /*
   * Title :- validatePasswordSubmit
   * Descrpation :- This function use for enable or disable change password button
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   validatePasswordSubmit() {
      return (
         this.state.passwordError.password === '' &&
         this.state.passwordError.confirm_password === ''
      );
   }

   /*
   * Title :- checkUsernameExist
   * Descrpation :- This function use for check username exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkUsernameExist(value, md_id) {
      this.props.checkUsernameExist(value, md_id);
   }
   /*
   * Title :- checkEmailExist
   * Descrpation :- This function use for check eamil exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkEmailExist(value, md_id) {
      this.props.checkEmailExist(value, md_id);
   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { udpate_err } = this.state;

      (nextProps.nameExist && nextProps.isEdit == 0) ? add_err['username'] = "Username already exists" : '';
      (nextProps.nameExist && nextProps.isEdit == 1) ? udpate_err['username'] = "Username already exists" : '';
      (nextProps.emailExist && nextProps.isEdit == 0) ? add_err['email_id'] = "Email already exists" : '';
      (nextProps.emailExist && nextProps.isEdit == 1) ? udpate_err['email_id'] = "Email already exists" : '';
      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });
      /*let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.nameExist) ? add_err['username'] = "Username already exists" : add_err['username'] = '';
      (nextProps.nameExist) ? udpate_err['username'] = "Username already exists" : udpate_err['username'] = '';

      (nextProps.emailExist) ? add_err['email_id'] = "Email already exists" : add_err['email_id'] = '';
      (nextProps.emailExist) ? udpate_err['email_id'] = "Email already exists" : udpate_err['email_id'] = '';


      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });*/

   }


   render() {
      const { add_err, udpate_err, selectedUser, editUser } = this.state;
      const userList = this.props.userList;
      const columns = [

         {
            name: 'ID',
            field: 'user_id'
         },
         {
            name: 'User Name',
            field: 'username',
         },
         {
            name: 'Provider Name',
            field: 'name',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.name) ? value.name : '-'
                  );
               }
            }
         },
         {
            name: 'Provider Location',
            field: 'location_name',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.location_name) ? value.location_name : '-'
                  );
               }
            }
         },
         {
            name: 'Email Address',
            field: 'email_id',
         },
         {
            name: 'User Type',
            field: 'user_type',
         },
         {
            name: 'User Role',
            field: 'user_role',
         },
         {
            name: 'Phone',
            field: 'phone',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewUserDetail(value)}><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditUser(value)}><i className="ti-pencil"></i></a> : ''}
                        {/*{(this.state.currentPermision.delete) ? <a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a> : ''}*/}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onPassword(value.user_id)}><i className="ti-key"></i></a> : ''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'MasterModule.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add) ? <CustomToolbar opnAddNewUserModal={this.opnAddNewUserModal.bind(this)} /> : ''
            );
         },
      };
      return (
         <div className="provider-user-list">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.users" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={userList}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete user permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  {editUser === null ?
                     'Add User' : 'Update User'
                  }
               </ModalHeader>
               <ModalBody>
                  {editUser === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewUserDetails={this.state.addNewUserDetail}
                        onChangeAddNewUserDetails={this.onChangeAddNewUserDetails.bind(this)}
                        userType={this.props.userType}
                        userRole={this.props.userRole}
                        providerList={this.props.providerList}
                        checkUsernameExist={this.checkUsernameExist.bind(this)}
                        checkEmailExist={this.checkEmailExist.bind(this)}
                        showProvider={this.state.showProvider}
                     />
                     : <UpdateUserForm
                        editUser={editUser}
                        addErr={udpate_err}
                        addNewUserDetails={this.state.addNewUserDetail}
                        onUpdateUserDetail={this.onUpdateUserDetails.bind(this)}
                        userType={this.props.userType}
                        userRole={this.props.userRole}
                        providerList={this.props.providerList}
                        checkUsernameExist={this.checkUsernameExist.bind(this)}
                        checkEmailExist={this.checkEmailExist.bind(this)}
                        showProvider={this.state.showProvider}
                        userRoledefault={this.state.userRoledefault}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editUser === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewUser()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateUser()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.chnagePassword} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  Change Password
               </ModalHeader>
               <ModalBody>
                  <ChangePasswordForm passwordError={this.state.passwordError} onChangePasswordDetail={this.onChangePasswordDetail.bind(this)} />
               </ModalBody>
               <ModalFooter>
                  <Button
                     variant="contained"
                     color="primary"
                     className={(this.validatePasswordSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                     onClick={() => this.changeUserPassword()}
                     disabled={!this.validatePasswordSubmit()}
                  >Submit</Button>

                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'User View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Username: </span> <span className="second-colmn">{selectedUser.username}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Email: </span> <span className="second-colmn">{selectedUser.email_id}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">First Name: </span> <span className="second-colmn">{(selectedUser.f_name) ? selectedUser.f_name : '-'}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Middle Name: </span> <span className="second-colmn">{(selectedUser.m_name) ? selectedUser.m_name : '-'}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Last Name: </span> <span className="second-colmn">{(selectedUser.l_name) ? selectedUser.l_name : '-'}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Phone No.: </span> <span className="second-colmn">{(selectedUser.phone) ? selectedUser.phone : '-'}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">User Type: </span> <span className="second-colmn">{selectedUser.user_type}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">User Role: </span> <span className="second-colmn">{selectedUser.user_role}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Provider: </span> <span className="second-colmn">{(selectedUser.name) ? selectedUser.name : '-'}</span></div>
                                 <div className="colmn-row"><span className="fw-bold first-colmn">Status: </span> <span className="second-colmn">{(selectedUser.status == 1) ? 'Active' : 'Inactive'}</span></div>

                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>


         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser }) => {
   const { user, loading, userList, userType, userRole, nameExist, emailExist, isEdit, providerList } = authUser;
   return { loading, user, userList, userType, userRole, nameExist, emailExist, isEdit, providerList }

}

export default connect(mapStateToProps, {
   getAllUsersProvider, checkUsernameExist, insertUserProvider, updateUserProvider, checkEmailExist, deleteUser, changeUserPassword, getUserRole
})(ProviderUser);
//export default ProviderUser;
