/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ editUser, addErr, onUpdateUserDetail, userType, userRole, providerList, checkUsernameExist, checkEmailExist, showProvider, userRoledefault }) => (
    <Form>
        <div className="row">
            <div className="col-md-6">
                <FormGroup>
                    <Label for="firstName">First Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="f_name"
                        id="f_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter First Name"
                        value={editUser.f_name}
                        error={(addErr.f_name) ? true : false}
                        helperText={addErr.f_name}
                        onChange={(e) => onUpdateUserDetail('f_name', e.target.value)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="middleName">Middle Name</Label>
                    <TextField
                        type="text"
                        name="m_name"
                        id="m_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Middle Name"
                        value={editUser.m_name}
                        error={(addErr.m_name) ? true : false}
                        helperText={addErr.m_name}
                        onChange={(e) => onUpdateUserDetail('m_name', e.target.value)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="lastName">Last Name</Label>
                    <TextField
                        type="text"
                        name="l_name"
                        id="l_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Middle Name"
                        value={editUser.l_name}
                        error={(addErr.l_name) ? true : false}
                        helperText={addErr.l_name}
                        onChange={(e) => onUpdateUserDetail('l_name', e.target.value)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="Phone">Phone No<span className="required-field">*</span></Label>
                    <TextField
                        type="phone"
                        name="phone"
                        id="phone"
                        fullWidth
                        variant="outlined"
                        placeholder="Phone No"
                        inputProps={{ maxLength: 14 }}
                        value={editUser.phone}
                        error={(addErr.phone) ? true : false}
                        helperText={addErr.phone}
                        onChange={(e) => onUpdateUserDetail('phone', e.target.value)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="Email">Email<span className="required-field">*</span></Label>
                    <TextField
                        type="email"
                        name="email_id"
                        id="email_id"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Email"
                        value={editUser.email_id}
                        error={(addErr.email_id) ? true : false}
                        helperText={addErr.email_id}
                        onChange={(e) => onUpdateUserDetail('email_id', e.target.value)}
                        onKeyUp={(e) => checkEmailExist(e.target.value, editUser.user_id)}
                    />
                </FormGroup>
            </div>



            <div className="col-md-6">
                <FormGroup>
                    <Label for="userType">Select User Type<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="mdv_user_type_id"
                        id="mdv_user_type_id"
                        placeholder="Enter Type"
                        defaultValue={editUser.mdv_user_type_id}
                        onChange={(e) => onUpdateUserDetail('mdv_user_type_id', e.target.value, e.target[e.target.selectedIndex].text)}

                    >
                        {userType && userType.map((type, key) => (
                            (type.mdv_id == 2)?<option value={type.mdv_id} key={key}>{type.value}</option>:''
                        ))}
                    </Input>
                    {(addErr.mdv_user_type_id) ? <FormHelperText>{addErr.mdv_user_type_id}</FormHelperText> : ''}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userRole">Select User Role<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="mdv_role_id"
                        id="mdv_role_id"
                        placeholder=""
                        /*defaultValue={editUser.mdv_role_id}*/
                        value={userRoledefault}
                        onChange={(e) => onUpdateUserDetail('mdv_role_id', e.target.value)}
                    >
                        <option value="">Select</option>

                        {userRole && userRole.map((role, key) => (
                            <option value={role.mdv_id} key={key}>{role.value}</option>
                        ))}

                    </Input>
                    {(addErr.mdv_role_id) ? <FormHelperText>{addErr.mdv_role_id}</FormHelperText> : ''}
                </FormGroup>
            </div>
        
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userName">User Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="username"
                        id="username"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Username"
                        value={editUser.username}
                        error={(addErr.username) ? true : false}
                        helperText={addErr.username}
                        onChange={(e) => onUpdateUserDetail('username', e.target.value)}
                        onKeyUp={(e) => checkUsernameExist(e.target.value, editUser.user_id)}
                    />
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label for="Status">Status<span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(editUser.status == 1) ? true : false}
                                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(editUser.status == 0) ? true : false}
                                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>
    </Form>
);

export default UpdateUserForm;
