/**
 * Support Request
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import PaymentChart from 'Components/Charts/PaymentChart';
import { Link } from 'react-router-dom';
// intl messagess
import IntlMessages from 'Util/IntlMessages';

class PaymentDashboard extends Component {
   render() {
      return (
         <div className="support-widget-wrap">
            <div className="text-center py-10">
               <PaymentChart />
            </div>
            <List className="list-unstyled p-0">
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardPaymentReceived" /></p>
                  <Badge color="primary" className="px-4">${(this.props.invoiceStatus)? parseFloat(this.props.invoiceStatus['received']).toFixed(2):"0.00"}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/4`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardPaymentDue" /></p>
                  <Badge color="warning" className="px-4">${(this.props.invoiceStatus)?parseFloat(this.props.invoiceStatus['total_amount']-this.props.invoiceStatus['received']).toFixed(2):"0.00"}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/7`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardPaymentSubmitted" /></p>
                  <Badge color="info" className="px-4">${(this.props.invoiceStatus)?parseFloat(this.props.invoiceStatus['total_amount']).toFixed(2):"0.00"}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/6`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardPaymentIOU" /></p>
                  <Badge color="dark" className="px-4">${(this.props.refundStatus)?parseFloat(this.props.refundStatus['iou']).toFixed(2):"0.00"}</Badge>
                  <Link to={`/provider/invoice/refund-invoices/1`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardPaymentRefund" /></p>
                  <Badge color="purple" className="px-4">${(this.props.refundStatus)?parseFloat(this.props.refundStatus['refund']).toFixed(2):"0.00"}</Badge>
                  <Link to={`/provider/invoice/refund-invoices/0`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
            </List>
            
         </div>
      );
   }
}


const mapStateToProps = ({ DashboardDetails }) => {
   //console.log(DashboardDetails)
   const { loading, invoiceStatus, refundStatus } = DashboardDetails;   
   return { loading, invoiceStatus, refundStatus }
}

export default connect(mapStateToProps, {
})(PaymentDashboard);