/**
 * Recent Orders
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// api
import api from 'Api';

class SupportDashboard extends Component {

	state = {
		recentOrders: null
	}



	render() {
		const { recentOrders } = this.state;
		return (
			<div className="table-responsive">
				<table className="table table-hover mb-0 table-sm">
					<thead>
						<tr>
							<th>Customer Type</th>
							<th>Subject</th>
							<th>Call Date/Time</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{this.props.customerSupport && this.props.customerSupport.map((support, key) => (
							<tr key={key}>
								<td>{(support.provider_id) ? "Provider" : 'Paitent'}</td>
								<td>
									{support.subject}
								</td>
								<td>{support.call_date_time}</td>
								<td>
									{support.status}
								</td>
								<td>
									<span className="list-action">
										{(support.follow_up_admin == 1) ? <Link to="#" title="Need attention"><i className="zmdi zmdi-alert-triangle text-danger"></i></Link> : ''}
										<Link to={`/provider/customer-support/view-ticket/${support.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
									</span>
								</td>
							</tr>
						))}
					</tbody>
				</table>
				<div className="col-sm-12 text-right">
					<Link to="/provider/profile" title="View Ticket">View All</Link>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({ DashboardDetails }) => {
	const { loading, customerSupport } = DashboardDetails;
	return { loading, customerSupport }

}
//export default SupportDashboard;
export default connect(mapStateToProps, {
})(SupportDashboard);
