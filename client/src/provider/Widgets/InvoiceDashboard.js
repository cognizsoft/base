/**
 * Support Request
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import InvoiceChart from 'Components/Charts/InvoiceChart';
import { Link } from 'react-router-dom';
// intl messagess
import IntlMessages from 'Util/IntlMessages';

class InvoiceDashboard extends Component {
   render() {
      return (
         <div className="support-widget-wrap">
            <div className="text-center py-10">
               <InvoiceChart />
            </div>
            <List className="list-unstyled p-0">
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardInvoiceSubmitted" /></p>
                  <Badge color="primary" className="px-4">{(this.props.invoiceStatus)?this.props.invoiceStatus['submitted']:0}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/1`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardInvoiceApproved" /></p>
                  <Badge color="secondary" className="px-4">{(this.props.invoiceStatus)?this.props.invoiceStatus['approved']:0}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/3`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardInvoiceConfirm" /></p>
                  <Badge color="success" className="px-4">{(this.props.invoiceStatus)?this.props.invoiceStatus['confirm_payment']:0}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/4`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardInvoiceCancel" /></p>
                  <Badge color="warning" className="px-4">{(this.props.invoiceStatus)?this.props.invoiceStatus['cancel']:0}</Badge>
                  <Link to={`/provider/invoice/submitted-invoices/2`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
            </List>
            
         </div>
      );
   }
}


const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, invoiceStatus } = DashboardDetails;
   return { loading, invoiceStatus }

}

export default connect(mapStateToProps, {
   
})(InvoiceDashboard);