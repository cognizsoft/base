/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
   FormGroup,
   Form,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';

// edit address from
import EditAddressForm from './EditAddressForm';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const Address = ({ providerLocation }) => (
         <div className="address-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
               {providerLocation.map((location, idx) => (
                  <div className="view-section-inner p-location mb-20" key={idx}>

                    <div className="view-box mb-10 ">

                        <h2>Physical Address</h2>
                        <div className="width-100">
                           <table>
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Location Name :</td>
                                  <td>{(location.physical_location_name) ? location.physical_location_name : '-'}</td>
                                  <td className="fw-bold">Zip Code :</td>
                                  <td>{(location.physical_zip_code) ? location.physical_zip_code : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address1 :</td>
                                  <td>{(location.physical_address1) ? location.physical_address1 : '-'}</td>
                                  <td className="fw-bold">Address2 :</td>
                                  <td>{(location.physical_address2) ? location.physical_address2 : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Country :</td>
                                  <td>{(location.physical_country) ? location.physical_country : '-'}</td>
                                  <td className="fw-bold">State :</td>
                                  <td>{(location.physical_state) ? location.physical_state : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Region :</td>
                                  <td>{(location.physical_region) ? location.physical_region : '-'}</td>
                                  <td className="fw-bold">City :</td>
                                  <td>{(location.physical_city) ? location.physical_city : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Phone No :</td>
                                  <td>{(location.physical_phone_no) ? location.physical_phone_no : '-'}</td>
                                  <td className="fw-bold">Billing and Physical address same:</td>
                                  <td>{(location.billing_address_flag == 1) ? 'Yes' : 'No'}</td>
                                </tr>
                                
                              </tbody>
                           </table>
                        </div>

                     </div>

                     {location.billing_address_flag !== 1 &&
                      <div className="view-box">
                        
                        <h2>Billing Address</h2>

                        <div className="width-100">
                           <table>
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Location Name :</td>
                                  <td>{(location.billing_location_name) ? location.billing_location_name : '-'}</td>
                                  <td className="fw-bold">Zip Code :</td>
                                  <td>{(location.billing_zip_code) ? location.billing_zip_code : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address1 :</td>
                                  <td>{(location.billing_address1) ? location.billing_address1 : '-'}</td>
                                  <td className="fw-bold">Address2 :</td>
                                  <td>{(location.billing_address2) ? location.billing_address2 : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Country :</td>
                                  <td>{(location.billing_country) ? location.billing_country : '-'}</td>
                                  <td className="fw-bold">State :</td>
                                  <td>{(location.billing_state) ? location.billing_state : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Region :</td>
                                  <td>{(location.billing_region) ? location.billing_region : '-'}</td>
                                  <td className="fw-bold">City :</td>
                                  <td>{(location.billing_city) ? location.billing_city : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Phone No :</td>
                                  <td>{(location.billing_phone_no) ? location.billing_phone_no : '-'}</td>
                                  <td className="fw-bold"></td>
                                  <td></td>
                                </tr>
                                
                              </tbody>
                           </table>
                        </div>

                     </div>
                     }

                  </div>
               ))}
            </div>
         </div>
      );
export default Address;
