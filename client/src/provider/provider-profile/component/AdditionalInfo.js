/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';

// edit address from
import EditAddressForm from './EditAddressForm';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const AdditionalInfo = ({ additionalDiscount, additionalFee }) => (
  <div className="address-wrapper d-flex">
    <div className="modal-body page-form-outer view-section">

      <div className="view-section-inner mb-20">
        <h3 className="p-view-title">Additional Discount</h3>
        <div className="view-box">
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>Discount Type</th>
                  <th>Discount Rate(%)</th>
                  <th>Reduced Interest PCT(%)</th>
                  <th>Loan Term (months)</th>
                  <th>From Date</th>
                  <th>Expire Date</th>
                </tr>
              </thead>
              <tbody>
                {additionalDiscount.map((discount, idx) => (
                  <tr key={idx}>
                    <td>{(discount.discount_type) ? discount.discount_type : ''}</td>
                    <td>{discount.discount_rate + '%'}</td>
                    <td>{discount.redu_discount_rate + '%'}</td>
                    <td>{(discount.loan_term_month_value) ? discount.loan_term_month_value : ''}</td>
                    <td>{(discount.discount_from_date) ? discount.discount_from_date : '-'}</td>
                    <td>{(discount.discount_to_date) ? discount.discount_to_date : '-'}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div className="view-section-inner">
        <h3 className="p-view-title">Additional Fee</h3>
        <div className="view-box">
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>Fee Type</th>
                  <th>Convience Fee</th>
                  <th>From Date</th>
                  <th>To Date</th>
                </tr>
              </thead>
              <tbody>
                {additionalFee.map((fee, idx) => (
                  <tr key={idx}>
                    <td>{(fee.additional_fee_type) ? fee.additional_fee_type : ''}</td>
                    <td>{(fee.additional_conv_fee) ? '$' + parseFloat(fee.additional_conv_fee).toFixed(2) : '-'}</td>
                    <td>{(fee.additional_from_date) ? fee.additional_from_date : '-'}</td>
                    <td>{(fee.additional_to_date) ? fee.additional_to_date : '-'}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
);
export default AdditionalInfo;
