/**
 * Email Prefrences Page
 */
import React, { Component } from 'react';
import Switch from 'react-toggle-switch';
import Button from '@material-ui/core/Button';
import { FormGroup, Input } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';

// intl messages
import IntlMessages from 'Util/IntlMessages';

const BankDetails = ({ providerBank, providerSpl }) => (
         <div className="prefrences-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
              {providerBank.map((bank, idx) => (
               <div className="view-section-inner mb-20" key={idx}>
                  <h3 className="p-view-title">Bank Details</h3>
                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Location Name :</td>
                               <td>{(bank.location_name) ? bank.location_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Bank Name :</td>
                               <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Name on Account :</td>
                               <td>{(bank.name_on_account) ? bank.name_on_account : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Routing Number :</td>
                               <td>{(bank.rounting_no) ? bank.rounting_no : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Bank A/C Type :</td>
                               <td>{(bank.bank_account_type) ? bank.bank_account_type : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Bank A/C# :</td>
                               <td>{(bank.bank_accout) ? bank.bank_accout : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Bank Address :</td>
                               <td>{(bank.bank_address) ? bank.bank_address : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold"></td>
                               <td></td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                 </div>

               </div>
               ))}

            </div>
         </div>
      );
export default BankDetails;