/**
 * User Block
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const UserBlock = ({ providerDetails }) => (
            <div className="profile-top mb-20">
                <img src={require('Assets/img/profile-bg.jpg')} alt="profile banner" className="img-fluid" width="1920" height="345" />
                <div className="profile-content">
                    <div className="media">
                        <img src={require('Assets/avatars/avatar-default.png')} alt="user profile" className="rounded-circle mr-30 bordered" width="140" height="140" />
                        <div className="media-body pt-50">
                            <div className="mb-20">
                                <h2 className="user-full-name">{(providerDetails.provider_name) ? providerDetails.provider_name : '-'}</h2>
                                <p>{(providerDetails.provider_email) ? providerDetails.provider_email : '-'}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="provider-edit-profile">
                    <Link to={`/provider/edit-profile/${providerDetails.provider_id}`} title="Edit Profile">
                       <i className="ti-pencil"></i>
                       <span> Edit Profile</span>
                    </Link>
                </div>
            </div>
        );

export default UserBlock;
