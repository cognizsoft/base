/**
 * Profile Page
 */
import React, { Component } from 'react';
import { FormGroup, Input, Form, Label, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';

// intlmessages
import IntlMessages from 'Util/IntlMessages';

const Profile = ({ provider_details }) => (
         <div className="profile-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">
                <h3 className="p-view-title">My Profile</h3>
                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Provider Name :</td>
                               <td className="text-capitalize">{(provider_details.provider_name) ? provider_details.provider_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Provider Type :</td>
                               <td className="text-capitalize">{(provider_details.provider_type) ? provider_details.provider_type : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Primary Contact Phone :</td>
                               <td className="text-capitalize">{(provider_details.provider_primary_contact_phone) ? provider_details.provider_primary_contact_phone : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Email Address :</td>
                               <td>{(provider_details.provider_email) ? provider_details.provider_email : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Fax No :</td>
                               <td>{(provider_details.provider_fax_no) ? provider_details.provider_fax_no : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Tax ID/SSN :</td>
                               <td>
                                  {(provider_details.provider_taxid_ssn) ? provider_details.provider_taxid_ssn.replace(/.(?=.{4})/g, 'x') : '-'}  
                               </td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

                  <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Primary Contact :</td>
                               <td>{(provider_details.provider_primary_contact) ? provider_details.provider_primary_contact : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Website :</td>
                               <td>{(provider_details.provider_website) ? provider_details.provider_website : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">In Network :</td>
                               <td>{(provider_details.provider_network == 1) ? 'Yes' : 'No'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Phone No :</td>
                               <td>{(provider_details.provider_phone_no) ? provider_details.provider_phone_no : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Status :</td>
                               <td>{(provider_details.provider_status == 1) ? 'Active' : 'Inactive'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold"></td>
                               <td></td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      );
export default Profile;