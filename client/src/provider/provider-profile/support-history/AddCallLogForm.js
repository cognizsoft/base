/**
 * Add New User Form
 */
import React from 'react';
import { Form, Label, FormGroup, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddCallLogForm = ({ addErr, addCallLogDetails, onChangeAddCallLogDetails, DatePicker, dateTimeStartDate, followUpDateTimeStartDate, masterTicketSource, providerApplication, providerInvoice }) => (
    <div>
        <div className="row">
            
            <div className="col-md-6">
                <FormGroup tag="fieldset" className="ticket-related">
                    <Label>Ticket related to: </Label>
                    <FormGroup check className="mr-20">
                        <Label check>
                        <Input 
                         type="checkbox" 
                         id="check-ticket-related"
                         checked={(addCallLogDetails.ticket_related_ac == 1) ? true : false}
                         value={(addCallLogDetails.ticket_related_ac != '') ? addCallLogDetails.ticket_related_ac : ''}
                         name="ticket_related_ac" 
                         onChange={(e) => onChangeAddCallLogDetails('ticket_related_ac', e.target.value)}
                         />{' '}
                         Profile
                        </Label>
                         {(addErr.ticket_related_ac != '') ? 
                          <FormHelperText>{addErr.ticket_related_ac}</FormHelperText>
                          : ''}
                    </FormGroup>

                    <FormGroup check className="mr-20">
                        <Label check>
                        <Input 
                         type="checkbox" 
                         id="check-ticket-related"
                         disabled={(!providerApplication.length > 0) ? true : false}
                         checked={(addCallLogDetails.ticket_related_app == 1) ? true : false}
                         value={(addCallLogDetails.ticket_related_app != '') ? addCallLogDetails.ticket_related_app : ''}
                         name="ticket_related_app" 
                         onChange={(e) => onChangeAddCallLogDetails('ticket_related_app', e.target.value)}
                         />{' '}
                         Application
                        </Label>
                         {(addErr.ticket_related_app != '') ? 
                          <FormHelperText>{addErr.ticket_related_app}</FormHelperText>
                          : ''}
                    </FormGroup>

                    <FormGroup check className="mr-20">
                        <Label check>
                        <Input 
                         type="checkbox" 
                         id="check-ticket-related"
                         disabled={(!providerInvoice.length > 0) ? true : false}
                         checked={(addCallLogDetails.ticket_related_invoice == 1) ? true : false}
                         value={(addCallLogDetails.ticket_related_invoice != '') ? addCallLogDetails.ticket_related_invoice : ''}
                         name="ticket_related_invoice" 
                         onChange={(e) => onChangeAddCallLogDetails('ticket_related_invoice', e.target.value)}
                         />{' '}
                         Invoice
                        </Label>
                         {(addErr.ticket_related_invoice != '') ? 
                          <FormHelperText>{addErr.ticket_related_invoice}</FormHelperText>
                          : ''}
                    </FormGroup>
                </FormGroup>    
            </div>

            {addCallLogDetails.ticket_related_app == 1 &&
            <div className="col-md-3">
                <FormGroup>
                    <Label for="application_no">Application No:</Label>
                    <Input
                        type="select"
                        name="application_no"
                        id="application_no"
                        placeholder=""
                        value={addCallLogDetails.application_no}
                        onChange={(e) => onChangeAddCallLogDetails('application_no', e.target.value)}
                    >
                        <option value="">Select Application</option>
                        {providerApplication && providerApplication.map((app, key) => (
                            <option value={app.application_id} key={key}>
                                {app.application_no}
                            </option>
                        ))}
                        
                    </Input>
                    {(addErr.application_no) ? <FormHelperText>{addErr.application_no}</FormHelperText> : ''}
                </FormGroup>
            </div>
            }

            {addCallLogDetails.ticket_related_invoice == 1 &&
            <div className="col-md-3">
                <FormGroup>
                    <Label for="invoice_no">Invoice No:</Label>
                    <Input
                        type="select"
                        name="invoice_no"
                        id="invoice_no"
                        placeholder=""
                        value={addCallLogDetails.invoice_no}
                        onChange={(e) => onChangeAddCallLogDetails('invoice_no', e.target.value)}
                    >
                        <option value="">Select Invoice</option>
                        {providerInvoice && providerInvoice.map((invoice, key) => (
                            <option value={invoice.provider_invoice_id} key={key}>
                                {invoice.invoice_number}
                            </option>
                        ))}
                        
                    </Input>
                    {(addErr.invoice_no) ? <FormHelperText>{addErr.invoice_no}</FormHelperText> : ''}
                </FormGroup>
            </div>
            }

        </div>

        <div className="row">
            
            <div className="col-md-6">
                <FormGroup>
                    <Label for="subject">Subject:</Label>
                    <TextField
                        type="text"
                        name="subject"
                        id="subject"
                        fullWidth
                        variant="outlined"
                        placeholder="Subject"
                        value={addCallLogDetails.subject}
                        error={(addErr.subject)?true:false}
                        helperText={addErr.subject}
                        onChange={(e) => onChangeAddCallLogDetails('subject', e.target.value)}
                    />
                    
                </FormGroup>
            </div>

        </div>        

        <div className="row">
            <div className="col-md-12">
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input
                        type="textarea" 
                        name="description" 
                        id="description" 
                        placeholder="Enter Description"
                        value={addCallLogDetails.description}
                        onChange={(e) => onChangeAddCallLogDetails('description', e.target.value)}
                        />
                </FormGroup>
            </div>
        </div>

    </div>
);

export default AddCallLogForm;
