/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import {
    FormGroup,
    Label,
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';

import "react-datepicker/dist/react-datepicker.css";

import { isEmpty } from '../../validator/Validator';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
    careCouldDetails, getCareCouldDetails
} from 'Actions';
class careCloud extends Component {
    state = {
        currentModule: 26,
        currentPermision: {
            add: false,
            edit: false,
            view: false,
            delete: false
        },
        carecloud: {
            id: "",
            api_key: "",
            secret_key: "",
        },
        error: {},
    }
    /*
    * Title :- componentDidMount
    * Descrpation :- This function call when component call and call another function or action
    * Author : Cognizsoft and Ramesh Kumar
    * Date :- June 5,2019
    */
    componentDidMount() {
        this.permissionFilter(this.state.currentModule);
        this.props.getCareCouldDetails();
    }

    /*
    * Title :- permissionFilter
    * Descrpation :- This function use filter action permission according to current user
    * Author : Cognizsoft and Ramesh Kumar
    * Date :- April 22,2019
    */
    permissionFilter = (name) => {
        let per = JSON.parse(this.props.user);

        let newUser = per.user_permission.filter(
            function (per) { return per.description == name }
        );

        this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
    }

    /*
    * Title :- onChnageExist
    * Descrpation :- This function use for onchnage
    * Author : Cognizsoft and Ramesh Kumar
    * Date :- 9 Aug,2019
    */
    onChnageExist(key, value) {
        let { error } = this.state;
        switch (key) {
            case 'api_key':
                if (isEmpty(value)) {
                    error[key] = "API key can't be blank";
                } else {
                    error[key] = '';
                }

                break;
            case 'secret_key':
                if (isEmpty(value)) {
                    error[key] = "Secret key can't be blank";
                } else {
                    error[key] = '';
                }

                break;

        }

        this.setState({
            carecloud: {
                ...this.state.carecloud,
                [key]: value
            }
        });
        this.setState({ error: error });
    }
    /*
    * Title :- searchCustomer
    * Descrpation :- This function use for search custoemr details
    * Author : Cognizsoft and Ramesh Kumar
    * Date :- 9 Aug,2019
    */
    Submit() {
        this.props.careCouldDetails(this.state.carecloud);
        const error = {}
        this.setState({ error: error });
    }

    /*
    * Title :- validateSearch
    * Descrpation :- This function use for validate data
    * Author : Cognizsoft and Ramesh Kumar
    * Date :- 9 Aug,2019
    */
    validateSearch() {
        return (
            this.state.error.api_key === '' &&
            this.state.error.secret_key === ''
        )
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.careCouldSecret !== undefined && nextProps.careCouldSecret != '') {
            let carecloud = {
                id: nextProps.careCouldSecret.id,
                api_key: nextProps.careCouldSecret.ap_key,
                secret_key: nextProps.careCouldSecret.secret_key,
            }
            let error = {
                api_key: '',
                secret_key: '',
            }
            this.setState({ carecloud: carecloud, error: error });
        };
    }
    goBack() {
        this.props.history.goBack(-1)
    }
    render() {
        return (
            <div className="user-management">

                <Helmet>
                    <title>Health Partner | Users Management</title>
                    <meta name="description" content="Reactify Widgets" />
                </Helmet>
                <PageTitleBar
                    title={<IntlMessages id="sidebar.careCloudPMS" />}
                    match={this.props.match}
                />

                {this.state.currentPermision.view != 1 &&
                    <RctCollapsibleCard heading="" fullBlock >
                        <div className="col-sm-12 p-20">
                            You are not authorized to access this page.
               </div>
                    </RctCollapsibleCard>
                }

                {this.state.currentPermision.view == 1 &&
                    <RctCollapsibleCard heading="" fullBlock>
                        <div className="modal-body page-form-outer text-left">

                            <div className="row">
                                <div className="col-md-4">
                                    <FormGroup>
                                        <Label for="exist_first_name">API Key</Label><br />
                                        <TextField
                                            type="text"
                                            name="exist_first_name"
                                            id="exist_first_name"
                                            fullWidth
                                            variant="outlined"
                                            placeholder="API Key"
                                            value={this.state.carecloud.api_key}
                                            error={(this.state.error.api_key) ? true : false}
                                            helperText={this.state.error.api_key}
                                            onChange={(e) => this.onChnageExist('api_key', e.target.value)}
                                        />
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <FormGroup>
                                        <Label for="secret_key">Secret Key</Label><br />
                                        <TextField
                                            type="text"
                                            name="secret_key"
                                            id="secret_key"
                                            fullWidth
                                            variant="outlined"
                                            placeholder="Secret Key"
                                            value={this.state.carecloud.secret_key}
                                            error={(this.state.error.secret_key) ? true : false}
                                            helperText={this.state.error.secret_key}
                                            onChange={(e) => this.onChnageExist('secret_key', e.target.value)}
                                        />
                                    </FormGroup>
                                </div>
                                <div className="col-md-4">
                                    <div className="mt-30">
                                        <Button onClick={this.Submit.bind(this)} disabled={!this.validateSearch()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                            Submit
                                        </Button>
                                        <Button onClick={this.goBack.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                            Cancel
                                        </Button>
                                    </div>
                                </div>
                            </div>



                        </div>

                        {this.props.loading &&
                            <RctSectionLoader />
                        }
                    </RctCollapsibleCard>
                }

            </div>
        );
    }
}
// map state to props
const mapStateToProps = ({ authUser, CareCouldReducer }) => {
    const { user } = authUser;
    const { loading, careCouldSecret } = CareCouldReducer;
    return { loading, user, careCouldSecret }



}

export default connect(mapStateToProps, {
    careCouldDetails, getCareCouldDetails
})(careCloud); 