/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import AppConfig from 'Constants/AppConfig';
import {
   planDetails, clearRedirectURL, allPlanDetails, viewSubmittedInvoice, viewInvoice
} from 'Actions';
class PlanDetails extends Component {

   state = {
      payment_history: '',
      payment_history_data: false,
      nested_arr_row: '',
      nested_arr_row_data: false,
      plan_row: '',
      plan_row_data: false,
      paid_status: '',
      current_month_invoice_sum: 0,
      current_month_invoice: '',
      current_month_invoice_data: false,
      overdue_month_invoice_grp: '',
      overdue_month_invoice_grp_data: false,
      invoice_last_index_value: 0,
      all_plans_total_amount: 0,
      single_plan_total: 0
   }

   componentDidMount() {
      
      this.props.clearRedirectURL();
      //this.props.allPlanDetails(this.props.match.params.appid);
      this.props.viewSubmittedInvoice(this.props.match.params.invoice_id, this.props.match.params.provider_id);
     
   }

   goBack(){
      this.props.history.goBack(-1)
   }
   submittedViewInvoice(invoice_id, provider_id) {
      this.props.viewInvoice(invoice_id, provider_id)
   }
   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails, application_details, pp_details, application_plans, selectedApplication, getInvoiceApplication } = this.props;
      let { payment_history, payment_history_data, nested_arr_row, nested_arr_row_data, plan_row, plan_row_data, paid_status, current_month_invoice_sum, current_month_invoice, current_month_invoice_data, overdue_month_invoice_grp, overdue_month_invoice_grp_data, invoice_last_index_value, all_plans_total_amount, single_plan_total } = this.state;
      
      
      //INVOICE AMOUNT
      if(selectedApplication) {

        var amount = selectedApplication.result.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex == 0) {
                accumulator['total_amount'] = currentValue.loan_amount;
                accumulator['discount_amount'] = currentValue.hps_discount_amt;
            } else {
                accumulator['total_amount'] = accumulator['total_amount'] + currentValue.loan_amount;
                accumulator['discount_amount'] = accumulator['discount_amount'] + currentValue.hps_discount_amt;
            }
            return accumulator
        }, []);

      
      }
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title="Provider Invoice" match={this.props.match} />
            <div className="row">
            
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
                           </li>
                           <li>
                           <a href="javascript:void(0)" onClick={this.submittedViewInvoice.bind(this, this.props.match.params.invoice_id, this.props.match.params.provider_id)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                           </li>
                        </ul>
                     </div>
                     <div className="p-10">
                        <h1 className="text-center mb-20">
                           <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                        </h1>
                        
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt submitted-invoice-view">

                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Provider Information</th>
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>App No :</strong></td>
                                    <td>
                                        {(selectedApplication) ? selectedApplication.provider.provider_ac : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>Name :</strong></td>
                                    <td className="text-capitalize">
                                        {(selectedApplication) ? selectedApplication.provider.name : '-'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td className="text-right"><strong>Address :</strong></td>
                                    <td>
                                        {(selectedApplication) ? selectedApplication.provider.address1+' '+selectedApplication.provider.address2+' '+selectedApplication.provider.city+', '+selectedApplication.provider.state_name+' - '+selectedApplication.provider.zip_code : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>Phone :</strong></td>
                                    <td>
                                        {(selectedApplication) ? selectedApplication.provider.primary_phone : '-'}
                                    </td>
                                 </tr>
                              
                                 </tbody>
                              </table>
                           
                           </div>
                          
                           <div className="add-card">
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Invoice Information</th>
                                    <th colSpan="2"></th>
                                 </tr>
                                 
                                 <tr>
                                    <td className="text-right"><strong>Invoice Number :</strong></td>
                                    <td>
                                        {(selectedApplication) ? (selectedApplication.invoice_number !== undefined) ? selectedApplication.invoice_number : selectedApplication.provider.invoice_number : '-' } 
                                    </td>
                                    <td className="text-right"><strong>Discount Amount :</strong></td>
                                    <td>
                                        {(selectedApplication) ? '$'+parseFloat(amount.discount_amount).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>Invoice Date :</strong></td>
                                    <td>
                                        {(selectedApplication) ? (selectedApplication.date_created !== undefined) ? selectedApplication.date_created : current_date : '-' } 
                                    </td>
                                    <td className="text-right"><strong>Amount Due :</strong></td>
                                    <td>
                                        {(selectedApplication) ? '$'+parseFloat(amount.total_amount.toFixed(2)-amount.discount_amount.toFixed(2)).toFixed(2) : '$0.00'}  
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>No. of account included :</strong></td>
                                    <td>
                                        {(selectedApplication) ? selectedApplication.result.length : '-'}
                                    </td>
                                    <td className="text-right"><strong>IOU Adjustment :</strong> </td>
                                    
                                    <td>{(getInvoiceApplication) ? (getInvoiceApplication.iou_paid_amount !== null) ? '$'+parseFloat(getInvoiceApplication.iou_paid_amount).toFixed(2) : '$0.00' : '$0.00'}</td>
                                    
                                 </tr>
                                 <tr>
                                    <td className="text-right"><strong>Invoice Amount :</strong></td>
                                    <td>
                                        {(selectedApplication) ? '$'+parseFloat(amount.total_amount).toFixed(2) : '$0.00'} 
                                    </td>
                                    <td className="text-right"><strong>Total amount due after Adjustment :</strong> </td>
                                    <td>{(getInvoiceApplication && selectedApplication) ? (getInvoiceApplication.iou_paid_amount !== null) ? '$'+(parseFloat(amount.total_amount.toFixed(2)-amount.discount_amount.toFixed(2)) - parseFloat(getInvoiceApplication.iou_paid_amount).toFixed(2)).toFixed(2) : '$0.00' : '$0.00'}</td>
                                 </tr>
                                 
                                  <tr>
                                    
                                    <td className="text-right"><strong>Status :</strong></td>
                                    <td>
                                        {(selectedApplication) ? (selectedApplication.invoice_status !== undefined) ? selectedApplication.invoice_status : selectedApplication.provider.invoice_status : '-' } 
                                    </td>
                                    
                                    {/*<td className="text-right"><strong>Remaining IOU Balance Amount:</strong> </td>
                                    <td></td>
                                    */}
                                  </tr>
                                 
                                 </tbody>
                              </table>
                           
                           </div>

                        </div>

                        {(selectedApplication && selectedApplication.invoiceStatus == 4) &&
                           <React.Fragment>
                           <h2 className="text-center mb-10">Payment Details</h2>
                           <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt submitted-invoice-vieww">
                              <div className="add-card w-100 mw-100 p-10">
                                 <div className="row">
                                    <div className="col-sm-6 p-5 d-inline">
                                       <strong>Check Number :</strong> {selectedApplication.txn_no}
                                    </div>
                                    <div className="col-sm-6 p-5 d-inline">
                                       <strong>Check Amount :</strong> {(selectedApplication.check_amount)? '$'+parseFloat(selectedApplication.check_amount).toFixed(2):'0.00'}
                                    </div>
                                    <div className="col-sm-6 p-5 d-inline">
                                       <strong>Bank Name :</strong> {selectedApplication.bank_name}
                                    </div>
                                    <div className="col-sm-6 p-5 d-inline">
                                       <strong>Paid Date :</strong> {selectedApplication.date_paid}
                                    </div>
                                    <div className="col-sm-6 p-5 d-inline">
                                        <strong>IOU Adjustment :</strong> {(getInvoiceApplication.iou_paid_amount !== null) ? '$'+parseFloat(getInvoiceApplication.iou_paid_amount).toFixed(2) : '$0.00'}
                                     </div>
                                    <div className="col-sm-12 p-5 d-inline">
                                       <strong>Note :</strong> {selectedApplication.comment}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           </React.Fragment>
                        }
                        
                        <div className="table-responsive mb-40 pymt-history">
                        <h2 className="text-center mb-10">Invoice Details</h2>
                           <table className="table table-borderless admin-application-payment-plan">
                              <thead>
                                <tr>
                                  <th>SN#</th>
                                  <th>Loan No</th>
                                  <th>A/C No</th>
                                  <th>App No</th>
                                  <th>First Name</th>
                                  <th>M/I</th>
                                  <th>Last Name</th>
                                  <th>DoB</th>
                                  <th>Phone</th>
                                  <th>City</th>
                                  <th>State</th>
                                  <th>Service Date</th>
                                  <th>Loan Amt</th>
                                </tr>
                              </thead>
                              <tbody>

                                 {selectedApplication && selectedApplication.result.map((invoice, idx) => {
                                    return (
                                       <tr key={idx}>
                                        <td>{idx+1}</td>
                                        <td>{invoice.plan_number}</td>
                                        <td>{invoice.patient_ac}</td>
                                        <td>{invoice.application_no}</td>
                                        <td>{invoice.f_name}</td>
                                        <td>{invoice.m_name}</td>
                                        <td>{invoice.l_name}</td>
                                        <td>{invoice.dob}</td>
                                        <td>{invoice.peimary_phone}</td>
                                        <td>{invoice.City}</td>
                                        <td>{invoice.name}</td>
                                        <td>{invoice.procedure_date}</td>
                                        <td>{'$'+parseFloat(invoice.loan_amount).toFixed(2)}</td>
                                       </tr> 
                                    )
                                 })}
                                 
                              </tbody>
                           </table>
                        </div>

                        

                        

                       
                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer, providerInvoice }) => {
   const { nameExist, isEdit } = authUser;
   const { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL } = creditApplication;
   const {application_details, application_plan_details, pp_details, application_plans} = PaymentPlanReducer;
   const {selectedApplication, getInvoiceApplication} = providerInvoice;
   
   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, application_details, application_plan_details, pp_details, application_plans, selectedApplication, getInvoiceApplication, redirectURL }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails, viewSubmittedInvoice, viewInvoice
})(PlanDetails);