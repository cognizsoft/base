/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TableCell from "@material-ui/core/TableCell";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   invoiceProviderSubmitList, viewInvoice
} from 'Actions';

class submitInvoiceList extends Component {

   state = {
      currentModule: 20,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      invoiceList: null, // initial user data

   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.invoiceProviderSubmitList(this.props.match.params.status_id);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         },

      }
   })

   componentWillReceiveProps(nextProps) {

      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
   }

   viewInvoice(invoice_id, provider_id, e) {
      this.props.viewInvoice(invoice_id, provider_id)
   }

   render() {
      const { invoiceList, loading, selectedUser, editUser, allSelected, selectedInvoices } = this.state;

      

      //const invoiceList = this.props.invoiceList;
      const columns = [
         {
            name: 'Invoice ID',
            field: 'invoice_number',
         },
         {
            name: '# Application',
            field: 'total_application',
         },
         {
            name: 'Total Amount($)',
            field: 'total_amt',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$'+parseFloat(value.total_amt).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'HPS Discount',
            field: 'total_hps_discount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$'+parseFloat(value.total_hps_discount).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Date Created',
            field: 'date_created',
         },
         {
            name: 'Status',
            field: 'invoice_status',

         },
         {
            name: 'Action',
            field: 'invoice_status',
            options: {
               noHeaderWrap: true,
               filter: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <span className="list-action">
                        {(this.state.currentPermision.view && value.mdv_invoice_status_id !== 2) ? <div><a href="javascript:void(0)" onClick={this.viewInvoice.bind(this, value.provider_invoice_id, value.provider_id)} color="primary" title="Download Invoice"><i className="ti-cloud-down"></i></a> <Link to={`/provider/invoice/submitted/view/${value.provider_invoice_id}/${value.provider_id}`} title="View Invoice"><i className="ti-eye"></i></Link></div> : '-'}
                     </span>
                  )
               },
            }

         },



      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'submittedInvoice.csv' },
      };
      /*<Link to="/provider/invoice/preview-invoice" color="primary" className="caret btn-sm mr-10-custome">Create and Preview</Link>*/


      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.submittedInvoices" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>
               <div className="table-responsive invoice_table">
                  {this.props.invoiceSubmittedList &&
                     <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MaterialDatatable
                           data={this.props.invoiceSubmittedList}
                           columns={columns}
                           options={options}
                        />
                     </MuiThemeProvider>
                  }
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, providerInvoice }) => {
   const { user } = authUser;
   
   const { loading, invoiceSubmittedList } = providerInvoice;
   return { loading, invoiceSubmittedList, user }

}

export default connect(mapStateToProps, {
   invoiceProviderSubmitList, viewInvoice
})(submitInvoiceList);