/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TableCell from "@material-ui/core/TableCell";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// rct card
import { RctCard } from 'Components/RctCard/index';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import {
   invoiceProviderPendingList, invoiceProviderPreviewLoad, invoiceProviderSelectedPendingList, invoiceProviderreviewPDF, invoiceProviderInvoiceSubmit
} from 'Actions';

class pendingInvoiceList extends Component {

   state = {
      all: false,
      invoiceList: null, // initial user data
      existUpdate: false,
      redirectURL: false,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.props.invoiceProviderPreviewLoad();
      //this.props.invoiceProviderPendingList();
   }




   componentWillReceiveProps(nextProps) {
      let { addData, add_err } = this.state;
      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
      if (nextProps.previewInvoice !== undefined && this.state.existUpdate === false) {
         this.setState({ existUpdate: true });
         this.props.invoiceProviderSelectedPendingList(nextProps.previewInvoice);
      }
      if (this.state.existUpdate === false) {
         //const list = [1,2];
         //this.props.invoiceProviderSelectedPendingList(list);
         //this.setState({existUpdate:true});
      }
      (nextProps.submitInvoice) ? this.setState({ redirectURL: true }) : ''

   }
   downloadPDF() {
      this.props.invoiceProviderreviewPDF(this.props.previewInvoice)
   }
   goBack() {
      this.setState({ redirectURL: true })
   }
   invoiceSubmit() {
      this.props.invoiceProviderInvoiceSubmit(this.props.previewInvoice)
   }
   render() {
      if (this.state.redirectURL === true) {
         return (<Redirect to={`/provider/invoice/create-invoice`} />);
      }
      const { invoiceList, loading, selectedUser, editUser, allSelected, selectedInvoices } = this.state;
      let currentDate = new Date();
      currentDate = moment(currentDate).format('MM/DD/YYYY');
      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.previewInvoice" />}
               match={this.props.match}
            />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     {this.props.invoicePreviewList &&
                        <div className="p-50">
                           <h1 className="text-center mb-20">Provider Invoice</h1>
                           <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                              <div className="add-card w-30 mr-10">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Provider Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Account No :</strong> </td>
                                          <td>{this.props.providerDetails.provider_ac}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Name :</strong> </td>
                                          <td>{this.props.providerDetails.name}</td>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Address :</strong> </td>
                                          <td>
                                          {this.props.providerDetails.address1 + ' ' + this.props.providerDetails.address2 + ' ' + this.props.providerDetails.city + ' ' + this.props.providerDetails.state_name}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Phone :</strong> </td>
                                          <td>{this.props.providerDetails.primary_phone}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card w-30">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Invoice Information</th>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Invoice Number: </strong> </td>
                                          <td>{this.props.providerDetails.invoice_number}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Invoice Date :</strong> </td>
                                          <td>{currentDate}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Number of accounts included :</strong> </td>
                                          <td>{this.props.invoicePreviewList.length}</td>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Invoice Amount :</strong> </td>
                                          <td>${this.props.totalAmount.total_amount.toFixed(2)}</td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card w-30">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2"></th>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Discount Amount :</strong> </td>
                                          <td>${this.props.totalAmount.discount_amount.toFixed(2)}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Amount Due :</strong> </td>
                                          <td>${parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>IOU Adjustment :</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)
                                                   :
                                                   (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount)).toFixed(2)
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)
                                                      :
                                                      (parseFloat(this.props.providerDetails.refund_due)).toFixed(2)

                                                   :
                                                   '0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Total amount due after Adjustment :</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   '0.00'
                                                   :
                                                   (parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount) - parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount)).toFixed(2)
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      '0.00'
                                                      :
                                                      (this.props.totalAmount.total_amount) ?
                                                         (parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount) - parseFloat(this.props.providerDetails.refund_due)).toFixed(2)
                                                         : '0.00'

                                                   :
                                                   parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Remaining IOU Balance Amount:</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) - parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)).toFixed(2)
                                                   :
                                                   '0.00'
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      (parseFloat(this.props.providerDetails.refund_due) - parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)).toFixed(2)
                                                      :
                                                      '0.00'

                                                   :
                                                   '0.00'}</td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>


                           </div>
                           <div className="table-responsive mb-40 pymt-history provider-invoice-preview">
                              <h2 className="text-center mb-10">Invoice Detail</h2>
                              <div className="table-responsive">
                                 <table className="table table-borderless mb-0">
                                    <thead>
                                       <tr>
                                          <th>Plan ID</th>
                                          <th>A/C No</th>
                                          <th>App No</th>
                                          <th>First Name</th>
                                          <th>M/I</th>
                                          <th>Last Name</th>
                                          <th>DoB</th>
                                          <th>Phone</th>
                                          <th>City</th>
                                          <th>State</th>
                                          {/*<th>Zip</th>*/}
                                          <th>Service Date</th>
                                          <th>Loan Amt</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       {this.props.invoicePreviewList.map((invoice, idx) => (
                                          <tr key={idx}>
                                             <td>{invoice.plan_number}</td>
                                             <td>
                                                {invoice.patient_ac}
                                             </td>
                                             <td>
                                                {invoice.application_no}
                                             </td>
                                             <td>
                                                {invoice.f_name}
                                             </td>
                                             <td>
                                                {invoice.m_name}
                                             </td>
                                             <td>
                                                {invoice.l_name}
                                             </td>
                                             <td>
                                                {invoice.dob}
                                             </td>
                                             <td>
                                                {invoice.peimary_phone}
                                             </td>
                                             <td>
                                                {invoice.City}
                                             </td>
                                             <td>
                                                {invoice.name}
                                             </td>
                                             {/*<td>
                                             {invoice.zip_code}
                                          </td>*/}
                                             <td>
                                                {invoice.procedure_date}
                                             </td>
                                             <td>
                                                ${invoice.loan_amount.toFixed(2)}
                                             </td>
                                          </tr>
                                       ))}
                                    </tbody>
                                 </table>

                              </div>
                           </div>
                        </div>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                     <div className="invoice-head">
                        <div className="row">
                           <div className="col-sm-6 text-left text-danger">Note:- Dont refresh this page</div>
                           <div className="col-sm-6 text-right">
                              <ul className="list-inline">
                                 <li><a href="javascript:void(0);" onClick={this.goBack.bind(this)}><i className="mr-10 ti-back-left"></i> Cancel</a></li>
                                 <li><a href="javascript:void(0);" onClick={this.invoiceSubmit.bind(this)}><i className="mr-10 ti-save"></i> Submit</a></li>
                                 <li><a href="javascript:void(0);" onClick={this.downloadPDF.bind(this)}><i className="mr-10 ti-import"></i> Export PDF</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </RctCard>
               </div>
            </div>

         </div>
      );
   }
}

const mapStateToProps = ({ providerInvoice }) => {
   const { loading, invoicePreviewList, previewInvoice, totalAmount, providerDetails, submitInvoice } = providerInvoice;
   return { loading, invoicePreviewList, previewInvoice, totalAmount, providerDetails, submitInvoice }

}

export default connect(mapStateToProps, {
   invoiceProviderPendingList, invoiceProviderPreviewLoad, invoiceProviderSelectedPendingList, invoiceProviderreviewPDF, invoiceProviderInvoiceSubmit
})(pendingInvoiceList);