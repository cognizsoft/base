/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TableCell from "@material-ui/core/TableCell";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   invoiceProviderPendingList,invoiceProviderSelected
} from 'Actions';

class pendingInvoiceList extends Component {

   state = {
      currentModule: 20,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      invoiceList: null, // initial user data
      selectedUser: null, // selected user to perform operations
      allSelected: false,
      selectedInvoices: 0,
      redirectURL:0,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.invoiceProviderPendingList();
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         },
         MuiTableCell: {
            root: {
               padding: "4px 8px 4px 8px"
            }
         },
         MuiSvgIcon:{
            root : {
               width : '1em !important'
            }
         },
         MuiButton:{
            containedPrimary :{
               'background-color' : '#0E5D97',
               "text-transform": "none !important"
            }
         },
         MuiCheckbox : {
            root: {
               "&$checked" : {
                  color : '#0E5D97 !important',
               }
            }
            
         }
      }
   })
   //Select All user
   onSelectAllInvoice(e) {
      const { selectedInvoices, invoiceList } = this.state;
      let selectAll = selectedInvoices < invoiceList.length;
      if (selectAll) {
         let selectAllInvoices = invoiceList.map(invoice => {
            //invoice.checked = (invoice.procedure_status == 0 || new Date(invoice.procedure_date) > new Date()) ? false : true
            invoice.checked = (invoice.procedure_status == 0) ? false : true
            return invoice
         });
         this.setState({ invoiceList: selectAllInvoices, selectedInvoices: selectAllInvoices.length })
      } else {
         let unselectedInvoices = invoiceList.map(invoice => {
            invoice.checked = false
            return invoice;
         });
         this.setState({ selectedInvoices: 0, invoiceList: unselectedInvoices });
      }
   }
   /**
	 * On Select User
	 */
   onSelectInvoice(invoice) {
      invoice.checked = !invoice.checked;
      let selectedInvoices = 0;
      let invoiceList = this.state.invoiceList.map(invoiceData => {
         if (invoiceData.pp_id === invoice.pp_id) {
            if (invoiceData.checked && invoiceData.checked !== undefined) {
               selectedInvoices++;
            }
            return invoice;
         } else {
            if (invoiceData.checked && invoiceData.checked !== undefined) {
               selectedInvoices++;
            }
            return invoiceData;
         }
      });
      this.setState({ invoiceList, selectedInvoices });
   }
   componentWillReceiveProps(nextProps) {
      let { addData, add_err } = this.state;
      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
   }

   previewInvoice() {
      const { selectedInvoices, invoiceList } = this.state;
      var planId = invoiceList.reduce(function (accumulator, currentValue) {
         if (currentValue.checked === true) {
            accumulator.push(currentValue.pp_id);
         }
         return accumulator
      }, []);
      //console.log(planId);     
      this.props.invoiceProviderSelected(planId)
      this.setState({redirectURL:1})
   }

   render() {
      if (this.state.redirectURL == 1) {
         return (<Redirect to={`/provider/invoice/preview-invoice`} />);
      }
      const { invoiceList, loading, selectedUser, editUser, allSelected, selectedInvoices } = this.state;
      let diableStatus = 0;
      if(invoiceList !== null){
         diableStatus = invoiceList.reduce(function (accumulator, currentValue) {
            if(currentValue.checked === true){
               accumulator++
            }               
            return accumulator;
         }, 0);
      }
      
      //disabled={(value.procedure_status == 0 || new Date(value.procedure_date) > new Date()) ? true : false}
      //const invoiceList = this.props.invoiceList;
      const columns = [
         {
            name: 'select',
            field: ' ',
            options: {
               filter:false,
               short:false,
               customHeadRender: (rowDataObject, tableMeta, updateValue) => {
                  return (

                     <TableCell key={rowDataObject} component="th" className="heading-checkbox">
                        <Checkbox
                           key={rowDataObject.rowIndex}
                           indeterminate={selectedInvoices > 0 && selectedInvoices < invoiceList.length}
                           checked={selectedInvoices > 0}
                           onChange={(e) => this.onSelectAllInvoice(e)}
                           value="all"
                           color="primary"
                        />
                     </TableCell>
                  )
               },
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <FormControlLabel
                        key={tableMeta.rowIndex}
                        control={
                           <Checkbox
                              checked={(value.checked) ? true : false}
                              disabled={(value.procedure_status == 0) ? true : false}
                              onChange={() => this.onSelectInvoice(value)}
                              color="primary"
                           />
                        }
                     />
                  )

               }
            }
         },
         {
            name: 'Loan No',
            field: 'plan_number',
         },
         {
            name: 'A/C No',
            field: 'patient_ac',
         },
         {
            name: 'App No',
            field: 'application_no',
         },
         {
            name: 'First Name',
            field: 'f_name',
         },
         { name: 'Last Name', field: 'l_name', },
         {
            name: 'DoB',
            field: 'dob',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.dob.toString()
                  )
               },
            }
         },
         { name: 'Phone', field: 'peimary_phone', },
         {
            name: 'Address',
            field: 'peimary_phone',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.address1+' '+value.address2
                  )
               },
            }
         },
         { name: 'City', field: 'City', },
         { name: 'State', field: 'name', },
         { name: 'Service Date', field: 'procedure_date', },         
         {
            name: 'Loan Amt',
            field: 'loan_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.loan_amount).toFixed(2)
                  )
               }
            }
         },
         {
            name: 'Procedure Amt',
            field: 'procedure_amt',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.procedure_amt).toFixed(2)
                  )
               }
            }
         },
         {
            name: 'Status',
            field: 'procedure_status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  var status = 'Pending';
                  if (value.procedure_status == 0) {
                     status = 'Inactive';
                  } else if (value.procedure_status == 2) {
                     status = 'Submitted';
                  }
                  return (
                     status
                  )
               }
            }

         },



      ];
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         customToolbar: () => {
            return (

               (this.state.currentPermision.add) ? <CustomToolbar previewInvoice={this.previewInvoice.bind(this)} disabled={diableStatus} /> : ''

            );
         },
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'creditApplication.csv' },
      };
      /*<Link to="/provider/invoice/preview-invoice" color="primary" className="caret btn-sm mr-10-custome">Create and Preview</Link>*/


      return (
         <div className="provider-create-invoice">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.invoiceList" />}
               match={this.props.match}
            />
            <div className="col-md-12 text-danger">Note :- Provider shouldn't able to submit an invoice before the procedure date.</div>
            <RctCollapsibleCard fullBlock>
               <div className="table-responsive invoice_table">
                  {invoiceList &&
                     <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MaterialDatatable
                           data={invoiceList}
                           columns={columns}
                           options={options}
                        />
                     </MuiThemeProvider>
                  }
                  
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'Risk Factor View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left">

                              <div className="media-body">
                                 <p>Invoice ID: <span className="fw-bold">{selectedUser.invoice_id}</span></p>
                                 <p>Provider Name: <span className="fw-bold">{selectedUser.provider_name}</span></p>
                                 <p>Total Amount: <span className="fw-bold">{selectedUser.total_amount}</span></p>
                                 <p>Submit Date: <span className="fw-bold">{selectedUser.submit_date}</span></p>
                                 <p>Status: <span className="fw-bold">{selectedUser.status}</span></p>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, providerInvoice }) => {
   const { user } = authUser;
   const { loading, invoiceList } = providerInvoice;
   return { loading, invoiceList, user }

}

export default connect(mapStateToProps, {
   invoiceProviderPendingList, invoiceProviderSelected
})(pendingInvoiceList);