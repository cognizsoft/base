/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';

import { URL } from '../../apifile/URL';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup, Label, Input,
} from 'reactstrap';
import Button from '@material-ui/core/Button';
import { isObjectEmpty } from '../../validator/Validator';
import {
  applicationDoc, downloadOneDrive, downloadDoc, customerAppUploadDocument
} from 'Actions';
class AllDocumentsApplication extends Component {

  state = {
    all: false,
    users: null, // initial user data
    opnDocFileModal: false,
    imgPath: '',
    imgPlanNumber: '',
    imgItemId: '',
    addData: {
      document: ''
    },
    add_err: {},
    inputKey: '',
  }


  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    //this.permissionFilter(this.state.currentModule);
    //this.props.applicationDoc(this.props.match.params.id);
  }

  opnDocFileModal(item_id, plan_number) {
    this.props.downloadOneDrive(item_id)
    this.setState({ opnDocFileModal: true, imgPlanNumber: plan_number, imgItemId: item_id })
  }

  opnViewDocFileModalClose = () => {
    this.setState({ opnDocFileModal: false })
  }

  downloadDoc() {
    this.props.downloadDoc(this.state.imgItemId)
    //return false;
  }

  onChnage(key, value) {
    let { add_err } = this.state;
    switch (key) {
      case 'document':
        value = value.target.files[0];
        
        if (isObjectEmpty(value)) {
          add_err[key] = "Please upload document before submitting.";
        } else {
          add_err[key] = '';
        }
        break;
    }
    this.setState({
      addData: {
        ...this.state.addData,
        [key]: value
      }
    });
    this.setState({ add_err: add_err });
  }

  onClear() {
    let addData = {
      document: ''
    }

    let add_err = {
      document: 'Select file'
    }
    this.setState({ addData: addData, add_err: add_err, inputKey: Math.random().toString(36) })
  }

  callAction(type, e) {
    let datafrom = new FormData();
    datafrom.append('app_id', this.props.match.params.id);
    datafrom.append('imgedata', this.state.addData.document);

    this.props.customerAppUploadDocument(datafrom);

    let addData = {
      document: ''
    }

    let add_err = {
      document: 'Select file'
    }
    this.setState({ addData: addData, add_err: add_err, inputKey: Math.random().toString(36) })
    //onClear()
  }

  validateSubmit() {
    return (this.state.add_err.document === '');
  }

  render() {
    const { loading, allDocuments } = this.props;
    return (
      <div className="all-documents">
        <Helmet>
          <title>Health Partner | Admin | Credit Application | All Documents</title>
          <meta name="description" content="All Documents" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.allDocuments" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          <div className="table-responsive">
            <div className="modal-body page-form-outer view-section">

              <div className="row">

                <div className="col-md-4">
                  <FormGroup>
                    <Label for="document">Upload Document</Label>
                    <Input
                      type="file"
                      name="document"
                      id="document"
                      key={this.state.inputKey || ''}
                      className="p-5"
                      accept="image/gif, image/jpeg, image/png, application/pdf"
                      onChange={(e) => this.onChnage('document', e)}
                    >
                    </Input>

                  </FormGroup>
                </div>

                <div className="col-md-4">
                  <div className="mps-submt-btn">
                    <Button variant="contained" color="primary" className="text-white mr-10" onClick={this.callAction.bind(this)} disabled={!this.validateSubmit()}>Upload</Button>
                    <Button variant="contained" color="secondary" className="text-white" onClick={this.onClear.bind(this)}>Clear</Button>
                  </div>
                </div>

              </div>

              {/*<div className="row">
                {allDocuments && allDocuments.map((img, key) => (
                  <div className="col-sm-6 col-md-4 col-lg-4 col-xl-3" key={key}>
                    <figure className="img-wrapper border border-secondary" onClick={() => this.opnDocFileModal(img.item_id, img.plan_number)}>

                      <img src={`${img.file_path}`} width="250" height="400" />
                      

                      <figcaption>
                        <h4>View and Download</h4>
                      </figcaption>
                      <a href="javascript:void(0);">&nbsp;</a>
                    </figure>
                  </div>
                ))}
                </div>*/}

            </div>
          </div>

          <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

            <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
              <span className="float-left>">{(this.state.imgPlanNumber) ? 'Plan Number: ' + this.state.imgPlanNumber : ''}</span>
              <span className="float-right"><a href="javascript:void(0)" onClick={() => this.downloadDoc()} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span>
            </ModalHeader>

            <ModalBody>
              {this.props.oneDrivePreview &&
                <embed src={this.props.oneDrivePreview} width="100%" height="500" download />

              }
              {this.props.loading &&
                <div className="p-50 text-center">Loading, Please Wait...</div>
              }
              {
                this.props.doc_loading &&
                <RctSectionLoader />
              }

            </ModalBody>

          </Modal>

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication }) => {
  const { loading, allDocuments, oneDrivePreview, doc_loading } = creditApplication;
  return { loading, allDocuments, oneDrivePreview, doc_loading }
}
export default connect(mapStateToProps, {
  applicationDoc, downloadOneDrive, downloadDoc, customerAppUploadDocument
})(AllDocumentsApplication);