/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
   FormGroup,
   Form,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';

// edit address from
import EditAddressForm from './EditAddressForm';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const Address = ({ customerDetails }) => (
   <div className="address-wrapper d-flex">
      <div className="modal-body page-form-outer view-section">
         {customerDetails.addresses.map((location, idx) => (
            <div className="view-section-inner p-location mb-20" key={idx}>

               <div className="view-box mb-10 ">

                  <h2>Physical Address</h2>
                  <div className="width-100">
                     <table>
                        <tbody>
                           <tr>
                              <td className="fw-bold">Address1 :</td>
                              <td>{(location.line1) ? location.line1 : '-'}</td>
                           </tr>
                           <tr>                             
                              <td className="fw-bold">Address2 :</td>
                              <td>{(location.line2) ? location.line2 : '-'}</td>
                           </tr>
                           <tr>
                              <td className="fw-bold">City :</td>
                              <td>{(location.city) ? location.city : '-'}</td>
                           </tr>
                           <tr>
                              <td className="fw-bold">State :</td>
                              <td>{(location.state) ? location.state : '-'}</td>
                           </tr>
                           <tr>                             
                              <td className="fw-bold">Country :</td>
                              <td>{(location.country_name) ? location.country_name : '-'}</td>
                           </tr>
                           <tr>
                              <td className="fw-bold">Zip Code :</td>
                              <td>{(location.zip) ? location.zip : '-'}</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>

               </div>


            </div>
         ))}
      </div>
   </div>
);
export default Address;
