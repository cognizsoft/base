/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import {
   planDetails, clearRedirectURL
} from 'Actions';
class CustomerBilling extends Component {

   state = {
      
   }

   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      //console.log(this.props.match.params.id)
      this.props.clearRedirectURL();
      this.props.planDetails(this.props.match.params.id);
   }

   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails } = this.props;

      
      //last payment rcvd
      var lastPymntRcvd = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).pop() : '-';
      
     //next pymnt due date
      var nxtPymntDueDate = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 0 ) : '';

      //payment missed count
      var countPymntMis = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.missed_flag == 1 ) : '';
      
      //last payment rcvd
      var pastPymntDue = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).reverse() : '-';
      
      //late fee amount
      var lateFeeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.late_flag == 1 ) : '';

      if(lateFeeAmt) {
         var lateFeeTotal = 0;
         for (var i = 0; i<lateFeeAmt.length; i++) {
           lateFeeTotal += parseFloat(lateFeeAmt[i].late_fee_received);
         }
      }

      //fin charge amount
      var finChargeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.fin_charge_amt !== 0 && x.fin_charge_amt !== null ) : '';
      //console.log(finChargeAmt);
      if(finChargeAmt) {
         var finChargeTotal = 0;
         for(var i=0; i<finChargeAmt.length; i++) {
            finChargeTotal += parseFloat(finChargeAmt[i].fin_charge_amt);
         }
      }

      //current balance
      var getPaidBalnc = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ) : '';
      if(getPaidBalnc) {
         var paidInsTotal = 0;
         for (var i = 0; i<getPaidBalnc.length; i++) {
           paidInsTotal += parseFloat(getPaidBalnc[i].installment_amt);
         }
      }
      
      //balnce for table row
      var crdtBalnce = (mainPlan) ? mainPlan[0].credit_amount : 0;
      var blnc = (mainPlan) ? mainPlan[0].credit_amount : 0;

      //total additional amount
      var addiAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.additional_amount !== 0 && x.additional_amount !== null ) : '';
      if(addiAmt) {
         var addiAmtTotal = 0;
         for(var i=0; i<addiAmt.length; i++) {
            addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
         }
      }
      
      var appId = (mainPlan) ? mainPlan[0].application_id : '';

      var countPaymentDetailRow = (paymentPlanDetails) ? paymentPlanDetails.length : 0;
      //console.log(countPaymentDetailRow);
      //check payment is due
      //console.log(appId);
      //var app
      //console.log(nxtPymntDueDat.due_date)
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">
            
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                           </li>
                        </ul>
                     </div>
                     <div className="p-10" ref={el => (this.componentRef = el)}>
                        <h1 className="text-center mb-20">Customer Account</h1>
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Customer Information</th>
                                 </tr>
                                 <tr>
                                    <td><strong>Account No :</strong></td>
                                    <td>{(mainPlan) ? mainPlan[0].patient_ac : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Name :</strong></td>
                                    <td>{(mainPlan) ? (mainPlan[0].f_name + ' ' + mainPlan[0].m_name + ' ' + mainPlan[0].l_name) : '-'}</td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Address :</strong></td>
                                    <td>{(mainPlan) ? (mainPlan[0].address1 + ', ' + mainPlan[0].address2 + ', ' + mainPlan[0].City + ', ' + mainPlan[0].region_name) : '-'}
                                    
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Phone :</strong></td>
                                    <td>{(mainPlan) ? mainPlan[0].peimary_phone : '-'}</td>
                                 </tr>
                              
                                 </tbody>
                              </table>
                           
                           </div>
                          
                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Loan Information</th>
                                 </tr>
                                 <tr>
                                    <td><strong>Loan Amount :</strong></td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].credit_amount.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Payment Term :</strong></td>
                                    <td>{(mainPlan) ? mainPlan[0].payment_term+' Month' : '-'}</td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Interest Rate(%) :</strong></td>
                                    <td>{(mainPlan) ? mainPlan[0].interest_rate+"%" : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Monthly Payment :</strong></td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Current Balance :</strong></td>
                                    <td>{(mainPlan) ? '$'+(mainPlan[0].credit_amount-paidInsTotal).toFixed(2) : '-'}</td>
                                 </tr>
                                 </tbody>
                              </table>
                           
                           </div>

                            <div className="add-card">
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Payment Information</th>
                                 </tr>
                                 <tr>
                                    <td><strong>Last Payment Received :</strong></td>
                                    <td>
                                       {(lastPymntRcvd !== undefined && lastPymntRcvd.payment_date) ? lastPymntRcvd.payment_date : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Next Payment Due Date :</strong></td>
                                    <td>
                                       {(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Late Fee Payments :</strong></td>
                                    <td>
                                       {(lateFeeAmt) ? lateFeeAmt.length : 0}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Payments Missed :</strong></td>
                                    <td>
                                       {(countPymntMis.length) ? countPymntMis.length : 0}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Late Fees :</strong></td>
                                    <td>
                                       {(lateFeeTotal) ? '$'+lateFeeTotal.toFixed(2) : 0}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Financial Charges :</strong></td>
                                    <td>
                                       {
                                          
                                          (finChargeTotal) ? '$'+finChargeTotal.toFixed(2) : 0
                                       }
                                    </td>
                                 </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div className="table-responsive mb-40 pymt-history">
                        <h2 className="text-center mb-10">Payment History</h2>
                           <table className="table table-borderless provider-application-payment-plan">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Payment Date</th>
                                    <th>Due Date</th>
                                    <th>Amount Paid</th>
                                    <th>Additional Amt.</th>
                                    <th>EMI</th>
                                    <th>Late Fee</th>
                                    <th>Financial Charge</th>
                                    <th>Total Paid</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    
                                 </tr>
                              </thead>
                              <tbody>
                                 {paymentPlanDetails && paymentPlanDetails.map(function(plan, idx) {
                                    return <tr 
                                       key={idx} 
                                       className={new Date(plan.comparison_date)<new Date() && plan.paid_flag == 0 ? "late-fee-payment" : ""}
                                       >
                                       <td>{idx + 1}</td>
                                       <td>
                                          {(plan.payment_date != "00/00/0000" && plan.paid_flag == 1) ? plan.payment_date : '-'}
                                       </td>
                                       <td>
                                          {plan.due_date}
                                       </td>
                                       <td>
                                          {(plan.payment_amount) ? '$'+(plan.payment_amount+plan.additional_amount).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {
                                             (plan.additional_amount) ? '$'+plan.additional_amount.toFixed(2) : '-'
                                          }
                                       </td>
                                       <td>
                                          {
                                             (plan.installment_amt) ? '$'+plan.installment_amt.toFixed(2) : '$0.00'
                                          }
                                       </td>
                                       <td>
                                          {(plan.late_fee_received) ? '$'+plan.late_fee_received.toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(plan.fin_charge_amt) ? '$'+plan.fin_charge_amt.toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {(plan.payment_amount) ? (plan.payment_amount + plan.late_fee_received + plan.additional_amount).toFixed(2) : '-'}
                                       </td>
                                       <td>
                                          {
                                             (plan.paid_flag == 1) ? '$' : ''  
                                          }
                                          {
                                             (plan.paid_flag == 1) ?
                                                blnc = (blnc - (plan.payment_amount+plan.additional_amount)).toFixed(2)
                                             : '-'
                                          }
                                       </td>
                                       <td>{(plan.paid_flag == 1) ? 'Paid' : ((plan.paid_flag == 2) ? 'In progress' : 'Unpaid')}</td>
                                       
                                    </tr>
                                 })}
                              </tbody>
                           </table>
                        </div>
                        {/**<div className="note-wrapper row">
                           <div className="invoice-note col-sm-12 col-md-8">
                              <h2 className="invoice-title">Note</h2>
                              <p className="fs-14">Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.</p>
                           </div>

                        </div>**/}
                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication }) => {
   console.log(creditApplication);
   const { nameExist, isEdit } = authUser;
   const { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL } = creditApplication;
   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL
})(CustomerBilling);