/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepThirdCredit = ({ addErr, addData, onChnagerovider, DatePicker, employed_since, employmentType }) => (
   
      <div className="modal-body page-form-outer text-left">
         <Form>
            <div className="row">

               <div className="col-md-12">
                  <FormGroup tag="fieldset">
                     <Label>Employed</Label>
                     <FormGroup check>
                        <Label check>
                           <Input
                              type="radio"
                              name="employed"
                              value={1}
                              checked={(addData.employed == 1) ? true : false}
                              onChange={(e) => onChnagerovider('employed', e.target.value)}
                           />
                           Yes
                                                 </Label>
                     </FormGroup>
                     <FormGroup check>
                        <Label check>
                           <Input
                              type="radio"
                              name="employed"
                              value={0}
                              checked={(addData.employed == 0) ? true : false}
                              onChange={(e) => onChnagerovider('employed', e.target.value)}
                           />
                           No
                                                 </Label>
                     </FormGroup>
                  </FormGroup>
               </div>
            </div>
            <div className={(addData.employed == 1) ? "row" : "d-none row"}>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="employment_type">Employment Type<span className="required-field">*</span></Label>
                     <Input
                        type="select"
                        name="employment_type"
                        id="employment_type"
                        placeholder=""
                        value={addData.employment_type}
                        onChange={(e) => onChnagerovider('employment_type', e.target.value)}
                     >
                        <option value="">Select</option>
                        {employmentType && employmentType.map((emp, key) => (
                            <option value={emp.mdv_id} key={key}>{emp.value}</option>
                        ))}

                     </Input>
                     {(addErr.employment_type != '' && addErr.employment_type !== undefined) ? <FormHelperText>{addErr.employment_type}</FormHelperText> : ''}
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="employer_name">Employer Name<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="employer_name"
                        id="employer_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Employer Name"
                        value={(addData.employer_name != '') ? addData.employer_name : ''}
                        error={(addErr.employer_name) ? true : false}
                        helperText={(addErr.employer_name != '') ? addErr.employer_name : ''}
                        onChange={(e) => onChnagerovider('employer_name', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="employer_email">Employer Email Address</Label><br />
                     <TextField
                        type="text"
                        name="employer_email"
                        id="employer_email"
                        fullWidth
                        variant="outlined"
                        placeholder="Employer Email Address"
                        value={(addData.employer_email != '') ? addData.employer_email : ''}
                        error={(addErr.employer_email) ? true : false}
                        helperText={(addErr.employer_email != '') ? addErr.employer_email : ''}
                        onChange={(e) => onChnagerovider('employer_email', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="employer_phone">Employer Phone No<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="employer_phone"
                        id="employer_phone"
                        fullWidth
                        variant="outlined"
                        placeholder="Employer Phone No"
                        inputProps={{ maxLength: 14 }}
                        value={(addData.employer_phone != '') ? addData.employer_phone : ''}
                        error={(addErr.employer_phone) ? true : false}
                        helperText={(addErr.employer_phone != '') ? addErr.employer_phone : ''}
                        onChange={(e) => onChnagerovider('employer_phone', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="employed-since">Employed Since<span className="required-field">*</span></Label>
                     <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="employed_since"
                        id="employed_since"
                        selected={employed_since}
                        placeholderText="MM/DD/YYYY"
                        maxDate={new Date()}
                        autocomplete={false}
                        showMonthDropdown
                        showYearDropdown
                        dropdownMode="select"
                        strictParsingF
                        onChange={(e) => onChnagerovider('employed_since', e)}
                     />
                     {(addErr.employed_since) ? <FormHelperText>{addErr.employed_since}</FormHelperText> : ''}
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="annual_income">Annual Income($)<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="annual_income"
                        id="annual_income"
                        fullWidth
                        variant="outlined"
                        placeholder="($)"
                        value={(addData.annual_income != '') ? addData.annual_income : ''}
                        error={(addErr.annual_income) ? true : false}
                        helperText={(addErr.annual_income != '') ? addErr.annual_income : ''}
                        onChange={(e) => onChnagerovider('annual_income', e.target.value)}
                     />
                  </FormGroup>
               </div>



               

               

               

               



            </div>
         </Form>
      </div>
   
);

export default StepThirdCredit;