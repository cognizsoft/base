/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import { InputAdornment, withStyles } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
const StepFirstCredit = ({ addErr, addData, onChnagerovider, DatePicker, startDate, existCustomer, onChnageExist, validateSearch, startDateExist, existError, searchCustomerSubmit, searchCustomerReset, handleClickShowSsn, showSsn, CoStateType, countriesList }) => (

   <div className="modal-body page-form-outer text-left">

      <div className="row">
         <div className="col-md-4">
            <FormGroup>
               <Label for="first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="first_name"
                  id="first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.first_name}
                  error={(addErr.first_name) ? true : false}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  helperText={addErr.first_name}
                  onChange={(e) => onChnagerovider('first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="middle_name"
                  id="middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.middle_name}
                  onChange={(e) => onChnagerovider('middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="last_name"
                  id="last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.last_name}
                  error={(addErr.last_name) ? true : false}
                  helperText={addErr.last_name}
                  onChange={(e) => onChnagerovider('last_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-12">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="alias_name"
                        value={1}
                        checked={(addData.alias_name == 1) ? true : false}
                        onChange={(e) => onChnagerovider('alias_name', e.target.value)}
                     />
                     Alias Name
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <div className={(addData.alias_name == 0) ? "row d-none" : "row"}>
         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_first_name">Alias First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_first_name"
                  id="alias_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.alias_first_name}
                  error={(addErr.alias_first_name) ? true : false}
                  helperText={addErr.alias_first_name}
                  onChange={(e) => onChnagerovider('alias_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_middle_name">Alias Middle Name</Label><br />
               <TextField
                  type="text"
                  name="alias_middle_name"
                  id="alias_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.alias_middle_name}
                  onChange={(e) => onChnagerovider('alias_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_last_name">Alias Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_last_name"
                  id="alias_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.alias_last_name}
                  error={(addErr.alias_last_name) ? true : false}
                  helperText={addErr.alias_last_name}
                  onChange={(e) => onChnagerovider('alias_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      <div className="row">

         <div className="col-md-4">
            <FormGroup>
               <Label for="dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="dob"
                  id="dob"
                  selected={startDate}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  maxDate={new Date()}
                  strictParsing
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  onChange={(e) => onChnagerovider('dob', e)}
               />
               {(addErr.dob) ? <FormHelperText className="jss116">{addErr.dob}</FormHelperText> : ''}
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup className="ssn-mask">
               <Label for="ssn">SSN / Tax ID<span className="required-field">*</span></Label><br />
               <TextField
                  type={(!showSsn) ? "password" : "text"}
                  name="ssn"
                  id="ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  InputProps={{
                     endAdornment: (
                        <InputAdornment position="end">
                           <IconButton
                              className="eye-mask"
                              onClick={handleClickShowSsn}
                           >
                              {!showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                           </IconButton>
                        </InputAdornment>
                     ),
                  }}
                  placeholder="Social Security Number"
                  value={addData.ssn}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  error={(addErr.ssn) ? true : false}
                  helperText={addErr.ssn}
                  onChange={(e) => onChnagerovider('ssn', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      <div className="row">
         <div className="col-md-3">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="freeze_override"
                        value={1}
                        checked={(addData.freeze_override == 1) ? true : false}
                        onChange={(e) => onChnagerovider('freeze_override', e.target.value)}
                     />
                     Is your credit history locked?
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(addData.freeze_override == 0) ? "col-md-9 d-none" : "col-md-9 text-danger"}>
            {/*<FormGroup tag="fieldset">
               <FormGroup>
                  <Label for="amount">Freeze Code<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="freeze_code"
                     id="freeze_code"
                     fullWidth
                     variant="outlined"
                     placeholder="Freeze Code"
                     value={(addData.freeze_code != '') ? addData.freeze_code : ''}
                     error={(addErr.freeze_code) ? true : false}
                     helperText={(addErr.freeze_code != '') ? addErr.freeze_code : ''}
                     onChange={(e) => onChnagerovider('freeze_code', e.target.value)}
                  />
               </FormGroup>
</FormGroup>*/}
            Your application can't be processed because of the lock on your credit history. Please contact your credit agency to unlock your credit history.
         </div>
      </div>
      <div className="row">
         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Gender</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'M'}
                        checked={(addData.gender == 'M') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Male
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'F'}
                        checked={(addData.gender == 'F') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Female
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>


         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Status</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addData.status == 1) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Active
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={0}
                        checked={(addData.status == 0) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Inactive
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <div className="row">
         <div className="col-md-12">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="co_signer"
                        value={1}
                        checked={(addData.co_signer == 1) ? true : false}
                        onChange={(e) => onChnagerovider('co_signer', e.target.value)}
                     />
                     Co-signer
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <div className={(addData.co_signer == 0) ? "row d-none" : "row"}>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_first_name"
                  id="co_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.co_first_name}
                  error={(addErr.co_first_name) ? true : false}
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  helperText={addErr.co_first_name}
                  onChange={(e) => onChnagerovider('co_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="co_middle_name"
                  id="co_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.co_middle_name}
                  onChange={(e) => onChnagerovider('co_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_last_name"
                  id="co_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={addData.co_last_name}
                  error={(addErr.co_last_name) ? true : false}
                  helperText={addErr.co_last_name}
                  onChange={(e) => onChnagerovider('co_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_email">Email Address<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_email"
                  id="co_email"
                  fullWidth
                  variant="outlined"
                  placeholder="Email Address"
                  disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                  value={(addData.co_email != '') ? addData.co_email : ''}
                  error={(addErr.co_email) ? true : false}
                  helperText={(addErr.co_email != '') ? addErr.co_email : ''}
                  onChange={(e) => onChnagerovider('co_email', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_phone">Phone No.<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_phone"
                  id="co_phone"
                  fullWidth
                  variant="outlined"
                  placeholder="Phone No."
                  inputProps={{ maxLength: 14 }}
                  value={(addData.co_phone != '') ? addData.co_phone : ''}
                  error={(addErr.co_phone) ? true : false}
                  helperText={(addErr.co_phone != '') ? addErr.co_phone : ''}
                  onChange={(e) => onChnagerovider('co_phone', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_relationship">Relationship<span className="required-field">*</span></Label>
               <Input
                  type="select"
                  name="co_relationship"
                  id="co_relationship"
                  placeholder=""
                  value={addData.co_relationship}
                  onChange={(e) => onChnagerovider('co_relationship', e.target.value)}
               >
                  <option value="">Select</option>
                  <option value="1">Father</option>
                  <option value="2">Mother</option>
                  <option value="2">Grandmother</option>
                  <option value="2">Grandfather</option>
               </Input>
               {(addErr.co_relationship != '' && addErr.co_relationship !== undefined) ? <FormHelperText>{addErr.co_relationship}</FormHelperText> : ''}
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_address1">Address1<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_address1"
                  id="co_address1"
                  fullWidth
                  variant="outlined"
                  placeholder="Address1"
                  value={(addData.co_address1 != '') ? addData.co_address1 : ''}
                  error={(addErr.co_address1) ? true : false}
                  helperText={(addErr.co_address1 != '') ? addErr.co_address1 : ''}
                  onChange={(e) => onChnagerovider('co_address1', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_address2">Address2</Label><br />
               <TextField
                  type="text"
                  name="co_address2"
                  id="co_address2"
                  fullWidth
                  variant="outlined"
                  placeholder="Address2"
                  value={(addData.co_address2 != '') ? addData.co_address2 : ''}
                  onChange={(e) => onChnagerovider('co_address2', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_city">City<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_city"
                  id="co_city"
                  fullWidth
                  variant="outlined"
                  placeholder="City"
                  value={(addData.co_city != '') ? addData.co_city : ''}
                  error={(addErr.co_city) ? true : false}
                  helperText={(addErr.co_city != '') ? addErr.co_city : ''}
                  onChange={(e) => onChnagerovider('co_city', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_state">State<span className="required-field">*</span></Label>
               <Input
                  type="select"
                  name="co_state"
                  id="co_state"
                  placeholder=""
                  value={addData.co_state}
                  onChange={(e) => onChnagerovider('co_state', e.target.value)}
               >
                  <option value="">Select</option>
                  {CoStateType && CoStateType.map((state, key) => (
                     <option value={state.state_id} key={key}>{state.name}</option>
                  ))}

               </Input>
               {(addErr.co_state != '' && addErr.co_state !== undefined) ? <FormHelperText>{addErr.co_state}</FormHelperText> : ''}
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_country">Country<span className="required-field">*</span></Label>
               <Input
                  type="select"
                  name="co_country"
                  id="co_country"
                  value={addData.co_country}
                  onChange={(e) => onChnagerovider('co_country', e.target.value)}
               >

                  {countriesList && countriesList.map((country, key) => (
                     <option value={country.id} key={key}>{country.name}</option>
                  ))}

               </Input>
               {(addErr.co_country != '' && addErr.co_country !== undefined) ? <FormHelperText>{addErr.co_country}</FormHelperText> : ''}
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_zip_code">Zip Code<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_zip_code"
                  id="co_zip_code"
                  fullWidth
                  variant="outlined"
                  placeholder="Zip Code"
                  value={(addData.co_zip_code != '') ? addData.co_zip_code : ''}
                  error={(addErr.co_zip_code) ? true : false}
                  helperText={(addErr.co_zip_code != '') ? addErr.co_zip_code : ''}
                  onChange={(e) => onChnagerovider('co_zip_code', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>


   </div>

);

export default StepFirstCredit;