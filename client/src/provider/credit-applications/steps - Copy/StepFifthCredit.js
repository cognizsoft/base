/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
const StepFifthCredit = ({ addErr, addData, onChnagerovider, checkUsernameExist }) => (

   <div className="modal-body page-form-outer text-left">
      <Form>
         <div className="row">
            <div className="col-md-4">
               <FormGroup>
                  <Label for="username">Username<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="username"
                     id="username"
                     fullWidth
                     variant="outlined"
                     placeholder="Username"
                     value={(addData.username != '') ? addData.username : ''}
                     error={(addErr.username) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={(addErr.username != '') ? addErr.username : ''}
                     onChange={(e) => onChnagerovider('username', e.target.value)}
                     onKeyUp={(e) => checkUsernameExist(e.target.value)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="password">Password<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="password"
                     id="password"
                     fullWidth
                     variant="outlined"
                     placeholder="Password"
                     value={(addData.password != '') ? addData.password : ''}
                     error={(addErr.password) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={(addErr.password != '') ? addErr.password : ''}
                     onChange={(e) => onChnagerovider('password', e.target.value)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="password">Confirm Password<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="confirm_password"
                     id="confirm_password"
                     fullWidth
                     variant="outlined"
                     placeholder="Enter Confirm Password"
                     value={(addData.confirm_password != '') ? addData.confirm_password : ''}
                     error={(addErr.confirm_password) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={addErr.confirm_password}
                     onChange={(e) => onChnagerovider('confirm_password', e.target.value)}
                  />
               </FormGroup>
            </div>
         </div>

         <div className="row">
            <div className="col-md-4">
               <FormGroup>
                  <Label for="email">Primary Email Address<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="email"
                     id="email"
                     fullWidth
                     variant="outlined"
                     placeholder="Email Address"
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     value={(addData.email != '') ? addData.email : ''}
                     error={(addErr.email) ? true : false}
                     helperText={(addErr.email != '') ? addErr.email : ''}
                     onChange={(e) => onChnagerovider('email', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="secondary_email">Secondary Email Address</Label><br />
                  <TextField
                     type="text"
                     name="secondary_email"
                     id="secondary_email"
                     fullWidth
                     variant="outlined"
                     placeholder="Secondary Email Address"
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     value={(addData.secondary_email != '') ? addData.secondary_email : ''}
                     error={(addErr.secondary_email) ? true : false}
                     helperText={(addErr.secondary_email != '') ? addErr.secondary_email : ''}
                     onChange={(e) => onChnagerovider('secondary_email', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="phone_1">Primary Phone No.<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="phone_1"
                     id="phone_1"
                     fullWidth
                     variant="outlined"
                     placeholder="Phone No."
                     inputProps={{ maxLength: 14 }}
                     value={(addData.phone_1 != '') ? addData.phone_1 : ''}
                     error={(addErr.phone_1) ? true : false}
                     helperText={(addErr.phone_1 != '') ? addErr.phone_1 : ''}
                     onChange={(e) => onChnagerovider('phone_1', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="phone_2">Secondary Phone No.</Label><br />
                  <TextField
                     type="text"
                     name="phone_2"
                     id="phone_2"
                     fullWidth
                     variant="outlined"
                     placeholder="Phone No."
                     inputProps={{ maxLength: 14 }}
                     value={(addData.phone_2 != '') ? addData.phone_2 : ''}
                     error={(addErr.phone_2) ? true : false}
                     helperText={(addErr.phone_2 != '') ? addErr.phone_2 : ''}
                     onChange={(e) => onChnagerovider('phone_2', e.target.value)}
                  />
               </FormGroup>
            </div>


         </div>
         <div className="row">
            <div className="col-md-12">
               <b>Note :-</b> You understand that by clicking on the I AGREE button immediately following this notice, you are providing ‘written instructions’ to Health Partner, Inc. under the Fair Credit Reporting Act authorizing Health Partner, Inc. to obtain information from your personal credit report or other information from Experian. You authorize Health Partner, Inc. to obtain such information solely to determine if you are eligible to receive a payment plan from Health Partner, Inc.
            </div>
         </div>

      </Form>
   </div>

);

export default StepFifthCredit;