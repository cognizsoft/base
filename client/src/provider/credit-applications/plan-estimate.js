/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isSSN, isEmpty, isEmail, isDecimals, pointDecimals } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { RctCard } from 'Components/RctCard/index';
import AppConfig from 'Constants/AppConfig';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import {
   estimatePlan, estimatePlanClear, printEstimatePlan,sendEstEmail
} from 'Actions';
class planEstimate extends Component {
   state = {
      procedure_date: '',
      selectedPlan: null,
      loan: {
         loan_amount: "",
         score: "",
         email_address: "",
         loan_type: 1,
         monthly_amount: "",
      },
      loanError: {},
      fullView: false,
      emailPopUp: false,
      customerPlanView: true,
      providerPlanView: false,
   }



   /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.estimatePlanClear();
   }


   /*
   * Title :- onChnagePlanExist
   * Descrpation :- This function use for plan onchnage 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnagePlanExist(key, value) {
      let { loanError } = this.state;
      switch (key) {
         case 'monthly_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Monthly amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Monthly amount not valid";
            } else {
               loanError[key] = "";
            }
            break;
         case 'loan_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Loan amount not valid";
            } else {
               loanError[key] = "";
            }
            break;
         case 'score':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Credit score can't be blank";
            } else {
               loanError[key] = "";
            }
            break;
         case 'email_address':
            if (isEmpty(value)) {
               loanError[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               loanError[key] = "Please enter a valid email address.";
            } else {
               loanError[key] = '';
            }
            break;

      }

      this.setState({
         loan: {
            ...this.state.loan,
            [key]: value
         }
      });
      this.setState({ loanError: loanError });
   }
   /*
   * Title :- validateSearchPlan
   * Descrpation :- This function use for validate data for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   validateSearchPlan() {
      if(this.state.loan.loan_type == 1){
         return (
            this.state.loanError.loan_amount === '' &&
            this.state.loanError.score === ''
         )
      } else {
         return (
            this.state.loanError.loan_amount === '' &&
            this.state.loanError.score === '' &&
            this.state.loanError.monthly_amount === ''
         )
      }
   }
   validateEmailSubmit() {
      return (
         this.state.loanError.email_address === ''
      )
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan according to customer
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   viewPlan() {
      this.props.estimatePlan(this.state.loan.score, this.state.loan.loan_amount, this.state.loan.loan_type, this.state.loan.monthly_amount);
      //console.log(this.state.loan.score+'-'+this.state.loan.loan_amount+'-'+this.state.loan.loan_type+'-'+this.state.loan.monthly_amount)
      //this.props.searchCustomerApplication(this.state.existApplication);
   }
   printPlan() {
      //console.log(this.state.current_application_id)
      //console.log(this.state.loan.loan_amount)
      //console.log(this.state.selectedPlan)
      this.props.printEstimatePlan(this.state.loan.score, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   checkboxAction = (id, e) => {
      this.setState({ selectedPlan: (e.target.checked) ? id : null });
   }
   /*
   * Tile :- fullViewPlan
   * Descrpation :- This function use for full plan view
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Oct 15,2019
   */
   fullViewPlan() {
      this.setState({ fullView: true })
      //this.props.fullViewPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   customerPlan() {
      this.setState({ customerPlanView: true, providerPlanView: false })
   }
   providerPlan() {
      this.setState({ providerPlanView: true, customerPlanView: false })
   }
   goBack() {
      this.setState({ fullView: false })
   }
   emailView() {
      this.setState({ emailPopUp: true })
   }
   popUpClose() {
      this.setState({ emailPopUp: false })
   }
   sendEmail(){
      console.log(this.state.loan)
      this.state.loan.plan_id = this.state.selectedPlan;
      this.props.sendEstEmail(this.state.loan)

      this.setState({ emailPopUp: false })
   }
   render() {
      let { customerPlanView, providerPlanView } = this.state;
      var paymentPlanAllowed = this.props.paymentPlanAllowed;
      var min_term = 0;
      if(paymentPlanAllowed) {
         var mterm = paymentPlanAllowed.map(d => d.term_month);
         min_term = Math.min(...mterm)
      }

      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Plan And Estimate</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.planEstimate" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <div className={(this.state.fullView) ? 'modal-body page-form-outer text-left d-none' : 'modal-body page-form-outer text-left'}>


                  <React.Fragment >


                     <div className="row" >
                        <div className="col-md-3">
                           <FormGroup tag="fieldset">
                              <Label for="Status">Loan Type<span className="required-field">*</span></Label>
                              <FormGroup check>
                                 <Label check>
                                    <Input
                                       type="radio"
                                       name="loan_type"
                                       value={1}
                                       checked={(this.state.loan.loan_type == 1) ? true : false}
                                       onChange={(e) => this.onChnagePlanExist('loan_type', e.target.value)}
                                    />
                                    Fixed term (months)
                                 </Label>
                              </FormGroup>
                              <FormGroup check>
                                 <Label check>
                                    <Input
                                       type="radio"
                                       name="loan_type"
                                       value={0}
                                       checked={(this.state.loan.loan_type == 0) ? true : false}
                                       onChange={(e) => this.onChnagePlanExist('loan_type', e.target.value)}
                                    />
                                    Fixed amount(monthly)
                                 </Label>
                              </FormGroup>
                           </FormGroup>
                        </div>
                        <div className={(this.state.loan.loan_type == 1)?"col-md-3 d-none":"col-md-3"}>
                           <FormGroup>
                              <Label for="procedure_amount">Monthly Amount($)<span className="required-field">*</span></Label><br />
                              <TextField
                                 type="text"
                                 name="monthly_amount"
                                 id="monthly_amount"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="($)"
                                 value={this.state.loan.monthly_amount}
                                 error={(this.state.loanError.monthly_amount) ? true : false}
                                 helperText={(this.state.loanError.monthly_amount != '') ? this.state.loanError.monthly_amount : ''}
                                 onChange={(e) => this.onChnagePlanExist('monthly_amount', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="score">Credit Score<span className="required-field">*</span></Label><br />
                              <TextField
                                 type="text"
                                 name="score"
                                 id="score"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Enter credit score"
                                 value={this.state.loan.score}
                                 error={(this.state.loanError.score) ? true : false}
                                 helperText={this.state.loanError.score}
                                 onChange={(e) => this.onChnagePlanExist('score', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="loan_amount">Enter Loan Amount($)<span className="required-field">*</span></Label><br />
                              <TextField
                                 type="text"
                                 name="loan_amount"
                                 id="loan_amount"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Enter Loan Amount"
                                 value={this.state.loan.loan_amount}
                                 error={(this.state.loanError.loan_amount) ? true : false}
                                 helperText={this.state.loanError.loan_amount}
                                 onChange={(e) => this.onChnagePlanExist('loan_amount', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">

                           <div className="mt-30">
                              <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 View Plan
                              </Button>
                              <Button onClick={this.fullViewPlan.bind(this)} disabled={(this.state.selectedPlan == null) ? true : false} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 view & print
                              </Button>

                           </div>


                        </div>
                     </div>
                  </React.Fragment>

                  {paymentPlanAllowed &&
                        <div className="w-100 price-list user-permission payment-plan-generator p-0 d-table m-0" >
                           
                           <div className="plan-select-tab-container mb-0">
                              <div className="plan-tab row m-0 text-center">
                                 <span className={(customerPlanView) ? 'p-customer col-md-6 selected-tab' : 'p-customer col-md-6'} onClick={this.customerPlan.bind(this)}>Customer Plan</span>
                                 <span className={(providerPlanView) ? 'p-provider col-md-6 selected-tab' : 'p-provider col-md-6'} onClick={this.providerPlan.bind(this)}>Provider Plan</span>
                              </div>
                           </div>

                           <div className={(providerPlanView) ? 'row row-eq-height pt-30 provider-tab-section w-100 m-0' : 'row row-eq-height pt-30 customer-tab-section w-100 m-0'}>

                              {customerPlanView && paymentPlanAllowed.map((plan, idx) => (

                                 <div className={(plan.term_month == min_term) ? 'col-md-4 recommended-plan customer-plans' : 'col-md-4 customer-plans'} key={idx} >
                                    <div className="price-plan plan1 rct-block">
                                       <div className="header pb-0 pt-5">

                                          <h2 className="pricing-title text-center">
                                             <span className="plan_term">{plan.term_month} months Plan</span>{plan.term_month == min_term && <span className="recommanded-text">Recommended</span>}
                                             <label className="check-container">
                                                <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                                <span className="checkmark"></span>
                                             </label>
                                             </h2>

                                          <div className="description-text">

                                             <div className="p-amt">Principal Amount: <span className="">${parseFloat(this.props.estDetails.amount).toFixed(2)}</span></div>

                                             <div className="apr">APR: <span className="">{(plan.interest_rate) ? parseFloat(plan.interest_rate).toFixed(2) : '0.00'}%</span></div>

                                          </div>
                                          </div>
                                       <div className="content ">
                                          <div className="page-form-outer select-plan-detail">
                                             
                                             <div className="price-list table-responsive">
                                                <table className="table table-bordered table-striped table-hover">
                                                   <thead>
                                                      <tr className="table-head">
                                                         <th>Payment#</th>
                                                         <th>Due Date</th>
                                                         <th>Payment Amt</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {
                                                         plan.monthlyPlan.map((month, idm) => (

                                                            <tr key={idm}>
                                                               <td>{idm + 1}</td>
                                                               <td>{month.next_month}</td>
                                                               <td>${month.perMonth}</td>
                                                            </tr>
                                                         ))
                                                      }



                                                   </tbody>
                                                </table>

                                             </div>
                                             {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                          </div>


                                       </div>
                                    </div>
                                 </div>

                              ))}

                              {providerPlanView && paymentPlanAllowed.map((plan, idx) => (

                                 <div className={(plan.term_month == min_term) ? 'col-md-4 recommended-plan customer-plans' : 'col-md-4 customer-plans'} key={idx} >
                                    <div className="price-plan plan1 rct-block">
                                       <div className="header pb-0 pt-5">

                                          <h2 className="pricing-title text-center">
                                             <span className="plan_term">{plan.term_month} months Plan</span>
                                             <label className="check-container">
                                                <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                                <span className="checkmark"></span>
                                             </label>
                                             </h2>

                                          <div className="description-text">

                                             <div className="p-amt">Principal Amount: <span className="">${parseFloat(this.props.estDetails.amount).toFixed(2)}</span></div>

                                             <div className="apr">APR: <span className="">{(plan.interest_rate) ? parseFloat(plan.interest_rate).toFixed(2) : '0.00'}%</span></div>

                                          </div>

                                          <div className="pro_discount">
                                             Provider Discount: <span className="pdis">

                                                {(plan.discount_p) ? '$'+parseFloat(plan.discount_p).toFixed(2) : '$0.00'}
                                                
                                             </span>
                                          </div>

                                       </div>
                                       <div className="content ">
                                          <div className="page-form-outer select-plan-detail">
                                             
                                             <div className="price-list table-responsive">
                                                <table className="table table-bordered table-striped table-hover">
                                                   <thead>
                                                      <tr className="table-head">
                                                         <th>Payment#</th>
                                                         <th>Due Date</th>
                                                         <th>Payment Amt</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {
                                                         plan.monthlyPlan.map((month, idm) => (

                                                            <tr key={idm}>
                                                               <td>{idm + 1}</td>
                                                               <td>{month.next_month}</td>
                                                               <td>${month.perMonth}</td>
                                                            </tr>
                                                         ))
                                                      }



                                                   </tbody>
                                                </table>

                                             </div>
                                             {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                          </div>


                                       </div>
                                    </div>
                                 </div>

                              ))}

                           </div>
                           {this.state.generatePlan &&
                              <div className="text-right">
                                 <button disabled={!this.validatePlan()} className="btn btn-primary pt-10 pb-10" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, this.state.selectedPlan)}><span>View Agreement</span></button>
                              </div>
                           }
                        </div>

                     }
               </div>
               <div className={(this.state.fullView) ? 'modal-body page-form-outer text-left' : 'modal-body page-form-outer text-left d-none'} >
                  <div className="row">
                     <div className="col-sm-12 mx-auto">
                        <RctCard>
                           {this.props.paymentPlanAllowed && this.state.selectedPlan !== null &&


                              <div className="p-50" ref={el => (this.componentRef = el)}>
                                 {(() => {
                                    var filterType = this.props.paymentPlanAllowed.filter(x => x.plan_id == this.state.selectedPlan);
                                    return <React.Fragment>
                                       <h1 className="text-center mb-20">
                                          <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                                       </h1>
                                       <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                                          <div className="add-card estimate-add-card">

                                             <table>
                                                <tbody>
                                                   <tr>
                                                      <th colSpan="4">Basic Information</th>
                                                   </tr>
                                                   <tr>
                                                      <td><strong>Credit Score:</strong> {this.props.estDetails.score}</td>
                                                      <td><strong>Loan Amount:</strong> ${parseFloat(this.props.estDetails.amount).toFixed(2)}</td>
                                                      <td><strong>Provider Name:</strong> {this.props.appProviderDetails.name}</td>
                                                      <td><strong>Provider Discount:</strong> {(filterType) ? '$'+parseFloat(filterType[0].discount_p).toFixed(2) : '-'}</td>
                                                   </tr>
                                                </tbody>
                                             </table>

                                          </div>

                                       </div>
                                       <div className="table-responsive mb-40 pymt-history">
                                          <h2 className="text-center mb-10">Plan Details</h2>
                                          <div className="table-responsive">
                                             <table className="table table-borderless">
                                                <thead>
                                                   <tr>
                                                      <th>SN#</th>
                                                      <th>Date</th>
                                                      <th>Amount</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   {filterType[0].monthlyPlan.map((plan, idx) => (
                                                      <tr key={idx}>
                                                         <td>{idx + 1}</td>
                                                         <td>{plan.next_month}</td>
                                                         <td>${parseFloat(plan.perMonth).toFixed(2)}</td>
                                                      </tr>
                                                   ))}
                                                </tbody>
                                             </table>

                                          </div>
                                       </div>

                                    </React.Fragment>
                                 })()}
                              </div>
                           }
                           <div className="invoice-head">
                              <div className="row">
                                 <div className="col-sm-12 text-right">
                                    <ul className="list-inline">
                                       <li><a href="javascript:void(0);" onClick={this.goBack.bind(this)}><i className="mr-10 ti-back-left"></i> Back</a></li>
                                       <li>
                                          <ReactToPrint
                                             trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                             content={() => this.componentRef}
                                          />
                                       </li>

                                       <li><a onClick={this.printPlan.bind(this)} href="javascript:void(0);" ><i className="mr-10 ti-import"></i> Export PDF</a></li>
                                       <li><a onClick={this.emailView.bind(this)} href="javascript:void(0);" ><i className="mr-10 ti-email"></i> Email</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </RctCard>
                     </div>
                  </div>
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <Modal isOpen={this.state.emailPopUp} toggle={() => this.popUpClose()}>
               <ModalHeader toggle={() => this.popUpClose()}>
                  Email
               </ModalHeader>
               <ModalBody>
                  <div className="col-md-12">
                     <FormGroup>
                        <Label for="email_address">Enter email<span className="required-field">*</span></Label><br />
                        <TextField
                           type="text"
                           name="email_address"
                           id="email_address"
                           fullWidth
                           variant="outlined"
                           placeholder="Enter Email Address"
                           value={this.state.loan.email_address}
                           error={(this.state.loanError.email_address) ? true : false}
                           helperText={this.state.loanError.email_address}
                           onChange={(e) => this.onChnagePlanExist('email_address', e.target.value)}
                        />
                     </FormGroup>
                  </div>
               </ModalBody>
               <ModalFooter>
                  <Button
                     variant="contained"
                     color="primary"
                     disabled={!this.validateEmailSubmit()}
                     onClick={() => this.sendEmail()}
                  >Send</Button>

                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.popUpClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, paymentPlanAllowed, estDetails, appProviderDetails } = creditApplication;
   return { loading, paymentPlanAllowed, estDetails, appProviderDetails }



}

export default connect(mapStateToProps, {
   estimatePlan, estimatePlanClear, printEstimatePlan,sendEstEmail
})(planEstimate); 