/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { URL } from '../../apifile/URL';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isEmpty, formatPhoneNumber, isPhone } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import queryString from 'query-string';

import Profile from './component/Profile';
import Address from './component/Address';
import {
   searchThirdParty, viewCareCouldPatient, MFSDetails, requestToken
} from 'Actions';
function TabContainer(props) {
   return (
      <Typography component="div" style={{ padding: 8 * 3 }}>
         {props.children}
      </Typography>
   );
}
class searchApplication extends Component {
   state = {
      searchApplication: {
         exist_last_name: "",
      },
      existError: {},
      viewPatientModal: false,
      activeTab: 0,
      applicationToken: false,
      resetSearch:false,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      this.props.MFSDetails();
      var tokenDetails = JSON.parse(localStorage.getItem('applicationToken'));
      //console.log(tokenDetails)
      if (tokenDetails != null) {
         if (tokenDetails.expire_at > Math.floor(new Date().getTime() / 1000)) {
            this.setState({ applicationToken: true });
         } else {
            this.setState({ applicationToken: false });
         }
      } else if (this.props.location.search != '') {
         this.props.requestToken(queryString.parse(this.props.location.search).code, window.location.origin.toString() + '/provider/search-customer', '')
         this.setState({ applicationToken: true });
      } else {
         this.setState({ applicationToken: false });
      }
      //this.props.applicationOption();
      //this.props.planReset(1);
      /*
      if(this.props.location.search != '' && this.state.careCould === false){
         var tokenDetails = JSON.parse(localStorage.getItem('applicationToken'));
         if(tokenDetails !== null && tokenDetails.expire_at < Math.floor(new Date().getTime() / 1000) && tokenDetails.refresh_token !== undefined){
            this.props.requestToken(queryString.parse(this.props.location.search).code,window.location.origin.toString()+'/provider/credit-applications',tokenDetails.refresh_token)
            this.setState({careCould:true})
         }else if(tokenDetails.expire_at > Math.floor(new Date().getTime() / 1000)){
            this.setState({careCould:true})
         }else{
            this.props.requestToken(queryString.parse(this.props.location.search).code,window.location.origin.toString()+'/provider/credit-applications','')
            this.setState({careCould:true})
         }
         //console.log(tokenDetails.expires_in*1000)
         //this.setState({careCould:true})
      }
      */
   }
   /*
   * Title :- onChnageExist
   * Descrpation :- This function use for onchnage
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnageExist(key, value) {
      let { existError } = this.state;
      switch (key) {
         case 'exist_last_name':
            if (isEmpty(value)) {
               //existError[key] = "Last Name can't be blank";
            } else {
               existError[key] = '';
            }
            break;

      }

      this.setState({
         searchApplication: {
            ...this.state.searchApplication,
            [key]: value
         }
      });
      this.setState({ existError: existError });
   }
   /*
   * Title :- searchCustomer
   * Descrpation :- This function use for search custoemr details
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerSubmit() {
      //console.log(this.state.searchApplication)
      this.props.searchThirdParty(this.state.searchApplication);
      this.setState({resetSearch:true})
   }
   /*
   * Title :- searchCustomerReset
   * Descrpation :- This function use for reset page
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerReset() {
      this.setState({
         resetSearch:false,
         searchApplication: {
            exist_last_name: "",
         },
         existError: {},
      }, () => {
         //this.props.removeSearchCustomer();
      })
   }
   /*
   * Title :- validateSearch
   * Descrpation :- This function use for validate data
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   validateSearch() {
      return (
         this.state.existError.exist_last_name === '' /*&&
         this.state.existError.exist_last_name === '' &&
         this.state.existError.exist_dob === '' &&
         this.state.existError.phone_no === '' &&
         this.state.existError.gender === ''*/
      )
   }


   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check state updated or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentWillReceiveProps(nextProps) {
      //(nextProps.rediretPlanURL != '') ? this.setState({ changeURL: nextProps.rediretPlanURL }) : '';
   }
   viewPatient(id, e) {
      this.setState({ viewPatientModal: true })
      this.props.viewCareCouldPatient(id);
   }
   closePatientModal() {

      this.setState({ viewPatientModal: false })
   }
   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {
         MuiTypography: {
            root: {
               padding: "0 !important"
            },
         },
      }
   })
   goBack(){
      this.props.history.goBack(-1)
   }
   render() {
      if (this.state.changeURL === 1) {
         return (<Redirect to={`/provider/customers/agreement-plan/${this.props.paymentPlanID}`} />);
      }
      
      
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.searchCustomer" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               {this.props.careCould && !this.state.applicationToken &&
                  <div className="modal-body page-form-outer text-left">
                     <a href={'https://platform.carecloud.com/oauth2/authorize?client_id=' + this.props.careCould + '&response_type=code&redirect_uri=' + window.location.origin.toString() +''+URL.SUB_PATH+ 'provider/search-customer'} color="primary" className="btn btn-primary">Sign in to CareCloud</a>
                     <Link to="#" onClick={this.goBack.bind(this)} color="primary" className="btn btn-primary ml-5">Cancel</Link>
                  </div>
               }
               {this.state.applicationToken &&
                  <div className="modal-body page-form-outer text-left">
                     <div className="row">
                        <div className="col-md-12 text-danger">
                           <span>Note:- If you receive an <strong>Invalid Grant</strong> related error then please try after some time.</span>
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="exist_last_name">Last Name</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_last_name"
                                 id="exist_last_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Last Name"
                                 value={this.state.searchApplication.exist_last_name}
                                 error={(this.state.existError.exist_last_name) ? true : false}
                                 helperText={this.state.existError.exist_last_name}
                                 onChange={(e) => this.onChnageExist('exist_last_name', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <div className="mt-30">
                              <Button onClick={this.searchCustomerSubmit.bind(this)} disabled={!this.validateSearch()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 Search
                              </Button>
                              <Button onClick={this.searchCustomerReset.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                 Reset
                              </Button>
                           </div>
                        </div>
                     </div>
                     {this.props.careCouldSearch && this.state.resetSearch &&
                        <div className="table-responsive mb-40 pymt-history">
                           <h2 className="text-center mb-10">Customer Detail</h2>
                           <div className="table-responsive">
                              <table className="table table-borderless admin-account-view-invoice">
                                 <thead>
                                    <tr>
                                       <th>First Name</th>
                                       <th>Last Name</th>
                                       <th>Gender</th>
                                       <th>DOB</th>
                                       <th>Phone</th>
                                       <th>Email</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {this.props.careCouldSearch.map((patient, idx) => (
                                       <tr key={idx}>
                                          <td>{patient.first_name}</td>
                                          <td>{patient.last_name}</td>
                                          <td>{patient.gender}</td>
                                          <td>{moment(patient.dob).format('MM/DD/YYYY')}</td>
                                          <td>{(patient.phones.length > 0) ? patient.phones[0].phone_number : '-'}</td>
                                          <td>{patient.email}</td>
                                          <td>
                                             <span className="list-action">
                                                <a href="javascript:void(0)" onClick={this.viewPatient.bind(this, patient.id)} title="View Complete Information"><i className="ti-eye"></i></a>
                                                <Link to={`/provider/credit-application/add-new?id=${patient.id}`} title="Process"><i className="ti-control-forward"></i></Link>
                                             </span>
                                          </td>
                                       </tr>
                                    ))}
                                 </tbody>
                              </table>

                           </div>
                        </div>
                     }
                  </div>
               }
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <Modal isOpen={this.state.viewPatientModal} toggle={() => this.closePatientModal()}>
               <ModalHeader toggle={() => this.closePatientModal()}>
                  View Customer Details
               </ModalHeader>
               <ModalBody>
                  <div className="rct-tabs">
                     <AppBar position="static">
                        <Tabs
                           value={this.state.activeTab}
                           onChange={this.handleChange}
                           variant="scrollable"
                           scrollButtons="off"
                           indicatorColor="primary"
                        >
                           <Tab
                              icon={<i className="ti-user"></i>}
                              label={<IntlMessages id="components.myProfile" />}
                           />
                           <Tab
                              icon={<i className="ti-home"></i>}
                              label={<IntlMessages id="components.address-details" />}
                           />
                        </Tabs>
                     </AppBar>
                     <MuiThemeProvider theme={this.getMuiTheme()}>
                        {this.state.activeTab === 0 &&
                           <TabContainer>
                              <Profile
                                 customerDetails={this.props.careCouldView}
                              />
                           </TabContainer>}
                        {this.state.activeTab === 1 &&
                           <TabContainer className="dfdfdffd">
                              <Address className="fdfdffdffdsfdf"
                                 customerDetails={this.props.careCouldView}
                              />
                           </TabContainer>}
                     </MuiThemeProvider>
                  </div>
               </ModalBody>
               <ModalFooter>
               </ModalFooter>
            </Modal>

         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, CareCouldReducer }) => {
   const { user } = authUser;
   const { loading, careCould, careCouldToken, careCouldSearch, careCouldView } = CareCouldReducer;
   return { loading, careCould, careCouldToken, careCouldSearch, careCouldView }



}

export default connect(mapStateToProps, {
   searchThirdParty, viewCareCouldPatient, MFSDetails, requestToken
})(searchApplication); 