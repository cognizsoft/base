/**
 * Invoice
 */
import React, { Component } from 'react';

import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import Button from '@material-ui/core/Button';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import { isEmpty, isDecimals, pointDecimals } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';
import SignatureCanvas from 'react-signature-canvas'
import {
   Form,
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import AppConfig from 'Constants/AppConfig';
import ReactToPrint from 'react-to-print';
import Tooltip from '@material-ui/core/Tooltip';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { currentUserId } from '../../apifile';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';



import {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice, adminPlanDetailsDownload, regeneratePlan, regeneratePlanProcess, regeneratePrintPlan, confirmedCancellation, closeCurrentPlan
} from 'Actions';
class PlanDetails extends Component {
   state = {
      provider_invoice_status: null,
      provider_confirm: null,
      refund_type: null,
      roviderConfirmations: false,
      openViewRefund: false,
      refundDetails: null,
      cancellationNote: '',
      cancellationPlanID: null,
      rePlanModal: false,
      data: {
         loan_amount: '',
         procedure_amount: '',
         procedure_date: '',
         note: '',
         expiry_date: "",
         agreement_date: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         name_of_borrower: "",
         witnessSignature: "",
         customerSignature: "",
         co_name_of_borrower: "",
         co_agreement_date: "",

         co_id_number: "",
         co_id_name: "",
         co_expiry_date: "",
         cosignerSignature: "",
      },
      procedure_date: '',
      dataError: {},
      currentPlanId: null,
      currentPlanNumber: null,
      currentLoanAmount: null,
      agreementView: false,
      expiry_date: '',
      co_expiry_date: '',
      agreement_date: '',
      co_agreement_date: '',
      customerSignURL: null,
      witnessSignURL: null,
      witnessSignAllow: true,
      customerSignAllow: true,
      checkAgreementView: true,
      cancelData: {
         action_type: 0,
         pp_id: null,
         note: '',
      },
      dataCancelError: {},
      cancelModal: false,
      cosignerSigPad: null,
      cosignerSignAllow: true,
      cosignerSignURL: null,
   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   componentDidMount() {
      this.props.clearRedirectURL();
      this.props.allPlanDetails(this.dec(this.props.match.params.appid));
   }
   /*
   * Title :- createInvoce
   * Descrpation :- This function use for create invoice according to due date
   * Date :- Dec 5, 2019
   * Author :- Ramesh Kumar
   */
   createInvoce(date, appid) {
      date = moment(date, 'MM/DD/YYYY', true).format('YYYY-MM-DD');
      this.props.createInvoice(date, appid)
   }

   mainInvoice() {
      this.setState({ singleView: null })
   }

   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   adminPlanDetails() {
      this.props.adminPlanDetailsDownload(this.dec(this.props.match.params.appid));
   }
   /**
    * rePlanPopup
    */
   rePlanPopup(planId, plan_number, loan_amount) {
      let { data, dataError } = this.state;
      dataError['loan_amount'] = "";
      data['loan_amount'] = loan_amount;
      this.setState({ rePlanModal: true, currentPlanId: planId, currentPlanNumber: plan_number, currentLoanAmount: loan_amount, dataError: dataError })
   }
   /*
   * cancel plan
   */
   cancelPopup(planId) {
      let { cancelData, dataError } = this.state;
      cancelData.pp_id = planId;
      this.setState({ cancelModal: true, cancelData: cancelData })
   }
   onChnagePlanCancel(key, value) {
      let { dataCancelError } = this.state;
      switch (key) {
         case 'note':
            if (isEmpty(value)) {
               dataCancelError[key] = "Note can't be blank";
            } else {
               dataCancelError[key] = '';
            }
            break;
      }

      this.setState({
         cancelData: {
            ...this.state.cancelData,
            [key]: value
         }
      });
      this.setState({ dataCancelError: dataCancelError });
   }
   cancelModalClose() {
      let { cancelData, dataError } = this.state;
      cancelData.pp_id = null;
      cancelData.note = null;
      this.setState({ cancelModal: false, cancelData: cancelData, dataError: {} })
   }
   validateCancelPlan() {
      return (
         this.state.dataCancelError.note === ''
      )
   }
   cancelPlanSubmit() {
      const { cancelData } = this.state;

      var promise = new Promise(function (resolve, reject) {
         // call resolve if the method succeeds
         this.props.closeCurrentPlan(cancelData);
         resolve(true);
      }.bind(this))
      promise.then((bool) => {
         this.props.allPlanDetails(this.dec(this.props.match.params.appid));
         this.setState({ cancelModal: false })
      })

   }
   /*
   * Title :- onChnagePlanExist
   * Descrpation :- This function use for plan onchnage 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnagePlanExist(key, value) {
      let { dataError } = this.state;
      switch (key) {
         case 'loan_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               dataError[key] = "Loan amount not valid";
            } else if (value <= 0) {
               dataError[key] = "The loan amount should be greater than $0";
            } else if (parseFloat(this.props.application_details[0].remaining_amount).toFixed(2) < parseFloat(value)) {
               dataError[key] = '"Loan amount exceeds the available credit" for override call HPS at (919) 600-5526';
            } else if (this.state.data.procedure_amount !== '' && parseFloat(this.state.data.procedure_amount) < parseFloat(value)) {
               dataError[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.data.procedure_amount !== '' && parseFloat(this.state.data.procedure_amount) >= parseFloat(value)) {
               dataError[key] = "";
               dataError['procedure_amount'] = "";
            } else {
               dataError[key] = "";
            }
            break;
         case 'procedure_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               dataError[key] = "Procedure amount not valid";
            } else if (value < 100 || value > 250000) {
               dataError[key] = "The Procedure amount amount should be between $100 - $250,000";
            }
            else if (this.state.data.loan_amount !== '' && parseFloat(this.state.data.loan_amount) > parseFloat(value)) {
               dataError[key] = "Procedure amount greater than or equal to Loan amount";
            } else if (this.state.data.loan_amount !== '' && parseFloat(this.state.data.loan_amount) <= parseFloat(value)) {
               dataError[key] = "";
               dataError['loan_amount'] = "";
            } else {
               dataError[key] = '';
            }
            break;
         case 'procedure_date':
            if (value == null) {
               dataError[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = '';
            }
            break;
         case 'note':
            if (isEmpty(value)) {
               dataError[key] = "Note can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'id_number':
            if (isEmpty(value)) {
               dataError[key] = "ID number can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'agree_with':
            value = (this.state.data.agree_with) ? 0 : 1;
            break;
         case 'id_name':
            if (isEmpty(value)) {
               dataError[key] = "ID type can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'expiry_date':
            if (value == null) {
               this.setState({ expiry_date: '' })
            } else {
               this.setState({ expiry_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = '';
            }
            break;
         case 'name_of_borrower':
            if (isEmpty(value)) {
               dataError[key] = "Name of Borrower can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'agreement_date':
            if (value == null) {
               this.setState({ agreement_date: '' })
               dataError[key] = "Agreement date can't be blank";
            } else {
               this.setState({ agreement_date: value })
               value = moment(value).format('YYYY-MM-DD HH:mm:ss');
               dataError[key] = '';
            }
            break;
         case 'co_id_number':
            if (isEmpty(value)) {
               dataError[key] = "ID number can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'co_id_name':
            if (isEmpty(value)) {
               dataError[key] = "ID type can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'co_expiry_date':
            if (value == null) {
               this.setState({ co_expiry_date: '' })
            } else {
               this.setState({ co_expiry_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = '';
            }
            break;
            
         case 'co_name_of_borrower':
            if (isEmpty(value)) {
               dataError[key] = "Cosigner Name of Borrower can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'co_agreement_date':
            if (value == null) {
               this.setState({ co_agreement_date: '' })
               dataError[key] = "Agreement date can't be blank";
            } else {
               this.setState({ co_agreement_date: value })
               value = moment(value).format('YYYY-MM-DD HH:mm:ss');
               dataError[key] = '';
            }
            break;

      }

      this.setState({
         data: {
            ...this.state.data,
            [key]: value
         }
      });
      this.setState({ dataError: dataError });
   }
   validateSearchPlan() {
      return (
         this.state.dataError.loan_amount === '' &&
         this.state.dataError.procedure_amount === '' &&
         this.state.dataError.procedure_date === '' &&
         this.state.dataError.note === ''
      )
   }
   rePlanModalClose() {
      var data = {
         loan_amount: '',
         procedure_amount: '',
         procedure_date: '',
         note: '',
         expiry_date: "",
         agreement_date: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         name_of_borrower: "",
         co_name_of_borrower: "",
         co_id_number: "",
         co_id_name: "",
         co_agreement_date : "",
         witnessSignature: "",
         customerSignature: "",
      }
      this.setState({ rePlanModal: false, currentPlanId: null, currentPlanNumber: null, currentLoanAmount: null, data: data, dataError: {} })
   }
   submitRe() {
      const { data } = this.state;
      data.plan_id = this.state.currentPlanId;
      data.app_id = this.dec(this.props.match.params.appid);

      this.props.regeneratePlan(data)
      this.setState({ checkAgreementView: true, });
   }

   processPrintPlanSubmit() {
      const { data } = this.state;
      data.plan_id = this.state.currentPlanId;
      data.app_id = this.dec(this.props.match.params.appid);
      this.props.regeneratePrintPlan(data)
      this.setState({ checkAgreementView: true, });
   }
   /*
   * Title :- processPlan
   * Descrpation :- This function use for process selected plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   processPlan() {
      const { data } = this.state;
      data.plan_id = this.state.currentPlanId;
      this.props.regeneratePlanProcess(data)
      this.setState({ agreementView: true, rePlanModal: false, });
   }
   customerSigPad = {}
   witnessSigPad = {}
   cosignerSigPad = {}
   clearCustomer = () => {
      this.customerSigPad.clear();
      let { data, dataError } = this.state;
      dataError['customerSignature'] = "Customer Signature required.";
      data['customerSignature'] = "";
      this.customerSigPad.on();
      this.setState({
         customerSignURL: null,
         data: data,
         dataError: dataError,
         customerSignAllow: true,
      })
   }
   trimCustomer = () => {
      let { data, dataError } = this.state;
      dataError['customerSignature'] = "";
      data['customerSignature'] = this.customerSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.customerSigPad.off();

      this.setState({
         customerSignURL: this.customerSigPad.getTrimmedCanvas().toDataURL('image/png'),
         data: data,
         dataError: dataError,
         customerSignAllow: false,
      })
      this.customerSigPad.clear();
   }
   clearWitness = () => {
      this.witnessSigPad.clear();
      let { data, dataError } = this.state;
      dataError['witnessSignature'] = "Witness Signature required.";
      data['witnessSignature'] = "";
      this.witnessSigPad.on();
      this.setState({
         witnessSignURL: null,
         data: data,
         dataError: dataError,
         witnessSignAllow: true,
      })
   }
   trimWitness = () => {
      let { data, dataError } = this.state;
      dataError['witnessSignature'] = "";
      data['witnessSignature'] = this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.witnessSigPad.off();

      this.setState({
         witnessSignURL: this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png'),
         data: data,
         dataError: dataError,
         witnessSignAllow: false,
      })
      this.witnessSigPad.clear();
   }
   clearCosigner = () => {
      this.cosignerSigPad.clear();
      let { data, dataError } = this.state;
      dataError['cosignerSignature'] = "Witness Signature required.";
      data['cosignerSignature'] = "";
      this.cosignerSigPad.on();
      this.setState({
         cosignerSignURL: null,
         data: data,
         dataError: dataError,
         cosignerSignAllow: true,
      })
   }
   trimCosigner = () => {
      let { data, dataError } = this.state;
      dataError['cosignerSignature'] = "";
      data['cosignerSignature'] = this.cosignerSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.cosignerSigPad.off();
      this.setState({
         cosignerSignURL: this.cosignerSigPad.getTrimmedCanvas().toDataURL('image/png'),
         data: data,
         dataError: dataError,
         cosignerSignAllow: false,
      })
      this.cosignerSigPad.clear();
   }
   goBackPlan() {
      var data = {
         loan_amount: '',
         procedure_amount: '',
         procedure_date: '',
         note: '',
         expiry_date: "",
         agreement_date: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         name_of_borrower: "",
         co_name_of_borrower:"",
         co_agreement_date: "",
         co_id_number: "",
         co_id_name: "",
         witnessSignature: "",
         customerSignature: "",
         cosignerSignature: "",
      }
      this.setState({
         agreementView: false,
         data: data,
         procedure_date: '',
         dataError: {},
         currentPlanId: null,
         currentPlanNumber: null,
         currentLoanAmount: null,
         agreementView: false,
         expiry_date: '',
         agreement_date: '',
         customerSignURL: null,
         witnessSignURL: null,
         cosignerSignURL: null,
      })
   }
   validateFinal() {
      if (this.state.agreementView == true && this.props.application_details[0].co_patient_id != null) {
         return (
            this.state.dataError.id_number === '' &&
            this.state.data.agree_with === 1 &&
            this.state.dataError.id_name === '' &&
            this.state.dataError.name_of_borrower === '' &&
            this.state.dataError.agreement_date === '' &&
            this.state.dataError.witnessSignature === '' &&
            this.state.dataError.customerSignature === '' &&
            this.state.dataError.co_id_number === '' &&
            this.state.dataError.co_id_name === '' &&
            this.state.dataError.cosignerSignature === '' &&
            this.state.dataError.co_name_of_borrower === '' &&
            this.state.dataError.co_agreement_date === ''
         )
      } else {
         return (
            this.state.dataError.id_number === '' &&
            this.state.data.agree_with === 1 &&
            this.state.dataError.id_name === '' &&
            this.state.dataError.name_of_borrower === '' &&
            this.state.dataError.agreement_date === '' &&
            this.state.dataError.witnessSignature === '' &&
            this.state.dataError.customerSignature === ''
         )
      }
   }
   validatePrintFinal() {
      return (
         this.state.data.agree_with === 1
      )
   }
   componentWillReceiveProps(nextProps) {
      if (nextProps.generateArea == 1 && this.state.checkAgreementView == true) {
         var data = {
            loan_amount: '',
            procedure_amount: '',
            procedure_date: '',
            note: '',
            expiry_date: "",
            agreement_date: "",
            id_number: "",
            id_name: "",
            agree_with: 0,
            name_of_borrower: "",
            co_name_of_borrower: "",
            co_agreement_date: "",
            co_id_number: "",
            co_id_name: "",
            witnessSignature: "",
            customerSignature: "",
            cosignerSignature: "",
         }
         this.customerSigPad.on();
         this.witnessSigPad.on();
         if (this.props.application_details[0].co_patient_id != null) {
            this.cosignerSigPad.on();
         }

         this.setState({
            agreementView: false,
            checkAgreementView: false,
            currentPlanId: null,
            currentPlanNumber: null,
            currentLoanAmount: null,

            procedure_date: '',
            expiry_date: '',
            co_expiry_date: '',
            agreement_date: '',
            customerSignURL: null,
            cosignerSignURL: null,
            witnessSignURL: null,
            witnessSignAllow: true,
            customerSignAllow: true,
            cosignerSignAllow: true,
            dataError: {},
            data: data
         })

      }
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {

         MuiTooltip: {
            tooltip: {
               fontSize: "15px"
            }
         },
      }
   })

   processPlanConfirmation(note, id, provider_invoice_status) {
      this.setState({ cancellationNote: note, cancellationPlanID: id, roviderConfirmations: true, provider_invoice_status: provider_invoice_status })
      //this.refs.deleteConfirmationDialog.open();
      //this.refs.deleteRadioConfirmationDialog.open();
   }

   processConfirmed() {
      if (this.state.cancellationPlanID != null) {
         this.props.confirmedCancellation(this.state.cancellationPlanID, this.dec(this.props.match.params.appid), this.state.provider_confirm, this.state.refund_type);
      }
      this.setState({ cancellationPlanID: null, refund_type: null, provider_confirm: null, provider_invoice_status: null })
      this.roviderConfirmationsClose()
      //this.refs.deleteRadioConfirmationDialog.close();
      //this.refs.deleteConfirmationDialog.close();
   }
   onChnagePConfim(key, value) {
      switch (key) {
         case 'provider_confirm':
            this.setState({ provider_confirm: parseInt(value) })
            break;
         case 'refund_type':
            this.setState({ refund_type: parseInt(value) })
            break;
      }
   }
   validatePconSubmit() {
      if (this.state.provider_confirm == 1) {
         return ((this.state.provider_invoice_status == 4 && this.state.refund_type === null)) ? false : true;
      } else {
         return (this.state.provider_confirm === null) ? false : true;
      }
   }
   roviderConfirmationsClose() {
      this.setState({ cancellationNote: '', cancellationPlanID: null, roviderConfirmations: false })
   }
   refundDetails(details) {
      this.setState({ openViewRefund: true, refundDetails: details });
   }
   onViewRefundClose() {
      this.setState({ openViewRefund: false, refundDetails: null })
   }

   render() {
      const oneYearFromNow = new Date();
      oneYearFromNow.setDate(oneYearFromNow.getDate() + 365);
      const twentyYearFromNow = new Date();
      twentyYearFromNow.setDate(twentyYearFromNow.getDate() + 365 * 20);
      const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;

      let totalLateAmount = 0;
      let totalFinCharge = 0;
      //var uniqueLoanAmount = application_plans && [...new Set(application_plans.map(item => item.amount))];
      var uniqueLoanAmount = 0;
      var uniqueRemainingAmount = 0;
      application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
            uniqueLoanAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.loan_amount : 0;
            uniqueRemainingAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.remaining_amount : 0;
         }
         return accumulator;
      }, []);
      //var uniqueLoanAmount = amountDetails && [...new Set(amountDetails.map(item => item.amount_rcvd))];
      var uniqueLateCount = 0;//application_plans && [...new Set(application_plans.map(item => item.late_count))];
      var uniqueMissedCount = application_plans && [...new Set(application_plans.map(item => item.missed_count))];


      // get plan details group by

      var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            // get recived amount
            var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
               if (planindex == 0) {
                  accumulator = 0;
               }
               accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
               return accumulator;
            }, 0);
            accumulator[currentValue.pp_id] = {
               paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.plan_status, invoice_exist: currentValue.invoice_exist, note: currentValue.note, status_name: currentValue.status_name, created_by: currentValue.created_by,
               refund_comment: currentValue.refund_comment,
               ach_account_no: currentValue.ach_account_no,
               ach_routing_no: currentValue.ach_routing_no,
               ach_bank_name: currentValue.ach_bank_name,
               check_date: currentValue.check_date,
               check_no: currentValue.check_no,
               status_name: currentValue.status_name,
               refund_amt: currentValue.refund_amt,
               payment_method: currentValue.payment_method,
               refundmethod: currentValue.refundmethod,
               refund_id: currentValue.refund_id,

               pro_refund_comment: currentValue.pro_refund_comment,
               pro_ach_account_no: currentValue.pro_ach_account_no,
               pro_ach_routing_no: currentValue.pro_ach_routing_no,
               pro_ach_bank_name: currentValue.pro_ach_bank_name,
               pro_check_date: currentValue.pro_check_date,
               pro_check_no: currentValue.pro_check_no,
               pro_status_name: currentValue.pro_status_name,
               pro_refund_amt: currentValue.pro_refund_amt,
               pro_payment_method: currentValue.pro_payment_method,
               pro_refundmethod: currentValue.pro_refundmethod,
               provider_invoice_status: currentValue.provider_invoice_status,
               iou_flag: currentValue.iou_flag,
            };
         } else {
            //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
         }

         return accumulator;
      }, []);
      var groupArrays = invoiceDetails && invoiceDetails.map((data, idx) => {
         var nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id == data.invoice_id);
         nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id > data.invoice_id && x.pp_id == nextDetailsDetails[0].pp_id);
         data.nextDeferred = 0;
         if (nextDetailsDetails.length > 0) {
            var checkNextInvoice = nextDetailsDetails.reduce(function (a, b) {
               return new Date(a.due_date) < new Date(b.due_date) ? a : b;
            });
            if (Object.keys(checkNextInvoice).length !== 0) {
               data.nextDeferred = 1;
            }
         }
         data.checkPartialPaid = (invoiceDetails.length > (idx + 1)) ? 1 : 0
         if (invoiceDetails[idx + 1] !== undefined && invoiceDetails[idx + 1].due_date == data.due_date) {
            data.checkPartialPaid = 0;
         }
         if (data.on_fly == 1 && nextDetailsDetails.length > 0 && data.invoice_status == 3 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else if (invoiceDetails.length > (idx + 1) && data.invoice_status != 1 && data.invoice_status != 4 && data.on_fly != 1 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else {
            data.missed_flag = 0;
         }

         //data.fin_charge_amt = (data.fin_charge_amt != null) ? (data.finpct != null) ? (parseFloat(data.fin_charge_amt) - (parseFloat(data.finpct) * parseFloat(datafin_charge_amt)) / 100) : parseFloat(data.fin_charge_amt) : 0;
         //data.late_fee_received = (data.late_fee_received != null) ? (data.latepct != null) ? (parseFloat(data.late_fee_received) - (parseFloat(data.latepct) * parseFloat(data.late_fee_received)) / 100) : parseFloat(data.late_fee_received) : 0;
         data.fin_charge_received = (data.invoice_status == 3) ? data.fin_charge_due : data.fin_charge_received;
         data.late_fee_received = (data.invoice_status == 3) ? data.late_fee_due : data.late_fee_received;

         //data.previous_fin_charge = (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
         //data.previous_late_fee = (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;
         data.previous_fin_charge = parseFloat(data.previous_fin_charge);
         data.previous_late_fee = parseFloat(data.previous_late_fee);

         data.total_due = data.payment_amount;
         //data.paid_amount1 = (data.invoice_status == 1 || data.invoice_status == 4) ? (data.paid_amount + data.fin_charge_received + data.late_fee_received + data.additional_amount + data.previous_fin_charge + data.previous_late_fee) : 0;
         data.paid_amount1 = (data.invoice_status == 1 || data.invoice_status == 4) ? (data.paid_amount + data.fin_charge_received + data.late_fee_received + data.additional_amount) : 0;
         data.total_due += (data.late_fee_received) ? data.late_fee_received : 0;
         data.total_due += (data.fin_charge_received) ? data.fin_charge_received : 0;

         data.total_due += (data.previous_late_fee && data.invoice_status == 3) ? data.previous_late_fee : 0;
         data.total_due += (data.previous_fin_charge && data.invoice_status == 3) ? data.previous_fin_charge : 0;

         if (idx != 0) {

            data.previous_blc += parseFloat(data.previous_fin_charge);
            data.previous_blc += parseFloat(data.previous_late_fee);
         }
         if (data.invoice_status == 1 || data.invoice_status == 4) {
            totalLateAmount += data.late_fee_received;// + data.previous_late_fee;
            totalFinCharge += data.fin_charge_received;// + data.previous_fin_charge;
         }
         data.finalAmt = (data.paid_amount1 <= data.total_due) ? data.total_due - data.paid_amount1 : 0;
         if (data.invoice_status == 1 || data.invoice_status == 4) {
            data.paid_amount1 = data.paid_amount1 + data.credit_charge_amt;
         }
         if ((data.invoice_status == 2 || data.invoice_status == 3) && invoiceDetails[invoiceDetails.length - 1].due_date > data.due_date) {
            uniqueLateCount++;
         }
         return data;
      })

      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
                           </li>
                           <li>
                              <a href="javascript:void(0)" onClick={this.adminPlanDetails.bind(this)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                           </li>
                        </ul>
                     </div>
                     {this.props.application_plans &&
                        <React.Fragment>
                           <div className={(this.state.agreementView) ? 'p-10 d-none' : 'p-10'}>

                              <h1 className="text-center mb-20">Customer Account</h1>
                              {this.props.application_plans[0].plan_updated == 1 &&
                                 <div class="alert alert-danger alert-dismissible fade show">
                                    Now this plan apply new Interest rate due to missed payment. Available Credit has been locked call HPS at (919) 600-5526 for any information.
                              </div>
                              }
                              <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">

                                 <div className="add-card">

                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="2">Customer Information</th>
                                          </tr>
                                          <tr>
                                             <td className="text-nowrap"><strong>Account No :</strong></td>
                                             <td>{(application_details) ? application_details[0].patient_ac : '-'}</td>
                                          </tr>
                                          <tr>
                                             <td className="text-nowrap"><strong>Application No :</strong></td>
                                             <td>{(application_details) ? application_details[0].application_no : '-'}</td>
                                          </tr>
                                          <tr>
                                             <td><strong>Name :</strong></td>
                                             <td className="text-capitalize">{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</td>
                                          </tr>

                                          <tr>
                                             <td><strong>Address :</strong></td>
                                             <td>{(application_details) ? (application_details[0].address1 + ', ' + application_details[0].City + ', ' + application_details[0].name + ', ' + application_details[0].zip_code) : '-'}

                                             </td>
                                          </tr>
                                          <tr>
                                             <td><strong>Phone :</strong></td>
                                             <td>{(application_details) ? application_details[0].peimary_phone : '-'}</td>
                                          </tr>

                                       </tbody>
                                    </table>

                                 </div>

                                 <div className="add-card">

                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="2">Loan Information</th>
                                          </tr>
                                          <tr>
                                             <td><strong>Line Of Credit :</strong></td>
                                             <td>{(application_details) ? '$' + parseFloat(application_details[0].approve_amount).toFixed(2) : '$0.00'}</td>
                                          </tr>
                                          <tr>
                                             <td><strong>Override Amount :</strong></td>
                                             <td>{(application_details) ? '$' + parseFloat(application_details[0].override_amount).toFixed(2) : '$0.00'}</td>
                                          </tr>
                                          <tr>
                                             <td><strong>Available Balance :</strong></td>
                                             <td>{(application_details) ? '$' + parseFloat(application_details[0].remaining_amount).toFixed(2) : '$0.00'}</td>
                                          </tr>
                                          <tr>
                                             <td><strong>Principal Amount :</strong></td>
                                             <td>{(application_details) ? '$' + parseFloat(uniqueLoanAmount).toFixed(2) : '$0.00'}</td>
                                          </tr>
                                          <tr>
                                             <td><strong>Outstanding Principal :</strong></td>
                                             <td>
                                                ${parseFloat(uniqueRemainingAmount).toFixed(2)}
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                 </div>

                                 <div className="add-card">
                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="2">Payment Information</th>
                                          </tr>

                                          <tr>
                                             <td><strong>Late/Missed Payments :</strong></td>
                                             <td>
                                                {uniqueLateCount}
                                             </td>
                                          </tr>
                                          {/*<tr>
                                             <td><strong>Payments Missed :</strong></td>
                                             <td>
                                                {uniqueMissedCount.reduce((a, b) => a + b, 0)}
                                             </td>
                                          </tr>*/}
                                          <tr>
                                             <td><strong>Late Fees :</strong></td>
                                             <td>
                                                ${parseFloat(totalLateAmount).toFixed(2)}
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><strong>Financial Charges :</strong></td>
                                             <td>
                                                ${parseFloat(totalFinCharge).toFixed(2)}
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                              {(application_details[0].co_patient_id != null) &&
                                 <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">
                                    <div className="add-card w-100">

                                       <table>
                                          <tbody>
                                             <tr>
                                                <th colSpan="3">Co-signer Information</th>
                                             </tr>
                                             <tr>
                                                <td><strong>Name:</strong> {application_details[0].co_first_name + ' ' + application_details[0].co_middle_name + ' ' + application_details[0].co_last_name}</td>
                                                <td><strong>Address:</strong> {application_details[0].co_address1 + ' ' + application_details[0].co_address2 + ' ' + application_details[0].co_City + ' ' + application_details[0].co_state_name}</td>
                                                <td><strong>Phone:</strong> {application_details[0].co_phone_no}</td>
                                             </tr>

                                          </tbody>
                                       </table>

                                    </div>
                                 </div>
                              }
                              <div className="table-responsive mb-5 pymt-history">
                                 <h3 className="text-center mb-10">Payment Plans</h3>
                                 <MuiThemeProvider theme={this.getMuiTheme()}>
                                    <table className="table table-borderless admin-application-payment-plan">
                                       <thead>
                                          <tr>
                                             <th>Plan ID</th>
                                             <th>Principal Amt</th>
                                             <th>Outstanding Principal Amt</th>
                                             <th>Payment Term</th>
                                             <th>APR(%)</th>
                                             <th>Monthly Payment</th>
                                             <th>Date Created</th>
                                             <th>Procedure Date</th>
                                             <th>Status</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>
                                       <tbody>

                                          {planDetails.map(function (row, idx) {
                                             return (<tr key={idx}>
                                                <td>{row.pp_id}</td>
                                                <td>${(row.loan_amount).toFixed(2)}</td>
                                                <td>${(row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2)}</td>
                                                <td>{row.payment_term_month} Month</td>
                                                <td>{parseFloat(row.discounted_interest_rate).toFixed(2)}%</td>
                                                <td>${parseFloat(row.monthly_amount).toFixed(2)}</td>
                                                <td>{row.date_created}</td>
                                                <td>{row.procedure_date}</td>


                                                <td>{
                                                   (row.status == 0)
                                                      ?
                                                      <span>{row.status_name} <Tooltip id="tooltip-top-end" title={row.note} placement="top-end"><i className="zmdi zmdi-comment-text"></i></Tooltip></span>
                                                      :
                                                      row.status_name
                                                }</td>

                                                <td>{(row.status == 4 && row.invoice_exist === null && row.created_by == currentUserId()) ?
                                                   <div>
                                                      <a href="javascript:void(0)" onClick={() => this.rePlanPopup(row.pp_id, row.plan_number, row.loan_amount)} title="Regenerate Plan"><i className="zmdi zmdi-hc-lg zmdi-time-restore mr-5"></i></a>
                                                      <a href="javascript:void(0)" onClick={() => this.cancelPopup(row.pp_id)} title="Cancel Plan"><i className="zmdi zmdi-hc-lg zmdi-close ml-1"></i></a>
                                                   </div>
                                                   :
                                                   (row.status == 2 && row.created_by == currentUserId()) ?
                                                      <Link to={`/provider/credit-application/upload-agreement/${this.enc(row.pp_id.toString())}`} title="Upload Signed Agreement"><i className="zmdi zmdi-upload zmdi-hc-lg"></i></Link>
                                                      :
                                                      (row.status == 3 && row.created_by == currentUserId()) ?
                                                         <i className="zmdi zmdi-assignment-alert zmdi-hc-lg"></i>
                                                         :
                                                         (row.status == 4 && row.created_by == currentUserId()) ?
                                                            <a href="javascript:void(0)" title="Document Approved"><i className="zmdi zmdi-check-all"></i></a>
                                                            :
                                                            (row.status == 9) ?
                                                               <a href="javascript:void(0)" title="Provider Confirmation" onClick={() => this.processPlanConfirmation(row.note, row.pp_id, row.provider_invoice_status)}><i className="zmdi zmdi-check-square"></i></a>
                                                               :
                                                               (row.status == 0 && row.refund_id != null) ?
                                                                  <a href="javascript:void(0)" title="View Refund details" onClick={() => this.refundDetails(row)}><i className="zmdi zmdi-eye zmdi-hc-lg"></i></a>
                                                                  :
                                                                  '-'
                                                }</td>
                                             </tr>)
                                          }.bind(this))
                                          }
                                       </tbody>
                                    </table>
                                 </MuiThemeProvider>
                              </div>

                              <div className="table-responsive mb-40 pymt-history">
                                 <h3 className="text-center mb-10">Invoice History</h3>
                                 <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                    <thead>
                                       <tr>
                                          <th>S. No.</th>
                                          <th>Invoice No.</th>
                                          <th>Payment Date</th>
                                          <th>Due Date</th>
                                          <th>Prev Bal</th>
                                          <th>Monthly Payment</th>
                                          <th>Late Fee</th>
                                          <th>Fin Charge</th>
                                          <th>Total Due</th>
                                          <th>Min Due</th>
                                          <th>Additional Amt</th>
                                          <th>Paid</th>
                                          <th>Balance</th>
                                          <th>Status</th>
                                       </tr>
                                    </thead>
                                    <tbody>

                                       {groupArrays && groupArrays.map(function (row, idx) {
                                          return <tr key={idx} className={idx}>
                                             <td>{idx + 1}</td>
                                             <td>{row.invoice_number}</td>
                                             <td>{(row.payment_date) ? row.payment_date : '-'}</td>
                                             <td>{row.due_date}</td>
                                             <td>{(row.previous_blc > 0) ? '$' + parseFloat(row.previous_blc).toFixed(2) : '-'}</td>
                                             <td>${parseFloat(row.payment_amount).toFixed(2)}</td>
                                             <td>{(row.late_fee_received) ? '$' + parseFloat(row.late_fee_received).toFixed(2) : '-'}</td>
                                             <td>{(row.fin_charge_received) ? '$' + parseFloat(row.fin_charge_received).toFixed(2) : '-'}</td>
                                             <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                             <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                             <td>{(row.additional_amount > 0) ? '$' + parseFloat(row.additional_amount).toFixed(2) : '-'}</td>
                                             <td>{(row.paid_amount1 > 0) ? '$' + parseFloat(row.paid_amount1).toFixed(2) : '-'}</td>
                                             <td>${parseFloat(row.finalAmt).toFixed(2)}</td>
                                             <td>

                                                {(row.missed_flag == 1) ?
                                                   'Missed'
                                                   :
                                                   (row.invoice_status == 1) ?
                                                      row.invoice_status_name
                                                      :
                                                      (row.invoice_status == 3) ?
                                                         row.invoice_status_name
                                                         :
                                                         (row.invoice_status == 4) ?
                                                            row.invoice_status_name
                                                            :
                                                            row.invoice_status_name
                                                }
                                             </td>
                                          </tr>
                                       }.bind(this))
                                       }
                                    </tbody>
                                 </table>
                              </div>

                           </div>
                           <div className={(this.state.agreementView) ? 'modal-body page-form-outer text-left' : 'modal-body page-form-outer text-left d-none'} >
                              <div className="row">
                                 <div className="col-sm-12 mx-auto">

                                    <RctCard>
                                       {this.props.rePlanDetails &&
                                          <React.Fragment>
                                             <div className="p-10" ref={el => (this.componentReff = el)}>
                                                {(() => {
                                                   return <React.Fragment>
                                                      <h1 className="text-center mb-20">
                                                         <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                                                      </h1>
                                                      <h1 className="text-center mb-20">Terms of Agreement for Addendum Plan {this.state.currentPlanNumber}</h1>
                                                      <div className="terms-condition-agreement">
                                                         <div>Date <strong><u>{moment(Date.now()).format('MM/DD/YYYY')}</u></strong></div>
                                                         <div>Location <u>{this.props.reProviderDetails.address1 + ' ' + this.props.reProviderDetails.address2 + ','}</u></div>
                                                         <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>{this.props.reProviderDetails.city + ', ' + this.props.reProviderDetails.state_name + '-' + this.props.reProviderDetails.zip_code}</u></div>
                                                         <p>On or before <strong>Date</strong>, for value received, the undersigned <strong><u>{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</u></strong> (the "Borrower") promises to pay to the order of HEALTH PARTNER INC. (the "Holder"), in the manner and at the place provided below, the principal sum of <strong><u>${parseFloat(this.state.data.loan_amount).toFixed(2)}</u></strong>.</p>

                                                         <h4><strong>1. PAYMENT :</strong></h4>
                                                         <p>All payments of principal and interest under this note will be made in lawful money of the United States, without offset, deduction, or counterclaim, by wire transfer of immediately available funds to an account designated by the Holder in writing at least 7 days after the effective date of this note or, if this designation is not made, by check mailed to the Holder at 5720 Creedmoor Road, Suite 103, Raleigh, North Carolina, 27612, or at such other place as the Holder may designate in writing.</p>

                                                         <h4><strong>2. INTEREST :</strong></h4>
                                                         <p>Interest on the unpaid principal balance of this note is payable from the date of this note until this note is paid in full, at the rate of <strong><u>{(this.props.rePlanDetails) ? (parseFloat(this.props.rePlanDetails.interest_rate)).toFixed(2) + "%" : '-'}</u></strong> per year, or the maximum amount allowed by applicable law, whichever is less. Accrued interest will be computed on the basis of a 365-day or 366-day year, as the case may be, based on the actual number of days elapsed in the period in which it accrues.</p>

                                                         <h4><strong>3. PREPAYMENT :</strong></h4>
                                                         <p>The Borrower may prepay this note, in whole or in part, at any time before maturity without penalty or premium. Any partial prepayment will be credited first to accrued interest, then to principal. No prepayment extends or postpones the maturity date of this note.</p>

                                                         <h4><strong>4. EVENTS OF DEFAULT :</strong></h4>
                                                         <p>Each of the following constitutes an <strong>"Event of Default"</strong> under this note:</p>
                                                         <ul>
                                                            <li>The Borrower's failure to make any payment when due under the terms of this note, including the lump-sum payment due under this note at its maturity;</li>
                                                            <li>The filing of any voluntary or involuntary petition in bankruptcy by or regarding the Borrower or the initiation of any proceeding under bankruptcy or insolvency laws against the Borrower;</li>
                                                            <li>An assignment made by the Borrower for the benefit of creditors;</li>
                                                            <li>The appointment of a receiver, custodian, trustee, or similar party to take possession of the Borrower's assets or property; or</li>
                                                            <li>The death of the Borrower.</li>
                                                         </ul>

                                                         <h4><strong>5. ACCELERATION; REMEDIES ON DEFAULT :</strong></h4>
                                                         <p>If any Event of Default occurs, all principal and other amounts owed under this note will become immediately due and payable without any action by the Holder, the Borrower, or any other person. The Holder, in addition to any rights and remedies available to the Holder under this note, may, in its sole discretion, pursue any legal or equitable remedies available to it under applicable law or in equity, including taking any of the following actions:</p>
                                                         <ul>
                                                            <li>Personally, or by agents or attorneys (in compliance with applicable law), take immediate possession of the collateral. To that end, the Holder may pursue the collateral where it may be found, and enter the Borrower's premises, with or without notice, demand, process of law, or legal procedure if this can be done without breach of the peace. If the premises on which any part of the collateral is located are not under the Borrower's direct control, the Borrower will exercise its best efforts to ensure that the Holder is promptly provided right of access to those premises. To the extent that the Borrower's consent would otherwise be required before a right of access could be granted, the Borrower hereby irrevocably grants that consent;</li>
                                                            <li>Require the Borrower to assemble the collateral and make it available to the Holder at a place to be designated by the Holder that is reasonably convenient to both parties (it being acknowledged that the Borrower's premises are reasonably convenient to the Borrower);</li>
                                                            <li>Sell, lease, or dispose of the collateral or any part of it in any manner permitted by applicable law or by contract; and</li>
                                                            <li>Exercise all rights and remedies of a secured party under applicable law.
</li>
                                                         </ul>

                                                         <h4><strong>6. WAIVER OF PRESENTMENT; DEMAND :</strong></h4>
                                                         <p>The Borrower hereby waives presentment, demand, notice of dishonor, notice of default or delinquency, notice of protest and nonpayment, notice of costs, expenses or losses and interest on those, notice of interest on interest and late charges, and diligence in taking any action to collect any sums owing under this note, including (to the extent permitted by law) waiving the pleading of any statute of limitations as a defense to any demand against the undersigned. Acceptance by the Holder or any other holder of this note of any payment differing from the designated lump-sum payment listed above does not relieve the undersigned of the obligation to honor the requirements of this note.</p>

                                                         <h4><strong>7. Frozen Credit and Penalties :</strong></h4>
                                                         <p>The Borrower agrees that in event of missed payment, any remaining line of credit will be frozen and will not be accessible until remaining balances have been paid. Missed payments will occur late fees and increased interest charges.</p>

                                                         <h4><strong>8. GOVERNING LAW :</strong></h4>
                                                         <ul>
                                                            <li><strong>Choice of Law.</strong> The laws of the state of North Carolina govern this note (without giving effect to its conflicts of law principles).</li>
                                                            <li><strong>Choice of Forum.</strong> Both parties consent to the personal jurisdiction of the state and federal courts in Wake, North Carolina.</li>
                                                         </ul>

                                                         <h4><strong>9. COLLECTION COSTS AND ATTORNEYS' FEES :</strong></h4>
                                                         <p>The Borrower shall pay all expenses of the collection of indebtedness evidenced by this note, including reasonable attorneys' fees and court costs in addition to other amounts due.</p>

                                                         <h4><strong>10. ASSIGNMENT AND DELEGATION :</strong></h4>
                                                         <ul>
                                                            <li><strong>No Assignment.</strong> The Borrower may not assign any of its rights under this note. All voluntary assignments of rights are limited by this subsection.</li>
                                                            <li><strong>No Delegation.</strong> The Borrower may not delegate any performance under this note.</li>
                                                            <li><strong>Enforceability of an Assignment or Delegation.</strong> If a purported assignment or purported delegation is made in violation of this section, it is void.</li>
                                                         </ul>

                                                         <h4><strong>11. SEVERABILITY :</strong></h4>
                                                         <p>If any one or more of the provisions contained in this note is, for any reason, held to be invalid, illegal, or unenforceable in any respect, that invalidity, illegality, or unenforceability will not affect any other provisions of this note, but this note will be construed as if those invalid, illegal, or unenforceable provisions had never been contained in it, unless the deletion of those provisions would result in such a material change so as to cause completion of the transactions contemplated by this note to be unreasonable.</p>

                                                         <h4><strong>12. NOTICES :</strong></h4>
                                                         <ul>
                                                            <li><strong>Writing; Permitted Delivery Methods.</strong> Each party giving or making any notice, request, demand, or other communication required or permitted by this note shall give that notice in writing and use one of the following types of delivery, each of which is a writing for purposes of this note: personal delivery, mail (registered or certified mail, postage prepaid, return-receipt requested), nationally recognized overnight courier (fees prepaid), facsimile, or email.</li>
                                                            <li><strong>Addresses.</strong> A party shall address notices under this section to a party at the following addresses:<br /><br />
                                                               <p><strong>If to the Borrower :</strong></p>
                                                               <p><u>{application_details[0].address1 + ' ' + application_details[0].address2},</u></p>
                                                               <p><u>{application_details[0].City + ', ' + application_details[0].name + '-' + application_details[0].zip_code}</u></p>
                                                               <p><u><strong>Phone:</strong> {application_details[0].phone_no}</u></p>

                                                               <strong>If to the Holder:</strong>
                                                               <p>Health Partner Inc.</p>
                                                               <p>5720 Creedmoor Road, Suite 103</p>
                                                               <p>Raleigh, North Carolina 27612</p>

                                                            </li>
                                                            <li><strong>Effectiveness.</strong> A notice is effective only if the party giving notice complies with subsections (a)</li>
                                                            <li>and if the recipient receives the notice</li>
                                                         </ul>

                                                         <h4><strong>13. WAIVER :</strong></h4>
                                                         <p>No waiver of a breach, failure of any condition, or any right or remedy contained in or granted by the provisions of this note will be effective unless it is in writing and signed by the party waiving the breach, failure, right, or remedy. No waiver of any breach, failure, right, or remedy will be deemed a waiver of any other breach, failure, right, or remedy, whether or not similar, and no waiver will constitute a continuing waiver, unless the writing so specifies.</p>

                                                         <h4><strong>14. HEADINGS :</strong></h4>
                                                         <p>The descriptive headings of the sections and subsections of this note are for convenience only, and do not affect this note's construction or interpretation.</p>


                                                         <br /><br />
                                                      </div>
                                                      <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                                                         <div className="add-card">

                                                            <table>
                                                               <tbody>
                                                                  <tr>
                                                                     <th colSpan="2">Customer Information</th>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Account No:</strong> {application_details[0].patient_ac}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Application No:</strong> {application_details[0].application_no}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Name:</strong> {application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Address:</strong> {application_details[0].address1 + ' ' + application_details[0].address2 + ' ' + application_details[0].City + ' ' + application_details[0].name}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Phone:</strong> {application_details[0].peimary_phone}</td>
                                                                     <td></td>
                                                                  </tr>



                                                               </tbody>
                                                            </table>

                                                         </div>
                                                         <div className="add-card">

                                                            <table>
                                                               <tbody>
                                                                  <tr>
                                                                     <th colSpan="2">Provider Information</th>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Account No:</strong> {this.props.reProviderDetails.provider_ac}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Name:</strong> {this.props.reProviderDetails.name}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Address:</strong> {this.props.reProviderDetails.address1 + ' ' + this.props.reProviderDetails.address2 + ' ' + this.props.reProviderDetails.city + ' ' + this.props.reProviderDetails.state_name}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Phone:</strong> {this.props.reProviderDetails.primary_phone}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Doctor:</strong> - {(this.props.reProviderDetails.docter_name != '') ? this.props.reProviderDetails.docter_name : '-'}</td>
                                                                     <td></td>
                                                                  </tr>


                                                               </tbody>
                                                            </table>

                                                         </div>

                                                         <div className="add-card">

                                                            <table>
                                                               <tbody>
                                                                  <tr>
                                                                     <th colSpan="2">Loan Information</th>
                                                                  </tr>

                                                                  <tr>
                                                                     <td><strong>Approved Amount:</strong> ${parseFloat(application_details[0].approve_amount).toFixed(2)}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Principal Amount:</strong> ${parseFloat(this.state.data.loan_amount).toFixed(2)}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Remaining Amount:</strong> ${parseFloat(application_details[0].remaining_amount + this.state.currentLoanAmount - this.state.data.loan_amount).toFixed(2)}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Loan Amount:</strong> ${parseFloat(this.props.rePlanDetails.totalAmount).toFixed(2)}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>APR:</strong> {(this.props.rePlanDetails.interest_rate) ? parseFloat(this.props.rePlanDetails.interest_rate).toFixed(2) : '0.00'}%</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Term Month:</strong> {this.props.rePlanDetails.term_month}</td>
                                                                     <td></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><strong>Procedure Date:</strong> {moment(this.state.data.procedure_date).format('MM/DD/YYYY')}</td>
                                                                     <td></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>

                                                         </div>


                                                      </div>
                                                      {(application_details[0].co_patient_id != null) &&
                                                         <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                                                            <div className="add-card w-100">

                                                               <table>
                                                                  <tbody>
                                                                     <tr>
                                                                        <th colSpan="3">Co-signer Information</th>
                                                                     </tr>
                                                                     <tr>
                                                                        <td><strong>Name:</strong> {application_details[0].co_first_name + ' ' + application_details[0].co_middle_name + ' ' + application_details[0].co_last_name}</td>
                                                                        <td><strong>Address:</strong> {application_details[0].co_address1 + ' ' + application_details[0].co_address2 + ' ' + application_details[0].co_City + ' ' + application_details[0].co_state_name}</td>
                                                                        <td><strong>Phone:</strong> {application_details[0].co_phone_no}</td>
                                                                     </tr>

                                                                  </tbody>
                                                               </table>

                                                            </div>

                                                         </div>
                                                      }
                                                      <div className="table-responsive mb-40 pymt-history">
                                                         <h2 className="text-center mb-10">Plan Details</h2>
                                                         <div className="table-responsive">
                                                            <table className="table table-borderless">
                                                               <thead>
                                                                  <tr>
                                                                     <th>SN#</th>
                                                                     <th>Date</th>
                                                                     <th>Amount</th>
                                                                  </tr>
                                                               </thead>
                                                               <tbody>
                                                                  {this.props.rePlanDetails.monthlyPlan.map((plan, idx) => (
                                                                     <tr key={idx}>
                                                                        <td>{idx + 1}</td>
                                                                        <td>{plan.next_month}</td>
                                                                        <td>${parseFloat(plan.perMonth).toFixed(2)}</td>
                                                                     </tr>
                                                                  ))}
                                                               </tbody>
                                                            </table>

                                                         </div>
                                                      </div>
                                                      <div className="mb-40 pymt-history">

                                                         <div className="row user-permission mt-20">
                                                            <div className="col-md-12">
                                                               <p className="text-danger">Note:- This payment plan does not reflect possible charges occurring from late fees, transaction fees, or financial charges.  Interest will be charged to your account at a variable APR of 26.99% from purchase date if the purchase amount is not paid in full within the loan term or if you make a late payment. To avoid late fees, you must make your Monthly Payments by the due date each month.</p>
                                                            </div>
                                                            <div className="col-md-12">
                                                               <p>
                                                                  <label className="check-container">
                                                                     <Input name="current_plan" type="checkbox" value="1" checked={(this.state.data.agree_with == 1) ? true : false} onChange={(e) => this.onChnagePlanExist('agree_with', e.target.value)} />
                                                                     <span className="checkmark"></span><span className="pl-30 agree-text">By signing below, you, the Borrower, agree to all terms listed in the above Agreement.</span>
                                                                  </label>
                                                               </p>
                                                            </div>



                                                            <div className="col-md-4">
                                                               <FormGroup>
                                                                  <Label for="provider_location">BY (Name of Borrower)<span className="required-field">*</span></Label>
                                                                  <TextField
                                                                     type="text"
                                                                     name="name_of_borrower"
                                                                     id="name_of_borrower"
                                                                     fullWidth
                                                                     variant="outlined"
                                                                     placeholder="Name of Borrower"
                                                                     value={this.state.data.name_of_borrower || ''}
                                                                     error={(this.state.dataError.name_of_borrower) ? true : false}
                                                                     helperText={(this.state.dataError.name_of_borrower != '') ? this.state.dataError.name_of_borrower : ''}
                                                                     onChange={(e) => this.onChnagePlanExist('name_of_borrower', e.target.value)}
                                                                  />
                                                               </FormGroup>
                                                            </div>
                                                            <div className="col-md-4">
                                                               <FormGroup>
                                                                  <Label for="procedure_date">Date<span className="required-field">*</span></Label>
                                                                  <DatePicker
                                                                     dateFormat="MM/dd/yyyy h:mm aa"
                                                                     timeIntervals={15}
                                                                     timeFormat="HH:mm"
                                                                     showTimeSelect
                                                                     name="agreement_date"
                                                                     id="agreement_date"
                                                                     minDate={new Date()}
                                                                     maxDate={new Date()}
                                                                     selected={this.state.agreement_date}
                                                                     placeholderText="MM/DD/YYYY"
                                                                     autocomplete={false}
                                                                     onChange={(e) => this.onChnagePlanExist('agreement_date', e)}
                                                                  />
                                                                  {(this.state.dataError.agreement_date) ? <FormHelperText>{this.state.dataError.agreement_date}</FormHelperText> : ''}
                                                               </FormGroup>
                                                            </div>



                                                         </div>
                                                         <h2 className="mb-10">ID Proof documents Details</h2>
                                                         <div className="row">
                                                            <div className="col-md-4">
                                                               <FormGroup>
                                                                  <Label for="provider_location">ID Type<span className="required-field">*</span></Label>
                                                                  <TextField
                                                                     type="text"
                                                                     name="id_name"
                                                                     id="id_name"
                                                                     fullWidth
                                                                     variant="outlined"
                                                                     placeholder="ID Type"
                                                                     value={this.state.data.id_name || ''}
                                                                     error={(this.state.dataError.id_name) ? true : false}
                                                                     helperText={(this.state.dataError.id_name != '') ? this.state.dataError.id_name : ''}
                                                                     onChange={(e) => this.onChnagePlanExist('id_name', e.target.value)}
                                                                  />
                                                               </FormGroup>
                                                            </div>
                                                            <div className="col-md-4">
                                                               <FormGroup>
                                                                  <Label for="provider_location">ID Number<span className="required-field">*</span></Label>
                                                                  <TextField
                                                                     type="text"
                                                                     name="id_number"
                                                                     id="id_number"
                                                                     fullWidth
                                                                     variant="outlined"
                                                                     placeholder="ID Number"
                                                                     value={this.state.data.id_number || ''}
                                                                     error={(this.state.dataError.id_number) ? true : false}
                                                                     helperText={(this.state.dataError.id_number != '') ? this.state.dataError.id_number : ''}
                                                                     onChange={(e) => this.onChnagePlanExist('id_number', e.target.value)}
                                                                  />
                                                               </FormGroup>
                                                            </div>
                                                            <div className="col-md-3">
                                                               <FormGroup>
                                                                  <Label for="procedure_date">Expiry Date</Label>
                                                                  <DatePicker
                                                                     dateFormat="MM/dd/yyyy"
                                                                     name="expiry_date"
                                                                     id="expiry_date"
                                                                     minDate={new Date()}
                                                                     maxDate={twentyYearFromNow}
                                                                     selected={this.state.expiry_date}
                                                                     placeholderText="MM/DD/YYYY"
                                                                     autocomplete={false}
                                                                     onChange={(e) => this.onChnagePlanExist('expiry_date', e)}
                                                                  />
                                                                  {(this.state.dataError.expiry_date) ? <FormHelperText>{this.state.dataError.expiry_date}</FormHelperText> : ''}
                                                               </FormGroup>
                                                            </div>
                                                         </div>
                                                         {(application_details[0].co_patient_id != null) &&
                                                            <React.Fragment>
                                                               <div className="row">
                                                                  <div className="col-md-4">
                                                                     <FormGroup>
                                                                        <Label for="co_name_of_borrower">BY (Cosigner Name)<span className="required-field">*</span></Label>
                                                                        <TextField
                                                                           type="text"
                                                                           name="co_name_of_borrower"
                                                                           id="co_name_of_borrower"
                                                                           fullWidth
                                                                           variant="outlined"
                                                                           placeholder="Name of Borrower"
                                                                           value={this.state.data.co_name_of_borrower || ''}
                                                                           error={(this.state.dataError.co_name_of_borrower) ? true : false}
                                                                           helperText={(this.state.dataError.co_name_of_borrower != '') ? this.state.dataError.co_name_of_borrower : ''}
                                                                           onChange={(e) => this.onChnagePlanExist('co_name_of_borrower', e.target.value)}
                                                                        />
                                                                     </FormGroup>
                                                                  </div>
                                                                  <div className="col-md-4">
                                                                     <FormGroup>
                                                                        <Label for="co_agreement_date">Date<span className="required-field">*</span></Label>
                                                                        <DatePicker
                                                                           dateFormat="MM/dd/yyyy h:mm aa"
                                                                           timeIntervals={15}
                                                                           timeFormat="HH:mm"
                                                                           showTimeSelect
                                                                           name="co_agreement_date"
                                                                           id="co_agreement_date"
                                                                           minDate={new Date()}
                                                                           maxDate={new Date()}
                                                                           selected={this.state.co_agreement_date}
                                                                           placeholderText="MM/DD/YYYY"
                                                                           autocomplete={false}
                                                                           onChange={(e) => this.onChnagePlanExist('co_agreement_date', e)}
                                                                        />
                                                                        {(this.state.dataError.co_agreement_date) ? <FormHelperText>{this.state.dataError.co_agreement_date}</FormHelperText> : ''}
                                                                     </FormGroup>
                                                                  </div>

                                                               </div>
                                                               <h2 className="mb-10">Co-signer ID Proof documents Details</h2>
                                                               <div className="row">
                                                                  <div className="col-md-4">
                                                                     <FormGroup>
                                                                        <Label for="co_id_name">ID Type<span className="required-field">*</span></Label>
                                                                        <TextField
                                                                           type="text"
                                                                           name="co_id_name"
                                                                           id="co_id_name"
                                                                           fullWidth
                                                                           variant="outlined"
                                                                           placeholder="ID Type"
                                                                           value={this.state.data.co_id_name || ''}
                                                                           error={(this.state.dataError.co_id_name) ? true : false}
                                                                           helperText={(this.state.dataError.co_id_name != '') ? this.state.dataError.co_id_name : ''}
                                                                           onChange={(e) => this.onChnagePlanExist('co_id_name', e.target.value)}
                                                                        />
                                                                     </FormGroup>
                                                                  </div>
                                                                  <div className="col-md-4">
                                                                     <FormGroup>
                                                                        <Label for="co_id_number">ID Number<span className="required-field">*</span></Label>
                                                                        <TextField
                                                                           type="text"
                                                                           name="co_id_number"
                                                                           id="co_id_number"
                                                                           fullWidth
                                                                           variant="outlined"
                                                                           placeholder="ID Number"
                                                                           value={this.state.data.co_id_number || ''}
                                                                           error={(this.state.dataError.co_id_number) ? true : false}
                                                                           helperText={(this.state.dataError.co_id_number != '') ? this.state.dataError.co_id_number : ''}
                                                                           onChange={(e) => this.onChnagePlanExist('co_id_number', e.target.value)}
                                                                        />
                                                                     </FormGroup>
                                                                  </div>
                                                                  <div className="col-md-3">
                                                                     <FormGroup>
                                                                        <Label for="co_expiry_date">Expiry Date</Label>
                                                                        <DatePicker
                                                                           dateFormat="MM/dd/yyyy"
                                                                           name="co_expiry_date"
                                                                           id="co_expiry_date"
                                                                           minDate={new Date()}
                                                                           maxDate={twentyYearFromNow}
                                                                           selected={this.state.co_expiry_date}
                                                                           placeholderText="MM/DD/YYYY"
                                                                           autocomplete={false}
                                                                           onChange={(e) => this.onChnagePlanExist('co_expiry_date', e)}
                                                                        />
                                                                        {(this.state.dataError.co_expiry_date) ? <FormHelperText>{this.state.dataError.co_expiry_date}</FormHelperText> : ''}
                                                                     </FormGroup>
                                                                  </div>
                                                               </div>
                                                            </React.Fragment>
                                                         }
                                                         <div className="user-permission mt-20">
                                                            <div className="row">

                                                               <div className="col-md-12">

                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div className="p-10">
                                                         <div className="row justify-content-between mb-30 add-full-card customer-accnt">

                                                            <div className="add-card-sign col-md-4 text-center sing_box">
                                                               <b>Customer Signature</b>
                                                               <div className="">
                                                                  <SignatureCanvas penColor='green'
                                                                     canvasProps={{ width: 285, height: 95, className: (this.state.customerSignAllow) ? 'sigCanvas border border-primary' : 'sigCanvas border border-primary bg-secondary' }} ref={(ref) => { this.customerSigPad = ref }} />
                                                               </div>
                                                               <div>
                                                                  <div className="mt-10">
                                                                     <Button onClick={this.clearCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                        Reset
                                                         </Button>
                                                                     <Button onClick={this.trimCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                        Done
                                                         </Button>
                                                                  </div>
                                                               </div>
                                                               <div className="mt-10">
                                                                  {this.state.customerSignURL ? <img src={this.state.customerSignURL} /> : null}
                                                               </div>

                                                            </div>
                                                            {(application_details[0].co_patient_id != null) &&
                                                               <div className="add-card-sign col-md-4 text-center sing_box">
                                                                  <b>Co-signer Signature</b>
                                                                  <div className="">
                                                                     <SignatureCanvas penColor='green'
                                                                        canvasProps={{ width: 285, height: 95, className: (this.state.cosignerSignAllow) ? 'sigCanvas border border-primary' : 'sigCanvas border border-primary bg-secondary' }} ref={(ref) => { this.cosignerSigPad = ref }} />
                                                                  </div>
                                                                  <div>
                                                                     <div className="mt-10">
                                                                        <Button onClick={this.clearCosigner} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                           Reset
                                                         </Button>
                                                                        <Button onClick={this.trimCosigner} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                           Done
                                                         </Button>
                                                                     </div>
                                                                  </div>
                                                                  <div className="mt-10">
                                                                     {this.state.cosignerSignURL ? <img src={this.state.cosignerSignURL} /> : null}
                                                                  </div>
                                                               </div>
                                                            }
                                                            <div className="add-card-sign col-md-4 text-center sing_box">

                                                               <b>Witness Signature</b>
                                                               <div className="">
                                                                  <SignatureCanvas penColor='green'
                                                                     canvasProps={{ width: 285, height: 95, className: (this.state.witnessSignAllow) ? 'sigCanvas border border-primary' : 'sigCanvas border border-primary bg-secondary' }} ref={(ref) => { this.witnessSigPad = ref }} />
                                                               </div>
                                                               <div>
                                                                  <div className="mt-10">
                                                                     <Button onClick={this.clearWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                        Reset
                                                         </Button>
                                                                     <Button onClick={this.trimWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                                        Done
                                                         </Button>
                                                                  </div>
                                                               </div>
                                                               <div className="mt-10">
                                                                  {this.state.witnessSignURL ? <img src={this.state.witnessSignURL} /> : null}
                                                               </div>
                                                            </div>

                                                         </div>
                                                      </div>

                                                   </React.Fragment>
                                                })()}
                                             </div>
                                             <div className="invoice-head">
                                                <div className="row">
                                                   <div className="col-sm-12 text-right">
                                                      <div className="mt-15 mb-10">
                                                         <Button onClick={this.goBackPlan.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                                            Back
                                                   </Button>
                                                         <Button disabled={!this.validateFinal()} onClick={() => this.submitRe()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                                            Submit
                                                   </Button>

                                                         <Button disabled={!this.validatePrintFinal()} onClick={() => this.processPrintPlanSubmit()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                                            Print
                                                   </Button>


                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </React.Fragment>
                                       }
                                    </RctCard>
                                 </div>
                              </div>
                           </div>
                        </React.Fragment>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                  </RctCard>

                  <Modal isOpen={this.state.rePlanModal}>
                     <ModalHeader toggle={() => this.rePlanModalClose()}>
                        Regenerate Plan - {this.state.currentPlanNumber}
                     </ModalHeader>
                     <ModalBody>
                        <Form autoComplete="off">
                           <div className="row" >
                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="procedure_amount"
                                       id="procedure_amount"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="($)"
                                       value={this.state.data.procedure_amount}
                                       error={(this.state.dataError.procedure_amount) ? true : false}
                                       helperText={(this.state.dataError.procedure_amount != '') ? this.state.dataError.procedure_amount : ''}
                                       onChange={(e) => this.onChnagePlanExist('procedure_amount', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>

                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                                    <DatePicker
                                       dateFormat="MM/dd/yyyy"
                                       name="procedure_date"
                                       id="procedure_date"
                                       minDate={new Date()}
                                       maxDate={oneYearFromNow}
                                       selected={this.state.procedure_date}
                                       placeholderText="MM/DD/YYYY"
                                       autocomplete={false}
                                       onChange={(e) => this.onChnagePlanExist('procedure_date', e)}
                                    />
                                    {(this.state.dataError.procedure_date) ? <FormHelperText>{this.state.dataError.procedure_date}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>

                              <div className="col-md-4">
                                 <FormGroup>
                                    <Label for="loan_amount">Loan Amount($)<span className="required-field">*</span></Label><br />
                                    <TextField
                                       type="text"
                                       name="loan_amount"
                                       id="loan_amount"
                                       fullWidth
                                       variant="outlined"
                                       placeholder="Enter Loan Amount"
                                       value={this.state.data.loan_amount}
                                       error={(this.state.dataError.loan_amount) ? true : false}
                                       helperText={this.state.dataError.loan_amount}
                                       onChange={(e) => this.onChnagePlanExist('loan_amount', e.target.value)}
                                    />
                                 </FormGroup>
                              </div>

                              <div className="col-md-12">
                                 <FormGroup>
                                    <Label for="note">Note<span className="required-field">*</span></Label><br />
                                    <Input
                                       type="textarea"
                                       name="note"
                                       id="note"
                                       variant="outlined"
                                       value={this.state.data.note}
                                       placeholder="Note"
                                       onChange={(e) => this.onChnagePlanExist('note', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.dataError.note) ? <FormHelperText>{this.state.dataError.note}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                           </div>
                        </Form>
                     </ModalBody>
                     <ModalFooter>
                        <Button
                           variant="contained"
                           color="primary"
                           //onClick={this.submitRe.bind(this)}
                           onClick={this.processPlan.bind(this)}
                           disabled={!this.validateSearchPlan()}
                        >Submit</Button>

                        <Button variant="contained" className="text-white btn-danger" onClick={() => this.rePlanModalClose()}>Cancel</Button>
                     </ModalFooter>

                     {this.state.loading &&
                        <RctSectionLoader />
                     }

                  </Modal>

                  <Modal isOpen={this.state.cancelModal}>
                     <ModalHeader toggle={() => this.cancelModalClose()}>
                        Cancel Plan
                     </ModalHeader>
                     <ModalBody>
                        <div className="row" >
                           <div className="col-md-12">
                              <FormGroup>
                                 <Label for="note">Note<span className="required-field">*</span></Label><br />
                                 <Input
                                    type="textarea"
                                    name="note"
                                    id="note"
                                    variant="outlined"
                                    value={this.state.cancelData.note}
                                    placeholder="Note"
                                    onChange={(e) => this.onChnagePlanCancel('note', e.target.value)}
                                 >
                                 </Input>
                                 {(this.state.dataCancelError.note) ? <FormHelperText>{this.state.dataCancelError.note}</FormHelperText> : ''}
                              </FormGroup>
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <Button
                           variant="contained"
                           color="primary"
                           //onClick={this.submitRe.bind(this)}
                           onClick={this.cancelPlanSubmit.bind(this)}
                           disabled={!this.validateCancelPlan()}
                        >Submit</Button>

                        <Button variant="contained" className="text-white btn-danger" onClick={() => this.cancelModalClose()}>Cancel</Button>
                     </ModalFooter>

                     {this.state.loading &&
                        <RctSectionLoader />
                     }

                  </Modal>

                  <Modal isOpen={this.state.openViewRefund} toggle={() => this.onViewRefundClose()}>
                     <ModalHeader toggle={() => this.onViewRefundClose()}>
                        Refund Details
                     </ModalHeader>
                     <ModalBody>
                        {this.state.refundDetails &&

                           <div>
                              {(this.state.refundDetails.iou_flag != null) &&
                                 <table className="table table-striped table-hover table-sm table-bordered">
                                    <tbody>
                                       <tr>
                                          <td>Refund Type</td>
                                          <td colSpan="3">
                                             {(this.state.refundDetails.iou_flag == 1) ? "IOU" : "Issue Refund"}
                                          </td>
                                       </tr>
                                       {(this.state.refundDetails.pro_payment_method == 3) &&
                                          <tr>
                                             <td><strong>Data Check No : </strong></td>
                                             <td>
                                                {this.state.pro_refundDetails.refund_duecheck_no}
                                             </td>
                                             <td><strong>Check Date : </strong></td>
                                             <td>
                                                {this.state.pro_refundDetails.refund_duecheck_date}
                                             </td>
                                          </tr>
                                       }
                                       {(this.state.refundDetails.pro_payment_method == 1) &&
                                          <React.Fragment>
                                             <tr>
                                                <td><strong>Bank Name : </strong></td>
                                                <td>
                                                   {this.state.refundDetails.pro_ach_bank_name}
                                                </td>
                                                <td><strong>Routing No. : </strong></td>
                                                <td>
                                                   {this.state.refundDetails.pro_ach_routing_no}
                                                </td>
                                             </tr>
                                             <tr>
                                                <td><strong>Account No : </strong></td>
                                                <td colSpan="3">
                                                   {this.state.refundDetails.pro_ach_account_no}
                                                </td>
                                             </tr>
                                          </React.Fragment>
                                       }
                                       <tr>
                                          <td><strong>Amount : </strong></td>
                                          <td colSpan="3">
                                             {(this.state.refundDetails.pro_refund_amt) ? '$' + this.state.refundDetails.pro_refund_amt.toFixed(2) : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Note : </strong></td>
                                          <td colSpan="3">
                                             {(this.state.refundDetails.pro_refund_comment) ? this.state.refundDetails.pro_refund_comment : '-'}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              }
                              {(this.state.refundDetails.iou_flag == null) &&
                                 <span>Refund details not found.</span>
                              }

                           </div>
                        }
                     </ModalBody>

                  </Modal>

                  <Modal isOpen={this.state.roviderConfirmations} toggle={() => this.roviderConfirmationsClose()}>
                     <ModalHeader toggle={() => this.roviderConfirmationsClose()}>
                        Plan cancellation process
                     </ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="col-md-12 pb-5">
                              Note: {this.state.cancellationNote}
                           </div>
                           <div className="col-md-12">
                              <FormGroup tag="fieldset">
                                 <Label>Do you approve cancellation of this plan?</Label>
                                 <FormGroup check>
                                    <Label check>
                                       <Input
                                          type="radio"
                                          name="provider_confirm"
                                          value={1}
                                          checked={(this.state.provider_confirm === 1) ? true : false}
                                          onChange={(e) => this.onChnagePConfim('provider_confirm', e.target.value)}
                                       />
                                    Yes
                                 </Label>
                                 </FormGroup>
                                 <FormGroup check>
                                    <Label check>
                                       <Input
                                          type="radio"
                                          name="provider_confirm"
                                          value={0}
                                          checked={(this.state.provider_confirm === 0) ? true : false}
                                          onChange={(e) => this.onChnagePConfim('provider_confirm', e.target.value)}
                                       />
                                    No
                                 </Label>
                                 </FormGroup>
                              </FormGroup>
                           </div>
                           <div className={(this.state.provider_confirm === 1 && this.state.provider_invoice_status == 4) ? "col-md-12" : "col-md-12 d-none"}>
                              <FormGroup tag="fieldset">
                                 <Label>Refund Type</Label>
                                 <FormGroup check>
                                    <Label check>
                                       <Input
                                          type="radio"
                                          name="refund_type"
                                          value={0}
                                          checked={(this.state.refund_type === 0) ? true : false}
                                          onChange={(e) => this.onChnagePConfim('refund_type', e.target.value)}
                                       />
                                    Issue Refund
                                    </Label>
                                 </FormGroup>
                                 <FormGroup check>
                                    <Label check>
                                       <Input
                                          type="radio"
                                          name="refund_type"
                                          value={1}
                                          checked={(this.state.refund_type === 1) ? true : false}
                                          onChange={(e) => this.onChnagePConfim('refund_type', e.target.value)}
                                       />
                                    IOU
                                 </Label>
                                 </FormGroup>
                              </FormGroup>
                           </div>

                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <Button
                           variant="contained"
                           className={(this.validatePconSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                           onClick={() => this.processConfirmed()}
                           disabled={!this.validatePconSubmit()}
                        >Ok</Button>
                        <Button variant="contained" className="text-white btn-danger" onClick={() => this.roviderConfirmationsClose()}>Cancel</Button>
                     </ModalFooter>
                  </Modal>

               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer }) => {

   const { nameExist, isEdit } = authUser;
   const { loading, application_details, invoiceDetails, application_plans, amountDetails, rePlanDetails, reProviderDetails, generateArea, invoicePlan, planCloseStatus } = PaymentPlanReducer;

   return { loading, application_details, invoiceDetails, application_plans, amountDetails, rePlanDetails, reProviderDetails, generateArea, invoicePlan, planCloseStatus }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice, adminPlanDetailsDownload, regeneratePlan, regeneratePlanProcess, regeneratePrintPlan, confirmedCancellation, closeCurrentPlan
})(PlanDetails);