import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import {
   Button,
   Form,
   FormGroup,
   Label,
   Input,
   FormText,
   Col,
   FormFeedback
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isObjectEmpty } from '../../validator/Validator';
import {
   creditApplicationUploadDocument, creditApplicationReview
} from 'Actions';

class uploadDocument extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         document: ''
      },
      add_err: {},
      uploadDocumentRedirect: 0,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.creditApplicationReview(this.props.match.params.id);
   }
   /*
  * Title :- callAction
  * Descrpation :- This function use for submit uploaded document
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   callAction(type, e) {
      let datafrom = new FormData();
      datafrom.append('application_id', this.props.match.params.id);
      datafrom.append('customer_id', this.props.match.params.customer_id);
      datafrom.append('imgedata', this.state.addData.document);
      this.props.creditApplicationUploadDocument(datafrom);
   }
   /*
  * Title :- validateSubmit
  * Descrpation :- This function use for check validation
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   validateSubmit() {
      return (this.state.add_err.document === '');
   }
   /*
  * Title :- componentWillReceiveProps
  * Descrpation :- This function use for check props and redirect url
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   componentWillReceiveProps(nextProps) {
      (nextProps.redirectUpload && nextProps.redirectUpload != '') ? this.setState({ uploadDocumentRedirect: nextProps.redirectUpload }) : '';
   }
   /*
  * Title :- onChnage
  * Descrpation :- This function use for set value and error in state according to user action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'document':
            value = value.target.files[0];
            if (isObjectEmpty(value)) {
               add_err[key] = "Please upload document before submitting.";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   render() {
      if (this.state.uploadDocumentRedirect == 1) {
         return (<Redirect to={`/provider/credit-applications`} />);
      }
      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsuploadCreditApplications" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">


                     <Form className="application-review-process">
                        {this.props.reviewAppDetails &&
                           <div className="table-responsive mb-40 pymt-history review-app-info">
                           <h3 className="app-review-info">Credit Application Information</h3>
                           <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Application No : <span>{this.props.reviewAppDetails[0].application_no}</span></th>
                                       <th>Approved Amount : <span>${this.props.reviewAppDetails[0].approve_amount}</span></th>
                                       <th>Credit Score : <span>{this.props.reviewAppDetails[0].score}</span></th>
                                    </tr>
                                 </thead>
                              </table>
                           </div>
                          
                        }
                        <div className="row">
                           <div className="col-md-12">
                              <FormGroup>
                                 <Label for="document">Document Information</Label>
                                 <Input
                                    type="file"
                                    name="document"
                                    id="document"
                                    accept="image/gif, image/jpeg, image/png, application/pdf"
                                    onChange={(e) => this.onChnage('document', e)}
                                 >
                                 </Input>
                                 {(this.state.add_err.document) ? <FormHelperText>{this.state.add_err.document}</FormHelperText> : ''}
                                 <FormText color="muted">
                                    Please upload all the required documents for credit application.
                                 </FormText>
                              </FormGroup>
                           </div>
                        </div>
                        <hr />


                        <div className="row">

                           <div className="col-md-12">
                              <div className="mps-submt-btn text-right">
                                 <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this)} disabled={!this.validateSubmit()}>Submit</Button>
                              </div>
                           </div>
                        </div>
                     </Form>


                  </div>


               </div>


               {this.props.loading &&
                  <RctSectionLoader />
               }        


            </RctCollapsibleCard>

         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, redirectUpload, reviewAppDetails } = creditApplication;
   return { loading, redirectUpload, reviewAppDetails }

}

export default connect(mapStateToProps, {
   creditApplicationUploadDocument, creditApplicationReview
})(uploadDocument);