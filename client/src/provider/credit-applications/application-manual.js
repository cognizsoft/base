/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';

import "react-datepicker/dist/react-datepicker.css";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { RctCard } from 'Components/RctCard/index';
import CryptoJS from 'crypto-js';


import {
   applicationFinalDetails, applicationEmail
} from 'Actions';
class applicationManual extends Component {
   state = {

   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      this.props.applicationFinalDetails(this.dec(this.props.match.params.id));
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for send mail
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.applicationEmail(type, 2);
   }
   render() {
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.creditManual" />}
               match={this.props.match}
            />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     {this.props.finalDetails &&
                        <React.Fragment>
                           <div className="customer-invoice pt-10 pb-10 pl-50 pr-50" ref={el => (this.componentRef = el)}>
                              <div className="text-center">
                                 <div className="sender-address">
                                    <div className="invoice-logo mb-30">
                                       <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid" />
                                    </div>
                                 </div>
                              </div>
                              <div>
                                 <div className="text-center">
                                    <h2><b>CREDIT APPLICATION PENDING MANUAL REVIEW.</b></h2>
                                 </div>
                                 <div className="text-left">

                                    <p><strong>Name:</strong> {this.props.finalDetails.f_name + ' ' + this.props.finalDetails.m_name + ' ' + this.props.finalDetails.l_name}</p>
                                    <p><strong>Address:</strong> {this.props.finalDetails.address1 + ' ' + this.props.finalDetails.address2}</p>
                                    <p>{this.props.finalDetails.City + ', ' + this.props.finalDetails.name + ' - ' + this.props.finalDetails.zip_code}</p>
                                    <p><strong>Phone:</strong>{this.props.finalDetails.phone_no}</p>

                                 </div>
                                 <hr />
                                 <div>
                                    <p>
                                    We want to thank you for submitting the credit application. Based on the information provided, your application is being evaluated and approval is pending manual review decision.
                              </p>
                                    <p>
                                    Application approval and assessment of line of credit is based on many criteria including your employment history, credit score, and credit history.
                              </p>
                                    <p>
                                    You will be notified of our review decision on your credit application in the next 48 hours via an email or phone call.
                              </p>
                              <p>If you have any additional questions, don’t hesitate to call us  at (919) 600-5526. Our friendly customer service associates are eager to help.</p>

                                    <br />
                                    <p>
                                       Thanks, you very much for considering Health partner for your medical needs.
                              </p>
                                    <br />
                                    <br />
                                    <p>
                                       <strong>Sincerely</strong>
                                    </p>
                                    <p>
                                       <strong>Health Partner Inc.</strong>
                                    </p>
                                 </div>
                              </div>
                           </div>
                           <div className="invoice-head text-right">
                              <ul className="list-inline">

                                 <li><a href="javascript:void(0);" onClick={this.callAction.bind(this, this.dec(this.props.match.params.id))}><i className="mr-10 ti-email"></i> Email</a></li>
                                 <li>
                                    <ReactToPrint
                                       trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                       content={() => this.componentRef}
                                    />
                                 </li>
                              </ul>
                           </div>
                        </React.Fragment>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                  </RctCard>
               </div>
            </div>


         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, finalDetails } = creditApplication;
   return { loading, finalDetails }
}

export default connect(mapStateToProps, {
   applicationFinalDetails, applicationEmail
})(applicationManual); 