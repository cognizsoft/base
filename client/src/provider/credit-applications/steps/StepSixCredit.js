/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepSixCredit = ({ addErr, addData, onChnagerovider, DatePicker, procedure_date, providerLocation, providerSpeciality }) => (

    <div className="modal-body page-form-outer text-left">
        <Form>

            <div className="row">

                <div className="col-md-4">
                    <FormGroup>
                        <Label for="provider_location">Location<span className="required-field">*</span></Label>
                        <Input
                            type="select"
                            name="provider_location"
                            id="provider_location"
                            placeholder=""
                            onChange={(e) => onChnagerovider('provider_location', e.target.value)}
                        >
                            <option value="">Select</option>
                            {providerLocation && providerLocation.map((loc, key) => (
                                <option value={loc.provider_location_id} key={key}>{loc.location_name}</option>
                            ))}

                        </Input>
                        {(addErr.provider_location != '' && addErr.provider_location !== undefined) ? <FormHelperText>{addErr.provider_location}</FormHelperText> : ''}
                    </FormGroup>
                </div>
                <div className="col-md-4">
                    <FormGroup>
                        <Label for="speciality_type">Speciality<span className="required-field">*</span></Label>
                        <Input
                            type="select"
                            name="speciality_type"
                            id="speciality_type"
                            value={addData.speciality_type}
                            onChange={(e) => onChnagerovider('speciality_type', e.target.value)}
                        >
                            <option value="">Select</option>
                            {providerSpeciality && providerSpeciality.map((spc, key) => (
                                <option value={spc.mdv_id+'-'+spc.procedure_id} key={key}>{spc.value}</option>
                            ))}
                        </Input>
                        {(addErr.speciality_type != '' && addErr.speciality_type !== undefined) ? <FormHelperText>{addErr.speciality_type}</FormHelperText> : ''}
                    </FormGroup>
                </div>
                <div className="col-md-4">
                    <FormGroup>
                        <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                        <DatePicker
                            dateFormat="MM/dd/yyyy"
                            name="procedure_date"
                            id="procedure_date"
                            selected={procedure_date}
                            placeholderText="MM/DD/YYYY"
                            autocomplete={false}
                            onChange={(e) => onChnagerovider('procedure_date', e)}
                        />
                        {(addErr.procedure_date) ? <FormHelperText>{addErr.procedure_date}</FormHelperText> : ''}
                    </FormGroup>
                </div>
                <div className="col-md-4">
                    <FormGroup>
                        <Label for="amount">Amount($)<span className="required-field">*</span></Label><br />
                        <TextField
                            type="text"
                            name="amount"
                            id="amount"
                            fullWidth
                            variant="outlined"
                            placeholder="($)"
                            error={(addErr.amount) ? true : false}
                            helperText={(addErr.amount != '') ? addErr.amount : ''}
                            onChange={(e) => onChnagerovider('amount', e.target.value)}
                        />
                    </FormGroup>
                </div>
                <div className="col-md-4">
                    <FormGroup>
                        <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                        <TextField
                            type="text"
                            name="procedure_amount"
                            id="procedure_amount"
                            fullWidth
                            variant="outlined"
                            placeholder="($)"
                            error={(addErr.procedure_amount) ? true : false}
                            helperText={(addErr.procedure_amount != '') ? addErr.procedure_amount : ''}
                            onChange={(e) => onChnagerovider('procedure_amount', e.target.value)}
                        />
                    </FormGroup>
                </div>
                <div className="col-md-4">
                    <FormGroup tag="fieldset">
                        <Label>Procedure Status</Label>
                        <FormGroup check>
                            <Label check>
                                <Input
                                    type="radio"
                                    name="procedure_status"
                                    value={1}
                                    checked={(addData.procedure_status == 1) ? true : false}
                                    onChange={(e) => onChnagerovider('procedure_status', e.target.value)}
                                />
                                Active
                                 </Label>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input
                                    type="radio"
                                    name="procedure_status"
                                    value={0}
                                    checked={(addData.procedure_status == 0) ? true : false}
                                    onChange={(e) => onChnagerovider('procedure_status', e.target.value)}
                                />
                                Inactive
                                 </Label>
                        </FormGroup>
                    </FormGroup>
                </div>
                <div className="col-md-12">
                    <FormGroup>
                        <Label for="comment">Your Comment <span className="required-field">*</span></Label><br />
                        <Input
                            type="textarea"
                            name="comment"
                            id="comment"
                            variant="outlined"
                            placeholder="Enter your comment"
                            defaultValue={addData.comment}
                            onChange={(e) => onChnagerovider('comment', e.target.value)}
                        >
                        </Input>
                        {(addErr.comment) ? <FormHelperText>{addErr.comment}</FormHelperText> : ''}
                    </FormGroup>
                </div>
            </div>
        </Form>
    </div>

);

export default StepSixCredit;