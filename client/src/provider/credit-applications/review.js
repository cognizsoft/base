/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { Form, FormGroup, Label, Input } from 'reactstrap';
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isEmpty } from '../../validator/Validator';

import {
   creditApplicationReview, creditApplicationManualActions, creditApplicationRemoveManualActions
} from 'Actions';

class ReviewApplication extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         comment: ''
      },
      add_err: {},
      uploadDocumentRedirect: 0,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.creditApplicationReview(this.props.match.params.id);
   }
   /*
   * Title :- onChnage
   * Descrpation :- This function use check on change value
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   /*
   * Title :- validateSubmit
   * Descrpation :- This function use for check validation
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   validateSubmit() {
      return (this.state.add_err.comment === '');
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for submit appication status
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.creditApplicationManualActions(this.props.match.params.id, type, this.state.addData.comment);

   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check updated props
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentWillReceiveProps(nextProps) {
      if (nextProps.manualType && nextProps.manualType != '') {
         this.props.creditApplicationRemoveManualActions();
         this.setState({ uploadDocumentRedirect: nextProps.manualType })
      }
   }
   render() {
      if (this.state.uploadDocumentRedirect == 1) {
         return (<Redirect to={`/admin/customers/customerPayment-plan/${this.props.match.params.id}`} />);
      } else if (this.state.uploadDocumentRedirect == 2) {
         return (<Redirect to={`/admin/credit-application/reject/${this.props.match.params.id}`} />);
      } else if (this.state.uploadDocumentRedirect == 3) {
         if (this.props.reviewAppDetails[0].provider_id == null) {
            return (<Redirect to={`/admin/credit-application/upload/${this.props.reviewAppDetails[0].patient_id}/${this.props.match.params.id}`} />);
         } else {
            return (<Redirect to={`/admin/credit-applications`} />);
         }
      }


      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsAddCreditApplications" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">

                     {this.props.reviewAppDetails &&
                        <Form>
                           <h4>Credit Application Information</h4>
                           <div className="row">
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Application NO:</strong> {this.props.reviewAppDetails[0].application_no}</div>
                              </div>
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Amount:</strong> ${this.props.reviewAppDetails[0].amount}</div>
                              </div>
                              <div className="col-md-4">
                                 <div className="credit-score"><strong>Credit Score :</strong> {this.props.reviewAppDetails[0].score}</div>
                              </div>
                           </div>
                           <hr />
                           <h4>Experian Risk Model</h4>
                           <div className="row">
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Model Indicator</strong></div>
                              </div>
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Score</strong></div>
                              </div>
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Evaluation</strong></div>
                              </div>
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Importance</strong></div>
                              </div>
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Code</strong></div>
                              </div>
                           </div>
                           {this.props.reviewApiRiskFactor && this.props.reviewApiRiskFactor.map((risk, idx) => (
                              <div className="row" key={idx}>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.modelIndicator}</div>
                                 </div>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.score}</div>
                                 </div>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.evaluation}</div>
                                 </div>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.importance}</div>
                                 </div>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.code}</div>
                                 </div>
                              </div>
                           ))
                           }
                           <hr />
                           <h4>HPS Risk Model</h4>
                           <div className="row">
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Weight</strong></div>
                              </div>
                              <div className="col-md-2">
                                 <div className="credit-score"><strong>Weight Type</strong></div>
                              </div>
                           </div>
                           {this.props.reviewHPSRiskFactor && this.props.reviewHPSRiskFactor.map((risk, idx) => (
                              <div className="row" key={idx}>
                                 <div className="col-md-2">
                                    <div className="credit-score">{risk.weight}</div>
                                 </div>
                                 <div className="col-md-2">
                                    <div className="credit-score">{(risk.weight_type == 1) ? '+ (Positive)' : '- (Negative)'}</div>
                                 </div>
                              </div>
                           ))
                           }

                           <hr />
                           <div className="row">
                              <hr />
                              <div className="col-md-12">
                                 <FormGroup>
                                    <Label for="comment">Your Comment <span className="required-field">*</span></Label><br />
                                    <Input
                                       type="textarea"
                                       name="comment"
                                       id="comment"
                                       variant="outlined"
                                       placeholder="Enter your comment before any action"
                                       defaultValue={this.state.addData.comment}
                                       onChange={(e) => this.onChnage('comment', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.add_err.comment) ? <FormHelperText>{this.state.add_err.comment}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-12">
                                 <div className="mps-submt-btn text-right">
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 1)} disabled={!this.validateSubmit()}>Approve</Button>
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 2)} disabled={!this.validateSubmit()}>Reject</Button>
                                    {/*<Link to={"/admin/credit-application/reject/"}><Button variant="contained" color="primary" className="text-white mr-10 mb-10" >Reject</Button></Link>*/}
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 3)} disabled={!this.validateSubmit()}>Upload Document</Button>
                                 </div>
                              </div>
                           </div>
                        </Form>
                     }

                  </div>


               </div>


               {this.props.loading &&
                  <RctSectionLoader />
               }


            </RctCollapsibleCard>

         </div>
      );
   }
}

const mapStateToProps = ({ creditApplication }) => {
   const { loading, reviewAppDetails, reviewApiRiskFactor, reviewHPSRiskFactor, manualType } = creditApplication;
   return { loading, reviewAppDetails, reviewApiRiskFactor, reviewHPSRiskFactor, manualType }

}

export default connect(mapStateToProps, {
   creditApplicationReview, creditApplicationManualActions, creditApplicationRemoveManualActions
})(ReviewApplication);