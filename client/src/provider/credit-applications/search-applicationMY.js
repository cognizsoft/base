/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import SignatureCanvas from 'react-signature-canvas'
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isSSN, isEmpty, formatSSNNumber, isDecimals, isNumeric, isLength, pointDecimals } from '../../validator/Validator';
import FormHelperText from '@material-ui/core/FormHelperText';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import ReactToPrint from 'react-to-print';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import AppConfig from 'Constants/AppConfig';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { RctCard } from 'Components/RctCard/index';

import {
   searchCustomerApplication, paymentPlan, applicationOption, getSpeciality, getDoctors, planReset, createPaymentPlan, printPlan, eamilPlan
} from 'Actions';
class searchApplication extends Component {
   state = {
      currentModule: 26,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      existApplication: {
         exist_first_name: "",
         exist_last_name: "",
         exist_dob: "",
         exist_ssn: "",
         application_no: "",
      },
      existError: {},
      startDateExist: "",
      changeURL: 0,
      showSearchPlan: false,
      current_application_id: null,
      generatePlan: false,
      selectedPlan: null,
      remainingAmount: 0.00,
      procedure_date: '',
      loan: {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         doctor: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         expiry_date: "",

         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_ac: "",
         account_name: "",
         account_type: "",

         name_of_borrower: "",
         agreement_date: "",

         loan_type: 1,
         monthly_amount: "",
         signature: "",
      },
      loanError: {},
      fullView: false,
      agreementView: false,
      expiry_date: '',
      agreement_date: '',
      customerPlanView: true,
      providerPlanView: false,
      customerSignURL: null,
      witnessSignURL: null,
   }
   customerSigPad = {}
   witnessSigPad = {}
   clearCustomer = () => {
      this.customerSigPad.clear();
      let { loan, loanError } = this.state;
      loanError['customerSignature'] = "Customer Signature required.";
      loan['customerSignature'] = "";
      this.setState({
         customerSignURL: null,
         loan: loan,
         loanError: loanError
      })
   }
   trimCustomer = () => {
      let { loan, loanError } = this.state;
      loanError['customerSignature'] = "";
      loan['customerSignature'] = this.customerSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.setState({
         customerSignURL: this.customerSigPad.getTrimmedCanvas().toDataURL('image/png'),
         loan: loan,
         loanError: loanError
      })
   }
   clearWitness = () => {
      this.witnessSigPad.clear();
      let { loan, loanError } = this.state;
      loanError['witnessSignature'] = "Witness Signature required.";
      loan['witnessSignature'] = "";
      this.setState({
         witnessSignURL: null,
         loan: loan,
         loanError: loanError
      })
   }
   trimWitness = () => {
      let { loan, loanError } = this.state;
      loanError['witnessSignature'] = "";
      loan['witnessSignature'] = this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.setState({
         witnessSignURL: this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png'),
         loan: loan,
         loanError: loanError
      })
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.applicationOption();
      this.props.planReset(1);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   /*
   * Title :- onChnageExist
   * Descrpation :- This function use for onchnage
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnageExist(key, value) {
      let { existError } = this.state;
      switch (key) {
         case 'exist_first_name':
            if (isEmpty(value)) {
               existError[key] = "First Name can't be blank";
            } else {
               existError[key] = '';
            }

            break;
         case 'exist_last_name':
            if (isEmpty(value)) {
               existError[key] = "Last Name can't be blank";
            } else {
               existError[key] = '';
            }

            break;
         case 'exist_ssn':
            if (isEmpty(value)) {
               existError[key] = "SSN can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               existError[key] = "SSN not valid";
            } else {
               value = formatSSNNumber(value)
               existError[key] = '';
            }
            //delete existError.application_no;
            break;
         case 'exist_dob':
            if (value == null) {
               existError[key] = "Select Date of Birth";
               this.setState({ startDateExist: '' })
            } else {
               this.setState({ startDateExist: value })
               value = moment(value).format('YYYY-MM-DD');
               existError[key] = '';
            }

            break;
         case 'application_no':
            if (isEmpty(value)) {
               existError[key] = "Application number can't be blank";
            } else {
               existError[key] = '';
            }
            //delete existError.exist_ssn;
            break;
      }

      this.setState({
         existApplication: {
            ...this.state.existApplication,
            [key]: value
         }
      });
      this.setState({ existError: existError });
   }
   /*
   * Title :- searchCustomer
   * Descrpation :- This function use for search custoemr details
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerSubmit() {
      this.props.searchCustomerApplication(this.state.existApplication);
      this.props.planReset(0);
      const loan = {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         doctor: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         expiry_date: "",
         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_ac: "",
         account_name: "",
         account_type: "",
         name_of_borrower: "",
         agreement_date: "",
         loan_type: 1,
         monthly_amount: "",
      }
      const loanError = {}
      this.setState({ showSearchPlan: false, current_application_id: null, generatePlan: false, loan: loan, procedure_date: '', selectedPlan: null, loanError: loanError });
   }
   /*
   * Title :- searchCustomerReset
   * Descrpation :- This function use for reset page
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   searchCustomerReset() {
      /*let { existError } = this.state;
      existError['exist_first_name'] = "";
      existError['exist_last_name'] = "";
      existError['exist_ssn'] = "";
      existError['exist_dob'] = "";
      existError['application_no'] = "";*/

      this.props.planReset(1);
      const loan = {
         loan_amount: "",
         procedure_date: "",
         procedure_amount: "",
         provider_location: "",
         doctor: "",
         id_number: "",
         id_name: "",
         agree_with: 0,
         expiry_date: "",
         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_ac: "",
         account_name: "",
         account_type: "",
         name_of_borrower: "",
         agreement_date: "",
         loan_type: 1,
         monthly_amount: "",
      }
      const loanError = {}
      this.setState({
         existApplication: {
            exist_first_name: "",
            exist_last_name: "",
            exist_dob: "",
            exist_ssn: "",
            application_no: "",
         },
         existError: {},
         startDateExist: "",
         showSearchPlan: false,
         current_application_id: null,
         generatePlan: false,
         loan: loan, procedure_date: '',
         selectedPlan: null,
         loanError: loanError,
      }, () => {
         //this.props.removeSearchCustomer();
      })
   }
   /*
   * Title :- validateSearch
   * Descrpation :- This function use for validate data
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   validateSearch() {
      return (
         (
            this.state.existError.exist_first_name === '' &&
            this.state.existError.exist_last_name === '' &&
            this.state.existError.exist_dob === ''
         ) ||
         (
            this.state.existError.exist_ssn === ''
         ) ||
         (
            this.state.existError.application_no === ''
         )


      )
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   searchPlan(patient_id, application_id, type, remainingAmount, e) {
      this.props.planReset(0);

      if (type == 1) {
         var filterBank = this.props.searchCustomer.filter(x => x.application_id == application_id);
         const loan = {
            loan_amount: "",
            procedure_date: "",
            procedure_amount: "",
            provider_location: "",
            doctor: "",
            id_number: "",
            id_name: "",
            agree_with: 0,
            expiry_date: "",
            bank_name: filterBank[0].bank_name,
            bank_address: filterBank[0].bank_address,
            rounting_no: filterBank[0].rounting_no,
            bank_ac: filterBank[0].account_number,
            account_name: filterBank[0].account_name,
            account_type: filterBank[0].account_type,
            name_of_borrower: "",
            agreement_date: "",
            loan_type: 1,
            monthly_amount: "",
         }
         const loanError = {
            /* bank_name:(filterBank[0].bank_name != '')?"":"Bank name can't be blank",
             rounting_no:(filterBank[0].rounting_no != '')?"":"Routing number can't be blank",
             bank_ac:(filterBank[0].account_number != '')?"":"Bank A/C can't be blank",
             account_name:(filterBank[0].account_name != '')?"":"Name on account can't be blank",
             account_type:(filterBank[0].account_type != '')?"":"Bank A/C type can't be blank",*/
            bank_name: (filterBank[0].bank_name != '') ? "" : "",
            rounting_no: (filterBank[0].rounting_no != '') ? "" : "",
            bank_ac: (filterBank[0].account_number != '') ? "" : "",
            account_name: (filterBank[0].account_name != '') ? "" : "",
            account_type: (filterBank[0].account_type != '') ? "" : "",
         }
         this.setState({ showSearchPlan: true, current_application_id: application_id, generatePlan: (type) ? true : false, loan: loan, procedure_date: '', selectedPlan: null, remainingAmount: remainingAmount, loanError: loanError });
      } else {
         const loan = {
            loan_amount: "",
            procedure_date: "",
            procedure_amount: "",
            provider_location: "",
            doctor: "",
            id_number: "",
            id_name: "",
            agree_with: 0,
            expiry_date: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_ac: "",
            account_name: "",
            account_type: "",
            name_of_borrower: "",
            agreement_date: "",
            loan_type: 1,
            monthly_amount: "",
         }
         const loanError = {}
         this.setState({ showSearchPlan: true, current_application_id: application_id, generatePlan: (type) ? true : false, loan: loan, procedure_date: '', selectedPlan: null, remainingAmount: remainingAmount, loanError: loanError });
      }




      //return (
      // this.state.loanError.loan_amount === ''
      //)
   }
   /*
   * Title :- onChnagePlanExist
   * Descrpation :- This function use for plan onchnage 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 9 Aug,2019
   */
   onChnagePlanExist(key, value) {
      let { loanError } = this.state;
      switch (key) {
         case 'monthly_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Monthly amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Monthly amount not valid";
            } else {
               loanError[key] = "";
            }
            break;
         case 'loan_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Loan amount not valid";
            } else if (value <= 0) {
               loanError[key] = "The loan amount should be greater than $0";
            } else if (parseFloat(this.state.remainingAmount) < parseFloat(value)) {
               loanError[key] = '"Loan amount exceeds the available credit" for override call HPS at (919) 600-5526';
            } else if (this.state.loan.procedure_amount !== '' && parseFloat(this.state.loan.procedure_amount) < parseFloat(value)) {
               loanError[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.loan.procedure_amount !== '' && parseFloat(this.state.loan.procedure_amount) >= parseFloat(value)) {
               loanError[key] = "";
               loanError['procedure_amount'] = "";
            } else {
               loanError[key] = "";
            }
            break;
         case 'procedure_date':
            if (value == null) {
               loanError[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               loanError[key] = '';
            }
            break;
         case 'procedure_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               loanError[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               loanError[key] = "Procedure amount not valid";
            } else if (value < 100 || value > 250000) {
               loanError[key] = "The Procedure amount amount should be between $100 - $250,000";
            }
            else if (this.state.loan.loan_amount !== '' && parseFloat(this.state.loan.loan_amount) > parseFloat(value)) {
               loanError[key] = "Procedure amount greater than or equal to Loan amount";
            } else if (this.state.loan.loan_amount !== '' && parseFloat(this.state.loan.loan_amount) <= parseFloat(value)) {
               loanError[key] = "";
               loanError['loan_amount'] = "";
            } else {
               loanError[key] = '';
            }
            break;
         case 'provider_location':
            if (isEmpty(value)) {
               loanError[key] = "Location can't be blank";
            } else {
               //this.props.getSpeciality(value);
               this.props.getDoctors(value);
               loanError[key] = '';
            }
            loanError['speciality_type'] = "Speciality can't be blank";
            break;
         case 'doctor':
            if (isEmpty(value)) {
               loanError[key] = "Doctor can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'agree_with':
            value = (this.state.loan.agree_with) ? 0 : 1;
            break;
         case 'id_number':
            if (isEmpty(value)) {
               loanError[key] = "ID number can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'id_name':
            if (isEmpty(value)) {
               loanError[key] = "ID type can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
            break;
         case 'expiry_date':
            if (value == null) {
               this.setState({ expiry_date: '' })
            } else {
               this.setState({ expiry_date: value })
               value = moment(value).format('YYYY-MM-DD');
               loanError[key] = '';
            }
            break;
         case 'bank_name':
            if (isEmpty(value)) {
               //loanError[key] = "Bank name can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'bank_address':
            if (isEmpty(value)) {
               //loanError[key] = "Bank address can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'rounting_no':
            if (isEmpty(value)) {
               //loanError[key] = "Routing number can't be blank";
            } else if (isNumeric(value) && !isEmpty(value)) {
               loanError[key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 9, trim: true }) && !isEmpty(value)) {
               loanError[key] = "Please enter 9 digit valid routing number";
            } else {
               loanError[key] = '';
            }
            break;
         case 'bank_ac':
            if (isEmpty(value)) {
               //loanError[key] = "Bank A/C can't be blank";
            } else if (isNumeric(value) && !isEmpty(value)) {
               loanError[key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 8, trim: true }) && !isEmpty(value)) {
               loanError[key] = "Please enter correct bank account number";
            } else {
               loanError[key] = '';
            }
            break;
         case 'account_name':
            if (isEmpty(value)) {
               //loanError[key] = "Name on account can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'account_type':
            if (isEmpty(value)) {
               //loanError[key] = "Bank A/C type can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'name_of_borrower':
            if (isEmpty(value)) {
               loanError[key] = "Name of Borrower can't be blank";
            } else {
               loanError[key] = '';
            }
            break;
         case 'agreement_date':
            if (value == null) {
               this.setState({ agreement_date: '' })
               loanError[key] = "Agreement date can't be blank";
            } else {
               this.setState({ agreement_date: value })
               value = moment(value).format('YYYY-MM-DD HH:mm:ss');
               loanError[key] = '';
            }
            break;
         /*case 'speciality_type':
            if (isEmpty(value)) {
               loanError[key] = "Speciality can't be blank";
            } else {
               loanError[key] = '';
            }
            break;*/
      }

      this.setState({
         loan: {
            ...this.state.loan,
            [key]: value
         }
      });
      this.setState({ loanError: loanError });
   }
   /*
   * Title :- validateSearchPlan
   * Descrpation :- This function use for validate data for search plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 19,2019
   */
   validateSearchPlan() {
      if (this.state.generatePlan) {
         if (this.state.loan_type == 0) {
            return (
               this.state.loanError.loan_amount === '' &&
               this.state.loanError.procedure_date === '' &&
               this.state.loanError.procedure_amount === '' &&
               this.state.loanError.provider_location === '' &&
               this.state.loanError.doctor === '' &&
               this.state.loanError.monthly_amount === ''
               //this.state.loanError.bank_name === '' &&
               //this.state.loanError.rounting_no === '' &&
               //this.state.loanError.bank_ac === '' &&
               //this.state.loanError.account_name === '' &&
               //this.state.loanError.account_type === ''
            )
         } else {
            return (
               this.state.loanError.loan_amount === '' &&
               this.state.loanError.procedure_date === '' &&
               this.state.loanError.procedure_amount === '' &&
               this.state.loanError.provider_location === '' &&
               this.state.loanError.doctor === ''
               //this.state.loanError.bank_name === '' &&
               //this.state.loanError.rounting_no === '' &&
               //this.state.loanError.bank_ac === '' &&
               //this.state.loanError.account_name === '' &&
               //this.state.loanError.account_type === ''
            )
         }
      } else {
         if (this.state.loan_type == 0) {
            return (
               this.state.loanError.loan_amount === '' &&
               this.state.loanError.procedure_date === '' &&
               this.state.loanError.procedure_amount === '' &&
               this.state.loanError.monthly_amount === ''
            )
         } else {
            return (
               this.state.loanError.loan_amount === '' &&
               this.state.loanError.procedure_date === '' &&
               this.state.loanError.procedure_amount === ''
            )
         }
      }
   }
   /*
   * Title :- searchPlan
   * Descrpation :- This function use for search plan according to customer
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Aug 13,2019
   */
   viewPlan() {
      this.props.paymentPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.loan.loan_type, this.state.loan.monthly_amount);
      //this.props.searchCustomerApplication(this.state.existApplication);
   }
   printPlan() {

      this.props.printPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   emailPlan() {
      this.props.eamilPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.selectedPlan);
   }
   /*
   * Tile :- fullViewPlan
   * Descrpation :- This function use for full plan view
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- Oct 15,2019
   */
   fullViewPlan() {
      this.setState({ fullView: true })
      //this.props.fullViewPlan(this.state.current_application_id, this.state.loan.loan_amount, this.state.selectedPlan);
   }

   customerPlan() {
      this.setState({ customerPlanView: true, providerPlanView: false })
   }
   providerPlan() {
      this.setState({ providerPlanView: true, customerPlanView: false })
   }

   goBack() {
      this.setState({ fullView: false, agreementView: false })
   }
   validatePlan() {
      return (
         this.state.loanError.loan_amount === '' &&
         this.state.loanError.procedure_date === '' &&
         this.state.loanError.procedure_amount === '' &&
         this.state.loanError.provider_location === '' &&
         this.state.loanError.doctor === '' &&
         this.state.selectedPlan !== null
         /*this.state.add_err.provider_location === '' &&
         //this.state.add_err.speciality_type === '' &&
         this.state.add_err.procedure_date === '' &&
         this.state.add_err.amount === '' &&
         this.state.add_err.procedure_amount === '' &&
         this.state.add_err.comment === ''*/
      )
   }
   checkboxAction = (id, e) => {
      this.setState({ selectedPlan: (e.target.checked) ? id : null });
   }
   /*
   * Title :- processPlan
   * Descrpation :- This function use for process selected plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   processPlan(application_id, plan_id) {
      this.setState({ agreementView: true });
      //this.props.createPaymentPlan(application_id, plan_id, this.state.loan);
   }

   processPlanSubmit(application_id, plan_id) {

      // return false;
      this.props.createPaymentPlan(application_id, plan_id, this.state.loan);
   }
   validateFinal() {
      console.log(this.state.loanError)
      return (
         this.state.loanError.id_number === '' &&
         this.state.loan.agree_with === 1 &&
         this.state.loanError.id_name === '' &&
         this.state.loanError.name_of_borrower === '' &&
         this.state.loanError.agreement_date === '' &&
         this.state.loanError.witnessSignature === '' &&
         this.state.loanError.customerSignature === ''
      )
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check state updated or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentWillReceiveProps(nextProps) {
      (nextProps.rediretPlanURL != '') ? this.setState({ changeURL: nextProps.rediretPlanURL }) : '';
   }
   render() {
      let { customerPlanView, providerPlanView } = this.state;
      if (this.state.changeURL === 1) {
         //return (<Redirect to={`/provider/customers/agreement-plan/${this.props.paymentPlanID}`} />);
         return (<Redirect to={`/provider/credit-applications`} />);
      }
      var paymentPlanAllow = this.props.paymentPlanAllow;
      var min_term = 0;
      var min_int = 0;
      if (paymentPlanAllow) {

         var mterm = paymentPlanAllow.map(d => d.term_month);
         min_term = Math.min(...mterm)

         var mint = paymentPlanAllow.map(d => d.interest_rate);
         min_int = Math.min(...mint)

      }
      //console.log(this.state.loan)
      return (
         <div className="user-management">

            <Helmet>
               <title>Health Partner | Users Management</title>
               <meta name="description" content="Reactify Widgets" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.searchApplication" />}
               match={this.props.match}
            />

            {this.state.currentPermision.view != 1 &&
               <RctCollapsibleCard heading="" fullBlock >
                  <div className="col-sm-12 p-20">
                     You are not authorized to access this page.
               </div>
               </RctCollapsibleCard>
            }

            {this.state.currentPermision.view == 1 &&
               <RctCollapsibleCard heading="" fullBlock >
                  <div className={(this.state.fullView || this.state.agreementView) ? 'modal-body page-form-outer text-left d-none' : 'modal-body page-form-outer text-left'} >
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_first_name">First Name</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_first_name"
                                 id="exist_first_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="First Name"
                                 value={this.state.existApplication.exist_first_name}
                                 error={(this.state.existError.exist_first_name) ? true : false}
                                 helperText={this.state.existError.exist_first_name}
                                 onChange={(e) => this.onChnageExist('exist_first_name', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_last_name">Last Name</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_last_name"
                                 id="exist_last_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Last Name"
                                 value={this.state.existApplication.exist_last_name}
                                 error={(this.state.existError.exist_last_name) ? true : false}
                                 helperText={this.state.existError.exist_last_name}
                                 onChange={(e) => this.onChnageExist('exist_last_name', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_dob">Date of Birth</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="exist_dob"
                                 id="exist_dob"
                                 selected={this.state.startDateExist}
                                 inputProps={{ maxLength: 10 }}
                                 placeholderText="MM/DD/YYYY"
                                 autocomplete={false}
                                 onChange={(e) => this.onChnageExist('exist_dob', e)}
                              />
                              {(this.state.existError.exist_dob) ? <FormHelperText>{this.state.existError.exist_dob}</FormHelperText> : ''}
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="exist_ssn">SSN / Tax ID</Label><br />
                              <TextField
                                 type="text"
                                 name="exist_ssn"
                                 id="exist_ssn"
                                 fullWidth
                                 variant="outlined"
                                 inputProps={{ maxLength: 11 }}
                                 placeholder="Social Security Number"
                                 value={this.state.existApplication.exist_ssn}
                                 error={(this.state.existError.exist_ssn) ? true : false}
                                 helperText={this.state.existError.exist_ssn}
                                 onChange={(e) => this.onChnageExist('exist_ssn', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="application_no">Application No</Label><br />
                              <TextField
                                 type="text"
                                 name="application_no"
                                 id="application_no"
                                 fullWidth
                                 variant="outlined"
                                 inputProps={{ maxLength: 11 }}
                                 placeholder="Application Number"
                                 value={this.state.existApplication.application_no}
                                 error={(this.state.existError.application_no) ? true : false}
                                 helperText={this.state.existError.application_no}
                                 onChange={(e) => this.onChnageExist('application_no', e.target.value)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="mt-30">
                              <Button onClick={this.searchCustomerSubmit.bind(this)} disabled={!this.validateSearch()} variant="contained" color="primary" className="text-white pt-10 pb-10 mr-5" >
                                 Search
                              </Button>
                              <Button onClick={this.searchCustomerReset.bind(this)} variant="contained" color="primary" className="text-white pt-10 pb-10" >
                                 Reset
                              </Button>
                           </div>
                        </div>
                     </div>
                     <div className="row">
                        <div className="col-md-12 text-danger">
                           Enter either First name, Last Name, DoB or SSN / Tax ID or Application Number to view patient's line credit
                        </div>
                     </div>
                     {this.props.searchCustomer &&
                        <div className="table-responsive mb-20 pymt-history">
                           <h2 className="text-center mb-10">Customer Detail</h2>
                           <div className="table-responsive pb-10">
                              <table className="table m-0 table-borderless admin-account-view-invoice">
                                 <thead>
                                    <tr>
                                       <th>A/C Number</th>
                                       <th>App No</th>
                                       <th>Full Name</th>
                                       <th>Address1</th>
                                       <th>City</th>
                                       <th>State</th>
                                       <th>Zip</th>
                                       <th>Phone</th>
                                       <th>Expiry Date</th>
                                       {/*<th>Approved Amt</th>*/}
                                       <th>Available Credit</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {this.props.searchCustomer && this.props.searchCustomer.map((cusomer, idx) => (
                                       <tr key={idx}>
                                          <td>{cusomer.patient_ac}</td>
                                          <td>{cusomer.application_no}</td>
                                          <td>{cusomer.first_name + ' ' + cusomer.middle_name + ' ' + cusomer.last_name}</td>
                                          <td>{cusomer.address1}</td>
                                          <td>{cusomer.City}</td>
                                          <td>{cusomer.state_name}</td>
                                          <td>{cusomer.zip_code}</td>
                                          <td>{cusomer.peimary_phone}</td>
                                          <td>{cusomer.expiry_date}</td>
                                          {/*<td>${(cusomer.approve_amount > 0) ? parseFloat(cusomer.approve_amount).toFixed(2) : '0.00'}</td>*/}
                                          <td>${(cusomer.remaining_amount > 0) ? parseFloat(cusomer.remaining_amount).toFixed(2) : '0.00'}</td>
                                          <td>
                                             {/*<Link to={`/provider/customers/customerPayment-plan/${this.props.searchCustomer.patient_id}/${this.props.searchCustomer.application_id}`} title="Process for next step"><i className="material-icons mr-10">visibility</i></Link>*/}
                                             <span className="list-action">
                                                {
                                                   //(moment(cusomer.expiry_date).format('MM/DD/YYYY') > moment().format('MM/DD/YYYY')) ?
                                                   (moment(cusomer.expiry_date, "MM/DD/YYYY").isAfter(Date.now())) ?
                                                      (cusomer.remaining_amount > 0) ?
                                                         <React.Fragment>
                                                            <a href="javascript:void(0)" onClick={this.searchPlan.bind(this, cusomer.patient_id, cusomer.application_id, 0, cusomer.remaining_amount.toFixed(2))} title="View Plan Details"><i className="ti-eye mr-10"></i></a>
                                                            <a href="javascript:void(0)" onClick={this.searchPlan.bind(this, cusomer.patient_id, cusomer.application_id, 1, cusomer.remaining_amount.toFixed(2))} title="Generate Payment Plan"><i className="ti-money mr-10"></i></a>
                                                         </React.Fragment>
                                                         :
                                                         <a href="javascript:void(0)" title="Remaining amount not sufficient"><i className="ti-na mr-10"></i></a>
                                                      :
                                                      <a href="javascript:void(0)" title="Application expired please contact your system administrator"><i className="ti-na mr-10"></i></a>

                                                }
                                             </span>
                                          </td>
                                       </tr>
                                    ))}

                                 </tbody>
                              </table>

                           </div>
                        </div>
                     }
                     {this.state.showSearchPlan &&
                        <React.Fragment >
                           <div className="payment-plan-generator">
                              <div className="table-responsive pymt-history select-customer-plan">
                                 <h2 className="text-center">View Payment Plan</h2>
                              </div>
                              {this.state.generatePlan &&
                                 <div>
                                    <div className="row">
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="provider_location">Location<span className="required-field">*</span></Label>
                                             <Input
                                                type="select"
                                                name="provider_location"
                                                id="provider_location"
                                                placeholder=""
                                                onChange={(e) => this.onChnagePlanExist('provider_location', e.target.value)}
                                             >
                                                <option value="">Select</option>
                                                {this.props.providerLocation && this.props.providerLocation.map((loc, key) => (
                                                   <option value={loc.provider_location_id} key={key}>{loc.location_name}</option>
                                                ))}

                                             </Input>
                                             {(this.state.loanError.provider_location != '' && this.state.loanError.provider_location !== undefined) ? <FormHelperText>{this.state.loanError.provider_location}</FormHelperText> : ''}
                                          </FormGroup>
                                       </div>
                                       {/*<div className="col-md-4">
                                    <FormGroup>
                                       <Label for="speciality_type">Speciality<span className="required-field">*</span></Label>
                                       <Input
                                          type="select"
                                          name="speciality_type"
                                          id="speciality_type"
                                          onChange={(e) => this.onChnagePlanExist('speciality_type', e.target.value)}
                                       >
                                          <option value="">Select</option>
                                          {this.props.providerSpeciality && this.props.providerSpeciality.map((spc, key) => (
                                             <option value={spc.mdv_id + '-' + spc.procedure_id} key={key}>{spc.value}</option>
                                          ))}
                                       </Input>
                                       {(this.state.loanError.speciality_type != '' && this.state.loanError.speciality_type !== undefined) ? <FormHelperText>{this.state.loanError.speciality_type}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>*/}
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="speciality_type">Doctor<span className="required-field">*</span></Label>
                                             <Input
                                                type="select"
                                                name="doctor"
                                                id="doctor"
                                                onChange={(e) => this.onChnagePlanExist('doctor', e.target.value)}
                                             >
                                                <option value="">Select</option>
                                                {this.props.providerDoctors && this.props.providerDoctors.map((doc, key) => (
                                                   <option value={doc.id} key={key}>{doc.name}</option>
                                                ))}
                                             </Input>
                                             {(this.state.loanError.doctor != '' && this.state.loanError.doctor !== undefined) ? <FormHelperText>{this.state.loanError.speciality_type}</FormHelperText> : ''}
                                          </FormGroup>
                                       </div>
                                    </div>


                                    <div className="row">
                                       <h2 className="p-10 w-100">Customer Bank Details</h2>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="bank_name">Bank Name</Label><br />
                                             <TextField
                                                type="text"
                                                name="bank_name"
                                                id="bank_name"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Bank Name"
                                                value={(this.state.loan.bank_name != '') ? this.state.loan.bank_name : ''}
                                                error={(this.state.loanError.bank_name) ? true : false}
                                                helperText={(this.state.loanError.bank_name != '') ? this.state.loanError.bank_name : ''}
                                                onChange={(e) => this.onChnagePlanExist('bank_name', e.target.value)}

                                             />
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="bank_address">Bank Address</Label><br />
                                             <TextField
                                                type="text"
                                                name="bank_address"
                                                id="bank_address"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Bank Address"
                                                value={(this.state.loan.bank_address != '') ? this.state.loan.bank_address : ''}
                                                error={(this.state.loanError.bank_address) ? true : false}
                                                helperText={(this.state.loanError.bank_address != '') ? this.state.loanError.bank_address : ''}
                                                onChange={(e) => this.onChnagePlanExist('bank_address', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="rounting_no">Routing Number</Label><br />
                                             <TextField
                                                type="text"
                                                name="rounting_no"
                                                id="rounting_no"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Routing Number"
                                                value={(this.state.loan.rounting_no != '') ? this.state.loan.rounting_no : ''}
                                                error={(this.state.loanError.rounting_no) ? true : false}
                                                helperText={(this.state.loanError.rounting_no != '') ? this.state.loanError.rounting_no : ''}
                                                onChange={(e) => this.onChnagePlanExist('rounting_no', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="bank_ac">Bank A/C#</Label><br />
                                             <TextField
                                                type="text"
                                                name="bank_ac"
                                                id="bank_ac"
                                                fullWidth
                                                variant="outlined"
                                                inputProps={{ maxLength: 17 }}
                                                placeholder="Bank A/C#"
                                                value={(this.state.loan.bank_ac != '') ? this.state.loan.bank_ac : ''}
                                                error={(this.state.loanError.bank_ac) ? true : false}
                                                helperText={(this.state.loanError.bank_ac != '') ? this.state.loanError.bank_ac : ''}
                                                onChange={(e) => this.onChnagePlanExist('bank_ac', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="account_name">Name on Account</Label><br />
                                             <TextField
                                                type="text"
                                                name="account_name"
                                                id="account_name"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Name on Account"
                                                value={(this.state.loan.account_name != '') ? this.state.loan.account_name : ''}
                                                error={(this.state.loanError.account_name) ? true : false}
                                                helperText={(this.state.loanError.account_name != '') ? this.state.loanError.account_name : ''}
                                                onChange={(e) => this.onChnagePlanExist('account_name', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>
                                       <div className="col-md-3">
                                          <FormGroup>
                                             <Label for="account_type">Bank A/C Type</Label>
                                             <Input
                                                type="select"
                                                name="account_type"
                                                id="account_type"
                                                placeholder=""
                                                value={this.state.loan.account_type}
                                                onChange={(e) => this.onChnagePlanExist('account_type', e.target.value)}
                                             >
                                                <option value="">Select</option>
                                                {this.props.bankType && this.props.bankType.map((bank, key) => (
                                                   <option value={bank.mdv_id} key={key}>{bank.value}</option>
                                                ))}
                                             </Input>
                                             {(this.state.loanError.account_type != '' && this.state.loanError.account_type !== undefined) ? <FormHelperText>{this.state.loanError.account_type}</FormHelperText> : ''}
                                          </FormGroup>
                                       </div>
                                    </div>
                                 </div>
                              }
                              <div className="row" >
                                 <h2 className="p-0 pl-15 pb-10 m-0 w-100">Loan Info</h2>
                                 <div className="col-md-3">
                                    <FormGroup tag="fieldset">
                                       <Label for="Status">Loan Type<span className="required-field">*</span></Label>
                                       <FormGroup check>
                                          <Label check>
                                             <Input
                                                type="radio"
                                                name="loan_type"
                                                value={1}
                                                checked={(this.state.loan.loan_type == 1) ? true : false}
                                                onChange={(e) => this.onChnagePlanExist('loan_type', e.target.value)}
                                             />
                                             Fixed term (months)
                                          </Label>
                                       </FormGroup>
                                       <FormGroup check>
                                          <Label check>
                                             <Input
                                                type="radio"
                                                name="loan_type"
                                                value={0}
                                                checked={(this.state.loan.loan_type == 0) ? true : false}
                                                onChange={(e) => this.onChnagePlanExist('loan_type', e.target.value)}
                                             />
                                             Fixed amount(monthly)
                                          </Label>
                                       </FormGroup>
                                    </FormGroup>
                                 </div>
                                 <div className={(this.state.loan.loan_type == 1) ? "col-md-3 d-none" : "col-md-3"}>
                                    <FormGroup>
                                       <Label for="procedure_amount">Monthly Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="monthly_amount"
                                          id="monthly_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="($)"
                                          value={this.state.loan.monthly_amount}
                                          error={(this.state.loanError.monthly_amount) ? true : false}
                                          helperText={(this.state.loanError.monthly_amount != '') ? this.state.loanError.monthly_amount : ''}
                                          onChange={(e) => this.onChnagePlanExist('monthly_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-3">
                                    <FormGroup>
                                       <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="procedure_amount"
                                          id="procedure_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="($)"
                                          value={this.state.loan.procedure_amount}
                                          error={(this.state.loanError.procedure_amount) ? true : false}
                                          helperText={(this.state.loanError.procedure_amount != '') ? this.state.loanError.procedure_amount : ''}
                                          onChange={(e) => this.onChnagePlanExist('procedure_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-3">
                                    <FormGroup>
                                       <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                                       <DatePicker
                                          dateFormat="MM/dd/yyyy"
                                          name="procedure_date"
                                          id="procedure_date"
                                          selected={this.state.procedure_date}
                                          placeholderText="MM/DD/YYYY"
                                          autocomplete={false}
                                          onChange={(e) => this.onChnagePlanExist('procedure_date', e)}
                                       />
                                       {(this.state.loanError.procedure_date) ? <FormHelperText>{this.state.loanError.procedure_date}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-3">
                                    <FormGroup>
                                       <Label for="loan_amount">Enter Loan Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="loan_amount"
                                          id="loan_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Enter Loan Amount"
                                          value={this.state.loan.loan_amount}
                                          error={(this.state.loanError.loan_amount) ? true : false}
                                          helperText={this.state.loanError.loan_amount}
                                          onChange={(e) => this.onChnagePlanExist('loan_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-3">
                                    {(this.state.generatePlan) ?
                                       <div className="mt-30">
                                          <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5 pt-10 pr-20 pb-10 pl-20" >
                                             Generate Plan
                                    </Button>
                                       </div>
                                       :
                                       <div className="mt-30">
                                          <Button onClick={this.viewPlan.bind(this)} disabled={!this.validateSearchPlan()} variant="contained" color="primary" className="text-white mr-10 mb-5 pt-10 pb-10" >
                                             View Plan
                                       </Button>
                                          {/*<Button onClick={this.printPlan.bind(this)} disabled={(this.state.selectedPlan == null) ? true : false} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                          Print Plan
                                       </Button>*/}
                                          <Button onClick={this.fullViewPlan.bind(this)} disabled={(this.state.selectedPlan == null) ? true : false} variant="contained" color="primary" className="text-white mr-10 mb-5 pt-10 pb-10" >
                                             view & print
                                       </Button>
                                       </div>

                                    }
                                 </div>
                              </div>
                           </div>
                        </React.Fragment>
                     }
                     {paymentPlanAllow &&
                        <div className="w-100 price-list user-permission payment-plan-generator p-0 d-table m-0" >

                           <div className="plan-select-tab-container mb-0">
                              <div className="plan-tab row m-0 text-center">
                                 <span className={(customerPlanView) ? 'p-customer col-md-6 selected-tab' : 'p-customer col-md-6'} onClick={this.customerPlan.bind(this)}>Customer Plan</span>
                                 <span className={(providerPlanView) ? 'p-provider col-md-6 selected-tab' : 'p-provider col-md-6'} onClick={this.providerPlan.bind(this)}>Provider Plan</span>
                              </div>
                           </div>

                           <div className={(providerPlanView) ? 'row row-eq-height pt-30 provider-tab-section w-100 m-0' : 'row row-eq-height pt-30 customer-tab-section w-100 m-0'}>

                              {customerPlanView && paymentPlanAllow.map((plan, idx) => (

                                 <div className={(plan.term_month == min_term) ? 'col-md-4 recommended-plan customer-plans' : 'col-md-4 customer-plans'} key={idx} >
                                    <div className="price-plan plan1 rct-block">
                                       <div className="header pb-0 pt-5">

                                          <h2 className="pricing-title text-center">
                                             <span className="plan_term">{plan.term_month} months Plan</span>{plan.term_month == min_term && <span className="recommanded-text">Recommended</span>}
                                             <label className="check-container">
                                                <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                                <span className="checkmark"></span>
                                             </label>
                                          </h2>

                                          <div className="description-text">

                                             <div className="p-amt">Principal Amount: <span className="">${parseFloat(this.props.applicationDetails[0].amount).toFixed(2)}</span></div>

                                             <div className="apr">APR: <span className="">{(plan.interest_rate) ? parseFloat(plan.interest_rate).toFixed(2) : '0.00'}%</span></div>

                                          </div>
                                       </div>
                                       <div className="content ">
                                          <div className="page-form-outer select-plan-detail">

                                             <div className="price-list table-responsive">
                                                <table className="table table-bordered table-striped table-hover">
                                                   <thead>
                                                      <tr className="table-head">
                                                         <th>Payment#</th>
                                                         <th>Due Date</th>
                                                         <th>Payment Amt</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {
                                                         plan.monthlyPlan.map((month, idm) => (

                                                            <tr key={idm}>
                                                               <td>{idm + 1}</td>
                                                               <td>{month.next_month}</td>
                                                               <td>${month.perMonth}</td>
                                                            </tr>
                                                         ))
                                                      }



                                                   </tbody>
                                                </table>

                                             </div>
                                             {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                          </div>


                                       </div>
                                    </div>
                                 </div>

                              ))}

                              {providerPlanView && paymentPlanAllow.map((plan, idx) => (

                                 <div className={(plan.term_month == min_term) ? 'col-md-4 recommended-plan customer-plans' : 'col-md-4 customer-plans'} key={idx} >
                                    <div className="price-plan plan1 rct-block">
                                       <div className="header pb-0 pt-5">

                                          <h2 className="pricing-title text-center">
                                             <span className="plan_term">{plan.term_month} months Plan</span>
                                             <label className="check-container">
                                                <Input name="current_plan" type="checkbox" value="1" checked={(this.state.selectedPlan == plan.plan_id) ? true : false} onChange={this.checkboxAction.bind(this, plan.plan_id)} />
                                                <span className="checkmark"></span>
                                             </label>
                                          </h2>

                                          <div className="description-text">

                                             <div className="p-amt">Principal Amount: <span className="">${parseFloat(this.props.applicationDetails[0].amount).toFixed(2)}</span></div>

                                             <div className="apr">APR: <span className="">{(plan.interest_rate) ? parseFloat(plan.interest_rate).toFixed(2) : '0.00'}%</span></div>

                                          </div>

                                          <div className="pro_discount">
                                             Provider Discount: <span className="pdis">

                                                {(plan.discount_p) ? '$' + parseFloat(plan.discount_p).toFixed(2) : '$0.00'}

                                             </span>
                                          </div>

                                       </div>
                                       <div className="content ">
                                          <div className="page-form-outer select-plan-detail">

                                             <div className="price-list table-responsive">
                                                <table className="table table-bordered table-striped table-hover">
                                                   <thead>
                                                      <tr className="table-head">
                                                         <th>Payment#</th>
                                                         <th>Due Date</th>
                                                         <th>Payment Amt</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {
                                                         plan.monthlyPlan.map((month, idm) => (

                                                            <tr key={idm}>
                                                               <td>{idm + 1}</td>
                                                               <td>{month.next_month}</td>
                                                               <td>${month.perMonth}</td>
                                                            </tr>
                                                         ))
                                                      }



                                                   </tbody>
                                                </table>

                                             </div>
                                             {/*<button disabled={!this.validatePlan()} className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>*/}

                                          </div>


                                       </div>
                                    </div>
                                 </div>

                              ))}

                           </div>
                           {this.state.generatePlan &&
                              <div className="row text-right w-100 mr-0 p-20 d-inline float-right">
                                 <button disabled={!this.validatePlan()} className="btn btn-primary pt-10 pb-10" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, this.state.selectedPlan)}><span>View Agreement</span></button>
                              </div>
                           }
                        </div>

                     }
                  </div>
                  <div className={(this.state.fullView) ? 'modal-body page-form-outer text-left' : 'modal-body page-form-outer text-left d-none'} >
                     <div className="row">
                        <div className="col-sm-12 mx-auto">
                           <RctCard>
                              {paymentPlanAllow && this.state.selectedPlan !== null &&


                                 <div className="p-50" ref={el => (this.componentReff = el)}>
                                    {(() => {
                                       var filterType = paymentPlanAllow.filter(x => x.plan_id == this.state.selectedPlan);
                                       return <React.Fragment>
                                          <h1 className="text-center mb-20">
                                             <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                                          </h1>
                                          <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Customer Information</th>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Account No:</strong> {this.props.applicationDetails[0].patient_ac}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Application No:</strong> {this.props.applicationDetails[0].application_no}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Name:</strong> {this.props.applicationDetails[0].f_name + ' ' + this.props.applicationDetails[0].m_name + ' ' + this.props.applicationDetails[0].l_name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Address:</strong> {this.props.applicationDetails[0].address1 + ' ' + this.props.applicationDetails[0].address2 + ' ' + this.props.applicationDetails[0].City + ' ' + this.props.applicationDetails[0].name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Phone:</strong> {this.props.applicationDetails[0].phone_no}</td>
                                                         <td></td>
                                                      </tr>



                                                   </tbody>
                                                </table>

                                             </div>
                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Provider Information</th>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Account No:</strong> {this.props.appProviderDetails.provider_ac}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Name:</strong> {this.props.appProviderDetails.name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Address:</strong> {this.props.appProviderDetails.address1 + ' ' + this.props.appProviderDetails.address2 + ' ' + this.props.appProviderDetails.city + ' ' + this.props.appProviderDetails.state_name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Phone:</strong> {this.props.appProviderDetails.primary_phone}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Doctor:</strong> - </td>
                                                         <td></td>
                                                      </tr>


                                                   </tbody>
                                                </table>

                                             </div>

                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Loan Information</th>
                                                      </tr>

                                                      <tr>
                                                         <td><strong>Approved Amount:</strong> ${parseFloat(this.props.applicationDetails[0].approve_amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Principal Amount:</strong> ${parseFloat(this.props.applicationDetails[0].amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Remaining Amount:</strong> ${parseFloat(this.props.applicationDetails[0].remaining_amount + this.props.applicationDetails[0].override_amount - this.props.applicationDetails[0].amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Loan Amount:</strong> {parseFloat(filterType[0].totalAmount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>APR:</strong> {(filterType[0].interest_rate) ? parseFloat(filterType[0].interest_rate).toFixed(2) : '0.00'}%</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Term Month:</strong> {filterType[0].term_month}</td>
                                                         <td></td>
                                                      </tr>
                                                   </tbody>
                                                </table>

                                             </div>


                                          </div>
                                          <div className="table-responsive mb-40 pymt-history">
                                             <h2 className="text-center mb-10">Plan Details</h2>
                                             <div className="table-responsive">
                                                <table className="table table-borderless">
                                                   <thead>
                                                      <tr>
                                                         <th>SN#</th>
                                                         <th>Date</th>
                                                         <th>Amount</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {filterType[0].monthlyPlan.map((plan, idx) => (
                                                         <tr key={idx}>
                                                            <td>{idx + 1}</td>
                                                            <td>{plan.next_month}</td>
                                                            <td>${parseFloat(plan.perMonth).toFixed(2)}</td>
                                                         </tr>
                                                      ))}
                                                   </tbody>
                                                </table>

                                             </div>
                                          </div>

                                       </React.Fragment>
                                    })()}
                                 </div>
                              }
                              <div className="invoice-head">
                                 <div className="row">
                                    <div className="col-sm-12 text-right">
                                       <ul className="list-inline">
                                          <li><a href="javascript:void(0);" onClick={this.goBack.bind(this)}><i className="mr-10 ti-back-left"></i> Back</a></li>
                                          <li>
                                             <ReactToPrint
                                                trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                                content={() => this.componentReff}
                                             />
                                          </li>

                                          <li><a onClick={this.printPlan.bind(this)} href="javascript:void(0);" ><i className="mr-10 ti-import"></i> Export PDF</a></li>
                                          <li><a onClick={this.emailPlan.bind(this)} href="javascript:void(0);" ><i className="mr-10 ti-email"></i> Email</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </RctCard>
                        </div>
                     </div>
                  </div>
                  <div className={(this.state.agreementView) ? 'modal-body page-form-outer text-left' : 'modal-body page-form-outer text-left d-none'} >
                     <div className="row">
                        <div className="col-sm-12 mx-auto">

                           <RctCard>
                              {paymentPlanAllow && this.state.selectedPlan !== null &&


                                 <div className="p-10" ref={el => (this.componentRef = el)}>
                                    {(() => {
                                       var filterType = paymentPlanAllow.filter(x => x.plan_id == this.state.selectedPlan);
                                       var docter_name = (this.props.providerDoctors) ? this.props.providerDoctors.filter(x => x.id == this.state.loan.doctor) : '';
                                       return <React.Fragment>
                                          <h1 className="text-center mb-20">
                                             <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                                          </h1>
                                          <h1 className="text-center mb-20">Terms of Agreement for Health Partner, Inc</h1>
                                          <div className="terms-condition-agreement">
                                             <div>Date <strong><u>{moment(Date.now()).format('MM/DD/YYYY')}</u></strong></div>
                                             <div>Location _____________</div>
                                             <p>On or before <strong>Date</strong>, for value received, the undersigned <strong><u>{(this.props.applicationDetails) ? (this.props.applicationDetails[0].f_name + ' ' + this.props.applicationDetails[0].m_name + ' ' + this.props.applicationDetails[0].l_name) : '-'}</u></strong> (the "Borrower") promises to pay to the order of HEALTH PARTNER INC. (the "Holder"), in the manner and at the place provided below, the principal sum of <strong><u>${parseFloat(this.props.applicationDetails[0].amount).toFixed(2)}</u></strong>.</p>

                                             <h4><strong>1. PAYMENT :</strong></h4>
                                             <p>All payments of principal and interest under this note will be made in lawful money of the United States, without offset, deduction, or counterclaim, by wire transfer of immediately available funds to an account designated by the Holder in writing at least 7 days after the effective date of this note or, if this designation is not made, by check mailed to the Holder at 5720 Creedmoor Road, Suite 103, Raleigh, North Carolina, 27612, or at such other place as the Holder may designate in writing.</p>

                                             <h4><strong>2. INTEREST :</strong></h4>
                                             <p>Interest on the unpaid principal balance of this note is payable from the date of this note until this note is paid in full, at the rate of <strong><u>{(filterType) ? (parseFloat(filterType[0].interest_rate)).toFixed(2) + "%" : '-'}</u></strong> per year, or the maximum amount allowed by applicable law, whichever is less. Accrued interest will be computed on the basis of a 365-day or 366-day year, as the case may be, based on the actual number of days elapsed in the period in which it accrues.</p>

                                             <h4><strong>3. PREPAYMENT :</strong></h4>
                                             <p>The Borrower may prepay this note, in whole or in part, at any time before maturity without penalty or premium. Any partial prepayment will be credited first to accrued interest, then to principal. No prepayment extends or postpones the maturity date of this note.</p>

                                             <h4><strong>4. EVENTS OF DEFAULT :</strong></h4>
                                             <p>Each of the following constitutes an <strong>"Event of Default"</strong> under this note:</p>
                                             <ul>
                                                <li>The Borrower's failure to make any payment when due under the terms of this note, including the lump-sum payment due under this note at its maturity;</li>
                                                <li>The filing of any voluntary or involuntary petition in bankruptcy by or regarding the Borrower or the initiation of any proceeding under bankruptcy or insolvency laws against the Borrower;</li>
                                                <li>An assignment made by the Borrower for the benefit of creditors;</li>
                                                <li>The appointment of a receiver, custodian, trustee, or similar party to take possession of the Borrower's assets or property; or</li>
                                                <li>The death of the Borrower.</li>
                                             </ul>

                                             <h4><strong>5. ACCELERATION; REMEDIES ON DEFAULT :</strong></h4>
                                             <p>If any Event of Default occurs, all principal and other amounts owed under this note will become immediately due and payable without any action by the Holder, the Borrower, or any other person. The Holder, in addition to any rights and remedies available to the Holder under this note, may, in its sole discretion, pursue any legal or equitable remedies available to it under applicable law or in equity, including taking any of the following actions:</p>
                                             <ul>
                                                <li>Personally, or by agents or attorneys (in compliance with applicable law), take immediate possession of the collateral. To that end, the Holder may pursue the collateral where it may be found, and enter the Borrower's premises, with or without notice, demand, process of law, or legal procedure if this can be done without breach of the peace. If the premises on which any part of the collateral is located are not under the Borrower's direct control, the Borrower will exercise its best efforts to ensure that the Holder is promptly provided right of access to those premises. To the extent that the Borrower's consent would otherwise be required before a right of access could be granted, the Borrower hereby irrevocably grants that consent;</li>
                                                <li>Require the Borrower to assemble the collateral and make it available to the Holder at a place to be designated by the Holder that is reasonably convenient to both parties (it being acknowledged that the Borrower's premises are reasonably convenient to the Borrower);</li>
                                                <li>Sell, lease, or dispose of the collateral or any part of it in any manner permitted by applicable law or by contract; and</li>
                                                <li>Exercise all rights and remedies of a secured party under applicable law.
</li>
                                             </ul>

                                             <h4><strong>6. WAIVER OF PRESENTMENT; DEMAND :</strong></h4>
                                             <p>The Borrower hereby waives presentment, demand, notice of dishonor, notice of default or delinquency, notice of protest and nonpayment, notice of costs, expenses or losses and interest on those, notice of interest on interest and late charges, and diligence in taking any action to collect any sums owing under this note, including (to the extent permitted by law) waiving the pleading of any statute of limitations as a defense to any demand against the undersigned. Acceptance by the Holder or any other holder of this note of any payment differing from the designated lump-sum payment listed above does not relieve the undersigned of the obligation to honor the requirements of this note.</p>

                                             <h4><strong>7. Frozen Credit and Penalties :</strong></h4>
                                             <p>The Borrower agrees that in event of missed payment, any remaining line of credit will be frozen and will not be accessible until remaining balances have been paid. Missed payments will occur late fees and increased interest charges.</p>

                                             <h4><strong>8. GOVERNING LAW :</strong></h4>
                                             <ul>
                                                <li><strong>Choice of Law.</strong> The laws of the state of North Carolina govern this note (without giving effect to its conflicts of law principles).</li>
                                                <li><strong>Choice of Forum.</strong> Both parties consent to the personal jurisdiction of the state and federal courts in Wake, North Carolina.</li>
                                             </ul>

                                             <h4><strong>9. COLLECTION COSTS AND ATTORNEYS' FEES :</strong></h4>
                                             <p>The Borrower shall pay all expenses of the collection of indebtedness evidenced by this note, including reasonable attorneys' fees and court costs in addition to other amounts due.</p>

                                             <h4><strong>10. ASSIGNMENT AND DELEGATION :</strong></h4>
                                             <ul>
                                                <li><strong>No Assignment.</strong> The Borrower may not assign any of its rights under this note. All voluntary assignments of rights are limited by this subsection.</li>
                                                <li><strong>No Delegation.</strong> The Borrower may not delegate any performance under this note.</li>
                                                <li><strong>Enforceability of an Assignment or Delegation.</strong> If a purported assignment or purported delegation is made in violation of this section, it is void.</li>
                                             </ul>

                                             <h4><strong>11. SEVERABILITY :</strong></h4>
                                             <p>If any one or more of the provisions contained in this note is, for any reason, held to be invalid, illegal, or unenforceable in any respect, that invalidity, illegality, or unenforceability will not affect any other provisions of this note, but this note will be construed as if those invalid, illegal, or unenforceable provisions had never been contained in it, unless the deletion of those provisions would result in such a material change so as to cause completion of the transactions contemplated by this note to be unreasonable.</p>

                                             <h4><strong>12. NOTICES :</strong></h4>
                                             <ul>
                                                <li><strong>Writing; Permitted Delivery Methods.</strong> Each party giving or making any notice, request, demand, or other communication required or permitted by this note shall give that notice in writing and use one of the following types of delivery, each of which is a writing for purposes of this note: personal delivery, mail (registered or certified mail, postage prepaid, return-receipt requested), nationally recognized overnight courier (fees prepaid), facsimile, or email.</li>
                                                <li><strong>Addresses.</strong> A party shall address notices under this section to a party at the following addresses:<br /><br />
                                                   <p><strong>If to the Borrower :</strong></p>
                                                   <p><u>{this.props.applicationDetails[0].address1 + ' ' + this.props.applicationDetails[0].address2},</u></p>
                                                   <p><u>{this.props.applicationDetails[0].City + ', ' + this.props.applicationDetails[0].name + '-' + this.props.applicationDetails[0].zip_code}</u></p>
                                                   <p><u><strong>Phone:</strong> {this.props.applicationDetails[0].phone_no}</u></p>

                                                   <strong>If to the Holder:</strong>
                                                   <p>Health Partner Inc.</p>
                                                   <p>5720 Creedmoor Road, Suite 103</p>
                                                   <p>Raleigh, North Carolina 27612</p>

                                                </li>
                                                <li><strong>Effectiveness.</strong> A notice is effective only if the party giving notice complies with subsections (a)</li>
                                                <li>and if the recipient receives the notice</li>
                                             </ul>

                                             <h4><strong>13. WAIVER :</strong></h4>
                                             <p>No waiver of a breach, failure of any condition, or any right or remedy contained in or granted by the provisions of this note will be effective unless it is in writing and signed by the party waiving the breach, failure, right, or remedy. No waiver of any breach, failure, right, or remedy will be deemed a waiver of any other breach, failure, right, or remedy, whether or not similar, and no waiver will constitute a continuing waiver, unless the writing so specifies.</p>

                                             <h4><strong>14. HEADINGS :</strong></h4>
                                             <p>The descriptive headings of the sections and subsections of this note are for convenience only, and do not affect this note's construction or interpretation.</p>


                                             <br /><br />
                                          </div>
                                          <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Customer Information</th>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Account No:</strong> {this.props.applicationDetails[0].patient_ac}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Application No:</strong> {this.props.applicationDetails[0].application_no}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Name:</strong> {this.props.applicationDetails[0].f_name + ' ' + this.props.applicationDetails[0].m_name + ' ' + this.props.applicationDetails[0].l_name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Address:</strong> {this.props.applicationDetails[0].address1 + ' ' + this.props.applicationDetails[0].address2 + ' ' + this.props.applicationDetails[0].City + ' ' + this.props.applicationDetails[0].name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Phone:</strong> {this.props.applicationDetails[0].phone_no}</td>
                                                         <td></td>
                                                      </tr>



                                                   </tbody>
                                                </table>

                                             </div>
                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Provider Information</th>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Account No:</strong> {this.props.appProviderDetails.provider_ac}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Name:</strong> {this.props.appProviderDetails.name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Address:</strong> {this.props.appProviderDetails.address1 + ' ' + this.props.appProviderDetails.address2 + ' ' + this.props.appProviderDetails.city + ' ' + this.props.appProviderDetails.state_name}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Phone:</strong> {this.props.appProviderDetails.primary_phone}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Doctor:</strong> - {(docter_name != '') ? docter_name[0].name : '-'}</td>
                                                         <td></td>
                                                      </tr>


                                                   </tbody>
                                                </table>

                                             </div>

                                             <div className="add-card">

                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <th colSpan="2">Loan Information</th>
                                                      </tr>

                                                      <tr>
                                                         <td><strong>Approved Amount:</strong> ${parseFloat(this.props.applicationDetails[0].approve_amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Principal Amount:</strong> ${parseFloat(this.props.applicationDetails[0].amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Remaining Amount:</strong> ${parseFloat(this.props.applicationDetails[0].remaining_amount + this.props.applicationDetails[0].override_amount - this.props.applicationDetails[0].amount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Loan Amount:</strong> ${parseFloat(filterType[0].totalAmount).toFixed(2)}</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>APR:</strong> {(filterType[0].interest_rate) ? parseFloat(filterType[0].interest_rate).toFixed(2) : '0.00'}%</td>
                                                         <td></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Term Month:</strong> {filterType[0].term_month}</td>
                                                         <td></td>
                                                      </tr>
                                                   </tbody>
                                                </table>

                                             </div>


                                          </div>
                                          <div className="table-responsive mb-40 pymt-history">
                                             <h2 className="text-center mb-10">Plan Details</h2>
                                             <div className="table-responsive">
                                                <table className="table table-borderless">
                                                   <thead>
                                                      <tr>
                                                         <th>SN#</th>
                                                         <th>Date</th>
                                                         <th>Amount</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                      {filterType[0].monthlyPlan.map((plan, idx) => (
                                                         <tr key={idx}>
                                                            <td>{idx + 1}</td>
                                                            <td>{plan.next_month}</td>
                                                            <td>${parseFloat(plan.perMonth).toFixed(2)}</td>
                                                         </tr>
                                                      ))}
                                                   </tbody>
                                                </table>

                                             </div>
                                          </div>
                                          <div className="mb-40 pymt-history">

                                             <div className="row user-permission mt-20">
                                                <div className="col-md-12">
                                                   <p className="text-danger">Note:- This payment plan does not reflect possible charges occurring from late fees, transaction fees, or financial charges.  Interest will be charged to your account at a variable APR of 26.99% from purchase date if the purchase amount is not paid in full within the loan term or if you make a late payment. To avoid late fees, you must make your Monthly Payments by the due date each month.</p>
                                                </div>
                                                <div className="col-md-12">
                                                   <p>
                                                      <label className="check-container">
                                                         <Input name="current_plan" type="checkbox" value="1" checked={(this.state.loan.agree_with == 1) ? true : false} onChange={(e) => this.onChnagePlanExist('agree_with', e.target.value)} />
                                                         <span className="checkmark"></span><span className="pl-30 agree-text">By signing below, you, the Borrower, agree to all terms listed in the above Agreement.</span>
                                                      </label>
                                                   </p>
                                                </div>



                                                <div className="col-md-4">
                                                   <FormGroup>
                                                      <Label for="provider_location">BY (Name of Borrower)<span className="required-field">*</span></Label>
                                                      <TextField
                                                         type="text"
                                                         name="name_of_borrower"
                                                         id="name_of_borrower"
                                                         fullWidth
                                                         variant="outlined"
                                                         placeholder="Name of Borrower"
                                                         value={this.state.loan.name_of_borrower || ''}
                                                         error={(this.state.loanError.name_of_borrower) ? true : false}
                                                         helperText={(this.state.loanError.name_of_borrower != '') ? this.state.loanError.name_of_borrower : ''}
                                                         onChange={(e) => this.onChnagePlanExist('name_of_borrower', e.target.value)}
                                                      />
                                                   </FormGroup>
                                                </div>
                                                <div className="col-md-4">
                                                   <FormGroup>
                                                      <Label for="procedure_date">Date<span className="required-field">*</span></Label>
                                                      <DatePicker
                                                         dateFormat="MM/dd/yyyy h:mm aa"
                                                         timeIntervals={15}
                                                         timeFormat="HH:mm"
                                                         showTimeSelect
                                                         name="agreement_date"
                                                         id="agreement_date"
                                                         selected={this.state.agreement_date}
                                                         placeholderText="MM/DD/YYYY"
                                                         autocomplete={false}
                                                         onChange={(e) => this.onChnagePlanExist('agreement_date', e)}
                                                      />
                                                      {(this.state.loanError.agreement_date) ? <FormHelperText>{this.state.loanError.agreement_date}</FormHelperText> : ''}
                                                   </FormGroup>
                                                </div>



                                             </div>
                                             <h2 className="text-center mb-10">ID Proof documents Details</h2>
                                             <div className="row">
                                                <div className="col-md-4">
                                                   <FormGroup>
                                                      <Label for="provider_location">ID Type<span className="required-field">*</span></Label>
                                                      <TextField
                                                         type="text"
                                                         name="id_name"
                                                         id="id_name"
                                                         fullWidth
                                                         variant="outlined"
                                                         placeholder="ID Type"
                                                         value={this.state.loan.id_name || ''}
                                                         error={(this.state.loanError.id_name) ? true : false}
                                                         helperText={(this.state.loanError.id_name != '') ? this.state.loanError.id_name : ''}
                                                         onChange={(e) => this.onChnagePlanExist('id_name', e.target.value)}
                                                      />
                                                   </FormGroup>
                                                </div>
                                                <div className="col-md-4">
                                                   <FormGroup>
                                                      <Label for="provider_location">ID Number<span className="required-field">*</span></Label>
                                                      <TextField
                                                         type="text"
                                                         name="id_number"
                                                         id="id_number"
                                                         fullWidth
                                                         variant="outlined"
                                                         placeholder="ID Number"
                                                         value={this.state.loan.id_number || ''}
                                                         error={(this.state.loanError.id_number) ? true : false}
                                                         helperText={(this.state.loanError.id_number != '') ? this.state.loanError.id_number : ''}
                                                         onChange={(e) => this.onChnagePlanExist('id_number', e.target.value)}
                                                      />
                                                   </FormGroup>
                                                </div>
                                                <div className="col-md-3">
                                                   <FormGroup>
                                                      <Label for="procedure_date">Expiry Date</Label>
                                                      <DatePicker
                                                         dateFormat="MM/dd/yyyy"
                                                         name="expiry_date"
                                                         id="expiry_date"
                                                         selected={this.state.expiry_date}
                                                         placeholderText="MM/DD/YYYY"
                                                         autocomplete={false}
                                                         onChange={(e) => this.onChnagePlanExist('expiry_date', e)}
                                                      />
                                                      {(this.state.loanError.expiry_date) ? <FormHelperText>{this.state.loanError.expiry_date}</FormHelperText> : ''}
                                                   </FormGroup>
                                                </div>
                                             </div>
                                             <div className="user-permission mt-20">
                                                <div className="row">

                                                   <div className="col-md-12">

                                                   </div>

                                                </div>
                                             </div>
                                          </div>
                                          <div className="p-10">
                                             <div className="row justify-content-between mb-30 add-full-card customer-accnt">

                                                <div className="add-card-sign col-md-4 text-center sing_box">
                                                   <b>Customer Signature</b>
                                                   <div className="">
                                                      <SignatureCanvas penColor='green'
                                                         canvasProps={{ width: 285, height: 95, className: 'sigCanvas border border-primary' }} ref={(ref) => { this.customerSigPad = ref }} />
                                                   </div>
                                                   <div>
                                                      <div className="mt-10">
                                                         <Button onClick={this.clearCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                            Reset
                                                         </Button>
                                                         <Button onClick={this.trimCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                            Done
                                                         </Button>
                                                      </div>
                                                   </div>
                                                   <div className="mt-10">
                                                      {this.state.customerSignURL ? <img src={this.state.customerSignURL} /> : null}
                                                   </div>

                                                </div>
                                                <div className="col-md-4"></div>
                                                <div className="add-card-sign col-md-4 text-center sing_box">

                                                   <b>Witness Signature</b>
                                                   <div className="">
                                                      <SignatureCanvas penColor='green'
                                                         canvasProps={{ width: 285, height: 95, className: 'sigCanvas border border-primary' }} ref={(ref) => { this.witnessSigPad  = ref }} />
                                                   </div>
                                                   <div>
                                                      <div className="mt-10">
                                                         <Button onClick={this.clearWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                            Reset
                                                         </Button>
                                                         <Button onClick={this.trimWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                                            Done
                                                         </Button>
                                                      </div>
                                                   </div>
                                                   <div className="mt-10">
                                                      {this.state.witnessSignURL ? <img src={this.state.witnessSignURL} /> : null}
                                                   </div>
                                                </div>

                                             </div>
                                          </div>
                                       </React.Fragment>
                                    })()}
                                 </div>
                              }
                              <div className="invoice-head">
                                 <div className="row">
                                    <div className="col-sm-12 text-right">
                                       <div className="mt-15 mb-10">
                                          <Button onClick={this.goBack.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                             Back
                                          </Button>
                                          <Button disabled={!this.validateFinal()} onClick={() => this.processPlanSubmit(this.props.applicationDetails[0].application_id, this.state.selectedPlan)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                             Submit
                                          </Button>

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </RctCard>
                        </div>
                     </div>
                  </div>

                  {this.props.loading &&
                     <RctSectionLoader />
                  }
               </RctCollapsibleCard>

            }

         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, searchCustomer, paymentPlanAllow, applicationDetails, providerLocation, providerSpeciality, providerDoctors, rediretPlanURL, paymentPlanID, appProviderDetails, bankType } = creditApplication;

   return { loading, searchCustomer, paymentPlanAllow, applicationDetails, providerLocation, providerSpeciality, providerDoctors, rediretPlanURL, paymentPlanID, appProviderDetails, bankType, user }

}

export default connect(mapStateToProps, {
   searchCustomerApplication, paymentPlan, applicationOption, getSpeciality, getDoctors, planReset, createPaymentPlan, printPlan, eamilPlan
})(searchApplication); 