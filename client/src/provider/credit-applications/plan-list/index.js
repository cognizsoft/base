/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TableCell from "@material-ui/core/TableCell";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import OpenConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   providerPlanList,providerPlanSubmit
} from 'Actions';

class planList extends Component {

   state = {
      currentModule: 20,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      planList: null, // initial user data
      selectedUser: null, // selected user to perform operations
      allSelected: false,
      selectedPlan: 0,
      redirectURL:0,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.providerPlanList();
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         },
         MuiTableCell: {
            root: {
               padding: "4px 8px 4px 8px"
            }
         },
         MuiSvgIcon:{
            root : {
               width : '1em !important'
            }
         },
         MuiButton:{
            containedPrimary :{
               'background-color' : '#0E5D97'
            }
         },
         MuiCheckbox : {
            root: {
               "&$checked" : {
                  color : '#0E5D97 !important'
               }
            }
            
         }
      }
   })
   //Select All user
   onSelectAllPlan(e) {
      const { selectedPlan, planList } = this.state;
      let selectAll = selectedPlan < planList.length;
      if (selectAll) {
         let selectAllPlan = planList.map(plan => {
            //plan.checked = (plan.procedure_status == 0 || new Date(plan.procedure_date) > new Date()) ? false : true
            plan.checked = (plan.procedure_status == 0) ? false : true
            return plan
         });
         this.setState({ planList: selectAllPlan, selectedPlan: selectAllPlan.length })
      } else {
         let unselectedPlan = planList.map(plan => {
            plan.checked = false
            return plan;
         });
         this.setState({ selectedPlan: 0, planList: unselectedPlan });
      }
   }
   /**
	 * On Select User
	 */
   onSelectPlan(plan) {
      plan.checked = !plan.checked;
      let selectedPlan = 0;
      let planList = this.state.planList.map(planData => {
         if (planData.pp_id === plan.pp_id) {
            if (planData.checked && planData.checked !== undefined) {
               selectedPlan++;
            }
            return plan;
         } else {
            if (planData.checked && planData.checked !== undefined) {
               selectedPlan++;
            }
            return planData;
         }
      });
      this.setState({ planList, selectedPlan });
   }
   componentWillReceiveProps(nextProps) {
      let { addData, add_err } = this.state;
      (nextProps.planList) ? this.setState({ planList: nextProps.planList }) : '';
   }

   confirmationPopupPlan() {
      this.refs.openConfirmationDialog.open();
   }

   providerPlanSubmit() {
      const { selectedPlan, planList } = this.state;
      var planId = planList.reduce(function (accumulator, currentValue) {
         if (currentValue.checked === true) {
            accumulator.push(currentValue.pp_id);
         }
         return accumulator
      }, []);
      
      this.props.providerPlanSubmit(planId)
      this.refs.openConfirmationDialog.close();
      this.setState({ selectedPlan: 0 })
   }

   render() {
      const { planList, loading, selectedUser, editUser, allSelected, selectedPlan } = this.state;
      //console.log(selectedPlan)
      let diableStatus = 0;
      if(planList !== null){
         diableStatus = planList.reduce(function (accumulator, currentValue) {
            if(currentValue.checked === true){
               accumulator++
            }               
            return accumulator;
         }, 0);
      }
      
      //disabled={(value.procedure_status == 0 || new Date(value.procedure_date) > new Date()) ? true : false}
      //const planList = this.props.planList;
      const columns = [
         {
            name: 'select',
            field: ' ',
            options: {
               filter:false,
               short:false,
               customHeadRender: (rowDataObject, tableMeta, updateValue) => {
                  if(Array.isArray(planList) && planList.length > 0) {
                     return (
                           <TableCell key={rowDataObject} component="th" className="heading-checkbox">
                              <Checkbox
                                 key={rowDataObject.rowIndex}
                                 indeterminate={selectedPlan > 0 && selectedPlan < planList.length}
                                 checked={selectedPlan > 0}
                                 onChange={(e) => this.onSelectAllPlan(e)}
                                 value="all"
                                 color="primary"
                              />
                           </TableCell>
                     )
                  }
               },
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <FormControlLabel
                        key={tableMeta.rowIndex}
                        control={
                           <Checkbox
                              checked={(value.checked) ? true : false}
                              disabled={(value.procedure_status == 0) ? true : false}
                              onChange={() => this.onSelectPlan(value)}
                              color="primary"
                           />
                        }
                     />
                  )

               }
            }
         },
         {
            name: 'SN#',
            field: ' ',
            filter: false,
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     tableMeta.rowIndex + 1
                  )
               },
            }
         },
         {
            name: 'Plan No',
            field: 'plan_number',
         },
         {
            name: 'A/C No',
            field: 'patient_ac',
         },
         {
            name: 'App No',
            field: 'application_no',
         },
         {
            name: 'Name',
            field: 'f_name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.f_name+' '+value.l_name
                  )
               }
            }
         },
         { 
            name: 'Phone', 
            field: 'phone_no', 
         },
         {
            name: 'Address',
            field: 'address1',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.address1
                  )
               },
            }
         },
         { 
            name: 'City', 
            field: 'City', 
         },
         {
            name: 'Loan Amt',
            field: 'loan_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.loan_amount).toFixed(2)
                  )
               }
            }
         },
         {
            name: 'Service Date',
            field: 'procedure_date',

         },
         {
            name: 'Plan Status',
            field: 'status_name',

         },



      ];
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         customToolbar: () => {
            return (

               (this.state.currentPermision.add) ? <CustomToolbar confirmationPopupPlan={this.confirmationPopupPlan.bind(this)} disabled={diableStatus} /> : ''

            );
         },
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'provider-plan-list.csv' },
      };

      return (
         <div className="provider-create-invoice">
            <Helmet>
               <title>Health Partner | Credit Applications | Plan List</title>
               <meta name="description" content="Plan List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="provider.providerSurgeryStatusUpdate" />}
               match={this.props.match}
            />
            
            <RctCollapsibleCard fullBlock>
               <div className="table-responsive invoice_table">
                  
                  <MuiThemeProvider theme={this.getMuiTheme()}>
                     <MaterialDatatable
                        data={(planList) ? planList : ''}
                        columns={columns}
                        options={options}
                     />
                  </MuiThemeProvider>
                 
                 <OpenConfirmationDialog
                     ref="openConfirmationDialog"
                     title="Are you sure want to confirm?"
                     message=""
                     onConfirm={() => this.providerPlanSubmit()}
                  />
                  
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, planList, submitPlan } = creditApplication;
   return { loading, planList, submitPlan, user }

}

export default connect(mapStateToProps, {
   providerPlanList, providerPlanSubmit
})(planList);