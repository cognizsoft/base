/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Helmet } from "react-helmet";
import {withRouter} from 'react-router-dom';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import queryString from 'query-string';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import {
   creditApplicationListProvider,downloadAgrement,requestToken
} from 'Actions';

class CreditApplicationList extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      careCould : false,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 06,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.creditApplicationListProvider(this.props.match.params.status_id);
      //console.log(window.location.origin.toString())
      if(this.props.location.search != '' && this.state.careCould === false){
         var tokenDetails = JSON.parse(localStorage.getItem('applicationToken'));
         if(tokenDetails !== null && tokenDetails.expire_at < Math.floor(new Date().getTime() / 1000) && tokenDetails.refresh_token !== undefined){
            this.props.requestToken(queryString.parse(this.props.location.search).code,window.location.origin.toString()+'/provider/credit-applications',tokenDetails.refresh_token)
            this.setState({careCould:true})
         }else if(tokenDetails.expire_at > Math.floor(new Date().getTime() / 1000)){
            this.setState({careCould:true})
         }else{
            this.props.requestToken(queryString.parse(this.props.location.search).code,window.location.origin.toString()+'/provider/credit-applications','')
            this.setState({careCould:true})
         }
         //console.log(tokenDetails.expires_in*1000)
         //this.setState({careCould:true})
      }
      
   }

      /*
      * Title :- permissionFilter
      * Descrpation :- This function use filter action permission according to current user
      * Author : Cognizsoft and Ramesh Kumar
      * Date :- April 22,2019
      */
      permissionFilter = (name) => {
         let per = JSON.parse(this.props.user);

         let newUser = per.user_permission.filter(
            function (per) { return per.description == name }
         );

         this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
      }
	

	


   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         }
      }
   })
   downloadAgrement(app_id,e){
      this.props.downloadAgrement(app_id);
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      if(this.state.careCould){
         return (<Redirect to={`/provider/search-customer`} />);
      }
      const applicationList = this.props.applicationList;
      console.log(this.props.careCouldToken)
      const columns = [
         { name: 'ID', field: 'application_id', },
         { name: 'Account No', field: 'patient_ac', },
         { name: 'App No', field: 'application_no', },
         {
            name: 'Full Name',
            field: 'f_name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  )
               },
            }
         },
         { name: 'Address', field: 'address1', },
         { name: 'City', field: 'City', },
         { name: 'State', field: 'state_name', },
         { name: 'Zip', field: 'zip_code', },
         { name: 'Primary Phone', field: 'peimary_phone', },
         {
            name: 'Status',
            field: 'status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if(value.status == '1' && value.plan_flag == '1'){
                     appstatus = 'Approved';
                  }else if(value.status == '1'  && value.plan_flag == '0'){
                     appstatus = 'Plan Not Selected';
                  }else if(value.status == '0'){
                     appstatus = 'Unapproved rejcect';
                  }else if(value.status == '2'){
                     appstatus = 'Manual Process';
                  }else if(value.status == '3'){
                     appstatus = 'Rejected';
                  }else if(value.status == '4'){
                     appstatus = 'Need More Information';
                  }else if(value.status == '5'){
                     appstatus = 'Document Review Process';
                  }
                  return (
                     <React.Fragment>
                        <span className="d-flex justify-content-start">
                           <div className="status">
                              <span className="d-block">{value.status_name}</span>
                           </div>
                        </span>
                     </React.Fragment>
                  )
               },
               customValue: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if(value.status == '1' && value.plan_flag == '1'){
                     appstatus = 'Approved';
                  }else if(value.status == '1'  && value.plan_flag == '0'){
                     appstatus = 'Plan Not Selected';
                  }else if(value.status == '0'){
                     appstatus = 'Unapproved';
                  }else if(value.status == '2'){
                     appstatus = 'Manual Process';
                  }else if(value.status == '3'){
                     appstatus = 'Rejected';
                  }else if(value.status == '4'){
                     appstatus = 'Need More Information';
                  }else if(value.status == '5'){
                     appstatus = 'Document Review Process';
                  }
                  return value.status_name;
               },
               customSortValue: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if(value.status == '1' && value.plan_flag == '1'){
                     appstatus = 'Approved';
                  }else if(value.status == '1'  && value.plan_flag == '0'){
                     appstatus = 'Plan Not Selected';
                  }else if(value.status == '0'){
                     appstatus = 'Unapproved';
                  }else if(value.status == '2'){
                     appstatus = 'Manual Process';
                  }else if(value.status == '3'){
                     appstatus = 'Rejected';
                  }else if(value.status == '4'){
                     appstatus = 'Need More Information';
                  }else if(value.status == '5'){
                     appstatus = 'Document Review Process';
                  }
                  return value.status_name;
               },
            }
         },
         {
            name: 'LOC',
            field: 'approve_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.approve_amount) ? '$'+(value.approve_amount).toFixed(2) : '$0.00'
                  )
               },
            }
         },
         { 
            name: 'Available Balance', 
            field: 'remaining_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.remaining_amount != null || value.override_amount) ? '$'+(value.remaining_amount+value.override_amount).toFixed(2) : '$0.00'
                  )
               },
            } 
         },
         { 
            name: 'Expire Date', 
            field: 'expiry_date',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.expiry_date
                  )
               },
            } 
         },
         {
            name: 'Action',
            field: 'md_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  
                  return (
                     <React.Fragment>
                        <span className="list-action">
                           {/*(value.status == 1 && value.plan_flag == 0 && value.score != null) ? <Link to={`/provider/customers/customerPayment-plan/${value.application_id}`} title="Select Plan"><i className="ti-clipboard"></i></Link> : ''*/}
                           {/*(this.state.currentPermision.view) ? <Link to={`/provider/credit-application/application/${value.application_id}`} title="View Application"><i className="ti-eye"></i></Link> : ''*/}
                           {/*<Link to={`/provider/credit-application/edit-application/${value.application_id}`}><i className="ti-pencil"></i></Link>*/}

                           {(value.plan_exists>0)?<Link to={`/provider/credit-application/plan-details/${this.enc(value.application_id.toString())}`} title="View account details"><i className="zmdi zmdi-card"></i></Link>:''}

                           {(value.plan_flag == 1 && value.score != null) ? <Link to={`/provider/credit-application/payment-plan/${value.application_id}`} title="View account details"><i className="zmdi zmdi-card"></i></Link> : ''}

                           {(value.status == '4' && this.state.currentPermision.edit) ? <Link to={`/provider/credit-application/upload/${value.patient_id}/${value.application_id}`} title="Upload Document for Application"><i className="ti-upload"></i></Link> : ''}
                           {/*(value.document_flag == 0)?<Link to={`/provider/customers/agreement-plan/${value.application_id}`} title="Upload Agreement"><i className="ti-thumb-up"></i></Link>:''*/}
                           {/*(value.document_flag == 1)?<a href="javascript:void(0)" onClick={this.downloadAgrement.bind(this,value.application_id)} title="Download Agreement"><i className="ti-cloud-down"></i></a>:''*/}
                           {(value.status == '1') ? <Link to={`/provider/credit-application/all-documents/${value.application_id}`} title="All Documents"><i className="zmdi zmdi-folder"></i></Link> : ''}
                        </span>
                     </React.Fragment>
                  )
               },

            }
         },


      ];
      const options = {

         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         customToolbar: () => {
            return (
               <React.Fragment>
                  {/*<Link to="/provider/credit-application/add-new-MPS" color="primary" className="caret btn-sm mr-10-custome">Create application from PMS <i className="zmdi zmdi-plus"></i></Link>*/}
                  {/*<a href={'https://platform.carecloud.com/oauth2/authorize?client_id='+this.props.careCould+'&response_type=code&redirect_uri='+window.location.origin.toString()+'/provider/credit-applications'} color="primary" className="caret btn-sm mr-10-custome">Create application from PMS <i className="zmdi zmdi-plus"></i></a>*/}
                  {(this.state.currentPermision.add) ? <Link to="/provider/search-customer" color="primary" className="caret btn-sm mr-10-custome">Create application from PMS <i className="zmdi zmdi-plus"></i></Link> : ''}
                  {(this.state.currentPermision.add) ? <Link to="/provider/credit-application/add-new" color="primary" className="caret btn-sm mr-10">Add Credit Application <i className="zmdi zmdi-plus"></i></Link> : ''}
               </React.Fragment>
            );
         },
         pagination: true,
         downloadOptions: { filename: 'creditApplication.csv' },
      };
      return (
         <div className="credit-application provider-application-list">
            <Helmet>
               <title>Health Partner | Credit Application | Application List</title>
               <meta name="description" content="Application List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.creditApplications" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <MuiThemeProvider theme={this.getMuiTheme()}>
                  <MaterialDatatable
                     data={applicationList}
                     columns={columns}
                     options={options}
                  />
               </MuiThemeProvider>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, CareCouldReducer }) => {
   const { loading, applicationList,careCould } = creditApplication;
   const { user } = authUser;
   return { loading, applicationList, careCould, user }
   //return { loading, applicationList, careCould}

}

export default connect(mapStateToProps, {
   creditApplicationListProvider,downloadAgrement,requestToken
})(CreditApplicationList);