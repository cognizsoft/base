/**
* Main App
*/
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
// css
import './lib/reactifyCss';
import { NotificationContainer } from 'react-notifications';
// firebase
//import './firebase';

// app component  basename={'/health/app'} 
import App from './container/App';

import { configureStore } from './store';

const MainApp = () => (
	<Provider store={configureStore()}>
		<MuiPickersUtilsProvider utils={MomentUtils}>
			<NotificationContainer />
			<Router basename={'/'}>
				<Switch>

					<Route path="/" component={App} />
				</Switch>
			</Router>
		</MuiPickersUtilsProvider>
	</Provider>
);

export default MainApp;
