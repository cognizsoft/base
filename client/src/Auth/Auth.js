import { configureStore } from '../store';
import { authHeader, expiresAtM } from '../apifile';
import { NotificationManager } from 'react-notifications';
export default class Auth {

  constructor() {

    this.isAuthenticated();
  }


  isAuthenticated() {
    if (Math.floor(new Date().getTime() / 1000) >= expiresAtM()) {
      localStorage.removeItem('user');
      localStorage.removeItem('user_permission');
      localStorage.removeItem('role');
      localStorage.removeItem('applicationToken');
      return false;
    } else {
      return true;
    }
  }
  isAuthenticatedMsg() {
    NotificationManager.error('Current session token expired. Please login again to continue.');
  }
}
