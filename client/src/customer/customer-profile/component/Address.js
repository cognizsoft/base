/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
   FormGroup,
   Form,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';

// edit address from
import EditAddressForm from './EditAddressForm';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const Address = ({ customerDetailsAddress }) => (
         <div className="address-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
               {customerDetailsAddress.map((addRess, idx) => (
                  <div className="view-section-inner" key={idx}>

                    <div className="view-box">
                        <div className="width-50">
                           <table>
                              <tbody>
                                 <tr>
                                  <td className="fw-bold">Address1 :</td>
                                  <td>{(addRess.address1) ? addRess.address1 : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Address2 :</td>
                                  <td>{(addRess.address2) ? addRess.address2 : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Country :</td>
                                  <td>{(addRess.country_name) ? addRess.country_name : '-'}</td>
                                </tr>
                              </tbody>
                           </table>
                        </div>

                        <div className="width-50">
                           <table>
                              <tbody>
                                <tr>
                                  <td className="fw-bold">State :</td>
                                  <td>{(addRess.state_name) ? addRess.state_name : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Phone No :</td>
                                  <td>{(addRess.phone_no) ? addRess.phone_no : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Billing and Physical address same :</td>
                                  <td>{(addRess.billing_address == 1) ? 'Yes' : 'No'}</td>
                                </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <div className="view-box">
                        <div className="width-50">
                           <table>
                              <tbody>
                                 <tr>
                                  <td className="fw-bold">City :</td>
                                  <td>{(addRess.City) ? addRess.City : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Zip Code :</td>
                                  <td>{(addRess.zip_code) ? addRess.zip_code : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">How long at this address? :</td>
                                  <td>{(addRess.address_time_period) ? addRess.address_time_period+'Yrs.' : '-'}</td>
                                </tr>
                              </tbody>
                           </table>
                        </div>

                        <div className="width-50">
                           <table>
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Primary Address :</td>
                                  <td>{(addRess.primary_address == 1) ? 'Yes' : 'No'}</td>
                                </tr>
                                <tr><td></td><td></td></tr>
                                 <tr><td></td><td></td></tr>
                              </tbody>
                           </table>
                        </div>
                     </div>

                  </div>
               ))}
            </div>
         </div>
      );
export default Address;
