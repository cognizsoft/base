/**
 * Email Prefrences Page
 */
import React, { Component } from 'react';
import Switch from 'react-toggle-switch';
import Button from '@material-ui/core/Button';
import { FormGroup, Input } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';

// intl messages
import IntlMessages from 'Util/IntlMessages';

const BankDetails = ({ customerDetails, onVerifyAmount, bankDetails }) => (
  <div className="prefrences-wrapper d-flex">
    <div className="modal-body page-form-outer view-section">

      <div className="view-section-inner">

        <div className="view-box">
          {bankDetails && bankDetails.map((bank, idx) => (
            <div className="width-100" key={idx}>
              <table className="table">
                <tbody>
                  <tr>
                    <td className="fw-bold">Bank Name:</td>
                    <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                    <td className="fw-bold">Name on Account:</td>
                    <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                  </tr>
                  <tr>
                    <td className="fw-bold">Routing Number:</td>
                    <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                    <td className="fw-bold">Bank A/C Type:</td>
                    <td>{(bank.value) ? bank.value : '-'}</td>
                  </tr>
                  <tr>
                    <td className="fw-bold">Bank A/C#:</td>
                    <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                    <td className="fw-bold">Bank Address</td>
                    <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                  </tr>
                </tbody>
              </table>
              <hr className="border-dark" />
            </div>
          ))
          }
          <div className="width-50">
            <table>
              <tbody>
                <tr>
                  <td className="fw-bold">Verification Status :</td>
                  <td>
                    <span>
                      {(customerDetails.bank_verify_flag) ? 'Yes' : 'No'}
                    </span>
                    {customerDetails.account_number && customerDetails.bank_verify_amt && customerDetails.bank_verify_amt_duration && customerDetails.bank_verify_flag == 0 &&
                      <span className="bank_verified_container text-right p-5 m-5">
                        <i className="zmdi zmdi-info mr-5"></i><span className="verify-bank-btn vf-btn" onClick={() => onVerifyAmount()}>Verify Bank Account</span>
                      </span>
                    }
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>

    </div>
  </div>
);
export default BankDetails;