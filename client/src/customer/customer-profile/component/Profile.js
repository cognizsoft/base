/**
 * Profile Page
 */
import React, { Component } from 'react';
import { FormGroup, Input, Form, Label, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';

// intlmessages
import IntlMessages from 'Util/IntlMessages';

const Profile = ({ customerDetails }) => (
         <div className="profile-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">

                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">First Name :</td>
                               <td className="text-capitalize">{(customerDetails.f_name) ? customerDetails.f_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Middle Name :</td>
                               <td className="text-capitalize">{(customerDetails.m_name) ? customerDetails.m_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Last Name :</td>
                               <td className="text-capitalize">{(customerDetails.l_name) ? customerDetails.l_name : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Other First Name :</td>
                               <td>{(customerDetails.other_f_name) ? customerDetails.other_f_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Other Middle Name :</td>
                               <td>{(customerDetails.other_m_name) ? customerDetails.other_m_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Other Last Name :</td>
                               <td>{(customerDetails.other_l_name) ? customerDetails.other_l_name : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

                  <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Date of Birth :</td>
                               <td>{(customerDetails.dob) ? customerDetails.dob : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Gender :</td>
                               <td>{(customerDetails.gender) ? customerDetails.gender : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Primary Email Address :</td>
                               <td>{(customerDetails.email) ? customerDetails.email : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Primary Phone No.:</td>
                               <td>{(customerDetails.user_phone1) ? customerDetails.user_phone1 : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Secondary Phone No.:</td>
                               <td>{(customerDetails.user_phone2) ? customerDetails.user_phone2 : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Social Security Number :</td>
                               <td>
                                  {(customerDetails.ssn) ? customerDetails.ssn.replace(/.(?=.{4})/g, 'x') : '-'}
                               </td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

                  <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Secondary Email Address :</td>
                               <td>{(customerDetails.secondary_email) ? customerDetails.secondary_email : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Customer A/C :</td>
                               <td>{(customerDetails.patient_ac) ? customerDetails.patient_ac : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Status :</td>
                               <td>{(customerDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold"></td>
                               <td></td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      );
export default Profile;