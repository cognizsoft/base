/**
 * Messages Page
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Input,
   InputGroup,
   InputGroupAddon,
   FormGroup,
   Label
} from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

const EmploymentDetails = ({ customerDetails }) => (
         <div className="messages-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">

                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Employed :</td>
                               <td>{(customerDetails.employment_status == 0) ? 'No' : 'Yes'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Employer Name :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.employer_name) ? customerDetails.employer_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Employment Type :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.emp_type_name) ? customerDetails.emp_type_name : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Employer Phone No :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.employer_phone) ? customerDetails.employer_phone : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Annual Income($) :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.annual_income) ? customerDetails.annual_income : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Employer Email Address :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.employer_email) ? customerDetails.employer_email : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Employed Since :</td>
                               <td>{(customerDetails.employment_status !== 0 && customerDetails.employer_since && customerDetails.employer_since !== '00/00/0000') ? customerDetails.employer_since : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                              <tr>
                               <td></td>
                               <td></td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      );
export default EmploymentDetails;