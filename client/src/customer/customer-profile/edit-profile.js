/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';
import HozNonLinear from './edit-customer-stepper';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllUsers
} from 'Actions';

export default class EditProfile extends Component {

   state = {
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0
   }


	
	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/**
	 * On Reload
	 */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

	/**
	 * On Select User
	 */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }

	
   /**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
      this.setState({ openViewUserDialog: true, selectedUser: data });
     
   }

	/**
	 * On Edit User
	 */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose() {
      this.setState({ addNewUserModal: false, editUser: null })
   }

    /**
	 * On View User Modal Close
	 */
   onViewUserModalClose = () => {
      this.setState({ openViewUserDialog: false, selectedUser: null })
   
   }
   
    componentDidMount() {
    
   }
	

   render() {
      const { users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      
      
      const options = {
         filterType: 'dropdown',
      };
      return (
         <div className="country regions">
            <Helmet>
               <title>Health Partner | Providers | Update Profile</title>
               <meta name="description" content="Update Profile" />
            </Helmet>
            {this.props.children}
            <PageTitleBar
               title={<IntlMessages id="sidebar.customerEdit" />}
               match={this.props.match}

            />
            
            <RctCollapsibleCard fullBlock>

               <HozNonLinear customer_edit_id={this.props.match.params.id}/>
            </RctCollapsibleCard>
           
         </div>
      );
   }
}
