/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Profile from './component/Profile';
import BankDetails from './component/BankDetails';
import EmploymentDetails from './component/EmploymentDetails';
import Address from './component/Address';
import UserBlock from './component/UserBlock';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge
} from 'reactstrap';
// rct card box
import { RctCard } from 'Components/RctCard';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { NotificationManager } from 'react-notifications';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import {patientID} from '../../apifile';
import VerifyAmountForm from './VerifyAmountForm';
import { isEmpty, isContainWhiteSpace, pointDecimals, isDecimals, isNumeric, isNumericDecimal, isLength } from '../../validator/Validator';

import {
  viewCustomer, submitCustomerVerifyAmount
} from 'Actions';

function getSteps() {
   return ['My Profile', 'Bank Details', 'Employment Details', 'Address'];
}

class UserProfile extends Component {

   state = {
      activeTab: this.props.location.state ? this.props.location.state.activeTab : 0,
      activeStep: 0,
      completed: {},

      addNewVerifyAmountModal: false,
      loading: false,

      verify_err: {
         verify_amount: ''
      },
      verify_amt: {
         patient_id: '',
         verify_amount: ''
      },
      customerDetails: {}
   }

   handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let { updateForm } = this.state;
    updateForm[name] = value;

    this.setState({
      updateForm: updateForm
    }, function () {
      this.onVerifyAmountDetails(name, value)
    });
  }

  onVerifyAmount() {
    let { verify_amt, verify_err } = this.state;

    var verify_error = {}
    verify_amt.patient_id = patientID()

    this.setState({ addNewVerifyAmountModal: true, verify_amt: verify_amt, verify_err: verify_error });
  }

  onVerifyAmountModalClose() {
    let { verify_err } = this.state

    var verify_error = {}

    this.setState({ addNewVerifyAmountModal: false, verify_err: verify_error })
  }

  onVerifyAmountDetails(fieldName, value) {

    let { verify_err } = this.state;
      //console.log(value)
    switch (fieldName) {
      case 'verify_amount':
        value = pointDecimals(value);
        if (isEmpty(value)) {
           verify_err[fieldName] = "Amount can't be blank";
        } else if (!isDecimals(value)) {
          verify_err[fieldName] = "Amount not valid";
        } else if(value <= 0 || value >= 1) {
          verify_err[fieldName] = 'Amount should be between 0.00 - 1.00';
        } else {
          verify_err[fieldName] = '';
        }

        this.setState({
          verify_amt: {
            ...this.state.verify_amt,
            [fieldName]: value
          }
        });
        break;
      default:
        break;

    }

    this.setState({ verify_err: verify_err });

    

  }

  verifyAmount() {
    const { verify_amt } = this.state;
    const { customerDetails } = this.props;

    //console.log(verify_amt)
    //return false;

   if((Date.now() / 1000) >= customerDetails.bank_verify_amt_duration) {
      NotificationManager.error('Time limit is expired. Please contact to admin.');
      //console.log(customerDetails.bank_verify_amt_duration)
      //console.log(Date.now() / 1000)
   } else {

      if(customerDetails.bank_verify_amt == verify_amt.verify_amount) {

         this.props.submitCustomerVerifyAmount(verify_amt);

         let self = this;
         setTimeout(() => {
            this.setState({ loading: true, addNewVerifyAmountModal: false });
            //NotificationManager.success('Amount Submitted!');
            self.setState({ loading: false });
         }, 2000);

      } else {
         NotificationManager.error('Amount does not match. Please enter correct amount.');
      }

   }

  }

  validateVerifyAmount() {
    return (
      this.state.verify_err.verify_amount === ''
    )
  }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   componentDidMount() {
    this.props.viewCustomer(patientID());
  }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   componentWillReceiveProps(nextProps) {
      
      let { customerDetails } = this.state;

      if(nextProps.customerDetails) {
         this.setState({ customerDetails: nextProps.customerDetails })
      }
      if(nextProps.verifyAmt !== undefined) {
         this.setState({ customerDetails: nextProps.verifyAmt.customerDetails })
      }

   }


   render() {
      const steps = getSteps();
      const { activeStep, verify_err, customerDetails } = this.state;
      const { loading, appDetailsAddress } = this.props;

      //console.log('this.state.customerDetails')
      //console.log(this.state.customerDetails)
      return (
         <div className="userProfile-wrapper">
            <Helmet>
               <title>User Profile</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.userProfile" />} match={this.props.match} />
            <RctCard>
            {customerDetails &&
               <UserBlock 
                  customerDetails={customerDetails}
                  customerID={patientID()}
               />
            }
               

            {customerDetails &&
               <div>
                  <Stepper nonLinear activeStep={activeStep}>
                     {steps.map((label, index) => {
                        return (
                           <Step key={label}>
                              <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                                 {label}
                              </StepButton>
                           </Step>
                        );
                     })}
                  </Stepper>
                  <div>
                     {this.allStepsCompleted() ? (
                        <div className="pl-40">
                           <p>All steps completed - you&quot;re finished</p>
                           <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                        </div>
                     ) : (
                           <div className="pl-40">
                              {(() => {
                                 switch (activeStep) {
                                    case 0:
                                       return <Profile 
                                                customerDetails={customerDetails}
                                              />
                                       break;
                                    case 1:
                                       return <BankDetails 
                                                customerDetails={customerDetails}
                                                onVerifyAmount={this.onVerifyAmount.bind(this)}
                                                bankDetails={this.props.bankDetails}
                                              />
                                       break;
                                    case 2:
                                       return <EmploymentDetails 
                                                customerDetails={customerDetails}
                                              />
                                       break;
                                    case 3:
                                       return <Address 
                                                customerDetailsAddress={appDetailsAddress}
                                              />
                                       break;
                                    default:
                                       return (<div></div>)
                                 }
                              })()}

                           </div>
                        )}
                  </div>
               </div>
            }

            </RctCard>

            <Modal isOpen={this.state.addNewVerifyAmountModal} toggle={() => this.onVerifyAmountModalClose()}>
               <ModalHeader toggle={() => this.onVerifyAmountModalClose()}>

                  Verify Bank Account

               </ModalHeader>
               
               <ModalBody>

                  <VerifyAmountForm
                    verifyAmtErr={verify_err}
                    onVerifyAmountDetail={this.onVerifyAmountDetails.bind(this)}
                    verifyAmt={this.state.verify_amt}
                  />

               </ModalBody>
               <ModalFooter>

                  <Button variant="contained" color="primary" className="text-white" onClick={() => this.verifyAmount()} disabled={!this.validateVerifyAmount()}>Verify</Button>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onVerifyAmountModalClose()}>Cancel</Button>

               </ModalFooter>
            </Modal>

         </div>
      );
   }
}
const mapStateToProps = ({ Customer, CustomerDashboardReducer }) => {
   
  const { loading, customerDetails, appDetailsAddress, bankDetails } = Customer;
  const { verifyAmt } = CustomerDashboardReducer
  return { loading, customerDetails, appDetailsAddress, verifyAmt, bankDetails }
}
export default connect(mapStateToProps, {
  viewCustomer, submitCustomerVerifyAmount
})(UserProfile);