/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import {
   planDetails, clearRedirectURL, getSingleInstallment, sendReport
} from 'Actions';
class CustomerBilling extends Component {

   state = {
      
   }

   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      //console.log(this.props.match.params.id)
      this.props.clearRedirectURL();
      this.props.planDetails(this.props.match.params.appid);
      this.props.getSingleInstallment(this.props.match.params.planid, this.props.match.params.instlmntid);
   }

   sendCustomerReport(app_id, plan_id, instlmnt_id) {
      console.log('------------');
      console.log(app_id+' '+plan_id+' '+instlmnt_id);
      this.props.sendReport(app_id, plan_id, instlmnt_id);
   }

   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails, single_installment_payment } = this.props;

      var ap = this.props.match.params.appid;
      var pp = this.props.match.params.planid;
      var it = this.props.match.params.instlmntid;

      //finance charge
      if(single_installment_payment) {
         var fincharge = ((single_installment_payment[0].installment_amt/single_installment_payment[0].payment_term)*single_installment_payment[0].interest_rate)/100;
         //console.log(single_installment_payment)

      //total enclosed
         var totalEnclosed = parseFloat(single_installment_payment[0].installment_amt)+parseFloat(fincharge)

      }



      //last payment info

      var lastPymntInfo = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).pop() : '-';

      if(lastPymntInfo) {
         if(lastPymntInfo.late_fee_received !== null) {
            var lat = parseFloat(lastPymntInfo.late_fee_received);
         } else {
            lat = 0;
         }

         if(lastPymntInfo.additional_amount && lastPymntInfo.additional_amount !== null) {
            var addi = parseFloat(lastPymntInfo.additional_amount);
         } else {
            addi = 0;
         }
         var amt = parseFloat(lastPymntInfo.payment_amount);
         var fin = parseFloat(lastPymntInfo.fin_charge_amt);

         var totalLastPymnt = (amt+addi+lat+fin).toFixed(2);
      }
      //console.log(amt+' '+lat+' '+fin);
      //end last payment info

      //last payment rcvd
      var lastPymntRcvd = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).pop() : '-';
      
     //next pymnt due date
      var nxtPymntDueDate = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 0 ) : '';

      //payment missed count
      var countPymntMis = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.missed_flag == 1 ) : '';
      
      //last payment rcvd
      var pastPymntDue = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ).reverse() : '-';
      
      //late fee amount
      var lateFeeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.late_flag == 1 ) : '';

      if(lateFeeAmt) {
         var lateFeeTotal = 0;
         for (var i = 0; i<lateFeeAmt.length; i++) {
           lateFeeTotal += parseFloat(lateFeeAmt[i].late_fee_received);
         }
      }

      var lateFEE = (this.props.payment_billing_late_fee) ? this.props.payment_billing_late_fee[0].late_fee : 0;
      //console.log('lateeeeee'+lateFEE)
      //fin charge amount
      var finChargeAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.fin_charge_amt !== 0 && x.fin_charge_amt !== null ) : '';
      //console.log(finChargeAmt);
      if(finChargeAmt) {
         var finChargeTotal = 0;
         for(var i=0; i<finChargeAmt.length; i++) {
            finChargeTotal += parseFloat(finChargeAmt[i].fin_charge_amt);
         }
      }

      //current balance
      var getPaidBalnc = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ) : '';
      if(getPaidBalnc) {
         var paidInsTotal = 0;
         for (var i = 0; i<getPaidBalnc.length; i++) {
           paidInsTotal += parseFloat(getPaidBalnc[i].installment_amt);
         }
      }
      
      //balnce for table row
      var crdtBalnce = (mainPlan) ? mainPlan[0].credit_amount : 0;
      var blnc = (mainPlan) ? mainPlan[0].credit_amount : 0;

      //total additional amount
      var addiAmt = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.additional_amount !== 0 && x.additional_amount !== null ) : '';
      if(addiAmt) {
         var addiAmtTotal = 0;
         for(var i=0; i<addiAmt.length; i++) {
            addiAmtTotal += parseFloat(addiAmt[i].additional_amount);
         }
      }
      
      var appId = (mainPlan) ? mainPlan[0].application_id : '';

      var countPaymentDetailRow = (paymentPlanDetails) ? paymentPlanDetails.length : 0;

      var todayTime = new Date();
      var month = todayTime.getMonth() + 1;
      var day = todayTime.getDate();
      var year = todayTime.getFullYear();
      var today = month + "/" + day + "/" + year;
      //console.log(today);
      //check payment is due
      //console.log('-----------');
      //var app
      //console.log(this.props.payment_billing_late_fee)
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.invoice" />} match={this.props.match} />
            <div className="row">
            
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           
                           <li><a href="javascript:void(0);" onClick={() => this.sendCustomerReport(ap, pp, it)}><i className="mr-10 ti-email"></i> Email</a></li>
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                           </li>
                        </ul>
                     </div>
                     <div className="customer-invoice p-50" ref={el => (this.componentRef = el)}>
                        <div className="d-flex justify-content-between">
                           <div className="sender-address">
                              <div className="invoice-logo mb-30">
                                 <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid"  />
                              </div>

                           </div>
                           <div className="invoice-address text-right">
                              <span>Invoice: #{(mainPlan) ? mainPlan[0].patient_ac : '-'}</span>
                              <span>{today}</span>
                           </div>
                        </div>

                        <div className="d-flex justify-content-between mb-30 add-full-card customer-detail-info">

                            <div className="add-card">
                              <h4>{(mainPlan) ? (mainPlan[0].f_name + ' ' + mainPlan[0].m_name + ' ' + mainPlan[0].l_name) : '-'}</h4>
                              <div className="mb-5">
                                 {(mainPlan) ? (mainPlan[0].address1 + ', ' + mainPlan[0].address2 + ', ' + mainPlan[0].City + ', ' + mainPlan[0].region_name) : '-'}
                              </div>
                              <div>
                                 {(mainPlan) ? mainPlan[0].peimary_phone : '-'}
                              </div>
                           </div>

                            <div className="add-card">
                              <table>
                              <tbody>
                                <tr>
                                    <td><strong>Invoice Date :</strong></td>
                                    <td>{today}</td>
                                 </tr>
                                 <tr>
                                    <td><strong>Account Number :</strong></td>
                                    <td>
                                       {(mainPlan) ? mainPlan[0].patient_ac : '-'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Payment Due Date :</strong></td>
                                    <td>
                                       {(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Amount Due :</strong></td>
                                    <td>
                                       {(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Late Fee after({(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}) :</strong></td>
                                    <td>
                                       {(lateFEE) ? '$'+parseFloat(lateFEE).toFixed(2) : 0}
                                    </td>
                                 </tr>
                              </tbody>
                              </table>
                           </div>
                        </div>

                         
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt customer-loan-info">

                           <div className="add-card">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Loan Information</th>
                                 </tr>
                                 <tr>
                                    <td>Loan Ammount :</td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].credit_amount.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Payment Term :</td>
                                    <td>{(mainPlan) ? mainPlan[0].payment_term+' Month' : '-'}</td>
                                 </tr>
                                 
                                 <tr>
                                    <td>Interest Rate(%) :</td>
                                    <td>{(mainPlan) ? mainPlan[0].interest_rate+"%" : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Monthly Payment :</td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Current Balance :</td>
                                    <td>{(mainPlan) ? '$'+(mainPlan[0].credit_amount-paidInsTotal).toFixed(2) : '-'}</td>
                                 </tr>
                                 </tbody>
                              </table>
                           
                           </div>

                           <div className="add-card explaination">
                           
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Explanation of Amount Due</th>
                                 </tr>
                                 <tr>
                                    <td>Regular Monthly Payment :</td>
                                    <td>
                                       {(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Late Fee :</td>
                                    <td>
                                       {(lateFEE) ? '$'+parseFloat(lateFEE).toFixed(2) : 0}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td className="pad-bottom-40">Other Fee and charges :</td>
                                    <td className="pad-bottom-40">
                                       {'$'+parseFloat(fincharge).toFixed(2)}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Total Ammount Due :</strong></td>
                                    <td>
                                       {'$'+parseFloat(totalEnclosed).toFixed(2)}
                                    </td>
                                 </tr>
                              
                                 </tbody>
                              </table>
                           
                           </div>
                          
                           <div className="add-card explaination">
                              <table>
                                 <tbody>
                                 <tr>
                                    <th colSpan="2">Last Payment</th>
                                 </tr>
                                 <tr>
                                    <td>Regular Monthly Payment :</td>
                                    <td>
                                       {(lastPymntInfo) ? '$'+parseFloat(lastPymntInfo.payment_amount).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Payment Date :</td>
                                    <td>
                                       {(lastPymntInfo) ? lastPymntInfo.payment_date : ''}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Additional Ammount :</td>
                                    <td>
                                       {(lastPymntInfo) ? '$'+parseFloat(lastPymntInfo.additional_amount).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Late Fee :</td>
                                    <td>
                                       {(lastPymntInfo && lastPymntInfo.late_fee_received !== null) ? '$'+parseFloat(lastPymntInfo.late_fee_received).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td>Other Fee and charges :</td>
                                    <td>
                                       {(lastPymntInfo) ? '$'+parseFloat(lastPymntInfo.fin_charge_amt).toFixed(2) : '$0.00'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Total Ammount Paid :</strong></td>
                                    <td>
                                       {'$'+totalLastPymnt}
                                    </td>
                                 </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>

                        <div className="mb-30 paymt-voucher">
                           <h3 className="text-center mb-30">PAYMENT VOUCHER</h3>
                              <div className="d-flex add-full-card pymt-voucher-detail">
                                 
                                  <div className="add-card address-card">
                                    <p><strong>Health Partner</strong><br/>PO Box 522, Raleigh, NC 27614</p>
                                  </div>

                                  <div className="add-card bg-blue">
                                    <table>
                                    <tbody>
                                    <tr><th colSpan="2">Amount Due</th>
                                   </tr>
                                    
                                   <tr>
                                       <td>Invoice Date :</td>
                                       <td>{today}</td>
                                    </tr>
                                    <tr>
                                       <td>Account Number :</td>
                                       <td>
                                          {(mainPlan) ? mainPlan[0].patient_ac : '-'}
                                       </td>
                                    </tr>
                                    
                                    <tr>
                                       <td>Payment Due Date :</td>
                                       <td>
                                          {(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Amount Due :</td>
                                       <td>
                                          {(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Late Fee after({(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}) :</td>
                                       <td>
                                          {(lateFEE) ? '$'+parseFloat(lateFEE).toFixed(2) : 0}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Finance Charge</td>
                                       <td>{'$'+parseFloat(fincharge).toFixed(2)}</td>
                                    </tr>
                                    </tbody>
                                    </table>
                                  </div>
                              </div>
                              <div className="d-flex mb-30 add-full-card pymt-voucher-detail">
                                 
                                 
                                  <div className="add-card total-amt">
                                    <table>
                                    <tbody>
                                   <tr>
                                       <td>Total Amount Enclosed : {'$'+parseFloat(totalEnclosed).toFixed(2)}</td>
                                    </tr>
                                    </tbody>
                                    </table>
                                  </div>
                              </div>
                        </div>


                        
                        
                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer }) => {
   //console.log(creditApplication);
   const { nameExist, isEdit } = authUser;
   const { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL } = creditApplication;
   const { single_installment_payment, payment_billing_late_fee } = PaymentPlanReducer
   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL, single_installment_payment, payment_billing_late_fee }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, getSingleInstallment, sendReport
})(CustomerBilling);