/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Form, FormGroup, Label, Input } from 'reactstrap';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
import ReactToPrint from 'react-to-print';
// rct card
import { RctCard } from 'Components/RctCard/index';
import { isEmpty, isNumeric, isLength, pointDecimals } from '../../../validator/Validator';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import CryptoJS from 'crypto-js';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
//import { withRouter } from 'react-router-dom';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
   getPaymentMasterFeeOption, getLateFeeWaiverType, getFinChargeWaiverType, getSingleInstallment, insertPayPayment, clearRedirectURL
} from 'Actions';
class CustomerPaymentBilling extends Component {

   state = {
      check_date: '',
      calculateAmount: {
         credit_charge: 0,
         credit_pct: '',
         single_installment: [{


         }],

         invoice_rows: [{


         }],

         installment_amount: '',
         payment_installment_id: '',

         late_fee_waiver_type: '',
         fin_charge_waiver_type: '',

         late_fee_waiver_reason: '',
         fin_charge_waiver_reason: '',

         late_fee_waiver_comt: '',
         fin_charge_waiver_comt: '',

         late_fee_pct: '',

         waiver: '',
         payment_note: '',

         bank_name: '',
         txn_ref_no: '',

         check_no: '',
         check_date: '',

         ach_bank_name: '',
         ach_routing_no: '',
         ach_account_no: '',

         invoice_id: '',

         additional_amount: 0,
         paid_amount: 0,
         cosigner: 0,
      },

      late_fee_max_waiver: '',
      fin_charge_max_waiver: '',

      previous_late_fee: '',
      waiver_late_fee: '',

      previous_fin_charge: '',
      waiver_fin_charge: '',

      err: {
         late_fee_waiver_type: '',
         fin_charge_waiver_type: '',

         late_fee_waiver_reason: '',
         fin_charge_waiver_reason: '',

         late_fee_max_waiver: '',
         fin_charge_max_waiver: '',

         late_fee_max_waiver_id: '',
         fin_charge_max_waiver_id: '',

         late_fee_waiver_comt: '',
         fin_charge_waiver_comt: '',

         late_fee_pct: '',

         waiver: '',
         payment_note: '',

         bank_name: '',
         txn_ref_no: '',

         check_no: '',
         check_date: '',

         ach_bank_name: '',
         ach_routing_no: '',
         ach_account_no: '',
      },
      add_err: {

      },
      late_fee: 0,
      financial_charge: 0,

      final_late_fee: 0,
      final_fin_charge: 0,

      interest_rate: 0,
      payment_term: 0,
      total_pay: 0,
      nextTpay: true,
      nextTpayEdit: true,
      nextTpayEditElse: true,
      payment_method: 1,
      payment_in: 2,
      cheque_method_field: false,
      cheque_method_fields: false,
      credit_card_method_field: false,
      ach_method_field: true,
      mail_method_field: false,
      changeURL: 0,
      paid_flag: 0,
      opnSampleImgModal: false,
      application_id: '',
      cancelPayment: 0,
      payDetails: false,
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   opnSampleImgModal(path) {
      this.setState({ opnSampleImgModal: true })
   }

   opnViewSampleImgModalClose = () => {
      this.setState({ opnSampleImgModal: false })
   }

   validateAddSubmit() {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = dd + '/' + mm + '/' + yyyy;

      //compare
      var lateFeeApplyVal;
      if (this.props.single_installment_payment) {
         var d1 = this.props.single_installment_payment[0].late_fee_due;
         var d2 = this.props.single_installment_payment[0].fin_charge_due;
         if (d1 && d2) {
            lateFeeApplyVal = true;
         } else {
            lateFeeApplyVal = false;
         }
      }

      if (this.state.payment_method == 1) {
         return (
            this.state.add_err.ach_bank_name === '' &&
            this.state.add_err.ach_routing_no === '' &&
            this.state.add_err.ach_account_no === '' &&
            this.state.add_err.additional_amount === ''
         );
      } else if (this.state.payment_method == 2) {
         return (
            this.state.add_err.additional_amount === ''
         );
      } else if (this.state.payment_method == 3) {
         return (
            this.state.add_err.additional_amount === '' &&
            this.state.add_err.check_no === '' &&
            this.state.add_err.check_date === ''
         );
      } else {
         return (
            this.state.add_err.ach_bank_name === '' &&
            this.state.add_err.ach_routing_no === '' &&
            this.state.add_err.ach_account_no === '' &&
            this.state.add_err.additional_amount === ''
         );
      }


   }

   cancelInstallmentPayment() {
      this.setState({
         cancelPayment: 1,
         calculateAmount: ''
      });
   }

   payInstallmentPayment() {

      var data = {
         invoice_id: this.dec(this.props.match.params.invoice_id),

         payment_amount: this.state.calculateAmount.installment_amount,

         late_fee_recieved: (this.state.final_late_fee_bool != true) ? (this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due).toFixed(2) : this.state.final_late_fee,

         late_fee_waiver_id: this.state.calculateAmount.late_fee_waiver_reason,

         prev_late_fee: this.state.previous_late_fee,
         waiver_late_fee: this.state.waiver_late_fee,

         prev_fin_charge: this.state.previous_fin_charge,
         waiver_fin_charge: this.state.waiver_fin_charge,

         fin_charge_waiver_id: this.state.calculateAmount.fin_charge_waiver_reason,

         payment_date: "",

         //bank_name: this.state.calculateAmount.bank_name,
         //txn_ref_no: this.state.calculateAmount.txn_ref_no,
         ach_bank_name: this.state.calculateAmount.ach_bank_name,

         ach_routing_no: this.state.calculateAmount.ach_routing_no,

         ach_account_no: this.state.calculateAmount.ach_account_no,

         check_no: this.state.calculateAmount.check_no,

         check_date: this.state.calculateAmount.check_date,

         fin_charge_received: (this.state.final_fin_charge_bool != true) ? (this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due).toFixed(2) : this.state.final_fin_charge,

         late_fee_comt: this.state.calculateAmount.late_fee_waiver_comt,

         finance_charge_commt: this.state.calculateAmount.fin_charge_waiver_comt,

         comments: this.state.calculateAmount.payment_note,

         payment_in: this.state.payment_in,

         additional_amount: this.state.calculateAmount.additional_amount,

         due_date: this.props.single_installment_payment[0].due_date,

         //for edit part
         payment_installment_id: this.state.calculateAmount.invoice_id,
         payment_method: this.state.payment_method,
         allData: this.state.calculateAmount.invoice_rows,
         app_id: this.state.application_id,
         payment_id: this.props.single_installment_payment[0].payment_id,
         total_recived: this.props.single_installment_payment[0].total_recived,
         prv_invoice_status: (this.props.single_installment_payment[0].prv_invoice_details != null) ? this.props.single_installment_payment[0].prv_invoice_details[0].prv_invoice_status : null,
         last_invoice_status: this.props.single_installment_payment[0].last_invoice_status,

         creditPct: this.state.calculateAmount.credit_pct,
         creditChr: parseFloat(this.state.calculateAmount.credit_charge).toFixed(2),
         cosigner: this.state.calculateAmount.cosigner,
      }

      //console.log(data)
      //return false;

      this.props.insertPayPayment(data);
   }

   componentDidMount() {

      this.props.getPaymentMasterFeeOption();
      this.props.getSingleInstallment(this.dec(this.props.match.params.invoice_id));
   }

   onChangePaymentDetail(key, value) {

      const { add_err, calculateAmount } = this.state;
      switch (key) {
         case 'additional_amount':
            value = pointDecimals(value);
            var late_fin;
            var late_find;

            var d1A = this.state.calculateAmount.single_installment[0].late_fee_due;
            var d2A = this.state.calculateAmount.single_installment[0].fin_charge_due;
            if (d1A && d2A) {
               if (!this.state.calculateAmount.late_fee_waiver_reason && !this.state.calculateAmount.fin_charge_waiver_reason) {
                  late_fin = this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due + this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due;

               } else {
                  late_find = parseFloat(this.state.final_late_fee) + parseFloat(this.state.final_fin_charge);
               }
            } else {
               late_fin = 0;
            }
            if (value == '') {
               value = 0;
               add_err[key] = "Amount Paid can't be blank.";
            } else if ((value <= late_fin || value <= late_find) && d1A < d2A) {
               add_err[key] = "Value should be more than sum of late fee and financial charge.";
            } else if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
               value = 0;
            } else {
               add_err[key] = "";
            }
            break;
         case 'late_fee_waiver_type':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver type";
               add_err['late_fee_waiver_reason'] = "Select waiver reason";
               var tp = this.state.calculateAmount.single_installment[0].payment_amount + parseFloat(this.state.calculateAmount.single_installment[0].fin_charge_due) + parseFloat(this.state.calculateAmount.single_installment[0].late_fee_due);


               calculateAmount.late_fee_waiver_reason = ''

               this.setState({ final_late_fee: parseFloat(this.state.late_fee), total_pay: tp, calculateAmount: calculateAmount })

            } else {
               add_err[key] = '';
               this.props.getLateFeeWaiverType(value);
            }
            break;
         case 'late_fee_waiver_reason':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver reason";
               var tp = this.state.calculateAmount.single_installment[0].payment_amount + parseFloat(this.state.calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(this.state.calculateAmount.single_installment[0].fin_charge_due) + parseFloat(this.state.calculateAmount.single_installment[0].previous_late_fee) + parseFloat(this.state.calculateAmount.single_installment[0].late_fee_due);

               var prev_lfee = this.state.calculateAmount.single_installment[0].previous_late_fee

               var wav_lfee = 0

               this.setState({ final_late_fee: 0, previous_late_fee: prev_lfee, waiver_late_fee: wav_lfee, total_pay: tp })
            } else {
               add_err[key] = '';
               var val = value.split('-');
               value = val[0];
               var finalLateFee = (this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due) - ((this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due) * val[1]) / 100;

               var prev_lfee = (this.state.calculateAmount.single_installment[0].previous_late_fee - (this.state.calculateAmount.single_installment[0].previous_late_fee * val[1]) / 100)

               var wav_lfee = (this.state.calculateAmount.single_installment[0].late_fee_due - (this.state.calculateAmount.single_installment[0].late_fee_due * val[1]) / 100)

               if (finalLateFee !== 0) {
                  var tp = this.state.calculateAmount.single_installment[0].payment_amount + finalLateFee;
               } else {
                  var tp = this.state.calculateAmount.single_installment[0].payment_amount + this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due + this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due;
               }

               /*if(this.state.final_fin_charge !==0) {
                  var tp = this.state.calculateAmount.installment_amount + finalLateFee + this.state.final_fin_charge;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + finalLateFee;
               }*/
               this.setState({ late_fee_max_waiver: val[1], final_late_fee: finalLateFee, previous_late_fee: prev_lfee, waiver_late_fee: wav_lfee, total_pay: tp })
            }
            break;
         case 'late_fee_waiver_comt':
            if (isEmpty(value)) {
               add_err[key] = "Comment field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'fin_charge_waiver_type':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver type";
               add_err['fin_charge_waiver_reason'] = "Select waiver reason";
               var tp = this.state.calculateAmount.installment_amount + parseFloat(this.state.financial_charge) + parseFloat(this.state.late_fee);
               calculateAmount.fin_charge_waiver_reason = ''
               this.setState({ final_fin_charge: parseFloat(this.state.financial_charge), total_pay: tp, calculateAmount: calculateAmount })
               //this.setState({waiver_type:''})
            } else {
               add_err[key] = '';
               this.props.getFinChargeWaiverType(value);
            }
            break;
         case 'fin_charge_waiver_reason':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver reason";
               var tp = this.state.calculateAmount.single_installment[0].payment_amount + parseFloat(this.state.calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(this.state.calculateAmount.single_installment[0].fin_charge_due) + parseFloat(this.state.calculateAmount.single_installment[0].previous_late_fee) + parseFloat(this.state.calculateAmount.single_installment[0].late_fee_due);

               var prev_fch = this.state.calculateAmount.single_installment[0].previous_fin_charge

               var wav_fch = 0

               this.setState({ final_fin_charge: 0, previous_fin_charge: prev_fch, waiver_fin_charge: wav_fch, total_pay: tp })
            } else {
               add_err[key] = '';
               var val = value.split('-');
               value = val[0];

               var finalFinCharge = (this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due) - ((this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due) * val[1]) / 100;

               var prev_fch = (this.state.calculateAmount.single_installment[0].previous_fin_charge - (this.state.calculateAmount.single_installment[0].previous_fin_charge * val[1]) / 100)

               var wav_fch = (this.state.calculateAmount.single_installment[0].fin_charge_due - (this.state.calculateAmount.single_installment[0].fin_charge_due * val[1]) / 100)

               if (finalFinCharge !== 0) {
                  var tp = this.state.calculateAmount.single_installment[0].payment_amount + finalFinCharge;
               } else {
                  var tp = this.state.calculateAmount.single_installment[0].payment_amount + this.state.calculateAmount.single_installment[0].previous_fin_charge + this.state.calculateAmount.single_installment[0].fin_charge_due + this.state.calculateAmount.single_installment[0].previous_late_fee + this.state.calculateAmount.single_installment[0].late_fee_due;
               }

               /*if(this.state.final_late_fee !==0) {
                  var tp = this.state.calculateAmount.installment_amount + finalFinCharge + this.state.final_late_fee;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + finalFinCharge;
               }*/
               this.setState({ fin_charge_max_waiver: val[1], final_fin_charge: finalFinCharge, previous_fin_charge: prev_fch, waiver_fin_charge: wav_fch, total_pay: tp })

            }
            break;
         case 'fin_charge_waiver_comt':
            if (isEmpty(value)) {
               add_err[key] = "Comment field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'financial_charge':
            if (isEmpty(value)) {
               add_err[key] = "Value field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'payment_note':
            if (isEmpty(value)) {
               add_err[key] = "Note field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'payment_method':
            if (value == 1) {
               calculateAmount.bank_name = '';
               calculateAmount.txn_ref_no = '';
               calculateAmount.credit_charge = 0;
               calculateAmount.credit_pct = '';
               //this.setState({calculateAmount: calculateAmount})
               this.setState({ payment_method: value, cheque_method_field: false, credit_card_method_field: false, ach_method_field: true, mail_method_field: false, calculateAmount: calculateAmount })
            } else if (value == 2) {
               var amt1, amt2, amt3, amt4, amt5, amt6, amt7
               if (this.state.final_fin_charge) {
                  amt1 = this.state.final_fin_charge
               } else {
                  amt1 = 0
               }

               if (this.state.final_late_fee) {
                  amt2 = this.state.final_late_fee
               } else {
                  amt2 = 0
               }

               if (this.state.calculateAmount.single_installment[0].previous_fin_charge) {
                  amt3 = this.state.calculateAmount.single_installment[0].previous_fin_charge
               } else {
                  amt3 = 0
               }

               if (this.state.calculateAmount.single_installment[0].fin_charge_due) {
                  amt4 = this.state.calculateAmount.single_installment[0].fin_charge_due
               } else {
                  amt4 = 0
               }

               if (this.state.calculateAmount.single_installment[0].previous_late_fee) {
                  amt5 = this.state.calculateAmount.single_installment[0].previous_late_fee
               } else {
                  amt5 = 0
               }

               if (this.state.calculateAmount.single_installment[0].late_fee_due) {
                  amt6 = this.state.calculateAmount.single_installment[0].late_fee_due
               } else {
                  amt6 = 0
               }

               if (this.state.calculateAmount.single_installment[0].total_recived !== null && this.state.calculateAmount.single_installment[0].total_credit_recived !== null) {
                  amt7 = this.state.calculateAmount.single_installment[0].total_recived - this.state.calculateAmount.single_installment[0].total_credit_recived
               } else {
                  amt7 = 0
               }

               var credit_ch = (((parseFloat(amt1) + parseFloat(amt2) + parseFloat(amt3) + parseFloat(amt4) + parseFloat(amt5) + parseFloat(amt6) + parseFloat(calculateAmount.single_installment[0].payment_amount)) - parseFloat(amt7)) * this.props.credit_charge) / 100

               calculateAmount.ach_bank_name = '';
               calculateAmount.ach_routing_no = '';
               calculateAmount.ach_account_no = '';
               calculateAmount.bank_name = '';
               calculateAmount.txn_ref_no = '';
               calculateAmount.credit_charge = credit_ch;
               calculateAmount.credit_pct = this.props.credit_charge;
               this.setState({ payment_method: value, cheque_method_field: false, credit_card_method_field: true, ach_method_field: false, mail_method_field: false, calculateAmount: calculateAmount })
            } else if (value == 3) {
               calculateAmount.ach_bank_name = '';
               calculateAmount.ach_routing_no = '';
               calculateAmount.ach_account_no = '';
               calculateAmount.bank_name = '';
               calculateAmount.txn_ref_no = '';
               calculateAmount.credit_charge = 0;
               calculateAmount.credit_pct = '';
               this.setState({ payment_method: value, cheque_method_field: false, credit_card_method_field: false, ach_method_field: false, mail_method_field: true, calculateAmount: calculateAmount })
            } else {
               calculateAmount.ach_bank_name = '';
               calculateAmount.ach_routing_no = '';
               calculateAmount.ach_account_no = '';
               calculateAmount.bank_name = '';
               calculateAmount.txn_ref_no = '';
               calculateAmount.credit_charge = 0;
               calculateAmount.credit_pct = '';
               this.setState({ payment_method: 0, cheque_method_field: false, credit_card_method_field: false, ach_method_field: false, mail_method_field: false, calculateAmount: calculateAmount })
            }
            break;
         case 'bank_name':
            if (isEmpty(value)) {
               add_err[key] = "Bank field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'txn_ref_no':
            if (isEmpty(value)) {
               add_err[key] = "Txn ref no can't be blank";
            } else {
               add_err[key] = ''
            }
            break;
         case 'check_no':
            if (isEmpty(value)) {
               add_err[key] = "Check no. can't be blank";
            } else if (isNumeric(value)) {
               add_err[key] = "Allow only numeric.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'check_date':
            if (value == null) {
               add_err[key] = "Select check date";
               this.setState({ check_date: '' })
            } else {
               this.setState({ check_date: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            break;
         case 'ach_bank_name':
            if (isEmpty(value)) {
               add_err[key] = "Bank field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'ach_routing_no':
            if (isEmpty(value)) {
               add_err[key] = "Txn ref no can't be blank";
            } else {
               add_err[key] = ''
            }
            break;
         case 'ach_account_no':
            if (isEmpty(value)) {
               add_err[key] = "Account no can't be blank";
            } else if (isNumeric(value)) {
               add_err[key] = "Allow only numeric.";
            } else {
               add_err[key] = ''
            }
            break;
         case 'payment_in':
            if (value == 1) {
               this.setState({ payment_in: 1 })
               add_err[key] = '';
            } else if (value == 2) {
               this.setState({ payment_in: 2 })
               add_err[key] = '';
            } else {
               add_err[key] = "Select payment option";
            }
            break;
         case 'cosigner':
            value = (this.state.calculateAmount.cosigner) ? 0 : 1;
            calculateAmount.ach_routing_no = '';
            calculateAmount.ach_bank_name = '';
            calculateAmount.ach_account_no = '';
            add_err['ach_bank_name'] = 'Bank field can\'t be blank';
            add_err['ach_routing_no'] = 'Txn ref no can\'t be blank';
            add_err['ach_account_no'] = 'Account no can\'t be blank';
            if (value == 1) {
               let customerCoBankP = this.props.customerCoBank && this.props.customerCoBank.map(bankData => {
                  if (bankData.primary_bank === 1) {
                     bankData.checked = true;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     add_err.ach_bank_name = '';
                     add_err.ach_routing_no = '';
                     add_err.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });
               /*let primarybank = this.props.customerCoBank.filter(x => x.primary_bank == 1)
               if (primarybank.length > 0) {
                  calculateAmount.ach_routing_no = primarybank[0].rounting_no;
                  calculateAmount.ach_bank_name = primarybank[0].bank_name;
                  calculateAmount.ach_account_no = primarybank[0].bank_ac;
                  add_err['ach_bank_name'] = '';
                  add_err['ach_routing_no'] = '';
                  add_err['ach_account_no'] = '';
               } else {
                  calculateAmount.ach_routing_no = 'Bank field can\'t be blank';
                  calculateAmount.ach_bank_name = 'Txn ref no can\'t be blank';
                  calculateAmount.ach_account_no = 'Account no can\'t be blank';
                  add_err['ach_bank_name'] = '';
                  add_err['ach_routing_no'] = '';
                  add_err['ach_account_no'] = '';
               }*/
               this.setState({ bankDetails: (this.props.customerCoBank != '') ? customerCoBankP : null })
            } else {
               let customerBankP = this.props.customerBank && this.props.customerBank.map(bankData => {
                  if (bankData.primary_bank === 1) {
                     bankData.checked = true;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     add_err.ach_bank_name = '';
                     add_err.ach_routing_no = '';
                     add_err.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });

               /*let primarybank = this.props.customerBank.filter(x => x.primary_bank == 1)
               if (primarybank.length > 0) {
                  calculateAmount.ach_routing_no = primarybank[0].rounting_no;
                  calculateAmount.ach_bank_name = primarybank[0].bank_name;
                  calculateAmount.ach_account_no = primarybank[0].bank_ac;
                  add_err['ach_bank_name'] = '';
                  add_err['ach_routing_no'] = '';
                  add_err['ach_account_no'] = '';
               } else {
                  calculateAmount.ach_routing_no = 'Bank field can\'t be blank';
                  calculateAmount.ach_bank_name = 'Txn ref no can\'t be blank';
                  calculateAmount.ach_account_no = 'Account no can\'t be blank';
                  add_err['ach_bank_name'] = '';
                  add_err['ach_routing_no'] = '';
                  add_err['ach_account_no'] = '';
               }*/
               this.setState({ bankDetails: (this.props.customerBank != '') ? customerBankP : null })
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         calculateAmount: {
            ...this.state.calculateAmount,
            [key]: value
         }
      });

   }

   componentWillReceiveProps(nextProps) {
      let { calculateAmount, add_err } = this.state;

      (nextProps.rediretURL != '') ? this.setState({ changeURL: nextProps.rediretURL }) : '';



      if (nextProps.single_installment_payment === undefined) {
         this.setState({
            nextTpay: true
         })
      }
      if (nextProps.single_installment_payment_edit === undefined) {
         this.setState({
            nextTpayEdit: true, nextTpayEditElse: true
         })
      }
      if (nextProps.single_installment_payment && this.state.nextTpay) {

         calculateAmount.single_installment = nextProps.single_installment_payment;
         calculateAmount.invoice_rows = nextProps.single_invoice_rows;

         calculateAmount.ach_bank_name = nextProps.single_installment_payment[0].ach_bank_name;
         calculateAmount.ach_routing_no = nextProps.single_installment_payment[0].ach_routing_no;
         calculateAmount.ach_account_no = nextProps.single_installment_payment[0].ach_account_no;
         calculateAmount.check_no = nextProps.single_installment_payment[0].check_no;
         //calculateAmount.check_date = moment(nextProps.single_installment_payment[0].check_date).format('YYYY-MM-DD');
         calculateAmount.check_date = nextProps.single_installment_payment[0].check_date;
         //calculateAmount.additional_amount = nextProps.single_installment_payment[0].additional_amount;
         //calculateAmount.paid_amount = nextProps.single_installment_payment[0].paid_amount;

         calculateAmount.late_fee_waiver_comt = nextProps.single_installment_payment[0].late_fee_waiver_comt;
         calculateAmount.fin_charge_waiver_comt = nextProps.single_installment_payment[0].fin_charge_waiver_comt;

         calculateAmount.payment_note = nextProps.single_installment_payment[0].comments;

         calculateAmount.credit_charge = nextProps.single_installment_payment[0].credit_charge_amt;
         calculateAmount.credit_pct = nextProps.single_installment_payment[0].credit_charge_pct;

         let adderr = {};
         if (nextProps.single_installment_payment[0].paid_flag !== 3) {
            adderr = {
               late_fee_waiver_type: '',
               fin_charge_waiver_type: '',
               late_fee_waiver_reason: '',
               fin_charge_waiver_reason: '',
               payment_note: '',
               additional_amount: '',
               bank_name: '',
               txn_ref_no: '',
               /*ach_bank_name: '',
               ach_routing_no: '',
               ach_account_no: '',*/
               payment_in: '',
               invoice_id: '',
            };

            (nextProps.single_installment_payment[0].ach_bank_name) ? adderr.ach_bank_name = '' : '';
            (nextProps.single_installment_payment[0].ach_routing_no) ? adderr.ach_routing_no = '' : '';
            (nextProps.single_installment_payment[0].ach_account_no) ? adderr.ach_account_no = '' : '';
            (nextProps.single_installment_payment[0].check_no) ? adderr.check_no = '' : '';
            (nextProps.single_installment_payment[0].check_date) ? adderr.check_date = '' : '';

         }

         if (nextProps.single_installment_payment[0].payment_method == 1) {

            this.setState({ credit_card_method_field: false, ach_method_field: true, mail_method_field: false });

         } else if (nextProps.single_installment_payment[0].payment_method == 2) {

            this.setState({ credit_card_method_field: true, ach_method_field: false, mail_method_field: false });

         } else if (nextProps.single_installment_payment[0].payment_method == 3) {

            this.setState({ credit_card_method_field: false, ach_method_field: false, mail_method_field: true });

         } else {

            this.setState({ credit_card_method_field: false, ach_method_field: true, mail_method_field: false });

         }


         var pymthd = (nextProps.single_installment_payment[0].payment_method) ? nextProps.single_installment_payment[0].payment_method : 1;
         /****************bank detials */
         let checkPayer = 0;
         let bankDetails = null;
         if (nextProps.single_installment_payment[0].paid_flag == 3) {
            if (nextProps.customerCoBank != '') {
               let customerCoBankP = nextProps.customerCoBank.map(bankData => {
                  if (bankData.primary_bank === 1) {
                     bankData.checked = true;
                     checkPayer = 1;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     adderr.ach_bank_name = '';
                     adderr.ach_routing_no = '';
                     adderr.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });
               bankDetails = customerCoBankP;
            } else if (nextProps.customerBank != '') {
               let customerBankP = nextProps.customerBank.map(bankData => {
                  if (bankData.primary_bank === 1) {
                     bankData.checked = true;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     adderr.ach_bank_name = '';
                     adderr.ach_routing_no = '';
                     adderr.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });
               bankDetails = customerBankP;
            }
         } else {

            if (nextProps.customerCoBank != '') {
               let customerCoBankP = nextProps.customerCoBank.map(bankData => {
                  if (bankData.rounting_no == nextProps.single_installment_payment[0].ach_routing_no && bankData.bank_name == nextProps.single_installment_payment[0].ach_bank_name && bankData.bank_ac == nextProps.single_installment_payment[0].ach_account_no) {
                     checkPayer = 1;
                     bankData.checked = true;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     adderr.ach_bank_name = '';
                     adderr.ach_routing_no = '';
                     adderr.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });
               bankDetails = customerCoBankP;
            }
            if (nextProps.customerBank != '' && checkPayer == 0) {
               let customerBankP = nextProps.customerBank.map(bankData => {
                  if (bankData.rounting_no == nextProps.single_installment_payment[0].ach_routing_no && bankData.bank_name == nextProps.single_installment_payment[0].ach_bank_name && bankData.bank_ac == nextProps.single_installment_payment[0].ach_account_no) {
                     checkPayer: 0;
                     bankData.checked = true;
                     calculateAmount.ach_routing_no = bankData.rounting_no;
                     calculateAmount.ach_bank_name = bankData.bank_name;
                     calculateAmount.ach_account_no = bankData.bank_ac;
                     adderr.ach_bank_name = '';
                     adderr.ach_routing_no = '';
                     adderr.ach_account_no = '';
                  } else {
                     bankData.checked = false;
                  }
                  return bankData;
               });
               bankDetails = customerBankP;
            }

         }
         calculateAmount.cosigner = checkPayer;
         this.setState({
            calculateAmount: calculateAmount,
            previous_late_fee: nextProps.single_installment_payment[0].late_fee_due,
            previous_fin_charge: nextProps.single_installment_payment[0].fin_charge_due,
            check_date: (nextProps.single_installment_payment[0].check_date) ? new Date(nextProps.single_installment_payment[0].check_date) : '',
            //payment_in: nextProps.single_installment_payment[0].paid_flag,
            payment_method: pymthd,
            nextTpay: false,
            application_id: (nextProps.invoice_rows_appid !== undefined) ? nextProps.invoice_rows_appid[0].application_id : '',
            add_err: adderr,
            bankDetails: bankDetails,
         })



      }

      if (nextProps.single_installment_payment_edit !== undefined && nextProps.single_installment_payment_edit.length !== 0 && this.state.nextTpayEdit) {


         calculateAmount.late_fee_waiver_type = nextProps.single_installment_payment_edit[0].mdv_late_id;
         calculateAmount.fin_charge_waiver_type = nextProps.single_installment_payment_edit[0].mdv_fin_id;

         calculateAmount.late_fee_waiver_reason = nextProps.single_installment_payment_edit[0].late_fee_waivers_id;

         calculateAmount.fin_charge_waiver_reason = nextProps.single_installment_payment_edit[0].fin_charge_waiver_id;

         ///final late fee
         var pv_lf = nextProps.single_installment_payment[0].previous_late_fee;
         var lf_rcvd = nextProps.single_installment_payment[0].late_fee_received;
         var lf_due = nextProps.single_installment_payment[0].late_fee_due;
         var lt_prc = nextProps.single_installment_payment_edit[0].mdv_late_percentage;
         var lt_bool;
         if (lt_prc > 0) {
            //var sum_pv_lf = ((parseFloat(pv_lf) + parseFloat(lf_rcvd)) - (((parseFloat(pv_lf) + parseFloat(lf_rcvd)) * parseFloat(lt_prc)) / 100)).toFixed(2);
            var sum_pv_lf = parseFloat(lf_rcvd);
            lt_bool = true;
         } else {
            var sum_pv_lf = parseFloat(pv_lf) + parseFloat(lf_due).toFixed(2);
            lt_bool = false;
         }


         ///final fin charge

         var pv_fc = nextProps.single_installment_payment[0].previous_fin_charge;
         var fc_rcvd = nextProps.single_installment_payment[0].fin_charge_received;
         var fc_due = nextProps.single_installment_payment[0].fin_charge_due;
         var fc_prc = nextProps.single_installment_payment_edit[0].mdv_fin_percentage;
         var fin_bool;
         if (fc_prc > 0) {
            //var sum_pv_fc = ((parseFloat(pv_fc) + parseFloat(fc_rcvd)) - (((parseFloat(pv_fc) + parseFloat(fc_rcvd)) * parseFloat(fc_prc)) / 100)).toFixed(2);
            var sum_pv_fc = parseFloat(fc_rcvd);
            fin_bool = true;
         } else {
            var sum_pv_fc = parseFloat(pv_fc) + parseFloat(fc_due).toFixed(2);
            fin_bool = false;
         }



         //calculateAmount.additional_amount = parseFloat(nextProps.single_installment_payment[0].paid_amount) + parseFloat(nextProps.single_installment_payment[0].additional_amount) + parseFloat(sum_pv_lf) + parseFloat(sum_pv_fc)
         //calculateAmount.additional_amount = (isNaN(calculateAmount.additional_amount))?calculateAmount.additional_amount:calculateAmount.additional_amount.toFixed(2)
         calculateAmount.additional_amount = (nextProps.single_installment_payment[0].amount_paid) ? parseFloat(nextProps.single_installment_payment[0].amount_paid) : ''

         calculateAmount.single_installment[0].total_recived = 0;
         calculateAmount.single_installment[0].total_credit_recived = 0;
         calculateAmount.single_installment[0].total_credit_recived = nextProps.single_installment_payment[0].prv_invoice_details && nextProps.single_installment_payment[0].prv_invoice_details.reduce((total, amount, index, array) => {
            return total += amount.credit_charge_amt;
         }, 0);
         calculateAmount.single_installment[0].total_recived = nextProps.single_installment_payment[0].prv_invoice_details && nextProps.single_installment_payment[0].prv_invoice_details != null && nextProps.single_installment_payment[0].prv_invoice_details.reduce((total, amount, index, array) => {
            return total += amount.total_recived;
         }, 0);
         calculateAmount.single_installment[0].total_recived = (calculateAmount.single_installment[0].total_recived == null) ? 0 : calculateAmount.single_installment[0].total_recived;
         calculateAmount.single_installment[0].total_credit_recived = (calculateAmount.single_installment[0].total_credit_recived) ? calculateAmount.single_installment[0].total_credit_recived : 0;
         //console.log(calculateAmount.single_installment)


         var amt1, amt2, amt3, amt4, amt5, amt6, amt7
         if (sum_pv_fc) {
            amt1 = sum_pv_fc
         } else {
            amt1 = 0
         }

         if (sum_pv_lf) {
            amt2 = sum_pv_lf
         } else {
            amt2 = 0
         }

         if (nextProps.single_installment_payment[0].previous_fin_charge) {
            amt3 = nextProps.single_installment_payment[0].previous_fin_charge
         } else {
            amt3 = 0
         }

         if (nextProps.single_installment_payment[0].fin_charge_due) {
            amt4 = nextProps.single_installment_payment[0].fin_charge_due
         } else {
            amt4 = 0
         }

         if (nextProps.single_installment_payment[0].previous_late_fee) {
            amt5 = nextProps.single_installment_payment[0].previous_late_fee
         } else {
            amt5 = 0
         }

         if (nextProps.single_installment_payment[0].late_fee_due) {
            amt6 = nextProps.single_installment_payment[0].late_fee_due
         } else {
            amt6 = 0
         }

         if (nextProps.single_installment_payment[0].total_recived !== null && nextProps.single_installment_payment[0].total_credit_recived !== null) {
            amt7 = nextProps.single_installment_payment[0].total_recived - nextProps.single_installment_payment[0].total_credit_recived
         } else {
            amt7 = 0
         }

         var credit_ch = (((parseFloat(amt1) + parseFloat(amt2) + parseFloat(amt3) + parseFloat(amt4) + parseFloat(amt5) + parseFloat(amt6) + parseFloat(nextProps.single_installment_payment[0].payment_amount)) - parseFloat(amt7)) * nextProps.credit_charge) / 100




         if (nextProps.single_installment_payment[0].last_invoice_status == 4 && nextProps.single_installment_payment[0].prv_invoice_details[0].prv_invoice_status == 4) {
            calculateAmount.additional_amount = '';
            calculateAmount.ach_account_no = '';
            calculateAmount.ach_bank_name = '';
            calculateAmount.ach_routing_no = '';
            calculateAmount.check_date = '';
            calculateAmount.check_no = '';
            calculateAmount.payment_note = '';
            calculateAmount.credit_charge = credit_ch;
            this.setState({ check_date: '' })
         }
         this.setState({
            calculateAmount: calculateAmount,
            final_late_fee: sum_pv_lf,
            final_late_fee_bool: lt_bool,
            final_fin_charge: sum_pv_fc,
            final_fin_charge_bool: fin_bool,
            late_fee_max_waiver: nextProps.single_installment_payment_edit[0].mdv_late_percentage,
            fin_charge_max_waiver: nextProps.single_installment_payment_edit[0].mdv_fin_percentage,
            nextTpayEdit: false,
         })

      } else if (nextProps.single_installment_payment_edit !== undefined && nextProps.single_installment_payment_edit.length == 0 && this.state.nextTpayEdit) {
         calculateAmount.additional_amount = parseFloat(nextProps.single_installment_payment[0].paid_amount) + parseFloat(nextProps.single_installment_payment[0].additional_amount);
         calculateAmount.additional_amount = (isNaN(calculateAmount.additional_amount)) ? calculateAmount.additional_amount : calculateAmount.additional_amount.toFixed(2)

         this.setState({
            calculateAmount: calculateAmount,
            nextTpayEditElse: false,
         })
      }
   }


   goBack() {
      this.props.history.goBack(-1)
   }

   viewPayDetails() {
      this.setState({ payDetails: true })
   }
   viewPayDetailsClose() {
      this.setState({ payDetails: false })
   }
   onSelectBank(bank) {
      const { calculateAmount, add_err } = this.state;
      bank.checked = !bank.checked;
      let bankDetails = this.state.bankDetails.map(bankData => {
         if (bankData.id === bank.id) {
            bankData.checked = true;
            calculateAmount.ach_routing_no = bankData.rounting_no;
            calculateAmount.ach_bank_name = bankData.bank_name;
            calculateAmount.ach_account_no = bankData.bank_ac;
            add_err.ach_bank_name = '';
            add_err.ach_routing_no = '';
            add_err.ach_account_no = '';
         } else {
            bankData.checked = false;
         }
         return bankData;
      });
      this.setState({ bankDetails, calculateAmount, add_err });
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {
         MuiIconButton: {
            root: {
               padding: "0"
            },
         },
         MuiPrivateSwitchBase: {
            input: {
               height: "100% !important"
            }
         },
         MuiSvgIcon: {
            root: {
               'color': "#0E5D97",
               'width': '15px',
               'height': '15px'
            }
         },
         MuiFormLabel: {
            root: {
               'font-size': '12px',
               "font-weight": 600,
               color: '#464D69'
            }
         },
         MuiFormControlLabel: {
            root: {
               'margin-left':'0px'
            }
         }
      }
   })
   render() {
      let { single_installment_payment, payment_billing_late_fee } = this.props;

      let { calculateAmount } = this.state;



      if (this.props.redirectURL === 1) {
         return (<Redirect to={'/customer/dashboard'} />);
      }
      if (this.state.cancelPayment == 1) {
         return (<Redirect to={"/customer/dashboard"} />);
      }

      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = dd + '/' + mm + '/' + yyyy;

      //compare
      var lateFeeApply;
      if (single_installment_payment) {
         var d1 = single_installment_payment[0].late_fee_due;
         var d2 = single_installment_payment[0].fin_charge_due;
         if (d1 && d2) {
            lateFeeApply = true;
         } else {
            lateFeeApply = false;
         }
      }



      const financial_charges_option = this.props.financial_charges_option;
      const late_fee_waivers_option = this.props.late_fee_waivers_option;
      const financial_charges_waiver_option = this.props.financial_charges_waiver_option;
      const late_fee_waiver_type_reason_option = this.props.late_fee_waiver_type_reason_option;
      const fin_charge_waiver_type_reason_option = this.props.fin_charge_waiver_type_reason_option;
      const payment_method_option = this.props.payment_method_option;


      return (
         <div className="invoice-wrapper">

            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li><ReactToPrint
                              trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                              content={() => this.componentRef}
                           /></li>
                        </ul>
                     </div>
                     <div className="pt-5 pb-5 px-50 customer-pymnt-single" ref={el => (this.componentRef = el)}>
                        <div className="table-responsive">
                           <div className="modal-body page-form-outer text-left billing-fields-container">
                              <Form>
                                 <h2 className="text-center mb-10">Customer Invoice</h2>
                                 <div className="row">
                                    <div className="col-sm-6">
                                       <h5 className="mb-5 text-left">Customer Name : {(this.props.invoice_rows_appid) ? this.props.invoice_rows_appid[0].f_name + ' ' + this.props.invoice_rows_appid[0].m_name + ' ' + this.props.invoice_rows_appid[0].l_name : ''}</h5>
                                       <h5 className="mb-5 text-left">Address : {(this.props.invoice_rows_appid) ? this.props.invoice_rows_appid[0].address1 + ' ' + this.props.invoice_rows_appid[0].address2 : ''}</h5>
                                       <h5 className="mb-5 text-left">{(this.props.invoice_rows_appid) ? this.props.invoice_rows_appid[0].City + ', ' + this.props.invoice_rows_appid[0].state_name + ', ' + this.props.invoice_rows_appid[0].zip_code : ''}</h5>
                                    </div>
                                    <div className="col-sm-6">
                                       <h5 className="text-right">Invoice No. #{calculateAmount.single_installment[0].invoice_number}</h5>
                                       {/*<h4 className="text-right mb-10">Total Amount Due: {(calculateAmount.single_installment) ? '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount)).toFixed(2) : '$0.00'}</h4>*/}
                                       <h5 className="text-right">Invoice Due Date: {(single_installment_payment) ? single_installment_payment[0].due_date : '$0.00'}</h5>

                                    </div>
                                 </div>


                                 <div className="row payment-method">
                                    <div className="col-md-12 mt-10">
                                       <MuiThemeProvider theme={this.getMuiTheme()}>
                                          <FormControl component="fieldset" required>
                                             <FormLabel component="legend">Payment Method</FormLabel>
                                             <RadioGroup row aria-label="action_type" name="action_type" value={this.state.payment_method.toString()} onChange={(e) => this.onChangePaymentDetail('payment_method', e.target.value)} >
                                                {payment_method_option && payment_method_option.map(function (option, idx) {
                                                   return <FormControlLabel key={idx} value={option.status_id.toString()} control={<Radio />} label={option.value} />
                                                }.bind(this))}
                                             </RadioGroup>
                                          </FormControl>
                                       </MuiThemeProvider>
                                       {/*<FormGroup tag="fieldset">
                                          <Label>Payment Method</Label>

                                          {payment_method_option && payment_method_option.map((option, key) => (

                                             <FormGroup check key={key}>
                                                <Label check title={option.value}>
                                                   <Input
                                                      type="radio"
                                                      name="payment_method"
                                                      value={option.status_id}
                                                      checked={(this.state.payment_method == option.status_id) ? true : false}
                                                      onChange={(e) => this.onChangePaymentDetail('payment_method', e.target.value)}
                                                   />{' '}
                                                   {option.value}
                                                </Label>
                                             </FormGroup>

                                          ))}

                                          </FormGroup>*/}
                                    </div>
                                 </div>


                                 {this.state.cheque_method_fields &&
                                    <div className="row cheque-method-fields">
                                       <div className="col-md-6">
                                          <FormGroup>
                                             <Label for="bank_name">Bank Name<span className="required-field">*</span></Label><br />
                                             <TextField
                                                type="text"
                                                name="bank_name"
                                                id="bank_name"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Bank Name"
                                                value={(this.state.calculateAmount.bank_name) ? this.state.calculateAmount.bank_name : ''}
                                                error={(this.state.add_err.bank_name) ? true : false}
                                                helperText={(this.state.add_err.bank_name != '') ? this.state.add_err.bank_name : ''}
                                                onChange={(e) => this.onChangePaymentDetail('bank_name', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>

                                       <div className="col-md-6">
                                          <FormGroup>
                                             <Label for="txn_ref_no">Transaction ID / Cheque No.<span className="required-field">*</span></Label><br />
                                             <TextField
                                                type="text"
                                                name="txn_ref_no"
                                                id="txn_ref_no"
                                                fullWidth
                                                variant="outlined"
                                                placeholder="Transaction ID / Cheque No."
                                                value={(this.state.calculateAmount.txn_ref_no) ? this.state.calculateAmount.txn_ref_no : ''}
                                                error={(this.state.add_err.txn_ref_no) ? true : false}
                                                helperText={(this.state.add_err.txn_ref_no != '') ? this.state.add_err.txn_ref_no : ''}
                                                onChange={(e) => this.onChangePaymentDetail('txn_ref_no', e.target.value)}
                                             />
                                          </FormGroup>
                                       </div>
                                    </div>
                                 }

                                 {this.state.ach_method_field &&
                                    <React.Fragment>
                                       <div className="row ach-method-fields">
                                          <div className="col-md-4">
                                             <FormGroup>
                                                <Label for="ach_bank_name">Bank Name<span className="required-field">*</span></Label><br />
                                                <TextField
                                                   type="text"
                                                   name="ach_bank_name"
                                                   id="ach_bank_name"
                                                   fullWidth
                                                   variant="outlined"
                                                   placeholder="Bank Name"
                                                   value={(this.state.calculateAmount.ach_bank_name) ? this.state.calculateAmount.ach_bank_name : ''}
                                                   error={(this.state.add_err.ach_bank_name) ? true : false}
                                                   helperText={(this.state.add_err.ach_bank_name != '') ? this.state.add_err.ach_bank_name : ''}
                                                   onChange={(e) => this.onChangePaymentDetail('ach_bank_name', e.target.value)}
                                                />
                                             </FormGroup>
                                          </div>

                                          <div className="col-md-4">
                                             <FormGroup>
                                                <Label for="ach_routing_no">Routing No.<span className="required-field">*</span> <span className="sample_img" onClick={() => this.opnSampleImgModal()}>(Sample Image)</span></Label><br />
                                                <TextField
                                                   type="text"
                                                   name="ach_routing_no"
                                                   id="ach_routing_no"
                                                   fullWidth
                                                   variant="outlined"
                                                   placeholder="Routing No."
                                                   value={(this.state.calculateAmount.ach_routing_no) ? this.state.calculateAmount.ach_routing_no : ''}
                                                   error={(this.state.add_err.ach_routing_no) ? true : false}
                                                   helperText={(this.state.add_err.ach_routing_no != '') ? this.state.add_err.ach_routing_no : ''}
                                                   onChange={(e) => this.onChangePaymentDetail('ach_routing_no', e.target.value)}
                                                />
                                             </FormGroup>
                                          </div>

                                          <div className="col-md-4">
                                             <FormGroup>
                                                <Label for="ach_account_no">Account No.<span className="required-field">*</span> <span className="sample_img" onClick={() => this.opnSampleImgModal()}>(Sample Image)</span></Label><br />
                                                <TextField
                                                   type="text"
                                                   name="ach_account_no"
                                                   id="ach_account_no"
                                                   fullWidth
                                                   variant="outlined"
                                                   placeholder="Account No."
                                                   value={(this.state.calculateAmount.ach_account_no) ? this.state.calculateAmount.ach_account_no : ''}
                                                   error={(this.state.add_err.ach_account_no) ? true : false}
                                                   helperText={(this.state.add_err.ach_account_no != '') ? this.state.add_err.ach_account_no : ''}
                                                   onChange={(e) => this.onChangePaymentDetail('ach_account_no', e.target.value)}
                                                />
                                             </FormGroup>
                                          </div>
                                       </div>
                                       <div className="row">
                                          {(this.props.single_installment_payment && this.props.single_installment_payment[0].patient_id != null) &&
                                             <div className="col-md-8">
                                                <FormGroup tag="fieldset">
                                                   <FormGroup check>
                                                      <Label check>
                                                         <Input
                                                            type="checkbox"
                                                            name="cosigner"
                                                            value={1}
                                                            checked={(this.state.calculateAmount.cosigner == 1) ? true : false}
                                                            onChange={(e) => this.onChangePaymentDetail('cosigner', e)}
                                                         />
                                                      Piad by Co-signer
                                                                  </Label>
                                                   </FormGroup>
                                                </FormGroup>
                                             </div>

                                          }
                                          {this.state.bankDetails &&
                                             <MuiThemeProvider theme={this.getMuiTheme()}>
                                                <div className="col-sm-12">
                                                   <table className="table-sm">
                                                      <tbody>
                                                         <tr>
                                                            <th colSpan="6">Bank Details</th>
                                                         </tr>
                                                         <tr>
                                                            <th></th>
                                                            <th>Account Name</th>
                                                            <th>Bank Name</th>
                                                            <th>Routing No.</th>
                                                            <th>Account No.</th>
                                                            <th>Primary.</th>
                                                         </tr>
                                                         {this.state.bankDetails.map(function (row, idx) {
                                                            return <tr key={idx}>
                                                               <td>
                                                                  <Checkbox
                                                                     checked={(row.checked) ? true : false}
                                                                     onChange={() => this.onSelectBank(row)}
                                                                     color="primary"
                                                                  />
                                                               </td>
                                                               <td>{row.account_name}</td>
                                                               <td>{row.bank_name}</td>
                                                               <td>{row.rounting_no}</td>
                                                               <td>{row.bank_ac}</td>
                                                               <td>{(row.primary_bank) ? 'Yes' : 'No'}</td>
                                                            </tr>
                                                         }.bind(this))
                                                         }
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </MuiThemeProvider>
                                          }
                                       </div>
                                    </React.Fragment>
                                 }

                                 {this.state.credit_card_method_field &&
                                    <div className="field-div">
                                       <p className="m-0"><strong>If you would like to pay by credit or debit card, call us at <a href="tel:919-600-5526" title="Call Us">919-600-5526</a></strong></p>
                                    </div>
                                 }

                                 {this.state.mail_method_field &&
                                    <div>
                                       <div className="field-div">
                                          <p><strong>Make check payable to Health Partner Inc. address:</strong></p>
                                          <p className="m-0">Health Partner Inc.<br />5720 Creedmoor Rd, Suite 103, <br />Raleigh, NC 27612</p>

                                          <div className="row cheque-method-fields">
                                             <div className="col-md-6">
                                                <FormGroup>
                                                   <Label for="check_no">Check Number<span className="required-field">*</span></Label><br />
                                                   <TextField
                                                      type="text"
                                                      name="check_no"
                                                      id="check_no"
                                                      fullWidth
                                                      variant="outlined"
                                                      placeholder="Check Number"
                                                      value={(this.state.calculateAmount.check_no) ? this.state.calculateAmount.check_no : ''}
                                                      error={(this.state.add_err.check_no) ? true : false}
                                                      helperText={(this.state.add_err.check_no != '') ? this.state.add_err.check_no : ''}
                                                      onChange={(e) => this.onChangePaymentDetail('check_no', e.target.value)}
                                                   />
                                                </FormGroup>
                                             </div>

                                             <div className="col-md-6">
                                                <FormGroup>
                                                   <Label for="check_date">Check Date<span className="required-field">*</span></Label><br />
                                                   <DatePicker
                                                      dateFormat="MM/dd/yyyy"
                                                      name="check_date"
                                                      id="check_date"
                                                      selected={this.state.check_date}
                                                      placeholderText="MM/DD/YYYY"
                                                      autocomplete={false}
                                                      showMonthDropdown
                                                      showYearDropdown
                                                      dropdownMode="select"
                                                      minDate={new Date()}
                                                      strictParsing
                                                      onChange={(e) => this.onChangePaymentDetail('check_date', e)}
                                                   />
                                                   {(this.state.add_err.check_date) ? <FormHelperText className="jss116">{this.state.add_err.check_date}</FormHelperText> : ''}

                                                </FormGroup>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 }

                                 <div className="d-flex justify-content-between mb-30 mt-30 add-full-card customer-accnt">

                                    <div className="add-card col-sm-12">
                                       <table className=" customer_payment">
                                          <tbody>
                                             <tr>
                                                <th colSpan="4">Payment Information</th>
                                             </tr>
                                             <tr>
                                                <td><strong>Amount Due:</strong></td>
                                                <td className="text-left">
                                                   {
                                                      (calculateAmount.single_installment) ? '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount)).toFixed(2) : '$0.00'
                                                   }
                                                </td>

                                                <td></td>


                                             </tr>


                                             {lateFeeApply &&

                                                <tr>
                                                   <td>
                                                      <strong>Late Fee <span className="font-weight-normal d-inline text-primary">({
                                                         ('$' + parseFloat(calculateAmount.single_installment[0].previous_late_fee).toFixed(2) + '+$' + parseFloat(calculateAmount.single_installment[0].late_fee_due).toFixed(2))
                                                      })</span> :</strong>

                                                   </td>

                                                   <td>
                                                      <span className={(this.state.final_late_fee && this.state.final_late_fee_bool) ? "late-fee-charge customer-lfee d-inline float-left mr-10 text-decoration-line-through" : "late-fee-charge customer-lfee d-inline float-left mr-10"}>
                                                         {
                                                            '$' + (parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due)).toFixed(2)
                                                         }
                                                      </span>
                                                      <span className="late-fee-waiver customer-lfee">
                                                         {
                                                            (this.state.final_late_fee !== 0 && this.state.final_late_fee !== null && this.state.final_late_fee_bool) ? '$' + parseFloat(this.state.final_late_fee).toFixed(2) : ''
                                                         }
                                                      </span>
                                                   </td>
                                                   <td></td>

                                                </tr>

                                             }

                                             {lateFeeApply &&

                                                <tr>
                                                   <td>
                                                      <strong>Financial Charge <span className="font-weight-normal d-inline text-primary">({
                                                         ('$' + parseFloat(calculateAmount.single_installment[0].previous_fin_charge).toFixed(2) + '+$' + parseFloat(calculateAmount.single_installment[0].fin_charge_due).toFixed(2))
                                                      })</span> :</strong>
                                                   </td>

                                                   <td>
                                                      <span className={(this.state.final_fin_charge && this.state.final_fin_charge_bool) ? "late-fin-charge customer-fin d-inline float-left mr-10 text-decoration-line-through" : "late-fin-charge customer-fin d-inline float-left mr-10"}>
                                                         {
                                                            '$' + (parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due)).toFixed(2)
                                                         }
                                                      </span>
                                                      <span className="late-fin-waiver customer-fin">
                                                         {
                                                            (this.state.final_fin_charge !== 0 && this.state.final_fin_charge !== null && this.state.final_fin_charge_bool) ? '$' + parseFloat(this.state.final_fin_charge).toFixed(2) : ''
                                                         }
                                                      </span>
                                                   </td>
                                                   <td></td>

                                                </tr>

                                             }


                                             <tr>
                                                <td><strong>Total Pay:</strong></td>
                                                <td className="text-left">
                                                   <strong>
                                                      {(() => {
                                                         if (calculateAmount.single_installment) {
                                                            if (this.state.final_fin_charge_bool == true && this.state.final_late_fee_bool == true) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee)).toFixed(2)
                                                               )
                                                            } else if ((this.state.final_late_fee !== 0 && this.state.final_late_fee !== null) && (this.state.final_fin_charge == 0) && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_late_fee) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due)).toFixed(2)
                                                               )
                                                            } else if ((this.state.final_fin_charge !== 0 && this.state.final_fin_charge !== null) && this.state.final_late_fee == 0 && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(this.state.final_fin_charge)).toFixed(2)
                                                               )
                                                            } else if (this.state.final_late_fee !== 0 && this.state.final_fin_charge !== 0 && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee)).toFixed(2)
                                                               )
                                                            } else if (lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due)).toFixed(2)
                                                               )
                                                            } else {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].payment_amount)).toFixed(2)
                                                               )
                                                            }

                                                         }

                                                      })()}


                                                   </strong>
                                                </td>

                                                <td></td>


                                             </tr>
                                             <tr>
                                                <td><strong>Amount already received:</strong></td>
                                                <td className="text-left">
                                                   {
                                                      (calculateAmount.single_installment) ? '$' + (parseFloat(calculateAmount.single_installment[0].total_recived) - parseFloat(calculateAmount.single_installment[0].total_credit_recived)).toFixed(2) : '$0.00'
                                                   }
                                                   {
                                                      (this.props.single_installment_payment && this.props.single_installment_payment[0].prv_invoice_details != null) ? <Link to="#" onClick={this.viewPayDetails.bind(this)} className="pl-10" title="View Payment Details">  <i className="zmdi zmdi-eye icon-fr"></i></Link> : ''
                                                   }
                                                </td>
                                                <td></td>
                                             </tr>

                                             <tr>
                                                <td><strong>Credit Card charge already received:</strong></td>
                                                <td className="text-left">
                                                   {
                                                      (calculateAmount.single_installment) ? '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived)).toFixed(2) : '$0.00'

                                                   }
                                                </td>
                                                <td></td>
                                             </tr>

                                             <tr>
                                                <td><strong>Invoice Balance:</strong></td>
                                                <td className="text-left">
                                                   <strong>
                                                      {(() => {
                                                         if (calculateAmount.single_installment) {
                                                            if (this.state.final_fin_charge_bool == true && this.state.final_late_fee_bool == true) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            } else if ((this.state.final_late_fee !== 0 && this.state.final_late_fee !== null) && (this.state.final_fin_charge == 0) && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_late_fee) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            } else if ((this.state.final_fin_charge !== 0 && this.state.final_fin_charge !== null) && this.state.final_late_fee == 0 && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(this.state.final_fin_charge) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            } else if (this.state.final_late_fee !== 0 && this.state.final_fin_charge !== 0 && lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            } else if (lateFeeApply) {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            } else {
                                                               return (
                                                                  '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.single_installment[0].payment_amount) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                               )
                                                            }

                                                         }

                                                      })()}


                                                   </strong>
                                                </td>
                                                <td></td>
                                             </tr>

                                             {this.state.payment_method == 2 && calculateAmount.credit_charge !== 0 &&
                                                <tr>
                                                   <td><strong>Credit Card Charge:</strong></td>
                                                   <td className="text-left">
                                                      {
                                                         '$' + parseFloat(calculateAmount.credit_charge).toFixed(2)
                                                      }
                                                   </td>

                                                   <td></td>
                                                </tr>
                                             }

                                             {this.state.payment_method == 2 && calculateAmount.credit_charge !== 0 &&

                                                <tr>
                                                   <td><strong>Invoice Balance with Credit Card Charge:</strong></td>
                                                   <td className="text-left">
                                                      <strong>
                                                         {(() => {
                                                            if (calculateAmount.single_installment) {
                                                               if (this.state.final_fin_charge_bool == true && this.state.final_late_fee_bool == true) {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               } else if ((this.state.final_late_fee !== 0 && this.state.final_late_fee !== null) && (this.state.final_fin_charge == 0) && lateFeeApply) {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_late_fee) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               } else if ((this.state.final_fin_charge !== 0 && this.state.final_fin_charge !== null) && this.state.final_late_fee == 0 && lateFeeApply) {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(this.state.final_fin_charge) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               } else if (this.state.final_late_fee !== 0 && this.state.final_fin_charge !== 0 && lateFeeApply) {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(this.state.final_fin_charge) + parseFloat(this.state.final_late_fee) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               } else if (lateFeeApply) {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) + parseFloat(calculateAmount.single_installment[0].previous_late_fee) + parseFloat(calculateAmount.single_installment[0].late_fee_due) + parseFloat(calculateAmount.single_installment[0].previous_fin_charge) + parseFloat(calculateAmount.single_installment[0].fin_charge_due) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               } else {
                                                                  return (
                                                                     '$' + (parseFloat(calculateAmount.single_installment[0].total_credit_recived) + parseFloat(calculateAmount.credit_charge) + parseFloat(calculateAmount.single_installment[0].payment_amount) - parseFloat(calculateAmount.single_installment[0].total_recived)).toFixed(2)
                                                                  )
                                                               }

                                                            }

                                                         })()}

                                                         {(calculateAmount.credit_charge !== 0) ? ' (Incl. of credit card charge ' + this.props.credit_charge + '%)' : ''}
                                                      </strong>

                                                   </td>
                                                   <td></td>
                                                </tr>

                                             }

                                             <tr>

                                                <td><strong>Amount Paid ($)<p className="required-field d-inline-block font-weight-bold m-0">*</p> :</strong></td>

                                                <td>
                                                   <FormGroup>
                                                      <TextField
                                                         type="text"
                                                         name="additional_amount"
                                                         id="additional_amount"
                                                         fullWidth
                                                         variant="outlined"
                                                         placeholder="Amount Paid"
                                                         value={(this.state.calculateAmount.additional_amount) ? this.state.calculateAmount.additional_amount : ''}
                                                         error={(this.state.add_err.additional_amount) ? true : false}
                                                         helperText={(this.state.add_err.additional_amount != '') ? this.state.add_err.additional_amount : ''}
                                                         onChange={(e) => this.onChangePaymentDetail('additional_amount', e.target.value)}
                                                      />
                                                   </FormGroup>
                                                </td>


                                                <td></td>


                                             </tr>

                                          </tbody>
                                       </table>
                                    </div>
                                 </div>



                                 {(this.state.paid_flag !== 1) ?
                                    <div className="float-right mb-20">

                                       <Button
                                          variant="contained"
                                          color="primary"
                                          className="text-white mr-10"
                                          onClick={this.goBack.bind(this)}
                                       >Cancel</Button>

                                       <Button
                                          variant="contained"
                                          color="primary"
                                          className="text-white"
                                          onClick={this.payInstallmentPayment.bind(this)}
                                          disabled={!this.validateAddSubmit()}
                                       >Submit</Button>

                                    </div> : ''
                                 }

                              </Form>
                           </div>
                        </div>

                     </div>

                  </RctCard>

                  <Modal className="p-view-img" isOpen={this.state.opnSampleImgModal} toggle={() => this.opnViewSampleImgModalClose()}>

                     <ModalHeader toggle={() => this.opnViewSampleImgModalClose()} className="p-view-popupImg">
                        <span className="float-left>">Sample Image</span>
                     </ModalHeader>

                     <ModalBody>
                        {this.state.opnSampleImgModal &&
                           <img src={require('Assets/img/sample_img.png')} className="img-fluid" alt="sample-image" width="100%" />
                        }
                     </ModalBody>

                  </Modal>
                  <Modal isOpen={this.state.payDetails} toggle={() => this.viewPayDetailsClose()}>

                     <ModalHeader toggle={() => this.viewPayDetailsClose()} className="p-view-popupImg">
                        <span className="float-left>">Payment Details</span>
                     </ModalHeader>

                     <ModalBody>
                        <div className="table-responsive mb-40 pymt-history">
                           {this.props.single_installment_payment && this.props.single_installment_payment[0].prv_invoice_details != null && this.props.single_installment_payment[0].prv_invoice_details.map(function (data, idx) {
                              return <table className="table table-bordered table-sm table-striped" key={idx}>
                                 <tbody>
                                    <tr>
                                       <td><strong>Payment Method : </strong></td>
                                       <td>
                                          {(data.payment_method == 1) ? 'ACH / eCheck' : ''}
                                          {(data.payment_method == 2) ? 'Credit Card' : ''}
                                          {(data.payment_method == 3) ? 'Check / Via Mail' : ''}
                                       </td>
                                       <td><strong>Paid Date : </strong></td>
                                       <td>
                                          {(data.payment_date) ? data.payment_date : ''}
                                       </td>
                                    </tr>
                                    {(data.payment_method == 3) ?
                                       <React.Fragment>
                                          <tr>
                                             <td><strong>Data Check No : </strong></td>
                                             <td>
                                                {data.check_no}
                                             </td>
                                             <td><strong>Check Date : </strong></td>
                                             <td>
                                                {data.check_date}
                                             </td>
                                          </tr>
                                       </React.Fragment>

                                       : ''}
                                    {(data.payment_method == 1) ?
                                       <React.Fragment>
                                          <tr>
                                             <td><strong>Bank Name : </strong></td>
                                             <td>
                                                {data.ach_bank_name}
                                             </td>
                                             <td><strong>Routing No. : </strong></td>
                                             <td>
                                                {data.ach_routing_no}
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><strong>Account No : </strong></td>
                                             <td colSpan="3">
                                                {data.ach_account_no}
                                             </td>
                                          </tr>
                                       </React.Fragment>

                                       : ''}
                                    <tr>
                                       <td><strong>Amount : </strong></td>
                                       <td colSpan="3">
                                          {(data.amount_paid) ? '$' + data.amount_paid.toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Note : </strong></td>
                                       <td colSpan="3">
                                          {(data.comments) ? data.comments : '-'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           })
                           }
                        </div>
                     </ModalBody>

                  </Modal>
                  {this.props.loading &&
                     <RctSectionLoader />
                  }
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer }) => {

   const { nameExist, isEdit } = authUser;
   const {
      redirectURL,
      loading,
      late_fee_waivers_option,
      financial_charges_waiver_option,
      financial_charges_option,
      payment_method_option,
      late_fee_waiver_type_reason_option,
      fin_charge_waiver_type_reason_option,
      single_installment_payment,
      single_installment_payment_edit,
      payment_billing_late_fee,
      payment_invoice_detail,
      single_invoice_rows,
      invoice_rows_appid,
      credit_charge,
      customerBank,
      customerCoBank,
   } = PaymentPlanReducer;

   return {
      redirectURL,
      loading,
      late_fee_waivers_option,
      financial_charges_waiver_option,
      financial_charges_option,
      payment_method_option,
      late_fee_waiver_type_reason_option,
      fin_charge_waiver_type_reason_option,
      single_installment_payment,
      single_installment_payment_edit,
      payment_billing_late_fee,
      payment_invoice_detail,
      single_invoice_rows,
      invoice_rows_appid,
      credit_charge,
      redirectURL,
      customerBank,
      customerCoBank,
   }
}

export default connect(mapStateToProps, {
   getPaymentMasterFeeOption, getLateFeeWaiverType, getFinChargeWaiverType, getSingleInstallment, insertPayPayment, clearRedirectURL
})(CustomerPaymentBilling);