/**
 * App Widgets
 */
import React from 'react';
import Loadable from 'react-loadable';
import PreloadWidget from 'Components/PreloadLayout/PreloadWidget';

const MyLoadingComponent = () => (
   <PreloadWidget />
)



const PaymentDashboard = Loadable({
   loader: () => import("./PaymentDashboard"),
   loading: MyLoadingComponent
});


const ApplicationDashboard = Loadable({
   loader: () => import("./ApplicationDashboard"),
   loading: MyLoadingComponent
})

const InvoiceDashboard = Loadable({
   loader: () => import("./InvoiceDashboard"),
   loading: MyLoadingComponent
})

const SupportOrdersWidget = Loadable({
   loader: () => import("./SupportDashboard"),
   loading: MyLoadingComponent
})


export {
   PaymentDashboard,
   InvoiceDashboard,
   ApplicationDashboard,
   SupportOrdersWidget
}