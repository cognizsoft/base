/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty, formatPhoneNumber, isPhone } from '../../validator/Validator';
import CryptoJS from 'crypto-js';
import {
   customerDashboardWeekMonthInvoiceReport, customerDashboardInvoiceReportFilter, customerDashboardInvoiceReportDownload, customerDashboardInvoiceReportDownloadXLS
} from 'Actions';
import { Link } from 'react-router-dom';
//import AddNewButton from './AddNewButton';

class CustomerDashboardInvoiceReports extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      reportFilter: {
         short_by: 2,
         invoice_start_date: '',
         invoice_end_date: '',
         invoice_due_start_date: '',
         invoice_due_end_date: '',
         customer_name: '',
         status: '',
      },
      invoice_start_date: '',
      invoice_end_date: '',
      invoice_due_start_date: '',
      invoice_due_end_date: '',
      widthTable: '',
   }


   componentDidMount() {
      this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      this.permissionFilter(this.state.currentModule);
      const { reportFilter } = this.state;
      (this.props.match.params.appid != 0) ? reportFilter.status = this.props.match.params.appid : '';
      this.setState({ reportFilter: reportFilter })
      this.props.customerDashboardInvoiceReportFilter(this.state.reportFilter);
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   onChangeReportFilter(key, value) {
      switch (key) {
         case 'invoice_start_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_start_date: '' })
            } else {
               this.setState({ invoice_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_end_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_end_date: '' })
            } else {
               this.setState({ invoice_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_due_start_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_due_start_date: '' })
            } else {
               this.setState({ invoice_due_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_due_end_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_due_end_date: '' })
            } else {
               this.setState({ invoice_due_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;

      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      this.props.customerDashboardInvoiceReportDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      this.props.customerDashboardInvoiceReportFilter(this.state.reportFilter);
   }

   fullReportDownloadXLS() {
      this.props.customerDashboardInvoiceReportDownloadXLS(this.state.reportFilter);
   }
   
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {

               if (currentValue.invoice_status == 3) {
                  var totalAmt = (parseFloat(currentValue.previous_late_fee) + parseFloat(currentValue.previous_fin_charge) + parseFloat(currentValue.late_fee_due) + parseFloat(currentValue.fin_charge_due) + parseFloat(currentValue.payment_amount)).toFixed(2)
               } else {
                  var totalAmt = (parseFloat(currentValue.late_fee_received) + parseFloat(currentValue.fin_charge_received) + parseFloat(currentValue.payment_amount)).toFixed(2)
               }
               accumulator['payment_amount'] = (accumulator['payment_amount'] !== undefined) ? accumulator['payment_amount'] + parseFloat(totalAmt) : parseFloat(totalAmt);
            }

            return accumulator
         }, []);
         return amount
      }
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      const columns = [

         {
            name: 'Invoice ID',
            field: 'invoice_number',
            footer: 'Total: 100',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Customer Name',
            field: 'name',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  );
               }
            }
         },
         {
            name: 'Address',
            field: 'address1',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'City',
            field: 'City',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'State',
            field: 'state_name',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Zip',
            field: 'zip_code',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Phone',
            field: 'peimary_phone',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Invoice date',
            field: 'payment_date',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (value.payment_date) ? value.payment_date : '-';
               }
            }
         },
         {
            name: 'Invoice due date',
            field: 'due_date',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Invoice Amt',
            field: 'payment_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  if (value.invoice_status == 3) {
                     var totalAmt = (parseFloat(value.previous_late_fee) + parseFloat(value.previous_fin_charge) + parseFloat(value.late_fee_due) + parseFloat(value.fin_charge_due) + parseFloat(value.payment_amount)).toFixed(2)
                  } else {
                     var totalAmt = (parseFloat(value.late_fee_received) + parseFloat(value.fin_charge_received) + parseFloat(value.payment_amount)).toFixed(2)
                  }
                  return (
                     (totalAmt) ? '$' + totalAmt : '-'
                  );
               }
            }
         },
         {
            name: 'Addtl Amt',
            field: 'additional_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (
                     (value.additional_amount) ? '$' + value.additional_amount.toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Total Paid',
            field: 'paid_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  var totalAmt = (parseFloat(value.late_fee_received) + parseFloat(value.fin_charge_received) + parseFloat(value.paid_amount)).toFixed(2)

                  return (
                     (value.invoice_status == 1 || value.invoice_status == 4) ? '$' + (parseFloat(totalAmt) + parseFloat(value.additional_amount)).toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Invoice status',
            field: 'paid_flag',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  return (
                     value.invoice_status_name
                  );
               }
            }
         },
         {
            name: 'Action',
            field: 'application_id',
            options: {
               headerNoWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value) => {
                  var due_date = value.due_date
                  var exp_date = due_date.split('/');
                  return (
                     <React.Fragment>
                        <span className="list-action">

                        {/*(value.paid_flag == 3)?<Link to={`/customer/credit-application/installment/invoice/${this.enc(value.invoice_id.toString())}`} title="Invoice"><i className="zmdi zmdi-file-text icon-fr"></i></Link>: ''*/}
                        {(value.invoice_status == 2 || value.invoice_status == 3)?<Link to={`/customer/credit-application/installment/payment/${this.enc(value.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>: ''}
                        {(value.invoice_status == 1 || value.invoice_status == 4)?<Link to={`/customer/credit-application/installment/receipt/${this.enc(value.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>: ''}

                        </span>
                     </React.Fragment>
                  )
               },

            }
         },

      ];
      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         responsive: "scroll",
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,
         /*customRow: (data) => {
         

         return (
            <TableRow>
            <TableCell rowSpan={3} />
            <TableCell colSpan={2}>Subtotal</TableCell>
            <TableCell align="right">{ccyFormat(invoiceSubtotal)}</TableCell>
            </TableRow>
         );
         },*/

         customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            rowsSelected
         ) => {
            const invoiceSubtotal = this.totalLoanAmount(this.props.customerInvoiceList, page, rowsPerPage);

            return (
               <React.Fragment>

                  <TableFooter>
                     <TableRow>
                        <TableCell style={footerStyle} ><b>Total Invoice Amount :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.payment_amount !== null) ? invoiceSubtotal.payment_amount.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     </TableRow>
                     <TableRow>
                        <TablePagination
                           count={count}
                           rowsPerPage={rowsPerPage}
                           page={page}
                           onChangePage={(_, page) => changePage(page)}
                           onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                           rowsPerPageOptions={[10, 20, 50, 100]}
                        />
                     </TableRow>
                  </TableFooter>
               </React.Fragment>
            );
         },
         onTableChange: (action, tableState) => {
            this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
         }

      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
         }
      });
      return (
         <div className="admin-reports-week-month">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.weekMonth" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="status">Status</Label>
                              <Input
                                 type="select"
                                 name="status"
                                 id="status"
                                 placeholder="Financial Chargesstatus"
                                 value={this.state.reportFilter.status}
                                 onChange={(e) => this.onChangeReportFilter('status', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="3">Due</option>
                                 <option value="2">In Progress</option>
                                 <option value="1">Paid</option>
                                 <option value="4">Partial Paid</option>
                              </Input>

                           </FormGroup>
                        </div>
                     
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_start_date">Invoice date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_start_date"
                                 id="invoice_start_date"
                                 selected={this.state.invoice_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_end_date"
                                 id="invoice_end_date"
                                 selected={this.state.invoice_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_end_date', e)}
                              />
                           </FormGroup>
                        </div>

                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_due_start_date">Invoice Due Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_due_start_date"
                                 id="invoice_due_start_date"
                                 selected={this.state.invoice_due_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_due_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_due_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_due_end_date"
                                 id="invoice_due_end_date"
                                 selected={this.state.invoice_due_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_due_end_date', e)}
                              />
                           </FormGroup>
                        </div>

                        <div className="col-md-2">
                           <div className="list-action">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>
                                 <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10">search</i></a>
                                 <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10">picture_as_pdf</i></a>
                                 <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10">insert_drive_file</i></a>
                              </FormGroup>
                           </div>
                        </div>

                     </div>
                  </Form>
               </div>
               <MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={this.props.customerInvoiceList}
                     columns={columns}
                     options={options}


                  />
               </MuiThemeProvider>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ CustomerDashboardReducer, authUser }) => {
   const { loading, customerInvoiceList, statusList, providerList } = CustomerDashboardReducer;
   const user = authUser.user;
   return { loading, user, customerInvoiceList, statusList, providerList }
}

export default connect(mapStateToProps, {
   customerDashboardWeekMonthInvoiceReport, customerDashboardInvoiceReportFilter, customerDashboardInvoiceReportDownload, customerDashboardInvoiceReportDownloadXLS
})(CustomerDashboardInvoiceReports);