/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import { currentUserId } from '../../apifile';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import Tooltip from '@material-ui/core/Tooltip';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MaterialDatatable from "material-datatable";
import AddCallLogButton from '../credit-applications/support-history/AddCallLogButton';
import AddCallLogForm from '../credit-applications/support-history/AddCallLogForm';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import { isEmpty, isMaster, isNumeric, isObjectEmpty } from '../../validator/Validator';
import {
    planDetails, clearRedirectURL, allPlanDetails, customerActivePlanDetails, customerPlanDetailsDownload, createSupportTicket, getAllTickets
} from 'Actions';
class TicketList extends Component {

    state = {
        openViewRefund: false,
        refundDetails: null,
        singleView: null,
        openInstallmentDialog: false,
        installmentDetails: null,

        ///Support History///
        addCallLogModal: false,
        addCallLogDetail: {
            ticket_source: 3,
            call_type: 'Yes',
            ticket_related_ac: 0,
            ticket_related_plan: 0,
            ticket_related_invoice: 0,
            account_no: '',
            plan_no: '',
            invoice_no: '',
            caller_name: '',
            call_date_time: '',
            subject: '',
            hps_agent_name: '',
            description: '',
            follow_up: 0,
            follow_up_date_time: '',
            invoice_check: true
        },
        view_ticket: false,
        ticket_number: '',
        application_id: '',
        add_err: {},
        dateTimeStartDate: '',
        followUpDateTimeStartDate: '',
        invoiceGroup: null,
    }
    dec(cipherText) {
        var SECRET = 'rmaeshCSS';
        var reb64 = CryptoJS.enc.Hex.parse(cipherText);
        var bytes = reb64.toString(CryptoJS.enc.Base64);
        var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
        var plain = decrypt.toString(CryptoJS.enc.Utf8);
        return plain;
    }
    componentDidMount() {
        var data = {
            appid: currentUserId(),
            filter_by: 4 // for filter in node file
        }
        this.props.clearRedirectURL();
        this.props.customerActivePlanDetails(currentUserId());
        this.props.getAllTickets(data);
    }





    enc(plainText) {
        var SECRET = 'rmaeshCSS'
        var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
        var e64 = CryptoJS.enc.Base64.parse(b64);
        var eHex = e64.toString(CryptoJS.enc.Hex);
        return eHex;
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {

            MuiTooltip: {
                tooltip: {
                    fontSize: "15px"
                }
            },
        }
    })

    onViewRefundClose() {
        this.setState({ openViewRefund: false, refundDetails: null })
    }

    ////Support History////
    opnAddCallLogModal() {
        this.setState({ addCallLogModal: true });
    }
    onAddUpdateCallLogModalClose = () => {
        let addR = {}
        let addCallLogDetail = {
            ticket_source: 1,
            call_type: 'Yes',
            ticket_related_ac: 0,
            ticket_related_plan: 0,
            ticket_related_invoice: 0,
            account_no: '',
            plan_no: '',
            invoice_no: '',
            caller_name: '',
            call_date_time: '',
            subject: '',
            hps_agent_name: '',
            description: '',
            follow_up: 0,
            follow_up_date_time: '',
            invoice_check: true
        };
        this.setState({ addCallLogModal: false, add_err: addR, addCallLogDetail: addCallLogDetail, view_ticket: false, ticket_number: '' })
    }
    addCallLogView() {
        const min = 1;
        const max = 100000000;
        const rand = Math.floor(Math.random() * (max - min + 1) + min)

        this.setState({ view_ticket: true, ticket_number: rand })
    }
    addCallLogViewBack() {
        this.setState({ view_ticket: false })
    }
    addCallLog() {
        this.state.addCallLogDetail.ticket_number = this.state.ticket_number
        this.state.addCallLogDetail.application_id = (this.props.app_id) ? this.props.app_id[0].application_id : ''
        this.state.addCallLogDetail.caller_name = (this.props.application_details[0].f_name + ' ' + this.props.application_details[0].m_name + ' ' + this.props.application_details[0].l_name)
        this.state.addCallLogDetail.customer_id = this.props.application_details[0].patient_id
        this.state.addCallLogDetail.commented_by = 2 // by customer
        //console.log(this.state.addCallLogDetail)
        //return false;
        //insertTermMonth
        this.props.createSupportTicket(this.state.addCallLogDetail);


        this.setState({ addCallLogModal: false, loading: true });
        let self = this;
        let log = {
            ticket_source: 3,
            call_type: 'Yes',
            ticket_related_ac: 0,
            ticket_related_plan: 0,
            ticket_related_invoice: 0,
            account_no: '',
            plan_no: '',
            invoice_no: '',
            caller_name: '',
            call_date_time: '',
            subject: '',
            hps_agent_name: '',
            description: '',
            follow_up: 0,
            follow_up_date_time: '',
            view_ticket: false,
            ticket_number: '',
            invoice_check: true
        }

        setTimeout(() => {
            self.setState({ loading: false, addCallLogDetail: log, view_ticket: false, ticket_number: '', dateTimeStartDate: '', followUpDateTimeStartDate: '' });
            NotificationManager.success('Ticket Created!');
        }, 2000);

    }
    onChangeAddCallLogDetails(key, value) {
        let { add_err, addCallLogDetail } = this.state;
        //console.log(value)
        switch (key) {
            case 'ticket_source':

                break;
            case 'call_type':

                break;
            case 'ticket_related_ac':
                value = (this.state.addCallLogDetail.ticket_related_ac) ? 0 : 1;

                break;
            case 'ticket_related_app':
                value = (this.state.addCallLogDetail.ticket_related_app) ? 0 : 1;

                break;
            case 'ticket_related_plan':
                value = (this.state.addCallLogDetail.ticket_related_plan) ? 0 : 1;
                if (value == 0) {
                    addCallLogDetail['plan_no'] = ''
                    addCallLogDetail['ticket_related_invoice'] = 0
                    addCallLogDetail['invoice_check'] = true
                    this.setState({ invoiceGroup: null })
                } else {
                    addCallLogDetail['invoice_check'] = false
                }
                add_err['plan_no'] = 'Select Plan No.'
                break;
            case 'ticket_related_invoice':
                value = (this.state.addCallLogDetail.ticket_related_invoice) ? 0 : 1;
                if (value == 0) {
                    addCallLogDetail['invoice_no'] = ''
                    this.setState({ invoiceGroup: null })
                } else {
                    addCallLogDetail['plan_no'] = ''
                    add_err['plan_no'] = 'Select Plan No.'
                }
                add_err['invoice_no'] = 'Select Invoice No.'
                break;
            case 'plan_no':
                if (isEmpty(value)) {
                    add_err[key] = "Select Plan No.";
                    add_err['invoice_no'] = 'Select Invoice No.'
                    this.setState({ invoiceGroup: null })
                } else {
                    add_err[key] = '';
                    addCallLogDetail['invoice_no'] = ''
                    add_err['invoice_no'] = 'Select Invoice No.'
                    var planInvoice = this.props.invoicePlan.filter(x => x.pp_id == value)
                    this.setState({ invoiceGroup: planInvoice })
                }
                break;
            case 'invoice_no':
                if (isEmpty(value)) {
                    add_err[key] = "Select Invoice No.";
                } else {
                    add_err[key] = '';
                }
                break;
            case 'caller_name':
                if (isEmpty(value)) {
                    add_err[key] = "Caller Name can't be blank";
                } else {
                    add_err[key] = '';
                }
                break;
            case 'call_date_time':
                if (value == null) {
                    add_err[key] = "Select Date";
                } else {
                    this.setState({ dateTimeStartDate: value })
                    value = moment(value).format('YYYY-MM-DD h:mm:ss');
                    add_err[key] = '';
                }
                break;
            case 'subject':
                if (isEmpty(value)) {
                    add_err[key] = "Subject can't be blank";
                } else {
                    add_err[key] = '';
                }
                break;
            case 'hps_agent_name':
                if (isEmpty(value)) {
                    add_err[key] = "Agent Name can't be blank";
                } else {
                    add_err[key] = '';
                }
                break;
            case 'description':
                if (isEmpty(value)) {
                    add_err[key] = "Description can't be blank";
                } else {
                    add_err[key] = '';
                }
                break;
            case 'follow_up':
                value = (this.state.addCallLogDetail.follow_up) ? 0 : 1;
                if (value == 0) {
                    addCallLogDetail['follow_up_date_time'] = ''
                    this.setState({ followUpDateTimeStartDate: '' })
                }
                add_err['follow_up_date_time'] = 'Select date'
                break;
            case 'follow_up_date_time':
                if (value == null) {
                    add_err[key] = "Select Date";
                } else {
                    this.setState({ followUpDateTimeStartDate: value })
                    value = moment(value).format('YYYY-MM-DD h:mm:ss');
                    add_err[key] = '';
                }
                break;
            default:
                break;
        }

        this.setState({ add_err: add_err });
        this.setState({
            addCallLogDetail: {
                ...this.state.addCallLogDetail,
                [key]: value
            }
        });
        //console.log(this.state.addCallLogDetail)
    }
    validateAddCallLogSubmit() {
        //console.log("adderorB"+this.state.add_err.value)
        let { addCallLogDetail } = this.state
        var common = true;

        common = (
            this.state.add_err.subject === '' &&
            this.state.add_err.description === ''
        ) ? true : false

        var count = 0;
        if (addCallLogDetail.ticket_related_ac == 1) {
            count++
        }
        if (addCallLogDetail.ticket_related_app == 1) {
            count++
        }
        if (addCallLogDetail.ticket_related_plan == 1) {
            var plan_select = (this.state.add_err.plan_no === '') ? true : false
            count++
        } else {
            var plan_select = true
        }
        if (addCallLogDetail.ticket_related_invoice == 1) {
            var invoice_select = (this.state.add_err.invoice_no === '') ? true : false
            count++
        } else {
            var invoice_select = true
        }

        return (count >= 1 && common == true && plan_select == true && invoice_select == true) ? true : false;




        return false
        return (/*
         this.state.add_err.ticket_source === '' &&
         this.state.add_err.call_type === '' &&*/
            this.state.add_err.caller_name === '' &&
            this.state.add_err.call_date_time === '' &&
            this.state.add_err.subject === '' &&
            this.state.add_err.hps_agent_name === '' &&
            this.state.add_err.description === ''
        );
        //console.log("adderorB"+this.state.add_err.value)
    }
    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        let { updateForm } = this.state;
        updateForm[name] = value;

        this.setState({
            updateForm: updateForm
        }, function () {
            this.onUpdateUserDetails(name, value)
        });
    }
    render() {
        const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;
        var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
            if (!accumulator[currentValue.pp_id]) {
                // get recived amount
                var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
                    if (planindex == 0) {
                        accumulator = 0;
                    }
                    accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
                    return accumulator;
                }, 0);
                accumulator[currentValue.pp_id] = {
                    paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.plan_status, invoice_exist: currentValue.invoice_exist, note: currentValue.note, status_name: currentValue.status_name,
                    refund_comment: currentValue.refund_comment,
                    ach_account_no: currentValue.ach_account_no,
                    ach_routing_no: currentValue.ach_routing_no,
                    ach_bank_name: currentValue.ach_bank_name,
                    check_date: currentValue.check_date,
                    check_no: currentValue.check_no,
                    status_name: currentValue.status_name,
                    refund_amt: currentValue.refund_amt,
                    payment_method: currentValue.payment_method,
                    refundmethod: currentValue.refundmethod,
                    refund_id: currentValue.refund_id,
                };
            } else {
                //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
            }

            return accumulator;
        }, []);

        /* console.log('this.props.application_plans.length')
         console.log(this.props.application_plans)*/

        /////Support History/////
        const supportColumns = [
            {
                name: 'Ticket ID',
                field: 'ticket_id'
            },
            {
                name: 'App No.',
                field: 'application_no',
                options: {
                    customBodyRender: (value) => {
                        if (value.application_no && application_details) {
                            return application_details[0].application_no
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Plan No.',
                field: 'plan_number',
                options: {
                    customBodyRender: (value) => {
                        if (value.plan_number && this.props.customerPlan) {
                            var ppnp = this.props.customerPlan.filter(x => x.pp_id == value.plan_number)
                            return ppnp[0].plan_number
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Invoice No.',
                field: 'invoice_number',
                options: {
                    customBodyRender: (value) => {
                        if (value.invoice_number && this.props.customerInvoice) {
                            var invo = this.props.customerInvoice.filter(x => x.invoice_id == value.invoice_number)

                            return invo[0].invoice_number
                        } else {
                            return '-'
                        }
                    }
                }
            },
            {
                name: 'Subject',
                field: 'subject',
            },
            {
                name: 'Status',
                field: 'status',
            },
            {
                name: 'Action',
                field: 'ticket_id',
                options: {
                    noHeaderWrap: true,
                    filter: false,
                    sort: false,
                    download: false,
                    customBodyRender: (value, tableMeta, updateValue) => {

                        return (
                            <React.Fragment>
                                <span className="list-action">
                                    <Link to={`/customer/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                                </span>
                            </React.Fragment>
                        )
                    },

                }
            }

        ];

        const supportOptions = {
            filterType: 'dropdown',
            selectableRows: false,
            customToolbar: () => {
                return (
                    <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
                );
            }
        };
        const myTheme = createMuiTheme({
            overrides: {
                MaterialDatatableToolbar: {
                    root: { display: "none" }
                },
                MuiTableCell: {
                    footer: { padding: "4px 8px 4px 8px" }
                },
                MuiPaper: {
                    root: { boxShadow: "none !important" }
                }
            }
        });

        return (
            <div className="invoice-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.customerSupportTicket" />} match={this.props.match} />
                <div className="row">

                    <div className="col-sm-12 mx-auto">
                        <RctCard>

                            {this.props.tickets &&
                                <div className="p-10">
                                    <div className="admin-application-list support-history-container">
                                        <RctCollapsibleCard
                                            colClasses="col-md-12 w-xs-full support-history"
                                            heading="Support History"

                                            fullBlock
                                            customClasses="overflow-hidden d-inline-block w-100"
                                        >
                                            <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
                                            <MuiThemeProvider theme={myTheme}>
                                                <MaterialDatatable
                                                    data={(this.props.tickets) ? this.props.tickets : ''}
                                                    columns={supportColumns}
                                                    options={supportOptions}
                                                />
                                            </MuiThemeProvider>
                                        </RctCollapsibleCard>
                                    </div>
                                </div>
                            }




                            <Modal isOpen={this.state.addCallLogModal} toggle={() => this.onAddUpdateCallLogModalClose()} className="support-history-modal">
                                <ModalHeader toggle={() => this.onAddUpdateCallLogModalClose()}>
                                    Add Call Log
                        </ModalHeader>
                                <ModalBody>
                                    <AddCallLogForm
                                        addErr={this.state.add_err}
                                        addCallLogDetails={this.state.addCallLogDetail}
                                        onChangeAddCallLogDetails={this.onChangeAddCallLogDetails.bind(this)}
                                        allInvoice={this.state.invoiceGroup}
                                        allPlan={planDetails}
                                        DatePicker={DatePicker}
                                        dateTimeStartDate={this.state.dateTimeStartDate}
                                        followUpDateTimeStartDate={this.state.followUpDateTimeStartDate}
                                        masterTicketSource={this.props.masterTicketSource}
                                    />

                                </ModalBody>
                                <ModalFooter>

                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className="text-white"
                                        onClick={() => this.addCallLog()}
                                        disabled={!this.validateAddCallLogSubmit()}
                                    >
                                        Submit
                           </Button>


                                    <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCallLogModalClose()}>Cancel</Button>
                                </ModalFooter>
                            </Modal>

                            {this.props.loading &&
                                <RctSectionLoader />
                            }

                        </RctCard>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ authUser, creditApplication, CustomerDashboardReducer, CustomerSupportReducer }) => {

    const { nameExist, isEdit } = authUser;
    const { loading, application_details, invoiceDetails, application_plans, amountDetails, invoicePlan, app_id } = CustomerDashboardReducer;
    const { tickets, masterTicketSource, customerPlan, customerInvoice } = CustomerSupportReducer
    return { loading, application_details, invoiceDetails, application_plans, amountDetails, invoicePlan, app_id, tickets, masterTicketSource, customerPlan, customerInvoice }
}

export default connect(mapStateToProps, {
    planDetails, clearRedirectURL, allPlanDetails, customerActivePlanDetails, customerPlanDetailsDownload, createSupportTicket, getAllTickets
})(TicketList);