/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ editAnswer, addErr, onUpdateAnswerDetails, questionlists, userslists }) => (
    <Form>

        <div className="row">
            
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userType">Select Question<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="security_questions_id"
                        id="security_questions_id"
                        defaultValue={editAnswer.security_questions_id}
                        onChange={(e) => onUpdateAnswerDetails('security_questions_id', e.target.value, e.target[e.target.selectedIndex].text)}
                    >

                        <option value="">Select</option>
                        {questionlists && questionlists.map((type, key) => (
                            <option value={type.security_questions_id} key={key}>{type.name}</option>
                        ))}
                    </Input>
                    {(addErr.security_questions_id) ? <FormHelperText>{addErr.security_questions_id}</FormHelperText> : ''}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="answers">Answer<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="answers"
                        id="answers"
                        placeholder="Answer"
                        fullWidth
                        variant="outlined"
                        value={editAnswer.answers}
                        error={(addErr.answers) ? true : false}
                        helperText={addErr.answers}
                        onChange={(e) => onUpdateAnswerDetails('answers', e.target.value)}
                    />

                </FormGroup>
            </div>


            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label>Status <span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(editAnswer.status == 1) ? true : false}
                                onChange={(e) => onUpdateAnswerDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(editAnswer.status == 0) ? true : false}
                                onChange={(e) => onUpdateAnswerDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>

    </Form>
);

export default UpdateUserForm;
