/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';

// api
import api from 'Api';
import { currentUserId } from '../../apifile';
// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isEmpty } from '../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   checkSecurityAnswerExist, getAllAnswer, insertAnswer, updateAnswer, deleteAnswer, getAllProviderAnswer, insertProviderAnswer, updateProviderAnswer, deleteProviderAnswer
} from 'Actions';

class SecurityQuestions extends Component {

   state = {
      currentModule: 6,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewAnswerModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewAnsweretail: {
         security_questions_id: '',
         answers: '',
         status: 1,
      },
      openViewAnswerDialog: false, // view user dialog box
      editAnswer: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         answers: '',
         security_questions_id: '',
      },
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllProviderAnswer();
      //console.log(currentUserId())
   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.answerExist && nextProps.isEdit == 0) ? add_err['security_questions_id'] = "Security Question already exists" : '';
      (nextProps.answerExist && nextProps.isEdit == 1) ? udpate_err['security_questions_id'] = "Security Question already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });

   }


   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- viewAnswerDetail
   * Descrpation :- This function use open view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   viewAnswerDetail(data) {
      this.setState({ openViewAnswerDialog: true, selectedUser: data });
   }

	/*
   * Title :- onEditAnswer
   * Descrpation :- This function use open popup in eidt case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onEditAnswer(answer) {
      console.log(this.state)
      this.setState({ addNewAnswerModal: true, editAnswer: answer });
   }

	/*
   * Title :- onAddUpdateAnswerModalClose
   * Descrpation :- This function use close popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onAddUpdateAnswerModalClose() {
      let emptyError = {
      }
      let addNewAnsweretail = {
         security_questions_id: '',
         answers: '',
         status: 1,
      }
      let emptyUpdateError = {
         answers: '',
         security_questions_id: '',
      }
      this.setState({ addNewAnswerModal: false, editAnswer: null, add_err: emptyError, addNewAnsweretail: addNewAnsweretail, udpate_err: emptyUpdateError })
   }

   /*
   * Title :- onViewAnswerModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onViewAnswerModalClose = () => {
      this.setState({ openViewAnswerDialog: false, selectedUser: null })
   }

   /*
   * Title :- opnAddNewAnswerModal
   * Descrpation :- This function use for open add popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   opnAddNewAnswerModal() {
      this.setState({ addNewAnswerModal: true });
   }
   /*
   * Title :- onChangeAddNewQuestionsDetails
   * Descrpation :- This function use for check field validation on add new question
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onChangeAddNewAnswerDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'answers':
            if (isEmpty(value)) {
               add_err[key] = "Answer name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'security_questions_id':
            if (isEmpty(value)) {
               add_err[key] = "Question can't be blank";
            } else {
               if(currentUserId()){
                  add_err['security_questions_id'] = "";
                  this.props.checkSecurityAnswerExist(value, currentUserId());
               }
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewAnsweretail: {
            ...this.state.addNewAnsweretail,
            [key]: value
         }
      });
   }

   /*
   * Title :- onUpdateAnswerDetails
   * Descrpation :- This function use for check filed validation on edit question case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onUpdateAnswerDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'answers':
            if (isEmpty(value)) {
               udpate_err[key] = "Answer can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'security_questions_id':
            if (isEmpty(value)) {
               udpate_err[key] = "Question can't be blank";
            } else {
               if(this.state.editAnswer.user_id){
                  udpate_err['security_questions_id'] = "";
                  this.props.checkSecurityAnswerExist(value, currentUserId(), this.state.editAnswer.id);
               }
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editAnswer: {
            ...this.state.editAnswer,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.answers === '' &&
         this.state.add_err.security_questions_id === ''
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.udpate_err.answers === '' &&
         this.state.udpate_err.security_questions_id === ''
      );
   }
   /*
   * Title :- addNewAnswer
   * Descrpation :- This function use for add new question action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   addNewAnswer() {
      this.props.insertProviderAnswer(this.state.addNewAnsweretail);
      this.setState({ addNewAnswerModal: false });
      let self = this;
      let addNewAnsweretail = {
         security_questions_id: '',
         answers: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewAnsweretail: addNewAnsweretail });
      }, 2000);
   }
   /*
   * Title :- updateQuestion
   * Descrpation :- This function use call question update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   updateQuestion() {
      const { editAnswer } = this.state;
      
      //const userName = this.props.userslist.filter(x => x.user_id == editAnswer.user_id);
      const questionName = this.props.questionlist.filter(x => x.security_questions_id == editAnswer.security_questions_id);
      //editAnswer.f_name = userName[0].f_name;
      //editAnswer.m_name = userName[0].m_name;
      //editAnswer.l_name = userName[0].l_name;
      editAnswer.name = questionName[0].name;
      
      let indexOfUpdateQuestion = '';
      let answerCurrent = this.props.answerlist;
      for (let i = 0; i < answerCurrent.length; i++) {
         const current = answerCurrent[i];
         if (current.id === editAnswer.id) {
            indexOfUpdateQuestion = i
         }
      }
      this.props.updateProviderAnswer(editAnswer);

      answerCurrent[indexOfUpdateQuestion] = editAnswer;

      this.setState({ editAnswer: null, addNewAnswerModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ answerCurrent });
      //}, 2000);
   }
   /*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }
   /*
   * Title :- deleteAnswerPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   deleteAnswerPermanently() {
      const { selectedUser } = this.state;
      let answers = this.props.answerlist;
      let indexOfDeleteQuestion = answers.indexOf(selectedUser);
      this.props.deleteProviderAnswer(answers[indexOfDeleteQuestion].id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedUser: null });
   }
   render() {
      const { selectedUser, editAnswer, add_err, udpate_err } = this.state;
      const answerlist = this.props.answerlist;
      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Security Question',
            field: 'name',
         },
         {
            name: 'Answer',
            field: 'answers',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        <a href="javascript:void(0)" onClick={() => this.viewAnswerDetail(value)}><i className="ti-eye"></i></a>
                        <a href="javascript:void(0)" onClick={() => this.onEditAnswer(value)}><i className="ti-pencil"></i></a>
                        <a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a>
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Question.csv' },
         customToolbar: () => {
            return (
               <CustomToolbar opnAddNewAnswerModal={this.opnAddNewAnswerModal.bind(this)} />
            );
         },
      };
      return (
         <div className="system-settings security-questions">
            <Helmet>
               <title>Health Partner | System | System Settings | Security Questions</title>
               <meta name="description" content="Security Questions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="provider.security-answers" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={answerlist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete Answer permanently."
               onConfirm={() => this.deleteAnswerPermanently()}
            />
            <Modal isOpen={this.state.addNewAnswerModal} toggle={() => this.onAddUpdateAnswerModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateAnswerModalClose()}>
                  {editAnswer === null ?
                     'Add Security Answer' : 'Update Security Answer'
                  }
               </ModalHeader>
               <ModalBody>
                  {editAnswer === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewAnsweretails={this.state.addNewAnsweretail}
                        questionlists={this.props.questionlist}
                        userslists={this.props.userslist}
                        onChangeAddNewAnswerDetails={this.onChangeAddNewAnswerDetails.bind(this)}

                     />
                     :
                     <UpdateUserForm
                        editAnswer={editAnswer}
                        addErr={udpate_err}
                        questionlists={this.props.questionlist}
                        userslists={this.props.userslist}
                        onUpdateAnswerDetails={this.onUpdateAnswerDetails.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editAnswer === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewAnswer()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateQuestion()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateAnswerModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewAnswerDialog} toggle={() => this.onViewAnswerModalClose()}>
               <ModalHeader toggle={() => this.onViewAnswerModalClose()}>
                  {selectedUser !== null ? 'Security Answer Details' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">

                                 <div className="colmn-row"><span className="first-colmn fw-bold">Question:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Answer:</span> <span className="second-colmn">{selectedUser.answers}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedUser.status == 1) ? 'Active' : 'Inactive'}</span></div>

                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, AnswerDetails }) => {
   const { user } = authUser;
   const { loading, answerlist, questionlist, userslist, answerExist, isEdit } = AnswerDetails;
   return { loading, answerlist, questionlist, userslist, user, answerExist, isEdit }
   //return {user }

}

export default connect(mapStateToProps, {
   checkSecurityAnswerExist, getAllAnswer, insertAnswer, updateAnswer, deleteAnswer, getAllProviderAnswer, insertProviderAnswer, updateProviderAnswer, deleteProviderAnswer
})(SecurityQuestions);