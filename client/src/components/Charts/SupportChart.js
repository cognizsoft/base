import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';

import { connect } from 'react-redux';

const options = {
   legend: {
      display: false,
      labels: {
         fontColor: ChartConfig.legendFontColor
      }
   },
   cutoutPercentage: 50
};

class SupportChart extends Component {

   render() {
      const data = {
         labels: [
            'Hold',
            'Closed',
            'Open',
            'Follow Up'
         ],
         datasets: [{
            data: [
               (this.props.supportStatus)?this.props.supportStatus['Hold']:0, 
               (this.props.supportStatus)?this.props.supportStatus['Closed']:0,
               (this.props.supportStatus)?this.props.supportStatus['open']:0,
               (this.props.supportStatus)?this.props.supportStatus['FollowUp']:0
            ],
            backgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark
            ],
            hoverBackgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark
            ]
         }]
      };
      return (
         (this.props.supportStatus && (this.props.supportStatus['Hold'] > 0 || this.props.supportStatus['Closed'] > 0 || this.props.supportStatus['open'] > 0 || this.props.supportStatus['FollowUp'] > 0))?
         <Doughnut data={data} options={options} height={100} />
         :
         'No Data to display chart'
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, supportStatus } = DashboardDetails;
   return { loading, supportStatus }

}

export default connect(mapStateToProps, {
})(SupportChart);