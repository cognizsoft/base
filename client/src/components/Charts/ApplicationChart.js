import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';

import { connect } from 'react-redux';

const options = {
   legend: {
      display: false,
      labels: {
         fontColor: ChartConfig.legendFontColor
      }
   },
   cutoutPercentage: 50
};

class ApplicationChart extends Component {

   render() {
      const data = {
         labels: [
            'Approved',
            'Rejected',
            'Pending',
            'Manual Processing'
         ],
         datasets: [{
            data: [
               (this.props.applicationDetails)?this.props.applicationDetails['approved']:0, 
               (this.props.applicationDetails)?this.props.applicationDetails['rejected']:0,
               (this.props.applicationDetails)?this.props.applicationDetails['pending']:0,
               (this.props.applicationDetails)?this.props.applicationDetails['manual']:0
            ],
            backgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark
            ],
            hoverBackgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark
            ]
         }]
      };
      return (
         (this.props.applicationDetails && (this.props.applicationDetails['approved'] > 0 || this.props.applicationDetails['rejected'] > 0 || this.props.applicationDetails['pending'] > 0 || this.props.applicationDetails['manual'] > 0))?
         <Doughnut data={data} options={options} height={100} />
         :
         'No Data to display chart'
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, applicationDetails } = DashboardDetails;
   return { loading, applicationDetails }

}

export default connect(mapStateToProps, {
})(ApplicationChart );