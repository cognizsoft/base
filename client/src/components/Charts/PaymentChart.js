import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';

import { connect } from 'react-redux';

const options = {
   legend: {
      display: false,
      labels: {
         fontColor: ChartConfig.legendFontColor
      }
   },
   cutoutPercentage: 50
};

class PaymentChart extends Component {

   render() {
      const data = {
         labels: [
            'Received',
            'Due',
            'Submitted',
            'IOU',
            'Refund'
         ],
         datasets: [{
            data: [
               (this.props.invoiceStatus)?parseFloat(this.props.invoiceStatus['received']).toFixed(2):"0.00", 
               (this.props.invoiceStatus)?parseFloat(this.props.invoiceStatus['due']).toFixed(2):"0.00",
               (this.props.invoiceStatus)?parseFloat(this.props.invoiceStatus['total_amount']).toFixed(2):"0.00",
               (this.props.refundStatus)?parseFloat(this.props.refundStatus['iou']).toFixed(2):"0.00",
               (this.props.refundStatus)?parseFloat(this.props.refundStatus['refund']).toFixed(2):"0.00"
            ],
            backgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark,
               ChartConfig.color.purple
            ],
            hoverBackgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info,
               ChartConfig.color.dark,
               ChartConfig.color.purple
            ]
         }]
      };
      return (
         (this.props.invoiceStatus && (this.props.invoiceStatus['received'] > 0 || this.props.invoiceStatus['due'] > 0 || this.props.invoiceStatus['total_amount'] > 0 || this.props.refundStatus['iou'] > 0 || this.props.refundStatus['refund'] > 0))?
         <Doughnut data={data} options={options} height={100} />
         :
         'No Data to display chart'
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, invoiceStatus, refundStatus } = DashboardDetails;
   return { loading, invoiceStatus, refundStatus }

}

export default connect(mapStateToProps, {
})(PaymentChart);