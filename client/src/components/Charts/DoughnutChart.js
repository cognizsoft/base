import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';

import { connect } from 'react-redux';

const options = {
   legend: {
      display: false,
      labels: {
         fontColor: ChartConfig.legendFontColor
      }
   },
   cutoutPercentage: 50
};

class PaymentChart extends Component {

   render() {
      const data = {
         labels: [
            'Submitted',
            'Approved',
            'Confirm Payment',
            'Cancel'
         ],
         datasets: [{
            data: [
               (this.props.invoiceStatus)?this.props.invoiceStatus['submitted']:0, 
               (this.props.invoiceStatus)?this.props.invoiceStatus['approved']:0,
               (this.props.invoiceStatus)?this.props.invoiceStatus['confirm_payment']:0,
               (this.props.invoiceStatus)?this.props.invoiceStatus['cancel']:0
            ],
            backgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info
            ],
            hoverBackgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.info
            ]
         }]
      };
      return (
         <Doughnut data={data} options={options} height={100} />
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, invoiceStatus } = DashboardDetails;
   return { loading, invoiceStatus }

}

export default connect(mapStateToProps, {
})(PaymentChart);