import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartConfig from 'Constants/chart-config';

import { connect } from 'react-redux';

const options = {
   legend: {
      display: false,
      labels: {
         fontColor: ChartConfig.legendFontColor
      }
   },
   cutoutPercentage: 50
};

class CustomerInvoiceChart extends Component {

   render() {
      const data = {
         labels: [
            'Paid',
            'In Progress',
            'Partial Paid',
            'Due',
            'Deferred',
            'Cancelled',
            'Refunded',
         ],
         datasets: [{
            data: [
               (this.props.customerStatus)?this.props.customerStatus['paid']:0, 
               (this.props.customerStatus)?this.props.customerStatus['progress']:0,
               (this.props.customerStatus)?this.props.customerStatus['partial']:0,
               (this.props.customerStatus)?this.props.customerStatus['due']:0,
               (this.props.customerStatus)?this.props.customerStatus['deferred']:0,
               (this.props.customerStatus)?this.props.customerStatus['cancelled']:0,
               (this.props.customerStatus)?this.props.customerStatus['refunded']:0,
            ],
            backgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.success,
               ChartConfig.color.purple,
               ChartConfig.color.danger,
               ChartConfig.color.dark,
               ChartConfig.color.lightBlue,
            ],
            hoverBackgroundColor: [
               ChartConfig.color.primary,
               ChartConfig.color.warning,
               ChartConfig.color.success,
               ChartConfig.color.purple,
               ChartConfig.color.danger,
               ChartConfig.color.dark,
               ChartConfig.color.lightBlue,
            ]
         }]
      };
      return (
         (this.props.customerStatus && (this.props.customerStatus['paid'] > 0 || this.props.customerStatus['progress'] > 0 || this.props.customerStatus['partial'] > 0 || this.props.customerStatus['due'] > 0 || this.props.customerStatus['deferred'] > 0 || this.props.customerStatus['cancelled'] > 0 || this.props.customerStatus['refunded'] > 0))?
         <Doughnut data={data} options={options} height={100} />
         :
         'No Data to display chart'
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, customerStatus } = DashboardDetails;
   return { loading, customerStatus }

}

export default connect(mapStateToProps, {
})(CustomerInvoiceChart);