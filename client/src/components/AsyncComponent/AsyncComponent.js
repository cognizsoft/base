/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */
import React from 'react';
import Loadable from 'react-loadable';

// rct page loader
import RctPageLoader from 'Components/RctPageLoader/RctPageLoader';

// ecommerce dashboard
const AsyncHealthDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/health"),
   loading: () => <RctPageLoader />,
});

// provider dashboard
const AsyncProviderDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/provider"),
   loading: () => <RctPageLoader />,
});

// customer dashboard
/*const AsyncCustomerDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/customer"),
   loading: () => <RctPageLoader />,
});*/

// ecommerce dashboard
const AsyncEcommerceDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/ecommerce"),
   loading: () => <RctPageLoader />,
});

// agency dashboard
const AsyncSaasDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/saas"),
   loading: () => <RctPageLoader />,
});

// agency dashboard
const AsyncAgencyDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/agency"),
   loading: () => <RctPageLoader />,
});

// boxed dashboard
const AsyncNewsDashboardComponent = Loadable({
   loader: () => import("Routes/dashboard/news"),
   loading: () => <RctPageLoader />,
});

const AsyncUserWidgetComponent = Loadable({
   loader: () => import("Routes/widgets/user-widgets"),
   loading: () => <RctPageLoader />,
});

const AsyncUserChartsComponent = Loadable({
   loader: () => import("Routes/widgets/charts-widgets"),
   loading: () => <RctPageLoader />,
});

const AsyncGeneralWidgetsComponent = Loadable({
   loader: () => import("Routes/widgets/general-widgets"),
   loading: () => <RctPageLoader />,
});

const AsyncPromoWidgetsComponent = Loadable({
   loader: () => import("Routes/widgets/promo-widgets"),
   loading: () => <RctPageLoader />,
});

// about us
const AsyncAboutUsComponent = Loadable({
   loader: () => import("Routes/about-us"),
   loading: () => <RctPageLoader />,
});

// chat app
const AsyncChatComponent = Loadable({
   loader: () => import("Routes/chat"),
   loading: () => <RctPageLoader />,
});

// mail app
const AsyncMailComponent = Loadable({
   loader: () => import("Routes/mail"),
   loading: () => <RctPageLoader />,
});

// todo app
const AsyncTodoComponent = Loadable({
   loader: () => import("Routes/todo"),
   loading: () => <RctPageLoader />,
});

// gallery
const AsyncGalleryComponent = Loadable({
   loader: () => import("Routes/pages/gallery"),
   loading: () => <RctPageLoader />,
});

// feedback
const AsyncFeedbackComponent = Loadable({
   loader: () => import("Routes/pages/feedback"),
   loading: () => <RctPageLoader />,
});

// report
const AsyncReportComponent = Loadable({
   loader: () => import("Routes/pages/report"),
   loading: () => <RctPageLoader />,
});

// faq
const AsyncFaqComponent = Loadable({
   loader: () => import("Routes/pages/faq"),
   loading: () => <RctPageLoader />,
});

// pricing
const AsyncPricingComponent = Loadable({
   loader: () => import("Routes/pages/pricing"),
   loading: () => <RctPageLoader />,
});

// blank
const AsyncBlankComponent = Loadable({
   loader: () => import("Routes/pages/blank"),
   loading: () => <RctPageLoader />,
});

// google maps
const AsyncGooleMapsComponent = Loadable({
   loader: () => import("Routes/maps/google-map"),
   loading: () => <RctPageLoader />,
});

// google maps
const AsyncLeafletMapComponent = Loadable({
   loader: () => import("Routes/maps/leaflet-map"),
   loading: () => <RctPageLoader />,
});

// shop list
const AsyncShoplistComponent = Loadable({
   loader: () => import("Routes/ecommerce/shop-list"),
   loading: () => <RctPageLoader />,
});

// shop grid
const AsyncShopGridComponent = Loadable({
   loader: () => import("Routes/ecommerce/shop-grid"),
   loading: () => <RctPageLoader />,
});

// shop 
const AsyncShopComponent = Loadable({
   loader: () => import("Routes/ecommerce/shop"),
   loading: () => <RctPageLoader />,
});

// cart 
const AsyncCartComponent = Loadable({
   loader: () => import("Routes/ecommerce/cart"),
   loading: () => <RctPageLoader />,
});

// checkout 
const AsyncCheckoutComponent = Loadable({
   loader: () => import("Routes/ecommerce/checkout"),
   loading: () => <RctPageLoader />,
});

// invoice
const AsyncInvoiceComponent = Loadable({
   loader: () => import("Routes/ecommerce/invoice"),
   loading: () => <RctPageLoader />,
});

// react dragula
const AsyncReactDragulaComponent = Loadable({
   loader: () => import("Routes/drag-drop/react-dragula"),
   loading: () => <RctPageLoader />,
});

// react dnd
const AsyncReactDndComponent = Loadable({
   loader: () => import("Routes/drag-drop/react-dnd"),
   loading: () => <RctPageLoader />,
});

// themify icons
const AsyncThemifyIconsComponent = Loadable({
   loader: () => import("Routes/icons/themify-icons"),
   loading: () => <RctPageLoader />,
});

// Simple Line Icons
const AsyncSimpleLineIconsComponent = Loadable({
   loader: () => import("Routes/icons/simple-line-icons"),
   loading: () => <RctPageLoader />,
});

// Material Icons
const AsyncMaterialIconsComponent = Loadable({
   loader: () => import("Routes/icons/material-icons"),
   loading: () => <RctPageLoader />,
});

// Basic Table
const AsyncBasicTableComponent = Loadable({
   loader: () => import("Routes/tables/basic"),
   loading: () => <RctPageLoader />,
});

// Basic Table
const AsyncDataTableComponent = Loadable({
   loader: () => import("Routes/tables/data-table"),
   loading: () => <RctPageLoader />,
});

// Responsive Table
const AsyncResponsiveTableComponent = Loadable({
   loader: () => import("Routes/tables/responsive"),
   loading: () => <RctPageLoader />,
});



/*--------------- Charts ----------------*/

// Re charts
const AsyncRechartsComponent = Loadable({
   loader: () => import("Routes/charts/recharts"),
   loading: () => <RctPageLoader />,
});

// ReactChartsjs2
const AsyncReactChartsjs2Component = Loadable({
   loader: () => import("Routes/charts/react-chartjs2"),
   loading: () => <RctPageLoader />,
});

/*---------------------- Calendar -----------*/

// Basic Calendar
const AsyncBasicCalendarComponent = Loadable({
   loader: () => import("Routes/calendar/BasicCalendar"),
   loading: () => <RctPageLoader />,
});

// Cultures Calendar
const AsyncCulturesComponent = Loadable({
   loader: () => import("Routes/calendar/Cultures"),
   loading: () => <RctPageLoader />,
});

// Selectable Calendar
const AsyncSelectableComponent = Loadable({
   loader: () => import("Routes/calendar/Selectable"),
   loading: () => <RctPageLoader />,
});

// Custom Calendar
const AsyncCustomComponent = Loadable({
   loader: () => import("Routes/calendar/Custom"),
   loading: () => <RctPageLoader />,
});

/*---------------- Session ------------------*/

// Session Login
const AsyncSessionLoginComponent = Loadable({
   loader: () => import("Routes/session/login"),
   loading: () => <RctPageLoader />,
});

// Session Register
const AsyncSessionRegisterComponent = Loadable({
   loader: () => import("Routes/session/register"),
   loading: () => <RctPageLoader />,
});

// Session Lock Screen
const AsyncSessionLockScreenComponent = Loadable({
   loader: () => import("Routes/session/lock-screen"),
   loading: () => <RctPageLoader />,
});

// Session Forgot Password
const AsyncSessionForgotPasswordComponent = Loadable({
   loader: () => import("Routes/session/forgot-password"),
   loading: () => <RctPageLoader />,
});

// Session Page 404
const AsyncSessionPage404Component = Loadable({
   loader: () => import("Routes/session/404"),
   loading: () => <RctPageLoader />,
});

// Session Page 404
const AsyncSessionPage500Component = Loadable({
   loader: () => import("Routes/session/500"),
   loading: () => <RctPageLoader />,
});

// terms and condition
const AsyncTermsConditionComponent = Loadable({
   loader: () => import("Routes/pages/terms-condition"),
   loading: () => <RctPageLoader />,
});

/*---------------- Editor -------------------*/

// editor quill
const AsyncQuillEditorComponent = Loadable({
   loader: () => import("Routes/editor/quill-editor"),
   loading: () => <RctPageLoader />,
});

// editor Wysiwyg
const AsyncWysiwygEditorComponent = Loadable({
   loader: () => import("Routes/editor/wysiwyg-editor"),
   loading: () => <RctPageLoader />,
});

/*------------- Form Elemets -------------*/

// forms elements
const AsyncFormElementsComponent = Loadable({
   loader: () => import("Routes/forms/form-elements"),
   loading: () => <RctPageLoader />,
});

// forms TextField
const AsyncTextFieldComponent = Loadable({
   loader: () => import("Routes/forms/material-text-field"),
   loading: () => <RctPageLoader />,
});

// forms TextField
const AsyncSelectListComponent = Loadable({
   loader: () => import("Routes/forms/select-list"),
   loading: () => <RctPageLoader />,
});

/*------------------ UI Components ---------------*/

// components Alerts
const AsyncUIAlertsComponent = Loadable({
   loader: () => import("Routes/components/alert"),
   loading: () => <RctPageLoader />,
});

// components Appbar
const AsyncUIAppbarComponent = Loadable({
   loader: () => import("Routes/components/app-bar"),
   loading: () => <RctPageLoader />,
});

// components BottomNavigation
const AsyncUIBottomNavigationComponent = Loadable({
   loader: () => import("Routes/components/bottom-navigation"),
   loading: () => <RctPageLoader />,
});

// components BottomNavigation
const AsyncUIAvatarsComponent = Loadable({
   loader: () => import("Routes/components/avatar"),
   loading: () => <RctPageLoader />,
});

// components Buttons
const AsyncUIButtonsComponent = Loadable({
   loader: () => import("Routes/components/buttons"),
   loading: () => <RctPageLoader />,
});

// components Badges
const AsyncUIBadgesComponent = Loadable({
   loader: () => import("Routes/components/badges"),
   loading: () => <RctPageLoader />,
});

// components CardMasonary
const AsyncUICardMasonaryComponent = Loadable({
   loader: () => import("Routes/components/card-masonry"),
   loading: () => <RctPageLoader />,
});

// components Cards
const AsyncUICardsComponent = Loadable({
   loader: () => import("Routes/components/cards"),
   loading: () => <RctPageLoader />,
});

// components Chips
const AsyncUIChipsComponent = Loadable({
   loader: () => import("Routes/components/chip"),
   loading: () => <RctPageLoader />,
});

// components Dialog
const AsyncUIDialogComponent = Loadable({
   loader: () => import("Routes/components/dialog"),
   loading: () => <RctPageLoader />,
});

// components Dividers
const AsyncUIDividersComponent = Loadable({
   loader: () => import("Routes/components/dividers"),
   loading: () => <RctPageLoader />,
});

// components Drawers
const AsyncUIDrawersComponent = Loadable({
   loader: () => import("Routes/components/drawers"),
   loading: () => <RctPageLoader />,
});

// components ExpansionPanel
const AsyncUIExpansionPanelComponent = Loadable({
   loader: () => import("Routes/components/expansion-panel"),
   loading: () => <RctPageLoader />,
});

// components Grid List
const AsyncUIGridListComponent = Loadable({
   loader: () => import("Routes/components/grid-list"),
   loading: () => <RctPageLoader />,
});

// components List
const AsyncUIListComponent = Loadable({
   loader: () => import("Routes/components/list"),
   loading: () => <RctPageLoader />,
});

// components Menu
const AsyncUIMenuComponent = Loadable({
   loader: () => import("Routes/components/menu"),
   loading: () => <RctPageLoader />,
});

// components Popover
const AsyncUIPopoverComponent = Loadable({
   loader: () => import("Routes/components/popover"),
   loading: () => <RctPageLoader />,
});

// components Progress
const AsyncUIProgressComponent = Loadable({
   loader: () => import("Routes/components/progress"),
   loading: () => <RctPageLoader />,
});

// components Snackbar
const AsyncUISnackbarComponent = Loadable({
   loader: () => import("Routes/components/snackbar"),
   loading: () => <RctPageLoader />,
});

// components SelectionControls
const AsyncUISelectionControlsComponent = Loadable({
   loader: () => import("Routes/components/selection-controls"),
   loading: () => <RctPageLoader />,
});

/*---------------- Advance UI Components -------------*/

// advance components DateAndTimePicker
const AsyncAdvanceUIDateAndTimePickerComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/dateTime-picker"),
   loading: () => <RctPageLoader />,
});

// advance components Tabs
const AsyncAdvanceUITabsComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/tabs"),
   loading: () => <RctPageLoader />,
});

// advance components Stepper
const AsyncAdvanceUIStepperComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/stepper"),
   loading: () => <RctPageLoader />,
});

// advance components NotificationComponent
const AsyncAdvanceUINotificationComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/notification"),
   loading: () => <RctPageLoader />,
});

// advance components SweetAlert
const AsyncAdvanceUISweetAlertComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/sweet-alert"),
   loading: () => <RctPageLoader />,
});

// advance components autoComplete
const AsyncAdvanceUIAutoCompleteComponent = Loadable({
   loader: () => import("Routes/advance-ui-components/autoComplete"),
   loading: () => <RctPageLoader />,
});

// Users Management
const AsyncUserManagementComponent = Loadable({
   loader: () => import("Admin/user-management/users"),
   loading: () => <RctPageLoader />,
});

// Users type
const AsyncUsersTypeComponent = Loadable({
   loader: () => import("Admin/user-management/users-type"),
   loading: () => <RctPageLoader />,
});

// Users role
const AsyncUsersRoleComponent = Loadable({
   loader: () => import("Admin/user-management/users-role"),
   loading: () => <RctPageLoader />,
});

//user permission
const AsyncPermissionComponent = Loadable({
   loader: () => import("Admin/permission/PermissionList"),
   loading: () => <RctPageLoader />,
});

//master data
const AsyncMasterDataComponent = Loadable({
   loader: () => import("Admin/system/master/master-data"),
   loading: () => <RctPageLoader />,
});

//master data value
const AsyncMasterDataValueComponent = Loadable({
   loader: () => import("Admin/system/master/master-data-value"),
   loading: () => <RctPageLoader />,
});

//fee > discount rate
const AsyncFeeDiscountRateComponent = Loadable({
   loader: () => import("Admin/system/fee/discount-rate"),
   loading: () => <RctPageLoader />,
});

//fee > financial charges
const AsyncFeeFinancialChargeComponent = Loadable({
   loader: () => import("Admin/system/fee/financial-charges"),
   loading: () => <RctPageLoader />,
});

//fee > interest rate
const AsyncFeeInterestRateComponent = Loadable({
   loader: () => import("Admin/system/fee/interest-rate"),
   loading: () => <RctPageLoader />,
});

//fee > late fee waiver
const AsyncFeeLateFeeWaiversComponent = Loadable({
   loader: () => import("Admin/system/fee/late-fee-waivers"),
   loading: () => <RctPageLoader />,
});

//fee > late fee amount
const AsyncFeeLateFeeAmountComponent = Loadable({
   loader: () => import("Admin/system/fee/late-fee-amount"),
   loading: () => <RctPageLoader />,
});

//fee > surcharge type
const AsyncFeeSurchargeTypeComponent = Loadable({
   loader: () => import("Admin/system/fee/surcharge-type"),
   loading: () => <RctPageLoader />,
});

//system settings > system module
const AsyncSystemSettingSystemModuleComponent = Loadable({
   loader: () => import("Admin/system/system-settings/system-module"),
   loading: () => <RctPageLoader />,
});

//system settings > security questions
const AsyncSystemSettingSecurityQuestionsComponent = Loadable({
   loader: () => import("Admin/system/system-settings/security-questions"),
   loading: () => <RctPageLoader />,
});

//system settings > security answers
const AsyncSystemSettingSecurityAnswersComponent = Loadable({
   loader: () => import("Admin/system/system-settings/security-answers"),
   loading: () => <RctPageLoader />,
});

//country > city
const AsyncCountryCityComponent = Loadable({
   loader: () => import("Admin/system/country/city"),
   loading: () => <RctPageLoader />,
});

//country > states
const AsyncCountryStatesComponent = Loadable({
   loader: () => import("Admin/system/country/states"),
   loading: () => <RctPageLoader />,
});

//country > country
const AsyncCountryCountryComponent = Loadable({
   loader: () => import("Admin/system/country/countries"),
   loading: () => <RctPageLoader />,
});

//others > risk factor
const AsyncOthersRiskFactorComponent = Loadable({
   loader: () => import("Admin/system/others/risk-factor"),
   loading: () => <RctPageLoader />,
});

//others > score thresholds
const AsyncOthersScoreThresholdsComponent = Loadable({
   loader: () => import("Admin/system/others/score-thresholds"),
   loading: () => <RctPageLoader />,
});
// Procedures
const AsyncProceduresComponent = Loadable({
   loader: () => import("Admin/system/procedures"),
   loading: () => <RctPageLoader />,
});

//providers > add new
const AsyncProvidersAddNewComponent = Loadable({
   loader: () => import("Admin/providers/add-new"),
   loading: () => <RctPageLoader />,
});

//providers > provider list
const AsyncProvidersListComponent = Loadable({
   loader: () => import("Admin/providers/provider-list"),
   loading: () => <RctPageLoader />,
});

//providers > provider edit
const AsyncProvidersEditComponent = Loadable({
   loader: () => import("Admin/providers/edit"),
   loading: () => <RctPageLoader />,
});

//providers > provider invoice list
const AsyncProvidersInvoiceListComponent = Loadable({
   loader: () => import("Admin/providers/invoice-list"),
   loading: () => <RctPageLoader />,
});

//providers > provider single invoice
const AsyncProvidersSingleInvoiceComponent = Loadable({
   loader: () => import("Admin/providers/invoice"),
   loading: () => <RctPageLoader />,
});

//providers > provider report list
const AsyncProvidersReportListComponent = Loadable({
   loader: () => import("Admin/providers/report-list"),
   loading: () => <RctPageLoader />,
});

//customers > add new
const AsyncPatientsAddNewComponent = Loadable({
   loader: () => import("Admin/credit-applications/add-new"),
   loading: () => <RctPageLoader />,
});

//customers > patient list
const AsyncPatientsListComponent = Loadable({
   loader: () => import("Admin/customers/customers-list"),
   loading: () => <RctPageLoader />,
});

//customers > Payment Plan
const AsyncCustomerPaymentPlanComponent = Loadable({
   loader: () => import("Admin/customers/customerPayment-plan"),
   loading: () => <RctPageLoader />,
});

//customers > patient edit
const AsyncPatientsEditComponent = Loadable({
   loader: () => import("Admin/customers/edit"),
   loading: () => <RctPageLoader />,
});

//customers > patient edit
const AsyncAdminCustomerEditComponent = Loadable({
   loader: () => import("Admin/customers/customer-edit/edit-profile"),
   loading: () => <RctPageLoader />,
});

//customers > invoice list
const AsyncCustomerInvoiceListComponent = Loadable({
   loader: () => import("Admin/customers/invoice-list"),
   loading: () => <RctPageLoader />,
});

//customers > single invoice
const AsyncCustomerSingleInvoiceComponent = Loadable({
   loader: () => import("Admin/customers/invoice"),
   loading: () => <RctPageLoader />,
});

//customers > report list
const AsyncCustomerReportListComponent = Loadable({
   loader: () => import("Admin/customers/report-list"),
   loading: () => <RctPageLoader />,
});

//credit apllication
const AsyncCreditApplicationsComponent = Loadable({
   loader: () => import("Admin/credit-applications/application-list"),
   loading: () => <RctPageLoader />,
});

const AsyncAddNewMPSCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/add-new-MPS"),
   loading: () => <RctPageLoader />,
});

//view credit apllication
const AsyncViewCreditApplicationsComponent = Loadable({
   loader: () => import("Admin/credit-applications/application"),
   loading: () => <RctPageLoader />,
});

//edit credit apllication
const AsyncEditCreditApplicationsComponent = Loadable({
   loader: () => import("Admin/credit-applications/edit-application"),
   loading: () => <RctPageLoader />,
});


//payment plan detail
const AsyncPaymenPlanComponent = Loadable({
   loader: () => import("Admin/credit-applications/payment-plan"),
   loading: () => <RctPageLoader />,
});

//payment > Open invoice
const AsyncPaymentsOpenInvoiceComponent = Loadable({
   loader: () => import("Admin/accounts/open-invoice"),
   loading: () => <RctPageLoader />,
});
//payment > Close invoice
const AsyncPaymentsCloseInvoiceComponent = Loadable({
   loader: () => import("Admin/accounts/close-invoice"),
   loading: () => <RctPageLoader />,
});
//payment > view invoice
const AsyncPaymentsViewInvoiceComponent = Loadable({
   loader: () => import("Admin/accounts/view-invoice"),
   loading: () => <RctPageLoader />,
});

//payment > single invoice
const AsyncPaymentsSingleInvoiceComponent = Loadable({
   loader: () => import("Admin/accounts/invoice"),
   loading: () => <RctPageLoader />,
});

//view customer
const AsyncViewCustomerComponent = Loadable({
   loader: () => import("Admin/customers/view-customer"),
   loading: () => <RctPageLoader />,
});

//view provider
const AsyncViewProviderComponent = Loadable({
   loader: () => import("Admin/providers/provider-view"),
   loading: () => <RctPageLoader />,
});

//credit application > add new
const AsyncAddNewCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/add-new"),
   loading: () => <RctPageLoader />,
});

//credit application > add new
const AsyncEditCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/edit"),
   loading: () => <RctPageLoader />,
});

//credit application > review
const AsyncReviewCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/review"),
   loading: () => <RctPageLoader />,
});

//credit application > reject
const AsyncRejectCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/reject"),
   loading: () => <RctPageLoader />,
});
//credit application > upload document
const AsyncUploadCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/upload"),
   loading: () => <RctPageLoader />,
});

//credit application > review document
const AsyncReviewDocumentCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/reviewDocument"),
   loading: () => <RctPageLoader />,
});

//credit application > payment
const AsyncPaymentCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/payment"),
   loading: () => <RctPageLoader />,
});

//accounts > admin report
const AsyncAccountsLoanReportComponent = Loadable({
   loader: () => import("Admin/accounts/loan-reports"),
   loading: () => <RctPageLoader />,
});

//accounts > admin report
const AsyncAccountsProviderInvoiceReportComponent = Loadable({
   loader: () => import("Admin/accounts/provider-invoice-reports"),
   loading: () => <RctPageLoader />,
});
//accounts > invoice view
const AsyncInvoiceViewComponent = Loadable({
   loader: () => import("Admin/accounts/submittedInvoiceView"),
   loading: () => <RctPageLoader />,
});
//credit application > installment > invoice
const AsyncInstallmentInvoiceCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/invoice"),
   loading: () => <RctPageLoader />,
});

//credit application > installment > view
const AsyncInstallmentViewCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/invoice/view"),
   loading: () => <RctPageLoader />,
});

const AsyncPaymentReceiptComponent = Loadable({
   loader: () => import("Admin/credit-applications/receipt"),
   loading: () => <RctPageLoader />,
});

const AsyncAccountsWeekMonthInvoiceReportComponent = Loadable({
   loader: () => import("Admin/accounts/week-month-reports"),
   loading: () => <RctPageLoader />,
});
const AsyncAccountsPrintMultipalInvoicesComponent = Loadable({
   loader: () => import("Admin/accounts/print-multiple-invoices"),
   loading: () => <RctPageLoader />,
});
const AsyncScoreRangeComponent = Loadable({
   loader: () => import("Admin/system/fee/score-range"),
   loading: () => <RctPageLoader />,
});


/***********************/
/*******PROVIDERS*******/
/***********************/
const AsyncProviderProfileComponent = Loadable({
   loader: () => import("Provider/provider-profile"),
   loading: () => <RctPageLoader />,
});

//provider > users
const AsyncProviderUsersComponent = Loadable({
   loader: () => import("Provider/users"),
   loading: () => <RctPageLoader />,
});

//provider > users
const AsyncProviderCreditApplicationsComponent = Loadable({
   loader: () => import("Provider/credit-applications/application-list"),
   loading: () => <RctPageLoader />,
});

//view credit apllication
const AsyncProivderViewCreditApplicationsComponent = Loadable({
   loader: () => import("Provider/credit-applications/application"),
   loading: () => <RctPageLoader />,
});

//edit credit apllication
const AsyncProviderEditCreditApplicationsComponent = Loadable({
   loader: () => import("Provider/credit-applications/edit-application"),
   loading: () => <RctPageLoader />,
});

//provider payment plan detail
const AsyncProviderPaymenPlanComponent = Loadable({
   loader: () => import("Provider/credit-applications/payment-plan"),
   loading: () => <RctPageLoader />,
});

//provider payment plan select
const AsyncProviderCustomerPaymentPlanComponent = Loadable({
   loader: () => import("Provider/customers/customerPayment-plan"),
   loading: () => <RctPageLoader />,
});


//customers > add new
const AsyncProPatientsAddNewComponent = Loadable({
   loader: () => import("Provider/customers/add-new"),
   loading: () => <RctPageLoader />,
});

//cusomers > patient list
const AsyncProPatientsListComponent = Loadable({
   loader: () => import("Provider/customers/customers-list"),
   loading: () => <RctPageLoader />,
});

//cusomers > patient list view
const AsyncProPatientsViewComponent = Loadable({
   loader: () => import("Provider/customers/view-customer"),
   loading: () => <RctPageLoader />,
});

//cusomers > patient list view
const AsyncProPatientsEditComponent = Loadable({
   loader: () => import("Provider/customers/edit"),
   loading: () => <RctPageLoader />,
});

//invoice > invocie create list
const AsyncProCreateInvoiceListComponent = Loadable({
   loader: () => import("Provider/invoice/invoice-create-list"),
   loading: () => <RctPageLoader />,
});
//invoice > preview invoice

const AsyncProPreviewInvoiceListComponent = Loadable({
   loader: () => import("Provider/invoice/preview-invoice"),
   loading: () => <RctPageLoader />,
});
//invoice > invocie submitted list
const AsyncProSubmittedInvoiceListComponent = Loadable({
   loader: () => import("Provider/invoice/invoice-submitted-list"),
   loading: () => <RctPageLoader />,
});

//invoice > invocie
const AsyncProInvoiceComponent = Loadable({
   loader: () => import("Provider/invoice/invoice"),
   loading: () => <RctPageLoader />,
});

//Provider > credit application > add new
const AsyncProCreditApplicationAddNewComponent = Loadable({
   loader: () => import("Provider/credit-applications/add-new"),
   loading: () => <RctPageLoader />,
});

//Provider > credit application > edit
const AsyncProCreditApplicationEditComponent = Loadable({
   loader: () => import("Provider/credit-applications/edit"),
   loading: () => <RctPageLoader />,
});

//credit application > review
const AsyncProviderReviewCreditApplicationComponent = Loadable({
   loader: () => import("Provider/credit-applications/review"),
   loading: () => <RctPageLoader />,
});

//credit application > reject
const AsyncProviderRejectCreditApplicationComponent = Loadable({
   loader: () => import("Provider/credit-applications/reject"),
   loading: () => <RctPageLoader />,
});
//credit application > upload document
const AsyncProviderUploadCreditApplicationComponent = Loadable({
   loader: () => import("Provider/credit-applications/upload"),
   loading: () => <RctPageLoader />,
});

//credit application > review document
const AsyncProviderReviewDocumentCreditApplicationComponent = Loadable({
   loader: () => import("Provider/credit-applications/reviewDocument"),
   loading: () => <RctPageLoader />,
});

//customre agreement application
const AsyncProviderCustomerAgreementComponent = Loadable({
   loader: () => import("Provider/customers/agreement-plan"),
   loading: () => <RctPageLoader />,
});

//customre agreement view menu
const AsyncProviderCustomerAgreementViewComponent = Loadable({
   loader: () => import("Provider/agreement"),
   loading: () => <RctPageLoader />,
});
/***********************/
/*******CUSTOMER*******/
/***********************/

//customer dashboard show plan
const AsyncCustomerCreditApplicationComponent = Loadable({
   loader: () => import("Customer/credit-applications/active-plan"),
   loading: () => <RctPageLoader />,
});

//customer profile
const AsyncCustomerProfileComponent = Loadable({
   loader: () => import("Customer/customer-profile"),
   loading: () => <RctPageLoader />,
});

//customer dashboard show plan
const AsyncCustomerInstallmentViewComponent = Loadable({
   loader: () => import("Customer/credit-applications/installment/view"),
   loading: () => <RctPageLoader />,
});
//customer dashboard show plan receipt
const AsyncCustomerInstallmentReceiptComponent = Loadable({
   loader: () => import("Customer/credit-applications/installment/receipt"),
   loading: () => <RctPageLoader />,
});
//customer dashboard show plan
const AsyncCustomerInstallmentPaymentComponent = Loadable({
   loader: () => import("Customer/credit-applications/installment/payment"),
   loading: () => <RctPageLoader />,
});
//customer dashboard show invoice
const AsyncCustomerInvoiceComponent = Loadable({
   loader: () => import("Customer/invoice/week-month-reports"),
   loading: () => <RctPageLoader />,
});
/**
 * Chnages according to client
 */
//provider payment plan detail
const AsyncProviderSearchApplicationComponent = Loadable({
   loader: () => import("Provider/credit-applications/search-application"),
   loading: () => <RctPageLoader />,
});
const AsyncProviderSearchCustomerComponent = Loadable({
   loader: () => import("Provider/credit-applications/search-customer"),
   loading: () => <RctPageLoader />,
});
const AsyncProviderPlanEstimateComponent = Loadable({
   loader: () => import("Provider/credit-applications/plan-estimate"),
   loading: () => <RctPageLoader />,
});

//admin applications plan details
const AsyncAdminApplicationPlanDetailsComponent = Loadable({
   loader: () => import("Admin/credit-applications/plan-details"),
   loading: () => <RctPageLoader />,
});

//provider applications plan details
const AsyncProviderApplicationPlanDetailsComponent = Loadable({
   loader: () => import("Provider/credit-applications/plan-details"),
   loading: () => <RctPageLoader />,
});

//AsyncProviderApplicationAuthorizedComponent
const AsyncProviderApplicationAuthorizedComponent = Loadable({
   loader: () => import("Provider/credit-applications/application-authorized"),
   loading: () => <RctPageLoader />,
});
const AsyncProviderApplicationDeclinedComponent = Loadable({
   loader: () => import("Provider/credit-applications/application-declined"),
   loading: () => <RctPageLoader />,
});
const AsyncProviderApplicationManualComponent = Loadable({
   loader: () => import("Provider/credit-applications/application-manual"),
   loading: () => <RctPageLoader />,
});

//admin applications all documents
const AsyncAdminApplicationAllDocumentsComponent = Loadable({
   loader: () => import("Admin/credit-applications/all-documents"),
   loading: () => <RctPageLoader />,
});

//provider applications all documents
const AsyncProviderApplicationAllDocumentsComponent = Loadable({
   loader: () => import("Provider/credit-applications/all-documents"),
   loading: () => <RctPageLoader />,
});

//provider edit profile
const AsyncProviderEditProfileComponent = Loadable({
   loader: () => import("Provider/provider-profile/edit-profile"),
   loading: () => <RctPageLoader />,
});
//provider careCloud
const AsyncProviderCareCloudComponent = Loadable({
   loader: () => import("Provider/care-cloud/carecloud"),
   loading: () => <RctPageLoader />,
});
//provider invoice view
const AsyncProviderInvoiceViewComponent = Loadable({
   loader: () => import("Provider/invoice/submittedInvoiceView"),
   loading: () => <RctPageLoader />,
});
//customer edit profile
const AsyncCustomerEditProfileComponent = Loadable({
   loader: () => import("Customer/customer-profile/edit-profile"),
   loading: () => <RctPageLoader />,
});

///////NEW CHANGES//////
//admin interest rate
const AsyncAdminTermMonthComponent = Loadable({
   loader: () => import("Admin/system/term-month"),
   loading: () => <RctPageLoader />,
});

const AsyncAdminLoanAmountComponent = Loadable({
   loader: () => import("Admin/system/loan-amount"),
   loading: () => <RctPageLoader />,
});

const AsyncAdminInterestRateComponent = Loadable({
   loader: () => import("Admin/system/interest-rate"),
   loading: () => <RctPageLoader />,
});
//admin approve application
const AsyncAdminApplicationAuthorizedComponent = Loadable({
   loader: () => import("Admin/credit-applications/application-authorized"),
   loading: () => <RctPageLoader />,
});
//provider security answer
const AsyncProviderSecurityAnswerComponent = Loadable({
   loader: () => import("Provider/provider-profile/security-answers"),
   loading: () => <RctPageLoader />,
});
//customer security answer
const AsyncCustomerSecurityAnswerComponent = Loadable({
   loader: () => import("Customer/security-answers"),
   loading: () => <RctPageLoader />,
});
//accounts > withdrawal report
const AsyncAdminWithdrawalComponent = Loadable({
   loader: () => import("Admin/accounts/customer-withdrawal-reports"),
   loading: () => <RctPageLoader />,
});
//accounts > withdrawal report
const AsyncAdminEmailTemplatesComponent = Loadable({
   loader: () => import("Admin/system/email-templates"),
   loading: () => <RctPageLoader />,
});
//accounts > Procedure date report
const AsyncAdminProcedureDateComponent = Loadable({
   loader: () => import("Admin/accounts/procedure-date-reports"),
   loading: () => <RctPageLoader />,
});
//credit application > upload plan agreement
const AsyncProviderUploadPlanAgreementComponent = Loadable({
   loader: () => import("Provider/credit-applications/upload-agreement"),
   loading: () => <RctPageLoader />,
});

//credit application > review document
const AsyncReviewPlanAgreementCreditApplicationComponent = Loadable({
   loader: () => import("Admin/credit-applications/reviewAgreement"),
   loading: () => <RctPageLoader />,
});

//provider plan list
const AsyncProviderPlanListComponent = Loadable({
   loader: () => import("Provider/credit-applications/plan-list/index"),
   loading: () => <RctPageLoader />,
});
// option to close
const AsyncPaymentOptionToCloseComponent = Loadable({
   loader: () => import("Admin/credit-applications/option-to-close"),
   loading: () => <RctPageLoader />,
});
//customer invoice pause
const AsyncPatientsInvoicePauseComponent = Loadable({
   loader: () => import("Admin/customers/customer-invoice-pause"),
   loading: () => <RctPageLoader />,
});

//credit application > upload plan agreement
const AsyncAdminUploadPlanAgreementComponent = Loadable({
   loader: () => import("Admin/credit-applications/upload-agreement"),
   loading: () => <RctPageLoader />,
});

//customer support
const AsyncAdminCustomerSupportComponent = Loadable({
   loader: () => import("Admin/customer-support"),
   loading: () => <RctPageLoader />,
});
const AsyncAdminProviderSupportComponent = Loadable({
   loader: () => import("Admin/customer-support/provider-support"),
   loading: () => <RctPageLoader />,
});

//customer support > view ticket
const AsyncAdminCustomerSupportViewTicketComponent = Loadable({
   loader: () => import("Admin/customer-support/view-ticket"),
   loading: () => <RctPageLoader />,
});

//customer > customer support > view ticket
const AsyncCustomerSupportViewTicketComponent = Loadable({
   loader: () => import("Customer/customer-support/view-ticket"),
   loading: () => <RctPageLoader />,
});

//provider > customer support > view ticket
const AsyncProviderCustomerSupportViewTicketComponent = Loadable({
   loader: () => import("Provider/customer-support/view-ticket"),
   loading: () => <RctPageLoader />,
});

// refund invoice
//invoice > invocie create list
const AsyncProRefundInvoiceListComponent = Loadable({
   loader: () => import("Provider/invoice/invoice-refund-list"),
   loading: () => <RctPageLoader />,
});

// refund admin invoice
//invoice > invocie create list
const AsyncProRefundAdminInvoiceListComponent = Loadable({
   loader: () => import("Admin/accounts/invoice-refund-list"),
   loading: () => <RctPageLoader />,
});

//accounts > account-receivables
const AsyncAdminAccountReceivablesComponent = Loadable({
   loader: () => import("Admin/accounts/account-receivables"),
   loading: () => <RctPageLoader />,
});

//customer-support > provider-support-report
const AsyncAdminProviderSupportReportComponent = Loadable({
   loader: () => import("Admin/customer-support/provider-support-reports"),
   loading: () => <RctPageLoader />,
});

//customer-support > customer-support-report
const AsyncAdminCustomerSupportReportComponent = Loadable({
   loader: () => import("Admin/customer-support/customer-support-reports"),
   loading: () => <RctPageLoader />,
});
/** account payable */
const AsyncAdminAccountPayableComponent = Loadable({
   loader: () => import("Admin/accounts/account-payable"),
   loading: () => <RctPageLoader />,
});
/** customers all documents */
const AsyncAdminCustomerDocumentsComponent = Loadable({
   loader: () => import("Admin/customers/all-documents"),
   loading: () => <RctPageLoader />,
});
/** provider all documents */
const AsyncAdminProviderDocumentsComponent = Loadable({
   loader: () => import("Admin/providers/all-documents"),
   loading: () => <RctPageLoader />,
});
/** account payable */
const AsyncAdminCreditChargesComponent = Loadable({
   loader: () => import("Admin/accounts/credit-charges"),
   loading: () => <RctPageLoader />,
});
/** cancel plan detail */
const AsyncAdminCancelPlanDetailComponent = Loadable({
   loader: () => import("Admin/credit-applications/details"),
   loading: () => <RctPageLoader />,
});

/** Co-signer dashboard details component */
const AsyncCosignerCreditApplicationComponent = Loadable({
   loader: () => import("Cosigner/credit-applications/application-list"),
   loading: () => <RctPageLoader />,
});
const AsyncCosignerViewCreditApplicationsComponent = Loadable({
   loader: () => import("Cosigner/credit-applications/plan-details"),
   loading: () => <RctPageLoader />,
});

const AsyncCosignerInstallmentPaymentComponent = Loadable({
   loader: () => import("Cosigner/credit-applications/installment/payment"),
   loading: () => <RctPageLoader />,
});

const AsyncCosignerInstallmentReceiptComponent = Loadable({
   loader: () => import("Cosigner/credit-applications/installment/receipt"),
   loading: () => <RctPageLoader />,
});

const AsyncCosignerSecurityAnswerComponent = Loadable({
   loader: () => import("Cosigner/security-answers"),
   loading: () => <RctPageLoader />,
});

const AsyncCosignerInvoiceComponent = Loadable({
   loader: () => import("Cosigner/invoice/week-month-reports"),
   loading: () => <RctPageLoader />,
});

const AsyncCosignerProfileComponent = Loadable({
   loader: () => import("Cosigner/customer-profile"),
   loading: () => <RctPageLoader />,
});
const AsyncCosignerEditProfileComponent = Loadable({
   loader: () => import("Cosigner/customer-profile/edit-profile"),
   loading: () => <RctPageLoader />,
});
//customer > customer support > view ticket
const AsyncCustomerSupportTicketComponent = Loadable({
   loader: () => import("Customer/customer-support/list"),
   loading: () => <RctPageLoader />,
});
const AsyncCosignerSupportTicketComponent = Loadable({
   loader: () => import("Cosigner/customer-support/list"),
   loading: () => <RctPageLoader />,
});
//customer > customer support > view ticket
const AsyncCosignerSupportViewTicketComponent = Loadable({
   loader: () => import("Cosigner/customer-support/view-ticket"),
   loading: () => <RctPageLoader />,
});
const AsyncAdminApplicationPlanDocumentsComponent = Loadable({
   loader: () => import("Admin/credit-applications/plan-documents"),
   loading: () => <RctPageLoader />,
});
const AsyncAdminApplicationDiskDocumentsComponent = Loadable({
   loader: () => import("Admin/credit-applications/disk-documents"),
   loading: () => <RctPageLoader />,
});

const AsyncCreditApplicationsReviewComponent = Loadable({
   loader: () => import("Admin/credit-applications/application-review"),
   loading: () => <RctPageLoader />,
});
export {
   AsyncUserWidgetComponent,
   AsyncUserChartsComponent,
   AsyncGeneralWidgetsComponent,
   AsyncPromoWidgetsComponent,
   AsyncAboutUsComponent,
   AsyncChatComponent,
   AsyncMailComponent,
   AsyncTodoComponent,
   AsyncGalleryComponent,
   AsyncFeedbackComponent,
   AsyncReportComponent,
   AsyncFaqComponent,
   AsyncPricingComponent,
   AsyncBlankComponent,
   AsyncGooleMapsComponent,
   AsyncLeafletMapComponent,
   AsyncShoplistComponent,
   AsyncShopGridComponent,
   AsyncInvoiceComponent,
   AsyncReactDragulaComponent,
   AsyncReactDndComponent,
   AsyncThemifyIconsComponent,
   AsyncSimpleLineIconsComponent,
   AsyncMaterialIconsComponent,
   AsyncBasicTableComponent,
   AsyncDataTableComponent,
   AsyncResponsiveTableComponent,
   AsyncRechartsComponent,
   AsyncReactChartsjs2Component,
   AsyncBasicCalendarComponent,
   AsyncCulturesComponent,
   AsyncSelectableComponent,
   AsyncCustomComponent,
   AsyncSessionLoginComponent,
   AsyncSessionRegisterComponent,
   AsyncSessionLockScreenComponent,
   AsyncSessionForgotPasswordComponent,
   AsyncSessionPage404Component,
   AsyncSessionPage500Component,
   AsyncTermsConditionComponent,
   AsyncQuillEditorComponent,
   AsyncWysiwygEditorComponent,
   AsyncFormElementsComponent,
   AsyncTextFieldComponent,
   AsyncSelectListComponent,
   AsyncUIAlertsComponent,
   AsyncUIAppbarComponent,
   AsyncUIBottomNavigationComponent,
   AsyncUIAvatarsComponent,
   AsyncUIButtonsComponent,
   AsyncUIBadgesComponent,
   AsyncUICardMasonaryComponent,
   AsyncUICardsComponent,
   AsyncUIChipsComponent,
   AsyncUIDialogComponent,
   AsyncUIDividersComponent,
   AsyncUIDrawersComponent,
   AsyncUIExpansionPanelComponent,
   AsyncUIGridListComponent,
   AsyncUIListComponent,
   AsyncUIMenuComponent,
   AsyncUIPopoverComponent,
   AsyncUIProgressComponent,
   AsyncUISnackbarComponent,
   AsyncUISelectionControlsComponent,
   AsyncAdvanceUIDateAndTimePickerComponent,
   AsyncAdvanceUITabsComponent,
   AsyncAdvanceUIStepperComponent,
   AsyncAdvanceUINotificationComponent,
   AsyncAdvanceUISweetAlertComponent,
   AsyncAdvanceUIAutoCompleteComponent,
   AsyncShopComponent,
   AsyncCartComponent,
   AsyncCheckoutComponent,
   AsyncEcommerceDashboardComponent,
   AsyncHealthDashboardComponent,
   AsyncSaasDashboardComponent,
   AsyncAgencyDashboardComponent,
   AsyncNewsDashboardComponent,
   
   AsyncUsersTypeComponent,
   AsyncUsersRoleComponent,
   AsyncUserManagementComponent,
   AsyncPermissionComponent,
   AsyncMasterDataComponent,
   AsyncMasterDataValueComponent,
   AsyncFeeDiscountRateComponent,
   AsyncFeeFinancialChargeComponent,
   AsyncFeeInterestRateComponent,
   AsyncFeeLateFeeWaiversComponent,
   AsyncFeeLateFeeAmountComponent,
   AsyncFeeSurchargeTypeComponent,
   AsyncSystemSettingSystemModuleComponent,
   AsyncSystemSettingSecurityQuestionsComponent,
   AsyncSystemSettingSecurityAnswersComponent,
   AsyncCountryCityComponent,
   AsyncCountryStatesComponent,
   AsyncCountryCountryComponent,
   AsyncOthersRiskFactorComponent,
   AsyncOthersScoreThresholdsComponent,
   AsyncProceduresComponent,
   AsyncProvidersAddNewComponent,
   AsyncProvidersListComponent,
   AsyncProvidersEditComponent,
   AsyncProvidersInvoiceListComponent,
   AsyncProvidersSingleInvoiceComponent,
   AsyncProvidersReportListComponent,
   AsyncPatientsAddNewComponent,
   AsyncPatientsListComponent,
   AsyncCustomerPaymentPlanComponent,
   AsyncPatientsEditComponent,
   AsyncAdminCustomerEditComponent,
   AsyncCustomerInvoiceListComponent,
   AsyncCustomerSingleInvoiceComponent,
   AsyncCustomerReportListComponent,
   AsyncCreditApplicationsComponent,
   AsyncPaymentsOpenInvoiceComponent,
   AsyncPaymentsCloseInvoiceComponent,
   AsyncPaymentsViewInvoiceComponent,
   AsyncPaymentsSingleInvoiceComponent,
   AsyncViewCustomerComponent,
   AsyncViewProviderComponent,
   AsyncPaymenPlanComponent,
   AsyncViewCreditApplicationsComponent,
   AsyncEditCreditApplicationsComponent,
   AsyncAddNewCreditApplicationComponent,
   AsyncEditCreditApplicationComponent,
   AsyncAddNewMPSCreditApplicationComponent,
   AsyncReviewCreditApplicationComponent,
   AsyncRejectCreditApplicationComponent,
   AsyncUploadCreditApplicationComponent,
   AsyncReviewDocumentCreditApplicationComponent,
   AsyncPaymentCreditApplicationComponent,
   AsyncAccountsLoanReportComponent,
   AsyncAccountsProviderInvoiceReportComponent,
   AsyncInstallmentInvoiceCreditApplicationComponent,
   AsyncInstallmentViewCreditApplicationComponent,
   AsyncAccountsWeekMonthInvoiceReportComponent,
   AsyncAccountsPrintMultipalInvoicesComponent,
   AsyncScoreRangeComponent,
   /***********************/
   /*******PROVIDERS*******/
   /***********************/
   AsyncProviderDashboardComponent,
   AsyncProviderProfileComponent,
   AsyncProviderUsersComponent,
   AsyncProviderCreditApplicationsComponent,
   AsyncProivderViewCreditApplicationsComponent,
   AsyncProviderEditCreditApplicationsComponent,
   AsyncProviderPaymenPlanComponent,
   AsyncProviderCustomerPaymentPlanComponent,
   AsyncProPatientsAddNewComponent,
   AsyncProPatientsListComponent,
   AsyncProPatientsViewComponent,
   AsyncProPatientsEditComponent,
   AsyncProCreateInvoiceListComponent,
   AsyncProPreviewInvoiceListComponent,
   AsyncProSubmittedInvoiceListComponent,
   AsyncProInvoiceComponent,
   AsyncProCreditApplicationAddNewComponent,
   AsyncProCreditApplicationEditComponent,
   AsyncProviderReviewCreditApplicationComponent,
   AsyncProviderRejectCreditApplicationComponent,
   AsyncProviderUploadCreditApplicationComponent,
   AsyncProviderReviewDocumentCreditApplicationComponent,
   AsyncProviderCustomerAgreementComponent,
   AsyncProviderCustomerAgreementViewComponent,

   /***********************/
   /*******CUSTOMER*******/
   /***********************/
   //AsyncCustomerDashboardComponent,
   AsyncCustomerProfileComponent,
   AsyncCustomerCreditApplicationComponent,
   AsyncCustomerInstallmentViewComponent,
   AsyncCustomerInstallmentReceiptComponent,
   AsyncCustomerInstallmentPaymentComponent,
   AsyncCustomerInvoiceComponent,

   // Client Chnages
   AsyncProviderSearchApplicationComponent,
   AsyncProviderSearchCustomerComponent,
   AsyncProviderPlanEstimateComponent,
   AsyncAdminApplicationPlanDetailsComponent,
   AsyncProviderApplicationPlanDetailsComponent,
   AsyncProviderApplicationAuthorizedComponent,
   AsyncProviderApplicationDeclinedComponent,
   AsyncProviderApplicationManualComponent,
   AsyncAdminApplicationAllDocumentsComponent,
   AsyncProviderApplicationAllDocumentsComponent,

   AsyncProviderEditProfileComponent,
   AsyncProviderCareCloudComponent,
   AsyncProviderInvoiceViewComponent,
   AsyncCustomerEditProfileComponent,

   AsyncInvoiceViewComponent,
   AsyncPaymentReceiptComponent,

   ///////NEW CHANGES//////
   AsyncAdminTermMonthComponent,
   AsyncAdminLoanAmountComponent,
   AsyncAdminInterestRateComponent,
   AsyncAdminApplicationAuthorizedComponent,
   AsyncProviderSecurityAnswerComponent,
   AsyncCustomerSecurityAnswerComponent,
   AsyncAdminWithdrawalComponent,

   AsyncAdminEmailTemplatesComponent,
   AsyncAdminProcedureDateComponent,

   AsyncProviderUploadPlanAgreementComponent,
   AsyncReviewPlanAgreementCreditApplicationComponent,
   AsyncProviderPlanListComponent,
   
   AsyncPaymentOptionToCloseComponent,
   AsyncPatientsInvoicePauseComponent,

   AsyncAdminUploadPlanAgreementComponent,

   AsyncAdminCustomerSupportComponent,
   AsyncAdminProviderSupportComponent,
   AsyncAdminCustomerSupportViewTicketComponent,
   AsyncCustomerSupportViewTicketComponent,
   AsyncProviderCustomerSupportViewTicketComponent,

   AsyncProRefundInvoiceListComponent,
   AsyncProRefundAdminInvoiceListComponent,

   AsyncAdminAccountReceivablesComponent,
   AsyncAdminProviderSupportReportComponent,
   AsyncAdminCustomerSupportReportComponent,

   AsyncAdminAccountPayableComponent,
   AsyncAdminCustomerDocumentsComponent,
   AsyncAdminProviderDocumentsComponent,

   AsyncAdminCreditChargesComponent,
   AsyncAdminCancelPlanDetailComponent,
   /**co-signer */
   AsyncCosignerCreditApplicationComponent,
   AsyncCosignerViewCreditApplicationsComponent,
   AsyncCosignerInstallmentPaymentComponent,
   AsyncCosignerInstallmentReceiptComponent,
   AsyncCosignerSecurityAnswerComponent,
   AsyncCosignerInvoiceComponent,
   AsyncCosignerProfileComponent,
   AsyncCosignerEditProfileComponent,
   AsyncCustomerSupportTicketComponent,
   AsyncCosignerSupportTicketComponent,
   AsyncCosignerSupportViewTicketComponent,
   AsyncAdminApplicationPlanDocumentsComponent,
   AsyncAdminApplicationDiskDocumentsComponent,
   AsyncCreditApplicationsReviewComponent,
};
