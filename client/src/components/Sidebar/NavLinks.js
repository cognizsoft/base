// sidebar nav links
export default {
   category1: [

      {
         "menu_title": "sidebar.dashboard",
         "menu_icon": "zmdi zmdi-view-dashboard",
         "path": "/admin/dashboard",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customers",
         "menu_icon": "zmdi zmdi-male-female",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "menu_title": "sidebar.week-month-reports",
               "path": "/admin/accounts/week-month-reports/all",
            },
            {
               "menu_title": "sidebar.customerMultipleInvoice",
               "path": "/admin/accounts/print-multiple-invoices",
            },
            {
               "menu_title": "sidebar.customersCustomersList",
               "path": "/admin/customers/customers-list",
            }


         ]
      },
      {
         "menu_title": "sidebar.providers",
         "menu_icon": "zmdi zmdi-store",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "menu_title": "sidebar.provider-invoice",
               "path": "/admin/accounts/open-invoice",
            },
            {
               "menu_title": "sidebar.providersProvidersList",
               "path": "/admin/providers/provider-list",
            },
            {
               "menu_title": "sidebar.refund_invoice",
               "path": "/admin/invoice/refund-invoices/all",
            },
         ]
      },
      {
         "menu_title": "sidebar.reports",
         "menu_icon": "zmdi zmdi-card",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "menu_title": "sidebar.hps-loan-reports",
               "path": "/admin/accounts/loan-reports",
            },
            {
               "menu_title": "sidebar.provider-invoices-reports",
               "path": "/admin/accounts/provider-invoice-reports/0",
            },
            {
               "menu_title": "sidebar.customer-withdrawals-reports",
               "path": "/admin/accounts/customer-withdrawal-reports",
            },
            {
               "menu_title": "sidebar.procedures-date-reports",
               "path": "/admin/accounts/procedure-date-reports",
            },
            {
               "menu_title": "sidebar.account-receivables",
               "path": "/admin/accounts/account-receivables",
            },
            {
               "menu_title": "sidebar.accout-payable",
               "path": "/admin/accounts/accout-payable",
            },
            {
               "menu_title": "sidebar.credit-charge",
               "path": "/admin/accounts/credit-charges",
            },
         ]
      },
      {
         "menu_title": "sidebar.patientsCreditApplications",
         "menu_icon": "zmdi zmdi-card",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "menu_title": "sidebar.CreditApplicationsList",
               "path": "/admin/credit-applications/all",
            },
            {
               "menu_title": "sidebar.CreditApplicationsReview",
               "path": "/admin/application/review",
            },
         ]
         
      },
      {
         "menu_title": "sidebar.systemSettings",
         "menu_icon": "zmdi zmdi-wrench",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "path": "/admin/system/system-settings/system-module",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemModule"
            },
            {
               "path": "/admin/system/master/master-data",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemMasterData"
            },
            {
               "path": "/admin/system/master/master-data-value",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemMasterDataValue"
            },
            /*{
               "path": "/admin/system/account-details",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.accountDetails"
            },*/
            /*{
               "path": "/admin/system/fee/interest-rate",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.interestRate"
            },*/
            
            
            {
               "path": "/admin/system/fee/score-range",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemScoreRange"
            },
            {
               "path": "/admin/system/fee/late-fee-waivers",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemLateFeeWaivers"
            },
            {
               "path": "/admin/system/fee/surcharge-type",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemSurchargeType"
            },
            {
               "path": "/admin/system/fee/discount-rate",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.bonusPayoff"
            },
            {
               "path": "/admin/system/others/score-thresholds",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemScoreThresholds"
            },
            
            {
               "menu_title": "sidebar.procedures",
               "path": "/admin/system/procedures",
               "menu_icon": "zmdi zmdi-store",
            },
            {
               "path": "/admin/system/email-templates",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.emailTemplates"
            },
            {
               "path": "/admin/system/country/countries",
               "menu_title": "sidebar.systemCountries"
            },
            {
               "path": "/admin/system/country/states",
               "menu_title": "sidebar.systemStates"
            },
            {
               "path": "/admin/system/country/city",
               "menu_title": "sidebar.systemCity"
            }
            /*{
               "menu_title": "sidebar.systemFee",
               "path": "#",
               "child_routes": [
                  {
                     "path": "/admin/system/fee/discount-rate",
                     "menu_title": "sidebar.systemDiscountRate"
                  }
                  
                  {
                     "path": "/admin/system/fee/financial-charges",
                     "menu_title": "sidebar.financialCharges"
                  },
                  
               ]
            }*/
         ]
      },
      /*{
         "menu_title": "sidebar.geographic",
         "menu_icon": "zmdi zmdi-globe",
         "path": "#",
         "type_multi": null,
         "child_routes": [
            {
               "path": "/admin/system/country/countries",
               "menu_title": "sidebar.systemCountries"
            },
            {
               "path": "/admin/system/country/states",
               "menu_title": "sidebar.systemStates"
            },
            {
               "path": "/admin/system/country/city",
               "menu_title": "sidebar.systemCity"
            }
            
         ]
      },*/
      {
         "menu_title": "sidebar.systemRiskFactorMain",
         "menu_icon": "zmdi zmdi-accounts",
         "path": "#",
         "type_multi": null,
         "child_routes": [
            {
               "path": "/admin/system/term-month",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.termMonth"
            },
            {
               "path": "/admin/system/loan-amount",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.loanAmount"
            },
            {
               "path": "/admin/system/interest-rate",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.interestRate"
            },
            {
               "path": "/admin/system/others/risk-factor",
               "menu_icon": "zmdi zmdi-view-dashboard",
               "menu_title": "sidebar.systemRiskFactor"
            },
         ]
      },
      {
         "menu_title": "sidebar.userManagement",
         "menu_icon": "zmdi zmdi-accounts",
         "path": "#",
         "type_multi": null,
         "child_routes": [
            /* {
                "path": "/admin/users/user-profile-1",
                "menu_title": "sidebar.userProfile1"
             },
             {
                "path": "/admin/users/user-profile",
                "menu_title": "sidebar.userProfile2"
             },*/
            {
               "path": "/admin/user-management/users",
               "menu_title": "sidebar.users"
            },
            {
               "path": "/admin/permission",
               "menu_title": "sidebar.userPermissions"
            },
            {
               "path": "/admin/user-management/users-type",
               "menu_title": "sidebar.usersType"
            },
            {
               "path": "/admin/user-management/users-role",
               "menu_title": "sidebar.userRoles"
            },
            {
               "path": "/admin/system/system-settings/security-questions",
               "menu_title": "sidebar.userSecurityQuestions"
            },
            {
               "path": "/admin/system/system-settings/security-answers",
               "menu_title": "sidebar.userSecurityAnswers"
            }

         ]
      },
      {
         "menu_title": "sidebar.customerSupportMain",
         "menu_icon": "zmdi zmdi-headset-mic",
         "type_multi": null,
         "path": "#",
         "child_routes": [
            {
               "menu_title": "sidebar.customerSupport",
               "path": "/admin/customer-support/all/4",
            },
            {
               "menu_title": "sidebar.providerSupports",
               "path": "/admin/provider-support/all/4",
            },
            {
               "menu_title": "sidebar.provider-support-reports",
               "path": "/admin/customer-support/provider-support-reports",
            },
            {
               "menu_title": "sidebar.customer-support-reports",
               "path": "/admin/customer-support/customers-support-reports",
            }
         ]
      },




   ],
   category2: [
      {
         "menu_title": "sidebar.inbox",
         "menu_icon": "zmdi zmdi-email",
         "path": "/admin/mail",
         "child_routes": null
      },
      {
         "menu_title": "sidebar.chat",
         "menu_icon": "zmdi zmdi-comments",
         "path": "/admin/chat",
         "child_routes": null
      },
      {
         "menu_title": "sidebar.toDo",
         "menu_icon": "zmdi zmdi-comment-text-alt",
         "path": "/admin/todo",
         "child_routes": null
      }
   ],
   category3: [
      {
         "menu_title": "sidebar.uiComponents",
         "menu_icon": "zmdi zmdi-wrench",
         "child_routes": [
            {
               "path": "/admin/ui-components/alerts",
               "menu_title": "sidebar.alerts"
            },
            {
               "path": "/admin/ui-components/app-bar",
               "menu_title": "sidebar.appBar"
            },
            {
               "path": "/admin/ui-components/avatars",
               "menu_title": "sidebar.avatars"
            },
            {
               "path": "/admin/ui-components/buttons",
               "menu_title": "sidebar.buttons"
            },
            {
               "path": "/admin/ui-components/bottom-navigations",
               "menu_title": "sidebar.bottomNavigations"
            },
            {
               "path": "/admin/ui-components/badges",
               "menu_title": "sidebar.badges"
            },
            {
               "path": "/admin/ui-components/cards",
               "menu_title": "sidebar.cards"
            },
            {
               "path": "/admin/ui-components/cards-masonry",
               "menu_title": "sidebar.cardsMasonry"
            },
            {
               "path": "/admin/ui-components/chip",
               "menu_title": "sidebar.chip"
            },
            {
               "path": "/admin/ui-components/dialog",
               "menu_title": "sidebar.dialog"
            },
            {
               "path": "/admin/ui-components/dividers",
               "menu_title": "sidebar.dividers"
            },
            {
               "path": "/admin/ui-components/drawers",
               "menu_title": "sidebar.drawers"
            },
            {
               "path": "/admin/ui-components/expansion-panel",
               "menu_title": "sidebar.expansionPanel"
            },
            {
               "path": "/admin/ui-components/grid-list",
               "menu_title": "sidebar.gridList"
            },
            {
               "path": "/admin/ui-components/list",
               "menu_title": "sidebar.list"
            },
            {
               "path": "/admin/ui-components/menu",
               "menu_title": "sidebar.menu"
            },
            {
               "path": "/admin/ui-components/popover",
               "menu_title": "sidebar.popoverAndToolTip"
            },
            {
               "path": "/admin/ui-components/progress",
               "menu_title": "sidebar.progress"
            },
            {
               "path": "/admin/ui-components/snackbar",
               "menu_title": "sidebar.snackbar"
            },
            {
               "path": "/admin/ui-components/selection-controls",
               "menu_title": "sidebar.selectionControls"
            }
         ]
      },
      {
         "menu_title": "sidebar.advancedComponent",
         "menu_icon": "zmdi zmdi-view-carousel",
         "child_routes": [
            {
               "path": "/admin/advanced-component/dateTime-picker",
               "menu_title": "sidebar.dateAndTimePicker"
            },
            {
               "path": "/admin/advanced-component/tabs",
               "menu_title": "sidebar.tabs"
            },
            {
               "path": "/admin/advanced-component/stepper",
               "menu_title": "sidebar.stepper"
            },
            {
               "path": "/admin/advanced-component/notification",
               "menu_title": "sidebar.notification"
            },
            {
               "path": "/admin/advanced-component/sweet-alert",
               "menu_title": "sidebar.sweetAlert"
            },
            {
               "path": "/admin/advanced-component/auto-complete",
               "menu_title": "sidebar.autoComplete"
            }
         ]
      },
      {
         "menu_title": "sidebar.aboutUs",
         "menu_icon": "zmdi zmdi-info",
         "path": "/admin/about-us",
         "child_routes": null
      }
   ],
   category4: [
      {
         "menu_title": "sidebar.forms",
         "menu_icon": "zmdi zmdi-file-text",
         "child_routes": [
            {
               "path": "/admin/forms/form-elements",
               "menu_title": "sidebar.formElements"
            },
            {
               "path": "/admin/forms/text-field",
               "menu_title": "sidebar.textField"
            },
            {
               "path": "/admin/forms/select-list",
               "menu_title": "sidebar.selectList"
            }
         ]
      },
      {
         "menu_title": "sidebar.charts",
         "menu_icon": "zmdi zmdi-chart-donut",
         "child_routes": [
            {
               "path": "/admin/charts/re-charts",
               "menu_title": "sidebar.reCharts"
            },
            {
               "path": "/admin/charts/react-chartjs2",
               "menu_title": "sidebar.reactChartjs2"
            }
         ]
      },
      {
         "menu_title": "sidebar.icons",
         "menu_icon": "zmdi zmdi-star",
         "child_routes": [
            {
               "path": "/admin/icons/themify-icons",
               "menu_title": "sidebar.themifyIcons"
            },
            {
               "path": "/admin/icons/simple-lineIcons",
               "menu_title": "sidebar.simpleLineIcons"
            },
            {
               "path": "/admin/icons/material-icons",
               "menu_title": "sidebar.materialIcons"
            }
         ]
      },
      {
         "menu_title": "sidebar.tables",
         "menu_icon": "zmdi zmdi-grid",
         "child_routes": [
            {
               "path": "/admin/tables/basic",
               "menu_title": "sidebar.basic"
            },
            {
               "path": "/admin/tables/data-table",
               "menu_title": "sidebar.dataTable"
            },
            {
               "path": "/admin/tables/responsive",
               "menu_title": "sidebar.responsive"
            }
         ]
      }
   ],
   category5: [
      {
         "menu_title": "sidebar.users",
         "menu_icon": "zmdi zmdi-accounts",
         "child_routes": [
            {
               "path": "/admin/users/user-profile-1",
               "menu_title": "sidebar.userProfile1"
            },
            {
               "path": "/admin/users/user-profile",
               "menu_title": "sidebar.userProfile2"
            },
            {
               "path": "/admin/users/user-management",
               "menu_title": "sidebar.userManagement"
            }
         ]
      },

      {
         "menu_title": "sidebar.maps",
         "menu_icon": "zmdi zmdi-map",
         "child_routes": [
            {
               "path": "/admin/maps/google-maps",
               "menu_title": "sidebar.googleMaps"
            },
            {
               "path": "/admin/maps/leaflet-maps",
               "menu_title": "sidebar.leafletMaps"
            }
         ]
      },

      {
         "menu_title": "sidebar.calendar",
         "menu_icon": "zmdi zmdi-calendar-note",
         "child_routes": [
            {
               "path": "/admin/calendar/basic",
               "menu_title": "components.basic"
            },
            {
               "path": "/admin/calendar/cultures",
               "menu_title": "sidebar.cultures"
            },
            {
               "path": "/admin/calendar/selectable",
               "menu_title": "sidebar.selectable"
            },
            {
               "path": "/admin/calendar/custom-rendering",
               "menu_title": "sidebar.customRendering"
            }
         ]
      },
      {
         "menu_title": "sidebar.editor",
         "menu_icon": "zmdi zmdi-edit",
         "child_routes": [
            {
               "path": "/admin/editor/wysiwyg-editor",
               "menu_title": "sidebar.wysiwygEditor"
            },
            {
               "path": "/admin/editor/quill-editor",
               "menu_title": "sidebar.quillEditor"
            }
         ]
      },
      {
         "menu_title": "sidebar.dragAndDrop",
         "menu_icon": "zmdi zmdi-mouse",
         "child_routes": [
            {
               "path": "/admin/drag-andDrop/react-dragula",
               "menu_title": "sidebar.reactDragula"
            },
            {
               "path": "/admin/drag-andDrop/react-dnd",
               "menu_title": "sidebar.reactDnd"
            }
         ]
      },
      {
         "menu_title": "sidebar.multiLevel",
         "menu_icon": "zmdi zmdi-view-web",
         "child_routes": [
            {
               "menu_title": "sidebar.level1",
               "child_routes": [
                  {
                     "path": "/admin/level2",
                     "menu_title": "sidebar.level2"
                  }
               ]
            }
         ]
      },
   ],
   category6: [
      {
         "menu_title": "sidebar.imageCropper",
         "menu_icon": "zmdi zmdi-crop",
         "path": "/admin/image-cropper",
         "child_routes": null
      },
      {
         "menu_title": "sidebar.videoPlayer",
         "menu_icon": "zmdi zmdi-collection-video",
         "path": "/admin/video-player",
         "child_routes": null
      },
      {
         "menu_title": "sidebar.dropzone",
         "menu_icon": "zmdi zmdi-dropbox",
         "path": "/admin/dropzone",
         "child_routes": null
      }
   ]
}
