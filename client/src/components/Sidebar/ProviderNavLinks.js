// sidebar nav links
export default {
   category1: [

      {
         "menu_title": "sidebar.dashboard",
         "menu_icon": "zmdi zmdi-view-dashboard",
         "path": "/provider/dashboard",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customer-profile",
         "menu_icon": "zmdi zmdi-account-circle",
         "type_multi": null,
         "child_routes": [
            {
               "menu_title": "sidebar.customer-profile",
               "path": "/provider/profile",

            },
            {
               "menu_title": "provider.careCloudPMS",
               "path": "/provider/care-cloud",

            },
            {
               "menu_title": "provider.agreement",
               "path": "/provider/agreement",
            },
            {
               "menu_title": "provider.security-answers",
               "path": "/provider/security-answers",
            },
         ]
      },
      {
         "menu_title": "provider.users",
         "menu_icon": "zmdi zmdi-accounts",
         "path": "/provider/users",
         "type_multi": null,
         "child_routes": null
      },
      /*{
         "menu_title": "provider.careCloud",
         "menu_icon": "zmdi zmdi-accounts",
         "path": "/provider/care-cloud",
         "type_multi": null,
         "child_routes": null
      },*/
      {
         "menu_title": "provider.creditApplication",
         "menu_icon": "zmdi zmdi-card",
         "path": "/provider/credit-applications/all",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "provider.searchApplication",
         "menu_icon": "zmdi zmdi-card",
         "path": "/provider/search-application",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "provider.customerProfile",
         "menu_icon": "zmdi zmdi-male-female",
         "type_multi": null,
         "child_routes": [
            {
               "menu_title": "provider.customerProfile",
               "path": "/provider/customers/customers-list",

            },
            {
               "menu_title": "provider.providerSurgeryStatusUpdate",
               "path": "/provider/credit-application/surgery-status-update",

            },
         ]
      },
      {
         "menu_title": "provider.invoice",
         "menu_icon": "zmdi zmdi-file-text",
         "type_multi": null,
         "child_routes": [
            {
               "menu_title": "sidebar.create_invoice",
               "path": "/provider/invoice/create-invoice",

            },
            {
               "menu_title": "sidebar.submit_invoice",
               "path": "/provider/invoice/submitted-invoices/5",

            },
            {
               "menu_title": "sidebar.refund_invoice",
               "path": "/provider/invoice/refund-invoices/all",

            },
         ]
      },
      {
         "menu_title": "provider.planEstimate",
         "menu_icon": "zmdi zmdi-card",
         "path": "/provider/plan-estimate",
         "type_multi": null,
         "child_routes": null
      }

   ],
   category2: [

   ],
   category3: [

   ],
   category4: [

   ],
   category5: [

   ],
   category6: [

   ]
}
