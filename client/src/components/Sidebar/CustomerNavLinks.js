// sidebar nav links
export default {
   category1: [

      {
         "menu_title": "sidebar.dashboard",
         "menu_icon": "zmdi zmdi-view-dashboard",
         "path": "/customer/dashboard",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customer-profile",
         "menu_icon": "zmdi zmdi-account-circle",
         "path": "/customer/profile",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.invoice",
         "menu_icon": "zmdi zmdi-file-text",
         "path": "/customer/invoice/week-month-reports/0",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customerSecurityAnswer",
         "menu_icon": "zmdi zmdi-shield-check",
         "path": "/customer/security-answers",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customerSupportTicket",
         "menu_icon": "zmdi zmdi-headset-mic",
         "path": "/customer/support-ticket",
         "type_multi": null,
         "child_routes": null
      },
     


   ],
   category2: [

   ],
   category3: [

   ],
   category4: [

   ],
   category5: [

   ],
   category6: [

   ]
}
