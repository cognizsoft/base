// sidebar nav links
export default {
   category1: [

      {
         "menu_title": "sidebar.dashboard",
         "menu_icon": "zmdi zmdi-view-dashboard",
         "path": "/cosigner/dashboard",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customer-profile",
         "menu_icon": "zmdi zmdi-account-circle",
         "path": "/cosigner/profile",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.invoice",
         "menu_icon": "zmdi zmdi-file-text",
         "path": "/cosigner/invoice/week-month-reports/0",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customerSecurityAnswer",
         "menu_icon": "zmdi zmdi-shield-check",
         "path": "/cosigner/security-answers",
         "type_multi": null,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.customerSupportTicket",
         "menu_icon": "zmdi zmdi-headset-mic",
         "path": "/cosigner/support-ticket",
         "type_multi": null,
         "child_routes": null
      },


   ],
   category2: [

   ],
   category3: [

   ],
   category4: [

   ],
   category5: [

   ],
   category6: [

   ]
}
