/**
 * Delete Confirmation Dialog
 */
import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';

class DeleteTypeConfirmationDialog extends Component {

   state = {
      open: false,
      validData:false,
   }

   // open dialog
   open() {
      this.setState({ open: true });
   }

   // close dialog
   close() {
      this.setState({ open: false });
   }

   typeConfirm() {
      return false;
   }

   onChangeCheck(key, value) {
      switch (key) {
         case 'confirm_type':
            if(value.toLowerCase() == this.props.typeKey){
               this.setState({validData:true})
            }else{
               this.setState({validData:false})
            }
            break;
         default:
            break;
      }
   }

   render() {
      const { title, message, onConfirm } = this.props;
      return (
         <Dialog
            open={this.state.open}
            onClose={() => this.close()}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
         >
            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>
               <div className="modal-body page-form-outer text-left">
                  <Label for="confirm_type">{message}</Label><br />
                  <FormGroup>
                     <TextField
                        type="text"
                        name="confirm_type"
                        id="confirm_type"
                        fullWidth
                        variant="outlined"
                        placeholder={message}
                        onChange={(e) => this.onChangeCheck('confirm_type', e.target.value)}
                     />
                  </FormGroup>
               </div>
            </DialogContent>
            <DialogActions>
               <Button onClick={() => this.close()} className="btn-danger text-white">
                  Cancel
               </Button>
               
               <Button disabled={!this.state.validData} onClick={onConfirm} variant="contained" color="primary" className="text-white" >
                  ok
               </Button>
            </DialogActions>
         </Dialog>
      );
   }
}

export default DeleteTypeConfirmationDialog;
