/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';

const UpdateAmountForm = ({ verifyAmtErr, onVerifyAmountDetail, verifyAmt }) => (
    <Form>
    <div className="row">
        
        
        <div className="col-md-12">
            <FormGroup>
               <Label for="verify_amount">Amount<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="verify_amount"
                  id="verify_amount"
                  fullWidth
                  variant="outlined"
                  value={(verifyAmt.verify_amount != '') ? verifyAmt.verify_amount : ''}
                  placeholder="Enter the amount"
                  error={(verifyAmtErr.verify_amount) ? true : false}
                  helperText={verifyAmtErr.verify_amount}
                  onChange={(e) => onVerifyAmountDetail('verify_amount', e.target.value)}
               />
            </FormGroup>
        </div>

    </div>
    </Form>
);

export default UpdateAmountForm;
