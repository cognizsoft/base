/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import CustomToolbar from "./CustomToolbar";
import moment from 'moment';

import { isEmpty } from '../../validator/Validator';
import {
  FormGroup,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';
import FormHelperText from '@material-ui/core/FormHelperText';

import {
  customerInvoicepPause, listInvoicepPause, customerInvoicepPauseUpdate,
} from 'Actions';
class OptionToClose extends Component {

  state = {
    data: {
      fin_charge_waived: "1",
      late_fee_waived: "1",
      interest_waived: "0",
      status: "1",
      due_to: '',
      start_date: '',
      end_date: '',
    },
    dataError: {},
    start_date: '',
    end_date: '',
    start_date_update: '',
    end_date_update: '',
    opnAddNewModal: false,
    editRecord: null,
    updateError: {
      due_to: '',
      start_date: '',
      end_date: '',
    }
  }

  dec(cipherText) {
    var SECRET = 'rmaeshCSS';
    var reb64 = CryptoJS.enc.Hex.parse(cipherText);
    var bytes = reb64.toString(CryptoJS.enc.Base64);
    var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
    var plain = decrypt.toString(CryptoJS.enc.Utf8);
    return plain;
  }

  componentDidMount() {
    this.props.listInvoicepPause(this.dec(this.props.match.params.patient_id));
  }
  onChnagePlanAction(key, value) {
    let { dataError } = this.state;
    switch (key) {
      case 'start_date':
        var existVal = this.props.listInvoicePause.filter((s, sidx) => value >= new Date(s.start_date) && value <= new Date(s.end_date));

        if (existVal.length > 0) {
          dataError[key] = "Date range already exists";
          this.setState({ start_date: '' })
        } else if (value == null) {
          dataError[key] = "Select Start Date";
          this.setState({ start_date: '' })
        } else {
          this.setState({ start_date: value })
          value = moment(value).format('YYYY-MM-DD');
          dataError[key] = '';
        }
        break;
      case 'end_date':
        var existVal = this.props.listInvoicePause.filter((s, sidx) => value >= new Date(s.start_date) && value <= new Date(s.end_date));

        if (existVal.length > 0) {
          dataError[key] = "Date range already exists";
          this.setState({ start_date: '' })
        } else if (value == null) {
          dataError[key] = "Select End Date";
          this.setState({ end_date: '' })
        } else {
          this.setState({ end_date: value })
          value = moment(value).format('YYYY-MM-DD');
          dataError[key] = '';
        }
        break;
      case 'due_to':
        if (isEmpty(value)) {
          dataError[key] = "Due To can't be blank";
        } else {
          dataError[key] = '';
        }
        break;
    }
    this.setState({
      data: {
        ...this.state.data,
        [key]: value
      }
    });
    this.setState({ dataError: dataError });
  }
  onChnagePlanEditAction(key, value) {
    let { updateError } = this.state;
    switch (key) {
      case 'start_date':
        var existVal = this.props.listInvoicePause.filter((s, sidx) => this.state.editRecord.id != s.id && value >= new Date(s.start_date) && value <= new Date(s.end_date));

        if (existVal.length > 0) {
          updateError[key] = "Date range already exists";
          this.setState({ start_date_update: '' })
        } else if (value == null) {
          updateError[key] = "Select Start Date";
          this.setState({ start_date_update: '' })
        } else {
          this.setState({ start_date_update: value })
          value = moment(value).format('YYYY-MM-DD');
          updateError[key] = '';
        }
        break;
      case 'end_date':
        var existVal = this.props.listInvoicePause.filter((s, sidx) => this.state.editRecord.id != s.id && value >= new Date(s.start_date) && value <= new Date(s.end_date));

        if (existVal.length > 0) {
          updateError[key] = "Date range already exists";
          this.setState({ end_date_update: '' })
        } else if (value == null) {
          updateError[key] = "Select End Date";
          this.setState({ end_date_update: '' })
        } else {
          this.setState({ end_date_update: value })
          value = moment(value).format('YYYY-MM-DD');
          updateError[key] = '';
        }
        break;
      case 'due_to':
        if (isEmpty(value)) {
          updateError[key] = "Due To can't be blank";
        } else {
          updateError[key] = '';
        }
        break;
    }
    this.setState({
      editRecord: {
        ...this.state.editRecord,
        [key]: value
      }
    });
    this.setState({ updateError: updateError });
  }
  processPauseConfirm() {
    this.refs.deleteConfirmationDialog.open();
  }
  processPauseAction() {
    const { data } = this.state;
    data.patient_id = this.dec(this.props.match.params.patient_id);

    if (this.state.editRecord === null) {
      this.props.customerInvoicepPause(data);
    } else {
      this.props.customerInvoicepPauseUpdate(this.state.editRecord);
    }

    this.refs.deleteConfirmationDialog.close();
    this.setState({
      opnAddNewModal: false,
      editRecord: null,
      updateError: {
        due_to: '',
        start_date: '',
        end_date: '',
      }
    });
  }
  validateAction() {
    //console.log(this.state.loanError)
    return (
      this.state.dataError.due_to === '' &&
      this.state.dataError.start_date === '' &&
      this.state.dataError.end_date === ''
    )
  }
  validateUpdateAction() {
    //console.log(this.state.loanError)
    return (
      this.state.updateError.due_to === '' &&
      this.state.updateError.start_date === '' &&
      this.state.updateError.end_date === ''
    )
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  /**
   * Open Add New Country Modal
   */
  opnAddNewModal() {
    this.setState({ opnAddNewModal: true });
  }
  opnAddNewModalClose() {

    this.setState({
      opnAddNewModal: false,
      data: {
        fin_charge_waived: "1",
        late_fee_waived: "1",
        interest_waived: "0",
        status: '1',
        due_to: '',
        start_date: '',
        end_date: '',
      },
      dataError: {},
      start_date: '',
      end_date: '',
      editRecord: null,
    })
  }
  /**
   * On Edit Country
   */
  onEditRecord(details) {
    var start_date_update = new Date(details.start_date);
    var end_date_update = new Date(details.end_date);
    details.start_date = moment(start_date_update).format('YYYY-MM-DD');
    details.end_date = moment(end_date_update).format('YYYY-MM-DD');
    this.setState({ opnAddNewModal: true, editRecord: details, start_date_update: start_date_update, end_date_update: end_date_update });
  }
  getMuiTheme = () => createMuiTheme({
    overrides: {

      MuiSvgIcon: {
        root: {
          'color': "#0E5D97"
        }
      },
      MuiFormControlLabel: {
        root: {
          width: "auto !important"
        }
      },
      MuiFormLabel: {
        root: {
          margin: "0px",
          'font-size': '14px',
          color: '#000',
          'font-weight': "600",

        }
      }

    }
  })
  goBack() {
    this.props.history.goBack(-1)
  }
  render() {

    const columns = [

      {
        name: 'ID',
        field: 'id'
      },
      {
        name: 'Start Date',
        field: 'start_date',
      },
      {
        name: 'End Date',
        field: 'end_date',
      },
      {
        name: "Finance Charges Waived",
        field: "fin_charge_waived",
        options: {
          filter: true,
          sort: true,
          empty: true,
          customBodyRender: (value) => {
            return (
              value.fin_charge_waived == 1 ? 'Yes' : 'No'
            );
          }
        }
      },
      {
        name: "Late Fee Waived",
        field: "late_fee_waived",
        options: {
          filter: true,
          sort: true,
          empty: true,
          customBodyRender: (value) => {
            return (
              value.late_fee_waived == 1 ? 'Yes' : 'No'
            );
          }
        }
      },
      {
        name: "Interest Waived",
        field: "interest_waived",
        options: {
          filter: true,
          sort: true,
          empty: true,
          customBodyRender: (value) => {
            return (
              value.interest_waived == 1 ? 'Yes' : 'No'
            );
          }
        }
      },
      {
        name: "Status",
        field: "status",
        options: {
          filter: true,
          sort: true,
          empty: true,
          customBodyRender: (value) => {
            return (
              value.status == 1 ? 'Active' : 'Inactive'
            );
          }
        }
      },
      {
        name: "Action",
        options: {
          filter: false,
          sort: false,
          empty: true,
          download: false,
          customBodyRender: (value) => {
            return (
              <div className="list-action">
                <a href="javascript:void(0)" onClick={() => this.onEditRecord(value)}><i className="ti-pencil"></i></a>
              </div>
            );
          }
        }
      }

    ];
    const options = {
      filter: true,
      filterType: 'dropdown',
      selectableRows: false,
      download: false,
      rowsPerPageOptions: [10, 20, 50, 100],

      pagination: true,
      downloadOptions: { filename: 'Country.csv' },
      customToolbar: () => {
        return (
          <CustomToolbar opnAddNewModal={this.opnAddNewModal.bind(this)} back={this.goBack.bind(this)} />
        );
      },
    };
    return (
      <div className="invoice-wrapper">
        <PageTitleBar title={<IntlMessages id="sidebar.invoice-pause" />} match={this.props.match} />
        <RctCollapsibleCard heading="" fullBlock>
          <MaterialDatatable
            data={this.props.listInvoicePause}
            columns={columns}
            options={options}
          />
        </RctCollapsibleCard>
        <Modal isOpen={this.state.opnAddNewModal} toggle={() => this.opnAddNewModalClose()}>
          <ModalHeader toggle={() => this.opnAddNewModalClose()}>
            {this.state.editRecord === null ?
              'Add a Pause on Inoices' : 'Update a Pause on Inoices'
            }
          </ModalHeader>
          <ModalBody>
            {this.state.editRecord === null ?
              <RctCard>
                <div className="row p-10">
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Finance Charges Waived</FormLabel>
                        <RadioGroup row aria-label="fin_charge_waived" name="fin_charge_waived" value={this.state.data.fin_charge_waived} onChange={(e) => this.onChnagePlanAction('fin_charge_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" />
                          <FormControlLabel value="0" control={<Radio />} label="No" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Late Fee Waived</FormLabel>
                        <RadioGroup row aria-label="late_fee_waived" name="late_fee_waived" value={this.state.data.late_fee_waived} onChange={(e) => this.onChnagePlanAction('late_fee_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" />
                          <FormControlLabel value="0" control={<Radio />} label="No" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Interest Waived</FormLabel>
                        <RadioGroup row aria-label="interest_waived" name="interest_waived" value={this.state.data.interest_waived} onChange={(e) => this.onChnagePlanAction('interest_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" disabled />
                          <FormControlLabel value="0" control={<Radio />} label="No" disabled />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Status</FormLabel>
                        <RadioGroup row aria-label="status" name="status" value={this.state.data.status} onChange={(e) => this.onChnagePlanAction('status', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Active" />
                          <FormControlLabel value="0" control={<Radio />} label="Inactive" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <FormGroup>
                      <Label for="start_date">Start Date<span className="required-field">*</span></Label>
                      <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="start_date"
                        id="start_date"
                        minDate={new Date()}
                        selected={this.state.start_date}
                        maxDate={this.state.end_date}
                        placeholderText="MM/DD/YYYY"
                        autocomplete={false}
                        onChange={(e) => this.onChnagePlanAction('start_date', e)}
                      />
                      {(this.state.dataError.start_date) ? <FormHelperText>{this.state.dataError.start_date}</FormHelperText> : ''}
                    </FormGroup>
                  </div>
                  <div className="col-md-6">
                    <FormGroup>
                      <Label for="end_date">End Date<span className="required-field">*</span></Label>
                      <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="end_date"
                        id="end_date"
                        minDate={(this.state.start_date != '') ? this.state.start_date : new Date()}
                        selected={this.state.end_date}
                        placeholderText="MM/DD/YYYY"
                        autocomplete={false}
                        onChange={(e) => this.onChnagePlanAction('end_date', e)}
                      />
                      {(this.state.dataError.end_date) ? <FormHelperText>{this.state.dataError.end_date}</FormHelperText> : ''}
                    </FormGroup>
                  </div>
                  <div className="col-md-12">
                    <FormGroup>
                      <Label for="due_to">Pause Reason<span className="required-field">*</span></Label><br />
                      <Input
                        type="textarea"
                        name="due_to"
                        id="due_to"
                        variant="outlined"
                        value={this.state.data.due_to}
                        placeholder="Due To"
                        onChange={(e) => this.onChnagePlanAction('due_to', e.target.value)}
                      >
                      </Input>
                      {(this.state.dataError.due_to) ? <FormHelperText>{this.state.dataError.due_to}</FormHelperText> : ''}
                    </FormGroup>
                  </div>

                </div>
              </RctCard>
              :
              <RctCard>
                <div className="row p-10">
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Finance Charges Waived</FormLabel>
                        <RadioGroup row aria-label="fin_charge_waived" name="fin_charge_waived" value={this.state.editRecord.fin_charge_waived.toString()} onChange={(e) => this.onChnagePlanEditAction('fin_charge_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" />
                          <FormControlLabel value="0" control={<Radio />} label="No" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Late Fee Waived</FormLabel>
                        <RadioGroup row aria-label="late_fee_waived" name="late_fee_waived" value={this.state.editRecord.late_fee_waived.toString()} onChange={(e) => this.onChnagePlanEditAction('late_fee_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" />
                          <FormControlLabel value="0" control={<Radio />} label="No" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Interest Waived</FormLabel>
                        <RadioGroup row aria-label="interest_waived" name="interest_waived" value={this.state.editRecord.interest_waived.toString()} onChange={(e) => this.onChnagePlanEditAction('interest_waived', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Yes" disabled />
                          <FormControlLabel value="0" control={<Radio />} label="No" disabled />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Status</FormLabel>
                        <RadioGroup row aria-label="status" name="status" value={this.state.editRecord.status.toString()} onChange={(e) => this.onChnagePlanEditAction('status', e.target.value)} >
                          <FormControlLabel value="1" control={<Radio />} label="Active" />
                          <FormControlLabel value="0" control={<Radio />} label="Inactive" />
                        </RadioGroup>
                      </FormControl>
                    </MuiThemeProvider>
                  </div>
                  <div className="col-md-6">
                    <FormGroup>
                      <Label for="start_date">Start Date<span className="required-field">*</span></Label>
                      <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="start_date"
                        id="start_date"
                        minDate={this.state.start_date_update}
                        selected={this.state.start_date_update}
                        maxDate={this.state.end_date_update}
                        placeholderText="MM/DD/YYYY"
                        autocomplete={false}
                        onChange={(e) => this.onChnagePlanEditAction('start_date', e)}
                      />
                      {(this.state.updateError.start_date) ? <FormHelperText>{this.state.updateError.start_date}</FormHelperText> : ''}
                    </FormGroup>
                  </div>
                  <div className="col-md-6">
                    <FormGroup>
                      <Label for="end_date">End Date<span className="required-field">*</span></Label>
                      <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="end_date"
                        id="end_date"
                        minDate={(this.state.start_date_update != '') ? this.state.start_date_update : new Date()}
                        selected={this.state.end_date_update}
                        placeholderText="MM/DD/YYYY"
                        autocomplete={false}
                        onChange={(e) => this.onChnagePlanEditAction('end_date', e)}
                      />
                      {(this.state.updateError.end_date) ? <FormHelperText>{this.state.updateError.end_date}</FormHelperText> : ''}
                    </FormGroup>
                  </div>
                  <div className="col-md-12">
                    <FormGroup>
                      <Label for="due_to">Pause Reason<span className="required-field">*</span></Label><br />
                      <Input
                        type="textarea"
                        name="due_to"
                        id="due_to"
                        variant="outlined"
                        value={this.state.editRecord.due_to}
                        placeholder="Due To"
                        onChange={(e) => this.onChnagePlanEditAction('due_to', e.target.value)}
                      >
                      </Input>
                      {(this.state.updateError.due_to) ? <FormHelperText>{this.state.updateError.due_to}</FormHelperText> : ''}
                    </FormGroup>
                  </div>

                </div>
              </RctCard>
            }
          </ModalBody>
          <ModalFooter>
            {this.state.editRecord === null ?
              <Button disabled={!this.validateAction()} onClick={() => this.processPauseConfirm()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                Add
              </Button>
              :
              <Button disabled={!this.validateUpdateAction()} onClick={() => this.processPauseConfirm()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                Update
              </Button>
            }
            <Button onClick={() => this.opnAddNewModalClose()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        <div className="row">

          <DeleteConfirmationDialog
            ref="deleteConfirmationDialog"
            title="Are You Sure?"
            message="Transaction cannot be rolled back."
            onConfirm={() => this.processPauseAction()}
          />
        </div>
        {this.props.loading &&
          <RctSectionLoader />
        }
      </div>
    );
  }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer }) => {

  const { loading, listInvoicePause, planCloseOption, planCloseStatus } = PaymentPlanReducer;
  return { loading, listInvoicePause, planCloseOption, planCloseStatus }
}

export default connect(mapStateToProps, {
  customerInvoicepPause, listInvoicepPause, customerInvoicepPauseUpdate,
})(OptionToClose);