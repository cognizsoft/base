/**
 * Pricing
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Switch from 'react-toggle-switch';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
// components
import PricingBlockV1 from 'Components/Pricing/PricingBlockV1';
import PricingBlockV2 from 'Components/Pricing/PricingBlockV2';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
   paymentPlan, createPaymentPlan
} from 'Actions';
class CustomerPaymentPlan extends Component {

   state = {
      monthlyPlan: true,
      businessPlan: 30,
      enterprisePlan: 59,
      changeURL: 0,
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      this.props.paymentPlan(this.props.match.params.id);
   }
   /*
   * Title :- processPlan
   * Descrpation :- This function use for process selected plan
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   processPlan(application_id, plan_id) {
      this.props.createPaymentPlan(application_id, plan_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check state updated or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 5,2019
   */
   componentWillReceiveProps(nextProps) {
      (nextProps.rediretPlanURL != '') ? this.setState({ changeURL: nextProps.rediretPlanURL }) : '';
   }
   // on plan change
   onPlanChange(isMonthly) {
      this.setState({ monthlyPlan: !isMonthly });
      if (!isMonthly) {
         this.setState({ businessPlan: 30, enterprisePlan: 59 });
      } else {
         this.setState({ businessPlan: 35, enterprisePlan: 70 });
      }
   }


   render() {
      if(this.state.changeURL === 1){
         return (<Redirect to={`/admin/credit-applications`} />);
      }
      const paymentPlanAllow = this.props.paymentPlanAllow;
      return (
         <div className="pricing-wrapper">
            <Helmet>
               <title>Health Partner | Customers | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsPaymentPlans" />}
               match={this.props.match}
            />

            <div className="table-responsive mb-20 pymt-history select-customer-plan">
               <table className="table">
                  <thead>
                     <tr>
                        <th>Date : {paymentPlanAllow && this.props.applicationDetails[0].date_created}</th>
                        <th>Credit Score : {paymentPlanAllow && this.props.applicationDetails[0].score}</th>
                     </tr>
                  </thead>
               </table>
            </div>
            
            <div className="price-list">
               <div className="row row-eq-height">
                  {paymentPlanAllow && paymentPlanAllow.map((plan, idx) => (
                     <div className="col-md-4" key={idx} >
                        <div className="price-plan plan1 rct-block text-center">
                           <div className="header">
                              <h2 className="pricing-title">
                                 <span className="price">{(this.props.applicationDetails[0].member_flag == 1)?0.00:plan.interest_rate}</span><span>%</span></h2>
                              <div className="description-text">Principal Amount: <span className="t-price">${this.props.applicationDetails[0].amount}</span></div>
                              <button className="btn-block btn-medium btn btn-primary top-select-btn" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>
                           </div>
                           <div className="content ">
                              <div className="page-form-outer select-plan-detail">
                                 <div className="row">

                                    <div className="col-md-10">Payment Plans</div>
                                    <div className="col-md-2">{idx + 1}</div>

                                    <div className="col-md-10">Payment Term (months)</div>
                                    <div className="col-md-2">{plan.term_month}</div>
                                 </div>

                                 <div className="price-list table-responsive">
                                    <table className="table table-bordered table-striped table-hover">
                                       <thead>
                                          <tr className="table-head">
                                             <th>SN</th>
                                             <th>Date</th>
                                             <th>Amount</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          {
                                             plan.monthlyPlan.map((month, idm) => (

                                                <tr key={idm}>
                                                   <td>{idm + 1}</td>
                                                   <td>{month.next_month}</td>
                                                   <td>${month.perMonth}</td>
                                                </tr>
                                             ))
                                          }



                                       </tbody>
                                    </table>

                                 </div>
                                 <button className="btn-block btn-lg btn btn-primary" onClick={() => this.processPlan(this.props.applicationDetails[0].application_id, plan.plan_id)}><span>Select Plan</span></button>

                              </div>


                           </div>
                        </div>
                     </div>
                  ))}


               </div>
            </div>
            {this.props.loading &&
               <RctSectionLoader />
            }
         </div>
      );
   }
}
const mapStateToProps = ({ authUser, creditApplication }) => {
   const { nameExist, isEdit } = authUser;
   const { loading, paymentPlanAllow, applicationDetails, rediretPlanURL } = creditApplication;
   return { loading, paymentPlanAllow, applicationDetails, rediretPlanURL }
}

export default connect(mapStateToProps, {
   paymentPlan, createPaymentPlan
})(CustomerPaymentPlan);
