/*===== Horizonatl Non Linear Stepper =====*/
import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
function getSteps() {
   return ['Customer Information', 'Locations', 'Employment Information', 'Bank Details'];
}

function getStepContent(step) {
   switch (step) {
      case 0:
         return (
        		<div className="table-responsive">
        			<div className="modal-body page-form-outer text-left">
                    <Form>
                       <div className="row">
                          <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="first-name">First Name</Label><br/>
                                         <TextField
                                         type="text"
                                         name="first-name"
                                         id="first-name"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="First Name"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="middle-name">Middle Name</Label><br/>
                                         <TextField
                                         type="text"
                                         name="middle-name"
                                         id="middle-name"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Middle Name"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="last-name">Last Name</Label><br/>
                                         <TextField
                                         type="text"
                                         name="last-name"
                                         id="last-name"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Last Name"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-12">
                                     <FormGroup tag="fieldset">
                                      <FormGroup check>
                                         <Label check>
                                            <Input type="radio" name="alias-name"  />{' '}
                                            Alias Name
                                             </Label>
                                      </FormGroup>
                                   </FormGroup>    
                                 </div>

                                 <div className="col-md-4">
                					        <FormGroup>
                					            <Label for="effective_date">Date of Birth</Label>
                					            <Input type="date" name="dob" id="dob" placeholder="date placeholder" />
                					        </FormGroup>
                					        </div>

        					       <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="phone-1">Phone No. 1</Label><br/>
                                         <TextField
                                         type="text"
                                         name="phone-1"
                                         id="phone-1"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Phone No."
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="phone-2">Phone No. 2</Label><br/>
                                         <TextField
                                         type="text"
                                         name="phone-2"
                                         id="phone-2"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Phone No."
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="email">Email Address</Label><br/>
                                         <TextField
                                         type="text"
                                         name="email"
                                         id="email"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Email Address"
                                         />
                                   </FormGroup>
                                </div>


                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="ssn">Social Security Number</Label><br/>
                                         <TextField
                                         type="text"
                                         name="ssn"
                                         id="ssn"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Social Security Number"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                     <FormGroup tag="fieldset">
                                      <Label>Gender</Label>
                                      <FormGroup check>
                                         <Label check>
                                            <Input type="radio" name="gender" checked />{' '}
                                            Male
                                             </Label>
                                      </FormGroup>
                                      <FormGroup check>
                                         <Label check>
                                            <Input type="radio" name="gender" />{' '}
                                            Female
                                             </Label>
                                      </FormGroup>
                                   </FormGroup>    
                                 </div>

                                 <div className="col-md-4">
                                     <FormGroup tag="fieldset">
                                      <Label>Status</Label>
                                      <FormGroup check>
                                         <Label check>
                                            <Input type="radio" name="radio1" checked />{' '}
                                            Active
                                             </Label>
                                      </FormGroup>
                                      <FormGroup check>
                                         <Label check>
                                            <Input type="radio" name="radio1" />{' '}
                                            Inactive
                                             </Label>
                                      </FormGroup>
                                   </FormGroup>    
                                 </div>





                       </div>
                    </Form>        
                 </div>
        		</div>
         );

      

      case 1:
       return (
           <div className="table-responsive">
             <div className="modal-body page-form-outer text-left">
                <Form>
                   <div className="row">
                      

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="address1">Address1</Label><br/>
                                     <TextField
                                     type="text"
                                     name="address1"
                                     id="address1"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Address1"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="address2">Address2</Label><br/>
                                     <TextField
                                     type="text"
                                     name="address2"
                                     id="address2"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Address2"
                                     />
                               </FormGroup>
                            </div>

                             <div className="col-md-4">
                               <FormGroup>
                                  <Label for="city">City</Label><br/>
                                     <TextField
                                     type="text"
                                     name="city"
                                     id="city"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="City"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="state">State</Label>
                                  <Input
                                      type="select"
                                      name="state"
                                      id="state"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('state', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Alabama</option>
                                      <option value="2">Alaska</option>
                                      <option value="3">California</option>
                                      <option value="4">Georgia</option>
                                      <option value="5">Kansas</option>
                                      <option value="6">Minnesota</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="zip-code">Zip Code</Label><br/>
                                     <TextField
                                     type="text"
                                     name="zip-code"
                                     id="zip-code"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Zip Code"
                                     />
                               </FormGroup>
                            </div>


                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="region">County</Label>
                                  <Input
                                      type="select"
                                      name="region"
                                      id="region"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('region', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Northeast</option>
                                      <option value="0">Midwest</option>
                                      <option value="0">South</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="country">Country</Label>
                                  <Input
                                      type="select"
                                      name="country"
                                      id="country"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('country', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Australia</option>
                                      <option value="0">Denmark</option>
                                      <option value="0">India</option>
                                      <option value="0">United States</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="how-long">How long at this address?</Label><br/>
                                     <TextField
                                     type="text"
                                     name="how-long"
                                     id="how-long"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="How long at this address?"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="phone-no">Phone No</Label><br/>
                                     <TextField
                                     type="text"
                                     name="phone-no"
                                     id="phone-no"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Phone No"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-12">
                                 <FormGroup tag="fieldset">
                                  <FormGroup check>
                                     <Label check>
                                        <Input type="checkbox" name="primary_address" />{' '}
                                        Set as primary address
                                         </Label>
                                  </FormGroup>
                                 <FormGroup check>
                                     <Label check>
                                        <Input type="checkbox" name="billing_address" />{' '}
                                        Billing and physical address same
                                         </Label>
                                  </FormGroup>
                               </FormGroup> 
                             </div>
                   </div>
                   <br/>
                   <h4 className="blue-small-title">Billing Address</h4>

                   <div className="row">
                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="address1">Address1</Label><br/>
                                     <TextField
                                     type="text"
                                     name="address1"
                                     id="address1"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Address1"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="address2">Address2</Label><br/>
                                     <TextField
                                     type="text"
                                     name="address2"
                                     id="address2"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Address2"
                                     />
                               </FormGroup>
                            </div>

                             <div className="col-md-4">
                               <FormGroup>
                                  <Label for="city">City</Label><br/>
                                     <TextField
                                     type="text"
                                     name="city"
                                     id="city"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="City"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="state">State</Label>
                                  <Input
                                      type="select"
                                      name="state"
                                      id="state"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('state', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Alabama</option>
                                      <option value="2">Alaska</option>
                                      <option value="3">California</option>
                                      <option value="4">Georgia</option>
                                      <option value="5">Kansas</option>
                                      <option value="6">Minnesota</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="zip-code">Zip Code</Label><br/>
                                     <TextField
                                     type="text"
                                     name="zip-code"
                                     id="zip-code"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Zip Code"
                                     />
                               </FormGroup>
                            </div>


                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="region">County</Label>
                                  <Input
                                      type="select"
                                      name="region"
                                      id="region"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('region', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Northeast</option>
                                      <option value="0">Midwest</option>
                                      <option value="0">South</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="country">Country</Label>
                                  <Input
                                      type="select"
                                      name="country"
                                      id="country"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('country', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Australia</option>
                                      <option value="0">Denmark</option>
                                      <option value="0">India</option>
                                      <option value="0">United States</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="how-long">How long at this address?</Label><br/>
                                     <TextField
                                     type="text"
                                     name="how-long"
                                     id="how-long"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="How long at this address?"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="phone-no">Phone No</Label><br/>
                                     <TextField
                                     type="text"
                                     name="phone-no"
                                     id="phone-no"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Phone No"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-12">
      
                               <a href="#">Add more locations (+)</a>   
                             </div>

                            
                   </div>

                </Form>        
             </div>
           </div>

            );

      case 2:
         return (
                   <div className="table-responsive">
                     <div className="modal-body page-form-outer text-left">
                        <Form>
                           <div className="row">
                              
                               <div className="col-md-12">
                                         <FormGroup tag="fieldset">
                                          <Label>Employed</Label>
                                          <FormGroup check>
                                             <Label check>
                                                <Input type="radio" name="employed" checked />{' '}
                                                Yes
                                                 </Label>
                                          </FormGroup>
                                          <FormGroup check>
                                             <Label check>
                                                <Input type="radio" name="employed" />{' '}
                                                No
                                                 </Label>
                                          </FormGroup>
                                       </FormGroup>    
                                   </div>

                                   <div className="col-md-4">
                                       <FormGroup>
                                          <Label for="employment-type">Employment Type</Label>
                                          <Input
                                              type="select"
                                              name="employment-type"
                                              id="employment-type"
                                              placeholder=""
                                              onChange={(e) => onChangeAddNewUserDetails('employment-type', e.target.value)}
                                          >
                                              <option value="">Select</option>
                                              <option value="1">Self</option>
                                              <option value="0">Salaried</option>
                                              <option value="2">Others</option>
                                              
                                          </Input>
                                       </FormGroup>
                                    </div>

                                  <div className="col-md-4">
                                       <FormGroup>
                                          <Label for="annual-income">Annual Income($)</Label><br/>
                                             <TextField
                                             type="text"
                                             name="annual-income"
                                             id="annual-income"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="($)"
                                             />
                                       </FormGroup>
                                    </div>

                                    

                                    <div className="col-md-4">
                                      <FormGroup>
                                          <Label for="employed-since">Employed Since</Label>
                                          <Input type="date" name="employed-since" id="employed-since" placeholder="date placeholder" />
                                      </FormGroup>
                                      </div>

                                      <div className="col-md-4">
                                       <FormGroup>
                                          <Label for="employer-name">Employer Name</Label><br/>
                                             <TextField
                                             type="text"
                                             name="employer-name"
                                             id="employer-name"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Employer Name"
                                             />
                                       </FormGroup>
                                    </div>

                                    <div className="col-md-4">
                                       <FormGroup>
                                          <Label for="employer-phone">Employer Phone No</Label><br/>
                                             <TextField
                                             type="text"
                                             name="employer-phone"
                                             id="employer-phone"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Employer Phone No"
                                             />
                                       </FormGroup>
                                    </div>

                                    <div className="col-md-4">
                                       <FormGroup>
                                          <Label for="employer-email-address">Employer Email Address</Label><br/>
                                             <TextField
                                             type="text"
                                             name="employer-email-address"
                                             id="employer-email-address"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Employer Email Address"
                                             />
                                       </FormGroup>
                                    </div>

                               

                           </div>
                        </Form>        
                     </div>
                   </div>

            ); 

      case 3:
         return (
           <div className="table-responsive">
             <div className="modal-body page-form-outer text-left">
                <Form>
                   <div className="row">
                      <div className="col-md-4">
                               <FormGroup>
                                  <Label for="bank-name">Bank Name</Label><br/>
                                     <TextField
                                     type="text"
                                     name="bank-name"
                                     id="bank-name"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Bank Name"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="bank-address">Bank Address</Label><br/>
                                     <TextField
                                     type="text"
                                     name="bank-address"
                                     id="bank-address"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Bank Address"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="rounting-no">Routing Number</Label><br/>
                                     <TextField
                                     type="text"
                                     name="rounting-no"
                                     id="rounting-no"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Routing Number"
                                     />
                               </FormGroup>
                            </div>

                             <div className="col-md-4">
                               <FormGroup>
                                  <Label for="bank-ac">Bank A/C#</Label><br/>
                                     <TextField
                                     type="text"
                                     name="bank-ac"
                                     id="bank-ac"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Bank A/C#"
                                     />
                               </FormGroup>
                            </div>

                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="account-name">Name on Account</Label><br/>
                                     <TextField
                                     type="text"
                                     name="account-name"
                                     id="account-name"
                                     fullWidth
                                     variant="outlined"
                                     placeholder="Name on Account"
                                     />
                               </FormGroup>
                            </div>


                            <div className="col-md-4">
                               <FormGroup>
                                  <Label for="bankac-type">Bank A/C Type</Label>
                                  <Input
                                      type="select"
                                      name="bankac-type"
                                      id="bankac-type"
                                      placeholder=""
                                      onChange={(e) => onChangeAddNewUserDetails('bankac-type', e.target.value)}
                                  >
                                      <option value="">Select</option>
                                      <option value="1">Saving</option>
                                      <option value="0">Checking</option>
                                      
                                  </Input>
                               </FormGroup>
                            </div>

                            

                           

                   </div>
                </Form>        
             </div>
           </div>

            );



      default:
         return 'Unknown step';
   }
}

export default class HorizontalNonLinearStepper extends React.Component {
   state = {
      activeStep: 0,
      completed: {},
   };

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   render() {
      const steps = getSteps();
      const { activeStep } = this.state;

      return (
         <div className="stepper-outer">
            <Stepper nonLinear activeStep={activeStep}>
               {steps.map((label, index) => {
                  return (
                     <Step key={label}>
                        <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                           {label}
                        </StepButton>
                     </Step>
                  );
               })}
            </Stepper>
            <div>
               {this.allStepsCompleted() ? (
                  <div className="pl-40">
                     <p>All steps completed - you&quot;re finished</p>
                     <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                  </div>
               ) : (
                     <div className="text-right">
                        <p>{getStepContent(activeStep)}</p>
                        <Button variant="contained" className="btn-secondary text-white mr-10 mb-10" disabled={activeStep === 0} onClick={this.handleBack}>
                           Back
              </Button>
                        <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.handleNext}>
                           Next
              </Button>
                        
                     </div>
                  )}
            </div>
         </div>
      );
   }
}
