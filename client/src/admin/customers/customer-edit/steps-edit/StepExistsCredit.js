/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
const StepExistsCredit = ({DatePicker, existCustomer, onChnageExist, validateSearch, startDateExist, existError, searchCustomerSubmit, searchCustomerReset}) => (

   <div className="modal-body page-form-outer text-left">
      <div className="row">
         <div className="col-md-10">
            <FormGroup tag="fieldset">
               <Label>Do you want search exist customer details?</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="exist_customer"
                        value={1}
                        checked={(existCustomer.exist_customer == 1) ? true : false}
                        onChange={(e) => onChnageExist('exist_customer', e.target.value)}
                     />
                     Yes
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="exist_customer"
                        value={0}
                        checked={(existCustomer.exist_customer == 0) ? true : false}
                        onChange={(e) => onChnageExist('exist_customer', e.target.value)}
                     />
                     No
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(existCustomer.exist_customer == 0) ? "col-md-2 d-none" : "col-md-2"}>
            <div>
               <Button onClick={searchCustomerSubmit} disabled={!validateSearch} variant="contained" color="primary" className="text-white mr-10 mb-10" size="small">
                  Search
            </Button>
               <Button onClick={searchCustomerReset} variant="contained" color="primary" className="text-white mr-10 mb-10" size="small">
                  Reset
            </Button>
            </div>
         </div>
      </div>
      <div className={(existCustomer.exist_customer == 0) ? "row d-none" : "row"}>
         <div className="col-md-2">
            <FormGroup>
               <Label for="exist_first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_first_name"
                  id="exist_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={existCustomer.exist_first_name}
                  error={(existError.exist_first_name) ? true : false}
                  helperText={existError.exist_first_name}
                  onChange={(e) => onChnageExist('exist_first_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-2">
            <FormGroup>
               <Label for="exist_last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_last_name"
                  id="exist_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={existCustomer.exist_last_name}
                  error={(existError.exist_last_name) ? true : false}
                  helperText={existError.exist_last_name}
                  onChange={(e) => onChnageExist('exist_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-2">
            <FormGroup>
               <Label for="exist_dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="exist_dob"
                  id="exist_dob"
                  selected={startDateExist}
                  inputProps={{ maxLength: 10 }}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  onChange={(e) => onChnageExist('exist_dob', e)}
               />
               {(existError.exist_dob) ? <FormHelperText>{existError.exist_dob}</FormHelperText> : ''}
            </FormGroup>
         </div>
         <div className="col-md-2">
            <FormGroup>
               <Label for="exist_ssn">SSN<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_ssn"
                  id="exist_ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  placeholder="Social Security Number"
                  value={existCustomer.exist_ssn}
                  error={(existError.exist_ssn) ? true : false}
                  helperText={existError.exist_ssn}
                  onChange={(e) => onChnageExist('exist_ssn', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-2">
            <FormGroup>
               <Label for="account_no">Account No<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="account_no"
                  id="account_no"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  placeholder="Account Number"
                  value={existCustomer.account_no}
                  error={(existError.account_no) ? true : false}
                  helperText={existError.account_no}
                  onChange={(e) => onChnageExist('account_no', e.target.value)}
               />
            </FormGroup>
         </div>

      </div>
      


   </div>

);

export default StepExistsCredit;