/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Profile from './component/Profile';
import BankDetails from './component/BankDetails';
import EmploymentDetails from './component/EmploymentDetails';
import Address from './component/Address';
import UserBlock from './component/UserBlock';

// rct card box
import { RctCard } from 'Components/RctCard';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
  viewCustomer
} from 'Actions';
import {patientID} from '../../apifile';

function getSteps() {
   return ['My Profile', 'Bank Details', 'Employment Details', 'Address'];
}

class UserProfile extends Component {

   state = {
      activeTab: this.props.location.state ? this.props.location.state.activeTab : 0,
      activeStep: 0,
      completed: {},
   }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   componentDidMount() {
    this.props.viewCustomer(patientID());
  }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   render() {
      const steps = getSteps();
      const { activeStep } = this.state;
      const { loading, customerDetails, customerDetailsAddress } = this.props;
      return (
         <div className="userProfile-wrapper">
            <Helmet>
               <title>User Profile</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.userProfile" />} match={this.props.match} />
            <RctCard>
            {customerDetails &&
               <UserBlock 
                  customerDetails={customerDetails}
                  customerID={patientID()}
               />
            }
               

            {customerDetails &&
               <div>
                  <Stepper nonLinear activeStep={activeStep}>
                     {steps.map((label, index) => {
                        return (
                           <Step key={label}>
                              <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                                 {label}
                              </StepButton>
                           </Step>
                        );
                     })}
                  </Stepper>
                  <div>
                     {this.allStepsCompleted() ? (
                        <div className="pl-40">
                           <p>All steps completed - you&quot;re finished</p>
                           <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                        </div>
                     ) : (
                           <div className="pl-40">
                              {(() => {
                                 switch (activeStep) {
                                    case 0:
                                       return <Profile 
                                                customerDetails={customerDetails}
                                              />
                                       break;
                                    case 1:
                                       return <BankDetails 
                                                customerDetails={customerDetails}
                                              />
                                       break;
                                    case 2:
                                       return <EmploymentDetails 
                                                customerDetails={customerDetails}
                                              />
                                       break;
                                    case 3:
                                       return <Address 
                                                customerDetailsAddress={customerDetailsAddress}
                                              />
                                       break;
                                    default:
                                       return (<div></div>)
                                 }
                              })()}

                           </div>
                        )}
                  </div>
               </div>
            }

            </RctCard>
         </div>
      );
   }
}
const mapStateToProps = ({ Customer }) => {
   
  const { loading, customerDetails, customerDetailsAddress } = Customer;
  return { loading, customerDetails, customerDetailsAddress }
}
export default connect(mapStateToProps, {
  viewCustomer
})(UserProfile);