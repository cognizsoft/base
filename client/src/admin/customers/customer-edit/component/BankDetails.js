/**
 * Email Prefrences Page
 */
import React, { Component } from 'react';
import Switch from 'react-toggle-switch';
import Button from '@material-ui/core/Button';
import { FormGroup, Input } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';

// intl messages
import IntlMessages from 'Util/IntlMessages';

const BankDetails = ({ customerDetails }) => (
         <div className="prefrences-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">

                 <div className="view-box">
                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Bank Name :</td>
                               <td>{(customerDetails.bank_name) ? customerDetails.bank_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Name on Account :</td>
                               <td>{(customerDetails.account_name) ? customerDetails.account_name : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Routing Number :</td>
                               <td>{(customerDetails.rounting_no) ? customerDetails.rounting_no : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>

                     <div className="width-50">
                        <table>
                           <tbody>
                             <tr>
                               <td className="fw-bold">Bank A/C Type :</td>
                               <td>{(customerDetails.account_type) ? customerDetails.account_type : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Bank A/C# :</td>
                               <td>{(customerDetails.account_number) ? customerDetails.account_number : '-'}</td>
                             </tr>
                             <tr>
                               <td className="fw-bold">Bank Address :</td>
                               <td>{(customerDetails.bank_address) ? customerDetails.bank_address : '-'}</td>
                             </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      );
export default BankDetails;