/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { URL } from '../../apifile/URL';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import Button from '@material-ui/core/Button';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isObjectEmpty } from '../../validator/Validator';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {
  applicationDoc, downloadOneDrive, downloadDoc, customerUploadDocument, customerDocument
} from 'Actions';
class AllDocumentsApplication extends Component {

  state = {
    all: false,
    users: null, // initial user data
    opnDocFileModal: false,
    imgPath: '',
    imgPlanNumber: '',
    imgItemId: '',
    loading: false,

    selectedDocCount: 0,
    appDocumentsList: null,
    selectedDoc: null,

    addData: {
      document: ''
    },
    add_err: {},
    inputKey: '',
  }

  onSelectInvoice(doc) {
    doc.checked = !doc.checked;
    let selectedDocCount = 0;
    let docObj = [];
    let appDocumentsList = this.state.appDocumentsList.map(docData => {
      if (docData.item_id === doc.item_id) {
        if (docData.checked && docData.checked !== undefined) {
          selectedDocCount++;

          docObj.push({ 'name': docData.name, 'item_id': docData.item_id })
          //selectedDoc[docData.name] = docData.item_id
        }
        return doc;
      } else {
        if (docData.checked && docData.checked !== undefined) {
          selectedDocCount++;

          docObj.push({ 'name': docData.name, 'item_id': docData.item_id })
        }
        return docData;
      }
    });

    this.setState({ appDocumentsList, selectedDocCount, selectedDoc: docObj });
  }

  componentWillReceiveProps(nextProps) {
    //(nextProps.allDocuments) ? this.setState({ appDocumentsList: nextProps.allDocuments }) : '';
    //(nextProps.docStatus && nextProps.docStatus !== undefined && nextProps.docStatus.status == 1) ? this.setState({ loading: false }) : '';
  }
  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    this.props.customerDocument(this.props.match.params.patient_id);
  }

  opnDocFileModal(item_id, plan_number) {
    this.props.downloadOneDrive(item_id)
    this.setState({ opnDocFileModal: true, imgPlanNumber: plan_number, imgItemId: item_id })
  }

  opnViewDocFileModalClose = () => {
    this.setState({ opnDocFileModal: false })
  }

  downloadDoc() {
    this.props.downloadDoc(this.state.imgItemId)
    //return false;
  }

  onChnage(key, value) {
    let { add_err } = this.state;
    switch (key) {
      case 'document':
        value = value.target.files[0];
        console.log(value)
        if (isObjectEmpty(value)) {
          add_err[key] = "Please upload document before submitting.";
        } else {
          add_err[key] = '';
        }
        break;
    }
    this.setState({
      addData: {
        ...this.state.addData,
        [key]: value
      }
    });
    this.setState({ add_err: add_err });
  }

  onClear() {
    let addData = {
      document: ''
    }

    let add_err = {
      document: 'Select file'
    }
    this.setState({ addData: addData, add_err: add_err, inputKey: Math.random().toString(36) })
  }

  callAction(type, e) {
    let datafrom = new FormData();
    datafrom.append('customer_id', this.props.match.params.patient_id);
    datafrom.append('imgedata', this.state.addData.document);

    this.props.customerUploadDocument(datafrom);

    let addData = {
      document: ''
    }

    let add_err = {
      document: 'Select file'
    }
    this.setState({ addData: addData, add_err: add_err, inputKey: Math.random().toString(36) })
    //onClear()
  }

  validateSubmit() {
    return (this.state.add_err.document === '');
  }

  render() {
    const { loading, allDocuments, selectedDocCount } = this.props;
    console.log(this.state)
    //console.log(this.state.selectedDoc)
    let diableStatus = 0;
    if (allDocuments !== undefined) {
      diableStatus = allDocuments.reduce(function (accumulator, currentValue) {
        if (currentValue.checked === true) {
          accumulator++
        }
        return accumulator;
      }, 0);
    }
    return (
      <div className="all-documents">
        <Helmet>
          <title>Health Partner | Admin | Customers | All Documents</title>
          <meta name="description" content="All Documents" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.allDocuments" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          <div className="table-responsive">
            <div className="modal-body page-form-outer view-section">

              <div className="row">

                <div className="col-md-4">
                  <FormGroup>
                    <Label for="document">Upload Document</Label>
                    <Input
                      type="file"
                      name="document"
                      id="document"
                      key={this.state.inputKey || ''}
                      className="p-5"
                      accept="image/gif, image/jpeg, image/png, application/pdf"
                      onChange={(e) => this.onChnage('document', e)}
                    >
                    </Input>

                  </FormGroup>
                </div>

                <div className="col-md-4">
                  <div className="mps-submt-btn">
                    <Button variant="contained" color="primary" className="text-white mr-10" onClick={this.callAction.bind(this)} disabled={!this.validateSubmit()}>Upload</Button>
                    <Button variant="contained" color="secondary" className="text-white" onClick={this.onClear.bind(this)}>Clear</Button>
                  </div>
                </div>

              </div>
              <hr class="border-dark"></hr>
              <h4>Plan Folder</h4>
              <div className="row">
                {
                  this.props.planDocuments && this.props.planDocuments.map((folder, idx) => (
                    <div key={idx} className="col-sm-6 col-md-4 col-lg-4 col-xl-3">
                      <div className="col-md-12">


                        <h4>View Plan {folder.plan_number}</h4>
                        <Link to={`/admin/credit-application/plan-documents/${folder.pp_id}`}>
                          <i class="zmdi zmdi-folder-person zmdi-hc-5x"></i>
                        </Link>


                      </div>
                    </div>
                  ))
                }
              </div>
              <hr class="border-dark"></hr>
              <h4>Application Documents</h4>
              <div className="row mt-10">
                {allDocuments && allDocuments.map((img, idx) => (
                  <div key={idx} className="col-sm-6 col-md-4 col-lg-4 col-xl-3">

                    <figure className="img-wrapper border border-secondary" onClick={() => this.opnDocFileModal(img.item_id, img.plan_number)}>

                      <img src={`${img.file_path}`} width="250" height="300" />
                      <figcaption>
                        <h4>View and Download</h4>
                      </figcaption>
                      <a href="javascript:void(0);">&nbsp;</a>
                    </figure>

                  </div>
                ))}


              </div>

            </div>
          </div>

          <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

            <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
              <span className="float-right"><a href="javascript:void(0)" onClick={() => this.downloadDoc()} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span>
            </ModalHeader>

            <ModalBody>
              {this.props.oneDrivePreview &&
                <embed src={this.props.oneDrivePreview} width="100%" height="500" download />
              }
              {!this.props.oneDrivePreview &&
                <div className="p-50 text-center"><RctSectionLoader /></div>
              }
              {
                this.props.doc_loading &&
                <RctSectionLoader />
              }

            </ModalBody>

          </Modal>

          {this.props.loading &&
            <RctSectionLoader />
          }
          {this.props.upl_loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication, Customer }) => {
  const { oneDrivePreview, docStatus, doc_loading } = creditApplication;
  const { loading, upl_loading, allDocuments, planDocuments } = Customer;
  return { loading, upl_loading, allDocuments, oneDrivePreview, docStatus, doc_loading, planDocuments }
}
export default connect(mapStateToProps, {
  applicationDoc, downloadOneDrive, downloadDoc, customerUploadDocument, customerDocument
})(AllDocumentsApplication);