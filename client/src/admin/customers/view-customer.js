/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IconButton from '@material-ui/core/IconButton';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge,
  Collapse
} from 'reactstrap';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Link } from 'react-router-dom';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import VerifyAmountForm from './VerifyAmountForm';
import { isEmpty, isContainWhiteSpace, isNumeric, pointDecimals, isDecimals, isNumericDecimal, isLength } from '../../validator/Validator';
import { NotificationManager } from 'react-notifications';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MaterialDatatable from "material-datatable";
import {
  viewCustomer, submitVerifyAmount
} from 'Actions';
class PatientView extends Component {

  state = {
    showSsn: false,
    all: false,
    addNewVerifyAmountModal: false,
    loading: false,

    verify_err: {
      verify_amount: ''
    },
    verify_amt: {
      patient_id: '',
      verify_amount: ''
    },
    collapse_sec: false,
    collapse_cus_info: false,
    collapse_app_info: false,
    collapse_add: false,
    collapse_emp_info: false,
    collapse_bank_info: false,
    collapse_user_info: false,
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let { updateForm } = this.state;
    updateForm[name] = value;

    this.setState({
      updateForm: updateForm
    }, function () {
      this.onVerifyAmountDetails(name, value)
    });
  }

  onVerifyAmount() {
    let { verify_amt, verify_err } = this.state;

    var verify_error = {}
    verify_amt.patient_id = this.props.match.params.id

    this.setState({ addNewVerifyAmountModal: true, verify_amt: verify_amt, verify_err: verify_error });
  }

  onVerifyAmountModalClose() {
    let { verify_err } = this.state

    var verify_error = {}

    this.setState({ addNewVerifyAmountModal: false, verify_err: verify_error })
  }

  onVerifyAmountDetails(fieldName, value) {
    console.log(value)
    let { verify_err } = this.state;
    switch (fieldName) {
      case 'verify_amount':
        value = pointDecimals(value);
        if (isEmpty(value)) {
          verify_err[fieldName] = "Amount can't be blank";
        } else if (!isDecimals(value)) {
          verify_err[fieldName] = "Amount not valid";
        } else if (value <= 0 || value >= 1) {
          verify_err[fieldName] = 'Amount should be between 0.00 - 1.00';
        } else {
          verify_err[fieldName] = '';
        }
        break;
      default:
        break;

    }

    this.setState({ verify_err: verify_err });

    this.setState({
      verify_amt: {
        ...this.state.verify_amt,
        [fieldName]: value
      }
    });

  }

  verifyAmount() {
    const { verify_amt } = this.state;

    console.log(verify_amt)
    //return false;

    this.props.submitVerifyAmount(verify_amt);

    let self = this;
    setTimeout(() => {
      this.setState({ loading: true, addNewVerifyAmountModal: false });
      //NotificationManager.success('Amount Submitted!');
      self.setState({ loading: false });
    }, 2000);
  }

  validateVerifyAmount() {
    return (
      this.state.verify_err.verify_amount === ''
    )
  }

  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 13,2019
  */
  componentDidMount() {
    //this.permissionFilter(this.state.currentModule);
    this.props.viewCustomer(this.props.match.params.id);
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  handleClickShowSsn = () => {
    this.setState({ showSsn: !this.state.showSsn });
  };
  /****Collpase secondary contact details****/
  onCollapse() {
    this.setState({ collapse_sec: !this.state.collapse_sec });
  }
  onCollapseCusInfo() {
    this.setState({ collapse_cus_info: !this.state.collapse_cus_info });
  }
  onCollapseappInfo() {
    this.setState({ collapse_app_info: !this.state.collapse_app_info });
  }
  onCollapseAddInfo() {
    this.setState({ collapse_add: !this.state.collapse_add });
  }
  onCollapseEmpInfo() {
    this.setState({ collapse_emp_info: !this.state.collapse_emp_info });
  }
  onCollapseBankInfo() {
    this.setState({ collapse_bank_info: !this.state.collapse_bank_info });
  }
  onCollapseUserInfo() {
    this.setState({ collapse_user_info: !this.state.collapse_user_info });
  }
  /****Collpase secondary contact details****/
  getMuiTheme = () => createMuiTheme({
    overrides: {
      MaterialDatatableToolbar: {
        root: { display: "none" }
      },
    }
  })
  render() {
    const { loading, customerDetails, customerDetailsAddress } = this.props;
    const { err, verify_err } = this.state;
    const applicationList = this.props.applicationDetails;
    const columns = [
      { name: 'ID', field: 'application_id', },
      { name: 'A/C No', field: 'patient_ac', },
      { name: 'App No', field: 'application_no', },
      {
        name: 'Full Name',
        field: 'f_name',
        options: {
          noHeaderWrap: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              value.f_name + ' ' + value.m_name + ' ' + value.l_name
            )
          },
        }
      },
      { name: 'Address', field: 'address1', },
      { name: 'City', field: 'City', },
      { name: 'State', field: 'state_name', },
      /*{ name: 'Zip', field: 'zip_code', },
      { name: 'Email', field: 'email', },*/
      { name: 'Primary Phone', field: 'peimary_phone', },
      {
        name: 'Status',
        field: 'status',
        options: {
          noHeaderWrap: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            var appstatus = '';
            if (value.status == '1' && value.plan_flag == '1') {
              appstatus = 'Approved';
            } else if (value.status == '1' && value.plan_flag == '0') {
              appstatus = 'Plan Not Selected';
            } else if (value.status == '0') {
              appstatus = 'Unapproved';
            } else if (value.status == '2') {
              appstatus = 'Manual Process';
            } else if (value.status == '3') {
              appstatus = 'Rejected';
            } else if (value.status == '4') {
              appstatus = 'Need More Information';
            } else if (value.status == '5') {
              appstatus = 'Document Review Process';
            }
            return (
              <React.Fragment>
                <span className="d-flex justify-content-start">
                  <div className="status">
                    <span className="d-block">{value.status_name}</span>
                  </div>
                </span>
              </React.Fragment>
            )
          },
          customValue: (value, tableMeta, updateValue) => {
            var appstatus = '';
            if (value.status == '1' && value.plan_flag == '1') {
              appstatus = 'Approved';
            } else if (value.status == '1' && value.plan_flag == '0') {
              appstatus = 'Plan Not Selected';
            } else if (value.status == '0') {
              appstatus = 'Unapproved';
            } else if (value.status == '2') {
              appstatus = 'Manual Process';
            } else if (value.status == '3') {
              appstatus = 'Rejected';
            } else if (value.status == '4') {
              appstatus = 'Need More Information';
            } else if (value.status == '5') {
              appstatus = 'Document Review Process';
            }
            return value.status_name;
          },
          customSortValue: (value, tableMeta, updateValue) => {
            var appstatus = '';
            if (value.status == '1' && value.plan_flag == '1') {
              appstatus = 'Approved';
            } else if (value.status == '1' && value.plan_flag == '0') {
              appstatus = 'Plan Not Selected';
            } else if (value.status == '0') {
              appstatus = 'Unapproved';
            } else if (value.status == '2') {
              appstatus = 'Manual Process';
            } else if (value.status == '3') {
              appstatus = 'Rejected';
            } else if (value.status == '4') {
              appstatus = 'Need More Information';
            } else if (value.status == '5') {
              appstatus = 'Document Review Process';
            }
            return value.status_name;
          },
        }
      },
      {
        name: 'LOC',
        field: 'approve_amount',
        options: {
          noHeaderWrap: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              (value.approve_amount) ? '$' + (value.approve_amount).toFixed(2) : '$0.00'
            )
          },
        }
      },
      {
        name: 'Over Amt',
        field: 'override_amount',
        options: {
          noHeaderWrap: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              (value.override_amount) ? '$' + (value.override_amount).toFixed(2) : '$0.00'
            )
          },
        }
      },
      {
        name: 'Avl Bal',
        field: 'remaining_amount',
        options: {
          noHeaderWrap: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              (value.remaining_amount != null || value.override_amount) ? '$' + (value.remaining_amount + value.override_amount).toFixed(2) : '$0.00'
            )
          },
        }
      },
      {
        name: 'Date Created',
        field: 'date_created',
      },
      {
        name: 'Expire Date',
        field: 'expiry_date',
      },
      {
        name: 'Action',
        field: 'md_id',
        options: {
          noHeaderWrap: true,
          filter: false,
          sort: false,
          download: false,
          customBodyRender: (value, tableMeta, updateValue) => {

            return (
              <React.Fragment>
                <span className="list-action">
                  <Link to={{ pathname: `/admin/credit-application/edit/${value.application_id}`, state: { prevPath: 1 } }}><i className="ti-pencil"></i></Link>

                </span>
              </React.Fragment>
            )
          },

        }
      },


    ];
    const options = {

      filter: true,
      filterType: 'dropdown',
      selectableRows: false,
      download: false,
      rowsPerPageOptions: [10, 20, 50, 100],
      pagination: true,
      downloadOptions: { filename: 'creditApplication.csv' },
    };
    const userAllowS = JSON.parse(this.props.user);

    return (
      <div className="country admin-application-list">
        <Helmet>
          <title>Health Partner | Customer | View</title>
          <meta name="description" content="Regions" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.viewCustomer" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          {customerDetails &&
            <div className="table-responsive2 provider-create-invoice">
              <div className="text-right p-10 pr-20">

                <div className="d-inline">
                  <span className="back-btn" onClick={this.goBack.bind(this)} title="Back"><i className="zmdi zmdi-arrow-left"></i></span>
                </div>

              </div>
              <div className="modal-body page-form-outer view-section pt-0">
                <div className="row">
                  <div className="col-sm-12 w-xs-full">
                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>Customer Information</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapseCusInfo()}><i className={(this.state.collapse_cus_info) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_cus_info}>
                        <div className="rct-block-content">
                          <div className="width-100">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td className="fw-bold">First Name:</td>
                                  <td>{customerDetails.f_name}</td>
                                  <td className="fw-bold">Status:</td>
                                  <td>{(customerDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Middle Name:</td>
                                  <td>{(customerDetails.m_name != '') ? customerDetails.m_name : '-'}</td>
                                  <td className="fw-bold">Primary Email Address:</td>
                                  <td>{customerDetails.email}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Last Name:</td>
                                  <td>{customerDetails.l_name}</td>
                                  <td className="fw-bold">Secondary Email Address:</td>
                                  <td>{(customerDetails.secondary_email) ? customerDetails.secondary_email : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Date of Birth:</td>
                                  <td>{customerDetails.dob}</td>
                                  <td className="fw-bold">Gender:</td>
                                  <td>{(customerDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Primary Phone No.:</td>
                                  <td>{customerDetails.peimary_phone}</td>
                                  <td className="fw-bold">Customer A/C:</td>
                                  <td>{customerDetails.patient_ac}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Secondary Phone No.:</td>
                                  <td>{(customerDetails.alternative_phone) ? customerDetails.alternative_phone : '-'}</td>
                                  <td className="fw-bold">SSN / Tax ID:</td>
                                  <td>
                                    {
                                      (userAllowS.role_name == 'SuperAdmin') ?
                                        <React.Fragment>
                                          {(customerDetails.ssn !== undefined) ? (!this.state.showSsn) ? customerDetails.ssn.replace(/.(?=.{4})/g, 'x') : customerDetails.ssn : '-'}

                                          <IconButton
                                            className="eye-mask"
                                            onClick={this.handleClickShowSsn.bind(this)}
                                          >
                                            {!this.state.showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                                          </IconButton>
                                        </React.Fragment>
                                        :
                                        'xxxxxxxxxxx'
                                    }

                                  </td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Withdrawal Day:</td>
                                  <td>{(customerDetails.withdrawal_date) ? customerDetails.withdrawal_date : '-'}</td>
                                  <td className="fw-bold"></td>
                                  <td>
                                    

                                  </td>
                                </tr>
                              </tbody>

                            </table>
                          </div>
                        </div>
                      </Collapse>
                    </div>

                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>Bank Details</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapseBankInfo()}><i className={(this.state.collapse_bank_info) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_bank_info}>
                        <div className="rct-block-content">
                          {this.props.bankDetails && this.props.bankDetails.map((bank, idx) => (
                            <div className="width-100" key={idx}>
                              <table className="table">
                                <tbody>
                                  <tr>
                                    <td className="fw-bold">Bank Name:</td>
                                    <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                                    <td className="fw-bold">Name on Account:</td>
                                    <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Routing Number:</td>
                                    <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                                    <td className="fw-bold">Bank A/C Type:</td>
                                    <td>{(bank.value) ? bank.value : '-'}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Bank A/C#:</td>
                                    <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                                    <td className="fw-bold">Bank Address</td>
                                    <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                                  </tr>
                                </tbody>
                              </table>
                              <hr className="border-dark" />
                            </div>
                          ))
                          }
                        </div>

                      </Collapse>
                    </div>

                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>Secondary Contact Details</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapse()}><i className={(this.state.collapse_sec) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_sec}>
                        <div className="rct-block-content">
                          {this.props.secDetails && this.props.secDetails.map((secDetails, idx) => (
                            <div className="width-100 mb-10" key={idx}>
                              <table className="table">
                                {this.props.secDetails &&
                                  <tbody>
                                    <tr>
                                      <td className="fw-bold">First Name:</td>
                                      <td>{secDetails.f_name}</td>
                                      <td className="fw-bold">Middle Name:</td>
                                      <td>{(secDetails.m_name != '') ? secDetails.m_name : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Last Name:</td>
                                      <td>{secDetails.l_name}</td>
                                      <td className="fw-bold">Relationship:</td>
                                      <td>{secDetails.relationship_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Phone:</td>
                                      <td>{secDetails.phone}</td>
                                      <td className="fw-bold">Email:</td>
                                      <td>{secDetails.email}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Address:</td>
                                      <td>{secDetails.address1 + ' ' + secDetails.address2}</td>
                                      <td className="fw-bold">City:</td>
                                      <td>{secDetails.city}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">State:</td>
                                      <td>{secDetails.state_name}</td>
                                      <td className="fw-bold">Country</td>
                                      <td>{secDetails.country_name}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Zip Code:</td>
                                      <td>{secDetails.zip_code}</td>
                                      <td className="fw-bold"></td>
                                      <td></td>
                                    </tr>
                                  </tbody>
                                }

                              </table>
                              <hr className="border-dark" />
                            </div>
                          ))
                          }
                          {(this.props.secDetails == "") &&
                            <div className="width-100 mb-10">
                              <table className="table">
                                <tbody><tr><td colSpan="4">Secondary contact details not available.</td></tr></tbody>
                              </table>
                            </div>
                          }
                        </div>
                      </Collapse>
                    </div>


                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>Locations</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapseAddInfo()}><i className={(this.state.collapse_add) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_add}>
                        <div className="rct-block-content">
                          {this.props.appDetailsAddress && this.props.appDetailsAddress.map((address, idx) => (
                            <div className="width-100 mb-10" key={idx}>
                              <table className="table">
                                <tbody>
                                  <tr>
                                    <td className="fw-bold">Address1:</td>
                                    <td>{address.address1}</td>
                                    <td className="fw-bold">City:</td>
                                    <td>{address.city}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Address2:</td>
                                    <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                    <td className="fw-bold">Zip Code:</td>
                                    <td>{address.zip_code}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Country:</td>
                                    <td>{address.country_name}</td>
                                    <td className="fw-bold">How long at this address?:</td>
                                    <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">State:</td>
                                    <td>{address.state_name}</td>
                                    <td className="fw-bold">Phone No:</td>
                                    <td>{address.phone_no}</td>
                                  </tr>
                                  <tr>
                                    <td className="fw-bold">Primary Address:</td>
                                    <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                    <td className="fw-bold">Billing and Physical address same:</td>
                                    <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                                  </tr>
                                  {/*<tr>
                              <td className="fw-bold">County:</td>
                              <td>{(address.county != '') ? address.county : '-'}</td>
                              <td className="fw-bold">Region:</td>
                              <td>{address.region_name}</td>                              
                            </tr>*/}
                                </tbody>
                              </table>
                              <hr className="border-dark" />
                            </div>
                          ))
                          }
                        </div>
                      </Collapse>
                    </div>

                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>Employment Information</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapseEmpInfo()}><i className={(this.state.collapse_emp_info) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_emp_info}>
                        <div className="rct-block-content">
                          <div className="width-100">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Employed:</td>
                                  <td>{(customerDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                                  <td className="fw-bold">Employer Name:</td>
                                  <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_name : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Employment Type:</td>
                                  <td>{(customerDetails.employment_status == 1) ? customerDetails.emp_type_name : '-'}</td>
                                  <td className="fw-bold">Employer Phone No:</td>
                                  <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_phone : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Annual Income($):</td>
                                  <td>{(customerDetails.employment_status == 1) ? '$' + customerDetails.annual_income : '-'}</td>
                                  <td className="fw-bold">Employer Email Address:</td>
                                  <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_email : '-'}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Employed Since:</td>
                                  <td>{(customerDetails.employment_status == 1) ? customerDetails.employer_since : '-'}</td>
                                  <td className="fw-bold">&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </tbody>

                            </table>
                          </div>
                        </div>
                      </Collapse>
                    </div>


                    <div className="rct-block custom-collapse">
                      <div className="rct-block-title">
                        <h4><span>User Information</span></h4>
                        <div className="contextual-link">
                          <a href="javascript:void(0)" onClick={() => this.onCollapseUserInfo()}><i className={(this.state.collapse_user_info) ? "ti-minus" : "ti-plus"}></i></a>
                        </div>
                      </div>
                      <Collapse isOpen={this.state.collapse_user_info}>
                        <div className="rct-block-content">
                          <div className="width-100">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <td className="fw-bold">Username:</td>
                                  <td>{customerDetails.username}</td>
                                  <td className="fw-bold">Primary Phone No.:</td>
                                  <td>{customerDetails.user_phone1}</td>
                                </tr>
                                <tr>
                                  <td className="fw-bold">Email Address:</td>
                                  <td>{customerDetails.user_eamil}</td>
                                  <td className="fw-bold">Secondary Phone No.:</td>
                                  <td>{(customerDetails.user_phone2 != '') ? customerDetails.user_phone2 : '-'}</td>
                                </tr>
                              </tbody>

                            </table>
                          </div>
                        </div>
                      </Collapse>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          }

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>
        <RctCollapsibleCard fullBlock>
          <div></div>
          {customerDetails &&
            <MuiThemeProvider theme={this.getMuiTheme()}>
              <MaterialDatatable
                data={applicationList}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          }
        </RctCollapsibleCard>
        <Modal isOpen={this.state.addNewVerifyAmountModal} toggle={() => this.onVerifyAmountModalClose()}>
          <ModalHeader toggle={() => this.onVerifyAmountModalClose()}>

            Verify Bank Account

               </ModalHeader>
          <ModalBody>

            <VerifyAmountForm
              verifyAmtErr={verify_err}
              onVerifyAmountDetail={this.onVerifyAmountDetails.bind(this)}
              verifyAmt={this.state.verify_amt}
            />

          </ModalBody>
          <ModalFooter>

            <Button variant="contained" color="primary" className="text-white" onClick={() => this.verifyAmount()} disabled={!this.validateVerifyAmount()}>Submit</Button>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onVerifyAmountModalClose()}>Cancel</Button>

          </ModalFooter>
        </Modal>

      </div>
    );
  }
}
const mapStateToProps = ({ Customer, authUser }) => {
  const { user } = authUser;
  const {
    loading,
    customerDetails,
    appDetailsAddress,
    secDetails,
    experianEmployment,
    experianAddress,
    bankDetails,
    applicationDetails,
  } = Customer;
  return {
    loading,
    customerDetails,
    appDetailsAddress,
    secDetails,
    experianEmployment,
    experianAddress,
    bankDetails,
    applicationDetails,
    user,
  }
}
export default connect(mapStateToProps, {
  viewCustomer, submitVerifyAmount
})(PatientView);