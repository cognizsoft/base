/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { Link } from 'react-router-dom';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';
import HozNonLinear from '../credit-applications/add-application-stepper';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllUsers
} from 'Actions';

export default class AddNewPatient extends Component {

   state = {
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0
   }


	
	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/**
	 * On Reload
	 */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

	/**
	 * On Select User
	 */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }

	
   /**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
       //console.log(this.state.openViewUserDialog);
      this.setState({ openViewUserDialog: true, selectedUser: data });
     // console.log(this.state.openViewUserDialog);
   }

	/**
	 * On Edit User
	 */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose() {
      // console.log(this.state.addNewUserModal);
      this.setState({ addNewUserModal: false, editUser: null })
      //console.log(this.state.addNewUserModal);
   }

    /**
	 * On View User Modal Close
	 */
   onViewUserModalClose = () => {
       //console.log(this.state.addViewUserModal);
      this.setState({ openViewUserDialog: false, selectedUser: null })
      //console.log(this.state.addViewUserModal);
   }
   
   
	

   render() {
      const { users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      const data = [
         { full_name: "David Smith", gender: "Male", email: "david@gmail.com", primary_phone: "+312-478-1120", status: "Active"},
         { full_name: "Maria Garcia", gender: "Female", email: "maria@gmail.com", primary_phone: "+271-347-0402", status: "Inactive"},
      ];
      console.log("dfddgdgdfAAA")
      const options = {
         filterType: 'dropdown',
      };
      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsAddCreditApplications" />}
               match={this.props.match}
            />
            
            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer text-left">
                     <Form>
                        <div className="row">
                           <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="first-name">First Name</Label><br/>
                                         <TextField
                                         type="text"
                                         name="first-name"
                                         id="first-name"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="First Name"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="last-name">Last Name</Label><br/>
                                         <TextField
                                         type="text"
                                         name="last-name"
                                         id="last-name"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Last Name"
                                         />
                                   </FormGroup>
                                </div>
                                <div className="col-md-4">
                                  <FormGroup>
                                      <Label for="effective_date">Date of Birth</Label>
                                      <Input type="date" name="dob" id="dob" placeholder="date placeholder" />
                                  </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="patent-record">Patient Record</Label><br/>
                                         <TextField
                                         type="text"
                                         name="patent-record"
                                         id="patent-record"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Patient Record"
                                         />
                                   </FormGroup>
                                </div>

                                <div className="col-md-4">
                                   <FormGroup>
                                      <Label for="ssn">Social Security Number</Label><br/>
                                         <TextField
                                         type="text"
                                         name="ssn"
                                         id="ssn"
                                         fullWidth
                                         variant="outlined"
                                         placeholder="Social Security Number"
                                         />
                                   </FormGroup>
                                </div>
                                <div className="col-md-4">
                                  <div className="mps-submt-btn">
                                 
                                  <Button variant="contained" color="primary" className="text-white mr-10 mb-10" >
                                     Submit
                                  </Button>
                                  
                                  </div>
                                </div>

                          
                        </div>
                     </Form>
                  </div>

                 <table className="table table-middle table-hover mb-0 mps-table-data">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Full Name</th>
                           <th>Gender</th>
                           <th>Email</th>
                           <th>Primary Phone</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        {data && data.map((user, key) => (
                           <tr key={key}>
                              <td>{key +1}</td>
                              <td>
                                 <div className="media">
                                    <div className="media-body">
                                       <h5 className="mb-5 fw-bold">{user.full_name}</h5>
                                    </div>
                                 </div>
                              </td>
                              <td><span className="">{user.gender}</span></td>
                              <td><span className="">{user.email}</span></td>
                              <td><span className="">{user.primary_phone}</span></td>
                              <td className="d-flex justify-content-start">
                               <div className="status">
                                    <span className="d-block">{user.status}</span>
                                 </div>
                              </td>
                              <td className="list-action">
                                 <Button variant="contained" color="primary" className="text-white mr-10 mb-10 mt-10" >
                                     Select
                                  </Button>

                              </td>
                           </tr>
                        ))}
                     </tbody>
                     <tfoot className="border-top">
                        
                     </tfoot>
                  </table>

               </div>





            </RctCollapsibleCard>
           
         </div>
      );
   }
}
