/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ updateErr, updateUserDetails, onUpdateUserDetail,DatePicker,startDate, selectedExpDatCmt }) => (
    <Form>
    <div className="row">
        
        
        <div className="col-md-12">
	        <FormGroup>
			<Label for="expiry_date">Expiry Date<span className="required-field">*</span></Label>
			
                <DatePicker
                dateFormat="MM/dd/yyyy"
                name="expiry_date"
                id="expiry_date"
                selected={startDate}
                placeholderText="MM/DD/YYYY"
                autocomplete={false}
                onChange={(e) => onUpdateUserDetail('expiry_date', e)}
                />
                {(updateErr.expiry_date) ? <FormHelperText>{updateErr.expiry_date}</FormHelperText> : ''}
            
		  </FormGroup>
        </div>

        <div className="col-md-12">
            <FormGroup>
                <Label for="expiry_date_cmt">Comment</Label>
                <Input
                    type="textarea"
                    name="expiry_date_cmt"
                    id="expiry_date_cmt"
                    value={(selectedExpDatCmt != '') ? selectedExpDatCmt : ''}
                    onChange={(e) => onUpdateUserDetail('expiry_date_cmt', e.target.value)}
                    />
            </FormGroup>
        </div>

    </div>
    </Form>
);

export default UpdateUserForm;
