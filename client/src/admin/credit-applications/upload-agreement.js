import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import {
   Button,
   Form,
   FormGroup,
   Label,
   Input,
   FormText,
   Col,
   FormFeedback
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import CryptoJS from 'crypto-js';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isObjectEmpty } from '../../validator/Validator';
import {
   planUploadAgreement, uploadAgreementViewDetails
} from 'Actions';

class uploadAgreementDocument extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         document: ''
      },
      add_err: {},
      uploadAgreementRedirect: 0,
   }
   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.uploadAgreementViewDetails(this.dec(this.props.match.params.planid));
   }
   /*
  * Title :- callAction
  * Descrpation :- This function use for submit uploaded document
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   callAction(type, e) {
      let datafrom = new FormData();
      datafrom.append('plan_id', this.dec(this.props.match.params.planid));
      datafrom.append('customer_id', this.props.planDetails[0].patient_ac);
      datafrom.append('application_id', this.props.planDetails[0].application_id);
      datafrom.append('application_no', this.props.planDetails[0].application_no);
      datafrom.append('pp_id', this.props.planDetails[0].pp_id);
      datafrom.append('imgedata', this.state.addData.document);

      this.props.planUploadAgreement(datafrom);
   }
   /*
  * Title :- validateSubmit
  * Descrpation :- This function use for check validation
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   validateSubmit() {
      return (this.state.add_err.document === '');
   }
   /*
  * Title :- componentWillReceiveProps
  * Descrpation :- This function use for check props and redirect url
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   componentWillReceiveProps(nextProps) {
      (nextProps.uploadAgreementRedirect && nextProps.uploadAgreementRedirect != '') ? this.setState({ uploadAgreementRedirect: nextProps.uploadAgreementRedirect }) : this.setState({ uploadAgreementRedirect: 0});
   }
   /*
  * Title :- onChnage
  * Descrpation :- This function use for set value and error in state according to user action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 20,2019
  */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'document':
            value = value.target.files[0];
            if (isObjectEmpty(value)) {
               add_err[key] = "Please upload document before submitting.";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   render() {
      if (this.state.uploadAgreementRedirect == 1) {
          return (<Redirect to={'/admin/credit-application/plan-details/'+this.enc((this.props.planDetails[0].application_id).toString())} />);
      }
      if(this.props.planDetails) {
          if(this.props.planDetails[0].plan_status !== 2) {
            return (<Redirect to={'/admin/credit-application/plan-details/'+this.enc((this.props.planDetails[0].application_id).toString())} />);
          }
      }
      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Credit Application | Upload Agreement</title>
               <meta name="description" content="Upload Agreement" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.uploadAgreement" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">


                     <Form className="plan-agreement-upload">
                        
                           <div className="table-responsive mb-40 pymt-history review-app-info">
                           <h3 className="app-review-info">Information</h3>
                           <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Application No : <span>{(this.props.planDetails) ? this.props.planDetails[0].application_no : '-'}</span></th>
                                       <th colSpan="2">Approved Amount : <span>${(this.props.planDetails) ? this.props.planDetails[0].approve_amount : '-'}</span></th>
                                       <th>Credit Score : <span>{(this.props.planDetails) ? this.props.planDetails[0].score : '-'}</span></th>
                                    </tr>
                                    <tr>
                                       <th>Plan No : <span>{(this.props.planDetails) ? this.props.planDetails[0].plan_number : '-'}</span></th>
                                       <th>Plan Principal Amount : <span>${(this.props.planDetails) ? this.props.planDetails[0].loan_amount : '-'}</span></th>
                                       <th>Term : <span>{(this.props.planDetails) ? this.props.planDetails[0].term : '-'} months</span></th>
                                       <th>Interest : <span>{(this.props.planDetails) ? this.props.planDetails[0].discounted_interest_rate : '-'}%</span></th>
                                    </tr>
                                 </thead>
                              </table>
                           </div>
                          
                       
                        <div className="row">
                           <div className="col-md-12">
                              <FormGroup>
                                 <Label for="document">Document Information</Label>
                                 <Input
                                    type="file"
                                    name="document"
                                    id="document"
                                    className="p-5 w-40"
                                    accept="image/gif, image/jpeg, image/png, application/pdf"
                                    onChange={(e) => this.onChnage('document', e)}
                                 >
                                 </Input>
                                 {(this.state.add_err.document) ? <FormHelperText>{this.state.add_err.document}</FormHelperText> : ''}
                                 <FormText color="muted">
                                    Please upload all the required documents for plan.
                                 </FormText>
                              </FormGroup>
                           </div>
                        </div>
                        <hr />


                        <div className="row">

                           <div className="col-md-12">
                              <div className="mps-submt-btn text-right">
                                 <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this)} disabled={!this.validateSubmit()}>Upload</Button>
                              </div>
                           </div>
                        </div>
                     </Form>


                  </div>


               </div>


               {this.props.loading &&
                  <RctSectionLoader />
               }        


            </RctCollapsibleCard>

         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, planDetails, uploadAgreementRedirect } = creditApplication;
   return { loading, planDetails, uploadAgreementRedirect }

}

export default connect(mapStateToProps, {
   planUploadAgreement, uploadAgreementViewDetails
})(uploadAgreementDocument);