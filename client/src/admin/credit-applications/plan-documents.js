/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { URL } from '../../apifile/URL';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import Button from '@material-ui/core/Button';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { Form, FormGroup, Label, Input } from 'reactstrap';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DeleteDocConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteDocConfirmationDialog';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import {
  applicationDoc, downloadOneDrive, resendApplicationDoc, downloadDoc, applicationDiskcleanup, planDoc, viewDisk, verifyPlanAction
} from 'Actions';
class AllDocumentsApplication extends Component {

  state = {
    all: false,
    users: null, // initial user data
    opnDocFileModal: false,
    imgPath: '',
    imgPlanNumber: '',
    imgItemId: '',
    loading: false,

    selectedDocCount: 0,
    appDocumentsList: null,
    selectedDoc: null,

  }

  onSelectInvoice(doc) {
    doc.checked = !doc.checked;
    let selectedDocCount = 0;
    let docObj = [];
    let appDocumentsList = this.state.appDocumentsList.map(docData => {
      if (docData.item_id === doc.item_id) {
        if (docData.checked && docData.checked !== undefined) {
          selectedDocCount++;

          docObj.push({ 'name': docData.name, 'item_id': docData.item_id })
          //selectedDoc[docData.name] = docData.item_id
        }
        return doc;
      } else {
        if (docData.checked && docData.checked !== undefined) {
          selectedDocCount++;

          docObj.push({ 'name': docData.name, 'item_id': docData.item_id })
        }
        return docData;
      }
    });

    this.setState({ appDocumentsList, selectedDocCount, selectedDoc: docObj });
  }

  componentWillReceiveProps(nextProps) {
    (nextProps.allDocuments) ? this.setState({ appDocumentsList: nextProps.allDocuments }) : '';
    (nextProps.docStatus && nextProps.docStatus !== undefined && nextProps.docStatus.status == 1) ? this.setState({ loading: false }) : '';
  }
  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    this.props.planDoc(this.props.match.params.id);
  }

  opnDocFileModal(item_id, plan_number) {
    this.props.downloadOneDrive(item_id)
    this.setState({ opnDocFileModal: true, imgPlanNumber: plan_number, imgItemId: item_id })
  }

  opnViewDocFileModalClose = () => {
    this.setState({ opnDocFileModal: false })
  }

  sendDoc() {
    let app_id = this.props.allDocuments[0].application_id;
    this.props.resendApplicationDoc(app_id, this.state.selectedDoc)
    this.setState({ loading: true })
  }
  verifyPlan(){
    this.refs.deleteConfirmationDialog.open();
  }
  verifyPlanAction(){
    this.props.verifyPlanAction(this.props.match.params.id);
    this.refs.deleteConfirmationDialog.close();
  }
  cleanDisk() {
    this.refs.DeleteDocConfirmationDialog.open();
  }
  viewDisk() {
    this.props.viewDisk(this.props.match.params.id);
  }
  processCleanDisk() {
    this.props.applicationDiskcleanup(this.props.match.params.id);
    this.refs.DeleteDocConfirmationDialog.close();
  }

  validateEmail() {
    if (this.state.selectedDoc != null) {
      if (this.state.selectedDoc.length > 0) {
        return true;
      }
    }
  }

  downloadDoc() {
    this.props.downloadDoc(this.state.imgItemId)
    //return false;
  }
  goBack() {
    this.props.history.goBack(-1)
  }

  render() {
    const { loading, allDocuments, selectedDocCount } = this.props;

    let diableStatus = 0;
    if (allDocuments !== undefined) {
      diableStatus = allDocuments.reduce(function (accumulator, currentValue) {
        if (currentValue.checked === true) {
          accumulator++
        }
        return accumulator;
      }, 0);
    }
    return (
      <div className="all-documents">
        <Helmet>
          <title>Health Partner | Admin | Credit Application | All Documents</title>
          <meta name="description" content="All Documents" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.allDocuments" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          <div className="table-responsive">
            <div className="modal-body page-form-outer view-section">
              <h4>Application Documents</h4>
              <div className="row">
                {allDocuments && allDocuments.map((img, idx) => (
                  <div key={idx} className="col-sm-6 col-md-4 col-lg-4 col-xl-3">

                    <div className="col-md-12">
                      <FormControlLabel className="check_doc"
                        key={idx}
                        control={
                          <Checkbox
                            checked={(img.checked) ? true : false}
                            onChange={() => this.onSelectInvoice(img)}
                            color="primary"
                          />
                        }
                      />
                    </div>

                    <figure className="img-wrapper border border-secondary" onClick={() => this.opnDocFileModal(img.item_id, img.plan_number)}>

                      <img src={`${img.file_path}`} width="250" height="400" />
                      {/*(() => {
                            var docType = img.file_path.split(".");
                            if (docType[docType.length - 1] == 'pdf' || docType[docType.length - 1] == 'png' || docType[docType.length - 1] == 'jpg' || docType[docType.length - 1] == 'jpeg') {
                              return (
                                  <embed src={`${URL.APIURL + '/' + img.file_path}`} width="500px" height="400px" />
                              )
                            } else {
                              return (
                                  <div>Preview not available <a href={`${URL.APIURL + '/' + img.file_path}`}>click here</a> to download </div>
                              )
                            }

                        })()*/}

                      <figcaption>
                        <h4>View and Download</h4>
                      </figcaption>
                      <a href="javascript:void(0);">&nbsp;</a>
                    </figure>

                  </div>
                ))}


              </div>

              <div className="float-right">

                <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={() => this.verifyPlan()} disabled={(allDocuments && allDocuments[0].verify_doc==1)?true:false} >
                  Verify Plan
                </Button>
                <Button variant="contained" color="primary" className="text-white mr-10 mb-10" >
                  <Link className="text-white" to={`/admin/credit-application/view-disk/${this.props.match.params.id}`}>
                    View Local Disk
                </Link>
                </Button>

                <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={() => this.sendDoc()} disabled={!this.validateEmail()} >
                  Email
                  </Button>
                <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.goBack.bind(this)} >
                  Cancel
                  </Button>
              </div>

            </div>
          </div>
          <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are all plan documents correct and complete?"
               //message="Transaction cannot be rolled back."
               onConfirm={() => this.verifyPlanAction()}
            />
          <DeleteDocConfirmationDialog
            ref="DeleteDocConfirmationDialog"
            title="Confirm disk cleanup files?"
            message="Type 'delete' in the text box."
            typeKey="delete"
            onConfirm={() => this.processCleanDisk()}
          />
          <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

            <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
              <span className="float-left>">{(this.state.imgPlanNumber) ? 'Plan Number: ' + this.state.imgPlanNumber : ''}</span>
              <span className="float-right"><a href="javascript:void(0)" onClick={() => this.downloadDoc()} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span>
            </ModalHeader>

            <ModalBody>
              {this.props.oneDrivePreview &&
                <embed src={this.props.oneDrivePreview} width="100%" height="500" download />

              }
              {this.props.loading &&
                <div className="p-50 text-center">Loading, Please Wait...</div>
              }
              {
                this.props.doc_loading &&
                <RctSectionLoader />
              }

            </ModalBody>

          </Modal>

          {this.state.loading &&
            <RctSectionLoader />
          }
          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

      </div >
    );
  }
}
const mapStateToProps = ({ creditApplication }) => {
  const { loading, allDocuments, oneDrivePreview, docStatus, doc_loading, planDocuments } = creditApplication;

  return { loading, allDocuments, oneDrivePreview, docStatus, doc_loading, planDocuments }
}
export default connect(mapStateToProps, {
  applicationDoc, downloadOneDrive, resendApplicationDoc, downloadDoc, applicationDiskcleanup, planDoc, viewDisk, verifyPlanAction
})(AllDocumentsApplication);