/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge,
  Collapse
} from 'reactstrap';
import { Link } from 'react-router-dom';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IconButton from '@material-ui/core/IconButton';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import UnlockConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

// update user form
import UpdateUserForm from './expire-date/UpdateUserForm';
import UpdateAmountForm from './overide-amount/UpdateAmountForm';
import { NotificationManager } from 'react-notifications';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
  viewApplication, updateAppExpireDate, updateOverrideAmount, unlockApplication, createSupportTicket, getAllTickets, experianCIR
} from 'Actions';

import AddCallLogButton from './support-history/AddCallLogButton';
import AddCallLogForm from './support-history/AddCallLogForm';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { isEmpty, isMaster, isNumeric, isObjectEmpty } from '../../validator/Validator';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

// For Tab Content
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}
class ApplicationView extends Component {

  state = {
    activeTab: this.props.location.state ? this.props.location.state.activeTab : 0,
    showSsn: false,
    all: false,
    users: null, // initial user data
    addNewUserModal: false, // add new user form modal
    addNewAmountModal: false, // add new user form modal
    viewExperionModel: false,
    startDate: '',
    editUser: null,

    err: {
      expiry_date: '',
      expiry_date_cmt: ''
    },
    exp_date: {
      application_id: '',
      expiry_date: '',
      expiry_date_cmt: ''
    },

    amt_err: {
      overide_amount: '',
      overide_amount_cmt: ''
    },
    ovr_amt: {
      application_id: '',
      overide_amount: '',
      overide_amount_cmt: ''
    },
    ovr_amt_selected: '',
    ovr_amt_selected_cmt: '',

    ///Support History///
    addCallLogModal: false,
    addCallLogDetail: {
      ticket_source: 1,
      call_type: 'Yes',
      ticket_related_ac: 0,
      ticket_related_plan: 0,
      ticket_related_invoice: 0,
      account_no: '',
      plan_no: '',
      invoice_no: '',
      caller_name: '',
      call_date_time: '',
      subject: '',
      hps_agent_name: '',
      description: '',
      follow_up: 0,
      follow_up_date_time: '',
      invoice_check: true
    },
    view_ticket: false,
    ticket_number: '',
    application_id: '',
    add_err: {},
    dateTimeStartDate: '',
    followUpDateTimeStartDate: '',
    invoiceGroup: null,

  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let { updateForm } = this.state;
    updateForm[name] = value;

    this.setState({
      updateForm: updateForm
    }, function () {
      this.onUpdateUserDetails(name, value)
      this.onUpdateAmountDetails(name, value)
    });
  }
  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    var data = {
      appid: this.props.match.params.id,
      filter_by: 1 // for filter in node file
    }
    this.props.viewApplication(this.props.match.params.id);
    this.props.getAllTickets(data);

  }

  onEditUser(expiry_date, expiry_date_cmt) {

    let { exp_date } = this.state;
    exp_date.expiry_date = expiry_date;
    exp_date.expiry_date_cmt = expiry_date_cmt;
    exp_date.application_id = this.props.match.params.id
    this.setState({ addNewUserModal: true, exp_date: exp_date, startDate: new Date(expiry_date) });

  }

  onAddUpdateUserModalClose() {
    let { exp_date, startDate } = this.state;
    //exp_date.expiry_date = moment(startDate).format('MM/DD/YYYY');
    this.setState({ addNewUserModal: false })
  }

  onUpdateUserDetails(fieldName, value) {

    let { err } = this.state;
    switch (fieldName) {

      case 'expiry_date':
        if (value == null) {
          err[fieldName] = "Select Expiry Date";
          this.setState({ startDate: '' })
        } else {
          this.setState({ startDate: value })
          value = moment(value).format('YYYY-MM-DD');
          err[fieldName] = '';
        }
        break;
      case 'expiry_date_cmt':
        if (value == '') {
          err[fieldName] = '';
        }
        break;
      default:
        break;

    }

    this.setState({ err: err });

    this.setState({
      exp_date: {
        ...this.state.exp_date,
        [fieldName]: value
      }
    });
  }

  updateUser() {
    const { exp_date } = this.state;


    exp_date.expiry_date = moment(exp_date.expiry_date).format('YYYY-MM-DD');
    //return false;
    this.props.updateAppExpireDate(exp_date);
    //this.onEditUser(exp_date.expiry_date)
    let self = this;
    setTimeout(() => {
      this.setState({ loading: true, addNewUserModal: false });
      NotificationManager.success('Date Updated!');
      self.setState({ loading: false });
    }, 2000);
  }

  /* componentWillReceiveProps(nextProps) {
       let { exp_date } = this.state;
 
       if(nextProps.appDetails) {
           exp_date.expiry_date = nextProps.appDetails.expiry_date;
       }
 
       this.setState({exp_date: exp_date})
 
   }*/

  onEditAmount(ov_amt, ov_amt_cmt) {
    let { ovr_amt } = this.state;
    if (ov_amt_cmt == null) {
      ov_amt_cmt = ''
    }
    ovr_amt.application_id = this.props.match.params.id

    this.setState({ addNewAmountModal: true, ovr_amt: ovr_amt, ovr_amt_selected: ov_amt, ovr_amt_selected_cmt: ov_amt_cmt });
  }

  onAddUpdateAmountModalClose() {
    this.setState({ addNewAmountModal: false })
  }

  onUpdateAmountDetails(fieldName, value) {

    let { amt_err } = this.state;
    switch (fieldName) {
      case 'overide_amount':
        if (value == '') {
          amt_err[fieldName] = "Amount can't be blank";
          this.setState({ ovr_amt_selected: '' })
        } else {
          this.setState({ ovr_amt_selected: value })
          amt_err[fieldName] = '';
        }
        break;
      case 'overide_amount_cmt':
        if (value == '') {
          this.setState({ ovr_amt_selected_cmt: '' })
        } else {
          this.setState({ ovr_amt_selected_cmt: value })
        }
        break;
      default:
        break;

    }

    this.setState({ amt_err: amt_err });

    this.setState({
      ovr_amt: {
        ...this.state.ovr_amt,
        [fieldName]: value
      }
    });

  }

  updateAmount() {
    const { ovr_amt } = this.state;


    //return false;

    this.props.updateOverrideAmount(ovr_amt);
    //this.onEditUser(exp_date.expiry_date)
    let self = this;
    setTimeout(() => {
      this.setState({ loading: true, addNewAmountModal: false });
      NotificationManager.success('Amount Updated!');
      self.setState({ loading: false });
    }, 2000);
  }

  validateOverideAmount() {
    return (
      this.state.amt_err.overide_amount === ''
    )
  }

  onUnlockApplication() {
    this.refs.unlockConfirmationDialog.open();
  }
  /*
  * View experion details
  */
  viewExperionData() {
    this.setState({ viewExperionModel: true })
  }
  viewExperionDataClose() {
    this.setState({ viewExperionModel: false })
  }
  unlockApplication() {

    this.props.unlockApplication(this.props.match.params.id);
    this.refs.unlockConfirmationDialog.close();
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  handleClickShowSsn = () => {
    this.setState({ showSsn: !this.state.showSsn });
  };

  ////Support History////
  opnAddCallLogModal() {
    this.setState({ addCallLogModal: true });
  }
  onAddUpdateCallLogModalClose = () => {
    let addR = {}
    let addCallLogDetail = {
      ticket_source: 1,
      call_type: "Yes",
      ticket_related_ac: 0,
      ticket_related_plan: 0,
      ticket_related_invoice: 0,
      account_no: '',
      plan_no: '',
      invoice_no: '',
      caller_name: '',
      call_date_time: '',
      subject: '',
      hps_agent_name: '',
      description: '',
      follow_up: 0,
      follow_up_date_time: ''
    };
    this.setState({ addCallLogModal: false, add_err: addR, addCallLogDetail: addCallLogDetail, view_ticket: false, ticket_number: '' })
  }
  addCallLogView() {
    const min = 1;
    const max = 100000000;
    const rand = Math.floor(Math.random() * (max - min + 1) + min)

    this.setState({ view_ticket: true, ticket_number: rand })
  }
  addCallLogViewBack() {
    this.setState({ view_ticket: false })
  }
  addCallLog() {
    this.state.addCallLogDetail.ticket_number = this.state.ticket_number
    this.state.addCallLogDetail.application_id = this.props.match.params.id
    this.state.addCallLogDetail.customer_id = this.props.appDetails.patient_id
    this.state.addCallLogDetail.commented_by = 1 // by admin
    //return false;
    //insertTermMonth
    this.props.createSupportTicket(this.state.addCallLogDetail);


    this.setState({ addCallLogModal: false, loading: true });
    let self = this;
    let log = {
      ticket_source: 1,
      call_type: "Yes",
      ticket_related_ac: 0,
      ticket_related_plan: 0,
      ticket_related_invoice: 0,
      account_no: '',
      plan_no: '',
      invoice_no: '',
      caller_name: '',
      call_date_time: '',
      subject: '',
      hps_agent_name: '',
      description: '',
      follow_up: 0,
      follow_up_date_time: '',
      view_ticket: false,
      ticket_number: ''
    }

    setTimeout(() => {
      self.setState({ loading: false, addCallLogDetail: log, view_ticket: false, ticket_number: '', dateTimeStartDate: '', followUpDateTimeStartDate: '' });
      NotificationManager.success('Ticket Created!');
    }, 2000);

  }
  onChangeAddCallLogDetails(key, value) {
    let { add_err, addCallLogDetail } = this.state;
    switch (key) {
      case 'ticket_source':

        break;
      case 'call_type':

        break;
      case 'ticket_related_ac':
        value = (this.state.addCallLogDetail.ticket_related_ac) ? 0 : 1;

        break;
      case 'ticket_related_app':
        value = (this.state.addCallLogDetail.ticket_related_app) ? 0 : 1;

        break;
      case 'ticket_related_plan':
        value = (this.state.addCallLogDetail.ticket_related_plan) ? 0 : 1;
        if (value == 0) {
          addCallLogDetail['plan_no'] = ''
          addCallLogDetail['ticket_related_invoice'] = 0
          addCallLogDetail['invoice_check'] = true
          this.setState({ invoiceGroup: null })
        } else {
          addCallLogDetail['invoice_check'] = false
        }
        add_err['plan_no'] = 'Select Plan No.'
        break;
      case 'ticket_related_invoice':
        value = (this.state.addCallLogDetail.ticket_related_invoice) ? 0 : 1;
        if (value == 0) {
          addCallLogDetail['invoice_no'] = ''
          this.setState({ invoiceGroup: null })
        } else {
          addCallLogDetail['plan_no'] = ''
          add_err['plan_no'] = 'Select Plan No.'
        }
        add_err['invoice_no'] = 'Select Invoice No.'
        break;
      case 'plan_no':
        if (isEmpty(value)) {
          add_err[key] = "Select Plan No.";
          add_err['invoice_no'] = 'Select Invoice No.'
          this.setState({ invoiceGroup: null })
        } else {
          add_err[key] = '';
          addCallLogDetail['invoice_no'] = ''
          add_err['invoice_no'] = 'Select Invoice No.'
          var planInvoice = this.props.customerInvoice.filter(x => x.pp_id == value)
          this.setState({ invoiceGroup: planInvoice })
        }
        break;
      case 'invoice_no':
        if (isEmpty(value)) {
          add_err[key] = "Select Invoice No.";
        } else {
          add_err[key] = '';
        }
        break;
      case 'caller_name':
        if (isEmpty(value)) {
          add_err[key] = "Caller Name can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'call_date_time':
        if (value == null) {
          add_err[key] = "Select Date";
        } else {
          this.setState({ dateTimeStartDate: value })
          value = moment(value).format('YYYY-MM-DD h:mm:ss');
          add_err[key] = '';
        }
        break;
      case 'subject':
        if (isEmpty(value)) {
          add_err[key] = "Subject can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'hps_agent_name':
        if (isEmpty(value)) {
          add_err[key] = "HPS Agent Name can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'description':
        if (isEmpty(value)) {
          add_err[key] = "Description can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'follow_up':
        value = (this.state.addCallLogDetail.follow_up) ? 0 : 1;
        if (value == 0) {
          addCallLogDetail['follow_up_date_time'] = ''
          this.setState({ followUpDateTimeStartDate: '' })
        }
        add_err['follow_up_date_time'] = 'Select date'
        break;
      case 'follow_up_date_time':
        if (value == null) {
          add_err[key] = "Select Date";
        } else {
          this.setState({ followUpDateTimeStartDate: value })
          value = moment(value).format('YYYY-MM-DD h:mm:ss');
          add_err[key] = '';
        }
        break;
      default:
        break;
    }

    this.setState({ add_err: add_err });
    this.setState({
      addCallLogDetail: {
        ...this.state.addCallLogDetail,
        [key]: value
      }
    });
  }
  validateAddCallLogSubmit() {
    let { addCallLogDetail } = this.state
    var common = true;

    common = (
      this.state.add_err.caller_name === '' &&
      this.state.add_err.call_date_time === '' &&
      this.state.add_err.subject === '' &&
      this.state.add_err.hps_agent_name === '' &&
      this.state.add_err.description === ''
    ) ? true : false

    var count = 0;
    if (addCallLogDetail.ticket_related_ac == 1) {
      count++
    }
    if (addCallLogDetail.ticket_related_app == 1) {
      count++
    }
    if (addCallLogDetail.ticket_related_plan == 1) {
      var plan_select = (this.state.add_err.plan_no === '') ? true : false
      count++
    } else {
      var plan_select = true
    }
    if (addCallLogDetail.ticket_related_invoice == 1) {
      var invoice_select = (this.state.add_err.invoice_no === '') ? true : false
      count++
    } else {
      var invoice_select = true
    }
    if (addCallLogDetail.follow_up == 1) {
      var follow_up_select = (this.state.add_err.follow_up_date_time === '') ? true : false
    } else {
      var follow_up_select = true
    }

    return (count >= 1 && common == true && plan_select == true && invoice_select == true && follow_up_select == true) ? true : false;




  }
  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let { updateForm } = this.state;
    updateForm[name] = value;

    this.setState({
      updateForm: updateForm
    }, function () {
      this.onUpdateUserDetails(name, value)
    });
  }
  /**
   * This function call experian CIR details
   */
  experianCIR() {
    var promise = new Promise(function (resolve, reject) {
      // call resolve if the method succeeds
      this.props.experianCIR(this.props.match.params.id);
      resolve(true);
    }.bind(this))
    promise.then((bool) => {
      this.props.viewApplication(this.props.match.params.id);
    })
  }
  handleChange = (event, value) => {
    this.setState({ activeTab: value });
  }
  /****Collpase secondary contact details****/
  onCollapse() {
    this.setState({ collapse_sec: !this.state.collapse_sec });
  }
  onCollapseCusInfo() {
    this.setState({ collapse_cus_info: !this.state.collapse_cus_info });
  }
  onCollapseappInfo() {
    this.setState({ collapse_app_info: !this.state.collapse_app_info });
  }
  onCollapseAddInfo() {
    this.setState({ collapse_add: !this.state.collapse_add });
  }
  onCollapseEmpInfo() {
    this.setState({ collapse_emp_info: !this.state.collapse_emp_info });
  }
  onCollapseBankInfo() {
    this.setState({ collapse_bank_info: !this.state.collapse_bank_info });
  }
  onCollapseUserInfo() {
    this.setState({ collapse_user_info: !this.state.collapse_user_info });
  }


  /****Collpase secondary contact details****/
  render() {
    const { loading, appDetails, appDetailsAddress } = this.props;
    const { err, amt_err, activeTab } = this.state;

    /////Support History/////
    const supportColumns = [
      {
        name: 'Ticket ID',
        field: 'ticket_id'
      },
      {
        name: 'Call Date/Time',
        field: 'call_date_time'
      },
      {
        name: 'Type(email/ph)',
        field: 'ticket_source',
      },
      {
        name: 'App No.',
        field: 'application_no',
        options: {
          customBodyRender: (value) => {
            if (value.application_no && appDetails) {
              return appDetails.application_no
            } else {
              return '-'
            }
          }
        }
      },
      {
        name: 'Plan No.',
        field: 'plan_number',
        options: {
          customBodyRender: (value) => {
            if (value.plan_number && this.props.customerPlan) {
              var ppnp = this.props.customerPlan.filter(x => x.pp_id == value.plan_number)
              return ppnp[0].plan_number
            } else {
              return '-'
            }
          }
        }
      },
      {
        name: 'Invoice No.',
        field: 'invoice_number',
        options: {
          customBodyRender: (value) => {
            if (value.invoice_number && this.props.customerInvoice) {
              var invo = this.props.customerInvoice.filter(x => x.invoice_id == value.invoice_number)

              return invo[0].invoice_number
            } else {
              return '-'
            }
          }
        }
      },
      {
        name: 'Caller Name',
        field: 'caller_name',
      },
      {
        name: 'Subject',
        field: 'subject',
      },
      {
        name: 'Status',
        field: 'status',
      },
      {
        name: 'Action',
        field: 'ticket_id',
        options: {
          noHeaderWrap: true,
          filter: false,
          sort: false,
          download: false,
          customBodyRender: (value, tableMeta, updateValue) => {

            return (
              <React.Fragment>
                <span className="list-action">
                  <Link to={`/admin/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                </span>
              </React.Fragment>
            )
          },

        }
      }

    ];

    const supportOptions = {
      filterType: 'dropdown',
      selectableRows: false,
      customToolbar: () => {
        return (
          <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
        );
      }
    };
    const myTheme = createMuiTheme({
      overrides: {
        MaterialDatatableToolbar: {
          root: { display: "none" }
        },
        MuiTableCell: {
          footer: { padding: "4px 8px 4px 8px" }
        },
        MuiPaper: {
          root: { boxShadow: "none !important" }
        },
        MuiTabs: {
          "scroller": { "overflow-x": 'auto !important' }
        }
      }
    });
    const userAllowS = JSON.parse(this.props.user);

    return (
      <div className="country regions">
        <Helmet>
          <title>Health Partner | Providers | Add New</title>
          <meta name="description" content="Regions" />
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.viewApplication" />}
          match={this.props.match}
        />


        <RctCollapsibleCard fullBlock>
          {appDetails &&
            <div className="table-responsive">
              <div className="ovride-action-btn mt-5 text-right mr-15">
                {(appDetails.application_status != 1 && appDetails.application_status != 0 && appDetails.application_status != 6) &&
                  <div className="d-inline">
                    <span className="overide-btn oe-btn" onClick={this.experianCIR.bind(this)}><i className="zmdi zmdi-refresh-alt"></i> Experian CIR</span>

                    <span className=" mr-10  ml-10"> / </span>
                  </div>
                }
                {(appDetails.application_status == 1 || appDetails.application_status == 6) &&
                  <div className="d-inline">

                    <span className="overide-btn oe-btn" onClick={() => this.onEditAmount(appDetails.override_amount, appDetails.override_amount_cmt)}><i className="zmdi zmdi-money"></i> Override Amount</span>

                    <span className=" mr-10  ml-10"> / </span>

                    <span className="expire-btn oe-btn" onClick={() => this.onEditUser(appDetails.expiry_date, appDetails.expiry_date_cmt)}><i className="zmdi zmdi-calendar-alt"></i> Expiry Date</span>
                    <span className=" mr-10  ml-10"> / </span>
                  </div>
                }
                {appDetails.application_status == 6 &&
                  <div className="d-inline">
                    <span className="expire-btn oe-btn" onClick={() => this.onUnlockApplication()}><i className="zmdi zmdi-lock-open"></i> Unlock Application</span>
                    <span className=" mr-10  ml-10"> / </span>
                  </div>
                }
                <div className="d-inline">
                  <span className="expire-btn oe-btn" onClick={() => this.viewExperionData()} title="Back"><i className="ti-eye"></i> View Experian Details</span>
                  <span className=" mr-10  ml-10"> / </span>
                </div>
                <div className="d-inline">
                  <span className="expire-btn oe-btn" onClick={this.goBack.bind(this)} title="Back"><i className="zmdi zmdi-arrow-left"></i></span>
                </div>
              </div>
              <div className="rct-tabs">
                <MuiThemeProvider theme={myTheme}>
                  <AppBar position="static">
                    <Tabs
                      value={activeTab}
                      onChange={this.handleChange}
                      variant="scrollable"
                      scrollButtons="off"
                      indicatorColor="primary"
                    >
                      <Tab
                        label="Application Details"
                      />
                      <Tab
                        label="Co-signer Details"
                      />
                    </Tabs>
                  </AppBar>
                </MuiThemeProvider>
                {activeTab === 0 &&
                  <TabContainer>
                    <div className="row">
                      <div className="col-sm-12 w-xs-full">
                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Customer Information</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseCusInfo()}><i className={(this.state.collapse_cus_info) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_cus_info}>
                            <div className="rct-block-content">
                              <div className="width-100">
                                <table className="table">
                                  <tbody>
                                    <tr>
                                      <td className="fw-bold">First Name:</td>
                                      <td>{appDetails.f_name}</td>
                                      <td className="fw-bold">Status:</td>
                                      <td>{(appDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Middle Name:</td>
                                      <td>{(appDetails.m_name != '') ? appDetails.m_name : '-'}</td>
                                      <td className="fw-bold">Gender:</td>
                                      <td>{(appDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Last Name:</td>
                                      <td>{appDetails.l_name}</td>
                                      <td className="fw-bold">SSN / Tax ID:</td>
                                      <td>
                                        {
                                          (userAllowS.role_name == 'SuperAdmin') ?
                                            <React.Fragment>
                                              {(appDetails.ssn !== undefined) ? (!this.state.showSsn) ? appDetails.ssn.replace(/.(?=.{4})/g, 'x') : appDetails.ssn : '-'}

                                              <IconButton
                                                className="eye-mask"
                                                onClick={this.handleClickShowSsn.bind(this)}
                                              >
                                                {!this.state.showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                                              </IconButton>
                                            </React.Fragment>
                                            :
                                            'xxxxxxxxxxx'
                                        }
                                      </td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Date of Birth:</td>
                                      <td>{appDetails.dob}</td>
                                      <td className="fw-bold">Application No</td>
                                      <td>{appDetails.application_no}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Primary Phone No.:</td>
                                      <td>{appDetails.peimary_phone}</td>
                                      <td className="fw-bold">Customer A/C:</td>
                                      <td>{appDetails.patient_ac}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Secondary Phone No.:</td>
                                      <td>{(appDetails.alternative_phone) ? appDetails.alternative_phone : '-'}</td>
                                      <td className="fw-bold">Primary Email Address:</td>
                                      <td>{appDetails.email}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Withdrawal Day:</td>
                                      <td>{(appDetails.withdrawal_date) ? appDetails.withdrawal_date : '-'}</td>
                                      <td className="fw-bold">Secondary Email Address:</td>
                                      <td>{(appDetails.secondary_email) ? appDetails.secondary_email : '-'}</td>
                                    </tr>
                                  </tbody>

                                </table>
                              </div>
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Application Information</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseappInfo()}><i className={(this.state.collapse_app_info) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_app_info}>
                            <div className="rct-block-content">
                              <div className="width-100">
                                <table className="table">
                                  <tbody>
                                    <tr>
                                      <td className="fw-bold">Application No:</td>
                                      <td>{appDetails.application_no}</td>
                                      <td className="fw-bold">Credit Score:</td>
                                      <td>{(appDetails.score) ? appDetails.score : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Authorization date:</td>
                                      <td>{(appDetails.application_status == 1) ? appDetails.authorization_date : '-'}</td>
                                      <td className="fw-bold">Authorization expiration:</td>
                                      <td>{(appDetails.expiry_date) ? appDetails.expiry_date : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Amount Approved:</td>
                                      <td>{(appDetails.application_status == 1) ? '$' + appDetails.approve_amount.toFixed(2) : '-'}</td>
                                      <td className="fw-bold">Expiration Comment:</td>
                                      <td>{(appDetails.expiry_date_cmt) ? appDetails.expiry_date_cmt : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Override Amount:</td>
                                      <td>{(appDetails.override_amount !== '') ? '$' + parseFloat(appDetails.override_amount).toFixed(2) : '$0.00'}</td>
                                      <td className="fw-bold">Remaining Amount:</td>
                                      <td>{(appDetails.application_status == 1) ? '$' + (parseFloat(appDetails.remaining_amount) + parseFloat(appDetails.override_amount)).toFixed(2) : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Override Amount Comment:</td>
                                      <td>{(appDetails.override_amount_cmt !== '') ? appDetails.override_amount_cmt : '-'}</td>
                                      <td className="fw-bold">Experian Status</td>
                                      <td>{(appDetails.experian_status) ? 'Record found' : 'Record not found'}</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Secondary Contact Details</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapse()}><i className={(this.state.collapse_sec) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_sec}>
                            <div className="rct-block-content">
                              <div className="width-100">
                                <table className="table">
                                  {this.props.secDetails &&
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">First Name:</td>
                                        <td>{this.props.secDetails.f_name}</td>
                                        <td className="fw-bold">Middle Name:</td>
                                        <td>{(this.props.secDetails.m_name != '') ? this.props.secDetails.m_name : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Last Name:</td>
                                        <td>{this.props.secDetails.l_name}</td>
                                        <td className="fw-bold">Relationship:</td>
                                        <td>{this.props.secDetails.relationship_name}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Phone:</td>
                                        <td>{this.props.secDetails.phone}</td>
                                        <td className="fw-bold">Email:</td>
                                        <td>{this.props.secDetails.email}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Address:</td>
                                        <td>{this.props.secDetails.address1 + ' ' + this.props.secDetails.address2}</td>
                                        <td className="fw-bold">City:</td>
                                        <td>{this.props.secDetails.city}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">State:</td>
                                        <td>{this.props.secDetails.state_name}</td>
                                        <td className="fw-bold">Country</td>
                                        <td>{this.props.secDetails.country_name}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Zip Code:</td>
                                        <td>{this.props.secDetails.zip_code}</td>
                                        <td className="fw-bold"></td>
                                        <td></td>
                                      </tr>
                                    </tbody>
                                  }
                                  {(this.props.secDetails == "") &&
                                    <tbody><tr><td colSpan="4">Secondary contact details not available.</td></tr></tbody>
                                  }
                                </table>
                              </div>
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Address</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseAddInfo()}><i className={(this.state.collapse_add) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_add}>
                            <div className="rct-block-content">
                              {appDetailsAddress && appDetailsAddress.map((address, idx) => (
                                <div className="width-100 mb-10" key={idx}>
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">Address1:</td>
                                        <td>{address.address1}</td>
                                        <td className="fw-bold">City:</td>
                                        <td>{address.city}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Address2:</td>
                                        <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                        <td className="fw-bold">Zip Code:</td>
                                        <td>{address.zip_code}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Country:</td>
                                        <td>{address.country_name}</td>
                                        <td className="fw-bold">How long at this address?:</td>
                                        <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">State:</td>
                                        <td>{address.state_name}</td>
                                        <td className="fw-bold">Phone No:</td>
                                        <td>{address.phone_no}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Primary Address:</td>
                                        <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                        <td className="fw-bold">Billing and Physical address same:</td>
                                        <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <hr className="border-dark" />
                                </div>
                              ))
                              }
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Employment Information</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseEmpInfo()}><i className={(this.state.collapse_emp_info) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_emp_info}>
                            <div className="rct-block-content">
                              <div className="width-100">
                                <table className="table">
                                  <tbody>
                                    <tr>
                                      <td className="fw-bold">Employed:</td>
                                      <td>{(appDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                                      <td className="fw-bold">Employer Name:</td>
                                      <td>{(appDetails.employment_status == 1) ? appDetails.employer_name : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Employment Type:</td>
                                      <td>{(appDetails.employment_status == 1) ? appDetails.emp_type_name : '-'}</td>
                                      <td className="fw-bold">Employer Phone No:</td>
                                      <td>{(appDetails.employment_status == 1) ? appDetails.employer_phone : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Annual Income($):</td>
                                      <td>{(appDetails.employment_status == 1) ? '$' + appDetails.annual_income : '-'}</td>
                                      <td className="fw-bold">Employer Email Address:</td>
                                      <td>{(appDetails.employment_status == 1) ? appDetails.employer_email : '-'}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Employed Since:</td>
                                      <td>{(appDetails.employment_status == 1) ? appDetails.employer_since : '-'}</td>
                                      <td className="fw-bold">&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>Bank Details</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseBankInfo()}><i className={(this.state.collapse_bank_info) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_bank_info}>
                            <div className="rct-block-content">
                              {this.props.bankDetails && this.props.bankDetails.map((bank, idx) => (
                                <div className="width-100" key={idx}>
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">Bank Name:</td>
                                        <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                                        <td className="fw-bold">Name on Account:</td>
                                        <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Routing Number:</td>
                                        <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                                        <td className="fw-bold">Bank A/C Type:</td>
                                        <td>{(bank.value) ? bank.value : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Bank A/C#:</td>
                                        <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                                        <td className="fw-bold">Bank Address</td>
                                        <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <hr className="border-dark" />
                                </div>
                              ))
                              }
                            </div>
                          </Collapse>
                        </div>

                        <div className="rct-block custom-collapse">
                          <div className="rct-block-title">
                            <h4><span>User Information</span></h4>
                            <div className="contextual-link">
                              <a href="javascript:void(0)" onClick={() => this.onCollapseUserInfo()}><i className={(this.state.collapse_user_info) ? "ti-minus" : "ti-plus"}></i></a>
                            </div>
                          </div>
                          <Collapse isOpen={this.state.collapse_user_info}>
                            <div className="rct-block-content">
                              <div className="width-100">
                                <table className="table">
                                  <tbody>
                                    <tr>
                                      <td className="fw-bold">Username:</td>
                                      <td>{appDetails.username}</td>
                                      <td className="fw-bold">Primary Phone No.:</td>
                                      <td>{appDetails.user_phone1}</td>
                                    </tr>
                                    <tr>
                                      <td className="fw-bold">Email Address:</td>
                                      <td>{appDetails.user_eamil}</td>
                                      <td className="fw-bold">Secondary Phone No.:</td>
                                      <td>{(appDetails.user_phone2 != '') ? appDetails.user_phone2 : '-'}</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </Collapse>
                        </div>
                      </div>
                    </div>

                  </TabContainer>}
                {activeTab === 1 &&
                  <TabContainer>
                    {this.props.coDetails &&
                      <div className="row">
                        <div className="col-sm-12 w-xs-full">
                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>Customer Information</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseCusInfo()}><i className={(this.state.collapse_cus_info) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_cus_info}>
                              <div className="rct-block-content">
                                <div className="width-100">
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">First Name:</td>
                                        <td>{this.props.CoappDetails.f_name}</td>
                                        <td className="fw-bold">Status:</td>
                                        <td>{(this.props.CoappDetails.status == 1) ? 'Active' : 'Inactive'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Middle Name:</td>
                                        <td>{(this.props.CoappDetails.m_name != '') ? appDetails.m_name : '-'}</td>
                                        <td className="fw-bold">Gender:</td>
                                        <td>{(this.props.CoappDetails.gender == 'M') ? 'Male' : 'Female'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Last Name:</td>
                                        <td>{this.props.CoappDetails.l_name}</td>
                                        <td className="fw-bold">SSN / Tax ID:</td>
                                        <td>
                                          {
                                            (userAllowS.role_name == 'SuperAdmin') ?
                                              <React.Fragment>
                                                {(this.props.CoappDetails.ssn !== undefined) ? (!this.state.showSsn) ? this.props.CoappDetails.ssn.replace(/.(?=.{4})/g, 'x') : this.props.CoappDetails.ssn : '-'}

                                                <IconButton
                                                  className="eye-mask"
                                                  onClick={this.handleClickShowSsn.bind(this)}
                                                >
                                                  {!this.state.showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                                                </IconButton>
                                              </React.Fragment>
                                              :
                                              'xxxxxxxxxxx'
                                          }



                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Date of Birth:</td>
                                        <td>{this.props.CoappDetails.dob}</td>
                                        <td className="fw-bold">Application No</td>
                                        <td>{this.props.CoappDetails.application_no}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Primary Phone No.:</td>
                                        <td>{this.props.CoappDetails.peimary_phone}</td>
                                        <td className="fw-bold">Customer A/C:</td>
                                        <td>{this.props.CoappDetails.patient_ac}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Secondary Phone No.:</td>
                                        <td>{(this.props.CoappDetails.alternative_phone) ? this.props.CoappDetails.alternative_phone : '-'}</td>
                                        <td className="fw-bold">Primary Email Address:</td>
                                        <td>{this.props.CoappDetails.email}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Relationship</td>
                                        <td>{(this.props.coDetails) ? this.props.coDetails.relationship_name : '-'}</td>
                                        <td className="fw-bold">Secondary Email Address:</td>
                                        <td>{(this.props.CoappDetails.secondary_email) ? this.props.CoappDetails.secondary_email : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Primary Bill Payer</td>
                                        <td>{(this.props.coDetails && this.props.coDetails.primary_bill_pay_flag == 1) ? 'Yes' : 'No'}</td>
                                        <td className="fw-bold">Withdrawal Day:</td>
                                      <td>{(this.props.CoappDetails.withdrawal_date) ? this.props.CoappDetails.withdrawal_date : '-'}</td>
                                      </tr>
                                    </tbody>

                                  </table>
                                </div>
                              </div>
                            </Collapse>
                          </div>

                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>Application Information</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseappInfo()}><i className={(this.state.collapse_app_info) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_app_info}>
                              <div className="rct-block-content">
                                <div className="width-100">
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">Application No:</td>
                                        <td>{this.props.CoappDetails.application_no}</td>
                                        <td className="fw-bold">Credit Score:</td>
                                        <td>{(this.props.CoappDetails.score) ? this.props.CoappDetails.score : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Authorization date:</td>
                                        <td>{(this.props.CoappDetails.application_status == 1) ? this.props.CoappDetails.authorization_date : '-'}</td>
                                        <td className="fw-bold">Authorization expiration:</td>
                                        <td>{(this.props.CoappDetails.expiry_date) ? this.props.CoappDetails.expiry_date : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Amount Approved:</td>
                                        <td>{(this.props.CoappDetails.application_status == 1) ? '$' + this.props.CoappDetails.approve_amount.toFixed(2) : '-'}</td>
                                        <td className="fw-bold">Expiration Comment:</td>
                                        <td>{(this.props.CoappDetails.expiry_date_cmt) ? this.props.CoappDetails.expiry_date_cmt : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Override Amount:</td>
                                        <td>{(this.props.CoappDetails.override_amount !== '') ? '$' + parseFloat(this.props.CoappDetails.override_amount).toFixed(2) : '$0.00'}</td>
                                        <td className="fw-bold">Remaining Amount:</td>
                                        <td>{(this.props.CoappDetails.application_status == 1) ? '$' + (parseFloat(this.props.CoappDetails.remaining_amount) + parseFloat(this.props.CoappDetails.override_amount)).toFixed(2) : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Override Amount Comment:</td>
                                        <td>{(this.props.CoappDetails.override_amount_cmt !== '') ? this.props.CoappDetails.override_amount_cmt : '-'}</td>
                                        <td className="fw-bold">Experian Status</td>
                                        <td>{(this.props.CoappDetails.experian_status) ? 'Record found' : 'Record not found'}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </Collapse>
                          </div>

                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>Address</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseAddInfo()}><i className={(this.state.collapse_add) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_add}>
                              <div className="rct-block-content">
                                {this.props.CoappDetailsAddress && this.props.CoappDetailsAddress.map((address, idx) => (
                                  <div className="width-100 mb-10" key={idx}>
                                    <table className="table">
                                      <tbody>
                                        <tr>
                                          <td className="fw-bold">Address1:</td>
                                          <td>{address.address1}</td>
                                          <td className="fw-bold">City:</td>
                                          <td>{address.city}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">Address2:</td>
                                          <td>{(address.address2 != '') ? address.address2 : '-'}</td>
                                          <td className="fw-bold">Zip Code:</td>
                                          <td>{address.zip_code}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">Country:</td>
                                          <td>{address.country_name}</td>
                                          <td className="fw-bold">How long at this address?:</td>
                                          <td>{(address.address_time_period) ? address.address_time_period : '-'}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">State:</td>
                                          <td>{address.state_name}</td>
                                          <td className="fw-bold">Phone No:</td>
                                          <td>{address.phone_no}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">Primary Address:</td>
                                          <td>{(address.primary_address) ? 'Yes' : 'No'}</td>
                                          <td className="fw-bold">Billing and Physical address same:</td>
                                          <td>{(address.billing_address) ? 'Yes' : 'No'}</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <hr className="border-dark" />
                                  </div>
                                ))
                                }
                              </div>
                            </Collapse>
                          </div>

                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>Employment Information</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseEmpInfo()}><i className={(this.state.collapse_emp_info) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_emp_info}>
                              <div className="rct-block-content">
                                <div className="width-100">
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">Employed:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? 'Yes' : 'No'}</td>
                                        <td className="fw-bold">Employer Name:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? this.props.CoappDetails.employer_name : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Employment Type:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? this.props.CoappDetails.emp_type_name : '-'}</td>
                                        <td className="fw-bold">Employer Phone No:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? this.props.CoappDetails.employer_phone : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Annual Income($):</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? '$' + this.props.CoappDetails.annual_income : '-'}</td>
                                        <td className="fw-bold">Employer Email Address:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? this.props.CoappDetails.employer_email : '-'}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Employed Since:</td>
                                        <td>{(this.props.CoappDetails.employment_status == 1) ? this.props.CoappDetails.employer_since : '-'}</td>
                                        <td className="fw-bold">&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </Collapse>
                          </div>

                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>Bank Details</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseBankInfo()}><i className={(this.state.collapse_bank_info) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_bank_info}>
                              <div className="rct-block-content">
                                {this.props.CobankDetails && this.props.CobankDetails.map((bank, idx) => (
                                  <div className="width-100" key={idx}>
                                    <table className="table">
                                      <tbody>
                                        <tr>
                                          <td className="fw-bold">Bank Name:</td>
                                          <td>{(bank.bank_name) ? bank.bank_name : '-'}</td>
                                          <td className="fw-bold">Name on Account:</td>
                                          <td>{(bank.account_name) ? bank.account_name : '-'}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">Routing Number:</td>
                                          <td>{(bank.rounting_no > 0) ? bank.rounting_no : '-'}</td>
                                          <td className="fw-bold">Bank A/C Type:</td>
                                          <td>{(bank.value) ? bank.value : '-'}</td>
                                        </tr>
                                        <tr>
                                          <td className="fw-bold">Bank A/C#:</td>
                                          <td>{(bank.bank_ac) ? bank.bank_ac : '-'}</td>
                                          <td className="fw-bold">Bank Address</td>
                                          <td>{(bank.bank_address != '') ? bank.bank_address : '-'}</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <hr className="border-dark" />
                                  </div>
                                ))
                                }
                              </div>
                            </Collapse>
                          </div>

                          <div className="rct-block custom-collapse">
                            <div className="rct-block-title">
                              <h4><span>User Information</span></h4>
                              <div className="contextual-link">
                                <a href="javascript:void(0)" onClick={() => this.onCollapseUserInfo()}><i className={(this.state.collapse_user_info) ? "ti-minus" : "ti-plus"}></i></a>
                              </div>
                            </div>
                            <Collapse isOpen={this.state.collapse_user_info}>
                              <div className="rct-block-content">
                                <div className="width-100">
                                  <table className="table">
                                    <tbody>
                                      <tr>
                                        <td className="fw-bold">Username:</td>
                                        <td>{this.props.CoappDetails.username}</td>
                                        <td className="fw-bold">Primary Phone No.:</td>
                                        <td>{this.props.CoappDetails.user_phone1}</td>
                                      </tr>
                                      <tr>
                                        <td className="fw-bold">Email Address:</td>
                                        <td>{this.props.CoappDetails.user_eamil}</td>
                                        <td className="fw-bold">Secondary Phone No.:</td>
                                        <td>{(this.props.CoappDetails.user_phone2 != '') ? this.props.CoappDetails.user_phone2 : '-'}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </Collapse>
                          </div>
                        </div>
                      </div>
                    }
                    {(this.props.coDetails == "") &&
                      <div className="row">
                        Co-signer details not available.
                      </div>
                    }
                  </TabContainer>}
              </div>
              <div className="modal-body page-form-outer view-section pt-5">





              </div>
            </div>
          }

          <div className="admin-application-list support-history-container">
            <RctCollapsibleCard
              colClasses="col-md-12 w-xs-full support-history"
              heading="Support History"
              collapsible
              fullBlock
              customClasses="overflow-hidden d-inline-block w-100"
            >
              <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
              <MuiThemeProvider theme={myTheme}>
                <MaterialDatatable
                  data={(this.props.tickets) ? this.props.tickets : ''}
                  columns={supportColumns}
                  options={supportOptions}
                />
              </MuiThemeProvider>
            </RctCollapsibleCard>
          </div>

          {this.props.loading &&
            <RctSectionLoader />
          }
        </RctCollapsibleCard>

        <Modal isOpen={this.state.addCallLogModal} toggle={() => this.onAddUpdateCallLogModalClose()} className="support-history-modal">
          <ModalHeader toggle={() => this.onAddUpdateCallLogModalClose()}>
            Add Call Log
           </ModalHeader>
          <ModalBody>

            <AddCallLogForm
              addErr={this.state.add_err}
              addCallLogDetails={this.state.addCallLogDetail}
              onChangeAddCallLogDetails={this.onChangeAddCallLogDetails.bind(this)}
              allInvoice={this.state.invoiceGroup}
              allPlan={this.props.customerPlan}
              DatePicker={DatePicker}
              dateTimeStartDate={this.state.dateTimeStartDate}
              followUpDateTimeStartDate={this.state.followUpDateTimeStartDate}
              masterTicketSource={this.props.masterTicketSource}
              customerDetails={this.props.appDetails}
            />


          </ModalBody>
          <ModalFooter>

            <Button
              variant="contained"
              color="primary"
              className="text-white"
              onClick={() => this.addCallLog()}
              disabled={!this.validateAddCallLogSubmit()}
            >
              Submit
              </Button>


            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCallLogModalClose()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
          <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>

            Change Expire Date

               </ModalHeader>
          <ModalBody>

            <UpdateUserForm
              updateErr={err}
              updateUserDetails={(this.state.exp_date.expiry_date) ? this.state.exp_date.expiry_date : ''}
              onUpdateUserDetail={this.onUpdateUserDetails.bind(this)}
              DatePicker={DatePicker}
              startDate={this.state.startDate}
              selectedExpDatCmt={this.state.exp_date.expiry_date_cmt}
            />

          </ModalBody>
          <ModalFooter>

            <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateUser()}>Update</Button>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>

          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.addNewAmountModal} toggle={() => this.onAddUpdateAmountModalClose()}>
          <ModalHeader toggle={() => this.onAddUpdateAmountModalClose()}>

            Override Amount

               </ModalHeader>
          <ModalBody>

            <UpdateAmountForm
              updateAmtErr={amt_err}
              updateAmountDetails={(this.state.overide_amount) ? this.state.overide_amount : ''}
              onUpdateAmountDetail={this.onUpdateAmountDetails.bind(this)}
              selectedOvrAmt={this.state.ovr_amt_selected}
              selectedOvrAmtCmt={this.state.ovr_amt_selected_cmt}
            />

          </ModalBody>
          <ModalFooter>

            <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateAmount()} disabled={!this.validateOverideAmount()}>Update</Button>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateAmountModalClose()}>Cancel</Button>

          </ModalFooter>
        </Modal>


        <Modal className="modal-dialog-custom" isOpen={this.state.viewExperionModel} toggle={() => this.viewExperionDataClose()}>
          <ModalHeader toggle={() => this.viewExperionDataClose()}>
            Experian Details
          </ModalHeader>
          <ModalBody>
            <h4><strong>Address Information</strong></h4>
            <table className="table table-sm table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Street Prefix</th>
                  <th>Street Suffix</th>
                  <th>Street Name</th>
                  <th>City</th>
                  <th>State</th>
                  <th>State Code</th>
                  <th>Zip Code</th>
                  <th>First Reported Date</th>
                  <th>Last Updated Date</th>
                  <th>Dwelling Type</th>
                  <th>Source</th>
                </tr>
              </thead>
              <tbody>
                {this.props.experianAddress && this.props.experianAddress.map((add, idx) => (
                  <tr key={idx}>
                    <td>{idx + 1}</td>
                    <td>{(add.streetPrefix != null) ? add.streetPrefix : '-'}</td>
                    <td>{(add.streetSuffix != null) ? add.streetSuffix : '-'}</td>
                    <td>{(add.streetName != null) ? add.streetName : '-'}</td>
                    <td>{(add.city != null) ? add.city : '-'}</td>
                    <td>{(add.state != null) ? add.state : '-'}</td>
                    <td>{(add.stateCode != null) ? add.stateCode : '-'}</td>
                    <td>{(add.zipCode != null) ? add.zipCode : '-'}</td>
                    <td>{(add.firstReportedDate != null) ? moment(add.firstReportedDate, 'MMDDYYYY').format('MM/DD/YYYY') : '-'}</td>
                    <td>{(add.lastUpdatedDate != null) ? moment(add.lastUpdatedDate, 'MMDDYYYY').format('MM/DD/YYYY') : '-'}</td>
                    <td>{(add.dwellingType != null) ? add.dwellingType : '-'}</td>
                    <td>{(add.source != null) ? add.source : '-'}</td>
                  </tr>
                ))}
                {(this.props.experianAddress && this.props.experianAddress.length == 0) &&
                  <tr>
                    <td colSpan="12">Record not found.</td>
                  </tr>
                }
              </tbody>
            </table>
            <h4><strong>Employment Information</strong></h4>
            <table className="table table-sm table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address Extra Line</th>
                  <th>Address First Line</th>
                  <th>Address Second Line</th>
                  <th>First Reported Date</th>
                  <th>Last Updated Date</th>
                  <th>Source</th>
                  <th>Zip Code</th>
                </tr>
              </thead>
              <tbody>
                {this.props.experianEmployment && this.props.experianEmployment.map((emp, idx) => (
                  <tr key={idx}>
                    <td>{idx + 1}</td>
                    <td>{(emp.name != null) ? emp.name : '-'}</td>
                    <td>{(emp.addressExtraLine != null) ? emp.addressExtraLine : '-'}</td>
                    <td>{(emp.addressFirstLine != null) ? emp.addressFirstLine : '-'}</td>
                    <td>{(emp.addressSecondLine != null) ? emp.addressSecondLine : '-'}</td>
                    <td>{(emp.firstReportedDate != null) ? moment(emp.firstReportedDate, 'MMDDYYYY').format('MM/DD/YYYY') : '-'}</td>
                    <td>{(emp.lastUpdatedDate != null) ? moment(emp.lastUpdatedDate, 'MMDDYYYY').format('MM/DD/YYYY') : '-'}</td>
                    <td>{(emp.source != null) ? emp.source : '-'}</td>
                    <td>{(emp.zipCode != null) ? emp.zipCode : '-'}</td>
                  </tr>
                ))}
                {(this.props.experianEmployment && this.props.experianEmployment.length == 0) &&
                  <tr>
                    <td colSpan="9">Record not found.</td>
                  </tr>
                }
              </tbody>
            </table>

          </ModalBody>
          <ModalFooter>
            <Button variant="contained" className="text-white btn-danger" onClick={() => this.viewExperionDataClose()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <UnlockConfirmationDialog
          ref="unlockConfirmationDialog"
          title="Are You Sure Want To Unlock?"
          message="This will unlock user's Application."
          onConfirm={() => this.unlockApplication()}
        />

      </div>
    );
  }
}
const mapStateToProps = ({ creditApplication, CustomerSupportReducer, authUser }) => {
  const { user } = authUser;

  const {
    loading,
    appDetails,
    appDetailsAddress,
    experianEmployment,
    experianAddress,
    secDetails,
    CoappDetails,
    CoappDetailsAddress,
    CoexperianEmployment,
    CoexperianAddress,
    CosecDetails,
    coDetails,
    bankDetails,
    CobankDetails,
  } = creditApplication;
  const { tickets, masterTicketSource, customerPlan, customerInvoice } = CustomerSupportReducer

  return {
    loading,
    appDetails,
    appDetailsAddress,
    tickets,
    masterTicketSource,
    customerPlan,
    customerInvoice,
    experianEmployment,
    experianAddress,
    secDetails,
    CoappDetails,
    CoappDetailsAddress,
    CoexperianEmployment,
    CoexperianAddress,
    CosecDetails,
    coDetails,
    bankDetails,
    CobankDetails,
    user,
  }
}
export default connect(mapStateToProps, {
  viewApplication, updateAppExpireDate, updateOverrideAmount, unlockApplication, createSupportTicket, getAllTickets, experianCIR
})(ApplicationView);