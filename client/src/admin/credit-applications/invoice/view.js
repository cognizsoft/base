/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Form, FormGroup, Label, Input } from 'reactstrap';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import { isEmpty, isNumeric, isLength } from '../../../validator/Validator';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import ReactToPrint from 'react-to-print';
//import { withRouter } from 'react-router-dom';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import {
   getPaymentMasterFeeOption, getLateFeeWaiverType, getFinChargeWaiverType, getSingleInstallment, insertPayPayment, clearRedirectURL, planDetails
} from 'Actions';
class CustomerPaymentBilling extends Component {

   state = {
      calculateAmount: {
         installment_amount: '',
         payment_installment_id: '',

         late_fee_waiver_type: '',
         fin_charge_waiver_type: '',

         late_fee_waiver_reason: '',
         fin_charge_waiver_reason: '',


         late_fee_waiver_comt:'',
         fin_charge_waiver_comt: '',

         late_fee_pct: '',

         waiver: '',
         payment_note: '',

         bank_name: '',
         txn_ref_no: '',
      },
      
      late_fee_max_waiver:'',
      fin_charge_max_waiver:'',

      err: {
         late_fee_waiver_type: '',
         fin_charge_waiver_type: '',

         late_fee_waiver_reason: '',
         fin_charge_waiver_reason: '',

         late_fee_max_waiver:'',
         fin_charge_max_waiver:'',

         late_fee_max_waiver_id: '',
         fin_charge_max_waiver_id: '',

         late_fee_waiver_comt:'',
         fin_charge_waiver_comt: '',

         late_fee_pct: '',

         waiver: '',
         payment_note: '',

         bank_name: '',
         txn_ref_no: '',
      },
      add_err: {
         
      },
      late_fee:0,
      financial_charge: 0,

      final_late_fee: 0,
      final_fin_charge: 0,

      interest_rate:0,
      payment_term:0,
      total_pay:0,
      nextTpay: true,
      payment_method: 1,
      payment_in: null,
      cheque_method_field: true,
      changeURL: 0,
      paid_flag: 0
   }

   validateAddSubmit() {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = dd + '/' + mm + '/' + yyyy;

      //compare
      var lateFeeApplyVali;
      if(this.props.single_installment_payment) {
         //console.log(this.props.single_installment_payment[0].due_date)
         var dv1 = new Date(this.props.single_installment_payment[0].due_date);
         var dv2 = new Date();
         //var d2 = new Date('');
         // console.log(dv1)
         // console.log('----------')
         // console.log(dv2)
         if(dv1<dv2) {
            lateFeeApplyVali = true;
            //console.log('true')
         } else {
            lateFeeApplyVali = false;
            //console.log('false')

         }
      } 

      if(!lateFeeApplyVali && this.state.payment_method == 1 && this.state.payment_in !== null) {
         //console.log('1111')
         return (
            //this.state.add_err.late_fee_waiver_type === '' &&
            //this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.bank_name === '' &&
            this.state.add_err.txn_ref_no === '' &&
            this.state.add_err.payment_in === ''
         );
      } else if(!lateFeeApplyVali && this.state.payment_method !== 1 && this.state.payment_in == null) { 
         //console.log('2222')
         return (
            //this.state.add_err.late_fee_waiver_type === '' &&
            //this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.payment_in === ''
            //this.state.add_err.bank_name === '' &&
            //this.state.add_err.txn_ref_no === ''
         );
      } else if(lateFeeApplyVali && this.state.payment_method == 1 && this.state.payment_in !== null) {
         //console.log('3333')
         //console.log(this.state.add_err)
         return (
            this.state.add_err.late_fee_waiver_type === '' &&
            this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.bank_name === '' &&
            this.state.add_err.txn_ref_no === '' &&
            this.state.add_err.payment_in === ''
         );
      } else if(lateFeeApplyVali && this.state.payment_method !== 1 && this.state.payment_in !== null) { 
         //console.log('4444')
         return (
            this.state.add_err.late_fee_waiver_type === '' &&
            this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.payment_in === ''
            //this.state.add_err.bank_name === '' &&
            //this.state.add_err.txn_ref_no === ''
         );
      } else if(lateFeeApplyVali && this.state.payment_method == 1 && this.state.payment_in == null) { 
         //console.log('5555')
         return (
            this.state.add_err.late_fee_waiver_type === '' &&
            this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.bank_name === '' &&
            this.state.add_err.txn_ref_no === '' &&
            this.state.add_err.payment_in === ''
         );
      } else if(lateFeeApplyVali && this.state.payment_method !== 1 && this.state.payment_in == null) { 
         //console.log('6666')
         return (
            this.state.add_err.late_fee_waiver_type === '' &&
            this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.bank_name === '' 
            //this.state.add_err.txn_ref_no === '' &&
            //this.state.add_err.payment_in === ''
         );
      } else {
         //console.log('else')
//console.log(this.state.payment_in)
         return (
            this.state.add_err.late_fee_waiver_type === '' &&
            this.state.add_err.late_fee_waiver_reason === '' &&
            this.state.add_err.fin_charge_waiver_type === '' &&
            this.state.add_err.fin_charge_waiver_reason === '' &&
            this.state.add_err.payment_note === '' &&
            this.state.add_err.payment_in === ''
         );
      }
   }

   payInstallmentPayment() {

      var data = {
         pp_id: this.props.match.params.planid,
         pp_installment_id: this.props.match.params.instlmntid,
         payment_amount: this.state.calculateAmount.installment_amount,
         late_fee_recieved: (this.state.final_late_fee == 0) ? this.state.late_fee : this.state.final_late_fee,
         late_fee_waiver_id: this.state.calculateAmount.late_fee_waiver_reason,
         fin_charge_waiver_id: this.state.calculateAmount.fin_charge_waiver_reason,
         payment_date: "",
         bank_name: this.state.calculateAmount.bank_name,
         txn_ref_no: this.state.calculateAmount.txn_ref_no,
         fin_charge_amt: (this.state.final_fin_charge == 0) ? this.state.financial_charge : this.state.final_fin_charge,
         late_fee_comt: this.state.calculateAmount.late_fee_waiver_comt,
         finance_charge_commt: this.state.calculateAmount.fin_charge_waiver_comt,
         comments: this.state.calculateAmount.payment_note,
         payment_in: this.state.payment_in,

         //for edit part
         payment_installment_id: this.state.calculateAmount.payment_installment_id,
      }
      //console.log(this.state.calculateAmount.late_fee_waiver_reason)

      this.props.insertPayPayment(data);
   }

   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);
      //console.log("appid "+this.props.match.params.appid)
      // let buff = new Buffer(this.props.match.params.amount, 'base64');
      // let text = buff.toString('ascii');
      // console.log("amount query string "+atob(this.props.match.params.amount))
      
      this.props.getPaymentMasterFeeOption();
      this.props.getSingleInstallment(this.props.match.params.planid, this.props.match.params.instlmntid);
      this.props.planDetails(this.props.match.params.appid);
   }  

   onChangePaymentDetail(key, value) {
      //console.log(key+' '+value)
      const { add_err, calculateAmount } = this.state;
      switch (key) {
         case 'late_fee_waiver_type':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver type";
            } else {
               add_err[key] = '';
               this.props.getLateFeeWaiverType(value);
            }
            break;
         case 'late_fee_waiver_reason':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver reason";
               this.setState({final_late_fee: 0})
            } else {
               add_err[key] = '';
               var val = value.split('-');
               value = val[0];
               var finalLateFee = this.state.late_fee - (this.state.late_fee*val[1])/100;

               if(finalLateFee !== 0) {
                  var tp = this.state.calculateAmount.installment_amount + finalLateFee;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + this.state.late_fee + this.state.financial_charge;
               }

               if(this.state.final_fin_charge !==0) {
                  var tp = this.state.calculateAmount.installment_amount + finalLateFee + this.state.final_fin_charge;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + finalLateFee;
               }
               this.setState({late_fee_max_waiver: val[1], final_late_fee: finalLateFee, total_pay: tp})
            }
            //console.log(value)
            break;
         case 'late_fee_waiver_comt':
            if (isEmpty(value)) {
               add_err[key] = "Comment field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'fin_charge_waiver_type':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver type";
               //this.setState({waiver_type:''})
            } else {
               add_err[key] = '';
               this.props.getFinChargeWaiverType(value);
            }
            break;
         case 'fin_charge_waiver_reason':
            if (isEmpty(value)) {
               add_err[key] = "Select waiver reason";
               this.setState({final_fin_charge: 0})
            } else {
               add_err[key] = '';
               var val = value.split('-');
               value = val[0];
               
               var finalFinCharge = this.state.financial_charge - (this.state.financial_charge*val[1])/100;

               if(finalFinCharge !== 0) {
                  var tp = this.state.calculateAmount.installment_amount + finalFinCharge;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + this.state.late_fee + this.state.financial_charge;
               }

               if(this.state.final_late_fee !==0) {
                  var tp = this.state.calculateAmount.installment_amount + finalFinCharge + this.state.final_late_fee;
               } else {
                  var tp = this.state.calculateAmount.installment_amount + finalFinCharge;
               }
               this.setState({fin_charge_max_waiver: val[1], final_fin_charge: finalFinCharge, total_pay: tp})
               
            }
            break;
         case 'fin_charge_waiver_comt':
            if (isEmpty(value)) {
               add_err[key] = "Comment field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'financial_charge':
            if (isEmpty(value)) {
               add_err[key] = "Value field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'payment_note':
            if (isEmpty(value)) {
               add_err[key] = "Note field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'payment_method':
            if(value == 1) {
               this.setState({payment_method: 1, cheque_method_field: true})
            } else {
               this.setState({payment_method: 2, cheque_method_field: false})
            }
            break;
         case 'bank_name':
            if(isEmpty(value)) {
               add_err[key] = "Bank field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'txn_ref_no':
            if(isEmpty(value)) {
               add_err[key] = "Txn ref no can't be blank";
            } else {
               add_err[key] = ''
            }
            break;
         case 'payment_in':
            if(value == 1) {
               this.setState({payment_in: 1})
               add_err[key] = '';
            } else if(value == 2) {
               this.setState({payment_in: 2})
               add_err[key] = '';
            } else {
               add_err[key] = "Select payment option";
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });
      //console.log('this.state.calculateAmount')
      //console.log('ghggh'+value)
      //console.log(key)
      this.setState({
         calculateAmount: {
            ...this.state.calculateAmount,
            [key]: value
         }
      });

   }

   componentWillReceiveProps(nextProps) {
      let { calculateAmount, add_err } = this.state;

      (nextProps.rediretURL != '') ? this.setState({ changeURL: nextProps.rediretURL }) : '';
      //console.log(this.props.single_installment_payment);
      if(this.props.single_installment_payment === undefined) {
         //console.log('dfdfdfdfdfdfddfdf');
         this.setState({ nextTpay: true
         })
      }
      if (nextProps.single_installment_payment && this.state.nextTpay) {
         //console.log(nextProps.single_installment_payment);
         let singInsValue = {
            installment_amount: nextProps.single_installment_payment[0].installment_amt-nextProps.single_installment_payment[0].additional_inst_amount,
            bank_name: nextProps.single_installment_payment[0].bank_name,
            txn_ref_no: nextProps.single_installment_payment[0].txn_ref_no,
            late_fee_waiver_type: nextProps.single_installment_payment[0].late_fee_reason_id,
            late_fee_waiver_reason: nextProps.single_installment_payment[0].late_fee_waivers_id,
            late_fee_waiver_comt: nextProps.single_installment_payment[0].late_fee_waiver_comt,
            fin_charge_waiver_comt: nextProps.single_installment_payment[0].fin_charge_waiver_comt,
            payment_note: nextProps.single_installment_payment[0].comments,
            
            fin_charge_waiver_type: nextProps.single_installment_payment[0].fin_charge_reason_id,
            fin_charge_waiver_reason: nextProps.single_installment_payment[0].fin_charge_waiver_id,
            payment_installment_id: nextProps.single_installment_payment[0].patient_payemnt_id
         }

         let fincharge = ((nextProps.single_installment_payment[0].installment_amt/nextProps.single_installment_payment[0].payment_term)*nextProps.single_installment_payment[0].interest_rate)/100;
         

         let adderr = {
            late_fee_waiver_type: '',
            fin_charge_waiver_type: '',
            late_fee_waiver_reason: '',
            fin_charge_waiver_reason: '',
            payment_note: '',
            bank_name: '',
            txn_ref_no: '',
            payment_in: '',
         }

         this.setState({
            calculateAmount: singInsValue,
            interest_rate: nextProps.single_installment_payment[0].interest_rate,
            payment_term: nextProps.single_installment_payment[0].payment_term,
            financial_charge: fincharge, 
            final_late_fee : nextProps.single_installment_payment[0].late_fee_received,
            final_fin_charge: nextProps.single_installment_payment[0].fin_charge_amt,
            payment_in: nextProps.single_installment_payment[0].paid_flag,
            late_fee_max_waiver: nextProps.single_installment_payment[0].late_fee_pct,
            fin_charge_max_waiver: nextProps.single_installment_payment[0].fin_fee_pct,
            nextTpay: false,


            ///error///
            add_err: adderr,
            


         })
      }

      /*if(nextProps.late_fee_waiver_type_reason_option && this.state.nextTpay) {
         let cal = {
            late_fee_pct: nextProps.late_fee_waiver_type_reason_option[0].max_waiver
         }
         this.setState({
            calculateAmount: late_fee_pct, nextTpay: false
         });
      }*/

      if (nextProps.payment_billing_late_fee && this.state.nextTpay) {
         this.setState({
            late_fee: nextProps.payment_billing_late_fee[0].late_fee, nextTpay: false
         });
      }

      if(nextProps.payment_billing_late_fee && nextProps.single_installment_payment && this.state.nextTpay ) {
         let tpay;   
         if(nextProps.single_installment_payment[0].late_fee_received || nextProps.single_installment_payment[0].fin_charge_amt) {

            tpay = nextProps.single_installment_payment[0].payment_amount+nextProps.single_installment_payment[0].additional_amount+nextProps.single_installment_payment[0].late_fee_received+nextProps.single_installment_payment[0].fin_charge_amt
            console.log('if')
         
         } else {

            var nextlateFeeApply;
            
            var d1 = new Date(nextProps.single_installment_payment[0].due_date);
            var d2 = new Date();
            if(d1<d2) {
               nextlateFeeApply = true;
               tpay = parseFloat(nextProps.single_installment_payment[0].installment_amt-nextProps.single_installment_payment[0].additional_inst_amount)+parseFloat(nextProps.payment_billing_late_fee[0].late_fee)+parseFloat((nextProps.single_installment_payment[0].installment_amt/nextProps.single_installment_payment[0].payment_term)*nextProps.single_installment_payment[0].interest_rate)/100;
            } else {
               nextlateFeeApply = false;
               tpay = parseFloat(nextProps.single_installment_payment[0].installment_amt-nextProps.single_installment_payment[0].additional_inst_amount)+parseFloat((nextProps.single_installment_payment[0].installment_amt/nextProps.single_installment_payment[0].payment_term)*nextProps.single_installment_payment[0].interest_rate)/100;

            }
             
            
            console.log('else')
         }
         this.setState({
            total_pay: tpay, nextTpay: false
         })
      }


      ///FOR EDIT////
      /*if(nextProps.single_installment_payment && this.state.nextTpay) {
        let { calculateAmount, add_err } = this.state;
        
        let defaultVl = {
            bank_name: nextProps.single_installment_payment[0].bank_name,
            txn_ref_no: nextProps.single_installment_payment[0].txn_ref_no,
            late_fee_waiver_type: nextProps.single_installment_payment[0].late_fee_reason_id,
            late_fee_waiver_reason: nextProps.single_installment_payment[0].late_fee_waivers_id,
            late_fee_waiver_comt: nextProps.single_installment_payment[0].late_fee_waiver_comt,
            fin_charge_waiver_comt: nextProps.single_installment_payment[0].fin_charge_waiver_comt,
            payment_note: nextProps.single_installment_payment[0].comments
        }
        this.setState({
         calculateAmount: defaultVl,
         nextTpay: false
        
        }) */
        // var single_installment_payment[0].bank_name;
        // var comments = single_installment_payment[0].comments;
        // var finChaAmt = single_installment_payment[0].fin_charge_amt;
        // var finChaComt = single_installment_payment[0].fin_charge_waiver_comt;
        // var lateFeePct = single_installment_payment[0].late_fee_pct;
        // var lateFeeReasonId = single_installment_payment[0].late_fee_reason_id;
        // var lateFeeReceived = single_installment_payment[0].late_fee_received;
        // var lateFeeWaiverComt = single_installment_payment[0].late_fee_waiver_comt;
        // var lateFeeWaiversId = single_installment_payment[0].late_fee_waivers_id;
        // var txnRefNo = single_installment_payment[0].txn_ref_no;
     //}

     
   }




   render() {
      let { mainPlan, paymentPlanDetails, single_installment_payment, payment_billing_late_fee } = this.props;

      let { calculateAmount } = this.state;
      console.log(mainPlan)
      if(single_installment_payment) {
         var amount = single_installment_payment[0].payment_amount;
         var addi_amt = single_installment_payment[0].additional_amount;
         var finance_ch = single_installment_payment[0].fin_charge_amt;
         var late_Fee = (single_installment_payment[0].late_fee_received && single_installment_payment[0].late_fee_received !== null) ? single_installment_payment[0].late_fee_received : 0;

         var total_paid = amount+addi_amt+finance_ch+late_Fee;

      }


      if (this.props.redirectURL === 1) {
         return (<Redirect to={'/admin/credit-application/customer-plan/'+this.props.match.params.appid} />);
      }

      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = dd + '/' + mm + '/' + yyyy;

      //console.log('----------------------')
      //console.log(this.state.final_fin_charge);
      //console.log(this.state.calculateAmount.late_fee_waiver_reason+'-'+this.state.calculateAmount.late_fee_max_waiver);


      //compare
      var lateFeeApply;
      if(single_installment_payment) {
         var d1 = new Date(single_installment_payment[0].due_date);
         var d2 = new Date();
         if(d1<d2) {
            lateFeeApply = true;
         } else {
            lateFeeApply = false;
         }
      } 

      //current balance
      var getPaidBalnc = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 1 ) : '';
      if(getPaidBalnc) {
         var paidInsTotal = 0;
         for (var i = 0; i<getPaidBalnc.length; i++) {
           paidInsTotal += parseFloat(getPaidBalnc[i].installment_amt);
         }
      }

      //next pymnt due date
      var nxtPymntDueDate = (paymentPlanDetails) ? paymentPlanDetails.filter(x => x.paid_flag == 0 ) : '';

      //console.log('this.state');
      //console.log(this.state.calculateAmount.late_fee_waiver_reason);

      /*var lateFee;
      if(payment_billing_late_fee) {
        lateFee = parseFloat(payment_billing_late_fee[0].late_fee);
      } else {
         lateFee = 0;
      }

      var financialCharge;
      if(single_installment_payment) {
         financialCharge = parseFloat(((single_installment_payment[0].installment_amt / single_installment_payment[0].payment_term) * single_installment_payment[0].interest_rate) / 100)
      } else {
         financialCharge = 0;
      }

      var installmentAmt;
      if(single_installment_payment) {
         installmentAmt = parseFloat(single_installment_payment[0].installment_amt);
      } else {
         installmentAmt = 0;
      }*/
      //console.log(lateFee+"-----");
     //  //last payment rcvd
     //  var lastPymntRcvd = (singlePlanDetails) ? singlePlanDetails.filter(x => x.paid_flag == 1 ).pop() : '';

     // //next pymnt due date
     //  var nxtPymntDueDate = (singlePlanDetails) ? singlePlanDetails.filter(x => x.paid_flag == 0 ) : '';
     //  var nxtPymntDueDat = (nxtPymntDueDate[0]) ? nxtPymntDueDate[0] : '';

     //  //application_id
     //  var appId = (mainPlan) ? mainPlan[0].application_id : '';

      const financial_charges_option = this.props.financial_charges_option;
      const late_fee_waivers_option = this.props.late_fee_waivers_option;
      const financial_charges_waiver_option = this.props.financial_charges_waiver_option;
      const late_fee_waiver_type_reason_option = this.props.late_fee_waiver_type_reason_option;
      const fin_charge_waiver_type_reason_option = this.props.fin_charge_waiver_type_reason_option;
      const payment_method_option = this.props.payment_method_option;

      //console.log(this.state);
      //console.log(nxtPymntDueDat.due_date)
      return (
         <div className="invoice-wrapper">
            {this.props.loading &&
                  <RctSectionLoader />
               }
            <PageTitleBar title={<IntlMessages id="sidebar.view" />} match={this.props.match} />
            <div className="row">
            
               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                           </li>
                        </ul>
                     </div>

                     <div className="p-50" ref={el => (this.componentRef = el)}>
                        <div className="d-flex justify-content-between">
                           <div className="sender-address">
                              <div className="invoice-logo mb-30">
                                 <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid"  />
                              </div>

                           </div>
                           <div className="invoice-address text-right">
                              <span>Invoice: #{(mainPlan) ? mainPlan[0].patient_ac : '-'}</span>
                              
                           </div>
                        </div>

                        <div className="d-flex justify-content-between mb-30 add-full-card customer-detail-info">

                            <div className="add-card">
                              <table>
                              <tbody>
                                
                                 <tr>
                                    <td className="text-left"><strong>Name :</strong></td>
                                    <td className="text-capitalize text-left">
                                       {(mainPlan) ? (mainPlan[0].f_name + ' ' + mainPlan[0].m_name + ' ' + mainPlan[0].l_name) : '-'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td className="text-left"><strong>Address :</strong></td>
                                    <td className="text-left">
                                       {(mainPlan) ? (mainPlan[0].address1 + ', ' + mainPlan[0].address2 + ', ' + mainPlan[0].City + ', ' + mainPlan[0].region_name) : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-left"><strong>Phone No :</strong></td>
                                    <td className="text-left">
                                       {(mainPlan) ? mainPlan[0].peimary_phone : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td></td>
                                    <td></td>
                                 </tr>
                                 <tr>
                                    <td className="text-left"><strong>Bank Name :</strong></td>
                                    <td className="text-left">
                                       {(mainPlan) ? mainPlan[0].bank_name : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td className="text-left"><strong>Txn Ref No :</strong></td>
                                    <td className="text-left">
                                       {(mainPlan) ? mainPlan[0].txn_ref_no : '-'}
                                    </td>
                                 </tr>
                              </tbody>
                              </table>
                           </div>

                            

                            <div className="add-card">
                              <table>
                              <tbody>
                                
                                 <tr>
                                    <td><strong>Account Number :</strong></td>
                                    <td>
                                       {(mainPlan) ? mainPlan[0].patient_ac : '-'}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td><strong>Payment Due Date :</strong></td>
                                    <td>
                                       {(nxtPymntDueDate) ? nxtPymntDueDate[0].due_date : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Payment Submitted Date :</strong></td>
                                    <td>
                                       {(single_installment_payment) ? single_installment_payment[0].payment_date : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Amount Due :</strong></td>
                                    <td>
                                       {(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><strong>Late Fee :</strong></td>
                                    <td>
                                       {'$'+parseFloat(late_Fee).toFixed(2)}
                                    </td>
                                 </tr>
                              </tbody>
                              </table>
                           </div>
                        </div>
                        <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt installment-view customer-loan-info">

                           <div className="add-card">
                              <table>
                              <tbody>
                                 <tr>
                                    <th colSpan="2">Loan Information</th>
                                 </tr>
                                 <tr>
                                    <td>Loan Amount :</td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].credit_amount.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Payment Term :</td>
                                    <td>{(mainPlan) ? mainPlan[0].payment_term+' Month' : '-'}</td>
                                 </tr>
                                 
                                 <tr>
                                    <td>Interest Rate(%) :</td>
                                    <td>{(mainPlan) ? mainPlan[0].interest_rate+"%" : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Monthly Payment :</td>
                                    <td>{(mainPlan) ? '$'+mainPlan[0].monthly_payment.toFixed(2) : '-'}</td>
                                 </tr>
                                 <tr>
                                    <td>Current Balance :</td>
                                    <td>{(mainPlan) ? '$'+(mainPlan[0].credit_amount-paidInsTotal).toFixed(2) : '-'}</td>
                                 </tr>
                              </tbody>
                              </table>
                           </div>
                          
                           <div className="add-card explaination">
                              <table>
                              <tbody>
                                 <tr>
                                    <th colSpan="2">Explanation of Payment</th>
                                 </tr>
                                 <tr>
                                    <td>Regular Monthly Payment :</td>
                                    <td>
                                       {'$'+parseFloat(amount).toFixed(2)}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Additional Amount :</td>
                                    <td>
                                       {'$'+parseFloat(addi_amt).toFixed(2)}
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Late Fee :</td>
                                    <td>
                                       {'$'+parseFloat(late_Fee).toFixed(2)}
                                    </td>
                                 </tr>
                                 
                                 <tr>
                                    <td className="pad-bottom-40">Other Fee and Charges :</td>
                                    <td className="pad-bottom-40">
                                       {'$'+parseFloat(finance_ch).toFixed(2)}
                                    </td>
                                 </tr>
                                 
                                 
                                 <tr>
                                    <td><strong>Total Amount Paid :</strong></td>
                                    <td><strong>{'$'+parseFloat(total_paid).toFixed(2)}</strong></td>
                                 </tr>
                              </tbody>
                              </table>
                           </div>

                           
                        </div>

                       


                     </div>


                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer, creditApplication }) => {
   //console.log(PaymentPlanReducer);
   const { nameExist, isEdit } = authUser;
   const { redirectURL, loading, late_fee_waivers_option, financial_charges_waiver_option, financial_charges_option, payment_method_option, late_fee_waiver_type_reason_option, fin_charge_waiver_type_reason_option, single_installment_payment, payment_billing_late_fee } = PaymentPlanReducer;
   const { mainPlan, paymentPlanDetails } = creditApplication;
   //console.log(single_installment_payment);
   return { redirectURL, loading, late_fee_waivers_option, financial_charges_waiver_option, financial_charges_option, payment_method_option, late_fee_waiver_type_reason_option, fin_charge_waiver_type_reason_option, single_installment_payment, payment_billing_late_fee, redirectURL, mainPlan, paymentPlanDetails }
}

export default connect(mapStateToProps, {
   getPaymentMasterFeeOption, getLateFeeWaiverType, getFinChargeWaiverType, getSingleInstallment, insertPayPayment, clearRedirectURL, planDetails
})(CustomerPaymentBilling);