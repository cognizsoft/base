/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import moment from 'moment';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import CryptoJS from 'crypto-js';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
// add new user form
import AddInvoiceMsgForm from './AddInvoiceMsgForm';
import { isEmpty } from '../../../validator/Validator';
import {
   planDetails, clearRedirectURL, getSingleInstallment, sendReport, invoiceDetails
} from 'Actions';
class CustomerBilling extends Component {

   state = {
      loan_plan_info: '',
      loan_plan_info_data: false,
      reg_mnthly_amt: 0,
      fincharge_amt: 0,
      totalEnclosed_amt: 0,
      application_id: '',

      addInvoiceMsgModal: false,
      invoice_msg: '',
      /*err: {
         invoice_msg: '',
      },*/
      add_err: {
         invoice_msg: '',
      },
      updateForm: {},
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }

   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   componentDidMount() {
      //this.permissionFilter(this.state.currentModule);

      this.props.clearRedirectURL();
      this.props.invoiceDetails(this.dec(this.props.match.params.invoice_id));
      //this.props.getSingleInstallment(this.props.match.params.invoice_id);
   }

   validateAddSubmit() {
      return (
         this.state.add_err.invoice_msg === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onChangeAddInvoiceMsgDetails(name, value)
      });
   }

   onChangeAddInvoiceMsgDetails(fieldName, value) {

      let { add_err, invoice_msg } = this.state;
      switch (fieldName) {
         case 'invoice_msg':
            if (isEmpty(value)) {
               add_err[fieldName] = "Message can't be blank";
            } else {
               add_err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err, invoice_msg: value });

   }

   sendCustomerReport() {
      let { invoice_msg } = this.state;
      //console.log(invoice_msg)
      //return false
      this.props.sendReport(invoice_msg, this.dec(this.props.match.params.invoice_id));
      this.setState({ addInvoiceMsgModal: false });
   }

   opnInvoiceModal() {
      let addR = { text: '' }
      this.setState({ addInvoiceMsgModal: true, add_err: addR, });
   }
   opnInvoiceModalClose = () => {
      let upR = { text: '' }
      let addR = { text: '' }

      this.setState({ addInvoiceMsgModal: false, add_err: addR, invoice_msg: '' })
   }

   componentWillReceiveProps(nextProps) {

      let mainPlan = nextProps.mainPlan;
      let invoiceDtl = nextProps.invoice_dtl;
      let amountDetailsInv = nextProps.amountDetailsInv;
      let single_installment_payment = nextProps.single_installment_payment;

      /*if (invoiceDtl !== undefined) {

         console.log('invoiceDtl')
         console.log(mainPlan)
         


         this.setState({
            loan_plan_info: loan_plan_info,
            loan_plan_info_data: true,
            application_id: invoiceDtl[0].application_id
            
         })

      }*/
   }

   goBack() {
      this.props.history.goBack(-1)
   }
   render() {
      let { mainPlan, singlePlanDetails, paymentPlanDetails, invoice_detail, single_installment_payment, last_payment_info, invoice_dtl, invoice_last_pymt, credit_charge } = this.props;

      let { add_err, loan_plan_info, loan_plan_info_data, reg_mnthly_amt, fincharge_amt, totalEnclosed_amt } = this.state;



      var todayTime = new Date();
      var month = ((todayTime.getMonth() + 1) < 10) ? '0' + (todayTime.getMonth() + 1) : todayTime.getMonth() + 1;
      var day = (todayTime.getDate() < 10) ? '0' + todayTime.getDate() : todayTime.getDate();
      var year = todayTime.getFullYear();
      var today = month + "/" + day + "/" + year;

      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.invoice" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li><a href="javascript:void(0);" onClick={() => this.opnInvoiceModal()}><i className="mr-10 ti-email"></i> Email</a></li>
                           <li>
                              <ReactToPrint
                                 trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                                 content={() => this.componentRef}
                              />
                           </li>
                        </ul>
                     </div>

                     <div className="customer-invoice pt-5 pb-5 px-50" ref={el => (this.componentRef = el)}>
                        <div className="d-flex justify-content-between">
                           <div className="sender-address">
                              <div className="invoice-logo mb-30">
                                 <img src={require('Assets/img/health-partner.png')} alt="session-logo" className="img-fluid" /><br />
                                 <span>5720 Creedmoor Road, Suite 103</span><br />
                                 <span>Raleigh, NC 27612</span>
                              </div>

                           </div>
                           <div className="invoice-address text-right">
                              <span>Invoice: #{(invoice_dtl) ? invoice_dtl[0].invoice_number : '-'}</span>
                              <span>{today}</span>
                              <span className="invoice_status">{/*(invoice_dtl) ? (invoice_dtl[0].paid_flag == 1) ? 'Paid' : (invoice_dtl[0].paid_flag == 2) ? 'In Progress' : (invoice_dtl[0].paid_flag !== 2 && invoice_dtl[0].paid_flag !==1) ? 'Unpaid' : '' : ''*/}</span>
                           </div>
                        </div>

                        <div className="d-flex justify-content-between mb-10 add-full-card customer-detail-info">

                           <div className="add-card">
                              <table>
                                 <tbody>

                                    <tr>
                                       <td className="text-capitalize text-left">
                                          {(invoice_dtl) ? (invoice_dtl[0].f_name + ' ' + invoice_dtl[0].m_name + ' ' + invoice_dtl[0].l_name) : '-'}
                                       </td>
                                    </tr>

                                    <tr>
                                       <td className="text-left">
                                          {(invoice_dtl) ? (invoice_dtl[0].address1) : '-'}<br />
                                          {(invoice_dtl) ? (invoice_dtl[0].City + ', ' + ' ' + invoice_dtl[0].state+ ' ' + invoice_dtl[0].zip_code) : '-'}
                                       </td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                       <td className="text-left">
                                          {(invoice_dtl) ? invoice_dtl[0].peimary_phone : '-'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </div>

                           {(invoice_dtl && invoice_dtl[0].primary_bill_pay_flag == 1) && <div className="add-card">
                              <table>
                                 <tbody>

                                    <tr>
                                       <td className="text-capitalize text-left">
                                       Co-signer Name : {(invoice_dtl) ? (invoice_dtl[0].co_f_name + ' ' + invoice_dtl[0].co_m_name + ' ' + invoice_dtl[0].co_l_name) : '-'}
                                       </td>
                                    </tr>

                                    <tr>
                                       <td className="text-left">
                                          {(invoice_dtl) ? (invoice_dtl[0].co_address1+' '+invoice_dtl[0].co_address2) : '-'}<br />
                                          {(invoice_dtl) ? (invoice_dtl[0].co_city + ', ' + ' ' + invoice_dtl[0].co_state_name + ' ' + invoice_dtl[0].co_zip_code) : '-'}
                                       </td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                       <td className="text-left">
                                          {(invoice_dtl) ? invoice_dtl[0].co_peimary_phone : '-'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>
                           </div>
                           }
                           <div className="add-card">
                              <table>
                                 <tbody>
                                    <tr>
                                       <td><strong>Invoice Date :</strong></td>
                                       <td>{today}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Account Number :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? invoice_dtl[0].patient_ac : '-'}
                                       </td>
                                    </tr>

                                    <tr>
                                       <td><strong>Payment Due Date :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? invoice_dtl[0].due_date : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Amount Due :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].payment_amount).toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Late Fee after({(invoice_dtl) ? invoice_dtl[0].due_date : '-'}) :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].master_late_fee).toFixed(2) : 0}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Withdrawal day of the month :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? invoice_dtl[0].withdrawal_date : '-'}
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>

                        <div className="table-responsive mb-10 pymt-history">
                           <h2 className="text-center mb-10">Plans</h2>
                           <table className="table table-borderless admin-application-payment-plan">
                              <thead>
                                 <tr>
                                    <th>Plan ID</th>
                                    <th>Principal Amt</th>
                                    <th>Outstanding Principal Amt</th>
                                    <th>Payment Term</th>
                                    <th>APR(%)</th>
                                    <th>Monthly Payment</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 {mainPlan && mainPlan.map(function (row, idx) {
                                    return (<tr key={idx}>
                                       <td>{row.pp_id}</td>
                                       <td>${(row.loan_amount).toFixed(2)}</td>
                                       <td>${row.remaining_amount.toFixed(2)}</td>
                                       <td>{row.term} month</td>
                                       <td>{parseFloat(row.discounted_interest_rate).toFixed(2)}%</td>
                                       <td>${row.monthly_amount}</td>

                                    </tr>)
                                 }.bind(this))
                                 }

                              </tbody>
                           </table>
                        </div>

                        <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt customer-loan-info">

                           <div className="add-card explaination w-50 mr-15">

                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Current Month</th>
                                    </tr>
                                    <tr>
                                       <td>Regular Monthly Payment :</td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].payment_amount).toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Late Fee :</td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].late_fee_due).toFixed(2) : '$0.00'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Previous Late Fee :</td>
                                       <td>
                                          {(invoice_dtl && invoice_dtl[0].previous_late_fee) ? '$' + parseFloat(invoice_dtl[0].previous_late_fee).toFixed(2) : '$0.00'}
                                       </td>
                                    </tr>

                                    <tr className="">
                                       <td>Finance Charge :</td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].fin_charge_due).toFixed(2) : '$0.00'}
                                       </td>
                                    </tr>
                                    <tr className="border-bottom-none">
                                       <td className="">Previous Finance Charge :</td>
                                       <td className="">
                                          {(invoice_dtl && invoice_dtl[0].previous_fin_charge) ? '$' + parseFloat(invoice_dtl[0].previous_fin_charge).toFixed(2) : '$0.00'}
                                       </td>
                                    </tr>
                                    <tr className="border-bottom-none">
                                       <td className="pad-bottom-40">&nbsp;</td>
                                       <td className="pad-bottom-40">&nbsp;</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Total Amount Due :</strong></td>
                                       <td>
                                          {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].payment_amount).toFixed(2) : '$0.00'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>

                           </div>

                           <div className="add-card explaination w-50 ml-15">
                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Last Month</th>
                                    </tr>
                                    <tr>
                                       <td>Regular Monthly Payment :</td>
                                       <td>
                                          {(invoice_last_pymt !== undefined && invoice_last_pymt.length > 0 && invoice_last_pymt[0].payment_amount) ? '$' + parseFloat(invoice_last_pymt[0].payment_amount).toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Payment Date :</td>
                                       <td>
                                          {(invoice_last_pymt !== undefined && invoice_last_pymt.length > 0 && invoice_last_pymt[0].payment_date) ? invoice_last_pymt[0].payment_date : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Paid Amount :</td>
                                       <td>
                                          {(invoice_last_pymt !== undefined && invoice_last_pymt.length > 0 && invoice_last_pymt[0].paid_amount) ? '$' + parseFloat(invoice_last_pymt[0].paid_amount + invoice_last_pymt[0].additional_amount).toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Credit Card Charge :</td>
                                       <td>
                                          {(invoice_last_pymt !== undefined && invoice_last_pymt.length > 0) ? '$' + parseFloat(invoice_last_pymt[0].credit_charge_amt).toFixed(2) : '-'}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Late Fee :</td>
                                       <td>
                                          {(() => {
                                             if (invoice_last_pymt !== undefined && invoice_last_pymt.length > 0) {

                                                var pv_lf = (invoice_last_pymt[0].previous_late_fee) ? invoice_last_pymt[0].previous_late_fee : 0;
                                                var lf = (invoice_last_pymt[0].late_fee_due) ? invoice_last_pymt[0].late_fee_due : 0;
                                                var lfr = (invoice_last_pymt[0].late_fee_received) ? invoice_last_pymt[0].late_fee_received : 0;

                                                if (invoice_last_pymt[0].mdv_late_percentage) {
                                                   //var lateff = ((parseFloat(pv_lf) + parseFloat(lf)) - ((parseFloat(pv_lf) + parseFloat(lf)) * invoice_last_pymt[0].mdv_late_percentage) / 100)
                                                   var lateff = parseFloat(lfr)

                                                   return '$' + lateff.toFixed(2);
                                                } else {
                                                   var lateff = parseFloat(lfr)

                                                   return '$' + lateff.toFixed(2);
                                                }

                                             } else {
                                                return '-';
                                             }
                                          })()}
                                       </td>
                                    </tr>

                                    <tr>
                                       <td className="pad-bottom-40">Finance Charge :</td>
                                       <td className="pad-bottom-40">
                                          {(() => {
                                             if (invoice_last_pymt !== undefined && invoice_last_pymt.length > 0) {

                                                var pv_fc = (invoice_last_pymt[0].previous_fin_charge) ? invoice_last_pymt[0].previous_fin_charge : 0;
                                                var fc = (invoice_last_pymt[0].fin_charge_due) ? invoice_last_pymt[0].fin_charge_due : 0;
                                                var fcr = (invoice_last_pymt[0].fin_charge_received) ? invoice_last_pymt[0].fin_charge_received : 0;

                                                if (invoice_last_pymt[0].mdv_fin_percentage) {
                                                   //var finCh = ((parseFloat(pv_fc) + parseFloat(fc)) - ((parseFloat(pv_fc) + parseFloat(fc)) * invoice_last_pymt[0].mdv_fin_percentage) / 100)
                                                   var finCh = parseFloat(fcr)

                                                   return '$' + finCh.toFixed(2);
                                                } else {
                                                   var finCh = parseFloat(fcr)

                                                   return '$' + finCh.toFixed(2);
                                                }

                                             } else {
                                                return '-'
                                             }
                                          })()}
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><strong>Total Amount Paid :</strong></td>
                                       <td>
                                          {(() => {
                                             if (invoice_last_pymt !== undefined && invoice_last_pymt.length > 0) {

                                                var pv_lf = (invoice_last_pymt[0].previous_late_fee) ? invoice_last_pymt[0].previous_late_fee : 0;
                                                var lf = (invoice_last_pymt[0].late_fee_due) ? invoice_last_pymt[0].late_fee_due : 0;

                                                var pv_fc = (invoice_last_pymt[0].previous_fin_charge) ? invoice_last_pymt[0].previous_fin_charge : 0;
                                                var fc = (invoice_last_pymt[0].fin_charge_due) ? invoice_last_pymt[0].fin_charge_due : 0;

                                                var lfee;
                                                var fchh;

                                                if (invoice_last_pymt[0].mdv_fin_percentage) {
                                                   var finCh = ((parseFloat(pv_fc) + parseFloat(fc)) - ((parseFloat(pv_fc) + parseFloat(fc)) * invoice_last_pymt[0].mdv_fin_percentage) / 100)

                                                   fchh = finCh.toFixed(2);
                                                } else {
                                                   fchh = parseFloat(pv_fc) + parseFloat(fc)
                                                }

                                                if (invoice_last_pymt[0].mdv_late_percentage) {
                                                   var lateff = ((parseFloat(pv_lf) + parseFloat(lf)) - ((parseFloat(pv_lf) + parseFloat(lf)) * invoice_last_pymt[0].mdv_late_percentage) / 100)

                                                   lfee = lateff.toFixed(2);
                                                } else {
                                                   lfee = parseFloat(pv_lf) + parseFloat(lf)
                                                }

                                                var totl = parseFloat(lfee) + parseFloat(fchh) + parseFloat(invoice_last_pymt[0].paid_amount + invoice_last_pymt[0].additional_amount + invoice_last_pymt[0].credit_charge_amt);

                                                return '$' + totl.toFixed(2);

                                             } else {
                                                return '-';
                                             }
                                          })()}
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>


                        <div className="mb-10 paymt-voucher">
                           <h3 className="text-center mb-10">Confirm Payment</h3>
                           <div className="add-full-card pymt-voucher-detail d-flex">

                              <div className="add-card vocher-box bg-grey mr-10 w-60 mw-100 pb-0 credit-card-details">
                                 <p className="credit-card-heading mb-0 p-10">If Paying By Credit Card Please Fill Out Below</p>
                                 <div className="logo-container">
                                    <p className="logo-heading mb-0 text-center">Check Card Using for Payment</p>
                                    <ul className="mb-0 p-10">
                                       <li>Visa</li>
                                       <li>Master Card</li>
                                       <li>American Express</li>
                                       <li>Discover</li>
                                    </ul>
                                 </div>
                                 <div className="field-container">
                                    <table>
                                       <tbody>
                                          <tr>
                                             <td>Card Number</td>
                                             <td>3 Digit Security Code</td>
                                          </tr>
                                          <tr>
                                             <td className="pt-5">Exp. Date</td>
                                             <td className="pt-5">Name on Card</td>
                                          </tr>
                                          <tr>
                                             <td>Zip Code</td>
                                             <td>Signature</td>
                                          </tr>
                                          <tr>
                                             <td>{credit_charge}% Additional Charge
                                                <span className="additional-credit-amount">
                                                   {(() => {
                                                      if (invoice_dtl) {

                                                         var pa = parseFloat(invoice_dtl[0].payment_amount);
                                                         var lfr = parseFloat(invoice_dtl[0].late_fee_due);
                                                         var lfp = (invoice_dtl[0].previous_late_fee) ? parseFloat(invoice_dtl[0].previous_late_fee) : 0;
                                                         var fca = parseFloat(invoice_dtl[0].fin_charge_due);
                                                         var fcp = (invoice_dtl[0].previous_fin_charge) ? parseFloat(invoice_dtl[0].previous_fin_charge) : 0;
                                                         var ttotal = pa + lfr + lfp + fca + fcp;
                                                         return '$' + parseFloat(((ttotal * credit_charge) / 100).toFixed(2))

                                                      }
                                                   })()}
                                                </span>
                                             </td>
                                             <td>Pay This Amount
                                                <span className="payable-amount">
                                                   {(() => {
                                                      if (invoice_dtl) {

                                                         var pa = parseFloat(invoice_dtl[0].payment_amount);
                                                         var lfr = parseFloat(invoice_dtl[0].late_fee_due);
                                                         var lfp = (invoice_dtl[0].previous_late_fee) ? parseFloat(invoice_dtl[0].previous_late_fee) : 0;
                                                         var fca = parseFloat(invoice_dtl[0].fin_charge_due);
                                                         var fcp = (invoice_dtl[0].previous_fin_charge) ? parseFloat(invoice_dtl[0].previous_fin_charge) : 0;

                                                         var total_credit = pa + lfr + lfp + fca + fcp;
                                                         var percentage = parseFloat(((total_credit * credit_charge) / 100)).toFixed(2)
                                                         return '$' + (parseFloat(total_credit) + parseFloat(percentage)).toFixed(2)
                                                      }
                                                   })()}
                                                </span>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>

                              <div className="add-card vocher-box  vocher-box-right bg-bluee mw-100 pb-0">
                                 <table>
                                    <tbody>
                                       <tr><th colSpan="2">Amount Due</th>
                                       </tr>

                                       <tr>
                                          <td>Invoice Date :</td>
                                          <td>{today}</td>
                                       </tr>
                                       <tr>
                                          <td>Account Number :</td>
                                          <td>
                                             {(invoice_dtl) ? invoice_dtl[0].patient_ac : '-'}
                                          </td>
                                       </tr>

                                       <tr>
                                          <td>Payment Due Date :</td>
                                          <td>
                                             {(invoice_dtl) ? invoice_dtl[0].due_date : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Amount Due :</td>
                                          <td>
                                             {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].payment_amount).toFixed(2) : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Late Fee :</td>
                                          <td>
                                             {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].late_fee_due).toFixed(2) : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Previous Late Fee :</td>
                                          <td>
                                             {(invoice_dtl && invoice_dtl[0].previous_late_fee) ? '$' + parseFloat(invoice_dtl[0].previous_late_fee).toFixed(2) : '$0.00'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Finance Charge :</td>
                                          <td>
                                             {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].fin_charge_due).toFixed(2) : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>Previous Finance Charge :</td>
                                          <td>
                                             {(invoice_dtl && invoice_dtl[0].previous_fin_charge) ? '$' + parseFloat(invoice_dtl[0].previous_fin_charge).toFixed(2) : '$0.00'}
                                          </td>
                                       </tr>
                                       <tr className="border-0">
                                          <td className="tot_price">
                                             <strong>Total Amount : </strong>
                                          </td>
                                          <td className="tot_price">
                                             {(invoice_dtl) ? '$' + parseFloat(invoice_dtl[0].payment_amount).toFixed(2) : '$0.00'}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <p className="credit-card-note m-0"><strong>Note: </strong>{credit_charge}% Processing charges will apply.</p>
                           <br />
                           <span>5720 Creedmoor Road, Suite 103</span><br />
                           <span>Raleigh, NC 27612</span>


                        </div>




                     </div>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li className="confirm_payment">
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back">Back</Link>
                           </li>
                           <li className="confirm_payment">
                              <Link to={`/admin/credit-application/payment/${this.props.match.params.invoice_id}`} title="Confirm Payment">Confirm Payment</Link>
                           </li>
                        </ul>
                     </div>
                  </RctCard>
               </div>
            </div>

            <Modal isOpen={this.state.addInvoiceMsgModal} toggle={() => this.opnInvoiceModalClose()}>
               <ModalHeader toggle={() => this.opnInvoiceModalClose()}>
                  Send Message with Invoice
               </ModalHeader>
               <ModalBody>

                  <AddInvoiceMsgForm
                     addErr={add_err}
                     addInvoiceMsgDetails={this.state.invoice_msg}
                     onChangeaddInvoiceMsgDetail={this.onChangeAddInvoiceMsgDetails.bind(this)}
                  />

               </ModalBody>
               <ModalFooter>
                  <Button
                     variant="contained"
                     color="primary"
                     className="text-white"
                     onClick={() => this.sendCustomerReport()}
                     disabled={!this.validateAddSubmit()}
                  >Send</Button>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.opnInvoiceModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer }) => {

   const { nameExist, isEdit } = authUser;
   console.log(mainPlan)
   const { single_installment_payment, payment_billing_late_fee, loading, mainPlan, singlePlanDetails, paymentPlanDetails, last_payment_info, invoice_detail, invoice_dtl, invoice_last_pymt, amountDetailsInv, credit_charge, redirectURL } = PaymentPlanReducer

   return { loading, mainPlan, singlePlanDetails, paymentPlanDetails, redirectURL, single_installment_payment, payment_billing_late_fee, last_payment_info, invoice_detail, invoice_dtl, invoice_last_pymt, amountDetailsInv, credit_charge }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, getSingleInstallment, sendReport, invoiceDetails
})(CustomerBilling);