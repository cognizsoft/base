/**
 * Invoice
 */
import React, { Component } from 'react';

import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import Button from '@material-ui/core/Button';
import { RctCard } from 'Components/RctCard/index';
import VerifyCustomerForm from './verify-customer/VerifyCustomerForm';
import moment from 'moment';
import CryptoJS from 'crypto-js';
import Tooltip from '@material-ui/core/Tooltip';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';


import {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice, adminPlanDetailsDownload, unlockUserAccount, getLockUserDetail, downloadPlanAgreement
} from 'Actions';
class PlanDetails extends Component {

   state = {

      verifyModal: false,
      selectedVerifyUserId: '',
      selectedVerifyUserQuestions: null,
      hover_user: '',
      hover: false,

      selected_question: '',
      selected_question_answer: '',

      selectedQuestion: {
         questions: '',
         answer: ''
      },
      unlockedUserId: '',

      check_fullname: 0,
      check_dob: 0,
      check_ssn: 0,
      check_question: 0,
      check_answer: 0,

      verify_err: {
         questions: '',
         check_fullname: '',
         check_dob: '',
         check_ssn: '',
         check_question: '',
         check_answer: ''
      },
      confimInvoiceAppid: '',
      confimInvoiceDate: '',
   }

   onVerifyUser(user_id) {
      const selectedVerifyUserQuestions = this.props.userQuestions
      //console.log('selectedUnlockUser')
      //console.log(selectedVerifyUserQuestions)

      this.props.getLockUserDetail(this.props.application_details[0].user_id)
      //return false;

      //var sltUserQue = selectedVerifyUserQuestions.filter(x => x.user_id == user_id)

      //console.log(sltUserQue)
      this.setState({ verifyModal: true, selectedVerifyUserId: user_id, selectedVerifyUserQuestions: selectedVerifyUserQuestions });
   }

   verifyModalClose() {
      //console.log(this.state.selectedUnlockUserId)
      this.setState({ verifyModal: false, selectedVerifyUserId: '', selectedVerifyUserQuestions: null })
   }

   onChangeVerifyUser(fieldName, value) {

      let { verify_err, selectedQuestion } = this.state;
      switch (fieldName) {
         case 'questions':
            //console.log(value)
            if (value == '') {
               this.setState({ selected_question: "" })
               verify_err[fieldName] = "Select question";
            } else {

               var que = this.state.selectedVerifyUserQuestions.filter(x => x.security_questions_id == value)
               selectedQuestion.answer = que[0].answers
               this.setState({ selected_question: value, selectedQuestion: selectedQuestion })
               verify_err[fieldName] = "";

            }
            break;
         case 'answer':
            if (value == '') {
               this.setState({ selected_question_answer: '' })
               verify_err[fieldName] = "Answer can't be blank";
            } else {
               this.setState({ selected_question_answer: value })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_fullname':
            //console.log(value)
            let vl1 = (this.state.check_fullname) ? 0 : 1;
            //console.log(vl1)
            if (this.state.check_fullname == 0) {
               this.setState({ check_fullname: vl1 })
               verify_err[fieldName] = "check fullname";
            } else {
               this.setState({ check_fullname: vl1 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_dob':
            //console.log(value)
            let vl2 = (this.state.check_dob) ? 0 : 1;
            ///console.log(vl2)
            if (this.state.check_dob == 0) {
               this.setState({ check_dob: vl2 })
               verify_err[fieldName] = "check dob";
            } else {
               this.setState({ check_dob: vl2 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_ssn':
            let vl3 = (this.state.check_ssn) ? 0 : 1;
            if (this.state.check_ssn == 0) {
               this.setState({ check_ssn: vl3 })
               verify_err[fieldName] = "check ssn";
            } else {
               this.setState({ check_ssn: vl3 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_question':
            let vl4 = (this.state.check_question) ? 0 : 1;
            if (this.state.check_question == 0) {
               //selectedQuestion.questions = ''
               this.setState({ check_question: vl4, /*selectedQuestion: selectedQuestion*/ })
               verify_err[fieldName] = "check question";
               //verify_err['questions'] = "Select Question";
            } else {
               this.setState({ check_question: vl4 })
               verify_err[fieldName] = "";
            }

            //console.log(this.state)
            break;
         case 'check_answer':
            let vl5 = (this.state.check_answer) ? 0 : 1;
            if (this.state.check_answer == 0) {
               this.setState({ check_answer: vl5 })
               verify_err[fieldName] = "check answer";
            } else {
               this.setState({ check_answer: vl5 })
               verify_err[fieldName] = "";
            }
            break;
         default:
            break;

      }

      this.setState({ verify_err: verify_err });
      //console.log(this.state.err)

      this.setState({
         selectedQuestion: {
            ...this.state.selectedQuestion,
            [fieldName]: value
         }
      });

   }

   verifyUserAccount() {
      const { selectedVerifyUserId, selectedQuestion } = this.state;

      /*selectedQuestion.user_id = selectedVerifyUserId
      

      var AllQueAns = this.props.userQuestions;

      var verifyAns = AllQueAns.filter(x => x.security_questions_id == selectedQuestion.questions && x.user_id == selectedQuestion.user_id && x.answers == selectedQuestion.answer)

      if(verifyAns.length == 0) {
         NotificationManager.error("Answer not matched!");
      } else {*/

      //this.props.unlockUserAccount(selectedQuestion.user_id);



      this.setState({ loading: true })
      let self = this;

      setTimeout(() => {
         let selectedQuestion = {
            questions: '',
            answer: ''
         }
         this.setState({ verifyModal: false, selectedVerifyUserId: '', selectedVerifyUser: null, selectedQuestion: selectedQuestion, check_fullname: 0, check_dob: 0, check_ssn: 0, check_question: 0, check_answer: 0 });
         NotificationManager.success("Customer Verified!");
         self.setState({ loading: false });
      }, 2000);

      //}
      //return false;

   }

   validateVerifyForm() {
      return (
         this.state.verify_err.questions === '' &&
         //this.state.unlock_err.answer === '' &&
         this.state.verify_err.check_fullname !== '' &&
         this.state.verify_err.check_dob !== '' &&
         this.state.verify_err.check_ssn !== '' &&
         this.state.verify_err.check_question !== '' &&
         this.state.verify_err.check_answer !== ''
      );
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }
   componentDidMount() {
      this.props.clearRedirectURL();
      this.props.allPlanDetails(this.dec(this.props.match.params.appid));
   }
   /*
   * Title :- createInvoce
   * Descrpation :- This function use for create invoice according to due date
   * Date :- Dec 5, 2019
   * Author :- Ramesh Kumar
   */
   createInvoce(date, appid) {
      date = moment(date, 'MM/DD/YYYY', true).format('YYYY-MM-DD');
      this.refs.deleteConfirmationDialog.open();
      this.setState({ confimInvoiceDate: date, confimInvoiceAppid: appid })
      //this.setState({ selectedProvider: data });
   }
   createInvoiceConfirm() {
      this.props.createInvoice(this.state.confimInvoiceDate, this.state.confimInvoiceAppid)
      this.refs.deleteConfirmationDialog.close();
      this.setState({ confimInvoiceDate: '', confimInvoiceAppid: '' })
   }
   //createInvoice

   mainInvoice() {
      this.setState({ singleView: null })
   }

   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   adminPlanDetails() {
      this.props.adminPlanDetailsDownload(this.dec(this.props.match.params.appid));
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {

         MuiTooltip: {
            tooltip: {
               fontSize: "15px"
            }
         },
      }
   })
   downloadPlanAgreement(pp_id) {
      this.props.downloadPlanAgreement(pp_id)
   }
   render() {
      const { verify_err, selectedVerifyUserQuestions } = this.state;
      const { application_details, application_plans, invoiceDetails, amountDetails } = this.props;

      let totalLateAmount = 0;
      let totalFinCharge = 0;
      //var uniqueLoanAmount = application_plans && [...new Set(application_plans.map(item => item.amount))];
      var uniqueLoanAmount = 0;
      var uniqueRemainingAmount = 0;
      application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            accumulator[currentValue.pp_id] = { pp_id: currentValue.pp_id, amount: currentValue.amount };
            uniqueLoanAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.loan_amount : 0;
            uniqueRemainingAmount += (currentValue.plan_status == 1 || currentValue.plan_status == 8) ? currentValue.remaining_amount : 0;
         }
         return accumulator;
      }, []);
      //var uniqueLoanAmount = amountDetails && [...new Set(amountDetails.map(item => item.amount_rcvd))];
      var uniqueLateCount = application_plans && [...new Set(application_plans.map(item => item.late_count))];
      var uniqueMissedCount = application_plans && [...new Set(application_plans.map(item => item.missed_count))];


      // get plan details group by

      var planDetails = application_plans && application_plans.reduce(function (accumulator, currentValue, currentindex) {
         if (!accumulator[currentValue.pp_id]) {
            // get recived amount
            var totalAmt = amountDetails && amountDetails.reduce(function (accumulator, currentplan, planindex) {
               if (planindex == 0) {
                  accumulator = 0;
               }
               accumulator += (currentplan.pp_id == currentValue.pp_id) ? currentplan.amount_rcvd : 0;
               return accumulator;
            }, 0);
            accumulator[currentValue.pp_id] = { paid_flag: currentValue.paid_flag, pp_id: currentValue.pp_id, loan_amount: currentValue.loan_amount, amount: currentValue.remaining_amount, monthly_amount: currentValue.monthly_amount, recived: totalAmt, discounted_interest_rate: currentValue.discounted_interest_rate, payment_term_month: currentValue.payment_term_month, date_created: currentValue.date_created, procedure_date: currentValue.procedure_date, plan_number: currentValue.plan_number, status: currentValue.plan_status, invoice_exist: currentValue.invoice_exist, note: currentValue.note, status_name: currentValue.status_name };
         } else {
            //accumulator[currentValue.pp_id].recived += currentValue.amount_rcvd;
         }

         return accumulator;
      }, []);
      var groupArrays = invoiceDetails && invoiceDetails.map((data, idx) => {
         var nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id == data.invoice_id);
         nextDetailsDetails = this.props.invoicePlan.filter(x => x.invoice_id > data.invoice_id && x.pp_id == nextDetailsDetails[0].pp_id);
         data.nextDeferred = 0;
         if (nextDetailsDetails.length > 0) {
            var checkNextInvoice = nextDetailsDetails.reduce(function (a, b) {
               return new Date(a.due_date) < new Date(b.due_date) ? a : b;
            });
            if(Object.keys(checkNextInvoice).length !== 0){
               data.nextDeferred = 1;
            }
         }
         //console.log(invoiceDetails[idx+1])
         data.checkPartialPaid = (invoiceDetails.length > (idx+1)) ? 1 :0
         if(invoiceDetails[idx+1] !== undefined && invoiceDetails[idx+1].due_date == data.due_date){
            data.checkPartialPaid = 0;   
         }
         if (data.on_fly == 1 && nextDetailsDetails.length > 0 && data.invoice_status == 3 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else if (invoiceDetails.length > (idx + 1) && data.invoice_status != 1 && data.invoice_status != 4 && data.on_fly != 1 && data.invoice_status != 5 && data.invoice_status != 0) {
            data.missed_flag = 1;
         } else {
            data.missed_flag = 0;
         }
         
         //data.fin_charge_amt = (data.fin_charge_amt != null) ? (data.finpct != null) ? (parseFloat(data.fin_charge_amt) - (parseFloat(data.finpct) * parseFloat(datafin_charge_amt)) / 100) : parseFloat(data.fin_charge_amt) : 0;
         //data.late_fee_received = (data.late_fee_received != null) ? (data.latepct != null) ? (parseFloat(data.late_fee_received) - (parseFloat(data.latepct) * parseFloat(data.late_fee_received)) / 100) : parseFloat(data.late_fee_received) : 0;
         data.fin_charge_received = (data.invoice_status == 3)? data.fin_charge_due:data.fin_charge_received;
         data.late_fee_received = (data.invoice_status == 3)?data.late_fee_due:data.late_fee_received;

         //data.previous_fin_charge = (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
         //data.previous_late_fee = (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;
         data.previous_fin_charge = parseFloat(data.previous_fin_charge);
         data.previous_late_fee = parseFloat(data.previous_late_fee);

         data.total_due = data.payment_amount;
         data.paid_amount1 = (data.invoice_status == 1 || data.invoice_status == 4) ? (data.paid_amount + data.fin_charge_received + data.late_fee_received + data.additional_amount + data.previous_fin_charge + data.previous_late_fee) : 0;
         data.total_due += (data.late_fee_received) ? data.late_fee_received : 0;
         data.total_due += (data.fin_charge_received) ? data.fin_charge_received : 0;
         data.total_due += (data.previous_late_fee) ? data.previous_late_fee : 0;
         data.total_due += (data.previous_fin_charge) ? data.previous_fin_charge : 0;

         if (idx != 0) {

            data.previous_blc += (data.previous_fin_charge != null) ? (data.finpct != null) ? (parseFloat(data.previous_fin_charge) - (parseFloat(data.finpct) * parseFloat(data.previous_fin_charge)) / 100) : parseFloat(data.previous_fin_charge) : 0;
            data.previous_blc += (data.previous_late_fee != null) ? (data.latepct != null) ? (parseFloat(data.previous_late_fee) - (parseFloat(data.latepct) * parseFloat(data.previous_late_fee)) / 100) : parseFloat(data.previous_late_fee) : 0;
         }
         if (data.invoice_status == 1 || data.invoice_status == 4) {
            totalLateAmount += data.late_fee_received + data.previous_late_fee;
            totalFinCharge += data.fin_charge_received + data.previous_fin_charge;
         }
         data.finalAmt = (data.paid_amount1 <= data.total_due) ? data.total_due - data.paid_amount1 : 0;
         return data;
      })
      var nextDueDate = '';
      if (invoiceDetails !== undefined && invoiceDetails.length > 0) {
         /*var getTrem = application_plans.reduce(function (a, b) {
            return a.payment_term_month < b.payment_term_month ? b : a;
         });
         if (getTrem.payment_term_month > invoiceDetails.length && (invoiceDetails[invoiceDetails.length - 1].invoice_status == 1 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 4 || moment(moment(invoiceDetails[invoiceDetails.length - 1].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')))) {
            //if (1) {

            var newDate = moment(invoiceDetails[invoiceDetails.length - 1].due_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
            newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
            nextDueDate = moment(newDate).format('MM/DD/YYYY');
         }*/
         var activePlan = application_plans.filter(function (data, idx) {
            
            if (data.payment_term_month > data.installments_count && data.plan_status == 1 && (invoiceDetails[invoiceDetails.length - 1].invoice_status == 0 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 1 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 5 || invoiceDetails[invoiceDetails.length - 1].invoice_status == 4 || moment(moment(invoiceDetails[invoiceDetails.length - 1].due_date, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment().format('YYYY-MM-DD')))) {
               
               if (nextDueDate == '' && data.installments_count == data.payment_term_month) {
                  nextDueDate = '';
               } else if (nextDueDate != '' && new Date(nextDueDate) > new Date(data.last_plan_due) && data.last_plan_due !== null) {
                  nextDueDate = data.last_plan_due;
               } else if (nextDueDate == '' && data.last_plan_due !== null) {
                  nextDueDate = data.last_plan_due;
               }
            }
         });
         //console.log(nextDueDate)
         if (nextDueDate) {
            var newDate = moment(nextDueDate, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
            newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
            nextDueDate = moment(newDate).format('MM/DD/YYYY');
         }

      } else {
         if (application_plans) {
            var activePlan = application_plans.filter(x => x.status == 1 && x.provider_invoice_status == 4);
            if (activePlan.length > 0) {
               var nextInvoice = activePlan.reduce(function (a, b) {
                  return new Date(a.procedure_date) < new Date(b.procedure_date) ? a : b;
               });
               if (moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('DD') > 15) {
                  var newDate = moment(nextInvoice.procedure_date, "MM/DD/YYYY").add((1), 'months').format('MM/DD/YYYY');
                  newDate = new Date(moment(newDate, "MM/DD/YYYY").format('YYYY'), moment(newDate, "MM/DD/YYYY").format('MM'), 0);
                  nextDueDate = newDate;
               } else {
                  var newDate = new Date(moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('YYYY'), moment(nextInvoice.procedure_date, "MM/DD/YYYY").format('MM'), 0);
                  nextDueDate = newDate;
               }
            }

         }

      }
      //console.log(nextDueDate)
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.customerPlan" />} match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i> Back</Link>
                           </li>
                           <li>
                              <a to="javascript:void(0)" onClick={() => this.onVerifyUser()} title="Back"><i className="zmdi zmdi-check-circle"></i> Verify Customer</a>
                           </li>
                           <li>
                              <a href="javascript:void(0)" onClick={this.adminPlanDetails.bind(this)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                           </li>
                        </ul>
                     </div>
                     {this.props.application_plans &&
                        <div className="p-10">

                           <h1 className="text-center mb-20">Customer Account</h1>
                           {this.props.application_plans[0].plan_updated == 1 &&
                              <div className="alert alert-danger alert-dismissible fade show">
                                 Now this plan apply new Interest rate due to missed payment. Available Credit has been locked call HPS at (919) 600-5526 for any information.
                              </div>
                           }
                           <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Customer Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-nowrap"><strong>Application No :</strong></td>
                                          <td>{(application_details) ? application_details[0].application_no : '-'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Name :</strong></td>
                                          <td className="text-capitalize">{(application_details) ? (application_details[0].f_name + ' ' + application_details[0].m_name + ' ' + application_details[0].l_name) : '-'}</td>
                                       </tr>

                                       <tr>
                                          <td><strong>Address :</strong></td>
                                          <td>{(application_details) ? (application_details[0].address1 + ', ' + application_details[0].City + ', ' + application_details[0].name + ', ' + application_details[0].zip_code) : '-'}

                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Phone :</strong></td>
                                          <td>{(application_details) ? application_details[0].peimary_phone : '-'}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Loan Information</th>
                                       </tr>
                                       <tr>
                                          <td><strong>Line Of Credit :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].approve_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Available Balance :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(application_details[0].remaining_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Principal Amount :</strong></td>
                                          <td>{(application_details) ? '$' + parseFloat(uniqueLoanAmount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td><strong>Outstanding Principal :</strong></td>
                                          <td>
                                             ${parseFloat(uniqueRemainingAmount).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Payment Information</th>
                                       </tr>

                                       <tr>
                                          <td><strong>Late Payments :</strong></td>
                                          <td>
                                             {uniqueLateCount.reduce((a, b) => a + b, 0)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Payments Missed :</strong></td>
                                          <td>
                                             {uniqueMissedCount.reduce((a, b) => a + b, 0)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Late Fees :</strong></td>
                                          <td>
                                             ${parseFloat(totalLateAmount).toFixed(2)}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td><strong>Financial Charges :</strong></td>
                                          <td>
                                             ${parseFloat(totalFinCharge).toFixed(2)}
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>

                           <div className="table-responsive mb-5 pymt-history">
                              <h3 className="text-center mb-10">Payment Plans</h3>
                              <MuiThemeProvider theme={this.getMuiTheme()}>
                                 <table className="table table-borderless admin-application-payment-plan">
                                    <thead>
                                       <tr>
                                          <th>Plan ID</th>
                                          <th>Principal Amt</th>
                                          <th>Outstanding Principal Amt</th>
                                          <th>Payment Term</th>
                                          <th>APR(%)</th>
                                          <th>Monthly Payment</th>
                                          <th>Date Created</th>
                                          <th>Procedure Date</th>
                                          <th>Status</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>

                                       {planDetails.map(function (row, idx) {
                                          return (<tr key={idx}>
                                             <td>{row.pp_id}</td>
                                             <td>${(row.loan_amount).toFixed(2)}</td>
                                             <td>${(row.amount.toFixed(2) - row.recived.toFixed(2)).toFixed(2)}</td>
                                             <td>{row.payment_term_month} Month</td>
                                             <td>{parseFloat(row.discounted_interest_rate).toFixed(2)}%</td>
                                             <td>${parseFloat(row.monthly_amount).toFixed(2)}</td>
                                             <td>{row.date_created}</td>
                                             <td>{row.procedure_date}</td>
                                             <td>{
                                                (row.status == 0)
                                                   ?
                                                   <span>{row.status_name} <Tooltip id="tooltip-top-end" title={row.note} placement="top-end"><i className="zmdi zmdi-comment-text"></i></Tooltip></span>
                                                   :
                                                   row.status_name
                                             }</td>
                                             <td>{
                                                (row.status == 4 && row.invoice_exist === null) ? 
                                                   <Link to={`/admin/credit-application/option-to-close/${this.enc(row.pp_id.toString())}`} title="Advanced plan options"><i className="zmdi zmdi-settings icon-fr"></i></Link> 
                                                   : 
                                                   (row.status == 2) ? 
                                                      <div>
                                                      <a className="" href="javascript:void(0)" onClick={() => this.downloadPlanAgreement(row.pp_id)}><i className="mr-10 zmdi zmdi-download zmdi-hc-lg"></i></a>
                                                      <Link to={`/admin/credit-application/upload-agreement/${this.enc(row.pp_id.toString())}`} title="Upload Signed Agreement"><i className="zmdi zmdi-upload zmdi-hc-lg"></i></Link> 
                                                      </div>
                                                      : 
                                                      (row.status == 7) ?
                                                         '-'
                                                         :
                                                         (row.status == 3) ? 
                                                            <Link to={`/admin/credit-application/plan/review-agreement/${this.enc(row.pp_id.toString())}`} title="Review Agreement"><i className="zmdi zmdi-assignment zmdi-hc-lg"></i></Link> 
                                                            : 
                                                            (row.status == 4) ? 
                                                               <a href="javascript:void(0)" title="Document Approved"><i className="zmdi zmdi-check-all"></i></a> : 
                                                               (row.status != 0 && row.status != 8) ? 
                                                                  <Link to={`/admin/credit-application/option-to-close/${this.enc(row.pp_id.toString())}`} title="Advanced plan options"><i className="zmdi zmdi-settings icon-fr"></i></Link> 
                                                                  : '-'
                                                }</td>
                                          </tr>)
                                       }.bind(this))
                                       }
                                    </tbody>
                                 </table>
                              </MuiThemeProvider>
                           </div>

                           <div className="table-responsive mb-40 pymt-history">
                              <h3 className="text-center mb-10">Invoice History {(nextDueDate != '') ? <a href="javascript:void(0);" onClick={() => this.createInvoce(nextDueDate, this.dec(this.props.match.params.appid))} title="Create Invoice"><i className="zmdi zmdi-file-plus icon-fr"></i></a> : ''}</h3>
                              <table className="table table-borderless admin-application-payment-plan" ref="slide">
                                 <thead>
                                    <tr>
                                       <th>S. No.</th>
                                       <th>Invoice No.</th>
                                       <th>Payment Date</th>
                                       <th>Due Date</th>
                                       <th>Prev Bal</th>
                                       <th>Monthly Payment</th>
                                       <th>Late Fee</th>
                                       <th>Fin Charge</th>
                                       <th>Total Due</th>
                                       <th>Min Due</th>
                                       <th>Additional Amt</th>
                                       <th>Paid</th>
                                       <th>Balance</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    {groupArrays && groupArrays.map(function (row, idx) {
                                       return <tr key={idx} className={idx}>
                                          <td>{idx + 1}</td>
                                          <td>{row.invoice_number}</td>
                                          <td>{(row.payment_date) ? row.payment_date : '-'}</td>
                                          <td>{row.due_date}</td>
                                          <td>{(row.previous_blc > 0) ? '$' + parseFloat(row.previous_blc).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.payment_amount).toFixed(2)}</td>
                                          <td>{(row.late_fee_received) ? '$' + parseFloat(row.late_fee_received).toFixed(2) : '-'}</td>
                                          <td>{(row.fin_charge_received) ? '$' + parseFloat(row.fin_charge_received).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>${parseFloat(row.total_due).toFixed(2)}</td>
                                          <td>{(row.additional_amount > 0) ? '$' + parseFloat(row.additional_amount).toFixed(2) : '-'}</td>
                                          <td>{(row.paid_amount1 > 0) ? '$' + parseFloat(row.paid_amount1).toFixed(2) : '-'}</td>
                                          <td>${parseFloat(row.finalAmt).toFixed(2)}</td>
                                          <td>

                                             {(row.missed_flag == 1) ?
                                                'Missed'
                                                :
                                                (row.invoice_status == 1) ?
                                                   row.invoice_status_name
                                                   :
                                                   (row.invoice_status == 3) ?
                                                      row.invoice_status_name
                                                      :
                                                      (row.invoice_status == 4) ?
                                                         row.invoice_status_name
                                                         :
                                                         row.invoice_status_name
                                             }
                                          </td>
                                          <td>
                                             {(row.missed_flag == 1) ?
                                                '-'
                                                :
                                                (row.invoice_status == 3 || row.invoice_status == 2 || (row.invoice_status == 5 && row.nextDeferred == 0)) ?
                                                   <div><Link to={`/admin/credit-application/payment/${this.enc(row.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>
                                                      <Link to={`/admin/credit-application/invoice/${this.enc(row.invoice_id.toString())}`} title="Invoice"><i className="zmdi zmdi-file-text icon-fr"></i></Link></div>
                                                   :
                                                   (row.invoice_status == 1 || row.invoice_status == 4) ?
                                                      (row.invoice_status == 1) ? 
                                                      <Link to={`/admin/credit-application/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>
                                                      :
                                                      (row.checkPartialPaid == 1) ?
                                                      <Link to={`/admin/credit-application/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link>
                                                      :
                                                      <div><Link to={`/admin/credit-application/payment/${this.enc(row.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link>
                                                      <Link to={`/admin/credit-application/receipt/${this.enc(row.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link></div>
                                                      :
                                                      '-'
                                             }</td>
                                       </tr>
                                    }.bind(this))
                                    }
                                 </tbody>
                              </table>
                           </div>

                        </div>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                     {this.props.down_loading &&
                        <RctSectionLoader />
                     }
                  </RctCard>

                  <Modal isOpen={this.state.verifyModal}>
                     <ModalHeader toggle={() => this.verifyModalClose()}>
                        Verify Customer
                     </ModalHeader>
                     <ModalBody>
                        <VerifyCustomerForm
                           addErr={verify_err}
                           questionList={selectedVerifyUserQuestions}
                           onChangeVerifyUser={this.onChangeVerifyUser.bind(this)}
                           check_fullname={this.state.check_fullname}
                           check_dob={this.state.check_dob}
                           check_ssn={this.state.check_ssn}
                           check_question={this.state.check_question}
                           check_answer={this.state.check_answer}
                           verifyUserDetail={this.props.userLockDetail}
                           selectedQuestion={this.state.selectedQuestion}
                        />
                     </ModalBody>
                     <ModalFooter>
                        <Button
                           variant="contained"
                           color="primary"
                           className={(this.validateVerifyForm()) ? "text-white btn-success" : "text-white btn-error"}
                           onClick={() => this.verifyUserAccount()}
                           disabled={!this.validateVerifyForm()}

                        >Verify</Button>

                        <Button variant="contained" className="text-white btn-danger" onClick={() => this.verifyModalClose()}>Cancel</Button>
                     </ModalFooter>

                     {this.state.loading &&
                        <RctSectionLoader />
                     }

                  </Modal>
                  <DeleteConfirmationDialog
                     ref="deleteConfirmationDialog"
                     title="Are You Sure Want To Create invoice?"
                     message="Transaction cannot be rolled back."
                     onConfirm={() => this.createInvoiceConfirm()}
                  />
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication, PaymentPlanReducer }) => {
   //console.log(PaymentPlanReducer)
   const { nameExist, isEdit, unlockStatus, userLockDetail } = authUser;
   const { loading, application_details, invoiceDetails, application_plans, amountDetails, userQuestions, invoicePlan } = PaymentPlanReducer;
   const { down_loading } = creditApplication
   return { loading, application_details, invoiceDetails, application_plans, amountDetails, userQuestions, unlockStatus, userLockDetail, invoicePlan, down_loading }
}

export default connect(mapStateToProps, {
   planDetails, clearRedirectURL, allPlanDetails, createInvoice, adminPlanDetailsDownload, unlockUserAccount, getLockUserDetail, downloadPlanAgreement
})(PlanDetails);