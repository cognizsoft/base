/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import DeleteTypeConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteTypeConfirmationDialog';
import { isEmpty, isDecimals, isNumeric, pointDecimals } from '../../validator/Validator';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import datetime from 'date-and-time';
import AppConfig from 'Constants/AppConfig';
import SignatureCanvas from 'react-signature-canvas';
import { NotificationManager } from 'react-notifications';

import {
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import FormHelperText from '@material-ui/core/FormHelperText';

import {
   viewOptionToClose, closeCurrentPlan, getClosePlanDetails
} from 'Actions';
class OptionToClose extends Component {

   state = {
      data: {
         action_type: '',
         confirmation_provider: 0,
         note: '',
         adjusted_amount: '',
         revised_int: 0,
         loan_type: '1',
         monthly_amount: '',
         //procedure_amount: '',
         //procedure_date: '',
         loan_amount: '',
         expiry_date: '',
         agreement_date: '',
         name_of_borrower: '',
         agree_with: 0,
         provider_refund: '',
         customer_refund: '',
         customer_refund_act: '',
         customer_pay_method: '1',
         check_date: '',
         check_no: '',
         ach_account_no: '',
         ach_routing_no: '',
         ach_bank_name: '',
         iou_flag: null,
      },
      dataError: {},

      closePlan: false,
      RecAmount: 0,
      RecIntAmount: 0,
      RecFinAmount: 0,
      RecLatAmount: 0,
      RecPrAmount: 0,
      RevisedIntAmt: 0,
      agreementView: false,
      planEst: {},
      expiry_date: '',
      agreement_date: '',

      customerSignURL: null,
      witnessSignURL: null,
      witnessSignAllow: true,
      customerSignAllow: true,
   }

   customerSigPad = {}
   witnessSigPad = {}
   clearCustomer = () => {
      this.customerSigPad.clear();
      let { data, dataError } = this.state;
      dataError['customerSignature'] = "Customer Signature required.";
      data['customerSignature'] = "";
      this.customerSigPad.on();
      this.setState({
         customerSignURL: null,
         data: data,
         dataError: dataError,
         customerSignAllow: true,
      })
   }
   trimCustomer = () => {
      let { data, dataError } = this.state;
      dataError['customerSignature'] = "";
      data['customerSignature'] = this.customerSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.customerSigPad.off();
      this.setState({
         customerSignURL: this.customerSigPad.getTrimmedCanvas().toDataURL('image/png'),
         data: data,
         dataError: dataError,
         customerSignAllow: false,
      })
      this.customerSigPad.clear();
   }
   clearWitness = () => {
      this.witnessSigPad.clear();
      let { data, dataError } = this.state;
      dataError['witnessSignature'] = "Witness Signature required.";
      data['witnessSignature'] = "";
      this.witnessSigPad.on();
      this.setState({
         witnessSignURL: null,
         data: data,
         dataError: dataError,
         witnessSignAllow: true,
      })
   }
   trimWitness = () => {
      let { data, dataError } = this.state;
      dataError['witnessSignature'] = "";
      data['witnessSignature'] = this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png');
      this.witnessSigPad.off();
      this.setState({
         witnessSignURL: this.witnessSigPad.getTrimmedCanvas().toDataURL('image/png'),
         data: data,
         dataError: dataError,
         witnessSignAllow: false,
      })
      this.witnessSigPad.clear();
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }

   componentDidMount() {
      const { data } = this.state;
      data.action_type = '';
      data.note = '';
      data.adjusted_amount = '';
      data.revised_int = 0;
      data.loan_type = 1;
      data.monthly_amount = '';
      data.loan_amount = '';

      let dataError = {}
      this.setState({ data: data, dataError: dataError })
      this.props.viewOptionToClose(this.dec(this.props.match.params.ppid));
   }
   onChnagePlanAction(key, value) {

      let { dataError, data } = this.state;
      switch (key) {
         case 'action_type':
            if (isEmpty(value)) {
               dataError[key] = "Monthly amount can't be blank";
            } else {
               this.setState({ closePlan: false })
               if (value == 1 || value == 2 || value == 3) {
                  this.props.getClosePlanDetails(this.dec(this.props.match.params.ppid))
               }
               dataError[key] = "";
            }
            break;
         case 'note':
            if (isEmpty(value)) {
               dataError[key] = "Note can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'adjusted_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Monthly amount can't be blank";
               data.revised_int = 0;
               this.setState({ data: data });
            } else if (!isDecimals(value)) {
               dataError[key] = "Monthly amount not valid";
               data.revised_int = 0;
               this.setState({ data: data });
               //} else if (value < this.props.planDetails.remaining_amount || value > (this.props.planDetails.remaining_amount + (this.state.RevisedIntAmt - this.state.RecIntAmount))) {
            } else if (value < this.props.planDetails.remaining_amount) {
               dataError[key] = "Adjusted Amount between Min Adjusted Amount and Total Due amount";
               data.revised_int = 0;
               this.setState({ data: data });
            } else {
               data.revised_int = value - this.props.planDetails.remaining_amount;
               this.setState({ data: data });
               dataError[key] = "";
            }
            break;
         case 'monthly_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Monthly amount can't be blank";
            } else if (!isDecimals(value)) {
               dataError[key] = "Monthly amount not valid";
            } else {
               dataError[key] = "";
            }
            break;
         case 'loan_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Loan amount can't be blank";
            } else if (!isDecimals(value)) {
               dataError[key] = "Loan amount not valid";
            } else if (value <= 0) {
               dataError[key] = "The loan amount should be greater than $0";
               //} else if (value < this.props.planDetails.remaining_amount || value > (this.props.planDetails.remaining_amount + (this.state.RevisedIntAmt - this.state.RecIntAmount))) {
            } else if (value < this.props.planDetails.remaining_amount) {
               dataError[key] = "The loan amount between Min Adjusted Amount and Total Due amount";
            }/*else if (parseFloat(this.state.remainingAmount) < parseFloat(value)) {
               dataError[key] = '"Loan amount exceeds the available credit" for override call HPS at (919) 600-5526';
            } else if (this.state.data.procedure_amount !== '' && parseFloat(this.state.data.procedure_amount) < parseFloat(value)) {
               dataError[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.data.procedure_amount !== '' && parseFloat(this.state.data.procedure_amount) >= parseFloat(value)) {
               dataError[key] = "";
               dataError['procedure_amount'] = "";
            } */else {
               dataError[key] = "";
            }
            break;
         /*case 'procedure_date':
            if (value == null) {
               dataError[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = '';
            }
            break;
         case 'procedure_amount':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               dataError[key] = "Procedure amount not valid";
            } else if (value < 100 || value > 250000) {
               dataError[key] = "The Procedure amount amount should be between $100 - $250,000";
            }
            else if (this.state.data.loan_amount !== '' && parseFloat(this.state.data.loan_amount) > parseFloat(value)) {
               dataError[key] = "Procedure amount greater than or equal to Loan amount";
            } else if (this.state.data.loan_amount !== '' && parseFloat(this.state.data.loan_amount) <= parseFloat(value)) {
               dataError[key] = "";
               dataError['loan_amount'] = "";
            } else {
               dataError[key] = '';
            }
            break;*/
         case 'name_of_borrower':
            if (isEmpty(value)) {
               dataError[key] = "Name of Borrower can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'agreement_date':
            if (value == null) {
               this.setState({ agreement_date: '' })
               dataError[key] = "Agreement date can't be blank";
            } else {
               this.setState({ agreement_date: value })
               value = moment(value).format('YYYY-MM-DD HH:mm:ss');
               dataError[key] = '';
            }
            break;
         case 'expiry_date':
            if (value == null) {
               this.setState({ expiry_date: '' })
            } else {
               this.setState({ expiry_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = '';
            }
            break;
         case 'agree_with':
            value = (this.state.data.agree_with) ? 0 : 1;
            break;
         case 'id_number':
            if (isEmpty(value)) {
               dataError[key] = "ID number can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'id_name':
            if (isEmpty(value)) {
               dataError[key] = "ID type can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'check_no':
            if (isEmpty(value)) {
               dataError[key] = "Check no. can't be blank.";
            } else if (isNumeric(value)) {
               dataError[key] = "Allow only numeric.";
            } else {
               dataError[key] = '';
            }
            break;
         case 'check_date':
            if (value == null) {
               this.setState({ check_date: '' })
               dataError[key] = "Enter valid check date";
            } else {
               this.setState({ check_date: value })
               value = moment(value).format('YYYY-MM-DD');
               dataError[key] = "";
            }
            break;
         case 'customer_refund':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "";
            } else if (!isDecimals(value)) {
               dataError[key] = "Customer Refundable amount not valid";
            } else if (this.state.data.customer_refund_act < value) {
               dataError[key] = "Customer amount should not be greater than the amount that is calculated";
            } else {
               dataError[key] = "";
            }
            break;
         case 'ach_bank_name':
            if (isEmpty(value)) {
               dataError[key] = "Bank field can't be blank";
            } else {
               dataError[key] = '';
            }
            break;
         case 'ach_routing_no':
            if (isEmpty(value)) {
               dataError[key] = "Txn ref no can't be blank";
            } else {
               dataError[key] = ''
            }
            break;
         case 'ach_account_no':
            if (isEmpty(value)) {
               dataError[key] = "Account no can't be blank";
            } else if (isNumeric(value)) {
               dataError[key] = "Allow only numeric.";
            } else {
               dataError[key] = ''
            }
            break;
         case 'provider_refund':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               dataError[key] = "";
            } else if (!isDecimals(value)) {
               dataError[key] = "Refund Due From Provider amount not valid";
            } else if (this.props.providerInvoiceDetails.invoice_status == 4 && this.props.providerInvoiceDetails.check_amount < value) {
               dataError[key] = "Provider amount should not be greater than the amount that is calculated";
            } else {
               dataError[key] = "";
            }
            break;


      }

      this.setState({
         data: {
            ...this.state.data,
            [key]: value
         }
      });
      this.setState({ dataError: dataError });
   }
   typeConfirm() {
      return true;
   }
   processPlanConfirm() {
      const { data } = this.state;
      data.confirmation_provider = 0;
      console.log(data)
      this.setState({ data: data })
      //DeleteTypeConfirmationDialog
      if (data.action_type == 3) {
         this.refs.deleteTypeConfirmationDialog.open();
      } else {
         this.refs.deleteConfirmationDialog.open();
      }
   }
   processPlanConfirmation() {
      const { data } = this.state;
      data.confirmation_provider = 1;
      this.setState({ data: data })
      this.refs.deleteConfirmationDialog.open();
   }
   processPlanAction() {
      const { data } = this.state;
      data.pp_id = this.dec(this.props.match.params.ppid);
      data.planEst = this.state.planEst;
      data.customerDetails = this.props.customerDetails;
      data.providerDetails = this.props.providerDetails;
      //return false;
      this.props.closeCurrentPlan(data);
      this.refs.deleteConfirmationDialog.close();
   }
   processAgreement() {
      //check type
      var date = datetime;
      //let now = new Date(this.state.data.procedure_date);
      let now = new Date();
      let { planEst } = this.state;
      let newTerm = this.props.closeTerm;
      if (this.state.data.loan_type == 1) {
         var perMonthInt = (this.props.closeInt / 12) / 100;

         var perMonth = this.state.data.loan_amount * (perMonthInt * Math.pow((1 + perMonthInt), newTerm)) / (Math.pow((1 + perMonthInt), newTerm) - 1);

         var monthlyPlan = [];
         var totalAmount = parseFloat(perMonth).toFixed(2) * newTerm;

         for (var i = 1; i <= newTerm; i++) {
            var month = {}
            if (date.format(now, 'DD') <= 15) {
               //let next_month = date.addMonths(now, (i - 1));
               let next_month = new Date(moment(now).add((i - 1), 'months'));
               next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
               next_month = date.format(next_month, 'MM/DD/YYYY');
               month.interest_id = newTerm;
               month.next_month = next_month;
               month.perMonth = parseFloat(perMonth).toFixed(2);
               monthlyPlan.push(month)
            } else {
               //let next_month = date.addMonths(now, (i));
               let next_month = new Date(moment(now).add((i), 'months'));
               next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
               next_month = date.format(next_month, 'MM/DD/YYYY');
               month.interest_id = newTerm;
               month.next_month = next_month;
               month.perMonth = parseFloat(perMonth).toFixed(2);
               monthlyPlan.push(month)
            }
         }


         planEst.interest_rate = this.props.closeInt;
         planEst.org_interest_rate = this.props.closeInt;
         planEst.plan_id = newTerm;
         planEst.term_month = newTerm;
         planEst.firstMonth = parseFloat(perMonth).toFixed(2);
         planEst.lastMonth = parseFloat(perMonth).toFixed(2);
         planEst.monthlyPlan = monthlyPlan;
         planEst.totalAmount = totalAmount.toFixed(2);

         // set state
         this.setState({ agreementView: true, planEst: planEst })
      } else {
         var perMonthInt = this.props.closeInt / 100;
         var newUTerm = -Math.log(parseFloat(((parseFloat(-perMonthInt) * this.state.data.loan_amount) / (12 * this.state.data.monthly_amount) + 1))) / Math.log((1 + parseFloat(perMonthInt) / 12));
         newUTerm = Math.ceil(newUTerm);
         if (newUTerm <= newTerm) {
            var monthlyPlan = [];
            var totalAmount = 0;
            var monthly_amount = this.state.data.monthly_amount;
            var perMonth = monthly_amount;
            var lastMonth = 0;
            var remainingAmt = this.state.data.loan_amount;
            for (var i = 1; i <= newUTerm; i++) {
               if (i < newUTerm) {
                  var redAmt = (((12 * monthly_amount) / perMonthInt) - this.state.data.loan_amount) * (Math.pow(1 + (perMonthInt / 12), i) - 1);
               } else {
                  var redAmt = 0;
                  monthly_amount = (remainingAmt * (1 + (perMonthInt / 12))).toFixed(2)
               }
               lastMonth = monthly_amount;
               redAmt = parseFloat(parseFloat(redAmt).toFixed(2));
               remainingAmt = this.state.data.loan_amount - redAmt;
               totalAmount += parseFloat(monthly_amount);
               var month = {}
               if (date.format(now, 'DD') <= 15) {
                  let next_month = new Date(moment(now).add((i - 1), 'months'));
                  next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                  next_month = date.format(next_month, 'MM/DD/YYYY');
                  month.interest_id = newUTerm;
                  month.next_month = next_month;
                  month.perMonth = parseFloat(monthly_amount).toFixed(2);
                  monthlyPlan.push(month)
               } else {
                  let next_month = new Date(moment(now).add((i), 'months'));
                  next_month = new Date(date.format(next_month, 'YYYY'), date.format(next_month, 'MM'), 0);
                  next_month = date.format(next_month, 'MM/DD/YYYY');
                  month.interest_id = newUTerm
                  month.next_month = next_month;
                  month.perMonth = parseFloat(monthly_amount).toFixed(2);
                  monthlyPlan.push(month)
               }
            }

            planEst.interest_rate = this.props.closeInt;
            planEst.org_interest_rate = this.props.closeInt;
            planEst.plan_id = newUTerm;
            planEst.term_month = newUTerm;
            planEst.firstMonth = parseFloat(perMonth).toFixed(2);
            planEst.lastMonth = parseFloat(lastMonth).toFixed(2);
            planEst.monthlyPlan = monthlyPlan;
            planEst.totalAmount = totalAmount.toFixed(2);

            // set state
            this.setState({ agreementView: true, planEst: planEst })
         } else {
            NotificationManager.error('Payment plan not found');
         }
      }

   }
   validateAction() {
      if (this.state.data.action_type === '') {
         return (
            this.state.dataError.action_type === '' &&
            this.state.dataError.note === ''
         )
      } else if (this.state.data.action_type == 0) {
         return (
            this.state.dataError.action_type === '' &&
            this.state.dataError.note === '' &&
            this.props.planInvoiceExist == 0
         )
      } else if (this.state.data.action_type == 1) {
         return (
            this.state.dataError.action_type === '' &&
            this.state.dataError.note === '' &&
            this.state.dataError.adjusted_amount === ''
         )
      } else if (this.state.data.action_type == 2) {
         return (
            this.state.dataError.action_type === '' &&
            this.state.dataError.note === '' &&
            /*this.state.dataError.procedure_amount === '' &&
            this.state.dataError.procedure_date === '' &&*/
            this.state.dataError.loan_amount === '' &&
            this.state.dataError.id_number === '' &&
            this.state.data.agree_with === 1 &&
            this.state.dataError.id_name === '' &&
            this.state.dataError.name_of_borrower === '' &&
            this.state.dataError.agreement_date === '' &&
            this.state.dataError.witnessSignature === '' &&
            this.state.dataError.customerSignature === ''
         )
      } else if (this.state.data.action_type == 3) {
         if (this.state.data.customer_refund > 0) {
            if (this.state.data.customer_pay_method == 1) {
               return (
                  this.state.dataError.action_type === '' &&
                  this.state.dataError.note === '' &&
                  this.state.dataError.customer_refund === '' &&
                  this.state.dataError.provider_refund === '' &&
                  this.state.dataError.ach_bank_name === '' &&
                  this.state.dataError.ach_routing_no === '' &&
                  this.state.dataError.ach_account_no === ''
               )
            } else {
               return (
                  this.state.dataError.action_type === '' &&
                  this.state.dataError.note === '' &&
                  this.state.dataError.customer_refund === '' &&
                  this.state.dataError.provider_refund === '' &&
                  this.state.dataError.check_no === '' &&
                  this.state.dataError.check_date === ''
               )
            }

         } else {
            return (
               this.state.dataError.action_type === '' &&
               this.state.dataError.note === '' &&
               this.state.dataError.customer_refund === '' &&
               this.state.dataError.provider_refund === ''
            )
         }
      }
   }
   validateAgreement() {
      return (
         this.state.dataError.action_type === '' &&
         this.state.dataError.note === '' &&
         /*this.state.dataError.procedure_amount === '' &&
         this.state.dataError.procedure_date === '' &&*/
         this.state.dataError.loan_amount === '' &&
         this.state.data.action_type == 2
      )
   }
   validateConfirmation() {
      return (
         this.state.dataError.action_type === '' &&
         this.state.dataError.note === '' &&
         this.state.data.action_type == 0 &&
         this.props.planInvoiceExist == 1
      )
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   closeBack() {
      let { data, dataError } = this.state;
      data.id_number = "";
      data.id_name = "";
      data.expiry_date = '';
      data.agreement_date = '';
      data.name_of_borrower = '';
      data.agree_with = 0;
      dataError.loan_amount = '';
      this.setState({
         agreementView: false,
         dataError: dataError,
         data: data,
         planEst: {},
         expiry_date: '',
         agreement_date: '',

         customerSignURL: null,
         witnessSignURL: null,
         witnessSignAllow: true,
         customerSignAllow: true,
      })
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {

         MuiSvgIcon: {
            root: {
               'color': "#0E5D97"
            }
         },
         MuiFormLabel: {
            root: {
               'font-size': '12px',
               "font-weight": 600,
               color: '#464D69'
            }
         }

      }
   })
   componentWillReceiveProps(nextProps) {
      if (this.state.closePlan == false && nextProps.planDetails !== undefined) {
         let TotalRecAmt = 0;
         let RecIntAmount = 0;
         let RecFinAmount = 0;
         let RecLatAmount = 0;
         let RecPrAmount = 0;

         let RevisedIntAmt = 0;
         nextProps.planInvoiceDetails.map((data, idx) => {
            //intrest rate
            if (data.invoice_status == 1 || data.invoice_status == 4) {
               if (data.interest == 0) {
                  RecIntAmount += data.installment_interest;
                  TotalRecAmt += data.installment_interest;
               } else {
                  RecIntAmount += data.installment_interest - data.interest;
                  TotalRecAmt += data.installment_interest - data.interest;
               }
               // principal amt
               if (data.principal_amount == 0) {
                  RecPrAmount += data.installment_principal_amount;
                  TotalRecAmt += data.installment_principal_amount;
               } else {
                  RecPrAmount += data.installment_principal_amount - data.principal_amount;
                  TotalRecAmt += data.installment_principal_amount - data.principal_amount;
               }
               // finical charge
               if (data.fin_charge > 0) {
                  RecFinAmount += data.fin_charge;
                  TotalRecAmt += data.fin_charge;
               }
               // late fee
               if (data.late_fee_received > 0) {
                  RecLatAmount += data.late_fee_received;
                  TotalRecAmt += data.late_fee_received;
               }
            }

         })
         if (nextProps.planDetails.loan_type == 1) {
            let perMonthInt = (nextProps.closeInt / 12) / 100;
            let perMonth = nextProps.planDetails.loan_amount * (perMonthInt * Math.pow((1 + perMonthInt), nextProps.planDetails.term)) / (Math.pow((1 + perMonthInt), nextProps.planDetails.term) - 1);
            RevisedIntAmt = parseFloat(perMonth.toFixed(2)) * nextProps.planDetails.term;
            RevisedIntAmt = parseFloat(RevisedIntAmt.toFixed(2));
            RevisedIntAmt = RevisedIntAmt - nextProps.planDetails.loan_amount;
            RevisedIntAmt = parseFloat(RevisedIntAmt.toFixed(2));
         } else {
            let perMonthInt = nextProps.closeInt / 100;
            var newTerm = -Math.log(parseFloat(((parseFloat(-perMonthInt) * nextProps.planDetails.loan_amount) / (12 * nextProps.planDetails.monthly_amount) + 1))) / Math.log((1 + parseFloat(perMonthInt) / 12));
            var term_month = Math.ceil(newTerm);

            var remainingAmt = 0;
            var monthly_amount = nextProps.planDetails.monthly_amount;
            for (var i = 1; i <= term_month; i++) {
               if (i < term_month) {
                  var redAmt = (((12 * monthly_amount) / perMonthInt) - nextProps.planDetails.loan_amount) * (Math.pow(1 + (perMonthInt / 12), i) - 1);
               } else {
                  var redAmt = 0;
                  monthly_amount = (remainingAmt * (1 + (perMonthInt / 12))).toFixed(2)
               }
               remainingAmt = nextProps.planDetails.loan_amount - redAmt;
               RevisedIntAmt += parseFloat(monthly_amount);
            }
            RevisedIntAmt = RevisedIntAmt - nextProps.planDetails.loan_amount;
            RevisedIntAmt = parseFloat(RevisedIntAmt.toFixed(2));
         }
         TotalRecAmt = parseFloat(TotalRecAmt).toFixed(2)

         RecIntAmount = parseFloat(RecIntAmount).toFixed(2)
         RecFinAmount = parseFloat(RecFinAmount).toFixed(2)
         RecPrAmount = parseFloat(RecPrAmount).toFixed(2)
         RecLatAmount = parseFloat(RecLatAmount).toFixed(2)
         let { dataError, data } = this.state;
         data.procedure_amount = nextProps.planDetails.remaining_amount;
         data.loan_amount = parseFloat((nextProps.planDetails.remaining_amount + RevisedIntAmt).toFixed(2))
         // refund process code for default value
         //(this.props.providerInvoiceDetails != undefined && this.props.providerInvoiceDetails.invoice_status == 4) ? '$' + parseFloat(this.props.providerInvoiceDetails.check_amount).toFixed(2) : '$0.00'
         data.customer_refund = parseFloat(TotalRecAmt).toFixed(2);
         data.customer_refund_act = parseFloat(TotalRecAmt).toFixed(2);
         data.provider_refund = (nextProps.providerInvoiceDetails.invoice_status == 4) ? parseFloat(nextProps.providerInvoiceDetails.check_amount).toFixed(2) : '0.00';
         data.iou_flag = nextProps.providerRefundDetails.iou_flag;
         dataError.customer_refund = '';
         dataError.provider_refund = '';
         //dataError.procedure_amount = '';
         dataError.loan_amount = '';
         this.setState({
            RecAmount: TotalRecAmt,
            RecIntAmount: RecIntAmount,
            RecFinAmount: RecFinAmount,
            RecPrAmount: RecPrAmount,
            RecLatAmount: RecLatAmount,
            RevisedIntAmt: RevisedIntAmt,
            data: data,
            dataError: dataError,
            closePlan: true
         })
      }

   }
   render() {
      const oneYearFromNow = new Date();
      oneYearFromNow.setDate(oneYearFromNow.getDate() + 365);

      const twentyYearFromNow = new Date();
      twentyYearFromNow.setDate(twentyYearFromNow.getDate() + 365 * 20);

      if (this.props.planCloseStatus === 1) {
         //return (<Redirect to={`/admin/credit-applications/3`} />);
         this.props.history.goBack(-1)

      }
      //(this.props.planInvoiceExist == 1 && data.option_id == 0) || 
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.option-to-plan-close" />} match={this.props.match} />
            {this.props.planCloseOption &&
               <RctCard>
                  {this.state.agreementView == false &&
                     <div className="modal-body page-form-outer text-left">
                        <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt">

                           <div className="add-card w-50 mr-2">
                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="2">Customer Information</th>
                                    </tr>
                                    <tr>
                                       <td className="text-nowrap"><strong>Account No :</strong></td>
                                       <td>{(this.props.customerDetails) ? this.props.customerDetails.patient_ac : '-'}</td>
                                       <td className="text-nowrap"><strong>Application No :</strong></td>
                                       <td>{(this.props.customerDetails) ? this.props.customerDetails.application_no : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Name :</strong></td>
                                       <td className="text-capitalize">{(this.props.customerDetails) ? (this.props.customerDetails.first_name + ' ' + this.props.customerDetails.last_name) : '-'}</td>
                                       <td><strong>Phone :</strong></td>
                                       <td>{(this.props.customerDetails) ? this.props.customerDetails.peimary_phone : '-'}</td>
                                    </tr>

                                    <tr>
                                       <td><strong>Address :</strong></td>
                                       <td colSpan="3">{(this.props.customerDetails) ? (this.props.customerDetails.address1 + ', ' + this.props.customerDetails.City + ', ' + this.props.customerDetails.state_name + ', ' + this.props.customerDetails.zip_code) : '-'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>

                           </div>

                           <div className="add-card w-50 ml-2">

                              <table>
                                 <tbody>
                                    <tr>
                                       <th colSpan="4">Provider Information</th>
                                    </tr>
                                    <tr>
                                       <td className="text-nowrap"><strong>Account No :</strong></td>
                                       <td colSpan="3">{(this.props.customerDetails) ? this.props.customerDetails.patient_ac : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td><strong>Name :</strong></td>
                                       <td className="text-capitalize">{(this.props.providerDetails) ? (this.props.providerDetails.name) : '-'}</td>
                                       <td><strong>Phone :</strong></td>
                                       <td>{(this.props.providerDetails) ? this.props.providerDetails.primary_phone : '-'}</td>
                                    </tr>

                                    <tr>
                                       <td><strong>Address :</strong></td>
                                       <td colSpan="3">{(this.props.providerDetails) ? (this.props.providerDetails.address1 + ', ' + this.props.providerDetails.city + ', ' + this.props.providerDetails.state_name + ', ' + this.props.providerDetails.zip_code) : '-'}
                                       </td>
                                    </tr>

                                 </tbody>
                              </table>

                           </div>

                        </div>

                        <div className="row">
                           <div className="col-md-12">
                              <MuiThemeProvider theme={this.getMuiTheme()}>
                                 <FormControl component="fieldset" required>
                                    <FormLabel component="legend">Choose Your Option</FormLabel>
                                    <RadioGroup row aria-label="action_type" name="action_type" value={this.state.data.action_type.toString()} onChange={(e) => this.onChnagePlanAction('action_type', e.target.value)} >
                                       {this.props.planCloseOption.map(function (data, idx) {
                                          return <FormControlLabel key={idx} value={data.option_id.toString()} control={<Radio />} disabled={((this.props.customerDetails.plan_status == 9) || (this.props.customerDetails.plan_status == 1 && data.option_id == 3) || (this.props.customerDetails.plan_status == 10 && data.option_id != 3) || (this.props.planInvoiceExist == 0 && data.option_id == 1) || (this.props.planInvoiceExist == 0 && data.option_id == 2) || (this.props.customerDetails.plan_status == 4 && data.option_id == 3)) ? true : false} label={data.name} />
                                       }.bind(this))}
                                    </RadioGroup>
                                 </FormControl>
                              </MuiThemeProvider>
                           </div>
                           {(this.props.planDetails && (this.state.data.action_type == 1 || this.state.data.action_type == 2)) &&
                              <React.Fragment>
                                 <div className="col-md-12">
                                    <div className="border border-secondary">
                                       <table className="table table-striped table-hover">
                                          <tbody>
                                             <tr>
                                                <td>Principal Loan Amount : ${this.props.planDetails.loan_amount}</td>
                                                <td></td>
                                                <td>Term : {this.props.planDetails.term}</td>
                                                <td></td>
                                             </tr>
                                             <tr>
                                                <td>Interest Rate : {this.props.planDetails.discounted_interest_rate}%</td>
                                                <td></td>
                                                <td>Total Loan Amount with Interest: ${this.props.planDetails.amount}</td>
                                                <td></td>
                                             </tr>

                                             <tr>
                                                <td>Interest Amount Received : ${this.state.RecIntAmount}</td>
                                                <td></td>
                                                <td>Finance Charge Amount Received: ${this.state.RecFinAmount}</td>
                                                <td></td>
                                             </tr>
                                             <tr>
                                                <td>Total Principal Amount Received : ${this.state.RecPrAmount}</td>
                                                <td></td>
                                                <td>Principal Amount Outstanding : ${this.props.planDetails.remaining_amount}</td>
                                                <td></td>
                                             </tr>

                                             <tr>
                                                <td>Total Late Fee Received : ${this.state.RecLatAmount}</td>
                                                <td></td>
                                                <td>Total Amount Received (Principal + Late Fee + Fin Charge) : ${this.state.RecAmount}</td>
                                                <td></td>
                                             </tr>



                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div className="col-md-12">
                                    <table className="table table-striped table-hover">
                                       <tbody>
                                          <tr>
                                             <td>Loan default interest rate : {parseFloat(this.props.closeInt).toFixed(2)}%</td>
                                             <td></td>
                                             <td>Interest amount with loan default interest rate : ${this.state.RevisedIntAmt}</td>
                                             <td></td>
                                          </tr>
                                          <tr>
                                             <td>Interest amount outstanding : ${(this.state.RevisedIntAmt - this.state.RecIntAmount).toFixed(2)}</td>
                                             <td></td>
                                             <td>Minimum settlement Amount : ${this.props.planDetails.remaining_amount}</td>
                                             <td></td>
                                          </tr>
                                          <tr>
                                             <td>Total amount due on settlement : ${(this.props.planDetails.remaining_amount + (this.state.RevisedIntAmt - this.state.RecIntAmount)).toFixed(2)}</td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div className={(this.state.data.action_type == 1) ? "col-md-3" : "col-md-3 d-none"}>
                                    <FormGroup>
                                       <Label for="application_no">Acutal Adjusted Amount</Label><br />
                                       <TextField
                                          type="text"
                                          name="adjusted_amount"
                                          id="adjusted_amount"
                                          fullWidth
                                          variant="outlined"
                                          inputProps={{ maxLength: 11 }}
                                          placeholder="Acutal Adjusted Amount"
                                          value={this.state.data.adjusted_amount}
                                          error={(this.state.dataError.adjusted_amount) ? true : false}
                                          helperText={this.state.dataError.adjusted_amount}
                                          onChange={(e) => this.onChnagePlanAction('adjusted_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                              </React.Fragment>

                           }

                           {this.state.data.action_type == 2 &&
                              <React.Fragment>
                                 <div className="col-md-4">
                                    <MuiThemeProvider theme={this.getMuiTheme()}>
                                       <FormControl component="fieldset" required>
                                          <FormLabel component="legend">Loan Type</FormLabel>
                                          <RadioGroup row aria-label="loan_type" name="loan_type" value={this.state.data.loan_type} onChange={(e) => this.onChnagePlanAction('loan_type', e.target.value)} >
                                             <FormControlLabel value="1" control={<Radio />} label="Fixed term (months)" />
                                             <FormControlLabel value="0" control={<Radio />} label="Fixed amount(monthly)" />
                                          </RadioGroup>
                                       </FormControl>
                                    </MuiThemeProvider>
                                 </div>
                                 <div className={(this.state.data.loan_type == 1) ? "col-md-3 d-none" : "col-md-3"}>
                                    <FormGroup>
                                       <Label for="procedure_amount">Monthly Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="monthly_amount"
                                          id="monthly_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="($)"
                                          value={this.state.data.monthly_amount}
                                          error={(this.state.dataError.monthly_amount) ? true : false}
                                          helperText={(this.state.dataError.monthly_amount != '') ? this.state.dataError.monthly_amount : ''}
                                          onChange={(e) => this.onChnagePlanAction('monthly_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 {/*<div className="col-md-3">
                                    <FormGroup>
                                       <Label for="procedure_amount">Procedure Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="procedure_amount"
                                          id="procedure_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="($)"
                                          value={this.state.data.procedure_amount}
                                          error={(this.state.dataError.procedure_amount) ? true : false}
                                          helperText={(this.state.dataError.procedure_amount != '') ? this.state.dataError.procedure_amount : ''}
                                          onChange={(e) => this.onChnagePlanAction('procedure_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-2">
                                    <FormGroup>
                                       <Label for="procedure_date">Procedure Date<span className="required-field">*</span></Label>
                                       <DatePicker
                                          dateFormat="MM/dd/yyyy"
                                          name="procedure_date"
                                          id="procedure_date"
                                          minDate={new Date()}
                                          maxDate={oneYearFromNow}
                                          selected={this.state.procedure_date}
                                          placeholderText="MM/DD/YYYY"
                                          autocomplete={false}
                                          onChange={(e) => this.onChnagePlanAction('procedure_date', e)}
                                       />
                                       {(this.state.dataError.procedure_date) ? <FormHelperText>{this.state.dataError.procedure_date}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                                 */}<div className="col-md-3">
                                    <FormGroup>
                                       <Label for="loan_amount">Enter Loan Amount($)<span className="required-field">*</span></Label><br />
                                       <TextField
                                          type="text"
                                          name="loan_amount"
                                          id="loan_amount"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Enter Loan Amount"
                                          value={this.state.data.loan_amount}
                                          error={(this.state.dataError.loan_amount) ? true : false}
                                          helperText={this.state.dataError.loan_amount}
                                          onChange={(e) => this.onChnagePlanAction('loan_amount', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                              </React.Fragment>
                           }

                           {(this.state.data.action_type == 3 && this.props.paymentMethod !== undefined) &&
                              <React.Fragment>
                                 <div className="col-md-12">
                                    <table className="table table-striped table-hover">
                                       <tbody>
                                          <tr>
                                             <td>Customer Amount Received : ${parseFloat(this.state.data.customer_refund_act).toFixed(2)}</td>
                                             <td></td>
                                             <td>Provider Amount Paid : {(this.props.providerInvoiceDetails != undefined && this.props.providerInvoiceDetails.invoice_status == 4) ? '$' + parseFloat(this.props.providerInvoiceDetails.check_amount).toFixed(2) : '$0.00'}</td>
                                             <td></td>
                                             <td>Plan Status : {(this.props.customerDetails) ? this.props.customerDetails.plan_status_name : '-'}</td>
                                             <td></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div className="col-md-6">
                                    <FormGroup>
                                       <Label for="customer_refund">Customer Refund Amount ($)</Label><br />
                                       <TextField
                                          type="text"
                                          name="customer_refund"
                                          id="customer_refund"
                                          variant="outlined"
                                          inputProps={{ maxLength: 11 }}
                                          placeholder="Acutal Adjusted Amount"
                                          value={this.state.data.customer_refund}
                                          error={(this.state.dataError.customer_refund) ? true : false}
                                          helperText={this.state.dataError.customer_refund}
                                          onChange={(e) => this.onChnagePlanAction('customer_refund', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-6">
                                    <FormGroup>
                                       <Label for="provider_refund">Refund Due From Provider ($)</Label><br />
                                       <TextField
                                          type="text"
                                          name="provider_refund"
                                          id="provider_refund"
                                          variant="outlined"
                                          inputProps={{ maxLength: 11 }}
                                          placeholder="Acutal Adjusted Amount"
                                          value={this.state.data.provider_refund}
                                          error={(this.state.dataError.provider_refund) ? true : false}
                                          helperText={this.state.dataError.provider_refund}
                                          onChange={(e) => this.onChnagePlanAction('provider_refund', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className={(this.state.data.customer_refund > 0) ? "col-md-6" : "col-md-6 d-none"}>
                                    <MuiThemeProvider theme={this.getMuiTheme()}>
                                       <FormControl component="fieldset" required>
                                          <FormLabel component="legend">Payment Method</FormLabel>
                                          <RadioGroup row aria-label="loan_type" name="loan_type" value={this.state.data.customer_pay_method} onChange={(e) => this.onChnagePlanAction('customer_pay_method', e.target.value)} >
                                             {this.props.paymentMethod.map(function (data, idx) {
                                                if (data.option_id != 2) {
                                                   return <FormControlLabel key={idx} value={data.option_id.toString()} control={<Radio />} label={data.value} />
                                                }
                                             }.bind(this))}
                                          </RadioGroup>
                                       </FormControl>
                                    </MuiThemeProvider>
                                 </div>
                                 <div className={(this.state.data.customer_refund <= 0) ? "col-md-6" : "col-md-6 d-none"}></div>
                                 <div className="col-md-6">
                                    {this.props.providerRefundDetails &&
                                       <FormGroup>
                                          <Label for="ach_bank_name">Provider Refund Type</Label><br />
                                          {(this.props.providerRefundDetails.iou_flag == 1) ? 'IOU' : 'Issue Refund'}
                                       </FormGroup>
                                    }
                                 </div>
                                 <div className={(this.state.data.customer_pay_method == 1 && this.state.data.customer_refund > 0) ? "w-50" : "d-none w-50"}>
                                    <div className="col-md-12">
                                       <FormGroup>
                                          <Label for="ach_bank_name">Bank Name<span className="required-field">*</span></Label><br />
                                          <TextField
                                             type="text"
                                             name="ach_bank_name"
                                             id="ach_bank_name"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Bank Name"
                                             value={this.state.data.ach_bank_name}
                                             error={(this.state.dataError.ach_bank_name) ? true : false}
                                             helperText={(this.state.dataError.ach_bank_name != '') ? this.state.dataError.ach_bank_name : ''}
                                             onChange={(e) => this.onChnagePlanAction('ach_bank_name', e.target.value)}
                                          />
                                       </FormGroup>
                                    </div>

                                    <div className="col-md-12">
                                       <FormGroup>
                                          <Label for="ach_routing_no">Routing No.<span className="required-field">*</span></Label><br />
                                          <TextField
                                             type="text"
                                             name="ach_routing_no"
                                             id="ach_routing_no"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Routing No"
                                             value={this.state.data.ach_routing_no}
                                             error={(this.state.dataError.ach_routing_no) ? true : false}
                                             helperText={(this.state.dataError.ach_routing_no != '') ? this.state.dataError.ach_routing_no : ''}
                                             onChange={(e) => this.onChnagePlanAction('ach_routing_no', e.target.value)}
                                          />
                                       </FormGroup>
                                    </div>

                                    <div className="col-md-12">
                                       <FormGroup>
                                          <Label for="ach_account_no">Account No.<span className="required-field">*</span></Label><br />
                                          <TextField
                                             type="text"
                                             name="ach_account_no"
                                             id="ach_account_no"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Account No"
                                             value={this.state.data.ach_account_no}
                                             error={(this.state.dataError.ach_account_no) ? true : false}
                                             helperText={(this.state.dataError.ach_account_no != '') ? this.state.dataError.ach_account_no : ''}
                                             onChange={(e) => this.onChnagePlanAction('ach_account_no', e.target.value)}
                                          />
                                       </FormGroup>
                                    </div>

                                 </div>
                                 <div className={(this.state.data.customer_pay_method == 3 && this.state.data.customer_refund > 0) ? "w-50" : "d-none w-100"}>
                                    <div className="col-md-12">
                                       <FormGroup>
                                          <Label for="check_no">Check Number<span className="required-field">*</span></Label><br />
                                          <TextField
                                             type="text"
                                             name="check_no"
                                             id="check_no"
                                             fullWidth
                                             variant="outlined"
                                             placeholder="Check No"
                                             value={this.state.data.check_no}
                                             error={(this.state.dataError.check_no) ? true : false}
                                             helperText={(this.state.dataError.check_no != '') ? this.state.dataError.check_no : ''}
                                             onChange={(e) => this.onChnagePlanAction('check_no', e.target.value)}
                                          />
                                       </FormGroup>
                                    </div>
                                    <div className="col-md-12">
                                       <FormGroup>
                                          <Label for="check_date">Check Date<span className="required-field">*</span></Label><br />
                                          <DatePicker
                                             dateFormat="MM/dd/yyyy"
                                             name="check_date"
                                             id="check_date"
                                             selected={this.state.check_date}
                                             placeholderText="MM/DD/YYYY"
                                             autocomplete={false}
                                             showMonthDropdown
                                             showYearDropdown
                                             dropdownMode="select"
                                             minDate={new Date()}
                                             strictParsing
                                             onChange={(e) => this.onChnagePlanAction('check_date', e)}
                                          />
                                          {(this.state.dataError.check_date) ? <FormHelperText className="jss116">{this.state.dataError.check_date}</FormHelperText> : ''}

                                       </FormGroup>
                                    </div>

                                 </div>
                                 <div className={(this.state.data.customer_pay_method == 1 && this.state.data.customer_refund <= 0) ? "w-50" : "d-none w-50"}></div>
                                 <div className="w-50">
                                    <div className="col-md-12">
                                       {this.props.providerRefundDetails &&
                                          <FormGroup>
                                             <Label for="ach_bank_name">Invoice Number</Label><br />
                                             {this.props.providerRefundDetails.invoice_number}
                                          </FormGroup>
                                       }
                                    </div>
                                 </div>
                              </React.Fragment>
                           }
                           <div className={(this.state.data.action_type != '') ? 'col-md-12' : 'col-md-12 d-none'}>
                              <FormGroup>
                                 <Label for="note">Note<span className="required-field">*</span></Label><br />
                                 <Input
                                    type="textarea"
                                    name="note"
                                    id="note"
                                    variant="outlined"
                                    value={this.state.data.note}
                                    placeholder="Note"
                                    onChange={(e) => this.onChnagePlanAction('note', e.target.value)}
                                 >
                                 </Input>
                                 {(this.state.dataError.note) ? <FormHelperText>{this.state.dataError.note}</FormHelperText> : ''}
                              </FormGroup>
                           </div>


                           <div className="col-sm-12 text-right">
                              <div className="mt-15 mb-10">
                                 <Button onClick={this.goBack.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Back
                                 </Button>
                                 <Button disabled={!this.validateConfirmation()} onClick={() => this.processPlanConfirmation()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Initiate Cancellation
                                 </Button>
                                 <Button disabled={!this.validateAction()} onClick={() => this.processPlanConfirm()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Submit
                                 </Button>
                                 <Button disabled={!this.validateAgreement()} onClick={() => this.processAgreement()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    View Agreement
                                 </Button>

                              </div>
                           </div>
                        </div>
                     </div>
                  }
                  {this.state.agreementView &&
                     <div className="modal-body page-form-outer text-left">
                        <div className="p-10">
                           <h1 className="text-center mb-20">
                              <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                           </h1>
                           <h1 className="text-center mb-20">Terms of Agreement for Health Partner, Inc</h1>
                           <div className="terms-condition-agreement">
                              <div>Date <strong><u>{moment(Date.now()).format('MM/DD/YYYY')}</u></strong></div>
                              <div>Location <u>{this.props.providerDetails.address1 + ' ' + this.props.providerDetails.address2 + ','}</u></div>
                              <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>{this.props.providerDetails.city + ', ' + this.props.providerDetails.state_name + '-' + this.props.providerDetails.zip_code}</u></div>
                              <p>On or before <strong>Date</strong>, for value received, the undersigned <strong><u>{(this.props.customerDetails) ? (this.props.customerDetails.first_name + ' ' + this.props.customerDetails.middle_name + ' ' + this.props.customerDetails.last_name) : '-'}</u></strong> (the "Borrower") promises to pay to the order of HEALTH PARTNER INC. (the "Holder"), in the manner and at the place provided below, the principal sum of <strong><u>${parseFloat(this.state.data.loan_amount).toFixed(2)}</u></strong>.</p>

                              <h4><strong>1. PAYMENT :</strong></h4>
                              <p>All payments of principal and interest under this note will be made in lawful money of the United States, without offset, deduction, or counterclaim, by wire transfer of immediately available funds to an account designated by the Holder in writing at least 7 days after the effective date of this note or, if this designation is not made, by check mailed to the Holder at 5720 Creedmoor Road, Suite 103, Raleigh, North Carolina, 27612, or at such other place as the Holder may designate in writing.</p>

                              <h4><strong>2. INTEREST :</strong></h4>
                              <p>Interest on the unpaid principal balance of this note is payable from the date of this note until this note is paid in full, at the rate of <strong><u>{(parseFloat(this.props.closeInt)).toFixed(2) + "%"}</u></strong> per year, or the maximum amount allowed by applicable law, whichever is less. Accrued interest will be computed on the basis of a 365-day or 366-day year, as the case may be, based on the actual number of days elapsed in the period in which it accrues.</p>

                              <h4><strong>3. PREPAYMENT :</strong></h4>
                              <p>The Borrower may prepay this note, in whole or in part, at any time before maturity without penalty or premium. Any partial prepayment will be credited first to accrued interest, then to principal. No prepayment extends or postpones the maturity date of this note.</p>

                              <h4><strong>4. EVENTS OF DEFAULT :</strong></h4>
                              <p>Each of the following constitutes an <strong>"Event of Default"</strong> under this note:</p>
                              <ul>
                                 <li>The Borrower's failure to make any payment when due under the terms of this note, including the lump-sum payment due under this note at its maturity;</li>
                                 <li>The filing of any voluntary or involuntary petition in bankruptcy by or regarding the Borrower or the initiation of any proceeding under bankruptcy or insolvency laws against the Borrower;</li>
                                 <li>An assignment made by the Borrower for the benefit of creditors;</li>
                                 <li>The appointment of a receiver, custodian, trustee, or similar party to take possession of the Borrower's assets or property; or</li>
                                 <li>The death of the Borrower.</li>
                              </ul>

                              <h4><strong>5. ACCELERATION; REMEDIES ON DEFAULT :</strong></h4>
                              <p>If any Event of Default occurs, all principal and other amounts owed under this note will become immediately due and payable without any action by the Holder, the Borrower, or any other person. The Holder, in addition to any rights and remedies available to the Holder under this note, may, in its sole discretion, pursue any legal or equitable remedies available to it under applicable law or in equity, including taking any of the following actions:</p>
                              <ul>
                                 <li>Personally, or by agents or attorneys (in compliance with applicable law), take immediate possession of the collateral. To that end, the Holder may pursue the collateral where it may be found, and enter the Borrower's premises, with or without notice, demand, process of law, or legal procedure if this can be done without breach of the peace. If the premises on which any part of the collateral is located are not under the Borrower's direct control, the Borrower will exercise its best efforts to ensure that the Holder is promptly provided right of access to those premises. To the extent that the Borrower's consent would otherwise be required before a right of access could be granted, the Borrower hereby irrevocably grants that consent;</li>
                                 <li>Require the Borrower to assemble the collateral and make it available to the Holder at a place to be designated by the Holder that is reasonably convenient to both parties (it being acknowledged that the Borrower's premises are reasonably convenient to the Borrower);</li>
                                 <li>Sell, lease, or dispose of the collateral or any part of it in any manner permitted by applicable law or by contract; and</li>
                                 <li>Exercise all rights and remedies of a secured party under applicable law.
</li>
                              </ul>

                              <h4><strong>6. WAIVER OF PRESENTMENT; DEMAND :</strong></h4>
                              <p>The Borrower hereby waives presentment, demand, notice of dishonor, notice of default or delinquency, notice of protest and nonpayment, notice of costs, expenses or losses and interest on those, notice of interest on interest and late charges, and diligence in taking any action to collect any sums owing under this note, including (to the extent permitted by law) waiving the pleading of any statute of limitations as a defense to any demand against the undersigned. Acceptance by the Holder or any other holder of this note of any payment differing from the designated lump-sum payment listed above does not relieve the undersigned of the obligation to honor the requirements of this note.</p>

                              <h4><strong>7. Frozen Credit and Penalties :</strong></h4>
                              <p>The Borrower agrees that in event of missed payment, any remaining line of credit will be frozen and will not be accessible until remaining balances have been paid. Missed payments will occur late fees and increased interest charges.</p>

                              <h4><strong>8. GOVERNING LAW :</strong></h4>
                              <ul>
                                 <li><strong>Choice of Law.</strong> The laws of the state of North Carolina govern this note (without giving effect to its conflicts of law principles).</li>
                                 <li><strong>Choice of Forum.</strong> Both parties consent to the personal jurisdiction of the state and federal courts in Wake, North Carolina.</li>
                              </ul>

                              <h4><strong>9. COLLECTION COSTS AND ATTORNEYS' FEES :</strong></h4>
                              <p>The Borrower shall pay all expenses of the collection of indebtedness evidenced by this note, including reasonable attorneys' fees and court costs in addition to other amounts due.</p>

                              <h4><strong>10. ASSIGNMENT AND DELEGATION :</strong></h4>
                              <ul>
                                 <li><strong>No Assignment.</strong> The Borrower may not assign any of its rights under this note. All voluntary assignments of rights are limited by this subsection.</li>
                                 <li><strong>No Delegation.</strong> The Borrower may not delegate any performance under this note.</li>
                                 <li><strong>Enforceability of an Assignment or Delegation.</strong> If a purported assignment or purported delegation is made in violation of this section, it is void.</li>
                              </ul>

                              <h4><strong>11. SEVERABILITY :</strong></h4>
                              <p>If any one or more of the provisions contained in this note is, for any reason, held to be invalid, illegal, or unenforceable in any respect, that invalidity, illegality, or unenforceability will not affect any other provisions of this note, but this note will be construed as if those invalid, illegal, or unenforceable provisions had never been contained in it, unless the deletion of those provisions would result in such a material change so as to cause completion of the transactions contemplated by this note to be unreasonable.</p>

                              <h4><strong>12. NOTICES :</strong></h4>
                              <ul>
                                 <li><strong>Writing; Permitted Delivery Methods.</strong> Each party giving or making any notice, request, demand, or other communication required or permitted by this note shall give that notice in writing and use one of the following types of delivery, each of which is a writing for purposes of this note: personal delivery, mail (registered or certified mail, postage prepaid, return-receipt requested), nationally recognized overnight courier (fees prepaid), facsimile, or email.</li>
                                 <li><strong>Addresses.</strong> A party shall address notices under this section to a party at the following addresses:<br /><br />
                                    <p><strong>If to the Borrower :</strong></p>
                                    <p><u>{this.props.customerDetails.address1 + ' ' + this.props.customerDetails.address2},</u></p>
                                    <p><u>{this.props.customerDetails.City + ', ' + this.props.customerDetails.state_name + '-' + this.props.customerDetails.zip_code}</u></p>
                                    <p><u><strong>Phone:</strong> {this.props.customerDetails.peimary_phone}</u></p>

                                    <strong>If to the Holder:</strong>
                                    <p>Health Partner Inc.</p>
                                    <p>5720 Creedmoor Road, Suite 103</p>
                                    <p>Raleigh, North Carolina 27612</p>

                                 </li>
                                 <li><strong>Effectiveness.</strong> A notice is effective only if the party giving notice complies with subsections (a)</li>
                                 <li>and if the recipient receives the notice</li>
                              </ul>

                              <h4><strong>13. WAIVER :</strong></h4>
                              <p>No waiver of a breach, failure of any condition, or any right or remedy contained in or granted by the provisions of this note will be effective unless it is in writing and signed by the party waiving the breach, failure, right, or remedy. No waiver of any breach, failure, right, or remedy will be deemed a waiver of any other breach, failure, right, or remedy, whether or not similar, and no waiver will constitute a continuing waiver, unless the writing so specifies.</p>

                              <h4><strong>14. HEADINGS :</strong></h4>
                              <p>The descriptive headings of the sections and subsections of this note are for convenience only, and do not affect this note's construction or interpretation.</p>


                              <br /><br />
                           </div>
                           <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Customer Information</th>
                                       </tr>
                                       <tr>
                                          <td><strong>Account No:</strong> {this.props.customerDetails.patient_ac}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Application No:</strong> {this.props.customerDetails.application_no}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Name:</strong> {this.props.customerDetails.first_name + ' ' + this.props.customerDetails.middle_name + ' ' + this.props.customerDetails.last_name}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Address:</strong> {this.props.customerDetails.address1 + ' ' + this.props.customerDetails.address2 + ' ' + this.props.customerDetails.City + ' ' + this.props.customerDetails.state_name}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Phone:</strong> {this.props.customerDetails.peimary_phone}</td>
                                          <td></td>
                                       </tr>



                                    </tbody>
                                 </table>

                              </div>
                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Provider Information</th>
                                       </tr>
                                       <tr>
                                          <td><strong>Account No:</strong> {this.props.providerDetails.provider_ac}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Name:</strong> {this.props.providerDetails.name}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Address:</strong> {this.props.providerDetails.address1 + ' ' + this.props.providerDetails.address2 + ' ' + this.props.providerDetails.city + ' ' + this.props.providerDetails.state_name}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Phone:</strong> {this.props.providerDetails.primary_phone}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Doctor:</strong> - </td>
                                          <td></td>
                                       </tr>


                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Loan Information</th>
                                       </tr>

                                       <tr>
                                          <td><strong>Approved Amount:</strong> ${parseFloat(this.props.customerDetails.approve_amount).toFixed(2)}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Principal Amount:</strong> ${parseFloat(this.state.data.loan_amount).toFixed(2)}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Remaining Amount:</strong> ${parseFloat(this.props.customerDetails.remaining_amount).toFixed(2)}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Loan Amount:</strong> ${parseFloat(this.state.planEst.totalAmount).toFixed(2)}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>APR:</strong> {(this.state.planEst.interest_rate) ? parseFloat(this.state.planEst.interest_rate).toFixed(2) : '0.00'}%</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Term Month:</strong> {this.state.planEst.term_month}</td>
                                          <td></td>
                                       </tr>
                                       <tr>
                                          <td><strong>Procedure Date:</strong> {moment(this.state.procedure_date).format('MM/DD/YYYY')}</td>
                                          <td></td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>


                           </div>
                           <div className="table-responsive mb-40 pymt-history">
                              <h2 className="text-center mb-10">Plan Details</h2>
                              <div className="table-responsive">
                                 <table className="table table-borderless">
                                    <thead>
                                       <tr>
                                          <th>SN#</th>
                                          <th>Date</th>
                                          <th>Amount</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       {this.state.planEst.monthlyPlan.map((plan, idx) => (
                                          <tr key={idx}>
                                             <td>{idx + 1}</td>
                                             <td>{plan.next_month}</td>
                                             <td>${parseFloat(plan.perMonth).toFixed(2)}</td>
                                          </tr>
                                       ))}
                                    </tbody>
                                 </table>

                              </div>
                           </div>
                           <div className="mb-40 pymt-history">

                              <div className="row user-permission mt-20">
                                 <div className="col-md-12">
                                    <p className="text-danger">Note:- This payment plan does not reflect possible charges occurring from late fees, transaction fees, or financial charges.  Interest will be charged to your account at a variable APR of 26.99% from purchase date if the purchase amount is not paid in full within the loan term or if you make a late payment. To avoid late fees, you must make your Monthly Payments by the due date each month.</p>
                                 </div>
                                 <div className="col-md-12">
                                    <p>
                                       <label className="check-container">
                                          <Input name="current_plan" type="checkbox" value="1" checked={(this.state.data.agree_with == 1) ? true : false} onChange={(e) => this.onChnagePlanAction('agree_with', e.target.value)} />
                                          <span className="checkmark"></span><span className="pl-30 agree-text">By signing below, you, the Borrower, agree to all terms listed in the above Agreement.</span>
                                       </label>
                                    </p>
                                 </div>



                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="name_of_borrower">BY (Name of Borrower)<span className="required-field">*</span></Label>
                                       <TextField
                                          type="text"
                                          name="name_of_borrower"
                                          id="name_of_borrower"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="Name of Borrower"
                                          value={this.state.data.name_of_borrower || ''}
                                          error={(this.state.dataError.name_of_borrower) ? true : false}
                                          helperText={(this.state.dataError.name_of_borrower != '') ? this.state.dataError.name_of_borrower : ''}
                                          onChange={(e) => this.onChnagePlanAction('name_of_borrower', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="agreement_date">Date<span className="required-field">*</span></Label>
                                       <DatePicker
                                          dateFormat="MM/dd/yyyy h:mm aa"
                                          timeIntervals={15}
                                          timeFormat="HH:mm"
                                          showTimeSelect
                                          name="agreement_date"
                                          id="agreement_date"
                                          minDate={new Date()}
                                          maxDate={new Date()}
                                          selected={this.state.agreement_date}
                                          placeholderText="MM/DD/YYYY"
                                          autocomplete={false}
                                          onChange={(e) => this.onChnagePlanAction('agreement_date', e)}
                                       />
                                       {(this.state.dataError.agreement_date) ? <FormHelperText>{this.state.dataError.agreement_date}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>



                              </div>
                              <h2 className="text-center mb-10">ID Proof documents Details</h2>
                              <div className="row">
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="id_name">ID Type<span className="required-field">*</span></Label>
                                       <TextField
                                          type="text"
                                          name="id_name"
                                          id="id_name"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="ID Type"
                                          value={this.state.data.id_name || ''}
                                          error={(this.state.dataError.id_name) ? true : false}
                                          helperText={(this.state.dataError.id_name != '') ? this.state.dataError.id_name : ''}
                                          onChange={(e) => this.onChnagePlanAction('id_name', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-4">
                                    <FormGroup>
                                       <Label for="id_number">ID Number<span className="required-field">*</span></Label>
                                       <TextField
                                          type="text"
                                          name="id_number"
                                          id="id_number"
                                          fullWidth
                                          variant="outlined"
                                          placeholder="ID Number"
                                          value={this.state.data.id_number || ''}
                                          error={(this.state.dataError.id_number) ? true : false}
                                          helperText={(this.state.dataError.id_number != '') ? this.state.dataError.id_number : ''}
                                          onChange={(e) => this.onChnagePlanAction('id_number', e.target.value)}
                                       />
                                    </FormGroup>
                                 </div>
                                 <div className="col-md-3">
                                    <FormGroup>
                                       <Label for="expiry_date">Expiry Date</Label>
                                       <DatePicker
                                          dateFormat="MM/dd/yyyy"
                                          name="expiry_date"
                                          id="expiry_date"
                                          minDate={new Date()}
                                          maxDate={twentyYearFromNow}
                                          selected={this.state.expiry_date}
                                          placeholderText="MM/DD/YYYY"
                                          autocomplete={false}
                                          onChange={(e) => this.onChnagePlanAction('expiry_date', e)}
                                       />
                                       {(this.state.dataError.expiry_date) ? <FormHelperText>{this.state.dataError.expiry_date}</FormHelperText> : ''}
                                    </FormGroup>
                                 </div>
                              </div>
                              <div className="user-permission mt-20">
                                 <div className="row">

                                    <div className="col-md-12">

                                    </div>

                                 </div>
                              </div>
                           </div>
                           <div className="p-10">
                              <div className="row justify-content-between mb-30 add-full-card customer-accnt">

                                 <div className="add-card-sign col-md-4 text-center sing_box">
                                    <b>Customer Signature</b>
                                    <div className="">
                                       <SignatureCanvas penColor='green'
                                          canvasProps={{ width: 285, height: 95, className: (this.state.customerSignAllow) ? 'sigCanvas border border-primary' : 'sigCanvas border border-primary bg-secondary' }} ref={(ref) => { this.customerSigPad = ref }} />
                                    </div>
                                    <div>
                                       <div className="mt-10">
                                          <Button onClick={this.clearCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                             Reset
                                                         </Button>
                                          <Button onClick={this.trimCustomer} variant="contained" color="primary" className="text-white mr-10" size="small">
                                             Done
                                                         </Button>
                                       </div>
                                    </div>
                                    <div className="mt-10">
                                       {this.state.customerSignURL ? <img src={this.state.customerSignURL} /> : null}
                                    </div>

                                 </div>
                                 <div className="col-md-4"></div>
                                 <div className="add-card-sign col-md-4 text-center sing_box">

                                    <b>Witness Signature</b>
                                    <div className="">
                                       <SignatureCanvas penColor='green'
                                          canvasProps={{ width: 285, height: 95, className: (this.state.witnessSignAllow) ? 'sigCanvas border border-primary' : 'sigCanvas border border-primary bg-secondary' }} ref={(ref) => { this.witnessSigPad = ref }} />
                                    </div>
                                    <div>
                                       <div className="mt-10">
                                          <Button onClick={this.clearWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                             Reset
                                                         </Button>
                                          <Button onClick={this.trimWitness} variant="contained" color="primary" className="text-white mr-10" size="small">
                                             Done
                                                         </Button>
                                       </div>
                                    </div>
                                    <div className="mt-10">
                                       {this.state.witnessSignURL ? <img src={this.state.witnessSignURL} /> : null}
                                    </div>
                                 </div>

                              </div>
                           </div>
                           <div className="col-sm-12 text-right">
                              <div className="mt-15 mb-10">
                                 <Button onClick={this.closeBack.bind(this)} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Back
                                 </Button>
                                 <Button disabled={!this.validateAction()} onClick={() => this.processPlanConfirm()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                                    Submit
                                 </Button>

                              </div>
                           </div>
                        </div>
                     </div>
                  }
                  {this.props.loading &&
                     <RctSectionLoader />
                  }
               </RctCard>
            }

            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title={(this.state.data.action_type == 0) ? "Are you sure you want to cancel this plan?" : "Are You Sure?"}
               message="Transaction cannot be rolled back."
               onConfirm={() => this.processPlanAction()}
            />
            <DeleteTypeConfirmationDialog
               ref="deleteTypeConfirmationDialog"
               title="Confirm close and refund?"
               message="Type 'close and refund' in the text box."
               typeKey="close and refund"
               onConfirm={() => this.processPlanAction()}
            />
         </div >
      );
   }
}

const mapStateToProps = ({ authUser, PaymentPlanReducer }) => {

   const { loading, planCloseOption, planCloseStatus, planInvoiceExist, planInvoiceDetails, planDetails, closeInt, providerDetails, customerDetails, closeTerm, customerPaid, providerInvoiceDetails, paymentMethod, providerRefundDetails } = PaymentPlanReducer;
   return { loading, planCloseOption, planCloseStatus, planInvoiceExist, planInvoiceDetails, planDetails, closeInt, providerDetails, customerDetails, closeTerm, customerPaid, providerInvoiceDetails, paymentMethod, providerRefundDetails }
}

export default connect(mapStateToProps, {
   viewOptionToClose, closeCurrentPlan, getClosePlanDetails
})(OptionToClose);