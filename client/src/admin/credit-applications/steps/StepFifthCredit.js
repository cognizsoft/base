/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
const StepFifthCredit = ({ addErr, addData, onChnagerovider, checkUsernameExist, checkUsernameExistCo }) => (

   <div className="modal-body page-form-outer text-left">
      <Form>
         <div className="row">
            <div className="col-md-4">
               <FormGroup>
                  <Label for="username">Username<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="username"
                     id="username"
                     fullWidth
                     variant="outlined"
                     placeholder="Username"
                     value={(addData.username != '') ? addData.username : ''}
                     error={(addErr.username) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={(addErr.username != '') ? addErr.username : ''}
                     onChange={(e) => onChnagerovider('username', e.target.value)}
                     onKeyUp={(e) => checkUsernameExist(e.target.value)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="password">Password<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="password"
                     id="password"
                     fullWidth
                     variant="outlined"
                     placeholder="Password"
                     value={(addData.password != '') ? addData.password : ''}
                     error={(addErr.password) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={(addErr.password != '') ? addErr.password : ''}
                     onChange={(e) => onChnagerovider('password', e.target.value)}
                  />
               </FormGroup>
            </div>
            <div className="col-md-4">
               <FormGroup>
                  <Label for="password">Confirm Password<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="confirm_password"
                     id="confirm_password"
                     fullWidth
                     variant="outlined"
                     placeholder="Enter Confirm Password"
                     value={(addData.confirm_password != '') ? addData.confirm_password : ''}
                     error={(addErr.confirm_password) ? true : false}
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     helperText={addErr.confirm_password}
                     onChange={(e) => onChnagerovider('confirm_password', e.target.value)}
                  />
               </FormGroup>
            </div>
         </div>

         <div className="row">
            <div className="col-md-4">
               <FormGroup>
                  <Label for="email">Primary Email Address<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="email"
                     id="email"
                     fullWidth
                     variant="outlined"
                     placeholder="Email Address"
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     value={(addData.email != '') ? addData.email : ''}
                     error={(addErr.email) ? true : false}
                     helperText={(addErr.email != '') ? addErr.email : ''}
                     onChange={(e) => onChnagerovider('email', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="secondary_email">Secondary Email Address</Label><br />
                  <TextField
                     type="text"
                     name="secondary_email"
                     id="secondary_email"
                     fullWidth
                     variant="outlined"
                     placeholder="Secondary Email Address"
                     disabled={(addData.patient_id != '' && addData.patient_id !== undefined) ? true : false}
                     value={(addData.secondary_email != '') ? addData.secondary_email : ''}
                     error={(addErr.secondary_email) ? true : false}
                     helperText={(addErr.secondary_email != '') ? addErr.secondary_email : ''}
                     onChange={(e) => onChnagerovider('secondary_email', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="phone_1">Primary Phone No.<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="phone_1"
                     id="phone_1"
                     fullWidth
                     variant="outlined"
                     placeholder="Phone No."
                     inputProps={{ maxLength: 14 }}
                     value={(addData.phone_1 != '') ? addData.phone_1 : ''}
                     error={(addErr.phone_1) ? true : false}
                     helperText={(addErr.phone_1 != '') ? addErr.phone_1 : ''}
                     onChange={(e) => onChnagerovider('phone_1', e.target.value)}
                  />
               </FormGroup>
            </div>

            <div className="col-md-4">
               <FormGroup>
                  <Label for="phone_2">Secondary Phone No.</Label><br />
                  <TextField
                     type="text"
                     name="phone_2"
                     id="phone_2"
                     fullWidth
                     variant="outlined"
                     placeholder="Phone No."
                     inputProps={{ maxLength: 14 }}
                     value={(addData.phone_2 != '') ? addData.phone_2 : ''}
                     error={(addErr.phone_2) ? true : false}
                     helperText={(addErr.phone_2 != '') ? addErr.phone_2 : ''}
                     onChange={(e) => onChnagerovider('phone_2', e.target.value)}
                  />
               </FormGroup>
            </div>


         </div>

         <div className={(addData.co_signer != 1) ? 'd-none' : ''}>
            <hr className="border-dark" />
            <div className="row">
               <div className="col-md-12"><strong>Co-signer Details</strong></div>
            </div>
            <div className="row">
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_username">Username<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="co_username"
                        id="co_username"
                        fullWidth
                        variant="outlined"
                        placeholder="Username"
                        value={(addData.co_username != '') ? addData.co_username : ''}
                        error={(addErr.co_username) ? true : false}
                        disabled={(addData.co_patient_id != '' && addData.co_patient_id !== undefined) ? true : false}
                        helperText={(addErr.co_username != '') ? addErr.co_username : ''}
                        onChange={(e) => onChnagerovider('co_username', e.target.value)}
                        onKeyUp={(e) => checkUsernameExistCo(e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_password">Password<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="co_password"
                        id="co_password"
                        fullWidth
                        variant="outlined"
                        placeholder="Password"
                        value={(addData.co_password != '') ? addData.co_password : ''}
                        error={(addErr.co_password) ? true : false}
                        disabled={(addData.co_patient_id != '' && addData.co_patient_id !== undefined) ? true : false}
                        helperText={(addErr.co_password != '') ? addErr.co_password : ''}
                        onChange={(e) => onChnagerovider('co_password', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_password">Confirm Password<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="co_confirm_password"
                        id="co_confirm_password"
                        fullWidth
                        variant="outlined"
                        placeholder="Enter Confirm Password"
                        value={(addData.co_confirm_password != '') ? addData.co_confirm_password : ''}
                        error={(addErr.co_confirm_password) ? true : false}
                        disabled={(addData.co_patient_id != '' && addData.co_patient_id !== undefined) ? true : false}
                        helperText={addErr.co_confirm_password}
                        onChange={(e) => onChnagerovider('co_confirm_password', e.target.value)}
                     />
                  </FormGroup>
               </div>
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_email">Primary Email Address<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="co_email"
                        id="co_email"
                        fullWidth
                        variant="outlined"
                        placeholder="Email Address"
                        disabled={(addData.co_patient_id != '' && addData.co_patient_id !== undefined) ? true : false}
                        value={(addData.co_email != '') ? addData.co_email : ''}
                        error={(addErr.co_email) ? true : false}
                        helperText={(addErr.co_email != '') ? addErr.co_email : ''}
                        onChange={(e) => onChnagerovider('co_email', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_secondary_email">Secondary Email Address</Label><br />
                     <TextField
                        type="text"
                        name="co_secondary_email"
                        id="co_secondary_email"
                        fullWidth
                        variant="outlined"
                        placeholder="Secondary Email Address"
                        disabled={(addData.co_patient_id != '' && addData.co_patient_id !== undefined) ? true : false}
                        value={(addData.co_secondary_email != '') ? addData.co_secondary_email : ''}
                        error={(addErr.co_secondary_email) ? true : false}
                        helperText={(addErr.co_secondary_email != '') ? addErr.co_secondary_email : ''}
                        onChange={(e) => onChnagerovider('co_secondary_email', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_phone_1">Primary Phone No.<span className="required-field">*</span></Label><br />
                     <TextField
                        type="text"
                        name="co_phone_1"
                        id="co_phone_1"
                        fullWidth
                        variant="outlined"
                        placeholder="Phone No."
                        inputProps={{ maxLength: 14 }}
                        value={(addData.co_phone_1 != '') ? addData.co_phone_1 : ''}
                        error={(addErr.co_phone_1) ? true : false}
                        helperText={(addErr.co_phone_1 != '') ? addErr.co_phone_1 : ''}
                        onChange={(e) => onChnagerovider('co_phone_1', e.target.value)}
                     />
                  </FormGroup>
               </div>

               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_phone_2">Secondary Phone No.</Label><br />
                     <TextField
                        type="text"
                        name="co_phone_2"
                        id="co_phone_2"
                        fullWidth
                        variant="outlined"
                        placeholder="Phone No."
                        inputProps={{ maxLength: 14 }}
                        value={(addData.co_phone_2 != '') ? addData.co_phone_2 : ''}
                        error={(addErr.co_phone_2) ? true : false}
                        helperText={(addErr.co_phone_2 != '') ? addErr.co_phone_2 : ''}
                        onChange={(e) => onChnagerovider('co_phone_2', e.target.value)}
                     />
                  </FormGroup>
               </div>


            </div>

         </div>

         <div className="row">
            <div className="col-md-12">
               <b>Note :-</b> You understand that by clicking on the I AGREE button immediately following this notice, you are providing ‘written instructions’ to Health Partner, Inc. under the Fair Credit Reporting Act authorizing Health Partner, Inc. to obtain information from your personal credit report or other information from Experian. You authorize Health Partner, Inc. to obtain such information solely to determine if you are eligible to receive a payment plan from Health Partner, Inc.
            </div>
         </div>

      </Form>
   </div>

);

export default StepFifthCredit;