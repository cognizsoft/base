/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const StepFourthCredit = ({
   addErr,
   addData,
   onChnagerovider,
   bankType,
   DatePicker,
   withdrawal_date,
   handleAddShareholder2,
   handleRemoveShareholder2,
   handleAddShareholder2Co,
   handleRemoveShareholder2Co
}) => (

      <div className="modal-body page-form-outer text-left">
         <Form>
            <div className="row">
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="withdrawal_date">Withdrawal day of the month (e.g-5,6,7...)</Label><br />
                     <TextField
                        type="text"
                        name="withdrawal_date"
                        id="withdrawal_date"
                        fullWidth
                        variant="outlined"
                        placeholder="Withdrawal day of month"
                        value={(addData.withdrawal_date != '') ? addData.withdrawal_date : ''}
                        error={(addErr.withdrawal_date) ? true : false}
                        helperText={(addErr.withdrawal_date != '') ? addErr.withdrawal_date : ''}
                        onChange={(e) => onChnagerovider('withdrawal_date', e.target.value)}
                     />
                  </FormGroup>
               </div>
            </div>
            {addData.bank.map((shareholder, idx) => (
               <div className="row" key={idx}>
                  {(() => {
                     if (idx != 0) {
                        return (
                           <React.Fragment>
                              <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                              <div className="col-md-2">
                                 <a href="#" onClick={(e) => handleRemoveShareholder2(idx)}>Remove Bank Details (-)</a>
                              </div>
                           </React.Fragment>
                        )
                     }

                  })()}
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="bank_name">Bank Name</Label><br />
                        <TextField
                           type="text"
                           name="bank_name"
                           id="bank_name"
                           fullWidth
                           variant="outlined"
                           placeholder="Bank Name"
                           value={(addData.bank[idx].bank_name != '') ? addData.bank[idx].bank_name : ''}
                           error={(addErr.bank[idx].bank_name) ? true : false}
                           helperText={(addErr.bank[idx].bank_name != '') ? addErr.bank[idx].bank_name : ''}
                           onChange={(e) => onChnagerovider('bank_name', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="bank_address">Bank Address</Label><br />
                        <TextField
                           type="text"
                           name="bank_address"
                           id="bank_address"
                           fullWidth
                           variant="outlined"
                           placeholder="Bank Address"
                           value={(addData.bank[idx].bank_address != '') ? addData.bank[idx].bank_address : ''}
                           error={(addErr.bank[idx].bank_address) ? true : false}
                           helperText={(addErr.bank[idx].bank_address != '') ? addErr.bank[idx].bank_address : ''}
                           onChange={(e) => onChnagerovider('bank_address', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="rounting_no">Routing Number</Label><br />
                        <TextField
                           type="text"
                           name="rounting_no"
                           id="rounting_no"
                           fullWidth
                           variant="outlined"
                           placeholder="Routing Number"
                           value={(addData.bank[idx].rounting_no != '') ? addData.bank[idx].rounting_no : ''}
                           error={(addErr.bank[idx].rounting_no) ? true : false}
                           helperText={(addErr.bank[idx].rounting_no != '') ? addErr.bank[idx].rounting_no : ''}
                           onChange={(e) => onChnagerovider('rounting_no', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="bank_ac">Bank A/C#</Label><br />
                        <TextField
                           type="text"
                           name="bank_ac"
                           id="bank_ac"
                           fullWidth
                           variant="outlined"
                           inputProps={{ maxLength: 17 }}
                           placeholder="Bank A/C#"
                           value={(addData.bank[idx].bank_ac != '') ? addData.bank[idx].bank_ac : ''}
                           error={(addErr.bank[idx].bank_ac) ? true : false}
                           helperText={(addErr.bank[idx].bank_ac != '') ? addErr.bank[idx].bank_ac : ''}
                           onChange={(e) => onChnagerovider('bank_ac', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="account_name">Name on Account</Label><br />
                        <TextField
                           type="text"
                           name="account_name"
                           id="account_name"
                           fullWidth
                           variant="outlined"
                           placeholder="Name on Account"
                           value={(addData.bank[idx].account_name != '') ? addData.bank[idx].account_name : ''}
                           error={(addErr.bank[idx].account_name) ? true : false}
                           helperText={(addErr.bank[idx].account_name != '') ? addErr.bank[idx].account_name : ''}
                           onChange={(e) => onChnagerovider('account_name', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>


                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="account_type">Bank A/C Type</Label>
                        <Input
                           type="select"
                           name="account_type"
                           id="account_type"
                           placeholder=""
                           value={addData.bank[idx].account_type}
                           onChange={(e) => onChnagerovider('account_type', e.target.value, idx)}
                        >
                           <option value="">Select</option>
                           {bankType && bankType.map((bank, key) => (
                              <option value={bank.mdv_id} key={key}>{bank.value}</option>
                           ))}

                        </Input>
                        {(addErr.bank[idx].account_type != '' && addErr.bank[idx].account_type !== undefined) ? <FormHelperText>{addErr.bank[idx].account_type}</FormHelperText> : ''}
                     </FormGroup>
                  </div>

                  <div className="col-md-12">
                     <FormGroup tag="fieldset">
                        <FormGroup check>
                           <Label check>
                              <Input
                                 type="checkbox"
                                 name="primary_bank"
                                 value={1}
                                 checked={(addData.bank[idx].primary_bank == 1) ? true : false}
                                 onChange={(e) => onChnagerovider('primary_bank', e.target.value, idx)}
                              />
                              Set as primary bank
              </Label>
                        </FormGroup>
                     </FormGroup>
                  </div>
               </div>

            ))}
         </Form>
         <div className="col-md-12">
            <a href="#" onClick={(e) => handleAddShareholder2()}>Add more Bank Details (+)</a>
         </div>
         <div className={(addData.co_signer != 1) ? 'd-none' : ''}>
            <hr className="border-dark" />
            <div className="row">
               <div className="col-md-12"><strong>Co-signer Details</strong></div>
            </div>
            <div className="row">
               <div className="col-md-4">
                  <FormGroup>
                     <Label for="co_withdrawal_date">Withdrawal day of the month (e.g-5,6,7...)</Label><br />
                     <TextField
                        type="text"
                        name="co_withdrawal_date"
                        id="co_withdrawal_date"
                        fullWidth
                        variant="outlined"
                        placeholder="Withdrawal day of month"
                        value={(addData.co_withdrawal_date != '') ? addData.co_withdrawal_date : ''}
                        error={(addErr.co_withdrawal_date) ? true : false}
                        helperText={(addErr.co_withdrawal_date != '') ? addErr.co_withdrawal_date : ''}
                        onChange={(e) => onChnagerovider('co_withdrawal_date', e.target.value)}
                     />
                  </FormGroup>
               </div>
            </div>
            {addData.co_bank.map((shareholder, idx) => (
               <div className="row" key={idx}>
                  {(() => {
                     if (idx != 0) {
                        return (
                           <React.Fragment>
                              <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                              <div className="col-md-2">
                                 <a href="#" onClick={(e) => handleRemoveShareholder2Co(idx)}>Remove Bank Details (-)</a>
                              </div>
                           </React.Fragment>
                        )
                     }
                  })()}
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_bank_name">Bank Name</Label><br />
                        <TextField
                           type="text"
                           name="co_bank_name"
                           id="co_bank_name"
                           fullWidth
                           variant="outlined"
                           placeholder="Bank Name"
                           value={(addData.co_bank[idx].co_bank_name != '') ? addData.co_bank[idx].co_bank_name : ''}
                           error={(addErr.co_bank[idx].co_bank_name) ? true : false}
                           helperText={(addErr.co_bank[idx].co_bank_name != '') ? addErr.co_bank[idx].co_bank_name : ''}
                           onChange={(e) => onChnagerovider('co_bank_name', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>
                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_bank_address">Bank Address</Label><br />
                        <TextField
                           type="text"
                           name="co_bank_address"
                           id="co_bank_address"
                           fullWidth
                           variant="outlined"
                           placeholder="Bank Address"
                           value={(addData.co_bank[idx].co_bank_address != '') ? addData.co_bank[idx].co_bank_address : ''}
                           error={(addErr.co_bank[idx].co_bank_address) ? true : false}
                           helperText={(addErr.co_bank[idx].co_bank_address != '') ? addErr.co_bank[idx].co_bank_address : ''}
                           onChange={(e) => onChnagerovider('co_bank_address', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_rounting_no">Routing Number</Label><br />
                        <TextField
                           type="text"
                           name="co_rounting_no"
                           id="co_rounting_no"
                           fullWidth
                           variant="outlined"
                           placeholder="Routing Number"
                           value={(addData.co_bank[idx].co_rounting_no != '') ? addData.co_bank[idx].co_rounting_no : ''}
                           error={(addErr.co_bank[idx].co_rounting_no) ? true : false}
                           helperText={(addErr.co_bank[idx].co_rounting_no != '') ? addErr.co_bank[idx].co_rounting_no : ''}
                           onChange={(e) => onChnagerovider('co_rounting_no', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_bank_ac">Bank A/C#</Label><br />
                        <TextField
                           type="text"
                           name="co_bank_ac"
                           id="co_bank_ac"
                           fullWidth
                           variant="outlined"
                           inputProps={{ maxLength: 17 }}
                           placeholder="Bank A/C#"
                           value={(addData.co_bank[idx].co_bank_ac != '') ? addData.co_bank[idx].co_bank_ac : ''}
                           error={(addErr.co_bank[idx].co_bank_ac) ? true : false}
                           helperText={(addErr.co_bank[idx].co_bank_ac != '') ? addErr.co_bank[idx].co_bank_ac : ''}
                           onChange={(e) => onChnagerovider('co_bank_ac', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>

                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_account_name">Name on Account</Label><br />
                        <TextField
                           type="text"
                           name="co_account_name"
                           id="co_account_name"
                           fullWidth
                           variant="outlined"
                           placeholder="Name on Account"
                           value={(addData.co_bank[idx].co_account_name != '') ? addData.co_bank[idx].co_account_name : ''}
                           error={(addErr.co_bank[idx].co_account_name) ? true : false}
                           helperText={(addErr.co_bank[idx].co_account_name != '') ? addErr.co_bank[idx].co_account_name : ''}
                           onChange={(e) => onChnagerovider('co_account_name', e.target.value, idx)}
                        />
                     </FormGroup>
                  </div>


                  <div className="col-md-4">
                     <FormGroup>
                        <Label for="co_account_type">Bank A/C Type</Label>
                        <Input
                           type="select"
                           name="co_account_type"
                           id="co_account_type"
                           placeholder=""
                           value={addData.co_bank[idx].co_account_type}
                           onChange={(e) => onChnagerovider('co_account_type', e.target.value, idx)}
                        >
                           <option value="">Select</option>
                           {bankType && bankType.map((bank, key) => (
                              <option value={bank.mdv_id} key={key}>{bank.value}</option>
                           ))}

                        </Input>
                        {(addErr.co_bank[idx].co_account_type != '' && addErr.co_bank[idx].co_account_type !== undefined) ? <FormHelperText>{addErr.co_bank[idx].co_account_type}</FormHelperText> : ''}
                     </FormGroup>
                  </div>

                  <div className="col-md-12">
                     <FormGroup tag="fieldset">
                        <FormGroup check>
                           <Label check>
                              <Input
                                 type="checkbox"
                                 name="primary_bank"
                                 value={1}
                                 checked={(addData.co_bank[idx].co_primary_bank == 1) ? true : false}
                                 onChange={(e) => onChnagerovider('co_primary_bank', e.target.value, idx)}
                              />
                              Set as primary bank
              </Label>
                        </FormGroup>
                     </FormGroup>
                  </div>
               </div>

            ))}
            <div className="col-md-12">
               <a href="#" onClick={(e) => handleAddShareholder2Co()}>Add more Bank Details (+)</a>
            </div>
         </div>
      </div>

   );

export default StepFourthCredit;