/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import { InputAdornment, withStyles } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
const StepFirstCredit = ({ addErr, addData, onChnagerovider, DatePicker, startDate, existCustomer, onChnageExist, validateSearch, startDateExist, existError, searchCustomerSubmit, searchCustomerReset, handleClickShowSsn, showSsn, CoStateType, countriesList, co_startDate, relationship }) => (

   <div className="modal-body page-form-outer text-left">

      <div className="row">
         <div className="col-md-4">
            <FormGroup>
               <Label for="first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="first_name"
                  id="first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.first_name}
                  error={(addErr.first_name) ? true : false}
                  helperText={addErr.first_name}
                  onChange={(e) => onChnagerovider('first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="middle_name"
                  id="middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.middle_name}
                  onChange={(e) => onChnagerovider('middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="last_name"
                  id="last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.last_name}
                  error={(addErr.last_name) ? true : false}
                  helperText={addErr.last_name}
                  onChange={(e) => onChnagerovider('last_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-12">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="alias_name"
                        value={1}
                        checked={(addData.alias_name == 1) ? true : false}
                        onChange={(e) => onChnagerovider('alias_name', e.target.value)}
                     />
                     Alias Name
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <div className={(addData.alias_name == 0) ? "row d-none" : "row"}>
         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_first_name">Alias First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_first_name"
                  id="alias_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.alias_first_name}
                  error={(addErr.alias_first_name) ? true : false}
                  helperText={addErr.alias_first_name}
                  onChange={(e) => onChnagerovider('alias_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_middle_name">Alias Middle Name</Label><br />
               <TextField
                  type="text"
                  name="alias_middle_name"
                  id="alias_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.alias_middle_name}
                  onChange={(e) => onChnagerovider('alias_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_last_name">Alias Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_last_name"
                  id="alias_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.alias_last_name}
                  error={(addErr.alias_last_name) ? true : false}
                  helperText={addErr.alias_last_name}
                  onChange={(e) => onChnagerovider('alias_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      <div className="row">

         <div className="col-md-4">
            <FormGroup>
               <Label for="dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="dob"
                  id="dob"
                  selected={startDate}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  maxDate={new Date()}
                  strictParsing
                  onChange={(e) => onChnagerovider('dob', e)}
               />
               {(addErr.dob) ? <FormHelperText className="jss116">{addErr.dob}</FormHelperText> : ''}
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup className="ssn-mask">
               <Label for="ssn">SSN / Tax ID<span className="required-field">*</span></Label><br />
               <TextField
                  type={(!showSsn) ? "password" : "text"}
                  name="ssn"
                  id="ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  InputProps={{
                     endAdornment: (
                        <InputAdornment position="end">
                           <IconButton
                              className="eye-mask"
                              onClick={handleClickShowSsn}
                           >
                              {!showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                           </IconButton>
                        </InputAdornment>
                     ),
                  }}
                  placeholder="Social Security Number"
                  value={addData.ssn}
                  error={(addErr.ssn) ? true : false}
                  helperText={addErr.ssn}
                  onChange={(e) => onChnagerovider('ssn', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      <div className="row">
         <div className="col-md-3">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="freeze_override"
                        value={1}
                        checked={(addData.freeze_override == 1) ? true : false}
                        onChange={(e) => onChnagerovider('freeze_override', e.target.value)}
                     />
                     Is your credit history locked?
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(addData.freeze_override == 0) ? "col-md-9 d-none" : "col-md-9 text-danger"}>
            {/*<FormGroup tag="fieldset">
               <FormGroup>
                  <Label for="amount">Freeze Code<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="freeze_code"
                     id="freeze_code"
                     fullWidth
                     variant="outlined"
                     placeholder="Freeze Code"
                     value={(addData.freeze_code != '') ? addData.freeze_code : ''}
                     error={(addErr.freeze_code) ? true : false}
                     helperText={(addErr.freeze_code != '') ? addErr.freeze_code : ''}
                     onChange={(e) => onChnagerovider('freeze_code', e.target.value)}
                  />
               </FormGroup>
</FormGroup>*/}
            Your application can't be processed because of the lock on your credit history. Please contact your credit agency to unlock your credit history.
         </div>
      </div>
      <div className="row">
         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Gender</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'M'}
                        checked={(addData.gender == 'M') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Male
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'F'}
                        checked={(addData.gender == 'F') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Female
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>


         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Status</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addData.status == 1) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Active
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={0}
                        checked={(addData.status == 0) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Inactive
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <hr className="border-dark" />
      <div className="row">
         <div className="col-md-12"><strong>Secondary Contact Details</strong></div>
      </div>
      <div className="row">
         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="sd_first_name"
                  id="sd_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.sd_first_name}
                  error={(addErr.sd_first_name) ? true : false}
                  helperText={addErr.sd_first_name}
                  onChange={(e) => onChnagerovider('sd_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="sd_middle_name"
                  id="sd_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.sd_middle_name}
                  onChange={(e) => onChnagerovider('sd_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="sd_last_name"
                  id="sd_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.sd_last_name}
                  error={(addErr.sd_last_name) ? true : false}
                  helperText={addErr.sd_last_name}
                  onChange={(e) => onChnagerovider('sd_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_email">Email Address<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="sd_email"
                  id="sd_email"
                  fullWidth
                  variant="outlined"
                  placeholder="Email Address"
                  value={(addData.sd_email != '') ? addData.sd_email : ''}
                  error={(addErr.sd_email) ? true : false}
                  helperText={(addErr.sd_email != '') ? addErr.sd_email : ''}
                  onChange={(e) => onChnagerovider('sd_email', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_phone">Phone No.<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="sd_phone"
                  id="sd_phone"
                  fullWidth
                  variant="outlined"
                  placeholder="Phone No."
                  inputProps={{ maxLength: 14 }}
                  value={(addData.sd_phone != '') ? addData.sd_phone : ''}
                  error={(addErr.sd_phone) ? true : false}
                  helperText={(addErr.sd_phone != '') ? addErr.sd_phone : ''}
                  onChange={(e) => onChnagerovider('sd_phone', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="sd_relationship">Relationship<span className="required-field">*</span></Label>
               <Input
                  type="select"
                  name="sd_relationship"
                  id="sd_relationship"
                  placeholder=""
                  value={addData.sd_relationship}
                  onChange={(e) => onChnagerovider('sd_relationship', e.target.value)}
               >
                  <option value="">Select</option>
                  {relationship && relationship.map((relation, key) => (
                     <option value={relation.status_id} key={key}>{relation.value}</option>
                  ))}
               </Input>
               {(addErr.sd_relationship != '' && addErr.sd_relationship !== undefined) ? <FormHelperText>{addErr.sd_relationship}</FormHelperText> : ''}
            </FormGroup>
         </div>
      </div>

      <hr className="border-dark" />
      <div className="row">
         <div className="col-md-8">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="co_signer"
                        value={1}
                        checked={(addData.co_signer == 1) ? true : false}
                        onChange={(e) => onChnagerovider('co_signer', e)}
                     />
                     <strong class="co-signer-same-font">Co-signer Details</strong>
                  </Label>
               </FormGroup>
            </FormGroup>
         </div>

      </div>
      <div className={(addData.co_signer == 1) ? "row" : "row d-none"}>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_first_name"
                  id="co_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.co_first_name}
                  error={(addErr.co_first_name) ? true : false}
                  helperText={addErr.co_first_name}
                  onChange={(e) => onChnagerovider('co_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="co_middle_name"
                  id="co_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.co_middle_name}
                  onChange={(e) => onChnagerovider('co_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="co_last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="co_last_name"
                  id="co_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.co_last_name}
                  error={(addErr.co_last_name) ? true : false}
                  helperText={addErr.co_last_name}
                  onChange={(e) => onChnagerovider('co_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="co_dob"
                  id="co_dob"
                  selected={co_startDate}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  maxDate={new Date()}
                  strictParsing
                  onChange={(e) => onChnagerovider('co_dob', e)}
               />
               {(addErr.co_dob) ? <FormHelperText className="jss116">{addErr.co_dob}</FormHelperText> : ''}
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup className="ssn-mask">
               <Label for="co_ssn">SSN / Tax ID<span className="required-field">*</span></Label><br />
               <TextField
                  type={(!showSsn) ? "password" : "text"}
                  name="co_ssn"
                  id="co_ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  InputProps={{
                     endAdornment: (
                        <InputAdornment position="end">
                           <IconButton
                              className="eye-mask"
                              onClick={handleClickShowSsn}
                           >
                              {!showSsn ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                           </IconButton>
                        </InputAdornment>
                     ),
                  }}
                  placeholder="Social Security Number"
                  value={addData.co_ssn}
                  error={(addErr.co_ssn) ? true : false}
                  helperText={addErr.co_ssn}
                  onChange={(e) => onChnagerovider('co_ssn', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="co_relationship">Relationship<span className="required-field">*</span></Label>
               <Input
                  type="select"
                  name="co_relationship"
                  id="co_relationship"
                  placeholder=""
                  value={addData.co_relationship}
                  onChange={(e) => onChnagerovider('co_relationship', e.target.value)}
               >
                  <option value="">Select</option>
                  {relationship && relationship.map((relation, key) => (
                     <option value={relation.status_id} key={key}>{relation.value}</option>
                  ))}
               </Input>
               {(addErr.co_relationship != '' && addErr.co_relationship !== undefined) ? <FormHelperText>{addErr.co_relationship}</FormHelperText> : ''}
            </FormGroup>
         </div>


         <div className="col-md-3">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="co_freeze_override"
                        value={1}
                        checked={(addData.co_freeze_override == 1) ? true : false}
                        onChange={(e) => onChnagerovider('co_freeze_override', e.target.value)}
                     />
                     Is your credit history locked?
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(addData.co_freeze_override == 0) ? "col-md-9 d-none" : "col-md-9 text-danger"}>
            {/*<FormGroup tag="fieldset">
               <FormGroup>
                  <Label for="amount">Freeze Code<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="freeze_code"
                     id="freeze_code"
                     fullWidth
                     variant="outlined"
                     placeholder="Freeze Code"
                     value={(addData.freeze_code != '') ? addData.freeze_code : ''}
                     error={(addErr.freeze_code) ? true : false}
                     helperText={(addErr.freeze_code != '') ? addErr.freeze_code : ''}
                     onChange={(e) => onChnagerovider('freeze_code', e.target.value)}
                  />
               </FormGroup>
</FormGroup>*/}
            Your application can't be processed because of the lock on your credit history. Please contact your credit agency to unlock your credit history.
         </div>
      </div>
      <div className="row">
         <div className={(addData.co_signer == 1) ? "col-md-12" : "col-md-12 d-none"}>
            <FormGroup tag="fieldset">
               <Label>Is the co signer the primary?</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="is_co_primary"
                        value={1}
                        checked={(addData.is_co_primary == 1) ? true : false}
                        onChange={(e) => onChnagerovider('is_co_primary', e.target.value)}
                     />
                     Yes
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="is_co_primary"
                        value={0}
                        checked={(addData.is_co_primary == 0) ? true : false}
                        onChange={(e) => onChnagerovider('is_co_primary', e.target.value)}
                     />
                     No
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>


   </div>

);

export default StepFirstCredit;