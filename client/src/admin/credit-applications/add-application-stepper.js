/*===== Horizonatl Non Linear Stepper =====*/
import React from 'react';
import { Redirect } from 'react-router-dom';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
function getSteps() {
   return ['Customer Information', 'Address', 'Employment Information', 'Bank Details', 'User'];
}
import StepExistsCredit from './steps/StepExistsCredit';
import StepFirstCredit from './steps/StepFirstCredit';
import StepSecondCredit from './steps/StepSecondCredit';
import StepThirdCredit from './steps/StepThirdCredit';
import StepFourthCredit from './steps/StepFourthCredit';
import StepFifthCredit from './steps/StepFifthCredit';
import StepSixCredit from './steps/StepSixCredit';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import CryptoJS from 'crypto-js';
import queryString from 'query-string';
import { ageVliad, isEmpty, isEmail, isContainWhiteSpace, isPassword, formatPhoneNumber, isPhone, formatSSNNumber, isSSN, isAlphaDigit, isDecimals, isNumeric, isLength, pointDecimals, isNumericDecimal } from '../../validator/Validator';

import {
   checkUsernameExist,
   applicationOption,
   getStates,
   getRegion,
   addMore,
   removeAddMore,
   editApplicationProvider,
   submitSearchCustomer,
   removeSearchCustomer,
   getSpeciality,
   viewCareCouldPatient,
   checkUsernameExistCo,
   viewApplication,
} from 'Actions';

class HorizontalNonLinearStepper extends React.Component {

   state = {
      showSsn: false,
      activeStep: 0,
      completed: {},
      cancelRedirect: false,
      existCustomer: {
         exist_customer: 0,
         exist_first_name: "",
         exist_last_name: "",
         exist_dob: "",
         exist_ssn: "",
         account_no: "",
      },
      existError: {},
      addData: {
         first_name: "",
         middle_name: "",
         last_name: "",
         alias_name: 0,
         alias_first_name: "",
         alias_middle_name: "",
         alias_last_name: "",
         dob: "",
         ssn: "",
         amount: "",
         freeze_override: 0,
         freeze_code: "",
         gender: 'M',
         status: 1,
         provider_location: '',
         speciality_type: '',
         procedure_date: '',
         procedure_amount: '',
         comment: '',
         procedure_status: 1,
         location: [{
            address1: "",
            address2: "",
            country: "",
            state: "",
            region: "",
            county: "",
            city: "",
            zip_code: "",
            how_long: "",
            phone_no: "",
            primary_address: 1,
            billing_address: 0,
         }],
         billing_address1: "",
         billing_address2: "",
         billing_country: "",
         billing_state: "",
         billing_region: "",
         billing_county: "",
         billing_city: "",
         billing_zip_code: "",
         billing_how_long: "",
         billing_phone_no: "",
         employed: 1,
         employment_type: "",
         annual_income: "",
         employer_name: "",
         employer_phone: "",
         employed_since: "",
         employer_email: "",

         username: "",
         password: "",
         email: "",
         secondary_email: "",
         phone_1: "",
         phone_2: "",
         confirm_password: "",
         co_signer: 0,
         is_co_primary: 0,
         co_first_name: "",
         co_middle_name: "",
         co_last_name: "",
         co_dob: "",
         co_ssn: "",
         co_relationship: "",
         co_freeze_override: 0,

         co_location: [{
            co_address1: "",
            co_address2: "",
            co_country: "",
            co_state: "",
            co_region: "",
            co_county: "",
            co_city: "",
            co_zip_code: "",
            co_how_long: "",
            co_phone_no: "",
            co_primary_address: 1,
            co_billing_address: 0,
         }],
         co_billing_address1: "",
         co_billing_address2: "",
         co_billing_country: "",
         co_billing_state: "",
         co_billing_region: "",
         co_billing_county: "",
         co_billing_city: "",
         co_billing_zip_code: "",
         co_billing_how_long: "",
         co_billing_phone_no: "",

         co_employed: 1,
         co_employment_type: "",
         co_annual_income: "",
         co_employer_name: "",
         co_employer_phone: "",
         co_employed_since: "",
         co_employer_email: "",

         sd_first_name: '',
         sd_middle_name: '',
         sd_last_name: '',
         sd_email: '',
         sd_phone: '',
         sd_relationship: '',
         sd_address1: '',
         sd_address1: '',
         sd_address2: '',
         sd_city: '',
         sd_state: '',
         sd_country: '',
         sd_zip_code: '',
         withdrawal_date: "",
         bank: [{
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_ac: "",
            account_name: "",
            account_type: "",
            primary_bank: 1,
         }],
         co_withdrawal_date: "",
         co_bank: [{
            co_bank_name: "",
            co_bank_address: "",
            co_rounting_no: "",
            co_bank_ac: "",
            co_account_name: "",
            co_account_type: "",
            co_primary_bank: 1,
         }],
         co_username: "",
         co_password: "",
         co_email: "",
         co_secondary_email: "",
         co_phone_1: "",
         co_phone_2: "",
         co_confirm_password: "",
      },
      shareholders: [{ name: "" }],
      add_err: {
         employer_email: '',
         primary_address: '',
         co_primary_address: '',
         location: [{
         }],
         bank: [{
            rounting_no: '',
            bank_ac: '',
         }],
         co_location: [{
         }],
         co_bank: [{
            co_rounting_no: '',
            co_bank_ac: '',
         }],
         withdrawal_date: "",
         co_withdrawal_date: "",
      },
      startDate: '',
      co_startDate: '',
      startDateExist: '',
      employed_since: '',
      co_employed_since: '',
      withdrawal_date: '',
      procedure_date: '',
      sameBilling: false,
      co_sameBilling: false,
      changeURL: 0,
      existUpdate: false,
      resetRedirect: false,
   };
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 19,2019
   */
   componentDidMount() {
      (this.props.patientId !== undefined) ? this.props.viewCareCouldPatient(this.props.patientId) : '';
      this.props.applicationOption();
      this.props.viewApplication(this.props.appId);
   }

   /*
   * Title :- onChnageExist
   * Descrpation :- This function use in search customer error
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 13 June,2019
   */
   onChnageExist(key, value) {
      let { existError } = this.state;
      switch (key) {
         case 'exist_first_name':
            if (isEmpty(value)) {
               existError[key] = "First Name can't be blank";
            } else {
               existError[key] = '';
            }
            break;
         case 'exist_last_name':
            if (isEmpty(value)) {
               existError[key] = "Last Name can't be blank";
            } else {
               existError[key] = '';
            }
            break;
         case 'exist_ssn':
            if (isEmpty(value)) {
               existError[key] = "SSN can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               existError[key] = "SSN not valid";
            } else {
               value = formatSSNNumber(value)
               existError[key] = '';
            }
            break;
         case 'exist_dob':
            if (value == null) {
               existError[key] = "Select Date of Birth";
               this.setState({ startDateExist: '' })
            } else {
               this.setState({ startDateExist: value })
               value = moment(value).format('YYYY-MM-DD');
               existError[key] = '';
            }
            break;
         case 'account_no':
            if (value == null) {
               existError[key] = "Select Date of Birth";
               this.setState({ account_no: '' })
            } else {
               this.setState({ account_no: value })
               existError[key] = '';
            }
            break;
      }
      if (key == 'exist_customer' && value == 0) {
         this.removeexistCustomer();
      } else {
         this.setState({
            existCustomer: {
               ...this.state.existCustomer,
               [key]: value
            }
         });
         this.setState({ existError: existError });
      }
   }
   /*
   * Title :- validateSearch
   * Descrpation :- This function use in search customer error
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 13 June,2019
   */
   validateSearch() {
      return (
         this.state.existError.exist_first_name === '' &&
         this.state.existError.exist_last_name === '' &&
         this.state.existError.exist_dob === '' &&
         this.state.existError.exist_ssn === '' &&
         this.state.existError.account_no === ''
      )
   }
   /*
   * Title :- searchCustomer
   * Descrpation :- This function use in send search customer data
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 13 June,2019
   */
   searchCustomerSubmit() {
      this.setState({
         activeStep: 0,
         completed: {},
         addData: {
            first_name: "",
            middle_name: "",
            last_name: "",
            alias_name: 0,
            alias_first_name: "",
            alias_middle_name: "",
            alias_last_name: "",
            dob: "",
            ssn: "",
            amount: "",
            freeze_override: 0,
            freeze_code: "",
            gender: 'M',
            status: 1,
            provider_location: '',
            speciality_type: '',
            procedure_date: '',
            procedure_amount: '',
            comment: '',
            procedure_status: 1,
            location: [{
               address1: "",
               address2: "",
               country: "",
               state: "",
               region: "",
               county: "",
               city: "",
               zip_code: "",
               how_long: "",
               phone_no: "",
               primary_address: 1,
               billing_address: 0,
            }],
            billing_address1: "",
            billing_address2: "",
            billing_country: "",
            billing_state: "",
            billing_region: "",
            billing_county: "",
            billing_city: "",
            billing_zip_code: "",
            billing_how_long: "",
            billing_phone_no: "",
            employed: 1,
            employment_type: "",
            annual_income: "",
            employer_name: "",
            employer_phone: "",
            employed_since: "",
            withdrawal_date: "",
            procedure_date: "",
            employer_email: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_ac: "",
            account_name: "",
            account_type: "",
            username: "",
            password: "",
            email: "",
            secondary_email: "",
            phone_1: "",
            phone_2: "",
            confirm_password: "",
            co_signer: 0,
         },
         shareholders: [{ name: "" }],
         add_err: {
            primary_address: '',
            location: [{
            }],

         },
         startDate: '',
         employed_since: '',
         withdrawal_date: '',
         procedure_date: '',
         sameBilling: false,
         changeURL: 0,
         existUpdate: false,
         resetRedirect: false,
      }, () => {
         this.props.submitSearchCustomer(this.state.existCustomer);
      })


   }
   /*
   * Title :- searchCustomerReset
   * Descrpation :- This function use for reset page
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 13 June,2019
   */
   searchCustomerReset() {
      this.setState({
         activeStep: 0,
         completed: {},
         existCustomer: {
            exist_customer: 1,
            exist_first_name: "",
            exist_last_name: "",
            exist_dob: "",
            exist_ssn: "",
            account_no: "",
         },
         existError: {},
         addData: {
            first_name: "",
            middle_name: "",
            last_name: "",
            alias_name: 0,
            alias_first_name: "",
            alias_middle_name: "",
            alias_last_name: "",
            dob: "",
            ssn: "",
            amount: "",
            freeze_override: 0,
            freeze_code: "",
            gender: 'M',
            status: 1,
            provider_location: '',
            speciality_type: '',
            procedure_date: '',
            procedure_amount: '',
            comment: '',
            procedure_status: 1,
            location: [{
               address1: "",
               address2: "",
               country: "",
               state: "",
               region: "",
               county: "",
               city: "",
               zip_code: "",
               how_long: "",
               phone_no: "",
               primary_address: 1,
               billing_address: 0,
            }],
            billing_address1: "",
            billing_address2: "",
            billing_country: "",
            billing_state: "",
            billing_region: "",
            billing_county: "",
            billing_city: "",
            billing_zip_code: "",
            billing_how_long: "",
            billing_phone_no: "",
            employed: 1,
            employment_type: "",
            annual_income: "",
            employer_name: "",
            employer_phone: "",
            employed_since: "",
            employer_email: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_ac: "",
            account_name: "",
            account_type: "",
            withdrawal_date: "",
            username: "",
            password: "",
            email: "",
            secondary_email: "",
            phone_1: "",
            phone_2: "",
            confirm_password: "",
            co_signer: 0,
         },
         shareholders: [{ name: "" }],
         add_err: {
            primary_address: '',
            location: [{
            }],

         },
         startDate: '',
         startDateExist: '',
         employed_since: '',
         withdrawal_date: '',
         procedure_date: '',
         sameBilling: false,
         changeURL: 0,
         existUpdate: false,
         resetRedirect: false,
      }, () => {
         this.props.removeSearchCustomer();
      })
   }
   /*
   * Title :- searchCustomerReset
   * Descrpation :- This function use for reset page
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- 13 June,2019
   */
   removeexistCustomer() {
      this.setState({
         activeStep: 0,
         completed: {},
         existCustomer: {
            exist_customer: 0,
            exist_first_name: "",
            exist_last_name: "",
            exist_dob: "",
            exist_ssn: "",
            account_no: "",
         },
         existError: {},
         addData: {
            first_name: "",
            middle_name: "",
            last_name: "",
            alias_name: 0,
            alias_first_name: "",
            alias_middle_name: "",
            alias_last_name: "",
            dob: "",
            ssn: "",
            amount: "",
            freeze_override: 0,
            freeze_code: "",
            gender: 'M',
            status: 1,
            provider_location: '',
            speciality_type: '',
            procedure_date: '',
            procedure_amount: '',
            comment: '',
            procedure_status: 1,
            location: [{
               address1: "",
               address2: "",
               country: "",
               state: "",
               region: "",
               county: "",
               city: "",
               zip_code: "",
               how_long: "",
               phone_no: "",
               primary_address: 1,
               billing_address: 0,
            }],
            billing_address1: "",
            billing_address2: "",
            billing_country: "",
            billing_state: "",
            billing_region: "",
            billing_county: "",
            billing_city: "",
            billing_zip_code: "",
            billing_how_long: "",
            billing_phone_no: "",
            employed: 1,
            employment_type: "",
            annual_income: "",
            employer_name: "",
            employer_phone: "",
            employed_since: "",
            employer_email: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_ac: "",
            account_name: "",
            account_type: "",
            withdrawal_date: "",
            username: "",
            password: "",
            email: "",
            secondary_email: "",
            phone_1: "",
            phone_2: "",
            confirm_password: "",
            co_signer: 0,
         },
         shareholders: [{ name: "" }],
         add_err: {
            primary_address: '',
            location: [{
            }],

         },
         startDate: '',
         startDateExist: '',
         employed_since: '',
         withdrawal_date: '',
         procedure_date: '',
         sameBilling: false,
         changeURL: 0,
         existUpdate: false,
         resetRedirect: false,
      }, () => {
         this.props.removeSearchCustomer();
      });
   }

   onChnagerovider(key, value, idx) {
      let { add_err, sameBilling, co_sameBilling } = this.state;
      let newShareholders;
      switch (key) {
         case 'first_name':
            if (isEmpty(value)) {
               add_err[key] = "First Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'middle_name':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'last_name':
            if (isEmpty(value)) {
               add_err[key] = "Last Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'alias_name':
            value = (this.state.addData.alias_name) ? 0 : 1;
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'alias_first_name':
            if (isEmpty(value)) {
               add_err[key] = "Alias first Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'alias_middle_name':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'alias_last_name':
            if (isEmpty(value)) {
               add_err[key] = "Alias last Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'dob':
            if (value == null) {
               add_err[key] = "Select Date of Birth";
               this.setState({ startDate: '' })
            } else if (ageVliad(value) < 18) {
               add_err[key] = "The minimum age limit to apply for loan should be 18 years.";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'ssn':
            if (isEmpty(value)) {
               add_err[key] = "Social security number can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               add_err[key] = "Social security number not valid";
            } else {
               value = formatSSNNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'freeze_override':
            value = (this.state.addData.freeze_override) ? 0 : 1;
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'freeze_code':
            if (isEmpty(value)) {
               add_err[key] = "Freeze Code can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'gender':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'status':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         /* New changes according to discussionn 08/14/2020*/
         case 'co_signer':
            value = (value.target.checked) ? value.target.value : 0;
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'is_co_primary':

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_first_name':
            if (isEmpty(value)) {
               add_err[key] = "First Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_middle_name':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_last_name':
            if (isEmpty(value)) {
               add_err[key] = "Last Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_dob':
            if (value == null) {
               add_err[key] = "Select Date of Birth";
               this.setState({ co_startDate: '' })
            } else if (ageVliad(value) < 18) {
               add_err[key] = "The minimum age limit to apply for loan should be 18 years.";
               this.setState({ co_startDate: '' })
            } else {
               this.setState({ co_startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_ssn':
            if (isEmpty(value)) {
               add_err[key] = "Social security number can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               add_err[key] = "Social security number not valid";
            } else {
               value = formatSSNNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_relationship':
            if (isEmpty(value)) {
               add_err[key] = "Relationship can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_freeze_override':
            value = (this.state.addData.co_freeze_override) ? 0 : 1;
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'sd_first_name':
            if (isEmpty(value)) {
               add_err[key] = "";//"First Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_middle_name':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_last_name':
            if (isEmpty(value)) {
               add_err[key] = "";//"Last Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_phone':
            if (isEmpty(value)) {
               add_err[key] = "";//"Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_email':
            if (isEmpty(value)) {
               add_err[key] = "";//"Employer email address can't be blank";
            } else if (!isEmail(value) && value != '') {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_address1':
            if (isEmpty(value)) {
               add_err[key] = "";//"Address1 can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_address2':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_country':
            if (isEmpty(value)) {
               add_err[key] = "";//"Country can't be blank";
            } else {
               add_err[key] = '';
               this.props.getStates(value);
            }
            add_err['co_state'] = "";//"State can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value,
                  'co_state': ''
               }
            });
            break;
         case 'sd_state':
            if (isEmpty(value)) {
               add_err[key] = "";//"State can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_city':
            if (isEmpty(value)) {
               add_err[key] = "";//"City can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_zip_code':
            if (isEmpty(value)) {
               add_err[key] = "";//"Zip code can't be blank";
            } else if (!isAlphaDigit(value)) {
               add_err[key] = "Zip code not valid";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'sd_relationship':
            if (isEmpty(value)) {
               add_err[key] = "";//"Relationship can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         /* New changes end here */
         case 'address1':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Address1 can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'address1': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'address2':
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'address2': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'country':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Country can't be blank";
            } else {
               add_err['location'][idx][key] = '';
               this.props.getStates(value, idx);
            }
            add_err['location'][idx]['state'] = "State can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'country': value, 'state': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'state':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "State can't be blank";
            } else {
               add_err['location'][idx][key] = '';
               //this.props.getRegion(value, idx);
            }
            add_err['location'][idx]['region'] = "Region can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'state': value, 'region': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'region':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Region can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'region': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'county':
            /*if (isEmpty(value)) {
               add_err['location'][idx][key] = "County can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }*/
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'county': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'city':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "City can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'city': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'zip_code':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Zip code can't be blank";
            } else if (!isAlphaDigit(value)) {
               add_err['location'][idx][key] = "Zip code not valid";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'zip_code': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'how_long':
            value = pointDecimals(value);
            if (!isEmpty(value) && value > ageVliad(this.state.startDate)) {
               add_err['location'][idx][key] = "Value will be less than or equal to the age of the person";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'how_long': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'phone_no':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'phone_no': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'primary_address':
            let addData = this.state.addData.location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.primary_address = 0
               }
               return item
            })
            this.setState({ addData: addData })
            value = (this.state.addData.location[idx].primary_address) ? 0 : 1;
            if (value == 0) {
               add_err['primary_address'] = "Atleast should be one primary address.";
            } else {
               add_err['primary_address'] = "";
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_address': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_address':
            // unselect all check box for billing
            let addDataN = this.state.addData.location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.billing_address = 0
               }
               return item
            })

            //this.copyBilling(idx);
            //value = (this.state.addData.location[idx].billing_address) ? 0 : 1;
            if ((this.state.addData.location[idx].billing_address)) {
               value = 0;
               sameBilling = false;
            } else {
               value = 1;
               sameBilling = true;
            }
            this.setState({ addData: addDataN, sameBilling: sameBilling })
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_address': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         // billing address
         case 'billing_address1':
            if (isEmpty(value)) {
               add_err[key] = "Address1 can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_address2':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_country':
            if (isEmpty(value)) {
               add_err[key] = "Country can't be blank";
            } else {
               add_err[key] = '';
               this.props.getStates(value);
            }
            add_err['billing_state'] = "State can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value,
                  'billing_state': ''
               }
            });
            break;
         case 'billing_state':
            if (isEmpty(value)) {
               add_err[key] = "State can't be blank";
            } else {
               add_err[key] = '';
               //this.props.getRegion(value);
            }
            add_err['billing_region'] = "Region can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value,
                  'billing_region': ''
               }
            });
            break;
         case 'billing_region':
            if (isEmpty(value)) {
               add_err[key] = "Region can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_county':
            /*if (isEmpty(value)) {
               add_err['location'][idx][key] = "County can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }*/
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_city':
            if (isEmpty(value)) {
               add_err[key] = "City can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_zip_code':
            if (isEmpty(value)) {
               add_err[key] = "Zip code can't be blank";
            } else if (!isAlphaDigit(value)) {
               add_err[key] = "Zip code not valid";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_how_long':
            value = pointDecimals(value);
            if (!isEmpty(value) && value > ageVliad(this.state.startDate)) {
               add_err[key] = "Value will be less than or equal to the age of the person";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'billing_phone_no':
            if (isEmpty(value)) {
               add_err[key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_address1':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "Address1 can't be blank";
            } else {
               add_err['co_location'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_address1': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_address2':
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_address2': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_country':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "Country can't be blank";
            } else {
               add_err['co_location'][idx][key] = '';
               this.props.getStates(value, idx);
            }
            add_err['co_location'][idx]['co_state'] = "State can't be blank";
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_country': value, 'co_state': '' };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_state':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "State can't be blank";
            } else {
               add_err['co_location'][idx][key] = '';
               //this.props.getRegion(value, idx);
            }

            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_state': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_city':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "City can't be blank";
            } else {
               add_err['co_location'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_city': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_zip_code':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "Zip code can't be blank";
            } else if (!isAlphaDigit(value)) {
               add_err['co_location'][idx][key] = "Zip code not valid";
            } else {
               add_err['co_location'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_zip_code': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_how_long':
            value = pointDecimals(value);
            if (!isEmpty(value) && value > ageVliad(this.state.startDate)) {
               add_err['co_location'][idx][key] = "Value will be less than or equal to the age of the person";
            } else {
               add_err['co_location'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_how_long': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_phone_no':
            if (isEmpty(value)) {
               add_err['co_location'][idx][key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err['co_location'][idx][key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err['co_location'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_phone_no': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_primary_address':
            let addDataCo = this.state.addData.co_location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.co_primary_address = 0
               }
               return item
            })
            this.setState({ addData: addDataCo })
            value = (this.state.addData.co_location[idx].co_primary_address) ? 0 : 1;
            if (value == 0) {
               add_err['co_primary_address'] = "Atleast should be one primary address.";
            } else {
               add_err['co_primary_address'] = "";
            }
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_primary_address': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_billing_address':
            // unselect all check box for billing
            let addDataNCo = this.state.addData.co_location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.co_billing_address = 0
               }
               return item
            })

            if ((this.state.addData.co_location[idx].co_billing_address)) {
               value = 0;
               co_sameBilling = false;
            } else {
               value = 1;
               co_sameBilling = true;
            }
            this.setState({ addData: addDataNCo, co_sameBilling: co_sameBilling })
            newShareholders = this.state.addData.co_location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_billing_address': value };
            });
            key = 'co_location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         // billing address
         case 'co_billing_address1':
            if (isEmpty(value)) {
               add_err[key] = "Address1 can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_billing_address2':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_billing_country':
            if (isEmpty(value)) {
               add_err[key] = "Country can't be blank";
            } else {
               add_err[key] = '';
               this.props.getStates(value);
            }
            add_err['co_billing_state'] = "State can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value,
                  'co_billing_state': ''
               }
            });
            break;
         case 'co_billing_state':
            if (isEmpty(value)) {
               add_err[key] = "State can't be blank";
            } else {
               add_err[key] = '';
            }

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value,
               }
            });
            break;
         case 'co_billing_city':
            if (isEmpty(value)) {
               add_err[key] = "City can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_billing_zip_code':
            if (isEmpty(value)) {
               add_err[key] = "Zip code can't be blank";
            } else if (!isAlphaDigit(value)) {
               add_err[key] = "Zip code not valid";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_billing_how_long':
            value = pointDecimals(value);
            if (!isEmpty(value) && value > ageVliad(this.state.startDate)) {
               add_err[key] = "Value will be less than or equal to the age of the person";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_billing_phone_no':
            if (isEmpty(value)) {
               add_err[key] = "Phone number can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'employed':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'employed_since':
            if (value == null) {
               add_err[key] = "Select employed since date";
               this.setState({ employed_since: '' })
            } else {
               this.setState({ employed_since: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'employment_type':
            if (isEmpty(value)) {
               add_err[key] = "Employment type can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'annual_income':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               add_err[key] = "Annual income type can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Annual income type not valid";
            } else if (value < 10000 || value > 500000) {
               add_err[key] = "Annual income greater than 10,000 and less than 500000";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'employer_name':
            if (isEmpty(value)) {
               add_err[key] = "Employer name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'employer_phone':
            if (isEmpty(value)) {
               add_err[key] = "Employer phone no can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Employer phone no not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'employer_email':
            /*if (isEmpty(value)) {
               add_err[key] = "Employer email address can't be blank";
            } else */
            if (!isEmail(value) && value != '') {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_employed':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_employed_since':
            if (value == null) {
               add_err[key] = "Select employed since date";
               this.setState({ co_employed_since: '' })
            } else {
               this.setState({ co_employed_since: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_employment_type':
            if (isEmpty(value)) {
               add_err[key] = "Employment type can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_annual_income':
            value = pointDecimals(value);
            if (isEmpty(value)) {
               add_err[key] = "Annual income type can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Annual income type not valid";
            } else if (value < 10000 || value > 500000) {
               add_err[key] = "Annual income greater than 10,000 and less than 500000";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_employer_name':
            if (isEmpty(value)) {
               add_err[key] = "Employer name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_employer_phone':
            if (isEmpty(value)) {
               add_err[key] = "Employer phone no can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Employer phone no not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_employer_email':
            /*if (isEmpty(value)) {
               add_err[key] = "Employer email address can't be blank";
            } else */
            if (!isEmail(value) && value != '') {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'bank_name':
            /*if (isEmpty(value)) {
               add_err[key] = "Bank name can't be blank";
            } else {
               add_err[key] = '';
            }*/
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_name': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'bank_address':
            /*if (isEmpty(value)) {
               add_err[key] = "Bank address can't be blank";
            } else {
               add_err[key] = '';
            }*/
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_address': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'rounting_no':
            /*if (isEmpty(value)) {
               add_err[key] = "Routing number can't be blank";
            } else */
            if (isNumeric(value) && !isEmpty(value)) {
               add_err['bank'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 9, trim: true }) && !isEmpty(value)) {
               add_err['bank'][idx][key] = "Please enter 9 digit valid routing number";
            } else {
               add_err['bank'][idx][key] = '';
            }
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'rounting_no': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'bank_ac':
            /*if (isEmpty(value)) {
               add_err[key] = "Bank A/C can't be blank";
            } else */
            if (isNumeric(value) && !isEmpty(value)) {
               add_err['bank'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 8, trim: true }) && !isEmpty(value)) {
               add_err['bank'][idx][key] = "Please enter correct bank account number";
            } else {
               add_err['bank'][idx][key] = '';
            }
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_ac': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'account_name':
            /*if (isEmpty(value)) {
               add_err[key] = "Name on account can't be blank";
            } else {
               add_err[key] = '';
            }*/
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'account_name': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'account_type':
            /*if (isEmpty(value)) {
               add_err[key] = "Bank A/C type can't be blank";
            } else {
               add_err[key] = '';
            }*/
            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'account_type': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'primary_bank':
            let addDatab = this.state.addData.bank.map(function (item, idsx) {
               if (idx != idsx) {
                  item.primary_bank = 0
               }
               return item
            })
            this.setState({ addData: addDatab })
            value = (this.state.addData.bank[idx].primary_bank) ? 0 : 1;

            newShareholders = this.state.addData.bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_bank': value };
            });
            key = 'bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'withdrawal_date':
            if (isNumericDecimal(value) && !isEmpty(value)) {
               add_err[key] = "Withdrawal day of month should be numeric";
            } else if ((value < 1 || value > 31) && !isEmpty(value)) {
               add_err[key] = "Withdrawal day of month should be in 1-31.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_bank_name':
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_bank_name': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_bank_address':
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_bank_address': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_rounting_no':
            if (isNumeric(value) && !isEmpty(value)) {
               add_err['co_bank'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 9, trim: true }) && !isEmpty(value)) {
               add_err['co_bank'][idx][key] = "Please enter 9 digit valid routing number";
            } else {
               add_err['co_bank'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_rounting_no': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_bank_ac':
            if (isNumeric(value) && !isEmpty(value)) {
               add_err['co_bank'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 8, trim: true }) && !isEmpty(value)) {
               add_err['co_bank'][idx][key] = "Please enter correct bank account number";
            } else {
               add_err['co_bank'][idx][key] = '';
            }
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_bank_ac': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_account_name':
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_account_name': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_account_type':
            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_account_type': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_primary_bank':
            let addDatabCo = this.state.addData.co_bank.map(function (item, idsx) {
               if (idx != idsx) {
                  item.co_primary_bank = 0
               }
               return item
            })
            this.setState({ addData: addDatabCo })
            value = (this.state.addData.co_bank[idx].co_primary_bank) ? 0 : 1;

            newShareholders = this.state.addData.co_bank.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'co_primary_bank': value };
            });
            key = 'co_bank';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'co_withdrawal_date':
            if (isNumericDecimal(value) && !isEmpty(value)) {
               add_err[key] = "Withdrawal day of month should be numeric";
            } else if ((value < 1 || value > 31) && !isEmpty(value)) {
               add_err[key] = "Withdrawal day of month should be in 1-31.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;


         case 'username':
            if (isEmpty(value)) {
               add_err[key] = "Username can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Username should not contain white spaces";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'password':
            if (isEmpty(value)) {
               add_err[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (this.state.addData.confirm_password !== '' && this.state.addData.confirm_password !== value) {
               add_err[key] = "Password and confirm password not match";
            } else if (this.state.addData.confirm_password !== '' && this.state.addData.confirm_password === value) {
               add_err[key] = "";
               add_err['confirm_password'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'confirm_password':
            if (isEmpty(value)) {
               add_err[key] = "Confirm password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Confirm password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Confirm password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (value !== this.state.addData.password) {
               add_err[key] = "Password and confirm password not match";
            } else if (value === this.state.addData.password) {
               add_err[key] = "";
               add_err['password'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'email':
            if (isEmpty(value)) {
               add_err[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'secondary_email':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else if (!isEmail(value)) {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'phone_1':
            if (isEmpty(value)) {
               add_err[key] = "Primary Phone No. can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Primary Phone No. not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'phone_2':
            if (isPhone(value) && !isEmpty(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Secondary Phone No. not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_username':
            if (isEmpty(value)) {
               add_err[key] = "Username can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Username should not contain white spaces";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_password':
            if (isEmpty(value)) {
               add_err[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (this.state.addData.co_confirm_password !== '' && this.state.addData.co_confirm_password !== value) {
               add_err[key] = "Password and confirm password not match";
            } else if (this.state.addData.co_confirm_password !== '' && this.state.addData.co_confirm_password === value) {
               add_err[key] = "";
               add_err['co_confirm_password'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_confirm_password':
            if (isEmpty(value)) {
               add_err[key] = "Confirm password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Confirm password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Confirm password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (value !== this.state.addData.co_password) {
               add_err[key] = "Password and confirm password not match";
            } else if (value === this.state.addData.co_password) {
               add_err[key] = "";
               add_err['co_password'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'co_email':
            if (isEmpty(value)) {
               add_err[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_secondary_email':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else if (!isEmail(value)) {
               add_err[key] = "Please enter a valid email address.";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_phone_1':
            if (isEmpty(value)) {
               add_err[key] = "Primary Phone No. can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Primary Phone No. not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'co_phone_2':
            if (isPhone(value) && !isEmpty(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Secondary Phone No. not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'provider_location':
            if (isEmpty(value)) {
               add_err[key] = "Location can't be blank";
            } else {
               this.props.getSpeciality(value);
               add_err[key] = '';
            }
            add_err['speciality_type'] = "Speciality can't be blank";
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'speciality_type':
            if (isEmpty(value)) {
               add_err[key] = "Speciality can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_date':
            if (value == null) {
               add_err[key] = "Select Procedure Date";
               this.setState({ procedure_date: '' })
            } else {
               this.setState({ procedure_date: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'amount':
            if (isEmpty(value)) {
               add_err[key] = "Amount can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Amount not valid";
            }
            else if (this.state.addData.procedure_amount !== '' && this.state.addData.procedure_amount < value) {
               add_err[key] = "Loan amount less than or equal to procedure amount";
            } else if (this.state.addData.procedure_amount !== '' && this.state.addData.procedure_amount >= value) {
               add_err[key] = "";
               add_err['procedure_amount'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_amount':
            if (isEmpty(value)) {
               add_err[key] = "Procedure amount can't be blank";
            } else if (!isDecimals(value)) {
               add_err[key] = "Procedure amount not valid";
            } else if (value < this.state.addData.amount) {
               add_err[key] = "Procedure amount greater than or equal to loan amount";
            } else if (value >= this.state.addData.amount) {
               add_err[key] = "";
               add_err['amount'] = "";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'procedure_status':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;


      }

      this.setState({ add_err: add_err });


   }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }
   submitData() {
      this.props.editApplicationProvider(this.state.addData);
   }
   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         //const steps = getSteps();
         //activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
         activeStep = 4;
         this.submitData();
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleCancel = () => {
      this.setState({
         cancelRedirect: true,
      });
   }

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };
   /*
      * Title :- validateAddSubmit
      * Descrpation :- This function use for enable or disable submit button in add case
      * Author : Cognizsoft and Ramesh Kumar
      * Date :- April 16,2019
      */
   validateSubmit() {
      // check vaidation according to step
      /*if (this.state.activeStep === 0) {
         if (this.state.existCustomer.exist_customer == 1) {
            return (this.props.appSearchDetails) ? true : false;
         } else {
            return true;
         }
      } else */if (this.state.activeStep === 0) {
         /**
          * First Check c-signer details
          * Second check seconday contact details
          * Third check primary contact details
          */
         if (this.state.addData.co_signer == 1) {
            if (this.state.addData.alias_name == 1) {
               if (this.state.addData.freeze_override == 1) {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.alias_first_name === '' &&
                     this.state.add_err.alias_last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.co_first_name === '' &&
                     this.state.add_err.co_last_name === '' &&
                     this.state.add_err.co_dob === '' &&
                     this.state.add_err.co_ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               } else {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.alias_first_name === '' &&
                     this.state.add_err.alias_last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.co_first_name === '' &&
                     this.state.add_err.co_last_name === '' &&
                     this.state.add_err.co_dob === '' &&
                     this.state.add_err.co_ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               }
            } else {
               if (this.state.addData.freeze_override == 1) {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.freeze_code === '' &&
                     this.state.add_err.co_first_name === '' &&
                     this.state.add_err.co_last_name === '' &&
                     this.state.add_err.co_dob === '' &&
                     this.state.add_err.co_ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               } else {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.co_first_name === '' &&
                     this.state.add_err.co_last_name === '' &&
                     this.state.add_err.co_dob === '' &&
                     this.state.add_err.co_ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               }
            }
         } else {
            if (this.state.addData.alias_name == 1) {
               if (this.state.addData.freeze_override == 1) {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.alias_first_name === '' &&
                     this.state.add_err.alias_last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''/*&&
                     this.state.add_err.freeze_code === ''*/
                  )
               } else {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.alias_first_name === '' &&
                     this.state.add_err.alias_last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               }
            } else {
               if (this.state.addData.freeze_override == 1) {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.freeze_code === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               } else {
                  return (
                     this.state.add_err.first_name === '' &&
                     this.state.add_err.last_name === '' &&
                     this.state.add_err.dob === '' &&
                     this.state.add_err.ssn === '' &&
                     this.state.add_err.sd_first_name === '' &&
                     this.state.add_err.sd_last_name === '' &&
                     this.state.add_err.sd_email === '' &&
                     this.state.add_err.sd_phone === '' &&
                     this.state.add_err.sd_relationship === ''
                  )
               }
            }
         }

         return true;
      } else if (this.state.activeStep === 1) {
         if (this.state.addData.co_signer == 1) {
            //check main application address
            var errorLoc = true;
            var primary_address = this.state.add_err.primary_address;
            var locArray = this.state.addData.location;
            this.state.add_err.location.map(function (item, idx) {
               if (errorLoc == true) {
                  if (locArray[idx].how_long != '') {
                     errorLoc = (
                        item.address1 === '' &&
                        /*item.country === '' &&*/
                        item.state === '' &&
                        /*item.region === '' &&*/
                        item.city === '' &&
                        item.zip_code === '' &&
                        item.phone_no === '' &&
                        item.how_long === '' &&
                        primary_address === ''
                     ) ? true : false;
                  } else {
                     errorLoc = (
                        item.address1 === '' &&
                        /*item.country === '' &&*/
                        item.state === '' &&
                        /*item.region === '' &&*/
                        item.city === '' &&
                        item.zip_code === '' &&
                        item.phone_no === '' &&
                        primary_address === ''
                     ) ? true : false;
                  }

               }
            })
            // check co-signer details
            var errorLocCo = true;
            var co_primary_address = this.state.add_err.co_primary_address;
            var co_locArray = this.state.addData.co_location;
            this.state.add_err.co_location.map(function (item, idx) {
               if (errorLocCo == true) {
                  if (co_locArray[idx].co_how_long != '') {
                     errorLocCo = (
                        item.co_address1 === '' &&
                        /*item.country === '' &&*/
                        item.co_state === '' &&
                        /*item.region === '' &&*/
                        item.co_city === '' &&
                        item.co_zip_code === '' &&
                        item.co_phone_no === '' &&
                        item.co_how_long === '' &&
                        co_primary_address === ''
                     ) ? true : false;
                  } else {
                     errorLocCo = (
                        item.co_address1 === '' &&
                        /*item.country === '' &&*/
                        item.co_state === '' &&
                        /*item.region === '' &&*/
                        item.co_city === '' &&
                        item.co_zip_code === '' &&
                        item.co_phone_no === '' &&
                        co_primary_address === ''
                     ) ? true : false;
                  }

               }
            })

            if (this.state.sameBilling && this.state.co_sameBilling) {
               return (
                  this.state.add_err.sd_address1 === '' &&
                  this.state.add_err.sd_city === '' &&
                  this.state.add_err.sd_state === '' &&
                  this.state.add_err.sd_zip_code === '' &&
                  errorLoc === true &&
                  errorLocCo === true
               )
            } else if (!this.state.sameBilling && this.state.co_sameBilling) {
               if (this.state.addData.billing_how_long != '') {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     /*this.state.add_err.billing_country === '' &&*/
                     this.state.add_err.billing_state === '' &&
                     /*this.state.add_err.billing_region === '' &&*/
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.billing_how_long === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               } else {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     /*this.state.add_err.billing_country === '' &&*/
                     this.state.add_err.billing_state === '' &&
                     /*this.state.add_err.billing_region === '' &&*/
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               }
            } else if (this.state.sameBilling && !this.state.co_sameBilling) {
               if (this.state.addData.co_billing_how_long != '') {
                  return (
                     this.state.add_err.co_billing_address1 === '' &&
                     /*this.state.add_err.billing_country === '' &&*/
                     this.state.add_err.co_billing_state === '' &&
                     /*this.state.add_err.billing_region === '' &&*/
                     this.state.add_err.co_billing_city === '' &&
                     this.state.add_err.co_billing_zip_code === '' &&
                     this.state.add_err.co_billing_phone_no === '' &&
                     this.state.add_err.co_billing_how_long === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               } else {
                  return (
                     this.state.add_err.co_billing_address1 === '' &&
                     this.state.add_err.co_billing_state === '' &&
                     this.state.add_err.co_billing_city === '' &&
                     this.state.add_err.co_billing_zip_code === '' &&
                     this.state.add_err.co_billing_phone_no === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               }
            } else {
               if (this.state.addData.billing_how_long != '') {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     this.state.add_err.billing_state === '' &&
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.billing_how_long === '' &&
                     this.state.add_err.co_billing_address1 === '' &&
                     this.state.add_err.co_billing_state === '' &&
                     this.state.add_err.co_billing_city === '' &&
                     this.state.add_err.co_billing_zip_code === '' &&
                     this.state.add_err.co_billing_phone_no === '' &&
                     this.state.add_err.co_billing_how_long === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               } else {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     this.state.add_err.billing_state === '' &&
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.co_billing_address1 === '' &&
                     this.state.add_err.co_billing_state === '' &&
                     this.state.add_err.co_billing_city === '' &&
                     this.state.add_err.co_billing_zip_code === '' &&
                     this.state.add_err.co_billing_phone_no === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true &&
                     errorLocCo === true
                  )
               }

            }
         } else {
            //check main application address
            var errorLoc = true;
            var primary_address = this.state.add_err.primary_address;
            var locArray = this.state.addData.location;
            this.state.add_err.location.map(function (item, idx) {
               if (errorLoc == true) {
                  if (locArray[idx].how_long != '') {
                     errorLoc = (
                        item.address1 === '' &&
                        /*item.country === '' &&*/
                        item.state === '' &&
                        /*item.region === '' &&*/
                        item.city === '' &&
                        item.zip_code === '' &&
                        item.phone_no === '' &&
                        item.how_long === '' &&
                        primary_address === ''
                     ) ? true : false;
                  } else {
                     errorLoc = (
                        item.address1 === '' &&
                        /*item.country === '' &&*/
                        item.state === '' &&
                        /*item.region === '' &&*/
                        item.city === '' &&
                        item.zip_code === '' &&
                        item.phone_no === '' &&
                        primary_address === ''
                     ) ? true : false;
                  }

               }
            })
            // check co-signer details
            var errorLocCo = true;
            var co_primary_address = this.state.add_err.co_primary_address;
            var co_locArray = this.state.addData.co_location;
            this.state.add_err.co_location.map(function (item, idx) {
               if (errorLocCo == true) {
                  if (co_locArray[idx].co_how_long != '') {
                     errorLocCo = (
                        item.co_address1 === '' &&
                        /*item.country === '' &&*/
                        item.co_state === '' &&
                        /*item.region === '' &&*/
                        item.co_city === '' &&
                        item.co_zip_code === '' &&
                        item.co_phone_no === '' &&
                        item.co_how_long === '' &&
                        co_primary_address === ''
                     ) ? true : false;
                  } else {
                     errorLocCo = (
                        item.co_address1 === '' &&
                        /*item.country === '' &&*/
                        item.co_state === '' &&
                        /*item.region === '' &&*/
                        item.co_city === '' &&
                        item.co_zip_code === '' &&
                        item.co_phone_no === '' &&
                        co_primary_address === ''
                     ) ? true : false;
                  }

               }
            })

            if (this.state.sameBilling) {
               return (
                  this.state.add_err.sd_address1 === '' &&
                  this.state.add_err.sd_city === '' &&
                  this.state.add_err.sd_state === '' &&
                  this.state.add_err.sd_zip_code === '' &&
                  errorLoc === true
               )
            } else {
               if (this.state.addData.billing_how_long != '') {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     /*this.state.add_err.billing_country === '' &&*/
                     this.state.add_err.billing_state === '' &&
                     /*this.state.add_err.billing_region === '' &&*/
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.billing_how_long === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true
                  )
               } else {
                  return (
                     this.state.add_err.billing_address1 === '' &&
                     /*this.state.add_err.billing_country === '' &&*/
                     this.state.add_err.billing_state === '' &&
                     /*this.state.add_err.billing_region === '' &&*/
                     this.state.add_err.billing_city === '' &&
                     this.state.add_err.billing_zip_code === '' &&
                     this.state.add_err.billing_phone_no === '' &&
                     this.state.add_err.sd_address1 === '' &&
                     this.state.add_err.sd_city === '' &&
                     this.state.add_err.sd_state === '' &&
                     this.state.add_err.sd_zip_code === '' &&
                     errorLoc === true
                  )
               }

            }
         }

         //return true;
      } else if (this.state.activeStep === 2) {
         if (this.state.addData.co_signer == 1) {
            if (this.state.addData.employed == 1 && this.state.addData.co_employed == 1) {
               return (
                  this.state.add_err.annual_income === '' &&
                  this.state.add_err.employer_name === '' &&
                  this.state.add_err.employer_phone === '' &&
                  this.state.add_err.employer_email === '' &&
                  this.state.add_err.employed_since === '' &&
                  this.state.add_err.co_annual_income === '' &&
                  this.state.add_err.co_employer_name === '' &&
                  this.state.add_err.co_employer_phone === '' &&
                  this.state.add_err.co_employer_email === '' &&
                  this.state.add_err.co_employed_since === ''
               )
            } else if (this.state.addData.employed == 1) {
               return (
                  this.state.add_err.annual_income === '' &&
                  this.state.add_err.employer_name === '' &&
                  this.state.add_err.employer_phone === '' &&
                  this.state.add_err.employer_email === '' &&
                  this.state.add_err.employed_since === ''
               )
            } else if (this.state.addData.co_employed == 1) {
               return (
                  this.state.add_err.co_annual_income === '' &&
                  this.state.add_err.co_employer_name === '' &&
                  this.state.add_err.co_employer_phone === '' &&
                  this.state.add_err.co_employer_email === '' &&
                  this.state.add_err.co_employed_since === ''
               )
            } else {
               return true;
            }
         } else {
            if (this.state.addData.employed == 1) {
               return (
                  this.state.add_err.annual_income === '' &&
                  this.state.add_err.employer_name === '' &&
                  this.state.add_err.employer_phone === '' &&
                  this.state.add_err.employer_email === '' &&
                  this.state.add_err.employed_since === ''
               )
            } else {
               return true;
            }
         }

      } else if (this.state.activeStep === 3) {
         var errorLoc = true;
         var errorLocCo = true;
         var withdrawal_date = this.state.add_err.withdrawal_date;
         var co_withdrawal_date = this.state.add_err.co_withdrawal_date;
         if (this.state.addData.co_signer == 1) {
            this.state.add_err.bank.map(function (item, idx) {

               if (errorLoc == true) {
                  if (item.rounting_no != '' && item.bank_ac != '' && item.rounting_no !== undefined && item.bank_ac !== undefined) {
                     errorLoc = (
                        item.rounting_no === '' &&
                        item.bank_ac === '' &&
                        withdrawal_date === ''
                     )
                  } else if (item.rounting_no != '' && item.rounting_no !== undefined) {
                     errorLoc = (
                        item.rounting_no === '' &&
                        withdrawal_date === ''
                     )
                  } else if (item.bank_ac != '' && item.bank_ac !== undefined) {
                     errorLoc = (
                        item.bank_ac === '' &&
                        withdrawal_date === ''
                     )
                  } else if (withdrawal_date != '' && withdrawal_date !== undefined) {
                     errorLoc = (
                        withdrawal_date === ''
                     )
                  }
               }
            })

            this.state.add_err.co_bank.map(function (item, idx) {
               if (errorLocCo == true) {
                  if (item.co_rounting_no != '' && item.co_bank_ac != '' && item.co_rounting_no !== undefined && item.co_bank_ac !== undefined) {
                     errorLocCo = (
                        item.co_rounting_no === '' &&
                        item.co_bank_ac === '' &&
                        co_withdrawal_date === ''
                     )
                  } else if (item.co_rounting_no != '' && item.co_rounting_no !== undefined) {
                     errorLocCo = (
                        item.co_rounting_no === '' &&
                        co_withdrawal_date === ''
                     )
                  } else if (item.co_bank_ac != '' && item.co_bank_ac !== undefined) {
                     errorLocCo = (
                        item.co_bank_ac === '' &&
                        co_withdrawal_date === ''
                     )
                  } else if (co_withdrawal_date != '' && co_withdrawal_date !== undefined) {
                     errorLocCo = (
                        co_withdrawal_date === ''
                     )
                  }
               }
            })
            return (errorLoc && errorLocCo);
         } else {
            this.state.add_err.bank.map(function (item, idx) {
               if (errorLoc == true) {
                  if (item.rounting_no != '' && item.bank_ac != '' && item.rounting_no !== undefined && item.bank_ac !== undefined) {
                     errorLoc = (
                        item.rounting_no === '' &&
                        item.bank_ac === '' &&
                        withdrawal_date === ''
                     )
                  } else if (item.rounting_no != '' && item.rounting_no !== undefined) {
                     errorLoc = (
                        item.rounting_no === ''
                     )
                  } else if (item.bank_ac != '' && item.bank_ac !== undefined) {
                     errorLoc = (
                        item.bank_ac === ''
                     )
                  } else if (withdrawal_date != '' && item.withdrawal_date !== undefined) {
                     errorLoc = (
                        withdrawal_date === ''
                     )
                  }
               }
            })
            return errorLoc;
         }

         if (this.state.addData.rounting_no != '' && this.state.addData.bank_ac != '') {
            return (
               this.state.add_err.rounting_no === '' &&
               this.state.add_err.bank_ac === '' &&
               this.state.add_err.withdrawal_date === ''
            )
         } else if (this.state.addData.rounting_no != '') {
            return (
               this.state.add_err.rounting_no === ''
            )
         } else if (this.state.addData.bank_ac != '') {
            return (
               this.state.add_err.bank_ac === ''
            )
         } else if (this.state.addData.withdrawal_date != '') {
            return (
               this.state.add_err.withdrawal_date === ''
            )
         } else {
            return true;
         }
         //return true;
         /*return (
            this.state.add_err.bank_name === '' &&
            this.state.add_err.rounting_no === '' &&
            this.state.add_err.bank_ac === '' &&
            this.state.add_err.account_name === '' &&
            this.state.add_err.account_type === ''
         )*/
      } else if (this.state.activeStep === 4) {
         if (this.state.addData.co_signer == 1) {
            if (this.state.addData.phone_2 != '' && this.state.addData.co_phone_2 != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === '' &&
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_phone_2 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.co_phone_2 != '') {
               return (
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_phone_2 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '' && this.state.addData.secondary_email != '' && this.state.addData.co_phone_2 != '' && this.state.addData.co_secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === '' &&
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_secondary_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_phone_2 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '' && this.state.addData.secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.co_phone_2 != '' && this.state.addData.co_secondary_email != '') {
               return (
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_secondary_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_phone_2 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '' && this.state.addData.secondary_email == '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email != '' && this.state.addData.co_phone_2 == '' && this.state.addData.co_secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === '' &&
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_secondary_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.co_phone_2 == '' && this.state.addData.co_secondary_email != '') {
               return (
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_secondary_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email == '' && this.state.addData.co_phone_2 == '' && this.state.addData.co_secondary_email == '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === '' &&
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email == '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.co_phone_2 == '' && this.state.addData.co_secondary_email == '') {
               return (
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            } else {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === '' &&
                  this.state.add_err.co_username === '' &&
                  this.state.add_err.co_password === '' &&
                  this.state.add_err.co_email === '' &&
                  this.state.add_err.co_secondary_email === '' &&
                  this.state.add_err.co_phone_1 === '' &&
                  this.state.add_err.co_confirm_password === ''
               )
            }
         } else {
            if (this.state.addData.phone_2 != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '' && this.state.addData.secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.phone_2 != '' && this.state.addData.secondary_email == '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.phone_2 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email != '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else if (this.state.addData.phone_2 == '' && this.state.addData.secondary_email == '') {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            } else {
               return (
                  this.state.add_err.username === '' &&
                  this.state.add_err.password === '' &&
                  this.state.add_err.email === '' &&
                  this.state.add_err.secondary_email === '' &&
                  this.state.add_err.phone_1 === '' &&
                  this.state.add_err.confirm_password === ''
               )
            }
         }



      } /*else if (this.state.activeStep === 6) {
         return (
            this.state.add_err.provider_location === '' &&
            //this.state.add_err.speciality_type === '' &&
            this.state.add_err.procedure_date === '' &&
            this.state.add_err.amount === '' &&
            this.state.add_err.procedure_amount === '' &&
            this.state.add_err.comment === ''
         )
      }*/
   }
   handleNameChange = evt => {
      this.setState({ name: evt.target.value });
   };

   handleShareholderNameChange(idx, value) {
      const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
         if (idx !== sidx) return shareholder;
         return { ...shareholder, name: value };
      });
      this.setState({ shareholders: newShareholders });
   };

   handleSubmit = evt => {
      const { name, shareholders } = this.state;
      alert(`Incorporated: ${name} with ${shareholders.length} shareholders`);
   };
   /*
   * Title :- handleAddShareholder
   * Descrpation :- This function use add new array
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   handleAddShareholder() {
      this.props.addMore();
      const { addData, add_err } = this.state;


      addData.location = this.state.addData.location.concat([{
         address1: "",
         address2: "",
         country: "",
         state: "",
         region: "",
         county: "",
         city: "",
         zip_code: "",
         how_long: "",
         phone_no: "",
         primary_address: 0,
         billing_address: 0,
      }]);
      this.props.getStates(this.props.countries[0].id, (addData.location.length - 1));
      add_err.location = this.state.add_err.location.concat([{
         /*address1: "",
         address2: "",
         country: "",
         state: "",
         region: "",
         county: "",
         city: "",
         zip_code: "",
         how_long: "",
         phone_no: "",*/
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };
   /*
   * Title :- handleRemoveShareholder
   * Descrpation :- This function use remove array
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   handleRemoveShareholder(idx) {
      const { addData, add_err } = this.state;
      if (this.state.addData.location[idx].primary_address == 1) {
         add_err['primary_address'] = "Atleast should be one primary address.";
      }
      this.props.removeAddMore(idx);


      addData.location = this.state.addData.location.filter((s, sidx) => idx !== sidx)
      add_err.location = this.state.add_err.location.filter((s, sidx) => idx !== sidx)

      this.setState({
         addData: addData, add_err: add_err
      });
   };
   handleAddShareholderCo() {
      this.props.addMore();
      const { addData, add_err } = this.state;


      addData.co_location = this.state.addData.co_location.concat([{
         co_address1: "",
         co_address2: "",
         co_country: "",
         co_state: "",
         co_region: "",
         co_county: "",
         co_city: "",
         co_zip_code: "",
         co_how_long: "",
         co_phone_no: "",
         co_primary_address: 0,
         co_billing_address: 0,
      }]);
      this.props.getStates(this.props.countries[0].id, (addData.co_location.length - 1), 1);
      add_err.co_location = this.state.add_err.co_location.concat([{
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };
   handleRemoveShareholderCo(idx) {
      const { addData, add_err } = this.state;
      if (this.state.addData.co_location[idx].co_primary_address == 1) {
         add_err['primary_address'] = "Atleast should be one primary address.";
      }
      this.props.removeAddMore(idx, 1);


      addData.co_location = this.state.addData.co_location.filter((s, sidx) => idx !== sidx)
      add_err.co_location = this.state.add_err.co_location.filter((s, sidx) => idx !== sidx)

      this.setState({
         addData: addData, add_err: add_err
      });
   };
   handleAddShareholder2() {
      this.props.addMore();
      const { addData, add_err } = this.state;


      addData.bank = this.state.addData.bank.concat([{
         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_ac: "",
         account_name: "",
         account_type: "",
         primary_bank: 0,
      }]);

      add_err.bank = this.state.add_err.bank.concat([{

      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };
   handleRemoveShareholder2(idx) {
      const { addData, add_err } = this.state;


      addData.bank = this.state.addData.bank.filter((s, sidx) => idx !== sidx)
      add_err.bank = this.state.add_err.bank.filter((s, sidx) => idx !== sidx)

      this.setState({
         addData: addData, add_err: add_err
      });
   }
   handleAddShareholder2Co() {
      //this.props.addMore();
      const { addData, add_err } = this.state;


      addData.co_bank = this.state.addData.co_bank.concat([{
         co_bank_name: "",
         co_bank_address: "",
         co_rounting_no: "",
         co_bank_ac: "",
         co_account_name: "",
         co_account_type: "",
         co_primary_bank: 0,
      }]);

      add_err.co_bank = this.state.add_err.co_bank.concat([{
         co_rounting_no: '',
         co_bank_ac: '',
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };
   handleRemoveShareholder2Co(idx) {
      const { addData, add_err } = this.state;


      addData.co_bank = this.state.addData.co_bank.filter((s, sidx) => idx !== sidx)
      add_err.co_bank = this.state.add_err.co_bank.filter((s, sidx) => idx !== sidx)

      this.setState({
         addData: addData, add_err: add_err
      });
   }
   /*
   * Title :- checkUsernameExist
   * Descrpation :- This function use for check username exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkUsernameExist(value, md_id) {
      this.props.checkUsernameExist(value, md_id);
   }
   checkUsernameExistCo(value, md_id) {
      this.props.checkUsernameExistCo(value, md_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      let { addData, add_err } = this.state;
      (nextProps.nameExist && nextProps.isEdit == 0) ? add_err['username'] = "Username already exists" : '';
      (nextProps.nameExistCO && nextProps.isEditCO == 0) ? add_err['co_username'] = "Username already exists" : '';

      this.setState({ add_err: add_err });
      (nextProps.rediretURL != '') ? this.setState({ changeURL: nextProps.rediretURL }) : '';
      if (nextProps.appDetails !== undefined && this.state.existUpdate === false) {
         addData.app_id = this.props.appId;
         addData.patient_id = nextProps.appDetails.patient_id;
         addData.first_name = nextProps.appDetails.f_name;
         addData.middle_name = nextProps.appDetails.m_name;
         addData.last_name = nextProps.appDetails.l_name;
         addData.alias_name = nextProps.appDetails.alias_name;
         addData.alias_first_name = nextProps.appDetails.other_f_name;
         addData.alias_middle_name = nextProps.appDetails.other_m_name;
         addData.alias_last_name = nextProps.appDetails.other_l_name;
         let checkDob = moment(nextProps.appDetails.dob, 'MM/DD/YYYY').format('YYYY-MM-DD');
         if (checkDob == 'Invalid date') {
            checkDob = nextProps.appDetails.dob;
         }
         nextProps.appDetails.dob = checkDob;
         addData.dob = nextProps.appDetails.dob;
         addData.ssn = nextProps.appDetails.ssn;
         addData.freeze_override = 0;
         addData.gender = nextProps.appDetails.gender;
         addData.status = nextProps.appDetails.status;




         var emp_date = new Date();
         if (nextProps.appDetails.employment_status == 1) {
            if (nextProps.appDetails.employer_since != '00/00/0000') {
               emp_date = new Date(nextProps.appDetails.employer_since);
            } else {
               emp_date = '';
            }
         }
         //var emp_date = (nextProps.appDetails.employment_status == 1) ? new Date(nextProps.appDetails.employer_since) : new Date();
         addData.employed_since = moment(emp_date).format('YYYY-MM-DD');
         addData.employed = nextProps.appDetails.employment_status;
         addData.employment_type = nextProps.appDetails.employment_type;
         addData.employer_name = nextProps.appDetails.employer_name;
         addData.employer_email = nextProps.appDetails.employer_email;
         addData.employer_phone = nextProps.appDetails.employer_phone;
         addData.annual_income = nextProps.appDetails.annual_income;
         addData.withdrawal_date = nextProps.appDetails.withdrawal_date;

         addData.username = nextProps.appDetails.username;
         addData.email = nextProps.appDetails.email;
         addData.secondary_email = nextProps.appDetails.secondary_email;
         addData.phone_1 = nextProps.appDetails.user_phone1;
         addData.phone_2 = nextProps.appDetails.user_phone2;




         addData.password = '*****';
         addData.confirm_password = '*****';
         var filterData = nextProps.appDetailsAddress.filter(x => x.same_billing_flag == 0 && x.billing_address == 1);

         if (filterData.length > 0) {
            addData.billing_address1 = filterData[0].address1;
            addData.billing_address2 = filterData[0].address2;
            addData.billing_country = filterData[0].country;
            addData.billing_state = filterData[0].state;
            addData.billing_region = filterData[0].region;
            addData.billing_county = filterData[0].county;
            addData.billing_city = filterData[0].city;
            addData.billing_zip_code = filterData[0].zip_code;
            addData.billing_how_long = filterData[0].address_time_period;
            addData.billing_phone_no = filterData[0].phone_no;
            nextProps.appDetailsAddress.splice(nextProps.appDetailsAddress.findIndex(e => e.same_billing_flag == 0 && e.billing_address == 1), 1);
         } else {
            addData.billing_address1 = "";
            addData.billing_address2 = "";
            addData.billing_country = "";
            addData.billing_state = "";
            addData.billing_region = "";
            addData.billing_county = "";
            addData.billing_city = "";
            addData.billing_zip_code = "";
            addData.billing_how_long = "";
            addData.billing_phone_no = "";
            addData.sameBilling = false;
         }

         addData.location = nextProps.appDetailsAddress;

         // set errot
         let addrError = [];
         let bankError = [];
         let getProps = this.props;
         addData.location && addData.location.map(function (item, idx) {
            addrError = addrError.concat([{
               address1: "",
               country: "",
               state: "",
               region: "",
               city: "",
               zip_code: "",
               how_long: "",
               phone_no: "",
            }]);
            addData.location[idx].how_long = item.address_time_period,
               //dispatch(addMore());
               getProps.getStates(item.country, idx);
            //dispatch(getRegion(item.state, idx));
         });

         if (nextProps.bankDetails.length > 0) {
            addData.bank = nextProps.bankDetails;
            addData.bank && addData.bank.map(function (item, idx) {
               bankError = bankError.concat([{
                  bank_name: "",
                  bank_address: "",
                  rounting_no: "",
                  bank_ac: "",
                  account_name: "",
                  account_type: "",
               }]);
               //dispatch(getRegion(item.state, idx));

            });

            add_err.bank = bankError;
         }
         add_err.location = addrError;



         /*****************************/
         if (nextProps.coDetails != '') {

            addData.co_patient_id = nextProps.CoappDetails.patient_id;
            addData.co_signer = 1;
            addData.is_co_primary = (nextProps.coDetails.primary_bill_pay_flag == 1) ? 1 : 0;
            addData.co_first_name = nextProps.CoappDetails.f_name;
            addData.co_middle_name = nextProps.CoappDetails.m_name;
            addData.co_last_name = nextProps.CoappDetails.l_name;
            checkDob = moment(nextProps.CoappDetails.dob, 'MM/DD/YYYY').format('YYYY-MM-DD');
            if (checkDob == 'Invalid date') {
               checkDob = nextProps.CoappDetails.dob;
            }
            nextProps.CoappDetails.dob = checkDob;
            addData.co_dob = nextProps.CoappDetails.dob;
            addData.co_ssn = nextProps.CoappDetails.ssn;
            addData.co_relationship = nextProps.coDetails.relationship;
            addData.co_freeze_override = 0;

            var filterDataCo = nextProps.CoappDetailsAddress.filter(x => x.same_billing_flag == 0 && x.billing_address == 1);

            if (filterDataCo.length > 0) {
               addData.co_billing_address1 = filterDataCo[0].address1;
               addData.co_billing_address2 = filterDataCo[0].address2;
               addData.co_billing_country = filterDataCo[0].country;
               addData.co_billing_state = filterDataCo[0].state;
               addData.co_billing_region = filterDataCo[0].region;
               addData.co_billing_county = filterDataCo[0].county;
               addData.co_billing_city = filterDataCo[0].city;
               addData.co_billing_zip_code = filterDataCo[0].zip_code;
               addData.co_billing_how_long = filterDataCo[0].address_time_period;
               addData.co_billing_phone_no = filterDataCo[0].phone_no;
               nextProps.CoappDetailsAddress.splice(nextProps.CoappDetailsAddress.findIndex(e => e.same_billing_flag == 0 && e.billing_address == 1), 1);
            } else {
               addData.co_billing_address1 = "";
               addData.co_billing_address2 = "";
               addData.co_billing_country = "";
               addData.co_billing_state = "";
               addData.co_billing_region = "";
               addData.co_billing_county = "";
               addData.co_billing_city = "";
               addData.co_billing_zip_code = "";
               addData.co_billing_how_long = "";
               addData.co_billing_phone_no = "";
               addData.co_sameBilling = false;
            }



            let addrErrorCo = [];
            let addCoLoc = [];
            nextProps.CoappDetailsAddress && nextProps.CoappDetailsAddress.map(function (item, idx) {

               addCoLoc = addCoLoc.concat([{
                  co_address1: item.address1,
                  co_address2: item.address2,
                  co_country: "",
                  co_state: item.state,
                  co_region: item.region_id,
                  co_county: item.county,
                  co_city: item.city,
                  co_zip_code: item.zip_code,
                  co_how_long: item.address_time_period,
                  co_phone_no: item.phone_no,
                  co_primary_address: item.primary_address,
                  co_billing_address: item.same_billing_flag,
               }]);
               addrErrorCo = addrErrorCo.concat([{
                  co_address1: "",
                  co_country: "",
                  co_state: "",
                  co_region: "",
                  co_city: "",
                  co_zip_code: "",
                  co_how_long: "",
                  co_phone_no: "",
               }]);
               //dispatch(addMore());
               getProps.getStates(item.country, idx, 1);
               //dispatch(getRegion(item.state, idx));
            });
            add_err.co_location = addrErrorCo;
            addData.co_location = addCoLoc;

            var emp_dateCo = new Date();
            if (nextProps.CoappDetails.employment_status == 1) {
               if (nextProps.CoappDetails.employer_since != '00/00/0000') {
                  emp_dateCo = new Date(nextProps.appDetails.employer_since);
               } else {
                  emp_dateCo = '';
               }
            }
            //var emp_dateCo = (nextProps.CoappDetails.employment_status == 1) ? new Date(nextProps.CoappDetails.employer_since) : new Date();

            addData.co_employed_since = moment(emp_dateCo).format('YYYY-MM-DD');
            addData.co_employed = nextProps.CoappDetails.employment_status;
            addData.co_employment_type = nextProps.CoappDetails.employment_type;
            addData.co_employer_name = nextProps.CoappDetails.employer_name;
            addData.co_employer_email = nextProps.CoappDetails.employer_email;
            addData.co_employer_phone = nextProps.CoappDetails.employer_phone;
            addData.co_annual_income = nextProps.CoappDetails.annual_income;
            addData.co_withdrawal_date = nextProps.CoappDetails.withdrawal_date;


            this.setState({ co_sameBilling: (addData.co_billing_address1 != '') ? false : true });
            this.setState({ co_startDate: new Date(nextProps.CoappDetails.dob), co_employed_since: emp_dateCo })


            add_err['co_first_name'] = "";
            add_err['co_last_name'] = "";
            add_err['co_dob'] = "";
            add_err['co_ssn'] = "";
            add_err['co_freeze_code'] = "";
            //add_err['amount'] = "Amount type can't be blank";
            add_err['co_billing_address1'] = "";
            add_err['co_billing_country'] = "";
            add_err['co_billing_state'] = "";
            add_err['co_billing_region'] = "";
            add_err['co_billing_city'] = "";
            add_err['co_billing_zip_code'] = "";
            add_err['co_billing_phone_no'] = "";

            if (nextProps.CoappDetails.employment_status == 1) {
               if (nextProps.CoappDetails.employer_since != '00/00/0000') {
                  add_err['co_employed_since'] = "";
               }
               //add_err['co_employed_since'] = "";
               add_err['co_employment_type'] = "";
               add_err['co_annual_income'] = "";
               add_err['co_employer_name'] = "";
               add_err['co_employer_phone'] = "";
               add_err['co_employer_email'] = "";
            } else {
               (nextProps.CoappDetails.employment_type != 0) ? add_err['co_employment_type'] = "" : '';
               (nextProps.CoappDetails.employed_since != '') ? add_err['co_employed_since'] = "" : '';
               (nextProps.CoappDetails.annual_income != 0) ? add_err['co_annual_income'] = "" : '';
               (nextProps.CoappDetails.employer_name != '') ? add_err['co_employer_name'] = "" : '';
               (nextProps.CoappDetails.employer_phone != '') ? add_err['co_employer_phone'] = "" : '';
               (nextProps.CoappDetails.employer_email != '') ? add_err['co_employer_email'] = "" : '';
            }


            addData.co_withdrawal_date = nextProps.CoappDetails.withdrawal_date;
            //addData.co_bank = nextProps.CobankDetails;

            let addrErrorBk = [];
            let addCoBank = [];
            if (nextProps.CobankDetails.length > 0) {
               nextProps.CobankDetails && nextProps.CobankDetails.map(function (item, idx) {
                  addCoBank = addCoBank.concat([{
                     co_bank_name: item.bank_name,
                     co_bank_address: item.bank_address,
                     co_rounting_no: item.rounting_no,
                     co_bank_ac: item.bank_ac,
                     co_account_name: item.account_name,
                     co_account_type: item.account_type,
                     co_primary_bank: (item.primary_bank == 1) ? 1 : 0,
                  }]);
                  addrErrorBk = addrErrorBk.concat([{
                     co_bank_name: '',
                     co_bank_address: '',
                     co_rounting_no: '',
                     co_bank_ac: '',
                     co_account_name: '',
                     co_account_type: '',
                  }]);
               });
               add_err.co_bank = addrErrorBk;
               addData.co_bank = addCoBank;
            }

            add_err['co_withdrawal_date'] = "";

            addData.co_username = nextProps.CoappDetails.username;
            addData.co_email = nextProps.CoappDetails.email;
            addData.co_secondary_email = nextProps.CoappDetails.secondary_email;
            addData.co_phone_1 = nextProps.CoappDetails.user_phone1;
            addData.co_phone_2 = nextProps.CoappDetails.user_phone2;
            addData.co_password = '*****';
            addData.co_confirm_password = '*****';

            add_err['co_username'] = "";
            add_err['co_password'] = "";
            add_err['co_confirm_password'] = "";
            add_err['co_email'] = "";
            add_err['co_secondary_email'] = "";
            add_err['co_phone_1'] = "";
            add_err['co_phone_2'] = "";
         }
         /*****************************/



         addData.sd_first_name = (nextProps.secDetails != '') ? nextProps.secDetails.f_name : '';
         addData.sd_middle_name = (nextProps.secDetails != '') ? nextProps.secDetails.m_name : '';
         addData.sd_last_name = (nextProps.secDetails != '') ? nextProps.secDetails.l_name : '';
         addData.sd_email = (nextProps.secDetails != '') ? nextProps.secDetails.email : '';
         addData.sd_phone = (nextProps.secDetails != '') ? nextProps.secDetails.phone : '';
         addData.sd_relationship = (nextProps.secDetails != '') ? nextProps.secDetails.relationship : '';
         addData.sd_address1 = (nextProps.secDetails != '') ? nextProps.secDetails.address1 : '';
         addData.sd_address2 = (nextProps.secDetails != '') ? nextProps.secDetails.address2 : '';
         addData.sd_city = (nextProps.secDetails != '') ? nextProps.secDetails.city : '';
         addData.sd_state = (nextProps.secDetails != '') ? nextProps.secDetails.state : '';
         addData.sd_country = (nextProps.secDetails != '') ? nextProps.secDetails.country : '';
         addData.sd_zip_code = (nextProps.secDetails != '') ? nextProps.secDetails.zip_code : '';


         this.setState({ addData: addData, existUpdate: true, employed_since: emp_date });
         this.setState({ sameBilling: (addData.billing_address1 != '') ? false : true });
         this.setState({ startDate: new Date(nextProps.appDetails.dob) })
         add_err['first_name'] = "";
         add_err['last_name'] = "";
         add_err['alias_first_name'] = "";
         add_err['alias_last_name'] = "";
         add_err['dob'] = "";
         add_err['ssn'] = "";
         add_err['freeze_code'] = "";
         //add_err['amount'] = "Amount type can't be blank";
         add_err['billing_address1'] = "";
         add_err['billing_country'] = "";
         add_err['billing_state'] = "";
         add_err['billing_region'] = "";
         add_err['billing_city'] = "";
         add_err['billing_zip_code'] = "";
         add_err['billing_phone_no'] = "";

         //add_err['sd_first_name'] = (nextProps.secDetails == '') ? "First Name can't be blank" : "";
         //add_err['sd_last_name'] = (nextProps.secDetails == '') ? "Last Name can't be blank" : "";
         //add_err['sd_email'] = (nextProps.secDetails == '') ? "Employer email address can't be blank" : "";
         //add_err['sd_phone'] = (nextProps.secDetails == '') ? "Phone number can't be blank" : "";
         //add_err['sd_relationship'] = (nextProps.secDetails == '') ? "Relationship can't be blank" : "";
         //add_err['sd_address1'] = (nextProps.secDetails == '') ? "Address1 can't be blank" : "";
         //add_err['sd_city'] = (nextProps.secDetails == '') ? "City can't be blank" : "";
         //add_err['sd_state'] = (nextProps.secDetails == '') ? "State can't be blank" : "";
         //add_err['sd_zip_code'] = (nextProps.secDetails == '') ? "Zip code can't be blank" : "";

         add_err['sd_first_name'] = "";
         add_err['sd_last_name'] = "";
         add_err['sd_email'] = "";
         add_err['sd_phone'] = "";
         add_err['sd_relationship'] = "";
         add_err['sd_address1'] = "";
         add_err['sd_city'] = "";
         add_err['sd_state'] = "";
         add_err['sd_zip_code'] = "";


         if (nextProps.appDetails.employment_status == 1) {

            if (nextProps.appDetails.employer_since != '00/00/0000') {
               add_err['employed_since'] = "";
            }
            //add_err['employed_since'] = "";
            add_err['employment_type'] = "";
            add_err['annual_income'] = "";
            add_err['employer_name'] = "";
            add_err['employer_phone'] = "";
            add_err['employer_email'] = "";
         } else {
            (nextProps.appDetails.employment_type != 0) ? add_err['employment_type'] = "" : '';
            (nextProps.appDetails.employed_since != '') ? add_err['employed_since'] = "" : '';
            (nextProps.appDetails.annual_income != 0) ? add_err['annual_income'] = "" : '';
            (nextProps.appDetails.employer_name != '') ? add_err['employer_name'] = "" : '';
            (nextProps.appDetails.employer_phone != '') ? add_err['employer_phone'] = "" : '';
            (nextProps.appDetails.employer_email != '') ? add_err['employer_email'] = "" : '';
         }


         add_err['withdrawal_date'] = "";

         add_err['username'] = "";
         add_err['password'] = "";
         add_err['confirm_password'] = "";
         add_err['email'] = "";
         add_err['secondary_email'] = "";
         add_err['phone_1'] = "";
         add_err['phone_2'] = "";
         this.setState({ add_err: add_err });

      }

   }
   getMuiThemeMain = () => createMuiTheme({
      overrides: {
         MuiFormHelperText: {
            root: {
               color: "#f44336"
            }
         },
         MuiButton: {
            containedPrimary: {
               'background-color': '#0E5D97',
               '&:hover': {
                  'background-color': '#0E5D97'
               }
            },

         },

      }
   })
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   handleClickShowSsn = () => {
      this.setState({ showSsn: !this.state.showSsn });
   };
   render() {
      const steps = getSteps();
      const { activeStep } = this.state;

      if (this.state.changeURL === 1) {
         //return (<Redirect to={`/admin/customers/customers-list`} />);
         this.props.customProps.goBack(-1)
      }

      if (this.state.cancelRedirect) {
         //return (<Redirect to={`/admin/customers/customers-list`} />);
         this.props.customProps.goBack(-1)
      }

      return (
         <div className="stepper-outer">
            <Stepper nonLinear activeStep={activeStep}>
               {steps.map((label, index) => {
                  return (
                     <Step key={label}>
                        <StepButton completed={this.state.completed[index]}>
                           {label}
                        </StepButton>
                     </Step>
                  );
               })}
            </Stepper>
            <div>
               {this.allStepsCompleted() ? (
                  <div className="pl-40">
                     <p>All steps completed - you&quot;re finished</p>
                     <Button variant="contained" className="btn-success text-white" onClick={this.handleReset} >Reset</Button>
                  </div>
               ) : (
                     <MuiThemeProvider theme={this.getMuiThemeMain()}>
                        <div className="text-right">
                           {(() => {

                              switch (activeStep) {
                                 case 0:
                                    return <StepFirstCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       DatePicker={DatePicker}
                                       startDate={this.state.startDate}
                                       existCustomer={this.state.existCustomer}
                                       onChnageExist={this.onChnageExist.bind(this)}
                                       validateSearch={this.validateSearch()}
                                       startDateExist={this.state.startDateExist}
                                       existError={this.state.existError}
                                       searchCustomerSubmit={this.searchCustomerSubmit.bind(this)}
                                       searchCustomerReset={this.searchCustomerReset.bind(this)}
                                       handleClickShowSsn={this.handleClickShowSsn.bind(this)}
                                       showSsn={this.state.showSsn}
                                       CoStateType={this.props.CoStateType}
                                       countriesList={this.props.countries}
                                       co_startDate={this.state.co_startDate}
                                       relationship={this.props.relationship}
                                    />
                                    break;
                                 case 1:
                                    return <StepSecondCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       handleShareholderNameChange={this.handleShareholderNameChange.bind(this)}
                                       handleRemoveShareholder={this.handleRemoveShareholder.bind(this)}
                                       handleAddShareholder={this.handleAddShareholder.bind(this)}
                                       shareholders={this.state.shareholders}
                                       sameBilling={this.state.sameBilling}
                                       countriesList={this.props.countries}
                                       stateType={this.props.stateType}
                                       regionType={this.props.regionType}
                                       blillingStateType={this.props.blillingStateType}
                                       blillingRegionType={this.props.blillingRegionType}
                                       handleRemoveShareholderCo={this.handleRemoveShareholderCo.bind(this)}
                                       handleAddShareholderCo={this.handleAddShareholderCo.bind(this)}
                                       stateTypeCo={this.props.stateTypeCo}
                                       co_sameBilling={this.state.co_sameBilling}
                                       CoStateType={this.props.CoStateType}
                                    />
                                    break;
                                 case 2:
                                    return <StepThirdCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       DatePicker={DatePicker}
                                       employed_since={this.state.employed_since}
                                       employmentType={this.props.employmentType}
                                       co_employed_since={this.state.co_employed_since}
                                    />
                                    break;
                                 case 3:
                                    return <StepFourthCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       bankType={this.props.bankType}
                                       DatePicker={DatePicker}
                                       withdrawal_date={this.state.withdrawal_date}
                                       handleAddShareholder2={this.handleAddShareholder2.bind(this)}
                                       handleRemoveShareholder2={this.handleRemoveShareholder2.bind(this)}
                                       handleAddShareholder2Co={this.handleAddShareholder2Co.bind(this)}
                                       handleRemoveShareholder2Co={this.handleRemoveShareholder2Co.bind(this)}
                                    />
                                    break;
                                 case 4:
                                    return <StepFifthCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       checkUsernameExist={this.checkUsernameExist.bind(this)}
                                       checkUsernameExistCo={this.checkUsernameExistCo.bind(this)}
                                    />
                                    break;
                                 /*case 6:
                                    return <StepSixCredit
                                       addErr={this.state.add_err}
                                       addData={this.state.addData}
                                       onChnagerovider={this.onChnagerovider.bind(this)}
                                       DatePicker={DatePicker}
                                       procedure_date={this.state.procedure_date}
                                       providerLocation={this.props.providerLocation}
                                       providerSpeciality={this.props.providerSpeciality}
                                    />
                                    break;*/
                                 default:
                                    return (<div>out</div>)
                              }
                           })()}
                           <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.handleCancel}>
                              Cancel
                        </Button>
                           <Button variant="contained" color="primary" className="text-white mr-10 mb-10" disabled={activeStep === 0} onClick={this.handleBack}>
                              Back
                        </Button>
                           <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.handleNext} disabled={!this.validateSubmit()} >
                              {(activeStep === 4) ? 'Update' : 'Next'}
                           </Button>
                           {this.props.loading &&
                              <RctSectionLoader />
                           }
                        </div>
                     </MuiThemeProvider>
                  )}
            </div>
         </div>
      );
   }
}


const mapStateToProps = ({ authUser, creditApplication, CareCouldReducer, CustomerDashboardReducer }) => {
   const { nameExist, isEdit, nameExistCO, isEditCO } = authUser;
   const {
      loading,
      bankType,
      countries,
      employmentType,
      stateType,
      regionType,
      blillingStateType,
      blillingRegionType,

      CoStateType,
      stateTypeCo,

      appDetails,
      appDetailsAddress,
      experianEmployment,
      experianAddress,
      secDetails,
      bankDetails,
      CoappDetails,
      CoappDetailsAddress,
      CoexperianEmployment,
      CoexperianAddress,
      CosecDetails,
      coDetails,
      CobankDetails,
      relationship,
   } = creditApplication;
   const { rediretURL } = CustomerDashboardReducer;
   const { careCouldView } = CareCouldReducer;

   return {
      loading,
      nameExist,
      isEdit,
      bankType,
      countries,
      employmentType,
      stateType,
      regionType,
      blillingStateType,
      blillingRegionType,
      rediretURL,
      stateTypeCo,
      nameExistCO,
      isEditCO,
      CoStateType,
      appDetails,
      appDetailsAddress,
      experianEmployment,
      experianAddress,
      secDetails,
      bankDetails,
      CoappDetails,
      CoappDetailsAddress,
      CoexperianEmployment,
      CoexperianAddress,
      CosecDetails,
      coDetails,
      CobankDetails,
      relationship,
   }
}

export default connect(mapStateToProps, {
   checkUsernameExist,
   applicationOption,
   getStates,
   getRegion,
   addMore,
   removeAddMore,
   editApplicationProvider,
   submitSearchCustomer,
   removeSearchCustomer,
   getSpeciality,
   viewCareCouldPatient,
   checkUsernameExistCo,
   viewApplication,
})(HorizontalNonLinearStepper);
