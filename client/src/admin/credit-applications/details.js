/**
 * Invoice
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import CryptoJS from 'crypto-js';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Link } from 'react-router-dom';
// rct card
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import AppConfig from 'Constants/AppConfig';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   cancelRefundDetails, cancelRefundDetailsDownload
} from 'Actions';
class CancelRefundPlanDetails extends Component {

   state = {
      payment_history: '',
      payment_history_data: false,
      nested_arr_row: '',
      nested_arr_row_data: false,
      plan_row: '',
      plan_row_data: false,
      paid_status: '',
      current_month_invoice_sum: 0,
      current_month_invoice: '',
      current_month_invoice_data: false,
      overdue_month_invoice_grp: '',
      overdue_month_invoice_grp_data: false,
      invoice_last_index_value: 0,
      all_plans_total_amount: 0,
      single_plan_total: 0
   }

   componentDidMount() {
      //this.props.clearRedirectURL();
      //this.props.allPlanDetails(this.props.match.params.appid);
      //this.props.viewSubmittedInvoice(this.props.match.params.invoice_id, this.props.match.params.provider_id);
      console.log('this.props.match.params.ppid')
      console.log(this.dec(this.props.match.params.ppid))
      this.props.cancelRefundDetails(this.dec(this.props.match.params.ppid))
      
   }

   dec(cipherText) {
      var SECRET = 'rmaeshCSS';
      var reb64 = CryptoJS.enc.Hex.parse(cipherText);
      var bytes = reb64.toString(CryptoJS.enc.Base64);
      var decrypt = CryptoJS.AES.decrypt(bytes, SECRET);
      var plain = decrypt.toString(CryptoJS.enc.Utf8);
      return plain;
   }

   goBack() {
      this.props.history.goBack(-1)
   }
   fullReportDownload(type) {
      //console.log(type)
      //return false;
      var ppid = {
         type: type,
         ppid: this.dec(this.props.match.params.ppid)
      }
      this.props.cancelRefundDetailsDownload(ppid);
   }
   render() {

      
      return (
         <div className="invoice-wrapper">
            <PageTitleBar title="Cancel Refund Plan Details" match={this.props.match} />
            <div className="row">

               <div className="col-sm-12 mx-auto">
                  <RctCard>
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li>
                              <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i></Link>
                           </li>
                           {this.props.customer_refund && this.props.customer_refund.length > 0 && this.props.provider_refund && this.props.provider_refund.length > 0 &&
                              <li>
                                 <a href="javascript:void(0)" onClick={() => this.fullReportDownload(1)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                              </li>
                           }

                           {this.props.sttlement_detail && this.props.sttlement_detail.length > 0 && 
                              <li>
                                 <a href="javascript:void(0)" onClick={() => this.fullReportDownload(2)} className="report-download" title="Download PDF"><i className="material-icons">picture_as_pdf</i> Download</a>
                              </li>
                           }
                        </ul>
                     </div>
                     <div className="p-10">
                        <h1 className="text-center mb-20">
                           <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                        </h1>

                        <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt submitted-invoice-vieww">

                           {this.props.customer_detail && this.props.customer_detail.length > 0 && 
                              <div className="add-card w-50 mr-5">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Customer Details</th>
                                       </tr>

                                       <tr>
                                          <td className=""><strong>Account No :</strong></td>
                                          <td>
                                             {this.props.customer_detail[0].patient_ac}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Name :</strong></td>
                                          <td>
                                             {this.props.customer_detail[0].f_name+' '+this.props.customer_detail[0].m_name+' '+this.props.customer_detail[0].l_name}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Address :</strong></td>
                                          <td>
                                             {this.props.customer_detail[0].address1+', '+this.props.customer_detail[0].City+', '+this.props.customer_detail[0].state_name+', '+this.props.customer_detail[0].zip_code}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Phone :</strong></td>
                                          <td>
                                             {this.props.customer_detail[0].peimary_phone}
                                          </td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>
                           }

                           {this.props.provider_detail && this.props.provider_detail.length > 0 && 
                              <div className="add-card w-50 ml-5">
                                 
                                    <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Provider Details</th>
                                       </tr>

                                       <tr>
                                          <td className=""><strong>Account No :</strong></td>
                                          <td>
                                             {this.props.provider_detail[0].provider_ac}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Name :</strong></td>
                                          <td>
                                             {this.props.provider_detail[0].name}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Address :</strong></td>
                                          <td>
                                             {this.props.provider_detail[0].address1+', '+this.props.provider_detail[0].city+', '+this.props.provider_detail[0].state_name+', '+this.props.provider_detail[0].zip_code}
                                          </td>
                                       </tr>
                                    
                                       <tr>
                                          <td className=""><strong>Phone :</strong></td>
                                          <td>
                                             {this.props.provider_detail[0].primary_phone}
                                          </td>
                                       </tr>

                                    </tbody>
                                 </table>
                                 
                              </div>
                           }

                        </div>

                        <div className="d-flex justify-content-between mb-10 add-full-card customer-accnt submitted-invoice-vieww">

                           {this.props.customer_refund && this.props.customer_refund.length > 0 &&
                              <div className="add-card w-50 mr-5">

                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="2">Customer Refund Details</th>
                                          </tr>

                                          {(this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Payment Method :</strong></td>
                                                <td>
                                                   {this.props.customer_refund[0].refundmethod}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.customer_refund[0].payment_method == 3 && this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Check Date :</strong></td>
                                                <td>
                                                   {this.props.customer_refund[0].check_date}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.customer_refund[0].payment_method == 3 && this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Check No :</strong></td>
                                                <td>
                                                   {this.props.customer_refund[0].check_no}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.customer_refund[0].payment_method == 1 && this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Bank Name :</strong></td>
                                                <td>
                                                   {this.props.customer_refund[0].ach_bank_name}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.customer_refund[0].payment_method == 1 && this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Account No :</strong></td>
                                                <td>
                                                   {this.props.customer_refund[0].ach_account_no}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.customer_refund[0].payment_method == 1 && this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Routing No :</strong> </td>
                                                <td>
                                                  {this.props.customer_refund[0].ach_routing_no}
                                                </td>
                                             </tr>
                                          }
                                          <tr>
                                             <td className=""><strong>Note :</strong></td>
                                             <td>
                                                {(this.props.customer_refund[0].refund_comment) ? this.props.customer_refund[0].refund_comment : '-'}
                                             </td>
                                          </tr>
                                          {(this.props.customer_refund[0].refund_amt > 0) &&
                                             <tr>
                                                <td className=""><strong>Amount :</strong> </td>
                                                <td>
                                                   {(this.props.customer_refund[0].refund_amt) ? '$' + this.props.customer_refund[0].refund_amt.toFixed(2) : '-'}
                                                </td>
                                             </tr>
                                          }

                                       </tbody>
                                    </table>

                              </div>
                           }

                           {this.props.provider_refund && this.props.provider_refund.length > 0 &&
                              <div className="add-card w-50 ml-5">
                                 
                                    <table>
                                       <tbody>
                                          <tr>
                                             <th colSpan="2">Refund From Provider</th>
                                          </tr>

                                          <tr>
                                             <td className=""><strong>Refund Type :</strong></td>
                                             
                                             <td>
                                                {(this.props.provider_refund[0].iou_flag == 1) ? "IOU" : "Issue Refund"}
                                             </td>
                                          </tr>
                                          {(this.props.provider_refund[0].payment_method == 3) &&
                                             <tr>
                                                <td><strong>Data Check No : </strong></td>
                                                <td>
                                                   {this.props.provider_refund[0].check_no}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.provider_refund[0].payment_method == 3) &&
                                             <tr>
                                                <td><strong>Check Date : </strong></td>
                                                <td>
                                                   {this.props.provider_refund[0].check_date}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.provider_refund[0].payment_method == 1) &&
                                             <tr>
                                                <td><strong>Bank Name : </strong></td>
                                                <td>
                                                   {this.props.provider_refund[0].ach_bank_name}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.provider_refund[0].payment_method == 1) &&
                                             <tr>
                                                <td><strong>Routing No. : </strong></td>
                                                <td>
                                                   {this.props.provider_refund[0].ach_routing_no}
                                                </td>
                                             </tr>
                                          }
                                          {(this.props.provider_refund[0].payment_method == 1) &&
                                             <tr>
                                                <td><strong>Account No : </strong></td>
                                                <td colSpan="3">
                                                   {this.props.provider_refund[0].ach_account_no}
                                                </td>
                                             </tr>
                                          }
                                       <tr>
                                          <td className=""><strong>Amount :</strong></td>
                                          <td>
                                             {(this.props.provider_refund[0].refund_due) ? '$' + this.props.provider_refund[0].refund_due.toFixed(2) : '-'}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className=""><strong>Note :</strong></td>
                                          <td>
                                             {(this.props.provider_refund[0].refund_comment) ? this.props.provider_refund[0].refund_comment : '-'}
                                          </td>
                                       </tr>
                                       

                                       </tbody>
                                    </table>
                                 
                              </div>
                           }

                           {this.props.sttlement_detail && this.props.sttlement_detail.length > 0 && this.props.sttlement_detail[0].plan_status == 7 &&
                                 
                                    <div className="add-card w-100">
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <th colSpan="2">Settlement Detils</th>
                                                </tr>

                                                <tr>
                                                   <td className=""><strong>Principal Amount :</strong></td>
                                                   
                                                   <td>
                                                      {'$'+this.props.sttlement_detail[0].loan_amount.toFixed(2)}
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td><strong>Adjusted Amount : </strong></td>
                                                   <td>
                                                      {'$'+this.props.sttlement_detail[0].adjusted_amount.toFixed(2)}
                                                   </td>
                                                </tr>
                                             
                                                <tr>
                                                   <td><strong>Interest Rate (%) : </strong></td>
                                                   <td>
                                                      {this.props.sttlement_detail[0].discounted_interest_rate.toFixed(2)+'%'}
                                                   </td>
                                                </tr>
                                             
                                                <tr>
                                                   <td><strong>Term (month) : </strong></td>
                                                   <td>
                                                      {this.props.sttlement_detail[0].term}
                                                   </td>
                                                </tr>
                                                
                                                
                                                <tr>
                                                   <td className=""><strong>Note :</strong></td>
                                                   <td>
                                                      {this.props.sttlement_detail[0].note}
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td className=""><strong>Current Plan :</strong></td>
                                                   <td>
                                                      {this.props.sttlement_detail[0].plan_number}
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td className=""><strong>Reference Plan :</strong></td>
                                                   <td>
                                                      {this.props.sttlement_detail[0].plan_ref_no}
                                                   </td>
                                                </tr>
                                             

                                             </tbody>
                                          </table>
                                    </div>
                                 
                           }

                        </div>

                      {this.props.loading &&
                        <RctSectionLoader />
                      }

                     </div>
                  </RctCard>
               </div>
            </div>
         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication }) => {
   console.log(creditApplication)
   const { nameExist, isEdit } = authUser;
   const { loading, customer_refund, provider_refund, sttlement_detail, provider_detail, customer_detail } = creditApplication;

   return { loading, customer_refund, provider_refund, sttlement_detail, provider_detail, customer_detail }
}

export default connect(mapStateToProps, {
   cancelRefundDetails, cancelRefundDetailsDownload
})(CancelRefundPlanDetails);