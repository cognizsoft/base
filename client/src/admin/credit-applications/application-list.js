/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// add new user form
import AddNewUserForm from './expire-date/AddNewUserForm';

// update user form
import UpdateUserForm from './expire-date/UpdateUserForm';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import CryptoJS from 'crypto-js';
import {
   creditApplicationList, downloadAgrement, updateAppExpireDate, creditApplicationListDownload, creditApplicationListDownloadXLS
} from 'Actions';

class CreditApplicationList extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0,
      startDate: '',
      err: {
         expiry_date: ''
      },
      app_status_id: '',

      //for filter
      reportFilter: {
         customer_name: '',
         created_start_date: '',
         created_end_date: '',
         status: '',
         filter_type: '',
         year: '',
         month: '',
         quarter: '',
      },
      created_start_date: '',
      created_end_date: '',
      widthTable: '',
   }
   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateUserDetails(name, value)
      });
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 06,2019
   */
   componentDidMount() {
      let { reportFilter } = this.state
      this.permissionFilter(this.state.currentModule);
      var app_status_id;
      (this.props.match.params.status_id) ? app_status_id = this.props.match.params.status_id : '';
      reportFilter['status'] = app_status_id
      this.setState({ app_status_id: app_status_id })
      //console.log('this.props.match.params')
      //console.log(this.props.match.params)
      this.props.creditApplicationList(this.state.reportFilter);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
  permissionFilter = (name) => {
   let per = JSON.parse(this.props.user);

   let newUser = per.user_permission.filter(
      function (per) { return per.description == name }
   );

   this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   /**
    * Open Add New User Modal
    */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }


   /**
    * View User Detail Hanlder
    */
   viewUserDetail(data) {
      //console.log(this.state.openViewUserDialog);
      this.setState({ openViewUserDialog: true, selectedUser: data });
      // console.log(this.state.openViewUserDialog);
   }

   /**
    * On Edit User
    */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

   /**
    * On Add & Update User Modal Close
    */
   onAddUpdateUserModalClose() {
      // console.log(this.state.addNewUserModal);
      this.setState({ addNewUserModal: false, editUser: null })
      //console.log(this.state.addNewUserModal);
   }

   /**
   * On View User Modal Close
   */
   onViewUserModalClose = () => {
      //console.log(this.state.addViewUserModal);
      this.setState({ openViewUserDialog: false, selectedUser: null })
      //console.log(this.state.addViewUserModal);
   }
   /**
    * On Edit Master Value
    */
   onEditUser(editUser) {
      //console.log(editRfactor)
      this.setState({ addNewUserModal: true, editUser: editUser, startDate: new Date(editUser.expiry_date) });
   }
   /**
    * On Update Master Value Details
    */
   onUpdateUserDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {

         case 'expiry_date':
            if (value == null) {
               err[fieldName] = "Select Expiry Date";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               err[fieldName] = '';
            }
            break;
         default:
            break;

      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editUser: {
            ...this.state.editUser,
            [fieldName]: value
         }
      });
   }
   /**
    * Update User
    */
   updateUser() {
      const { editUser } = this.state;

      let indexOfUpdateUser = '';
      let User = this.props.applicationList;
      //console.log(InterestRate)
      for (let i = 0; i < User.length; i++) {
         const user = User[i];

         if (user.application_id === editUser.application_id) {
            indexOfUpdateUser = i
         }
      }
      //console.log(editRiskFactor)
      this.props.updateAppExpireDate(editUser);

      let self = this;
      setTimeout(() => {
         console.log(editUser.expiry_date)
         editUser.expiry_date = moment(editUser.expiry_date).format('MM/DD/YYYY');
         User[indexOfUpdateUser] = editUser;
         this.setState({ loading: true, editUser: null, addNewUserModal: false });
         self.setState({ User, loading: false });
         NotificationManager.success('Date Updated!');
      }, 2000);
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            root: { display: "none" }
         },
      }
   })

   downloadAgrement(app_id, e) {
      this.props.downloadAgrement(app_id);
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   fullReportDownload() {
      //console.log(this.state.reportFilter);
      this.props.creditApplicationListDownload(this.state.reportFilter);
   }
   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.creditApplicationListDownloadXLS(this.state.reportFilter);
   }
   fullReportFilter() {
      //this.props.loanReportFilter(this.state.reportFilter);
      //console.log(this.state.reportFilter)
      this.props.creditApplicationList(this.state.reportFilter);
   }

   onChangeReportFilter(key, value) {
      let { reportFilter } = this.state
      switch (key) {
         case 'filter_type': 
            reportFilter['created_start_date'] = ''
            reportFilter['created_end_date'] = ''
            reportFilter['year'] = ''
            reportFilter['month'] = ''
            reportFilter['quarter'] = ''
            reportFilter['status'] = ''
            this.setState({ created_start_date: '', created_end_date: '' })
            break;
         case 'created_start_date':
            if (value == null) {
               value = '';
               this.setState({ created_start_date: '' })
            } else {
               this.setState({ created_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'created_end_date':
            if (value == null) {
               value = '';
               this.setState({ created_end_date: '' })
            } else {
               this.setState({ created_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }

   validateButton() {

      if (this.state.reportFilter.filter_type == 1 || this.state.reportFilter.filter_type == 5 || this.state.reportFilter.filter_type == '') {
         return true
      } else if (this.state.reportFilter.filter_type == 2) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.month
         )
      } else if (this.state.reportFilter.filter_type == 3) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.quarter
         )
      } else if (this.state.reportFilter.filter_type == 4) {
         return (
            this.state.reportFilter.year
         )
      } else if (this.state.reportFilter.filter_type == 6) {
         return (
            this.state.reportFilter.created_start_date &&
            this.state.reportFilter.created_end_date
         )
      } else {
         return false
      }

   }  
   render() {
      //console.log(this.state)
      const { err, users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      const applicationList = this.props.applicationList;
      const columns = [
         { name: 'ID', field: 'application_id', },
         { name: 'A/C No', field: 'patient_ac', },
         { name: 'App No', field: 'application_no', },
         {
            name: 'Full Name',
            field: 'f_name',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  )
               },
            }
         },
         { name: 'Address', field: 'address1', },
         { name: 'City', field: 'City', },
         { name: 'State', field: 'state_name', },
         /*{ name: 'Zip', field: 'zip_code', },
         { name: 'Email', field: 'email', },*/
         { name: 'Primary Phone', field: 'peimary_phone', },
         {
            name: 'Status',
            field: 'status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if (value.status == '1' && value.plan_flag == '1') {
                     appstatus = 'Approved';
                  } else if (value.status == '1' && value.plan_flag == '0') {
                     appstatus = 'Plan Not Selected';
                  } else if (value.status == '0') {
                     appstatus = 'Unapproved';
                  } else if (value.status == '2') {
                     appstatus = 'Manual Process';
                  } else if (value.status == '3') {
                     appstatus = 'Rejected';
                  } else if (value.status == '4') {
                     appstatus = 'Need More Information';
                  } else if (value.status == '5') {
                     appstatus = 'Document Review Process';
                  }
                  return (
                     <React.Fragment>
                        <span className="d-flex justify-content-start">
                           <div className="status">
                              <span className="d-block">{value.status_name}</span>
                           </div>
                        </span>
                     </React.Fragment>
                  )
               },
               customValue: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if (value.status == '1' && value.plan_flag == '1') {
                     appstatus = 'Approved';
                  } else if (value.status == '1' && value.plan_flag == '0') {
                     appstatus = 'Plan Not Selected';
                  } else if (value.status == '0') {
                     appstatus = 'Unapproved';
                  } else if (value.status == '2') {
                     appstatus = 'Manual Process';
                  } else if (value.status == '3') {
                     appstatus = 'Rejected';
                  } else if (value.status == '4') {
                     appstatus = 'Need More Information';
                  } else if (value.status == '5') {
                     appstatus = 'Document Review Process';
                  }
                  return value.status_name;
               },
               customSortValue: (value, tableMeta, updateValue) => {
                  var appstatus = '';
                  if (value.status == '1' && value.plan_flag == '1') {
                     appstatus = 'Approved';
                  } else if (value.status == '1' && value.plan_flag == '0') {
                     appstatus = 'Plan Not Selected';
                  } else if (value.status == '0') {
                     appstatus = 'Unapproved';
                  } else if (value.status == '2') {
                     appstatus = 'Manual Process';
                  } else if (value.status == '3') {
                     appstatus = 'Rejected';
                  } else if (value.status == '4') {
                     appstatus = 'Need More Information';
                  } else if (value.status == '5') {
                     appstatus = 'Document Review Process';
                  }
                  return value.status_name;
               },
            }
         },
         {
            name: 'LOC',
            field: 'approve_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.approve_amount) ? '$' + (value.approve_amount).toFixed(2) : '$0.00'
                  )
               },
            }
         },
         {
            name: 'Over Amt',
            field: 'override_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.override_amount) ? '$' + (value.override_amount).toFixed(2) : '$0.00'
                  )
               },
            }
         },
         {
            name: 'Avl Bal',
            field: 'remaining_amount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.remaining_amount != null || value.override_amount) ? '$' + (value.remaining_amount+value.override_amount).toFixed(2) : '$0.00'
                  )
               },
            }
         },
         {
            name: 'Date Created',
            field: 'date_created',
         },
         {
            name: 'Expire Date',
            field: 'expiry_date',
         },
         {
            name: 'Action',
            field: 'md_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {

                  return (
                     <React.Fragment>
                        <span className="list-action">
                           {/*(value.status == 1 && value.plan_flag == 0 && value.score != null) ? <Link to={`/admin/customers/customerPayment-plan/${value.application_id}`}><i className="ti-clipboard"></i></Link> : ''*/}
                           {(this.state.currentPermision.view) ? <Link to={{pathname: `/admin/credit-application/edit/${value.application_id}`, state: { prevPath: 1 }}}><i className="ti-pencil"></i></Link> : ''}
                           {(this.state.currentPermision.view) ? <Link to={`/admin/credit-application/application/${value.application_id}`} title="View Application"><i className="ti-eye"></i></Link> : ''}
                           {(value.plan_exists>0)?<Link to={`/admin/credit-application/plan-details/${this.enc(value.application_id.toString())}`} title="View account details"><i className="zmdi zmdi-card"></i></Link>:''}
                           {/*<Link to={"/admin/credit-application/reject/"}><i class="zmdi zmdi-card"></i></Link>*/}
                           {(value.status == '2' && this.state.currentPermision.edit) ? <Link to={`/admin/credit-application/review/${value.application_id}`} title="View Application for Manual Process"><i className="ti-check-box"></i></Link> : ''}
                           {(value.status == '4') ? <Link to={`/admin/credit-application/upload/${value.patient_id}/${value.application_id}`} title="UPload Document for Review"><i className="ti-upload"></i></Link> : ''}
                           {(value.status == '5') ? <Link to={`/admin/credit-application/review-document/${value.application_id}`} title="Download Document for Review"><i className="ti-download"></i></Link> : ''}
                           {/*(value.document_flag == 0)?<Link to={`/provider/customers/agreement-plan/${value.application_id}`} title="Upload Agreement"><i className="ti-thumb-up"></i></Link>:''*/}
                           {/*(value.document_flag == 1)?<a href="javascript:void(0)" onClick={this.downloadAgrement.bind(this,value.application_id)} title="Download Agreement"><i className="ti-cloud-down"></i></a>:''*/}

                           {/*(value.status == '1') ? <a href="javascript:void(0)" onClick={() => this.onEditUser(value)} title="Change Expire Date"><i className="zmdi zmdi-calendar-alt"></i></a> : ''*/}
                           {(value.status == '1' || value.status == '0') ? <Link to={`/admin/credit-application/all-documents/${value.application_id}`} title="All Documents"><i className="zmdi zmdi-folder"></i></Link> : ''}
                           
                        </span>
                     </React.Fragment>
                  )
               },

            }
         },


      ];
      const options = {

         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         /*customToolbar: () => {
            return (
               <React.Fragment>
                  <Link to="/admin/credit-application/add-new-MPS" color="primary" className="caret btn-sm mr-10-custome">Create application from MPS <i className="zmdi zmdi-plus"></i></Link>
                  <Link to="/admin/credit-application/add-new" color="primary" className="caret btn-sm mr-10">Add Credit Application <i className="zmdi zmdi-plus"></i></Link>
               </React.Fragment>
            );
         },*/
         pagination: true,
         downloadOptions: { filename: 'creditApplication.csv' },
      };

      let currentYear = new Date();
      let currentMonth = currentYear.getMonth()+1;
      currentYear = currentYear.getFullYear();
      const yearList = [];
      if (this.props.yearStart != '') {
         for (var i = currentYear; i >= this.props.yearStart; i--) {
            yearList.push({ id: i, value: i });
         }
      } else {
         for (var i = currentYear; i >= (currentYear - 5); i--) {
            yearList.push({ id: i, value: i });
         }
      }
      return (
         <div className="credit-application admin-application-list">
            <Helmet>
               <title>Health Partner | Credit Application | Application List</title>
               <meta name="description" content="Application List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.creditApplications" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="filter_type">Submitted Period</Label>
                              <Input
                                 type="select"
                                 name="filter_type"
                                 id="filter_type"
                                 value={this.state.reportFilter.filter_type}
                                 onChange={(e) => this.onChangeReportFilter('filter_type', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Current Week</option>
                                 <option value="2">Monthly</option>
                                 <option value="3">Quarterly</option>
                                 <option value="4">Yearly</option>
                                 <option value="5">Year to date</option>
                                 <option value="6">By Date</option>
                              </Input>

                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2 || this.state.reportFilter.filter_type == 3 || this.state.reportFilter.filter_type == 4) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="year">Year</Label>
                              <Input
                                 type="select"
                                 name="year"
                                 id="year"
                                 value={this.state.reportFilter.year}
                                 onChange={(e) => this.onChangeReportFilter('year', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {yearList && yearList.map((data, key) => (
                                    <option value={data.id} key={key}>{data.value}</option>
                                 ))}
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="month">Month</Label>
                              <Input
                                 type="select"
                                 name="month"
                                 id="month"
                                 value={this.state.reportFilter.month}
                                 onChange={(e) => this.onChangeReportFilter('month', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1" disabled={(currentMonth < 1 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jan</option>
                                 <option value="2" disabled={(currentMonth < 2 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Feb</option>
                                 <option value="3" disabled={(currentMonth < 3 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Mar</option>
                                 <option value="4" disabled={(currentMonth < 4 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Apr</option>
                                 <option value="5" disabled={(currentMonth < 5 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>May</option>
                                 <option value="6" disabled={(currentMonth < 6 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jun</option>
                                 <option value="7" disabled={(currentMonth < 7 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jul</option>
                                 <option value="8" disabled={(currentMonth < 8 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Aug</option>
                                 <option value="9" disabled={(currentMonth < 9 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Sep</option>
                                 <option value="10" disabled={(currentMonth < 10 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Oct</option>
                                 <option value="11" disabled={(currentMonth < 11 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Nov</option>
                                 <option value="12" disabled={(currentMonth < 12 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Dec</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 3) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="quarter">Quarterly</Label>
                              <Input
                                 type="select"
                                 name="quarter"
                                 id="quarter"
                                 value={this.state.reportFilter.quarter}
                                 onChange={(e) => this.onChangeReportFilter('quarter', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Q1</option>
                                 <option value="2">Q2</option>
                                 <option value="3">Q3</option>
                                 <option value="4">Q4</option>
                              </Input>
                           </FormGroup>
                        </div>

                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="created_start_date">Date Created</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="created_start_date"
                                 id="created_start_date"
                                 selected={this.state.created_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('created_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="created_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="created_end_date"
                                 id="created_end_date"
                                 selected={this.state.created_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('created_end_date', e)}
                              />
                           </FormGroup>
                        </div>


                        {/*<div className="col-md-2">
                           <FormGroup>
                              <Label for="customer_name">Customer Name</Label>
                              <Input
                                 type="text"
                                 name="customer_name"
                                 id="customer_name"
                                 placeholder="Customer Name"
                                 onChange={(e) => this.onChangeReportFilter('customer_name', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>*/
                        }

                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="status">Status</Label>
                              <Input
                                 type="select"
                                 name="status"
                                 id="status"
                                 value={this.state.reportFilter.status}
                                 placeholder="Status"
                                 onChange={(e) => this.onChangeReportFilter('status', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {this.props.appStatus && this.props.appStatus.map((app_st, key) => (
                                    <option value={app_st.status_id} key={key}>{app_st.value}</option>
                                 ))}

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="list-action  search_down_btn">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>

                                 {this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10 btn_down">search</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                                 }
                                 {!this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down_disable">search</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf_disable">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls_disable">insert_drive_file</i></a>
                                    </span>
                                 }
                              </FormGroup>

                           </div>
                        </div>


                     </div>
                  </Form>
               </div>

               <MuiThemeProvider theme={this.getMuiTheme()}>
                  <MaterialDatatable
                     data={applicationList}
                     columns={columns}
                     options={options}
                  />
               </MuiThemeProvider>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>

                  Change Expire Date

               </ModalHeader>
               <ModalBody>

                  <UpdateUserForm
                     updateErr={err}
                     updateUserDetails={editUser}
                     onUpdateUserDetail={this.onUpdateUserDetails.bind(this)}
                     DatePicker={DatePicker}
                     startDate={this.state.startDate}
                  />

               </ModalBody>
               <ModalFooter>

                  <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateUser()}>Update</Button>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>

               </ModalFooter>
            </Modal>



         </div>
      );
   }
}

const mapStateToProps = ({ authUser, creditApplication }) => {
   const { user } = authUser;
   const { loading, applicationList, yearStart, appStatus } = creditApplication;
   return { user, loading, applicationList, yearStart, appStatus }

}

export default connect(mapStateToProps, {
   creditApplicationList, downloadAgrement, updateAppExpireDate, creditApplicationListDownload, creditApplicationListDownloadXLS
})(CreditApplicationList);