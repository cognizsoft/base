/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
const StepFirstCredit = ({ addErr, addData, onChnagerovider, DatePicker, startDate, existCustomer, onChnageExist, validateSearch, startDateExist, existError, searchCustomerSubmit, searchCustomerReset }) => (

   <div className="modal-body page-form-outer text-left">
      <div className="row">
         <div className="col-md-10">
            <FormGroup tag="fieldset">
               <Label>Do you want search with exist customer details?</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="exist_customer"
                        value={1}
                        checked={(existCustomer.exist_customer == 1) ? true : false}
                        onChange={(e) => onChnageExist('exist_customer', e.target.value)}
                     />
                     Yes
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="exist_customer"
                        value={0}
                        checked={(existCustomer.exist_customer == 0) ? true : false}
                        onChange={(e) => onChnageExist('exist_customer', e.target.value)}
                     />
                     No
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(existCustomer.exist_customer == 0) ? "col-md-2 d-none" : "col-md-2"}>
            <div>
               <Button onClick={searchCustomerSubmit} disabled={!validateSearch} variant="contained" color="primary" className="text-white mr-10 mb-10" size="small">
                  Search
            </Button>
               <Button onClick={searchCustomerReset} variant="contained" color="primary" className="text-white mr-10 mb-10" size="small">
                  Reset
            </Button>
            </div>
         </div>
      </div>
      <div className={(existCustomer.exist_customer == 0) ? "row d-none" : "row"}>
         <div className="col-md-3">
            <FormGroup>
               <Label for="exist_first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_first_name"
                  id="exist_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={existCustomer.exist_first_name}
                  error={(existError.exist_first_name) ? true : false}
                  helperText={existError.exist_first_name}
                  onChange={(e) => onChnageExist('exist_first_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-3">
            <FormGroup>
               <Label for="exist_last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_last_name"
                  id="exist_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={existCustomer.exist_last_name}
                  error={(existError.exist_last_name) ? true : false}
                  helperText={existError.exist_last_name}
                  onChange={(e) => onChnageExist('exist_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-3">
            <FormGroup>
               <Label for="exist_dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="exist_dob"
                  id="exist_dob"
                  selected={startDateExist}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  onChange={(e) => onChnageExist('exist_dob', e)}
               />
               {(existError.exist_dob) ? <FormHelperText>{existError.exist_dob}</FormHelperText> : ''}
            </FormGroup>
         </div>
         <div className="col-md-3">
            <FormGroup>
               <Label for="exist_ssn">SSN<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="exist_ssn"
                  id="exist_ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  placeholder="Social Security Number"
                  value={existCustomer.exist_ssn}
                  error={(existError.exist_ssn) ? true : false}
                  helperText={existError.exist_ssn}
                  onChange={(e) => onChnageExist('exist_ssn', e.target.value)}
               />
            </FormGroup>
         </div>

      </div>
      <div className="row">
         <div className="col-md-4">
            <FormGroup>
               <Label for="first_name">First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="first_name"
                  id="first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.first_name}
                  error={(addErr.first_name) ? true : false}
                  helperText={addErr.first_name}
                  onChange={(e) => onChnagerovider('first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="middle_name">Middle Name</Label><br />
               <TextField
                  type="text"
                  name="middle_name"
                  id="middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.middle_name}
                  onChange={(e) => onChnagerovider('middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="last_name">Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="last_name"
                  id="last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.last_name}
                  error={(addErr.last_name) ? true : false}
                  helperText={addErr.last_name}
                  onChange={(e) => onChnagerovider('last_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-12">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="alias_name"
                        value={1}
                        checked={(addData.alias_name == 1) ? true : false}
                        onChange={(e) => onChnagerovider('alias_name', e.target.value)}
                     />
                     Alias Name
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>
      <div className={(addData.alias_name == 0) ? "row d-none" : "row"}>
         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_first_name">Alias First Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_first_name"
                  id="alias_first_name"
                  fullWidth
                  variant="outlined"
                  placeholder="First Name"
                  value={addData.alias_first_name}
                  error={(addErr.alias_first_name) ? true : false}
                  helperText={addErr.alias_first_name}
                  onChange={(e) => onChnagerovider('alias_first_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_middle_name">Alias Middle Name</Label><br />
               <TextField
                  type="text"
                  name="alias_middle_name"
                  id="alias_middle_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Middle Name"
                  value={addData.alias_middle_name}
                  onChange={(e) => onChnagerovider('alias_middle_name', e.target.value)}
               />
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="alias_last_name">Alias Last Name<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="alias_last_name"
                  id="alias_last_name"
                  fullWidth
                  variant="outlined"
                  placeholder="Last Name"
                  value={addData.alias_last_name}
                  error={(addErr.alias_last_name) ? true : false}
                  helperText={addErr.alias_last_name}
                  onChange={(e) => onChnagerovider('alias_last_name', e.target.value)}
               />
            </FormGroup>
         </div>
      </div>
      <div className="row">

         <div className="col-md-4">
            <FormGroup>
               <Label for="dob">Date of Birth<span className="required-field">*</span></Label>
               <DatePicker
                  dateFormat="MM/dd/yyyy"
                  name="dob"
                  id="dob"
                  selected={startDate}
                  placeholderText="MM/DD/YYYY"
                  autocomplete={false}
                  onChange={(e) => onChnagerovider('dob', e)}
               />
               {(addErr.dob) ? <FormHelperText>{addErr.dob}</FormHelperText> : ''}
            </FormGroup>
         </div>

         <div className="col-md-4">
            <FormGroup>
               <Label for="ssn">Social Security Number<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="ssn"
                  id="ssn"
                  fullWidth
                  variant="outlined"
                  inputProps={{ maxLength: 11 }}
                  placeholder="Social Security Number"
                  value={addData.ssn}
                  error={(addErr.ssn) ? true : false}
                  helperText={addErr.ssn}
                  onChange={(e) => onChnagerovider('ssn', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup>
               <Label for="amount">Amount($)<span className="required-field">*</span></Label><br />
               <TextField
                  type="text"
                  name="amount"
                  id="amount"
                  fullWidth
                  variant="outlined"
                  placeholder="($)"
                  value={(addData.amount != '') ? addData.amount : ''}
                  error={(addErr.amount) ? true : false}
                  helperText={(addErr.amount != '') ? addErr.amount : ''}
                  onChange={(e) => onChnagerovider('amount', e.target.value)}
               />
            </FormGroup>
         </div>
         <div className="col-md-12">
            <FormGroup tag="fieldset">
               <FormGroup check>
                  <Label check>
                     <Input
                        type="checkbox"
                        name="freeze_override"
                        value={1}
                        checked={(addData.freeze_override == 1) ? true : false}
                        onChange={(e) => onChnagerovider('freeze_override', e.target.value)}
                     />
                     Is freeze?
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
         <div className={(addData.freeze_override == 0) ? "col-md-4 d-none" : "col-md-4"}>
            <FormGroup tag="fieldset">
               <FormGroup>
                  <Label for="amount">Freeze Code<span className="required-field">*</span></Label><br />
                  <TextField
                     type="text"
                     name="freeze_code"
                     id="freeze_code"
                     fullWidth
                     variant="outlined"
                     placeholder="Freeze Code"
                     value={(addData.freeze_code != '') ? addData.freeze_code : ''}
                     error={(addErr.freeze_code) ? true : false}
                     helperText={(addErr.freeze_code != '') ? addErr.freeze_code : ''}
                     onChange={(e) => onChnagerovider('freeze_code', e.target.value)}
                  />
               </FormGroup>
            </FormGroup>
         </div>
         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Gender</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'M'}
                        checked={(addData.gender == 'M') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Male
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="gender"
                        value={'F'}
                        checked={(addData.gender == 'F') ? true : false}
                        onChange={(e) => onChnagerovider('gender', e.target.value)}
                     />
                     Female
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>


         <div className="col-md-4">
            <FormGroup tag="fieldset">
               <Label>Status</Label>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addData.status == 1) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Active
                                 </Label>
               </FormGroup>
               <FormGroup check>
                  <Label check>
                     <Input
                        type="radio"
                        name="status"
                        value={0}
                        checked={(addData.status == 0) ? true : false}
                        onChange={(e) => onChnagerovider('status', e.target.value)}
                     />
                     Inactive
                                 </Label>
               </FormGroup>
            </FormGroup>
         </div>
      </div>



   </div>

);

export default StepFirstCredit;