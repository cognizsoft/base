import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import {
   Button,
   Form,
   FormGroup,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { URL } from './../../apifile/URL';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { isEmpty } from '../../validator/Validator';
import {
   creditApplicationReviewDocument, creditApplicationManualActions, creditApplicationRemoveManualActions, downloadOneDrive
} from 'Actions';

class uploadDocument extends Component {

   state = {
      all: false,
      users: null, // initial user data
      addData: {
         comment: ''
      },
      add_err: {},
      uploadDocumentRedirect: 0,
      opnDocFileModal: false,
      imgPath: '',
      imgPlanNumber: '',
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentDidMount() {
      this.props.creditApplicationReviewDocument(this.props.match.params.id);
   }
   opnDocFileModal(item_id, plan_number) {
      this.props.downloadOneDrive(item_id)
      this.setState({ opnDocFileModal: true, imgPlanNumber:plan_number})
   }
   /*
   * Title :- onChnage
   * Descrpation :- This function use check on change value
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'comment':
            if (isEmpty(value)) {
               add_err[key] = "Comment can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
      }
      this.setState({
         addData: {
            ...this.state.addData,
            [key]: value
         }
      });
      this.setState({ add_err: add_err });
   }
   /*
   * Title :- validateSubmit
   * Descrpation :- This function use for check validation
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   validateSubmit() {
      return (this.state.add_err.comment === '');
   }
   /*
   * Title :- callAction
   * Descrpation :- This function use for submit appication status
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   callAction(type, e) {
      this.props.creditApplicationManualActions(this.props.match.params.id, type, this.state.addData);

   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check updated props
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 18,2019
   */
   componentWillReceiveProps(nextProps) {
      if (nextProps.manualType && nextProps.manualType != '') {
         this.props.creditApplicationRemoveManualActions();
         this.setState({ uploadDocumentRedirect: nextProps.manualType })
      }
   }

   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }
   render() {
      if (this.state.uploadDocumentRedirect == 1) {
         return (<Redirect to={`/admin/credit-applications/all`} />);
      } else if (this.state.uploadDocumentRedirect == 2) {
         return (<Redirect to={`/admin/credit-application/reject/${this.props.match.params.id}`} />);
      }
      return (
         <div className="patients add-new-patient">
            <Helmet>
               <title>Health Partner | Patients | Add New</title>
               <meta name="description" content="Add Patient" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.patientsreviewDocCreditApplications" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>

               <div className="table-responsive">
                  <div className="modal-body page-form-outer view-section">

                     {this.props.appDocDetails &&
                        <Form className="application-review-process">
                           <div className="table-responsive mb-20 pymt-history review-app-info">
                              <h3 className="app-review-info">Credit Application Information</h3>
                              <table className="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Application No : <span>{this.props.appDocDetails.application_no}</span></th>
                                       <th>Approved Amount : <span>${this.props.appDocDetails.approve_amount}</span></th>
                                       <th>Credit Score : <span>{this.props.appDocDetails.score}</span></th>
                                    </tr>
                                 </thead>
                              </table>
                           </div>
                           <h3 className="app-review-info">Credit Application Document</h3>
                           <div className="row">
                              {this.props.docDetails && this.props.docDetails.map((doc, idx) => (
                                 <div className="col-sm-6 col-md-4 col-lg-4 col-xl-3" key={idx}>
                                    <figure className="img-wrapper border border-secondary" onClick={() => this.opnDocFileModal(doc.item_id, doc.plan_number)}>
                                       <img src={`${doc.file_path}`} width="250" height="400" />
                                       <figcaption>
                                          <h4>View and Download {doc.name}</h4>
                                       </figcaption>
                                       <a href="javascript:void(0);">&nbsp;</a>
                                    </figure>


                                 </div>

                              ))
                              }
                           </div>
                           <hr />
                           <div className="row">
                              <div className="col-md-12">
                                 <FormGroup>
                                    <Label for="comment">Your Comment <span className="required-field">*</span></Label><br />
                                    <Input
                                       type="textarea"
                                       name="comment"
                                       id="comment"
                                       variant="outlined"
                                       placeholder="Enter your comment before any action"
                                       defaultValue={this.state.addData.comment}
                                       onChange={(e) => this.onChnage('comment', e.target.value)}
                                    >
                                    </Input>
                                    {(this.state.add_err.comment) ? <FormHelperText>{this.state.add_err.comment}</FormHelperText> : ''}
                                 </FormGroup>
                              </div>
                              <div className="col-md-12">
                                 <div className="mps-submt-btn text-right">
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 1)} disabled={!this.validateSubmit()}>Approve</Button>
                                    <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.callAction.bind(this, 2)} disabled={!this.validateSubmit()}>Reject</Button>
                                 </div>
                              </div>
                           </div>
                        </Form>

                     }
                  </div>


               </div>

               <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

                  <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
                     <span className="float-left>">{(this.state.imgPlanNumber) ? 'Plan Number: ' + this.state.imgPlanNumber : ''}</span>
                     <span className="float-right"><a href={this.props.oneDrivePreview} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span>
                  </ModalHeader>

                  <ModalBody>
                     {this.props.oneDrivePreview &&
                        <embed src={this.props.oneDrivePreview} width="100%" height="500" download />

                     }
                     {this.props.loading &&
                        <div className="p-50 text-center">Loading, Please Wait...</div>
                     }

                  </ModalBody>

               </Modal>

               {this.props.loading &&
                  <RctSectionLoader />
               }


            </RctCollapsibleCard>

         </div>
      );
   }
}
const mapStateToProps = ({ creditApplication }) => {
   const { loading, appDocDetails, docDetails, manualType, oneDrivePreview } = creditApplication;
   return { loading, appDocDetails, docDetails, manualType, oneDrivePreview }

}

export default connect(mapStateToProps, {
   creditApplicationReviewDocument, creditApplicationManualActions, creditApplicationRemoveManualActions, downloadOneDrive
})(uploadDocument);