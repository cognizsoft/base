/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
  ];
const AddNewPermissionForm = ({ addNewUserDetails, onChangeAddNewUserDetails }) => (
    <Form>
        <FormGroup>
            <Label for="userName">Page Url</Label>
            <Input
                type="text"
                name="url"
                id="userName"
                placeholder="Enter Page url"
                value={addNewUserDetails.name}
                onChange={(e) => onChangeAddNewUserDetails('name', e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="userEmail">User Type</Label>
            <Input 
                type="select"
                name="user_type"
                id="user_type"
                onChange={(e) => onChangeAddNewUserDetails('user_type', e.target.value)}
                multiple
            >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </Input>
        </FormGroup>
        <FormGroup>
            <Label for="userType">User Role</Label>
            <Input
                type="select"
                name="user_role"
                id="user_role"
                onChange={(e) => onChangeAddNewUserDetails('user_role', e.target.value)}
                multiple
            >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </Input>
            <Select
                multiple
                value=''
                onChange={this.handleChange}
                input={<Input id="select-multiple-chip" />}
                renderValue={selected => (
                <div>
                    {selected.map(value => <Chip key={value} label={value} />)}
                </div>
                )} MenuProps={MenuProps}>
                {names.map(name => (
                <MenuItem key={name} value={name}>
                    {name}
                </MenuItem>
                ))}
            </Select>
        </FormGroup>
        
    </Form>
);

export default AddNewPermissionForm;
