/**
 * Users Routes
 */
/* eslint-disable */
import React from 'react';


const NotAllow = () => (
    <div className="content-wrapper">
        <div className="alert alert-danger fade show" role="alert">You are not authorized to access this page.</div>
    </div>
);

export default NotAllow;
