/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';
import {
   Form,
   FormGroup,
   Label,
   Input,
   FormText,
   Col,
   FormFeedback
} from 'reactstrap';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IntlMessages from 'Util/IntlMessages';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// add new user form
import AddNewPermissionForm from './AddNewPermissionForm';

// update user form
import UpdatePermissionForm from './UpdatePermissionForm';
import {
   getPermisionFilter,
   getPermisionModule,
   getPermisionSubmit,
   getPermisionReset,
   getUserRoleP
} from 'Actions';

class PermissionList extends Component {
   /*
   * Title :- state
   * Descrption :- this state use for manage all value in class
   * Author :- Cogniz software & ramesh Kumar 
   * Date :- 11 March 2019
   */
   state = {
      currentModule: 16,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      loading: false, // loading activity
      test: [],
      user_type: 1,
      user_role: 1,
   }
   componentDidMount = () => {
      this.permissionFilter(this.state.currentModule);
      this.props.getPermisionFilter(this.props.history);
   }
   /*
   * Title :- permissionFilter
   * Descrption :- this function use open check user permission
   * Author :- Cogniz software & ramesh Kumar 
   * Date :- 11 March 2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- opnAddNewUserModal
   * Descrption :- this function use open popup for new permsission
   * Author :- Cogniz software & ramesh Kumar 
   * Date :- 11 March 2019
   */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }
   onAddUpdateUserModalClose() {
      this.setState({ addNewUserModal: false, editUser: null })
   }

   usertypeChange = event => {
      if (event.target.value != 0) {
         this.setState({ [event.target.name]: event.target.value });
         this.setState({ user_role: '' });
         //this.props.getPermisionModule(this.props.history, event.target.value);
         this.props.getUserRoleP(event.target.value);
      }else{
         this.setState({ [event.target.name]: event.target.value });
         this.setState({ user_role: '' });
         this.props.getPermisionReset(this.props.history);
      }
   };

   userroleChange = event => {
      if (event.target.value != 0) {
         this.setState({ [event.target.name]: event.target.value });
         this.props.getPermisionModule(this.props.history, event.target.value);
      }else{
         this.setState({ [event.target.name]: event.target.value });
         this.props.getPermisionReset(this.props.history,this.state.user_type);
      }
   };
   /**
	 * On Change Add New User Details
	 */
   onChangeAddNewUserDetails(key, value) {
      this.setState({
         addNewUserDetail: {
            ...this.state.addNewUserDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- submitData()
   * Descrption :- this function return submit permission data
   * Author :- Cogniz software & ramesh Kumar 
   * Date :- 26 March 2019
   */
   submitData = (e) => {
      e.preventDefault();
      const data = {
         'role_id': this.state.user_role,
         'permission': this.props.permissiondata
      }
      this.props.getPermisionSubmit(this.props.history, data)
      return false;
   }
   updateaction = (index, e) => {
      let permissiondata = this.props.permissiondata;
      if (e.target.name == 'add') {
         permissiondata[index].create_flag = (e.target.checked) ? 1 : 0;
      } else if (e.target.name == 'edit') {
         permissiondata[index].edit_flag = (e.target.checked) ? 1 : 0;
      } else if (e.target.name == 'delete') {
         permissiondata[index].delete_flag = (e.target.checked) ? 1 : 0;
      } else if (e.target.name == 'view') {
         permissiondata[index].view_flag = (e.target.checked) ? 1 : 0;
      }

      let self = this;
      self.setState({ permissiondata });
   }
   validateSubmit() {
      return this.state.user_role != 0;
   }
   /*
   * Title :- render()
   * Descrption :- this function return final html for this module
   * Author :- Cogniz software & ramesh Kumar 
   * Date :- 11 March 2019
   */
   render() {
      const { usertypes, permissiondata, userrole } = this.props;
      //console.log(this.props.user)
      //console.log('-------')
      //console.log(this.state.currentPermision)
      return (
         <div className="user-permission">
            <Helmet>
               <title>Permission Management</title>
               <meta name="description" content="Permission Management" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.permissionManagement" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <Form onSubmit={this.submitData}>
                  <div className="table-responsive">
                     {this.state.currentPermision.edit == 1 &&
                     <div className="d-flex py-20 px-10 border-bottom">

                        <div className="col-sm-3 col-md-3 padd-0-custome">
                           <div className="form-group">
                              <FormControl fullWidth>
                                 <InputLabel htmlFor="age-simple">User Type</InputLabel>
                                 <Select value={this.state.user_type} onChange={this.usertypeChange}
                                    inputProps={{ name: 'user_type', id: 'user_type', }}>
                                    <MenuItem value="0"><em>None</em></MenuItem>
                                    {usertypes && usertypes.map((type, index) =>
                                       <MenuItem key={index} value={type.mdv_id}>{type.value}</MenuItem>
                                    )}
                                 </Select>
                              </FormControl>
                           </div>
                        </div>
                        <div className="col-sm-3 col-md-3 padd-0-custome">
                           <div className="form-group">
                              <FormControl fullWidth>
                                 <InputLabel htmlFor="age-simple">User Role</InputLabel>
                                 <Select value={this.state.user_role} onChange={this.userroleChange}
                                    inputProps={{ name: 'user_role', id: 'user_role', }}>
                                    <MenuItem value="0"><em>None</em></MenuItem>
                                    {userrole && userrole.map((type, index) =>
                                       <MenuItem key={index} value={type.mdv_id}>{type.value}</MenuItem>
                                    )}
                                 </Select>
                              </FormControl>
                           </div>
                        </div>
                        {this.state.currentPermision.edit == 1 &&
                        <div className="col-sm-3 col-md-3 padd-0-custome">
                           <FormGroup className="mb-15 padd-10-custome">
                              <Button
                                 color="primary"
                                 className="primary text-white"
                                 variant="contained"
                                 size="medium"
                                 type="submit"
                                 disabled={!this.validateSubmit()}
                              >
                                 Submit
                                 </Button>
                           </FormGroup>
                        </div>
                        }

                     </div>
                     }
                     
                     {this.state.currentPermision.view == 1 &&
                     <div className="permission-tbl">
                     <table className="table table-middle table-hover mb-0">
                        <thead>
                           <tr>
                              <th>Module</th>
                              <th>Add</th>
                              <th>Edit</th>
                              <th>Delete</th>
                              <th>View</th>
                           </tr>
                        </thead>
                        <tbody>
                           {!permissiondata &&
                              <tr>
                                 <td colSpan="5">No records found.</td>
                              </tr>
                           }
                           {permissiondata && permissiondata.map((data, key) => (
                              <tr key={key}>
                                 <td>{data.description}</td>
                                 <td>
                                 <Label className="check-container">
                                 <Input name="add" type="checkbox" readOnly checked={(data.create_flag != null) ? data.create_flag : 0} onClick={this.updateaction.bind(this, key)} value="1" />
                                 <span className="checkmark"></span>
                                 </Label>
                                 </td>
                                 <td>
                                 <Label className="check-container">
                                 <Input name="edit" type="checkbox" readOnly checked={(data.edit_flag != null) ? data.edit_flag : 0} onClick={this.updateaction.bind(this, key)} value="1" />
                                 <span className="checkmark"></span>
                                 </Label>
                                 </td>
                                 <td>
                                 <Label className="check-container">
                                 <Input name="delete" type="checkbox" readOnly checked={(data.delete_flag != null) ? data.delete_flag : 0} onClick={this.updateaction.bind(this, key)} value="1" />
                                 <span className="checkmark"></span>
                                 </Label>
                                 </td>
                                 <td>
                                 <Label className="check-container">
                                 <Input name="view" type="checkbox" readOnly checked={(data.view_flag != null) ? data.view_flag : 0} onClick={this.updateaction.bind(this, key)} value="1" />
                                 <span className="checkmark"></span>
                                 </Label>
                                 </td>
                              </tr>
                           ))}
                        </tbody>
                        
                     </table>
                     </div>
                     }
                  </div>
                  {this.state.currentPermision.edit == 1 &&
                  <FormGroup className="mb-15 padd-10-custome">
                     <Button
                        color="primary"
                        className="primary text-white"
                        variant="contained"
                        size="medium"
                        type="submit"
                        disabled={!this.validateSubmit()}
                     >
                        Submit
                        </Button>
                  </FormGroup>
                  }
               </Form>
            </RctCollapsibleCard>

         </div>
      );
   }
}

// map state to props
const mapStateToProps = ({ permissionFilter,authUser }) => {
   const { usertypes, userrole, loading, permissiondata } = permissionFilter;
   const user = authUser.user;
   console.log('authUser.user')
   console.log(userrole)
   return { usertypes, userrole, loading, permissiondata, user }
}

export default connect(mapStateToProps, {
   getPermisionFilter,
   getPermisionModule,
   getPermisionSubmit,
   getPermisionReset,
   getUserRoleP
})(PermissionList);
