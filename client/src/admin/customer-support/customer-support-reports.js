/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import { customerSupportReport, customerSupportReportFilter, customerSupportReportDownload, customerSupportReportDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class CustomerSupportReport extends Component {

   state = {

      reportFilter: {
         filter_type:'',
         start_date: '',
         end_date: '',
      },
      start_date: '',
      end_date: '',
      widthTable: '',

   }


   componentDidMount() {
      this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      this.props.customerSupportReport();

   }


   onChangeReportFilter(key, value) {
      let { reportFilter } = this.state
      switch (key) {

         case 'start_date':
            if (value == null) {
               value = '';
               this.setState({ start_date: '' })
            } else {
               this.setState({ start_date: value })
               value = moment(value).format('YYYY-MMM');
            }
            break;
         case 'end_date':
            if (value == null) {
               value = '';
               this.setState({ end_date: '' })
            } else {
               this.setState({ end_date: value })
               value = moment(value).format('YYYY-MMM');
            }
            break;
         default:
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      this.props.customerSupportReportDownload(this.state.reportFilter);
   }
   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.customerSupportReportDownloadXLS(this.state.reportFilter);
   }
   fullReportFilter() {
      this.props.customerSupportReportFilter(this.state.reportFilter);
   }
   totalReports(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var ticket_sum = items.reduce(function (accumulator, currentValue, currentindex) {
           
            if (currentindex >= start && currentindex < end) {
               if (currentValue.ticket_opened != '') {
                  accumulator['ticket_opened'] = (accumulator['ticket_opened'] !== undefined) ? accumulator['ticket_opened'] + currentValue.ticket_opened : currentValue.ticket_opened;
               }
               if (currentValue.ticket_closed != '') {
                  accumulator['ticket_closed'] = (accumulator['ticket_closed'] !== undefined) ? accumulator['ticket_closed'] + currentValue.ticket_closed : currentValue.ticket_closed;
               }
               if (currentValue.ticket_hold != '') {
                  accumulator['ticket_hold'] = (accumulator['ticket_hold'] !== undefined) ? accumulator['ticket_hold'] + currentValue.ticket_hold : currentValue.ticket_hold;
               }
               if (currentValue.ticket_pending != '') {
                  accumulator['ticket_pending'] = (accumulator['ticket_pending'] !== undefined) ? accumulator['ticket_pending'] + currentValue.ticket_pending : currentValue.ticket_pending;
               }
               

            }

            return accumulator
         }, []);
         //console.log(ticket_sum);
         return ticket_sum
      }

      //return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }


   render() {

      const support_report_list = this.props.customer_support_report_list;

      if(support_report_list) {

         var tickArr = support_report_list.reduce((acc, obj) => {
            const key1 = obj['date_created'];
            //const key2 = obj['provider_id'];

            const key = key1

            var opn = 0
            var clo = 0
            var pen = 0
            var hol = 0
            var neW = 0
            if(obj['status'] == 0) {
               ++clo
            }
            if(obj['status'] == 1 || obj['status'] == 2) {
               ++pen
            } 
            if(obj['status'] == 2) {
               ++hol
            }
            if(obj['commented_by'] !== 1) {
               ++neW
            }

            if (!acc[key]) {
               acc[key] = {
               ticket_opened:clo+pen,
               ticket_closed:clo,
               ticket_pending:pen,
               ticket_hold:hol,
               ticket_new:neW,
               provider_id:obj['customer_id'],
               //provider_name:obj['name'],
               date_created:obj['date_created'],
            };
            } else {
               acc[key].ticket_opened +=clo+pen;
               acc[key].ticket_closed +=clo;
               acc[key].ticket_pending +=pen;
               acc[key].ticket_hold +=hol;
               acc[key].ticket_new +=neW;
               provider_id:obj['customer_id'];
               //provider_name:obj['name'];
               date_created:obj['date_created'];
         }

            return acc;
         }, []);

         var ticket_array = new Array();
         for (var items in tickArr){
             ticket_array.push( tickArr[items] );
         }
         //console.log(ticket_array)

      }

      const columns = [
         {
            name: 'Month',
            field: 'date_created',
         },
         {
            name: 'New Cases',
            field: 'ticket_new',
         },
         {
            name: "Opened",
            field: "ticket_opened",
         },
         {
            name: "Closed",
            field: "ticket_closed",
         },
         {
            name: "Hold",
            field: "ticket_hold",
         },
         {
            name: "Pending",
            field: "ticket_pending",
         },

      ];
      const footerStyle = {
         display:'flex', 
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px',
         height: '20px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,
         customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            rowsSelected
         ) => {
            const reportSubtotal = this.totalReports(ticket_array, page, rowsPerPage);
            
            return (

               <TableFooter>
                  <TableRow>
                     <TableCell style={footerStyle} ><b>Total Opened : {(reportSubtotal !== undefined) ? (reportSubtotal.ticket_opened !== undefined && reportSubtotal.ticket_opened !== null)?reportSubtotal.ticket_opened:'0' : '0'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Closed : {(reportSubtotal !== undefined) ? (reportSubtotal.ticket_closed !== undefined && reportSubtotal.ticket_closed !== null)?reportSubtotal.ticket_closed : '0' : '0'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Hold : {(reportSubtotal !== undefined) ? (reportSubtotal.ticket_hold !== undefined && reportSubtotal.ticket_hold !== null)?reportSubtotal.ticket_hold : '0' : '0'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Pending : {(reportSubtotal !== undefined) ? (reportSubtotal.ticket_pending !== undefined && reportSubtotal.ticket_pending !== null)?reportSubtotal.ticket_pending : '0' : '0'}</b></TableCell>
                  </TableRow>
                  
                  <TableRow>
                     <TablePagination
                        count={count}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={(_, page) => changePage(page)}
                        onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                        rowsPerPageOptions={[1, 25, 50, 100]}
                     />
                  </TableRow>
               </TableFooter>
            );
         }
      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
         }
      });
      return (
         <div className="admin-reports-loan-report">
            <Helmet>
               <title>Health Partner | Customer Support | Customer Support Reports</title>
               <meta name="description" content="Customer Support Reports" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.customer-support-reports" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form autoComplete="off">
                     
                     <div className="row">
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="customer_name">Customer Name</Label>
                              <Input
                                 type="text"
                                 name="customer_name"
                                 id="customer_name"
                                 placeholder="Customer Name"
                                 onChange={(e) => this.onChangeReportFilter('customer_name', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="start_date">Ticket Month</Label>
                              <DatePicker
                                 dateFormat="MMM-yy"
                                 name="start_date"
                                 id="start_date"
                                 showMonthYearPicker
                                 selected={this.state.start_date}
                                 placeholderText="From month"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MMM-yy"
                                 name="end_date"
                                 id="end_date"
                                 showMonthYearPicker
                                 selected={this.state.end_date}
                                 placeholderText="To month"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('end_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="btn">&nbsp;</Label>
                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down">search</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                              </FormGroup>
                           </div>
                        </div>
                     </div>
                     
                  </Form>
               </div>
               <MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={(ticket_array) ? ticket_array : ''}
                     columns={columns}
                     options={options}


                  />
               </MuiThemeProvider>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {

   const { loading, customer_support_report_list } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, customer_support_report_list }

}

export default connect(mapStateToProps, {
customerSupportReport, customerSupportReportFilter, customerSupportReportDownload, customerSupportReportDownloadXLS
})(CustomerSupportReport);