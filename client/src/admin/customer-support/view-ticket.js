/**
 * Application Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Badge
} from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IconButton from '@material-ui/core/IconButton';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import UnlockConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty } from '../../validator/Validator';

import { NotificationManager } from 'react-notifications';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { RctCard } from 'Components/RctCard/index';
import ReactToPrint from 'react-to-print';
import { Link } from 'react-router-dom';
import { Form, Label, FormGroup, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import { userName, currentCoSignerId } from '../../apifile';
import {
  getAllTickets, createSupportTicket
} from 'Actions';
class ViewTicket extends Component {

  state = {
    logDetails: {
      caller_name: '',
      hps_agent_name: '',
      description: '',
      call_type: 'Yes',
      follow_up: 0,
      follow_up_date_time: '',

      ticket_hold: 0,
      ticket_close: 0,
      ticket_status: 1
    },
    followUpDateTimeStartDate: '',
    add_err: {

    },
    reopnTicket: false,
  }


  /*
  * Title :- componentDidMount
  * Descrpation :- This function call when component call and call another function or action
  * Author : Cognizsoft and Ramesh Kumar
  * Date :- June 5,2019
  */
  componentDidMount() {
    var data = {
      ticket_id: this.props.match.params.ticket_id,
      filter_by: 3 // for filter in node file
    }
    this.props.getAllTickets(data);
    const { logDetails, add_err } = this.state;
    logDetails.hps_agent_name = userName();
    add_err.hps_agent_name = '';
    this.setState({ logDetails: logDetails, add_err: add_err })
  }

  onChangeLogDetails(key, value) {
    let { add_err, logDetails } = this.state;
    //console.log(value)
    switch (key) {
      case 'call_type':

        break;
      case 'caller_name':
        if (isEmpty(value)) {
          add_err[key] = "Caller Name can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'hps_agent_name':
        if (isEmpty(value)) {
          add_err[key] = "HPS Agent Name can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'description':
        if (isEmpty(value)) {
          add_err[key] = "Description can't be blank";
        } else {
          add_err[key] = '';
        }
        break;
      case 'follow_up':
        value = (this.state.logDetails.follow_up) ? 0 : 1;
        if (value == 0) {
          logDetails['follow_up_date_time'] = ''
          this.setState({ followUpDateTimeStartDate: '' })
        }
        add_err['follow_up_date_time'] = 'Select date'
        break;
      case 'follow_up_date_time':
        if (value == null) {
          add_err[key] = "Select Date";
        } else {
          this.setState({ followUpDateTimeStartDate: value })
          value = moment(value).format('YYYY-MM-DD h:mm:ss');
          add_err[key] = '';
        }
        break;
      case 'ticket_hold':
        value = (this.state.logDetails.ticket_hold) ? 0 : 1;
        if (value == 0) {
          logDetails['ticket_status'] = 1
        } else {
          logDetails['ticket_close'] = ''
          logDetails['ticket_status'] = 2
        }
        break;
      case 'ticket_close':
        value = (this.state.logDetails.ticket_close) ? 0 : 1;
        if (value == 0) {
          logDetails['ticket_status'] = 1
        } else {
          logDetails['ticket_hold'] = ''
          logDetails['ticket_status'] = 0
        }
        break;
      default:
        break;
    }

    this.setState({ add_err: add_err });
    this.setState({
      logDetails: {
        ...this.state.logDetails,
        [key]: value
      }
    });
    //console.log(this.state.logDetails)
  }

  validateAddCallLogSubmit() {

    let { logDetails } = this.state
    var common = true;

    common = (
      this.state.add_err.caller_name === '' &&
      this.state.add_err.hps_agent_name === '' &&
      this.state.add_err.description === ''
    ) ? true : false


    if (logDetails.follow_up == 1) {
      var follow_up_select = (this.state.add_err.follow_up_date_time === '') ? true : false
    } else {
      var follow_up_select = true
    }

    return (common == true && follow_up_select == true) ? true : false;




    return false;
    return (
      this.state.add_err.caller_name === '' &&
      this.state.add_err.hps_agent_name === '' &&
      this.state.add_err.description === ''
    );
  }

  addCallLog() {
    this.state.logDetails.ticket_id = this.props.tickets[0].ticket_id
    this.state.logDetails.submit = 1 //for view page reducer result
    this.state.logDetails.commented_by = 1 // by admin
    //console.log(this.state.logDetails)
    //return false;
    //insertTermMonth
    this.props.createSupportTicket(this.state.logDetails);

    //this.setState({ addCallLogModal: false, loading: true });
    let self = this;
    let log = {
      caller_name: (this.props.tickets[0].name != null)?this.props.tickets[0].name:this.props.tickets[0].f_name + ' ' + this.props.tickets[0].m_name + ' ' + this.props.tickets[0].l_name,
      hps_agent_name: userName(),
      description: '',
      call_type: 'Yes',
      follow_up: 0,
      follow_up_date_time: '',
      ticket_hold: 0,
      ticket_close: 0,
      ticket_status: 1
    }
    
    setTimeout(() => {
      self.setState({ loading: false, logDetails: log, followUpDateTimeStartDate: '', add_err: {hps_agent_name:'',caller_name:''},reopnTicket: false });
      NotificationManager.success('Call log submitted successfully!');
    }, 2000);
    var data = {
      ticket_id: this.props.match.params.ticket_id,
      filter_by: 3 // for filter in node file
    }
    this.props.getAllTickets(data);

  }

  addCallLogCancel() {
    let log = {
      caller_name: '',
      hps_agent_name: '',
      description: '',
      call_type: 'Yes',
      follow_up: 0,
      follow_up_date_time: '',
      ticket_hold: 0,
      ticket_close: 0,
      ticket_status: 1
    }
    this.setState({ loading: false, logDetails: log, followUpDateTimeStartDate: '' });

  }
  componentWillReceiveProps(nextProps) {

    if (nextProps.tickets != undefined) {
      const { logDetails, add_err } = this.state;
      if (nextProps.tickets[0].name != null) {
        logDetails.caller_name = nextProps.tickets[0].name;
        add_err.caller_name = '';
      } else {
        logDetails.caller_name = nextProps.tickets[0].f_name + ' ' + nextProps.tickets[0].m_name + ' ' + nextProps.tickets[0].l_name;
        add_err.caller_name = '';
      }
      this.setState({ logDetails: logDetails, add_err: add_err })
    }
  }
  goBack() {
    this.props.history.goBack(-1)
  }
  reOpen() {
    this.setState({ reopnTicket: true })
  }
  render() {

    let { logDetails, add_err } = this.state;
    let { tickets, masterTicketSource } = this.props;
    return (
      <div className="view-support-ticket">
        <PageTitleBar title={<IntlMessages id="sidebar.viewTicket" />} match={this.props.match} />
        {tickets &&
          <div className="row">

            <div className="col-sm-12 mx-auto">
              <div className="view-ticket-container">
                <div className="invoice-head text-right">
                  <ul className="list-inline">
                    <li>
                      <Link to="#" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 material-icons">arrow_back</i> Back</Link>
                    </li>
                    <li>
                      <ReactToPrint
                        trigger={() => <a href="javascript:void(0);" ><i className="mr-10 ti-printer"></i> Print</a>}
                        content={() => this.componentRef}
                      />
                    </li>
                    {tickets[tickets.length - 1].status_id == 0 &&
                      <li>
                        <Link to="#" onClick={this.reOpen.bind(this)} title="Back"><i className="mr-10 ti-reload"></i> Reopen</Link>

                      </li>
                    }
                  </ul>
                </div>
                <div className="customer-invoice pt-5 pb-5 px-50" ref={el => (this.componentRef = el)}>
                  <div className="ticket-head-container d-flex justify-content-between">
                    <div className="ticket-head mt-30 w-100 border-bottom row">
                      <div className="col-sm-4 text-left">
                        <h2>[Ticket #{tickets[0].ticket_id}] {tickets[0].subject} <span className={(tickets[tickets.length - 1].status_id == 1) ? "ticket-status ticket-open" : (tickets[tickets.length - 1].status_id == 2) ? "ticket-status ticket-hold" : "ticket-status ticket-close"}>{tickets[tickets.length - 1].status}</span></h2>
                        <span><strong className="mr-5">Account Number:</strong> {(tickets[0].provider_ac != null) ? tickets[0].provider_ac : tickets[0].patient_ac}</span>
                        <span><strong className="mr-5">Customer Name:</strong> {(tickets[0].name != null) ? tickets[0].name : tickets[0].f_name + ' ' + tickets[0].m_name + ' ' + tickets[0].l_name}</span>
                      </div>

                      <div className="col-sm-4 text-left">
                        <span><strong className="mr-5">Ticket related to:</strong>
                          {(tickets[0].ac_ticket_flag == 1) ? 'A/C, ' : ''}
                          {(tickets[0].plan_ticket_flag == 1) ? 'Plan, ' : ''}
                          {(tickets[0].app_ticket_flag == 1) ? 'Application, ' : ''}
                          {(tickets[0].invoice_ticket_flag == 1) ? 'Invocie' : ''}
                        </span>

                        {tickets[0].plan_ticket_flag == 1 &&
                          <span><strong className="mr-5">Plan Number:</strong>  {tickets[0].plan_number} <br /></span>
                        }

                        {tickets[0].app_ticket_flag == 1 &&
                          <span><strong className="mr-5">Application Number:</strong>  {tickets[0].application_no} <br /></span>
                        }

                        {tickets[0].invoice_ticket_flag == 1 &&
                          <span><strong className="mr-5">Invoice Number:</strong>  {(tickets[0].provider_id) ? tickets[0].provider_invoice_number : tickets[0].invoice_number} </span>
                        }
                      </div>

                      <div className="col-sm-4 text-right">
                        <span><strong className="mr-5">Created By:</strong> {tickets[0].hps_agent_name}</span>
                      </div>

                    </div>

                  </div>

                  {!tickets[0].date_created && <div className="comments ticket-comment-section mt-20 mb-40">
                    <div className="comment">
                      <span className="comment-date">{tickets[0].date_created}</span>
                      <span className="comment-description">{tickets[0].description}</span>
                    </div>
                  </div>
                  }

                  <div className="all-comment-section mt-20 mb-20">
                    <table className="table table-sm">
                      <thead>
                        <tr>
                          <th>CS Tkt Dtl ID</th>
                          <th>Call Date</th>
                          <th>Inc Call Flag</th>
                          <th>HPS Agent</th>
                          <th>Caller Name</th>                          
                          <th>Description</th>
                          <th>Follow Up</th>
                          <th>Follow Up Sch</th>
                        </tr>
                      </thead>
                      <tbody>
                        {tickets && tickets.map((tkt, key) => {
                          return (
                            <tr key={key} className={(tkt.commented_by == 1) ? 'admin_comment comment' : 'customer_comment comment'}>
                              <td>{key + 1}</td>
                              <td>{tkt.call_date_time}</td>
                              <td>{(tkt.incoming_call_flag == "Yes") ? "Yes" : "No"}</td>
                              <td>{tkt.hps_agent_name}</td>
                              <td>{tkt.caller_name}</td>                              
                              <td>{tkt.description}</td>
                              <td>{(tkt.follow_up_flag == 1) ? 'Yes' : 'No'}</td>
                              <td>{(tkt.follow_up_flag == 1) ? tkt.follow_up_sch : '-'}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>


                  <div className={(tickets[tickets.length - 1].status_id == 0 && this.state.reopnTicket == false) ? "mb-20 justify-content-between add-full-card customer-detail-info d-none" : "mb-20 d-flex justify-content-between add-full-card customer-detail-info"}>

                    <div className="w-100">
                      <div className="row">

                        <div className="col-md-12">
                          <FormGroup tag="fieldset" className="m-0">
                            <Label className="d-inline w-auto label-auto float-left mb-0">Incoming Call Flag?: </Label>
                            <FormGroup check className="mr-10 ml-10">
                              <Label check>
                                <Input
                                  type="radio"
                                  name="call_type"
                                  value="Yes"
                                  checked={(logDetails.call_type == "Yes") ? true : false}
                                  onChange={(e) => this.onChangeLogDetails('call_type', e.target.value)}
                                />{' '}
                                                    Yes
                                                </Label>
                            </FormGroup>
                            <FormGroup check className="mr-10 ml-10">
                              <Label check>
                                <Input
                                  type="radio"
                                  name="call_type"
                                  value="No"
                                  checked={(logDetails.call_type == "No") ? true : false}
                                  onChange={(e) => this.onChangeLogDetails('call_type', e.target.value)}
                                />{' '}
                                                    No
                                                </Label>
                            </FormGroup>
                          </FormGroup>
                        </div>


                      </div>

                      <div className="row">


                        <div className="col-md-3">
                          <FormGroup className="m-0">
                            <Label for="caller_name" className="mb-0">Caller Name:</Label>
                            <Input
                              type="text"
                              name="caller_name"
                              id="caller_name"
                              className=""
                              placeholder="Caller Name"
                              value={logDetails.caller_name}
                              onChange={(e) => this.onChangeLogDetails('caller_name', e.target.value)}
                            />
                            {(add_err.caller_name) ? <FormHelperText>{add_err.caller_name}</FormHelperText> : ''}
                          </FormGroup>
                        </div>

                        <div className="col-md-3">
                          <FormGroup className="m-0">
                            <Label for="hps_agent_name" className="mb-0">HPS Agent Name:</Label>
                            <Input
                              type="text"
                              name="hps_agent_name"
                              id="hps_agent_name"
                              className=""
                              placeholder="HPS Agent Name"
                              value={logDetails.hps_agent_name}
                              onChange={(e) => this.onChangeLogDetails('hps_agent_name', e.target.value)}
                            />
                            {(add_err.hps_agent_name) ? <FormHelperText>{add_err.hps_agent_name}</FormHelperText> : ''}
                          </FormGroup>
                        </div>

                        <div className="col-md-2">
                          <FormGroup tag="fieldset" className="follow-up-needed m-0 d-flex">
                            <FormGroup check className="w-100">
                              <Label for="caller_name" className="mb-0"></Label>
                              <Label check className="mt-5">
                                <Input
                                  type="checkbox"
                                  id="follow-up-needed"
                                  checked={(logDetails.follow_up == 1) ? true : false}
                                  value={(logDetails.follow_up != '') ? logDetails.follow_up : ''}
                                  name="follow_up"
                                  onChange={(e) => this.onChangeLogDetails('follow_up', e.target.value)}
                                />{' '}
                                                 Follow up needed?
                                                </Label>
                              {(add_err.follow_up != '') ?
                                <FormHelperText>{add_err.follow_up}</FormHelperText>
                                : ''}
                            </FormGroup>
                          </FormGroup>
                        </div>

                        {logDetails.follow_up == 1 && <div className="col-md-4">
                          <FormGroup className="m-0">
                            <Label for="follow_up_date_time" className="mb-0">Follow up Date & Time: </Label>
                            <DatePicker
                              name="follow_up_date_time"
                              id="follow_up_date_time"
                              className="form-control"
                              minDate={new Date()}
                              selected={this.state.followUpDateTimeStartDate}
                              placeholderText="DD/MM/YYYY HH:MM:SS"
                              autocomplete={false}
                              showTimeSelect
                              timeFormat="HH:mm"
                              timeIntervals={15}
                              timeCaption="Time"
                              dateFormat="dd/MM/yyyy h:mm:ss"
                              onChange={(e) => this.onChangeLogDetails('follow_up_date_time', e)}
                            />

                            {(add_err.follow_up_date_time) ? <FormHelperText>{add_err.follow_up_date_time}</FormHelperText> : ''}
                          </FormGroup>
                        </div>
                        }

                      </div>

                      <div className="row">
                        <div className="col-md-12">
                          <FormGroup className="mb-0">
                            <Label for="description" className="mb-0">Descritpion</Label>
                            <Input
                              type="textarea"
                              name="description"
                              id="description"
                              placeholder="Enter Desccription"
                              value={logDetails.description}
                              onChange={(e) => this.onChangeLogDetails('description', e.target.value)}
                            />
                          </FormGroup>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-1">

                          <FormGroup check className="w-100">
                            <Label for="ticket_hold" className="mb-0"></Label>
                            <Label check className="mt-5">
                              <Input
                                type="checkbox"
                                id="ticket_hold"
                                checked={(logDetails.ticket_hold == 1) ? true : false}
                                value={(logDetails.ticket_hold != '') ? logDetails.ticket_hold : ''}
                                name="ticket_hold"
                                onChange={(e) => this.onChangeLogDetails('ticket_hold', e.target.value)}
                              />{' '}
                                                 Hold
                                                </Label>
                          </FormGroup>

                        </div>
                        <div className="col-md-2">

                          <FormGroup check className="w-100">
                            <Label for="ticket_close" className="mb-0"></Label>
                            <Label check className="mt-5">
                              <Input
                                type="checkbox"
                                id="ticket_close"
                                checked={(logDetails.ticket_close == 1) ? true : false}
                                value={(logDetails.ticket_close != '') ? logDetails.ticket_close : ''}
                                name="ticket_close"
                                onChange={(e) => this.onChangeLogDetails('ticket_close', e.target.value)}
                              />{' '}
                                                 Close
                                                </Label>
                          </FormGroup>

                        </div>
                      </div>

                      <div className="ticket-action-button float-right mt-20">

                        <Button
                          variant="contained"
                          color="primary"
                          className="text-white cancel-btn mr-10"
                          onClick={() => this.addCallLogCancel()}
                        >
                          Cancel
                                  </Button>

                        <Button
                          variant="contained"
                          color="primary"
                          className="text-white submit-btn"
                          onClick={() => this.addCallLog()}
                          disabled={!this.validateAddCallLogSubmit()}
                        >
                          Submit
                                  </Button>

                      </div>

                    </div>

                  </div>


                </div>
              </div>
            </div>

          </div>
        }
        {
          this.props.loading &&
          <RctSectionLoader />
        }
      </div >
    );
  }
}
const mapStateToProps = ({ CustomerSupportReducer }) => {

  const { loading, tickets, masterTicketSource } = CustomerSupportReducer;
  return { loading, tickets, masterTicketSource }
}
export default connect(mapStateToProps, {
  getAllTickets, createSupportTicket
})(ViewTicket);