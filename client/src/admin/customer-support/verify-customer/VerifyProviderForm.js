/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const VerifyProviderForm = ({ addErr, questionList, onChangeVerifyUser, check_fullname, check_phone, check_ssn, check_question, check_answer, verifyUserDetail, selectedQuestion }) => (
    <Form>
        <div className="row">
            <div className="col-md-12">
               <FormGroup tag="fieldset" className="check_fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_fullname != '') ? check_fullname : ''}
                     name="check_fullname" 
                     onChange={(e) => onChangeVerifyUser('check_fullname', e.target.value)}
                    />{' '} <span className="checkLabel">Provider Name</span> <span className="checkName">{(verifyUserDetail) ? verifyUserDetail[0].name : ''}</span>
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
               <FormGroup tag="fieldset" className="check_fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_phone != '') ? check_phone : ''}
                     name="check_phone" 
                     onChange={(e) => onChangeVerifyUser('check_phone', e.target.value)}
                    />{' '} <span className="checkLabel">Phone</span> <span className="checkDob">{(verifyUserDetail) ? verifyUserDetail[0].primary_phone : 'N/A'}</span>
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
                
             <div className="col-md-12">
               <FormGroup tag="fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_question != '') ? check_question : ''}
                     name="check_question" 
                     onChange={(e) => onChangeVerifyUser('check_question', e.target.value)}
                    />{' '} Security Questions
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
                <FormGroup>
                    
                    <Input
                        type="select"
                        name="questions"
                        id="questions"
                        value={(selectedQuestion.questions) ? selectedQuestion.questions : ''}
                        onChange={(e) => onChangeVerifyUser('questions', e.target.value)}

                    >

                        <option value="">Select</option>
                        {questionList && questionList.map((que, key) => (
                            <option value={que.security_questions_id} key={key}>{que.name}</option>
                        ))}
                    </Input>
                    {(addErr.questions) ? <FormHelperText>{addErr.questions}</FormHelperText> : ''}
                </FormGroup>
            </div>

            {check_answer == 1 && <div className="col-md-12">
               <FormGroup tag="fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_answer != '') ? check_answer : ''}
                     name="check_answer" 
                     onChange={(e) => onChangeVerifyUser('check_answer', e.target.value)}
                    />{' '} Answer
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            }
            <div className="col-md-12">
                <FormGroup>
                    
                    <TextField
                        type="text"
                        name="answer"
                        id="answer"
                        fullWidth
                        inputProps={{
                            readOnly: true
                        }}
                        value={(selectedQuestion.answer) ? selectedQuestion.answer : ''}
                        variant="outlined"
                        placeholder="Answer"
                        error={(addErr.answer) ? true : false}
                        helperText={addErr.answer}
                        onChange={(e) => onChangeVerifyUser('answer', e.target.value)}
                    />
                </FormGroup>
            </div>      
        </div>
    </Form>
);

export default VerifyProviderForm;
