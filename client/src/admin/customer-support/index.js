/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import CryptoJS from 'crypto-js';
import {
   customerAccountFilter, getLockUserDetail, getQuestions, getAllTickets
} from 'Actions';
import { Link } from 'react-router-dom';
//import AddNewButton from './AddNewButton';
import { ageVliad, isEmpty, isEmail, isContainWhiteSpace, isPassword, formatPhoneNumber, isPhone, formatSSNNumber, isSSN, isAlphaDigit, isDecimals, isNumeric, isLength, pointDecimals, isNumericDecimal } from '../../validator/Validator';
import VerifyCustomerForm from './verify-customer/VerifyCustomerForm';
import VerifyProviderForm from './verify-customer/VerifyProviderForm';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { Redirect } from 'react-router-dom';
import AddCallLogButton from './support-history/AddCallLogButton';
import AddCallLogForm from './support-history/AddCallLogForm';

class CustomerSupport extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      invoiceList: null, // initial user data
      selectedUser: null, // selected user to perform operations
      allSelected: false,
      selectedInvoices: 0,
      selectedInvoiceArr: null,
      accountFilter: {
         sort_by: 2,
         dob: '',
         phone: '',
         ssn: '',
         first_name: '',
         last_name: '',
         provider_name: '',
      },
      hideSearchSection: false,
      providerDetail: null,
      hideField: true,
      invoice_start_date: '',
      invoice_end_date: '',
      invoice_due_start_date: '',
      invoice_due_end_date: '',
      widthTable: '',

      add_err: {

      },

      ////Verify USER/////
      verifyModal: false,
      selectedVerifyUserId: '',
      selectedVerifyUserQuestions: null,
      hover_user: '',
      hover: false,

      selected_question: '',
      selected_question_answer: '',
      selectedAppId: '',
      selectedAppViewId: '',
      selectedQuestion: {
         questions: '',
         answer: ''
      },
      unlockedUserId: '',

      check_fullname: 0,
      check_dob: 0,
      check_ssn: 0,
      check_question: 0,
      check_answer: 0,
      check_phone: 0,

      verify_err: {
         questions: '',
         check_fullname: '',
         check_dob: '',
         check_ssn: '',
         check_phone: '',
         check_question: '',
         check_answer: ''
      },
   }

   componentWillReceiveProps(nextProps) {
      if(nextProps.customerQuestionsList) {
         this.setState( {
            selectedVerifyUserQuestions: nextProps.customerQuestionsList
         })
      }
   }

   componentDidMount() {
      var data = {
         ticketType:0,
         filter_by: 0, // for filter in node file
         status:this.props.match.params.statusType,
      }
      this.props.getAllTickets(data);
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   onChangeReportFilter(key, value) {
      let { add_err, accountFilter } = this.state
      switch (key) {
         case 'dob':
            if (value == null) {
               value = '';
               this.setState({ dob: '' })
            } else {
               this.setState({ dob: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'sort_by':
            /*var vl = {
               dob: '',
               phone: '',
               ssn: '',
               first_name: '',
               last_name: '',
               provider_name: '',
            }*/
            accountFilter['dob'] = ''
            accountFilter['phone'] = ''
            accountFilter['ssn'] = ''
            accountFilter['first_name'] = ''
            accountFilter['last_name'] = ''
            accountFilter['provider_name'] = ''
            if(value == 1) {
               this.setState({ hideField: false })
            } else {
               this.setState({ hideField: true })
            }
            //this.setState({ accountFilter: vl })
            console.log(this.state)
            break;
         case 'ssn':
         //console.log('eferer')
            if (isEmpty(value)) {
               add_err[key] = "";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               add_err[key] = "Social security number not valid";
            } else {
               value = formatSSNNumber(value)
               add_err[key] = '';
            }
            break;
         case 'phone':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = "Phone number not valid";
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            break;
         case 'f_name':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else {
               add_err[key] = '';
            }
            break;
         case 'l_name':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else {
               add_err[key] = '';
            }
            break;
         case 'provider_name':
            if (isEmpty(value)) {
               add_err[key] = "";
            } else {
               add_err[key] = '';
            }
            break;

      }
      this.setState({
         accountFilter: {
            ...this.state.accountFilter,
            [key]: value
         }
      });
   }

   fullReportFilter() {
      console.log(this.state.accountFilter)
      //return false
     this.props.customerAccountFilter(this.state.accountFilter);
   }

   goBack() {
      this.props.history.goBack(-1)
   }
  
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   ///////Verify USER////////

   onVerifyUser(user_id) {

      if(this.state.accountFilter.sort_by == 2) {
         this.props.getQuestions(user_id)
         this.props.getLockUserDetail(user_id)

         //const selectedVerifyUserQuestions = this.props.customerQuestionsList
         var selectedUser = this.props.customerAccountList.filter(x => x.user_id == user_id);
         var selectedUserView = this.props.customerAccountList.filter(x => x.user_id == user_id && x.plan_exists == 0);
         //console.log(selectedUser)
         this.setState({ verifyModal: true, selectedVerifyUserId: user_id, selectedAppId: selectedUser[0].application_id });

         if(selectedUserView && selectedUserView.length > 0) {
            this.setState({ selectedAppViewId: selectedUserView[0].application_id });
         } else {
            this.setState({ selectedAppViewId: '' });
         }

      } else {
         this.props.getQuestions(user_id)
         var selectedProvider = this.props.providerAccountList.filter(x => x.user_id == user_id);

         this.setState({ verifyModal: true, selectedVerifyUserId: user_id, providerDetail: selectedProvider });
      }

   }

   verifyModalClose() {
      var vErr = {
         questions: '',
         check_fullname: '',
         check_dob: '',
         check_ssn: '',
         check_phone: '',
         check_question: '',
         check_answer: ''
      }
      //console.log(this.state.selectedUnlockUserId)
      this.setState({ verifyModal: false, selectedVerifyUserId: '', selectedVerifyUserQuestions: null, verify_err: vErr, check_fullname: 0, check_dob: 0, check_ssn: 0, check_question: 0, check_answer: 0, check_phone: 0,})
   }

   onChangeVerifyUser(fieldName, value) {

      let { verify_err, selectedQuestion } = this.state;
      switch (fieldName) {
         case 'questions':
            //console.log(value)
            if (value == '') {
               this.setState({ selected_question: "" })
               verify_err[fieldName] = "Select question";
            } else {

               var que = this.state.selectedVerifyUserQuestions.filter(x => x.security_questions_id == value)
               selectedQuestion.answer = que[0].answers
               this.setState({ selected_question: value, selectedQuestion: selectedQuestion })
               verify_err[fieldName] = "";

            }
            break;
         case 'answer':
            if (value == '') {
               this.setState({ selected_question_answer: '' })
               verify_err[fieldName] = "Answer can't be blank";
            } else {
               this.setState({ selected_question_answer: value })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_fullname':
            //console.log(value)
            let vl1 = (this.state.check_fullname) ? 0 : 1;
            //console.log(vl1)
            if (this.state.check_fullname == 0) {
               this.setState({ check_fullname: vl1 })
               verify_err[fieldName] = "check fullname";
            } else {
               this.setState({ check_fullname: vl1 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_dob':
            //console.log(value)
            let vl2 = (this.state.check_dob) ? 0 : 1;
            ///console.log(vl2)
            if (this.state.check_dob == 0) {
               this.setState({ check_dob: vl2 })
               verify_err[fieldName] = "check dob";
            } else {
               this.setState({ check_dob: vl2 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_ssn':
            let vl3 = (this.state.check_ssn) ? 0 : 1;
            if (this.state.check_ssn == 0) {
               this.setState({ check_ssn: vl3 })
               verify_err[fieldName] = "check ssn";
            } else {
               this.setState({ check_ssn: vl3 })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_phone':
            let vl3a = (this.state.check_phone) ? 0 : 1;
            if (this.state.check_phone == 0) {
               this.setState({ check_phone: vl3a })
               verify_err[fieldName] = "check phone";
            } else {
               this.setState({ check_phone: vl3a })
               verify_err[fieldName] = "";
            }
            break;
         case 'check_question':
            let vl4 = (this.state.check_question) ? 0 : 1;
            if (this.state.check_question == 0) {
               //selectedQuestion.questions = ''
               this.setState({ check_question: vl4, /*selectedQuestion: selectedQuestion*/ })
               verify_err[fieldName] = "check question";
               //verify_err['questions'] = "Select Question";
            } else {
               this.setState({ check_question: vl4 })
               verify_err[fieldName] = "";
            }

            //console.log(this.state)
            break;
         case 'check_answer':
            let vl5 = (this.state.check_answer) ? 0 : 1;
            if (this.state.check_answer == 0) {
               this.setState({ check_answer: vl5 })
               verify_err[fieldName] = "check answer";
            } else {
               this.setState({ check_answer: vl5 })
               verify_err[fieldName] = "";
            }
            break;
         default:
            break;

      }

      this.setState({ verify_err: verify_err });
      //console.log(this.state.err)

      this.setState({
         selectedQuestion: {
            ...this.state.selectedQuestion,
            [fieldName]: value
         }
      });

   }

   verifyUserAccount() {
      const { selectedVerifyUserId, selectedQuestion, selectedAppId, selectedAppViewId, providerDetail } = this.state;

      this.setState({ loading: true })
      let self = this;

      if(this.state.accountFilter.sort_by == 2) {
         setTimeout(() => {
            let selectedQuestion = {
               questions: '',
               answer: ''
            }
            this.setState({ verifyModal: false, selectedVerifyUserId: '', selectedVerifyUser: null, selectedQuestion: selectedQuestion, check_fullname: 0, check_dob: 0, check_ssn: 0, check_question: 0, check_answer: 0 });
            NotificationManager.success("Customer Verified!");
            self.setState({ loading: false });


         }, 2000);

         if(selectedAppViewId !== '') {
            this.props.history.push('/admin/credit-application/application/' + selectedAppId);
         } else {
            this.props.history.push('/admin/credit-application/plan-details/' + this.enc(selectedAppId.toString()));
         }
      } else {
         setTimeout(() => {
            let selectedQuestion = {
               questions: '',
               answer: ''
            }
            this.setState({ verifyModal: false, selectedVerifyUserId: '', selectedVerifyUser: null, selectedQuestion: selectedQuestion, check_fullname: 0, check_dob: 0, check_ssn: 0, check_question: 0, check_answer: 0, check_phone: 0 });
            NotificationManager.success("Provider Verified!");
            self.setState({ loading: false });


         }, 2000);

         console.log(providerDetail)
         //return false;
         this.props.history.push('/admin/providers/provider-view/' + providerDetail[0].provider_id);
      }

      //}
      //return false;

   }

   validateVerifyForm() {
      if(this.state.accountFilter.sort_by == 2) {
            var count = 0;
            if(this.state.verify_err.check_fullname !== '') {
               count++
            } 
            if(this.state.verify_err.check_dob !== '') {
               count++
            }
            if(this.state.verify_err.check_ssn !== '') {
               count++
            }
            if(this.state.verify_err.check_question !== '') {
               count++
            }

            if(count >= 3) {
               return true
            } else {
               return false
            }
         /*return (

               //this.state.verify_err.questions === '' &&
               //this.state.unlock_err.answer === '' &&
               this.state.verify_err.check_fullname !== '' &&
               this.state.verify_err.check_dob !== '' &&
               this.state.verify_err.check_ssn !== '' &&
               this.state.verify_err.check_question !== ''
               //this.state.verify_err.check_answer !== ''

         );*/
      } else {

         var count = 0;
         if(this.state.verify_err.check_fullname !== '') {
            count++
         } 
         if(this.state.verify_err.check_phone !== '') {
            count++
         }
         if(this.state.verify_err.check_question !== '') {
            count++
         }

         if(count >= 2) {
            return true
         } else {
            return false
         }
         /*return (
            this.state.verify_err.check_fullname !== '' &&
            this.state.verify_err.check_phone !== '' &&
            this.state.verify_err.check_ssn !== ''
         )*/
      }
   }


   //////End Verify User/////

   showSearchFields() {
      this.setState({ hideSearchSection: true })
   }
   hideSearchFields() {
      this.setState({ hideSearchSection: false })
   }

   render() {
      const { invoiceList, loading, selectedUser, allSelected, selectedInvoices, verify_err, selectedVerifyUserQuestions, providerDetail } = this.state;

      const customerColumns = [
         {
            name: 'A/C No',
            field: 'patient_ac',
         },
         {
            name: 'First Name',
            field: 'f_name',
         },
         {
            name: 'Last Name',
            field: 'l_name',
         },
         {
            name: 'DOB',
            field: 'dob',
         },
         {
            name: 'SSN',
            field: 'ssn',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.ssn.replace(/.(?=.{4})/g, 'x')
                  )
               },
            }
         },
         {
            name: 'Phone',
            field: 'peimary_phone',
         },
         {
            name: 'Address1',
            field: 'address1',
         },
         {
            name: 'Address2',
            field: 'address2',
         },
         {
            name: 'City',
            field: 'City',
         },
         {
            name: 'State',
            field: 'state',
         },
         {
            name: 'Zip Code',
            field: 'zip_code',
         },
         {
            name: 'Actions',
            field: 'application_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  if(value.plan_exists>0){
                     return (
                     
                        <React.Fragment>
                           <span className="list-action">
                              <a href="javascript:void(0)" onClick={() => this.onVerifyUser(value.user_id)} title="Verify Customer"><i className="ti-check-box"></i></a>
                              <Link to={`/admin/credit-application/plan-details/${this.enc(value.application_id.toString())}`} title="Create Ticket"><i className="ti-plus"></i></Link>
                           </span>
                        </React.Fragment>
                     )
                  }else{
                     return (
                     
                        <React.Fragment>
                           <span className="list-action">
                              <a href="javascript:void(0)" onClick={() => this.onVerifyUser(value.user_id)} title="Verify Customer"><i className="ti-check-box"></i></a>
                              <Link to={`/admin/credit-application/application/${value.application_id}`} title="Create Ticket"><i className="ti-plus"></i></Link>
                           </span>
                        </React.Fragment>
                     )
                  }
                  
               },
            }
         }

      ];
      const providerColumns = [
         {
            name: 'A/C No',
            field: 'provider_ac',
         },
         {
            name: 'Provider Name',
            field: 'name',
         }, 
         {
            name: 'SSN / Tax ID',
            field: 'tax_ssn_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.tax_ssn_id.replace(/.(?=.{4})/g, 'x')
                  )
               },
            }
         },
         {
            name: 'Phone',
            field: 'primary_phone',
         },
         {
            name: 'Address1',
            field: 'address1',
         },
         {
            name: 'Address2',
            field: 'address2',
         },
         {
            name: 'City',
            field: 'city',
         },
         {
            name: 'State',
            field: 'state',
         },
         {
            name: 'Zip Code',
            field: 'zip_code',
         },
         {
            name: 'Actions',
            field: 'user_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <React.Fragment>
                        <span className="list-action">
                           {/*<Link to={`/admin/credit-application/plan-details/${this.enc(value.application_id.toString())}`} title="View account details"><i className="ti-eye"></i></Link>*/}
                           <a href="javascript:void(0)" onClick={() => this.onVerifyUser(value.user_id)} title="Verify Provider"><i className="ti-check-box"></i></a>
                           <Link to={`/admin/providers/provider-view/${value.provider_id}`} title="Create Ticket"><i className="ti-plus"></i></Link> 
                        </span>
                     </React.Fragment>
                  )
               },
            }
         }

      ];
      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         responsive: "scroll",
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,

      };
      const myTheme = createMuiTheme({
         overrides: {
            
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
         }
      });

      ///ALL support history///
      const supportColumns = [
      {
         name: 'Ticket ID',
         field: 'ticket_id'
      },
      {
         name: 'Call Date/Time',
         field: 'call_date_time'
      },
      {
         name: 'Customer Name',
         field: 'f_name',
         options: {
            customBodyRender: (value) => {
               return (
                  (value.f_name) ? value.f_name+' '+value.m_name+' '+value.l_name : '-'
               );
            }
         }
      },
      {
         name: 'Type(email/ph)',
         field: 'ticket_source',
      },
      
      {
         name: 'App No.',
         field: 'application_no',
         options: {
            customBodyRender: (value) => {
               return (
                  (value.application_no) ? value.application_no : '-'
               );
            }
         }
      },
      {
         name: 'Plan No',
         field: 'plan_number',
         options: {
            customBodyRender: (value) => {
               return (
                  (value.plan_number) ? value.plan_number : '-'
               );
            }
         }
      },
      {
         name: 'Cust Inv No',
         field: 'invoice_number',
         options: {
            customBodyRender: (value) => {
               return (
                  (value.invoice_number) ? value.invoice_number : '-'
               );
            }
         }
      },
      
      {
         name: 'Subject',
         field: 'subject'
      },
      {
         name: 'Status',
         field: 'status',
      },
      {
         name: 'Action',
         field: 'md_id',
         options: {
            noHeaderWrap: true,
            filter: false,
            sort: false,
            download: false,
            customBodyRender: (value, tableMeta, updateValue) => {

               return (
                  <React.Fragment>
                     <span className="list-action">
                        {(value.follow_up_admin == 1)? <Link to="#" title="Need attention"><i className="zmdi zmdi-alert-triangle text-danger"></i></Link> :''}
                        <Link to={`/admin/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                     </span>
                  </React.Fragment>
               )
            },

         }
      }
      
   ];

   const supportOptions = {
      filterType: 'dropdown',
      selectableRows: false,
      
   };
   const supportMyTheme = createMuiTheme({
      overrides: {
         
         MuiTableCell: {
            footer: { padding: "4px 8px 4px 8px" }
         },
         
      }
   });
      return (
         <div className="admin-reports-week-month">
            <Helmet>
               <title>Health Partner | Customer Support</title>
               <meta name="description" content="Customer Support" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.customerSupport" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>

               
                  <div className="modal-body page-form-outer text-left">
                     <Form>
                        {this.state.hideSearchSection && 
                           <div className="row">
                              {/*<div className="col-md-1">
                                 <FormGroup>
                                    <Label for="customer_name"></Label>

                                    <FormGroup check>
                                       <Label check>
                                          <Input
                                             type="radio"
                                             name="sort_by"
                                             value={2}
                                             checked={(this.state.accountFilter.sort_by == 2) ? true : false}
                                             onChange={(e) => this.onChangeReportFilter('sort_by', e.target.value)}
                                          />
                                          Patient
                                    </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                       <Label check>
                                          <Input
                                             type="radio"
                                             name="sort_by"
                                             value={1}
                                             checked={(this.state.accountFilter.sort_by == 1) ? true : false}
                                             onChange={(e) => this.onChangeReportFilter('sort_by', e.target.value)}
                                          />
                                          Provider
                                    </Label>
                                    </FormGroup>

                                 </FormGroup>
                              </div>*/}
                              {this.state.hideField &&
                              <div className="col-md-2">
                                 <FormGroup>
                                    <Label for="first_name">First Name</Label>
                                    <Input
                                       type="text"
                                       name="first_name"
                                       id="first_name"
                                       placeholder="First Name"
                                       onChange={(e) => this.onChangeReportFilter('first_name', e.target.value)}
                                    >
                                    </Input>
                                 </FormGroup>
                              </div>
                              }
                              {this.state.hideField &&
                              <div className="col-md-2">
                                 <FormGroup>
                                    <Label for="last_name">Last Name</Label>
                                    <Input
                                       type="text"
                                       name="last_name"
                                       id="last_name"
                                       placeholder="Last Name"
                                       onChange={(e) => this.onChangeReportFilter('last_name', e.target.value)}
                                    >
                                    </Input>
                                 </FormGroup>
                              </div>
                              }

                              {!this.state.hideField &&
                              <div className="col-md-2">
                                 <FormGroup>
                                    <Label for="provider_name">Provider Name</Label>
                                    <Input
                                       type="text"
                                       name="provider_name"
                                       id="provider_name"
                                       placeholder="Provider Name"
                                       onChange={(e) => this.onChangeReportFilter('provider_name', e.target.value)}
                                    >
                                    </Input>
                                 </FormGroup>
                              </div>
                              }

                              <div className="col-md-2">
                                 <FormGroup>
                                    <Label for="phone">Phone No.</Label>
                                    <Input
                                       type="text"
                                       name="phone"
                                       id="phone"
                                       maxLength="14"
                                       value={(this.state.accountFilter.phone != '') ? this.state.accountFilter.phone : ''}
                                       placeholder="Phone No."
                                       onChange={(e) => this.onChangeReportFilter('phone', e.target.value)}
                                    >
                                    </Input>

                                 </FormGroup>
                              </div>
                              <div className="col-md-2">
                                 <FormGroup>
                                    <Label for="ssn">SSN / Tax ID</Label>
                                    <Input
                                       type="text"
                                       name="ssn"
                                       id="ssn"
                                       placeholder="SSN / Tax ID"
                                       maxLength="11"
                                       value={(this.state.accountFilter.ssn != '') ? this.state.accountFilter.ssn : ''}
                                       onChange={(e) => this.onChangeReportFilter('ssn', e.target.value)}
                                    >
                                    </Input>
                                 </FormGroup>
                              </div>
                              {this.state.hideField &&

                                 <div className="col-md-2">
                                    <FormGroup>
                                       <Label for="dob">DOB</Label>
                                       <DatePicker
                                          dateFormat="MM/dd/yyyy"
                                          name="dob"
                                          id="dob"
                                          selected={this.state.dob}
                                          placeholderText="DOB"
                                          autocomplete={false}
                                          onChange={(e) => this.onChangeReportFilter('dob', e)}
                                       />
                                    </FormGroup>
                                 </div>

                              }
                              <div className="col-md-1 p-0">
                                 <div className="list-action">
                                    <FormGroup>
                                       <Label for="loan_date">&nbsp;</Label>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10">search</i></a>
                                       
                                       <a href="javascript:void(0);" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 mt-10 material-icons">arrow_back</i></a>
                                    </FormGroup>
                                 </div>
                              </div>
                           </div>
                        }
                     </Form>
                     <div className="search-btn d-table w-100 p-1">
                        {!this.state.hideSearchSection &&
                           <Button
                              variant="contained"
                              color="primary"
                              className="text-white hide-show-search-btn"
                              onClick={() => this.showSearchFields()}
                           >
                              Create Ticket
                           </Button>
                        }
                        {this.state.hideSearchSection &&
                           <Button
                              variant="contained"
                              color="primary"
                              className="text-white hide-show-search-btn"
                              onClick={() => this.hideSearchFields()}
                           >
                              Hide
                           </Button>
                        }
                  </div>
                  </div>
            


               {this.props.customerAccountList && this.state.hideSearchSection &&
                  <MuiThemeProvider theme={myTheme}>
                     <MaterialDatatable
                        ref="myTableData"
                        title={"Customer Search Result"}
                        data={(this.props.customerAccountList) ? this.props.customerAccountList : ''}
                        columns={customerColumns}
                        options={options}


                     />
                  </MuiThemeProvider>
               }
               {this.props.providerAccountList && this.state.hideSearchSection &&
                  <MuiThemeProvider theme={myTheme}>
                     <MaterialDatatable
                        ref="myTableData"
                        title={"Provider Search Result"}
                        data={(this.props.providerAccountList) ? this.props.providerAccountList : ''}
                        columns={providerColumns}
                        options={options}


                     />
                  </MuiThemeProvider>
               }

               
               <MuiThemeProvider theme={supportMyTheme}>
                  <MaterialDatatable
                     title={"All Support History"}
                     data={(this.props.tickets) ? this.props.tickets : ''}
                     columns={supportColumns}
                     options={supportOptions}
                  />
               </MuiThemeProvider>
               
               {this.props.loading &&
                  <RctSectionLoader />
               }

               <Modal isOpen={this.state.verifyModal}>
                  <ModalHeader toggle={() => this.verifyModalClose()}>
                     {(this.props.customerAccountList) ? 'Verify Customer' : 'Verify Provider'}
                  </ModalHeader>
                  <ModalBody>
                     {this.props.customerAccountList && 
                        <VerifyCustomerForm
                           addErr={verify_err}
                           questionList={selectedVerifyUserQuestions}
                           onChangeVerifyUser={this.onChangeVerifyUser.bind(this)}
                           check_fullname={this.state.check_fullname}
                           check_dob={this.state.check_dob}
                           check_ssn={this.state.check_ssn}
                           check_question={this.state.check_question}
                           check_answer={this.state.check_answer}
                           verifyUserDetail={this.props.userLockDetail}
                           selectedQuestion={this.state.selectedQuestion}
                        />
                     }

                     {this.props.providerAccountList &&
                        <VerifyProviderForm
                           addErr={verify_err}
                           questionList={selectedVerifyUserQuestions}
                           onChangeVerifyUser={this.onChangeVerifyUser.bind(this)}
                           check_fullname={this.state.check_fullname}
                           check_phone={this.state.check_phone}
                           check_ssn={this.state.check_ssn}
                           check_question={this.state.check_question}
                           check_answer={this.state.check_answer}
                           verifyUserDetail={this.state.providerDetail}
                           selectedQuestion={this.state.selectedQuestion}
                        />
                     }
                  </ModalBody>
                  <ModalFooter>
                     <Button
                        variant="contained"
                        color="primary"
                        className={(this.validateVerifyForm()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.verifyUserAccount()}
                        disabled={!this.validateVerifyForm()}

                     >Verify</Button>

                     <Button variant="contained" className="text-white btn-danger" onClick={() => this.verifyModalClose()}>Cancel</Button>
                  </ModalFooter>

                  {this.state.loading &&
                     <RctSectionLoader />
                  }

               </Modal>

            </RctCollapsibleCard>

         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ CustomerSupportReducer, authUser }) => {
   const { loading, customerAccountList, providerAccountList, customerQuestionsList, tickets } = CustomerSupportReducer;
   
   const { user, userLockDetail } = authUser;
   return { loading, customerAccountList, providerAccountList, userLockDetail, customerQuestionsList, tickets }
}

export default connect(mapStateToProps, {
   customerAccountFilter, getLockUserDetail, getQuestions, getAllTickets
})(CustomerSupport);