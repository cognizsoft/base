/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import {
   fullReportList, loanReportFilter, loanReportDownload, loanReportDownloadXLS, accountReceivablesReport, accountReceivablesReportFilter, accountReceivablesReportDownload, accountReceivablesReportDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class AccountReceivables extends Component {

   state = {

      reportFilter: {
         filter_type: '',
         provider_name: '',
         start_date: '',
         end_date: '',
         year: '',
         month: '',
         quarter: '',
      },
      start_date: '',
      end_date: '',
      widthTable: '',
      mainHeading: 'Account Receivable'
   }


   componentDidMount() {
      //console.log(ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children)
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //this.props.accountReceivablesReport();
      this.props.accountReceivablesReportFilter(this.state.reportFilter);
   }


   onChangeReportFilter(key, value) {
      let { reportFilter } = this.state
      switch (key) {

         case 'filter_type':
            reportFilter['start_date'] = ''
            reportFilter['end_date'] = ''
            reportFilter['year'] = ''
            reportFilter['month'] = ''
            reportFilter['quarter'] = ''
            reportFilter['provider_name'] = ''
            this.setState({ start_date: '', end_date: '' })
            break;
         case 'start_date':
            if (value == null) {
               value = '';
               this.setState({ start_date: '' })
            } else {
               this.setState({ start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'end_date':
            if (value == null) {
               value = '';
               this.setState({ end_date: '' })
            } else {
               this.setState({ end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'year':

            break;
         case 'month':

            break;
         case 'quarter':

            break;
         case 'provider_name':

            break;
         default:
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      this.props.accountReceivablesReportDownload(this.state.reportFilter);
   }
   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.accountReceivablesReportDownloadXLS(this.state.reportFilter);
   }
   fullReportFilter() {
      this.props.accountReceivablesReportFilter(this.state.reportFilter);
      const { reportFilter } = this.state;
      let fileName = '';
      if (reportFilter.filter_type == 1) {
         moment.updateLocale('en', {
            week: {
               dow: 1,
               doy: 1
            }
         });

         let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
         let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
         fileName = 'Weekly Accounts Receivable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
      } else if (reportFilter.filter_type == 2) {
         let monthName = moment().month(reportFilter.month - 1).format("MMM");
         fileName = 'Monthly Accounts Receivable Report (' + monthName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 3) {
         let queterName = ''
         if (reportFilter.quarter == 1) {
            queterName = 'Q1';
         } else if (reportFilter.quarter == 2) {
            queterName = 'Q2';
         } else if (reportFilter.quarter == 3) {
            queterName = 'Q3';
         } else if (reportFilter.quarter == 4) {
            queterName = 'Q4';
         }
         fileName = 'Quarterly Accounts Receivable Report (' + queterName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 4) {
         fileName = 'Yearly Accounts Receivable Report (' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 5) {
         let currentDate = moment().format('MM/DD/YYYY');
         fileName = 'Year to Date Accounts Receivable Report (01/01/' + reportFilter.year + '-' + currentDate + ')';
      } else if (reportFilter.filter_type == 6) {
         let loanStart = moment(reportFilter.start_date).format('MM/DD/YYYY');
         let loanEnd = moment(reportFilter.end_date).format('MM/DD/YYYY');
         fileName = 'By Date Accounts Receivable Report (' + loanStart + '-' + loanEnd + ')';
      } else {
         fileName = this.state.mainHeading;
      }
      this.setState({ mainHeading: fileName })
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {

            if (currentindex >= start && currentindex < end) {
               if (currentValue.main_amount != '') {
                  accumulator['main_amount'] = (accumulator['main_amount'] !== undefined) ? accumulator['main_amount'] + currentValue.main_amount : currentValue.main_amount;
               }
               if (currentValue.remaining_amount != '') {
                  accumulator['remaining_amount'] = (accumulator['remaining_amount'] !== undefined) ? accumulator['remaining_amount'] + currentValue.remaining_amount : currentValue.remaining_amount;
               }
               if (currentValue.interest_due != '') {
                  accumulator['interest_due'] = (accumulator['interest_due'] !== undefined) ? accumulator['interest_due'] + currentValue.interest_due : currentValue.interest_due;
               }

               //accumulator['total_due'] = var1+var2
               accumulator['total_due'] = (accumulator['total_due'] !== undefined) ? parseFloat(accumulator['total_due']) + currentValue.interest_due + currentValue.remaining_amount : currentValue.interest_due + currentValue.remaining_amount;

               accumulator['total_amt_rcvd'] = (accumulator['total_amt_rcvd'] !== undefined) ? parseFloat(accumulator['total_amt_rcvd']) + (currentValue.main_amount - currentValue.remaining_amount) + currentValue.inv_interest + currentValue.late_fee + currentValue.fin_fee : (currentValue.main_amount - currentValue.remaining_amount) + currentValue.inv_interest + currentValue.late_fee + currentValue.fin_fee;

               var prin_bal;
               if ((currentValue.main_amount - currentValue.remaining_amount) < currentValue.main_amount && (currentValue.main_amount - currentValue.remaining_amount) !== 0) {
                  prin_bal = currentValue.main_amount - currentValue.remaining_amount;
               } else if ((currentValue.main_amount - currentValue.remaining_amount) == 0) {
                  prin_bal = currentValue.main_amount;
               } else {
                  prin_bal = 0;
               }

               accumulator['prin_bal'] = (accumulator['prin_bal'] !== undefined) ? parseFloat(accumulator['prin_bal']) + prin_bal : prin_bal;



            }

            return accumulator
         }, []);
         //console.log(amount);
         return amount
      }

      //return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   componentWillReceiveProps(nextProps) {
      var lateFinArr = [];
      if (nextProps.lateFinCharge) {

         /*nextProps.lateFinCharge.forEach(function(chr) {
            var late_fee = (chr.late_fee_received*chr.late_fee_per)/100
            var fin_fee = (chr.fin_charge_amt*chr.fin_charge_per)/100
            var pp_id = chr.pp_id

            var obj = {
               "late_fee": late_fee,
               "fin_fee": fin_fee,
               "pp_id": pp_id,
            }
            lateFinArr.push(obj)
         })*/
         //console.log('nextProps.lateFinCharge')
         //console.log(nextProps.lateFinCharge)


         //let perMonthInt = (nextProps.closeInt / 12) / 100;

         //let perMonth = nextProps.planDetails.loan_amount  (perMonthInt  Math.pow((1 + perMonthInt), nextProps.planDetails.term)) / (Math.pow((1 + perMonthInt), nextProps.planDetails.term) - 1) * term - loan_amount;

         //var newTerm = -Math.log(parseFloat(((parseFloat(-perMonthInt)  nextProps.planDetails.loan_amount) / (12  nextProps.planDetails.monthly_amount) + 1))) / Math.log((1 + parseFloat(perMonthInt) / 12));

      }
   }


   validateButton() {

      if (this.state.reportFilter.filter_type == 1 || this.state.reportFilter.filter_type == 5) {
         return true
      } else if (this.state.reportFilter.filter_type == 2) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.month
         )
      } else if (this.state.reportFilter.filter_type == 3) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.quarter
         )
      } else if (this.state.reportFilter.filter_type == 4) {
         return (
            this.state.reportFilter.year
         )
      } else if (this.state.reportFilter.filter_type == 6) {
         return (
            this.state.reportFilter.start_date &&
            this.state.reportFilter.end_date
         )
      } else {
         return false
      }

   }

   render() {

      const acc_rcv_report_list = this.props.acc_rcv_report_list;

      let currentYear = new Date();
      let currentMonth = currentYear.getMonth() + 1;
      currentYear = currentYear.getFullYear();
      const yearList = [];
      if (this.props.yearStart != '') {
         for (var i = currentYear; i >= this.props.yearStart; i--) {
            yearList.push({ id: i, value: i });
         }
      } else {
         for (var i = currentYear; i >= (currentYear - 5); i--) {
            yearList.push({ id: i, value: i });
         }
      }

      let totalPriAmt = 0;
      let totalPriDueAmt = 0;
      let totalPriBlcAmt = 0;
      let totalIntDueAmt = 0;
      let totalLateAmt = 0;
      let totalFinAmt = 0;
      let totalDueAmt = 0;
      let totalAmtRcvd = 0;
      let totalRefAmt = 0;
      return (
         <div className="admin-reports-loan-report">
            <Helmet>
               <title>Health Partner | Accounts | Accounts Receivable</title>
               <meta name="description" content="Accounts Receivable" />
            </Helmet>
            <PageTitleBar
               title={this.state.mainHeading}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form autoComplete="off">

                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="filter_type">Report Period</Label>
                              <Input
                                 type="select"
                                 name="filter_type"
                                 id="filter_type"
                                 value={this.state.reportFilter.filter_type}
                                 onChange={(e) => this.onChangeReportFilter('filter_type', e.target.value)}
                              >
                                 <option value="">Select Report Period</option>
                                 <option value="1">Current Week</option>
                                 <option value="2">Monthly</option>
                                 <option value="3">Quarterly</option>
                                 <option value="4">Yearly</option>
                                 <option value="5">Year to date</option>
                                 <option value="6">By Date</option>
                              </Input>

                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2 || this.state.reportFilter.filter_type == 3 || this.state.reportFilter.filter_type == 4) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="year">Year</Label>
                              <Input
                                 type="select"
                                 name="year"
                                 id="year"
                                 value={this.state.reportFilter.year}
                                 onChange={(e) => this.onChangeReportFilter('year', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {yearList && yearList.map((data, key) => (
                                    <option value={data.id} key={key}>{data.value}</option>
                                 ))}
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="month">Month</Label>
                              <Input
                                 type="select"
                                 name="month"
                                 id="month"
                                 value={this.state.reportFilter.month}
                                 onChange={(e) => this.onChangeReportFilter('month', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1" disabled={(currentMonth < 1 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jan</option>
                                 <option value="2" disabled={(currentMonth < 2 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Feb</option>
                                 <option value="3" disabled={(currentMonth < 3 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Mar</option>
                                 <option value="4" disabled={(currentMonth < 4 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Apr</option>
                                 <option value="5" disabled={(currentMonth < 5 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>May</option>
                                 <option value="6" disabled={(currentMonth < 6 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jun</option>
                                 <option value="7" disabled={(currentMonth < 7 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jul</option>
                                 <option value="8" disabled={(currentMonth < 8 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Aug</option>
                                 <option value="9" disabled={(currentMonth < 9 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Sep</option>
                                 <option value="10" disabled={(currentMonth < 10 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Oct</option>
                                 <option value="11" disabled={(currentMonth < 11 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Nov</option>
                                 <option value="12" disabled={(currentMonth < 12 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Dec</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 3) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="quarter">Quarterly</Label>
                              <Input
                                 type="select"
                                 name="quarter"
                                 id="quarter"
                                 value={this.state.reportFilter.quarter}
                                 onChange={(e) => this.onChangeReportFilter('quarter', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Q1</option>
                                 <option value="2">Q2</option>
                                 <option value="3">Q3</option>
                                 <option value="4">Q4</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="start_date">Loan Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="start_date"
                                 id="start_date"
                                 selected={this.state.start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="end_date"></Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="end_date"
                                 id="end_date"
                                 selected={this.state.end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('end_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="provider_name">Provider Name</Label>
                              <Input
                                 type="text"
                                 name="provider_name"
                                 id="provider_name"
                                 value={this.state.reportFilter.provider_name}
                                 placeholder="Provider Name"
                                 onChange={(e) => this.onChangeReportFilter('provider_name', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>
                        <div className="col-md-4">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="btn">&nbsp;</Label>
                                 {this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down custom_icon"></i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                                 }
                                 {!this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down_disable custom_icon"></i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf_disable">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls_disable">insert_drive_file</i></a>
                                    </span>
                                 }
                              </FormGroup>
                           </div>
                        </div>
                     </div>

                  </Form>
               </div>
               <div className="table-responsive">
                  <table className="table table-bordered table-sm">
                     <thead>
                        <tr>
                           <th>Loan No</th>
                           <th>Provider Name</th>
                           <th>Customer Name</th>
                           <th>Loan Date</th>
                           <th>Prin Amt</th>
                           <th>Prin Due</th>
                           <th>Prin Bal</th>
                           <th>Int Due</th>
                           <th>Late Fee</th>
                           <th>Fin Charges</th>
                           <th>Total Due</th>
                           <th>Amt Rcvd</th>
                           <th>Refund Amt</th>
                        </tr>
                     </thead>
                     <tbody>
                        {acc_rcv_report_list && acc_rcv_report_list.map(function (row, idx) {
                           let prin_bal;
                           if ((row.main_amount - row.remaining_amount) < row.main_amount && (row.main_amount - row.remaining_amount) !== 0) {
                              prin_bal = row.main_amount - row.remaining_amount;
                           } else if ((row.main_amount - row.remaining_amount) == 0) {
                              prin_bal = row.main_amount;
                           } else {
                              prin_bal = 0;
                           }

                           totalPriAmt = totalPriAmt + row.main_amount;
                           totalPriDueAmt = totalPriDueAmt + row.remaining_amount;
                           totalPriBlcAmt = totalPriBlcAmt + prin_bal;
                           totalIntDueAmt = totalIntDueAmt + row.interest_due;
                           totalLateAmt = totalLateAmt + row.late_fee;
                           totalFinAmt = totalFinAmt + row.fin_fee;
                           totalDueAmt = totalDueAmt + (row.remaining_amount + row.interest_due);
                           totalAmtRcvd = totalAmtRcvd + ((row.main_amount - row.remaining_amount) + row.inv_interest + row.late_fee + row.fin_fee);
                           row.refund_amt = (row.refund_amt > 0)?row.refund_amt:0;
                           totalRefAmt = totalRefAmt + row.refund_amt;
                           return <tr key={idx}>
                              <td>
                                 <Link to={`/admin/credit-application/plan-details/${this.enc(row.application_id.toString())}`}>{row.plan_number}</Link>
                              </td>
                              <td>{row.name}</td>
                              <td>{row.f_name + ' ' + row.m_name + ' ' + row.l_name}</td>
                              <td>{row.loan_date}</td>
                              <td className="text-right">{(row.main_amount) ? '$' + row.main_amount.toFixed(2) : '$0.00'}</td>
                              <td className="text-right">{(row.remaining_amount) ? '$' + (row.remaining_amount).toFixed(2) : '$0.00'}</td>
                              <td className="text-right">${parseFloat(prin_bal).toFixed(2)}</td>
                              <td className="text-right">{(row.interest_due) ? '$' + (parseFloat(row.interest_due)).toFixed(2) : '$0.00'}</td>
                              <td className="text-right">${parseFloat(row.late_fee).toFixed(2)}</td>
                              <td className="text-right">${parseFloat(row.fin_fee).toFixed(2)}</td>
                              <td className="text-right">${parseFloat(row.remaining_amount + row.interest_due).toFixed(2)}</td>
                              <td className="text-right">${parseFloat((row.main_amount - row.remaining_amount) + row.inv_interest + row.late_fee + row.fin_fee).toFixed(2)}</td>
                              <td className="text-right">${(row.refund_amt) ? parseFloat(row.refund_amt).toFixed(2) : '0.00'}</td>
                           </tr>
                        }.bind(this))
                        }
                        {(acc_rcv_report_list === undefined || acc_rcv_report_list.length == 0) &&
                           <tr>
                              <td colSpan="15">Sorry, no matching records found</td>
                           </tr>
                        }
                     </tbody>
                     <tfoot>
                        <tr>
                           <th colSpan="4">Total</th>
                           <th className="text-right">${parseFloat(totalPriAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalPriDueAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalPriBlcAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalIntDueAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalLateAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalFinAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalDueAmt).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalAmtRcvd).toFixed(2)}</th>
                           <th className="text-right">${parseFloat(totalRefAmt).toFixed(2)}</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {

   const { loading, acc_rcv_report_list, yearStart, lateFinCharge } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, acc_rcv_report_list, yearStart, lateFinCharge }

}

export default connect(mapStateToProps, {
   fullReportList, loanReportFilter, loanReportDownload, loanReportDownloadXLS, accountReceivablesReport, accountReceivablesReportFilter, accountReceivablesReportDownload, accountReceivablesReportDownloadXLS
})(AccountReceivables);