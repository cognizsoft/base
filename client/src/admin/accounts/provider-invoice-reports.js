/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty, formatPhoneNumber, isPhone } from '../../validator/Validator';
import {
   providerInvoiceReport, providerInvoiceReportFilter, providerInvoiceReportDownload, providerInvoiceReportDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class ProviderInvoiceReports extends Component {

   state = {
      reportFilter: {
         paid_start_date: '',
         paid_end_date: '',
         provider_name: '',
         provider_phone: '',
         status: '',
      },
      paid_start_date: '',
      paid_end_date: '',
      widthTable: '',
   }


   componentDidMount() {
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //console.log(this.props.match.params.status_id)

      this.props.providerInvoiceReport(this.props.match.params.status_id);

   }


   onChangeReportFilter(key, value) {

      switch (key) {
         case 'paid_start_date':
            if (value == null) {
               value = '';
               this.setState({ paid_start_date: '' })
            } else {
               this.setState({ paid_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'paid_end_date':
            if (value == null) {
               value = '';
               this.setState({ paid_end_date: '' })
            } else {
               this.setState({ paid_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'provider_phone':
            if (isEmpty(value)) {
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
            } else {
               value = formatPhoneNumber(value)
            }
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      //console.log(this.state.reportFilter);
      this.props.providerInvoiceReportDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      //console.log(this.state.reportFilter);
      this.props.providerInvoiceReportFilter(this.state.reportFilter);
   }

   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.providerInvoiceReportDownloadXLS(this.state.reportFilter);
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {

               accumulator['total_amt'] = (accumulator['total_amt'] !== undefined) ? accumulator['total_amt'] + currentValue.total_amt : currentValue.total_amt;
               accumulator['total_hps_discount'] = (accumulator['total_hps_discount'] !== undefined) ? accumulator['total_hps_discount'] + currentValue.total_hps_discount : currentValue.total_hps_discount;
               accumulator['check_amount'] = (accumulator['check_amount'] !== undefined) ? accumulator['check_amount'] + currentValue.check_amount : currentValue.check_amount;
            }

            return accumulator
         }, []);
         return amount
      }
   }

   render() {
      /*const columns = [

         {
            name: 'Invoice ID',
            field: 'invoice_number'
         },
         {
            name: 'Provider Name',
            field: 'name',
         },
         {
            name: 'Provider Location',
            field: 'location_name',
         },
         {
            name: 'Provider Phone',
            field: 'phone'
         },
         {
            name: 'Total Amt',
            field: 'total_amt',
            options: {
               customBodyRender: (value) => {
                  return (
                     '$' + parseFloat(value.total_amt).toFixed(2)
                  );
               }
            }
         },
         {
            name: 'HPS Discount',
            field: 'total_hps_discount',
            options: {
               customBodyRender: (value) => {
                  return (
                     '$' + parseFloat(value.total_hps_discount).toFixed(2)
                  );
               }
            }
         },
         {
            name: 'Amount Due',
            field: 'check_amount',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.value != 'Paid') ? '$' + (parseFloat(value.total_amt) - parseFloat(value.total_hps_discount)).toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Amount Paid',
            field: 'check_amount',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.check_amount) ? '$' + parseFloat(value.check_amount).toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'IOU Adj',
            field: 'check_amount',
            options: {
               customBodyRender: (value) => {
                  let iouAdj = this.props.iouAmt.filter(function (el) {
                     return el.provider_invoice_id == value.provider_invoice_id;
                  });
                  let totalIOU = iouAdj.reduce(function (accumulator, currentValue, currentindex) {
                     return accumulator += currentValue.iou_paid_amount;
                  }, 0);
                  return (
                     (totalIOU) ? '$' + parseFloat(totalIOU).toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Invoice Date',
            field: 'date_created',
         },
         {
            name: 'Paid Date',
            field: 'date_paid',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.date_paid) ? value.date_paid : '-'
                  );
               }
            }
         },
         {
            name: 'Bank Name',
            field: 'bank_name',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.date_paid) ? value.bank_name : '-'
                  );
               }
            }
         },
         {
            name: 'Txn No',
            field: 'txn_no',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.date_paid) ? value.txn_no : '-'
                  );
               }
            }
         },
         {
            name: "Status",
            field: "value",

         },
         {
            name: "Action",
            options: {
               noHeaderWrap: true,
               sort: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <span className="list-action">
                        {(value.value != 'Reject') ? <Link to={`/admin/provider-invoice-reports/view/${value.provider_invoice_id}/${value.provider_id}`}><i className="ti ti-eye"></i></Link> : '-'}
                     </span>
                  )
               }
            }
         }

      ];
      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,
         customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            rowsSelected
         ) => {
            const invoiceSubtotal = this.totalLoanAmount(this.props.providerInvoiceList, page, rowsPerPage);
            return (

               <TableFooter>
                  <TableRow>
                     <TableCell style={footerStyle} ><b>Total Amount :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.total_amt !== null) ? invoiceSubtotal.total_amt.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total HPS Discount :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.total_hps_discount !== null) ? invoiceSubtotal.total_hps_discount.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Paid Amount :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.check_amount !== null) ? invoiceSubtotal.check_amount.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                  </TableRow>

                  <TableRow>
                     <TablePagination
                        count={count}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={(_, page) => changePage(page)}
                        onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                        rowsPerPageOptions={[10, 20, 50, 100]}
                     />
                  </TableRow>
               </TableFooter>
            );
         }

      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
         }
      });*/
      let totalAmt = 0;
      let discountAmt = 0;
      let dueAmt = 0;
      let paidAmt = 0;
      let iouAmt = 0;
      return (
         <div className="admin-reports-pro-invoice-report">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.provider-invoice-reports" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="provider_name">Provider Name</Label>
                              <Input
                                 type="text"
                                 name="provider_name"
                                 id="provider_name"
                                 placeholder="Provider Name"
                                 onChange={(e) => this.onChangeReportFilter('provider_name', e.target.value)}
                              >

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="provider_phone">Provider Phone</Label>
                              <Input
                                 type="text"
                                 name="provider_phone"
                                 id="provider_phone"
                                 placeholder="Provider Phone"
                                 maxLength="14"

                                 value={this.state.reportFilter.provider_phone}
                                 onChange={(e) => this.onChangeReportFilter('provider_phone', e.target.value)}
                              >

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="paid_start_date">Invoice date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="paid_start_date"
                                 id="paid_start_date"
                                 selected={this.state.paid_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('paid_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="paid_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="paid_end_date"
                                 id="paid_end_date"
                                 selected={this.state.paid_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('paid_end_date', e)}
                              />
                           </FormGroup>
                        </div>




                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="status">Status</Label>
                              <Input
                                 type="select"
                                 name="status"
                                 id="status"
                                 placeholder="Financial Chargesstatus"
                                 onChange={(e) => this.onChangeReportFilter('status', e.target.value)}
                              >
                                 <option value="">Status</option>
                                 {this.props.statusList && this.props.statusList.map((status, key) => (
                                    <option value={status.status_id} key={key}>{status.value}</option>
                                 ))}

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>
                                 <span>
                                    <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download" title="Search"><i className="material-icons mr-5 mt-10 btn_down custom_icon"></i></a>
                                    <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-5 mt-10 btn_down">picture_as_pdf</i></a>
                                    <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-5 mt-10 btn_down">insert_drive_file</i></a>
                                    <a href="javascript:void(0);" onClick={this.goBack.bind(this)} title="Back"><i className="mr-5 mt-10 material-icons btn_down">arrow_back</i></a>
                                 </span>
                              </FormGroup>
                           </div>
                        </div>


                     </div>
                  </Form>
                  <div className="table-responsive">
                     <table className="table table-bordered table-sm">
                        <thead>
                           <tr>
                              <th>Invoice ID</th>
                              <th>Provider Name</th>
                              <th>Provider Location</th>
                              <th>Provider Phone</th>
                              <th>Total Amt</th>
                              <th>HPS Discount</th>
                              <th>Amount Due</th>
                              <th>Amount Paid</th>
                              <th>IOU Adj</th>
                              <th>Invoice Date</th>
                              <th>Paid Date</th>
                              <th>Bank Name</th>
                              <th>Txn No</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.providerInvoiceList && this.props.providerInvoiceList.map(function (row, idx) {
                              let iouAdj = this.props.iouAmt.filter(function (el) {
                                 return el.provider_invoice_id == row.provider_invoice_id;
                              });
                              let totalIOU = iouAdj.reduce(function (accumulator, currentValue, currentindex) {
                                 return accumulator += currentValue.iou_paid_amount;
                              }, 0);
                              totalAmt = totalAmt + row.total_amt;
                              discountAmt = discountAmt + row.total_hps_discount;
                              let dAmt = (row.value != 'Paid') ? (parseFloat(row.total_amt) - parseFloat(row.total_hps_discount)) : 0;
                              dueAmt = dueAmt + dAmt;
                              paidAmt = paidAmt + row.check_amount;
                              iouAmt = iouAmt + totalIOU;
                              return <tr key={idx}>
                                 <td>
                                    {row.invoice_number}
                                 </td>
                                 <td>{row.name}</td>
                                 <td>{row.location_name}</td>
                                 <td>{row.phone}</td>
                                 <td className="text-right">{(row.total_amt) ? '$' + row.total_amt.toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.total_hps_discount) ? '$' + row.total_hps_discount : '$0.00'}</td>
                                 <td className="text-right">{(row.value != 'Paid') ? '$' + (parseFloat(row.total_amt) - parseFloat(row.total_hps_discount)).toFixed(2) : '-'}</td>
                                 <td className="text-right">{(row.check_amount) ? '$' + (row.check_amount).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(totalIOU) ? '$' + totalIOU.toFixed(2) : '$0.00'}</td>
                                 <td>{(row.date_created) ? row.date_created : '-'}</td>
                                 <td>{(row.date_paid) ? row.date_paid : '-'}</td>
                                 <td>{(row.bank_name) ? row.bank_name : '-'}</td>
                                 <td>{(row.date_paid) ? row.txn_no : '-'}</td>
                                 <td>{row.value}</td>
                                 <td>
                                    <span className="list-action">
                                       {(row.value != 'Reject') ? <Link to={`/admin/provider-invoice-reports/view/${row.provider_invoice_id}/${row.provider_id}`}><i className="ti ti-eye"></i></Link> : '-'}
                                    </span>
                                 </td>
                              </tr>
                           }.bind(this))
                           }
                           {(this.props.providerInvoiceList === undefined || this.props.providerInvoiceList.length == 0) &&
                              <tr>
                                 <td colSpan="15">Sorry, no matching records found</td>
                              </tr>
                           }
                        </tbody>
                        <tfoot>
                           <tr>
                              <th colSpan="4">Total</th>
                              <th className="text-right">${parseFloat(totalAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(discountAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(dueAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(paidAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(iouAmt).toFixed(2)}</th>
                              <th colSpan="6"></th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               {/*<MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={this.props.providerInvoiceList}
                     columns={columns}
                     options={options}


                        />
                        </MuiThemeProvider>*/}

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   //console.log(AdminReportReducer)

   const { loading, providerInvoiceList, statusList, providerList, iouAmt } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, providerInvoiceList, statusList, providerList, iouAmt }

}

export default connect(mapStateToProps, {
   providerInvoiceReport, providerInvoiceReportFilter, providerInvoiceReportDownload, providerInvoiceReportDownloadXLS
})(ProviderInvoiceReports);