/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import CryptoJS from 'crypto-js';
import {
   customerWeekMonthInvoiceReport, customerInvoiceReportFilter, customerInvoiceReportDownload, customerInvoiceReportDownloadXLS, customerReminderEmail
} from 'Actions';
import { Link } from 'react-router-dom';
//import AddNewButton from './AddNewButton';

class ProviderInvoiceReports extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      invoiceList: null, // initial user data
      selectedUser: null, // selected user to perform operations
      allSelected: false,
      selectedInvoices: 0,
      selectedInvoiceArr: null,
      reportFilter: {
         //short_by: 2,
         invoice_start_date: '',
         invoice_end_date: '',
         invoice_due_start_date: '',
         invoice_due_end_date: '',
         customer_name: '',
         invoice_number: '',
         status: '',
      },
      invoice_start_date: '',
      invoice_end_date: '',
      invoice_due_start_date: '',
      invoice_due_end_date: '',
      widthTable: '',
   }

   componentWillReceiveProps(nextProps) {
      (nextProps.customerInvoiceList) ? this.setState({ invoiceList: nextProps.customerInvoiceList }) : '';
   }

   componentDidMount() {
      this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //this.props.customerWeekMonthInvoiceReport();
      this.permissionFilter(this.state.currentModule);
      const { reportFilter } = this.state;
      (this.props.match.params.appid) ? reportFilter.status = this.props.match.params.appid : '';
      this.setState({ reportFilter: reportFilter })
      this.props.customerInvoiceReportFilter(this.state.reportFilter);
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   onChangeReportFilter(key, value) {
      switch (key) {
         case 'invoice_start_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_start_date: '' })
            } else {
               this.setState({ invoice_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_end_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_end_date: '' })
            } else {
               this.setState({ invoice_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_due_start_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_due_start_date: '' })
            } else {
               this.setState({ invoice_due_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'invoice_due_end_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_due_end_date: '' })
            } else {
               this.setState({ invoice_due_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;

      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      this.props.customerInvoiceReportDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      this.props.customerInvoiceReportFilter(this.state.reportFilter);
   }

   fullReportDownloadXLS() {
      this.props.customerInvoiceReportDownloadXLS(this.state.reportFilter);
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   reminderEmail() {
      console.log(this.state.selectedInvoiceArr)
      this.props.customerReminderEmail(this.state.selectedInvoiceArr);
   }
   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         },
         MuiTableCell: {
            root: {
               padding: "4px 8px 4px 8px !important"
            }
         },
         MuiSvgIcon: {
            root: {
               width: '1em !important'
            }
         },
         MuiButton: {
            containedPrimary: {
               'background-color': '#0E5D97'
            }
         },
         MuiCheckbox: {
            root: {
               "&$checked": {
                  color: '#0E5D97 !important'
               }
            }

         }
      }
   })
   onSelectAllInvoice(e) {
      const { selectedInvoices, invoiceList } = this.state;
      let selectAll = selectedInvoices < invoiceList.length;
      var selectedInvoiceArr = [];
      if (selectAll) {
         let selectAllInvoices = invoiceList.map(invoice => {
            //invoice.checked = (invoice.procedure_status == 0 || new Date(invoice.procedure_date) > new Date()) ? false : true
            invoice.checked = (invoice.invoice_status !== 3) ? false : true;
            (invoice.invoice_status == 3) ? selectedInvoiceArr.push(invoice.invoice_id) : '';
            return invoice
         });
         this.setState({ invoiceList: selectAllInvoices, selectedInvoices: selectAllInvoices.length, selectedInvoiceArr })
      } else {
         let unselectedInvoices = invoiceList.map(invoice => {
            invoice.checked = false;
            //(invoice.paid_flag == 3) ? selectedInvoiceArr.push(invoice.invoice_id) : '';
            return invoice;
         });
         this.setState({ selectedInvoices: 0, invoiceList: unselectedInvoices, selectedInvoiceArr: null });
      }
   }
   /**
    * On Select User
    */
   onSelectInvoice(invoice) {
      invoice.checked = !invoice.checked;
      let selectedInvoices = 0;
      var selectedInvoiceArr = [];
      let invoiceList = this.state.invoiceList.map(invoiceData => {
         if (invoiceData.invoice_number === invoice.invoice_number) {
            if (invoiceData.checked && invoiceData.checked !== undefined) {
               selectedInvoices++;
               selectedInvoiceArr.push(invoiceData.invoice_id)
            }
            return invoice;
         } else {
            if (invoiceData.checked && invoiceData.checked !== undefined) {
               selectedInvoices++;
               selectedInvoiceArr.push(invoiceData.invoice_id)
            }
            return invoiceData;
         }
      });
      this.setState({ invoiceList, selectedInvoices, selectedInvoiceArr });
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {

               if (currentValue.invoice_status == 3) {
                  var totalAmt = (parseFloat(currentValue.previous_late_fee) + parseFloat(currentValue.previous_fin_charge) + parseFloat(currentValue.late_fee_due) + parseFloat(currentValue.fin_charge_due) + parseFloat(currentValue.payment_amount)).toFixed(2)
               } else {
                  var totalAmt = (parseFloat(currentValue.late_fee_received) + parseFloat(currentValue.fin_charge_received) + parseFloat(currentValue.payment_amount)).toFixed(2)
               }
               accumulator['payment_amount'] = (accumulator['payment_amount'] !== undefined) ? accumulator['payment_amount'] + parseFloat(totalAmt) : parseFloat(totalAmt);
            }

            return accumulator
         }, []);
         return amount
      }
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      const { invoiceList, loading, selectedUser, allSelected, selectedInvoices } = this.state;
      console.log(invoiceList)
      const columns = [
         {
            name: 'select',
            field: ' ',
            options: {
               filter: false,
               short: false,
               customHeadRender: (rowDataObject, tableMeta, updateValue) => {
                  return (

                     <TableCell key={rowDataObject} component="th" className="p-5 heading-checkbox">
                        <Checkbox
                           key={rowDataObject.rowIndex}
                           indeterminate={selectedInvoices > 0 && selectedInvoices < invoiceList.length}
                           checked={selectedInvoices > 0}
                           onChange={(e) => this.onSelectAllInvoice(e)}
                           value="all"
                           color="primary"
                        />
                     </TableCell>
                  )
               },
               customBodyRender: (value, tableMeta, updateValue) => {

                  return (
                     <FormControlLabel
                        key={tableMeta.rowIndex}
                        control={
                           <Checkbox
                              checked={(value.checked) ? true : false}
                              disabled={(value.invoice_status !== 3) ? true : false}
                              onChange={() => this.onSelectInvoice(value)}
                              color="primary"
                           />
                        }
                     />
                  )


               }
            }
         },
         {
            name: 'Invoice ID',
            field: 'invoice_number',
            footer: 'Total: 100',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Customer Name',
            field: 'name',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  );
               }
            }
         },
         {
            name: 'Address',
            field: 'address1',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'City',
            field: 'City',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'State',
            field: 'state_name',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Zip',
            field: 'zip_code',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Phone',
            field: 'peimary_phone',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Invoice date',
            field: 'payment_date',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (value.payment_date) ? value.payment_date : '-';
               }
            }
         },
         {
            name: 'Invoice due date',
            field: 'due_date',
            options: {
               headerNoWrap: true,
            }
         },
         {
            name: 'Invoice Amt',
            field: 'payment_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  if (value.invoice_status == 3) {
                     var totalAmt = (parseFloat(value.previous_late_fee) + parseFloat(value.previous_fin_charge) + parseFloat(value.late_fee_due) + parseFloat(value.fin_charge_due) + parseFloat(value.payment_amount)).toFixed(2)
                  } else {
                     var totalAmt = (parseFloat(value.late_fee_received) + parseFloat(value.fin_charge_received) + parseFloat(value.payment_amount)).toFixed(2)
                  }
                  return (
                     (totalAmt) ? '$' + totalAmt : '-'
                  );
               }
            }
         },
         {
            name: 'Addtl Amt',
            field: 'additional_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {
                  return (
                     (value.additional_amount) ? '$' + value.additional_amount.toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Total Paid',
            field: 'paid_amount',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  var totalAmt = (parseFloat(value.late_fee_received) + parseFloat(value.fin_charge_received) + parseFloat(value.paid_amount)).toFixed(2)

                  return (
                     (value.invoice_status == 1 || value.invoice_status == 4) ? '$' + (parseFloat(totalAmt) + parseFloat(value.additional_amount)).toFixed(2) : '-'
                  );
               }
            }
         },
         {
            name: 'Invoice status',
            field: 'paid_flag',
            options: {
               headerNoWrap: true,
               customBodyRender: (value) => {

                  return (
                     value.invoice_status_name
                  );
               }
            }
         },
         {
            name: 'Action',
            field: 'application_id',
            options: {
               headerNoWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value) => {
                  var due_date = value.due_date
                  var exp_date = due_date.split('/');
                  return (
                     <React.Fragment>
                        <span className="list-action">

                           {(this.state.currentPermision.view) ? (value.invoice_status == 3) ? <Link to={`/admin/credit-application/invoice/${this.enc(value.invoice_id.toString())}`} title="Invoice"><i className="zmdi zmdi-file-text icon-fr"></i></Link> : '' : ''}
                           {(this.state.currentPermision.view) ? (value.invoice_status == 2) ? <Link to={`/admin/credit-application/payment/${this.enc(value.invoice_id.toString())}`} title="Pay"><i className="zmdi zmdi-edit icon-fr"></i></Link> : '' : ''}
                           {(this.state.currentPermision.view) ? (value.invoice_status == 1 || value.invoice_status == 4) ? <Link to={`/admin/credit-application/receipt/${this.enc(value.invoice_id.toString())}`} title="Payment Receipt"><i className="zmdi zmdi-eye icon-fr"></i></Link> : '' : ''}

                        </span>
                     </React.Fragment>
                  )
               },

            }
         },

      ];
      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         responsive: "scroll",
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,
         /*customRow: (data) => {
         

         return (
            <TableRow>
            <TableCell rowSpan={3} />
            <TableCell colSpan={2}>Subtotal</TableCell>
            <TableCell align="right">{ccyFormat(invoiceSubtotal)}</TableCell>
            </TableRow>
         );
         },*/

         customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            rowsSelected
         ) => {
            const invoiceSubtotal = this.totalLoanAmount(this.props.customerInvoiceList, page, rowsPerPage);

            return (
               <React.Fragment>

                  <TableFooter>
                     <TableRow>
                        <TableCell style={footerStyle} ><b>Total Invoice Amount :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.payment_amount !== null) ? invoiceSubtotal.payment_amount.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     </TableRow>
                     <TableRow>
                        <TablePagination
                           count={count}
                           rowsPerPage={rowsPerPage}
                           page={page}
                           onChangePage={(_, page) => changePage(page)}
                           onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                           rowsPerPageOptions={[10, 20, 50, 100]}
                        />
                     </TableRow>
                  </TableFooter>
               </React.Fragment>
            );
         },

      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
            MuiFormControlLabel: {
               root: {
                  margin: '0px'
               }
            },
            MuiCheckbox : {
               root: {
                  padding : '0px 12px',
                  margin: '0px'
               }
               
            }
         }
      });
      return (
         <div className="admin-reports-week-month">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.weekMonth" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="customer_name">Customer Name</Label>
                              <Input
                                 type="text"
                                 name="customer_name"
                                 id="customer_name"
                                 placeholder="Search by Customer Name"
                                 onChange={(e) => this.onChangeReportFilter('customer_name', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="status">Status</Label>
                              <Input
                                 type="select"
                                 name="status"
                                 id="status"
                                 placeholder="Financial Chargesstatus"
                                 value={this.state.reportFilter.status}
                                 onChange={(e) => this.onChangeReportFilter('status', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {this.props.customerInvoiceStatus && this.props.customerInvoiceStatus.map((inv_status, key) => (
                                    <option value={inv_status.status_id} key={key}>{inv_status.value}</option>
                                 ))}

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="invoice_number">Invoice Number</Label>
                              <Input
                                 type="text"
                                 name="invoice_number"
                                 id="invoice_number"
                                 placeholder="Invoice Number"
                                 onChange={(e) => this.onChangeReportFilter('invoice_number', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <div className="list-action">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>
                                 <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10">search</i></a>
                                 <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10">picture_as_pdf</i></a>
                                 <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10">insert_drive_file</i></a>
                                 <a href="javascript:void(0)" onClick={this.reminderEmail.bind(this)} className="report-download" title="Invoice Reminder Email"><i className="material-icons mr-10 mt-10">email</i></a>
                                 <a href="javascript:void(0);" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 mt-10 material-icons">arrow_back</i></a>
                              </FormGroup>
                           </div>
                        </div>
                     </div>
                     <div className="row">

                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="invoice_start_date">Invoice date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_start_date"
                                 id="invoice_start_date"
                                 selected={this.state.invoice_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="invoice_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_end_date"
                                 id="invoice_end_date"
                                 selected={this.state.invoice_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_end_date', e)}
                              />
                           </FormGroup>
                        </div>

                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="invoice_due_start_date">Invoice Due Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_due_start_date"
                                 id="invoice_due_start_date"
                                 selected={this.state.invoice_due_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_due_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="invoice_due_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_due_end_date"
                                 id="invoice_due_end_date"
                                 selected={this.state.invoice_due_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_due_end_date', e)}
                              />
                           </FormGroup>
                        </div>




                     </div>
                  </Form>
               </div>
               <MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={(invoiceList) ? invoiceList : ''}
                     columns={columns}
                     options={options}


                  />
               </MuiThemeProvider>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   console.log(AdminReportReducer)
   const { loading, customerInvoiceList, statusList, providerList, customerInvoiceStatus } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, customerInvoiceList, statusList, providerList, customerInvoiceStatus }
}

export default connect(mapStateToProps, {
   customerWeekMonthInvoiceReport, customerInvoiceReportFilter, customerInvoiceReportDownload, customerInvoiceReportDownloadXLS, customerReminderEmail
})(ProviderInvoiceReports);