/**
 * User Management Page
 */
import React, { Component } from 'react';

import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";

import { connect } from 'react-redux';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import FormHelperText from '@material-ui/core/FormHelperText';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge,
   Button
} from 'reactstrap';




// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { isEmpty, isDecimals } from '../../validator/Validator';
import {
   invoiceAdminCloseList, viewInvoiceDetails, viewInvoiceNotes, cancelInvoiceConfirmations
} from 'Actions';

class openInvoiceList extends Component {

   state = {
      listInvoiceView: null, // initial user data
      viewInvoice: false,
      cancelInvoice: false,
      viewNote: false,
      cancelInvoiceID: null,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.props.invoiceAdminCloseList();
   }

   componentWillReceiveProps(nextProps) {

      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
   }

   viewInvoice(invoice_id, e) {
      this.props.viewInvoiceDetails(invoice_id);
      this.setState({ viewInvoice: true });
   }
   viewInvoiceNote(invoice_id, e) {
      this.props.viewInvoiceNotes(invoice_id);
      this.setState({ viewNote: true });
   }
   cancelInvoiceSubmit() {
      this.props.cancelInvoiceConfirmations(this.state.cancelInvoiceID);
      this.setState({ cancelInvoiceID: null, cancelInvoice: false });
   }
   cancelInvoice(invoice_id, e) {
      this.setState({ cancelInvoice: true, cancelInvoiceID: invoice_id });
   }




   /**
    * On View invoice details close pop up
    */
   viewInvoiceClose() {
      this.setState({ viewInvoice: false, cancelInvoice: false, cancelInvoiceID: null, viewNote: false });
   }

   componentWillReceiveProps(nextProps) {
      (nextProps.listInvoiceView) ? this.setState({ listInvoiceView: nextProps.listInvoiceView }) : '';
   }



   render() {
      //const invoiceList = this.props.invoiceList;
      const columns = [
         {
            name: 'Invoice ID',
            field: 'provider_invoice_id',
         },
         {
            name: 'Provider',
            field: 'name',
         },
         {
            name: 'Total $ Amount',
            field: 'total_amt',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + value.total_amt.toFixed(2)
                  )
               },
            }
         },
         {
            name: 'HPS Discount',
            field: 'total_hps_discount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + value.total_hps_discount.toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Date Created',
            field: 'date_created',
         },
         {
            name: 'Action',
            field: 'invoice_status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <div className="list-action">
                        <a href="javascript:void(0)" onClick={this.viewInvoiceNote.bind(this, value.provider_invoice_id)} title="Application Note"><i className="ti-notepad"></i></a>
                        <a href="javascript:void(0)" onClick={this.viewInvoice.bind(this, value.provider_invoice_id)} title="View Invoice"><i className="ti-eye"></i></a>
                        <a href="javascript:void(0)" onClick={this.cancelInvoice.bind(this, value.provider_invoice_id)} title="Remove Invoice"><i className="ti-close"></i></a>
                     </div>
                  )
               },
            }

         },



      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'submittedInvoice.csv' },
      };
      /*<Link to="/provider/invoice/preview-invoice" color="primary" className="caret btn-sm mr-10-custome">Create and Preview</Link>*/


      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.closeInvoice" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>
               <div className="table-responsive invoice_table">
                  {this.props.closeInvoiceList &&

                     <MaterialDatatable
                        data={this.props.closeInvoiceList}
                        columns={columns}
                        options={options}
                     />

                  }
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <Modal className="modal-dialog-invoice" isOpen={this.state.viewInvoice} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Invoice Details
               </ModalHeader>
               <ModalBody>
                  <div className="table-responsive">
                     <table className="table table-middle table-hover mb-0">
                        <thead>
                           <tr>
                              <th>A/C Number</th>
                              <th>Application No</th>
                              <th>First Name</th>
                              <th>Middle Name</th>
                              <th>Last Name</th>
                              <th>DOB</th>
                              <th>Phone</th>
                              <th>Address</th>
                              <th>City</th>
                              <th>State</th>
                              <th>Zip</th>
                              <th>Procedure Amount</th>
                              <th>Loan Amount</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.openInvoiceView && this.props.openInvoiceView.map((application, idx) => (
                              <tr key={idx}>
                                 <td>{application.patient_ac}</td>
                                 <td>{application.application_no}</td>
                                 <td>{application.f_name}</td>
                                 <td>{application.m_name}</td>
                                 <td>{application.l_name}</td>
                                 <td>{application.dob.toString()}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.City}</td>
                                 <td>{application.name}</td>
                                 <td>{application.zip_code}</td>
                                 <td>{application.procedure_amt}</td>
                                 <td>{application.loan_amount}</td>
                                 <td>Paid</td>
                              </tr>
                           ))

                           }

                        </tbody>

                     </table>
                  </div>
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.cancelInvoice} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Cancel Invoice
               </ModalHeader>
               <ModalBody>
                  Are you sure?
               </ModalBody>
               <ModalFooter>
                  <Button className="mr-5" color="primary" size="sm" onClick={this.cancelInvoiceSubmit.bind(this)}>Ok</Button>
               </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.viewNote} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Confirm Invoice Dialogue Box
               </ModalHeader>
               <ModalBody>
                  <div className="table-responsive">
                     <table className="table table-middle table-hover mb-0">
                        <tbody>
                           {this.props.noteDetails &&
                              <React.Fragment>
                                 <tr>
                                    <td>Check #</td>
                                    <td>{this.props.noteDetails[0].txn_no}</td>
                                    <td>Check Amount</td>
                                    <td>${this.props.noteDetails[0].check_amount}</td>
                                 </tr>
                                 <tr>
                                    <td>Bank Name</td>
                                    <td>{this.props.noteDetails[0].bank_name}</td>
                                    <td>Paid Date</td>
                                    <td>{this.props.noteDetails[0].date_paid}</td>
                                 </tr>
                                 <tr>
                                    <td>Invoice Note</td>
                                    <td colSpan="3">{this.props.noteDetails[0].comment}</td>
                                 </tr>
                              </React.Fragment>
                           }
                        </tbody>
                     </table>
                  </div>
               </ModalBody>
            </Modal>


         </div>
      );
   }
}

const mapStateToProps = ({ adminInvoice }) => {
   const { loading, closeInvoiceList, openInvoiceView, listInvoiceView, noteDetails } = adminInvoice;
   //console.log(noteDetails[0])
   return { loading, closeInvoiceList, openInvoiceView, listInvoiceView, noteDetails }

}

export default connect(mapStateToProps, {
   invoiceAdminCloseList, viewInvoiceDetails, viewInvoiceNotes, cancelInvoiceConfirmations
})(openInvoiceList);