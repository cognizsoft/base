/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty, formatPhoneNumber, isPhone } from '../../validator/Validator';
import {
   providerAccountPayableFilter, providerAccountPayableDownload, providerAccountPayableDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class ProviderInvoiceReports extends Component {

   state = {
      reportFilter: {
         filter_type: '',
         start_date: '',
         end_date: '',
         year: '',
         month: '',
         quarter: '',
      },
      start_date: '',
      end_date: '',
      widthTable: '',
      mainHeading: 'Accounts Payable'
   }


   componentDidMount() {
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //console.log(this.props.match.params.status_id)

      this.props.providerAccountPayableFilter(this.state.reportFilter);

   }


   onChangeReportFilter(key, value) {

      switch (key) {
         case 'start_date':
            if (value == null) {
               value = '';
               this.setState({ start_date: '' })
            } else {
               this.setState({ start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'end_date':
            if (value == null) {
               value = '';
               this.setState({ end_date: '' })
            } else {
               this.setState({ end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'provider_phone':
            if (isEmpty(value)) {
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
            } else {
               value = formatPhoneNumber(value)
            }
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      //console.log(this.state.reportFilter);
      this.props.providerAccountPayableDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      //console.log(this.state.reportFilter);
      this.props.providerAccountPayableFilter(this.state.reportFilter);
      const { reportFilter } = this.state;
      let fileName = '';
      if (reportFilter.filter_type == 1) {
         moment.updateLocale('en', {
            week: {
               dow: 1,
               doy: 1
            }
         });

         let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
         let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
         fileName = 'Weekly Accounts Payable Report (' + startOfWeek + ' - ' + endOfWeek + ')';
      } else if (reportFilter.filter_type == 2) {
         let monthName = moment().month(reportFilter.month - 1).format("MMM");
         fileName = 'Monthly Accounts Payable Report (' + monthName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 3) {
         let queterName = ''
         if (reportFilter.quarter == 1) {
            queterName = 'Q1';
         } else if (reportFilter.quarter == 2) {
            queterName = 'Q2';
         } else if (reportFilter.quarter == 3) {
            queterName = 'Q3';
         } else if (reportFilter.quarter == 4) {
            queterName = 'Q4';
         }
         fileName = 'Quarterly Accounts Payable Report (' + queterName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 4) {
         fileName = 'Yearly Accounts Payable Report (' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 5) {
         let currentDate = moment().format('MM/DD/YYYY');
         fileName = 'Year to Date Accounts Payable Report (01/01/' + reportFilter.year + '-' + currentDate + ')';
      } else if (reportFilter.filter_type == 6) {
         let loanStart = moment(reportFilter.loan_start_date).format('MM/DD/YYYY');
         let loanEnd = moment(reportFilter.loan_end_date).format('MM/DD/YYYY');
         fileName = 'By Date Accounts Payable Report (' + loanStart + '-' + loanEnd + ')';
      } else {
         fileName = this.state.mainHeading;
      }
      this.setState({ mainHeading: fileName })
   }

   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.providerAccountPayableDownloadXLS(this.state.reportFilter);
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {

               accumulator['procedure_amt'] = (accumulator['procedure_amt'] !== undefined) ? accumulator['procedure_amt'] + currentValue.procedure_amt : currentValue.procedure_amt;
               accumulator['hps_discount_amt'] = (accumulator['hps_discount_amt'] !== undefined) ? accumulator['hps_discount_amt'] + currentValue.hps_discount_amt : currentValue.hps_discount_amt;
               if (accumulator['total_due'] !== undefined) {
                  accumulator['total_due'] += (currentValue.mdv_invoice_status_id != 4) ? (parseFloat(currentValue.procedure_amt) - parseFloat(currentValue.hps_discount_amt)) : 0;
               } else {
                  accumulator['total_due'] = (currentValue.mdv_invoice_status_id != 4) ? (parseFloat(currentValue.procedure_amt) - parseFloat(currentValue.hps_discount_amt)) : 0;
               }

            }

            return accumulator
         }, []);
         return amount
      }
   }
   validateButton() {

      if (this.state.reportFilter.filter_type == 1 || this.state.reportFilter.filter_type == 5) {
         return true
      } else if (this.state.reportFilter.filter_type == 2) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.month
         )
      } else if (this.state.reportFilter.filter_type == 3) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.quarter
         )
      } else if (this.state.reportFilter.filter_type == 4) {
         return (
            this.state.reportFilter.year
         )
      } else if (this.state.reportFilter.filter_type == 6) {
         return (
            this.state.reportFilter.start_date &&
            this.state.reportFilter.end_date
         )
      } else {
         return false
      }

   }
   render() {
      let totalInvAmt = 0;
      let disAmt = 0;
      let payAmt = 0;
      let paideAmt = 0;
      let blcAmt = 0;
      let refAmt = 0;
      /*const columns = [

         {
            name: 'Loan No',
            field: 'plan_number'
         },
         {
            name: 'Provider Name',
            field: 'name',
         },
         {
            name: 'Customer Name',
            field: 'f_name',
            options: {
               customBodyRender: (value) => {
                  return (
                     value.f_name + ' ' + value.m_name + ' ' + value.l_name
                  );
               }
            }
         },
         {
            name: 'Loan Date',
            field: 'procedure_date'
         },
         {
            name: 'Principal Amt',
            field: 'procedure_amt',
            options: {
               customBodyRender: (value) => {
                  return (
                     '$' + parseFloat(value.procedure_amt).toFixed(2)
                  );
               }
            }
         },
         {
            name: 'Discount Amt',
            field: 'hps_discount_amt',
            options: {
               customBodyRender: (value) => {
                  return (
                     '$' + parseFloat(value.hps_discount_amt).toFixed(2)
                  );
               }
            }
         },
         {
            name: 'Total Due',
            field: 'check_amount',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.mdv_invoice_status_id != 4) ? '$' + (parseFloat(value.procedure_amt) - parseFloat(value.hps_discount_amt)).toFixed(2) : '$0.00'
                  );
               }
            }
         },
         {
            name: 'Refund Amt',
            field: 'refund_recived',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.iou_flag != 1) ? (value.refund_recived != null) ? '$' + parseFloat(value.refund_recived).toFixed(2) : '$0.00' : '$' + parseFloat(value.refund_due).toFixed(2)
                  );
               }
            }
         },


      ];
      const footerStyle = {
         display: 'flex',
         justifyContent: 'flex-end',
         padding: '0px 24px 0px 24px'
      };
      const options = {
         //filterType: 'dropdown',
         selectableRows: false,
         filter: false,
         search: false,
         print: false,
         download: false,
         viewColumns: false,

         customToolbar: false,
         customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            rowsSelected
         ) => {
            const invoiceSubtotal = this.totalLoanAmount(this.props.providerPayableList, page, rowsPerPage);
            return (

               <TableFooter>
                  <TableRow>
                     <TableCell style={footerStyle} ><b>Total Principal Amt :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.procedure_amt !== null) ? invoiceSubtotal.procedure_amt.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Discount Amt :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.hps_discount_amt !== null) ? invoiceSubtotal.hps_discount_amt.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                     <TableCell style={footerStyle} ><b>Total Due Amt :- ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.total_due !== null) ? invoiceSubtotal.total_due.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                  </TableRow>

                  <TableRow>
                     <TablePagination
                        count={count}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={(_, page) => changePage(page)}
                        onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                        rowsPerPageOptions={[10, 20, 50, 100]}
                     />
                  </TableRow>
               </TableFooter>
            );
         }

      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
         }
      });*/
      let currentYear = new Date();
      let currentMonth = currentYear.getMonth() + 1;
      currentYear = currentYear.getFullYear();
      const yearList = [];
      if (this.props.yearStart != '') {
         for (var i = currentYear; i >= this.props.yearStart; i--) {
            yearList.push({ id: i, value: i });
         }
      } else {
         for (var i = currentYear; i >= (currentYear - 5); i--) {
            yearList.push({ id: i, value: i });
         }
      }

      return (
         <div className="admin-reports-pro-invoice-report">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={this.state.mainHeading}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="filter_type">Select options</Label>
                              <Input
                                 type="select"
                                 name="filter_type"
                                 id="filter_type"
                                 onChange={(e) => this.onChangeReportFilter('filter_type', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Current Week</option>
                                 <option value="2">Monthly</option>
                                 <option value="3">Quarterly</option>
                                 <option value="4">Yearly</option>
                                 <option value="5">Year to date</option>
                                 <option value="6">By Date</option>
                              </Input>

                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2 || this.state.reportFilter.filter_type == 3 || this.state.reportFilter.filter_type == 4) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="year">Year</Label>
                              <Input
                                 type="select"
                                 name="year"
                                 id="year"
                                 onChange={(e) => this.onChangeReportFilter('year', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {yearList && yearList.map((data, key) => (
                                    <option value={data.id} key={key}>{data.value}</option>
                                 ))}
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="month">Month</Label>
                              <Input
                                 type="select"
                                 name="month"
                                 id="month"
                                 onChange={(e) => this.onChangeReportFilter('month', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1" disabled={(currentMonth < 1 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jan</option>
                                 <option value="2" disabled={(currentMonth < 2 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Feb</option>
                                 <option value="3" disabled={(currentMonth < 3 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Mar</option>
                                 <option value="4" disabled={(currentMonth < 4 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Apr</option>
                                 <option value="5" disabled={(currentMonth < 5 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>May</option>
                                 <option value="6" disabled={(currentMonth < 6 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jun</option>
                                 <option value="7" disabled={(currentMonth < 7 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jul</option>
                                 <option value="8" disabled={(currentMonth < 8 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Aug</option>
                                 <option value="9" disabled={(currentMonth < 9 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Sep</option>
                                 <option value="10" disabled={(currentMonth < 10 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Oct</option>
                                 <option value="11" disabled={(currentMonth < 11 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Nov</option>
                                 <option value="12" disabled={(currentMonth < 12 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Dec</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 3) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="quarter">Quarterly</Label>
                              <Input
                                 type="select"
                                 name="quarter"
                                 id="quarter"
                                 onChange={(e) => this.onChangeReportFilter('quarter', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Q1</option>
                                 <option value="2">Q2</option>
                                 <option value="3">Q3</option>
                                 <option value="4">Q4</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="start_date">Start Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="start_date"
                                 id="start_date"
                                 selected={this.state.start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="end_date">End Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="end_date"
                                 id="end_date"
                                 selected={this.state.end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('end_date', e)}
                              />
                           </FormGroup>
                        </div>


                        <div className="col-md-2">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="loan_end_date">&nbsp;</Label>
                                 {this.validateButton() &&

                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down custom_icon"></i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                                 }
                                 {!this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down_disable custom_icon"></i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf_disable">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls_disable">insert_drive_file</i></a>
                                    </span>
                                 }
                              </FormGroup>
                           </div>

                        </div>


                     </div>
                  </Form>
                  <div className="table-responsive">
                     <table className="table table-bordered table-sm">
                        <thead>
                           <tr>
                              <th>Loan No</th>
                              <th>Provider Name</th>
                              <th>Customer Name</th>
                              <th>Loan Date</th>
                              <th>Total Amt Invoiced</th>
                              <th>Total Discount Amt</th>
                              <th>Total Amt payable</th>
                              <th>Total Amt paid</th>
                              <th>Balance Amt Payable</th>
                              <th>Refund Amt</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.providerPayableList && this.props.providerPayableList.map(function (row, idx) {
                              totalInvAmt = totalInvAmt + row.procedure_amt;
                              disAmt = disAmt + row.hps_discount_amt;
                              payAmt = payAmt + (parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt));
                              let chckAmt = (row.check_amount != null) ? row.check_amount : 0;
                              if(row.mdv_invoice_status_id == 4){
                                 paideAmt = paideAmt + (parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt));
                              }
                              
                              blcAmt = blcAmt + (row.mdv_invoice_status_id != 4) ? (parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt)).toFixed(2) : 0;
                              let singRefAmt = (row.iou_flag != 1) ? (row.refund_recived != null) ? parseFloat(row.refund_recived) : 0 : parseFloat(row.refund_due);
                              refAmt = refAmt + singRefAmt;
                              return <tr key={idx}>
                                 <td>
                                    {row.plan_number}
                                 </td>
                                 <td>{row.name}</td>
                                 <td>{row.f_name + ' ' + row.m_name + ' ' + row.l_name}</td>
                                 <td>{row.procedure_date}</td>
                                 <td className="text-right">{(row.procedure_amt) ? '$' + row.procedure_amt.toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.hps_discount_amt) ? '$' + (row.hps_discount_amt).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.procedure_amt) ? '$' + (parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt)).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">${(row.mdv_invoice_status_id == 4)?(parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt)).toFixed(2):'0.00'}</td>
                                 <td className="text-right">{(row.mdv_invoice_status_id != 4) ? '$' + (parseFloat(row.procedure_amt) - parseFloat(row.hps_discount_amt)).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{parseFloat(singRefAmt).toFixed(2)}</td>
                                 <td>{(row.inv_status != null) ? row.inv_status : '-'}</td>
                              </tr>
                           }.bind(this))
                           }
                           {(this.props.providerPayableList === undefined || this.props.providerPayableList.length == 0) &&
                              <tr>
                                 <td colSpan="15">Sorry, no matching records found</td>
                              </tr>
                           }
                        </tbody>
                        <tfoot>
                           <tr>
                              <th colSpan="4">Total</th>
                              <th className="text-right">${parseFloat(totalInvAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(disAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(payAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(paideAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(blcAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(refAmt).toFixed(2)}</th>
                              <th></th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               {/*<MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={this.props.providerPayableList}
                     columns={columns}
                     options={options}


                  />
               </MuiThemeProvider>
               */}
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   //console.log(AdminReportReducer)

   const { loading, providerPayableList, statusList, providerList, yearStart } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, providerPayableList, statusList, providerList, yearStart }

}

export default connect(mapStateToProps, {
   providerAccountPayableFilter, providerAccountPayableDownload, providerAccountPayableDownloadXLS
})(ProviderInvoiceReports);