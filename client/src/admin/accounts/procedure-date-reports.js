/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import CryptoJS from 'crypto-js';
import {
   customerWithdrawalReport, customerProcedureReportFilter, customerProcedureReportDownload, customerProcedureReportDownloadXLS, customerProcedureReportEmail, customerProcedureSingleReportEmail,
} from 'Actions';
import { Link } from 'react-router-dom';
//import AddNewButton from './AddNewButton';

class ProviderInvoiceReports extends Component {

   state = {
      currentModule: 19,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      reportFilter: {
         provider_name: '',
         invoice_start_date: '',
         invoice_end_date: '',
      },
      invoice_start_date: '',
      invoice_end_date: '',
      widthTable: '',
      current_week: 0,
   }


   componentDidMount() {
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //this.props.customerWeekMonthInvoiceReport();
      this.permissionFilter(this.state.currentModule);

      this.props.customerProcedureReportFilter(this.state.reportFilter);
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   onChangeReportFilter(key, value) {
      const { reportFilter } = this.state;
      switch (key) {
         case 'provider_name':
            reportFilter['provider_name'] = value;
            break;
         case 'invoice_start_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_start_date: '' })
            } else {
               this.setState({ invoice_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
               reportFilter['invoice_start_date'] = value;
            }
            break;
         case 'invoice_end_date':
            if (value == null) {
               value = '';
               this.setState({ invoice_end_date: '' })
            } else {
               this.setState({ invoice_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
               reportFilter['invoice_end_date'] = value;
            }
            break;
         case 'current_week':
            value = (this.state.current_week) ? 0 : 1;
            this.setState({ current_week: value })
            if (value == 1) {
               var current = new Date();     // get current date    
               var weekstart = current.getDate() - current.getDay() + 1;
               var weekend = weekstart + 6;       // end day is the first day + 6 
               var monday = new Date(current.setDate(weekstart));
               var sunday = new Date(current.setDate(weekend));

               this.setState({ invoice_start_date: monday })
               monday = moment(monday).format('YYYY-MM-DD');
               reportFilter['invoice_start_date'] = monday;

               this.setState({ invoice_end_date: sunday })
               sunday = moment(sunday).format('YYYY-MM-DD');
               reportFilter['invoice_end_date'] = sunday;
            }
            break;


      }
      this.setState({
         reportFilter: reportFilter
      });
   }


   fullReportDownload() {
      this.props.customerProcedureReportDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      this.props.customerProcedureReportFilter(this.state.reportFilter);
   }

   fullReportDownloadXLS() {
      this.props.customerProcedureReportDownloadXLS(this.state.reportFilter);
   }
   fullReportEmail() {
      this.props.customerProcedureReportEmail(this.state.reportFilter);
   }
   singleReportEmail(id) {
      //console.log(id)
      this.props.customerProcedureSingleReportEmail(id);
   }
   goBack() {
      this.props.history.goBack(-1)
   }

   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {
               var newLateFin = 0;
               var newLateFee = 0;
               if (currentValue.finpct != null) {
                  if ((currentValue.previous_fin_charge != null)) {
                     newLateFin = (((parseFloat(currentValue.previous_fin_charge) + parseFloat(currentValue.fin_charge_amt)) - (parseFloat(currentValue.finpct) * (parseFloat(currentValue.previous_fin_charge) + parseFloat(currentValue.fin_charge_amt))) / 100)).toFixed(2);
                  } else {
                     newLateFin = (currentValue.fin_charge_amt != null) ? ((parseFloat(currentValue.fin_charge_amt) - (parseFloat(currentValue.finpct) * parseFloat(currentValue.fin_charge_amt)) / 100)).toFixed(2) : 0;
                  }
               } else {
                  if ((currentValue.previous_fin_charge != null)) {
                     newLateFin = parseFloat(currentValue.previous_fin_charge) + parseFloat(currentValue.fin_charge_amt);
                  } else {
                     newLateFin = (currentValue.fin_charge_amt != null) ? parseFloat(currentValue.fin_charge_amt) : 0;
                  }
               }
               // get late feee
               if (currentValue.latepct != null) {
                  if ((currentValue.previous_late_fee != null)) {
                     newLateFee = (((parseFloat(currentValue.previous_late_fee) + parseFloat(currentValue.late_fee_received)) - (parseFloat(currentValue.latepct) * (parseFloat(currentValue.previous_late_fee) + parseFloat(currentValue.late_fee_received))) / 100)).toFixed(2);
                  } else {
                     newLateFee = (currentValue.late_fee_received != null) ? ((parseFloat(currentValue.late_fee_received) - (parseFloat(currentValue.latepct) * parseFloat(currentValue.late_fee_received)) / 100)).toFixed(2) : 0;
                  }
               } else {
                  if ((currentValue.previous_late_fee != null)) {
                     newLateFee = parseFloat(currentValue.previous_late_fee) + parseFloat(currentValue.late_fee_received);
                  } else {
                     newLateFee = (currentValue.previous_late_fee != null) ? parseFloat(currentValue.previous_late_fee) : 0;
                  }
               }
               var totalAmt = (parseFloat(newLateFee) + parseFloat(newLateFin) + parseFloat(currentValue.payment_amount)).toFixed(2)



               accumulator['payment_amount'] = (accumulator['payment_amount'] !== undefined) ? accumulator['payment_amount'] + parseFloat(totalAmt) : parseFloat(totalAmt);
            }

            return accumulator
         }, []);
         return amount
      }
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }
   render() {
      let totalAmt = 0;
      return (
         <div className="admin-reports-week-month">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.procedure-date-reports" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="provider_name">Provider Name</Label>
                              <Input
                                 type="text"
                                 name="provider_name"
                                 id="provider_name"
                                 placeholder="Provider Name"
                                 onChange={(e) => this.onChangeReportFilter('provider_name', e.target.value)}
                              >

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_start_date">From date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_start_date"
                                 id="invoice_start_date"
                                 selected={this.state.invoice_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="invoice_end_date">To Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="invoice_end_date"
                                 id="invoice_end_date"
                                 selected={this.state.invoice_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('invoice_end_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <Label for="current_week">&nbsp;</Label>
                           <FormGroup check>
                              <Label check>
                                 <Input
                                    type="checkbox"
                                    checked={(this.state.current_week == 1) ? true : false}
                                    value={(this.state.current_week != '') ? this.state.current_week : ''}
                                    name="current_week"
                                    onChange={(e) => this.onChangeReportFilter('current_week', e)}
                                 />
                                 Current Week
                              </Label>

                           </FormGroup>
                        </div>
                        <div className="col-md-4">
                           <div className="list-action text-right search_down_btn">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>
                                 <span>
                                    <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10 btn_down custom_icon"></i></a>
                                    <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                    <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    <a href="javascript:void(0)" onClick={this.fullReportEmail.bind(this)} className="report-download" title="Email"><i className="material-icons mr-10 mt-10 btn_xls">email</i></a>
                                    <a href="javascript:void(0);" onClick={this.goBack.bind(this)} title="Back"><i className="mr-10 mt-10 material-icons">arrow_back</i></a>
                                 </span>
                              </FormGroup>
                           </div>
                        </div>
                     </div>

                  </Form>
                  <div className="table-responsive">
                     <table className="table table-bordered table-sm">
                        <thead>
                           <tr>
                              <th>Provider A/C</th>
                              <th>Provider Name</th>
                              <th>Plan No.</th>
                              <th>Plan Amount</th>
                              <th>Customer Name</th>
                              <th>Address</th>
                              <th>City</th>
                              <th>State</th>
                              <th>Zip</th>
                              <th>Phone</th>
                              <th>Procedure date</th>
                              <th>Procedure Status</th>
                              <th>Email</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.procedureDateList && this.props.procedureDateList.map(function (row, idx) {
                              totalAmt = totalAmt + row.amount;
                              return <tr key={idx}>
                                 <td>
                                    {row.provider_ac}
                                 </td>
                                 <td>{row.name}</td>
                                 <td>{row.plan_number}</td>
                                 <td className="text-right">{parseFloat(row.amount).toFixed(2)}</td>
                                 <td>{row.customer_f_name + ' ' + row.customer_m_name + ' ' + row.customer_l_name}</td>
                                 <td>{row.address1}</td>
                                 <td>{row.city}</td>
                                 <td>{row.state_name}</td>
                                 <td>{row.zip_code}</td>
                                 <td>{row.phone}</td>
                                 <td>{row.procedure_date}</td>
                                 <td>{row.plan_status}</td>
                                 <td>{row.email}</td>
                                 <td>
                                    <React.Fragment>
                                       <span className="list-action">
                                          {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={this.singleReportEmail.bind(this, row.pp_id)} className="report-download" title="Email"><i className="zmdi zmdi-email icon-fr"></i></a> : ''}
                                       </span>
                                    </React.Fragment>
                                 </td>
                              </tr>
                           }.bind(this))
                           }
                           {(this.props.procedureDateList === undefined || this.props.procedureDateList.length == 0) &&
                              <tr>
                                 <td colSpan="15">Sorry, no matching records found</td>
                              </tr>
                           }
                        </tbody>
                        <tfoot>
                           <tr>
                              <th colSpan="3">Total</th>
                              <th className="text-right">${parseFloat(totalAmt).toFixed(2)}</th>
                              <th colSpan="11"></th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   const { loading, procedureDateList, statusList, providerList } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, procedureDateList, statusList, providerList }
}

export default connect(mapStateToProps, {
   customerWithdrawalReport, customerProcedureReportFilter, customerProcedureReportDownload, customerProcedureReportDownloadXLS, customerProcedureReportEmail, customerProcedureSingleReportEmail,
})(ProviderInvoiceReports);