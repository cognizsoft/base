/**
 * User Management Page
 */
import React, { Component } from 'react';

import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import FormHelperText from '@material-ui/core/FormHelperText';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge,
   Button
} from 'reactstrap';




// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import moment from 'moment';
import { isEmpty, isDecimals, isNumeric } from '../../validator/Validator';
import {
   invoiceAdminOpenList, viewInvoiceDetails, deleteInvoiceApplication, listInvoiceApplication, addInvoiceApplication, cancelInvoiceApplication, confirmInvoicesSubmit
} from 'Actions';

class openInvoiceList extends Component {

   state = {
      currentModule: 20,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      listInvoiceView: null, // initial user data
      viewInvoice: false,
      addApplication: false,
      cancelInvoice: false,
      allSelected: false,
      selectedApp: 0,
      cancelInvoiceID: null,
      confirmInvoiceID: null,
      confirmInvoice: false,
      commentNote: '',
      paidDate: '',
      errorConfirm: {},
      paymentConfirm: {
         check_number: "",
         amount: "",
         bank_name: "",
         paid_date: "",
         comment: "",
         iouAmt: null,
         refund_id: null,
         provider_id: null,
         amount_due: null,
         adjusted_amt: null,
         invoiceAmt: null,
         discountAmt: null,
         afterAdjDue: null,
         remainingIOU: null,
      },
      currentIOUDET: 0,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.invoiceAdminOpenList();
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   componentWillReceiveProps(nextProps) {
      (nextProps.listInvoiceView) ? this.setState({ listInvoiceView: nextProps.listInvoiceView }) : '';
      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
   }
   confirmInvoice(invoice_id) {
      let { errorConfirm, paymentConfirm } = this.state;
      this.setState({ confirmInvoiceID: invoice_id, confirmInvoice: true })
      var activeInvoice = (this.props.openInvoiceList) ? this.props.openInvoiceList.filter(x => x.provider_invoice_id == invoice_id) : '';
      if (activeInvoice[0].provider_invoice_id == invoice_id) {
         let iouDet = (this.props.iouDetails) ? this.props.iouDetails.filter(x => x.provider_id == activeInvoice[0].provider_id) : '';
         console.log(iouDet)
         let currentIOUDET = 0;
         if (iouDet.length > 0) {
            currentIOUDET = (iouDet[0].iou_paid_amount == null) ? iouDet[0].refund_due : iouDet[0].refund_due - iouDet[0].iou_paid_amount;
            paymentConfirm.refund_id = iouDet[0].refund_id;
         }
         paymentConfirm.invoiceAmt = activeInvoice[0].total_amt,
            paymentConfirm.discountAmt = activeInvoice[0].total_hps_discount,
            //paymentConfirm.amount = activeInvoice[0].total_amt - activeInvoice[0].total_hps_discount - currentIOUDET;
            paymentConfirm.provider_id = activeInvoice[0].provider_id;
         paymentConfirm.amount = activeInvoice[0].total_amt - activeInvoice[0].total_hps_discount;
         paymentConfirm.amount_due = paymentConfirm.amount;
         paymentConfirm.iouAmt = (paymentConfirm.amount > currentIOUDET) ? currentIOUDET : paymentConfirm.amount;
         //paymentConfirm.adjusted_amt = (paymentConfirm.amount > currentIOUDET) ? currentIOUDET : paymentConfirm.amount;
         if (currentIOUDET == 0) {
            paymentConfirm.adjusted_amt = 0.00;
         } else if (paymentConfirm.amount >= currentIOUDET) {
            paymentConfirm.adjusted_amt = currentIOUDET;
         } else {
            paymentConfirm.adjusted_amt = paymentConfirm.amount;
         }


         paymentConfirm.amount = paymentConfirm.amount - currentIOUDET;
         paymentConfirm.amount = (paymentConfirm.amount > 0) ? paymentConfirm.amount : '0.00';

         if (paymentConfirm.amount > 0) {
            paymentConfirm.remainingIOU = 0;
         } else {
            paymentConfirm.remainingIOU = currentIOUDET - (activeInvoice[0].total_amt - activeInvoice[0].total_hps_discount);
         }
         paymentConfirm.bank_name = activeInvoice[0].bank_name;
         errorConfirm.amount = '';
         this.setState({ paymentConfirm: paymentConfirm, errorConfirm: errorConfirm, currentIOUDET: currentIOUDET })
      }
   }
   confirmInvoiceSubmit() {
      this.state.paymentConfirm.invoice_id = this.state.confirmInvoiceID;

      this.props.confirmInvoicesSubmit(this.state.paymentConfirm);
      let paymentConfirm = {
         check_number: "",
         amount: "",
         bank_name: "",
         paid_date: "",
         comment: "",
      }
      this.setState({ confirmInvoice: false, paidDate: '', confirmInvoiceID: null, paymentConfirm: paymentConfirm, errorConfirm: {} });
   }
   viewInvoice(invoice_id, e) {
      this.props.viewInvoiceDetails(invoice_id);
      this.setState({ viewInvoice: true });
   }
   cancelInvoiceSubmit() {
      this.props.cancelInvoiceApplication(this.state.cancelInvoiceID, this.state.commentNote);
      this.setState({ cancelInvoiceID: null, cancelInvoice: false });
   }
   cancelInvoice(invoice_id, e) {
      this.setState({ cancelInvoice: true, cancelInvoiceID: invoice_id });
   }
   deleteInvoiceApp(application_id, invoice_id, provider_id, e) {
      this.props.deleteInvoiceApplication(application_id, invoice_id, provider_id);
   }
   listApplication(invoice_id, provider_id, e) {
      this.props.listInvoiceApplication(provider_id, invoice_id);
      this.setState({ addApplication: true });
   }

   addInvoiceApp() {
      var applicationId = this.state.listInvoiceView.reduce(function (accumulator, currentValue) {
         if (currentValue.checked === true) {
            accumulator.push(currentValue.application_id);
         }
         return accumulator
      }, []);

      if (applicationId.length > 0) {
         this.props.addInvoiceApplication(applicationId, this.props.currentInvoiceID);
         this.setState({ addApplication: false, selectedApp: 0 });
      }
   }
   /**
    * On View invoice details close pop up
    */
   viewInvoiceClose() {
      let paymentConfirm = {
         check_number: "",
         amount: "",
         bank_name: "",
         paid_date: "",
         comment: "",
      }
      this.setState({ viewInvoice: false, addApplication: false, cancelInvoice: false, cancelInvoiceID: null, selectedApp: 0, confirmInvoiceID: null, confirmInvoice: false, paidDate: '', confirmInvoiceID: null, paymentConfirm: paymentConfirm, errorConfirm: {} });
   }

   /*componentWillReceiveProps(nextProps) {
      (nextProps.listInvoiceView) ? this.setState({ listInvoiceView: nextProps.listInvoiceView }) : '';
   }*/
   //Select All user
   onSelectAllApp(e) {
      const { selectedApp, listInvoiceView } = this.state;
      let selectAll = selectedApp < listInvoiceView.length;
      if (selectAll) {
         let selectAllInvoices = listInvoiceView.map(invoice => {
            invoice.checked = (invoice.procedure_status == 0) ? false : true
            return invoice
         });
         this.setState({ listInvoiceView: selectAllInvoices, selectedApp: selectAllInvoices.length })
      } else {
         let unselectedApp = listInvoiceView.map(invoice => {
            invoice.checked = false
            return invoice;
         });
         this.setState({ selectedApp: 0, listInvoiceView: unselectedApp });
      }
   }
   /**
	 * On Select User
	 */
   onSelectApp(application) {
      application.checked = !application.checked;
      let selectedApp = 0;
      let listInvoiceView = this.state.listInvoiceView.map(appData => {
         if (appData.application_id === application.application_id) {
            if (appData.checked && appData.checked !== undefined) {
               selectedApp++;
            }
            return application;
         } else {
            if (appData.checked && appData.checked !== undefined) {
               selectedApp++;
            }
            return appData;
         }
      });
      this.setState({ listInvoiceView, selectedApp });
   }
   onChnageConfirm(key, value) {

      let { errorConfirm } = this.state;
      switch (key) {
         case 'check_number':
            if (isEmpty(value)) {
               errorConfirm[key] = "Check number can't be blank";
            } else if (isNumeric(value)) {
               dataError[key] = "Allow only numeric.";
            } else {
               errorConfirm[key] = '';
            }
            break;
         case 'amount':
            if (isEmpty(value)) {
               errorConfirm[key] = "Amount can't be blank";
            } else if (!isDecimals(value)) {
               errorConfirm[key] = "Amount not valid";
            } else {
               errorConfirm[key] = '';
            }
            break;
         case 'bank_name':
            if (isEmpty(value)) {
               errorConfirm[key] = "Bank Name can't be blank";
            } else {
               errorConfirm[key] = '';
            }
            break;
         case 'comment':
            if (isEmpty(value)) {
               errorConfirm[key] = "Comment can't be blank";
            } else {
               errorConfirm[key] = '';
            }
            break;
         case 'paid_date':
            if (value == null) {
               errorConfirm[key] = "Select Paid Date";
               this.setState({ paidDate: '' })
            } else {
               this.setState({ paidDate: value })
               value = moment(value).format('YYYY-MM-DD');
               errorConfirm[key] = '';
            }
            break;
      }
      this.setState({
         paymentConfirm: {
            ...this.state.paymentConfirm,
            [key]: value
         }
      });
      this.setState({ errorConfirm: errorConfirm });

   }
   validateSubmit() {
      if (this.state.paymentConfirm.amount > 0) {
         return (
            this.state.errorConfirm.check_number === '' &&
            this.state.errorConfirm.amount === '' &&
            this.state.errorConfirm.bank_name === '' &&
            this.state.errorConfirm.paid_date === '' &&
            this.state.errorConfirm.comment === ''
         )
      } else {
         return (
            this.state.errorConfirm.amount === '' &&
            this.state.errorConfirm.paid_date === '' &&
            this.state.errorConfirm.comment === ''
         )
      }

   }
   validateInvoiceSubmit() {
      return (this.state.add_err === '');
   }
   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'commentNote':
            if (isEmpty(value)) {
               add_err = "Comment can't be blank";
            } else {
               add_err = '';
            }
            break;
      }
      this.setState({ commentNote: value });
      this.setState({ add_err: add_err });
   }
   render() {
      const { selectedApp, listInvoiceView } = this.state;
      //const invoiceList = this.props.invoiceList;
      const columns = [
         {
            name: 'Invoice ID',
            field: 'invoice_number',
         },
         {
            name: 'Provider',
            field: 'name',
         },
         { name: 'Phone', field: 'primary_phone', },
         {
            name: 'Address',
            field: 'address1',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     value.address1 + ' ' + value.address2
                  )
               },
            }
         },
         { name: 'City', field: 'city', },
         /*{
            name: 'County',
            field: 'county',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.county) ? value.county : '-'
                  )
               },
            }
         },
         { name: 'Regions', field: 'regions_name' },*/
         { name: 'State', field: 'state_name', },
         { name: 'Country', field: 'country_name', },
         { name: 'Zip', field: 'zip_code', },
         {
            name: 'Total Amt',
            field: 'total_amt',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.total_amt).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'HPS Discount',
            field: 'total_hps_discount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.total_hps_discount).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Total Due',
            field: 'total_hps_discount',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.total_amt.toFixed(2) - value.total_hps_discount.toFixed(2)).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Date Created',
            field: 'date_created',
         },
         {
            name: 'Status',
            field: 'invoice_status',
         },
         {
            name: 'Action',
            field: 'invoice_status',
            options: {
               filter: false,
               short: false,
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {

                  if (value.mdv_invoice_status_id == 1) {
                     return (
                        <div className="list-action">
                           {/*<a href="javascript:void(0)" onClick={this.viewInvoice.bind(this, value.provider_invoice_id)} title="View Invoice"><i className="ti-eye"></i></a>*/}
                           {(this.state.currentPermision.view) ? <NavLink to={`/admin/accounts/view-invoice/${value.provider_invoice_id}`} title="View Invoice">
                           <img src="http://getdrawings.com/free-icon-bw/accounts-receivable-icon-1.png" width="20" />
                           </NavLink> : ''}
                           {/*<a href="javascript:void(0)" onClick={this.listApplication.bind(this, value.provider_invoice_id, value.provider_id)} title="Add Application"><i className="ti-plus"></i></a>*/}
                           {/*(this.state.currentPermision.delete) ? <a href="javascript:void(0)" onClick={this.cancelInvoice.bind(this, value.provider_invoice_id)} title="Reject Invoice"><i className="ti-close"></i></a> : ''*/}

                        </div>
                     )
                  } else if (value.mdv_invoice_status_id == 3) {
                     return (
                        <div className="list-action">
                           {/*<a href="javascript:void(0)" onClick={this.viewInvoice.bind(this, value.provider_invoice_id)} title="View Invoice"><i className="ti-eye"></i></a>*/}
                           <a href="javascript:void(0)" onClick={this.confirmInvoice.bind(this, value.provider_invoice_id)} title="Confirm Payment"><img src="http://getdrawings.com/free-icon-bw/accounts-receivable-icon-1.png" width="20" /></a>
                           {(this.state.currentPermision.view) ? <NavLink to={`/admin/accounts/view-invoice/${value.provider_invoice_id}`} title="View Invoice">
                           <i className="ti-eye"></i>
                           </NavLink> : ''}
                        </div>
                     )
                  } else if (value.mdv_invoice_status_id == 2) {
                     return (
                        <div className="list-action">
                           -
                        </div>
                     )
                  } else {
                     return (
                        <div className="list-action">
                           {(this.state.currentPermision.view) ? <NavLink to={`/admin/provider-invoice-reports/view/${value.provider_invoice_id}/${value.provider_id}`} title="View Invoice">
                              <i className="ti-eye"></i>
                           </NavLink> : ''}
                        </div>
                     )
                  }

               },
            }

         },



      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'submittedInvoice.csv' },
      };
      /*<Link to="/provider/invoice/preview-invoice" color="primary" className="caret btn-sm mr-10-custome">Create and Preview</Link>*/


      return (
         <div className="credit-application admin-account-open-invoice">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.openInvoice" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>
               <div className="table-responsive invoice_table">
                  {this.props.openInvoiceList &&

                     <MaterialDatatable
                        data={this.props.openInvoiceList}
                        columns={columns}
                        options={options}
                     />

                  }
               </div>
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <Modal className="modal-dialog-invoice" isOpen={this.state.viewInvoice} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Invoice Details
               </ModalHeader>
               <ModalBody>
                  <div className="table-responsive">
                     <table className="table table-middle table-hover mb-0">
                        <thead>
                           <tr>
                              <th>A/C Number</th>
                              <th>Application No</th>
                              <th>First Name</th>
                              <th>Middle Name</th>
                              <th>Last Name</th>
                              <th>DOB</th>
                              <th>Phone</th>
                              <th>Address</th>
                              <th>City</th>
                              <th>State</th>
                              <th>Zip</th>
                              <th>Procedure Amount</th>
                              <th>Loan Amount</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.openInvoiceView && this.props.openInvoiceView.map((application, idx) => (
                              <tr key={idx}>
                                 <td>{application.patient_ac}</td>
                                 <td>{application.application_no}</td>
                                 <td>{application.f_name}</td>
                                 <td>{application.m_name}</td>
                                 <td>{application.l_name}</td>
                                 <td>{application.dob.toString()}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.City}</td>
                                 <td>{application.name}</td>
                                 <td>{application.zip_code}</td>
                                 <td>{application.procedure_amt}</td>
                                 <td>{application.loan_amount}</td>
                                 <td>Submitted</td>
                                 <td><a href="javascript:void(0)" onClick={this.deleteInvoiceApp.bind(this, application.application_id, application.provider_invoice_id, application.provider_id)} title="Remove Application"><i className="ti-close"></i></a></td>
                              </tr>
                           ))

                           }

                        </tbody>

                     </table>
                  </div>
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>

            <Modal className="modal-dialog-invoice" isOpen={this.state.addApplication} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Add Application
               </ModalHeader>
               <ModalBody>
                  <div className="table-responsive">
                     <table className="table table-middle table-hover mb-0">
                        <thead>
                           <tr>
                              <th><FormControlLabel
                                 control={
                                    <Checkbox
                                       indeterminate={selectedApp > 0 && selectedApp < listInvoiceView.length}
                                       checked={selectedApp > 0}
                                       onChange={(e) => this.onSelectAllApp(e)}
                                       value="all"
                                       color="primary"
                                    />
                                 }
                                 label="All"
                              /></th>
                              <th>A/C Number</th>
                              <th>Application No</th>
                              <th>First Name</th>
                              <th>Middle Name</th>
                              <th>Last Name</th>
                              <th>DOB</th>
                              <th>Phone</th>
                              <th>Address</th>
                              <th>City</th>
                              <th>State</th>
                              <th>Zip</th>
                              <th>Procedure Amount</th>
                              <th>Loan Amount</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           {listInvoiceView && listInvoiceView.map((application, idx) => (
                              <tr key={idx}>
                                 <td>
                                    <FormControlLabel
                                       control={
                                          <Checkbox
                                             checked={(application.checked) ? true : false}
                                             disabled={(application.procedure_status == 0) ? true : false}
                                             onChange={() => this.onSelectApp(application)}
                                             color="primary"
                                          />
                                       }
                                    />
                                 </td>
                                 <td>{application.patient_ac}</td>
                                 <td>{application.application_no}</td>
                                 <td>{application.f_name}</td>
                                 <td>{application.m_name}</td>
                                 <td>{application.l_name}</td>
                                 <td>{application.dob.toString()}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.peimary_phone}</td>
                                 <td>{application.City}</td>
                                 <td>{application.name}</td>
                                 <td>{application.zip_code}</td>
                                 <td>{application.procedure_amt}</td>
                                 <td>{application.loan_amount}</td>
                                 <td>{(application.procedure_status == 1) ? 'Pending' : 'Inactive'}</td>

                              </tr>
                           ))

                           }

                        </tbody>

                     </table>
                  </div>
               </ModalBody>
               <ModalFooter>
                  <Button className="mr-5" color="primary" size="sm" onClick={this.addInvoiceApp.bind(this)}>Add</Button>
               </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.cancelInvoice} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Are you sure you want to reject this invoice?
               </ModalHeader>
               <ModalBody>
                  <div className="row">
                     <div className="col-md-12">
                        <FormGroup>
                           <h3 className="text-left mb-10">Your Comment<span className="required-field">*</span></h3>
                           <Input
                              type="textarea"
                              name="commentNote"
                              id="commentNote"
                              variant="outlined"
                              defaultValue={this.state.commentNote}
                              placeholder="Enter your comment before cancel invoice"
                              onChange={(e) => this.onChnage('commentNote', e.target.value)}
                           >
                           </Input>
                           {(this.state.add_err) ? <FormHelperText>{this.state.add_err}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                  </div>
               </ModalBody>
               <ModalFooter>
                  <Button variant="contained" color="primary" onClick={() => this.viewInvoiceClose()}>Cancel</Button>
                  <Button variant="contained" color="primary" onClick={this.cancelInvoiceSubmit.bind(this)} disabled={!this.validateInvoiceSubmit()}>Submit</Button>
                  {/*<Button className="mr-5" color="primary" size="sm" onClick={this.cancelInvoiceSubmit.bind(this)}>Ok</Button>*/}
               </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.confirmInvoice} toggle={() => this.viewInvoiceClose()}>
               <ModalHeader toggle={() => this.viewInvoiceClose()}>
                  Confirm Invoice
               </ModalHeader>
               <ModalBody>
                  <div className="row">
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">Invoice Amt</Label><br />
                           ${parseFloat(this.state.paymentConfirm.invoiceAmt).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">Discount Amt</Label><br />
                           ${parseFloat(this.state.paymentConfirm.discountAmt).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">Amt Due</Label><br />
                           ${parseFloat(this.state.paymentConfirm.amount_due).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">IOU Adjustment Amt</Label><br />
                           ${parseFloat(this.state.currentIOUDET).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">After Adj Amt Due</Label><br />
                           ${parseFloat(this.state.paymentConfirm.amount).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className="col-sm-4">
                        <FormGroup>
                           <Label for="paid_date">Remaining IOU Balance</Label><br />
                           ${parseFloat(this.state.paymentConfirm.remainingIOU).toFixed(2)}
                        </FormGroup>
                     </div>
                     <div className={(this.state.paymentConfirm.amount > 0) ? "col-md-4" : "col-md-4 d-none"}>
                        <FormGroup>
                           <Label for="check_number">Check/ACH Ref#.<span className="required-field">*</span></Label><br />
                           <TextField
                              type="text"
                              name="check_number"
                              id="check_number"
                              fullWidth
                              variant="outlined"
                              placeholder="Check Number"
                              value={this.state.paymentConfirm.check_number}
                              error={(this.state.errorConfirm.check_number) ? true : false}
                              helperText={this.state.errorConfirm.check_number}
                              onChange={(e) => this.onChnageConfirm('check_number', e.target.value)}
                           />
                        </FormGroup>
                     </div>

                     <div className={(this.state.paymentConfirm.amount > 0) ? "col-md-4" : "col-md-4 d-none"}>
                        <FormGroup>
                           <Label for="bank_name">Bank Name<span className="required-field">*</span></Label><br />
                           <Input
                              type="select"
                              name="name"
                              id="name"
                              placeholder=""
                              onChange={(e) => this.onChnageConfirm('bank_name', e.target.value)}
                           >
                              <option value="">Select</option>
                              {this.props.providerBankDetails && this.props.providerBankDetails.map((bank, key) => (
                                 <option value={bank.bank_name} key={key}>{bank.bank_name}</option>
                              ))}

                           </Input>
                           {(this.state.errorConfirm.bank_name) ? <FormHelperText>{this.state.errorConfirm.bank_name}</FormHelperText> : ''}
                           {/*<TextField
                              type="text"
                              name="bank_name"
                              id="bank_name"
                              fullWidth
                              variant="outlined"
                              placeholder="Bank Name"
                              value={this.state.paymentConfirm.bank_name}
                              error={(this.state.errorConfirm.bank_name) ? true : false}
                              helperText={this.state.errorConfirm.bank_name}
                              onChange={(e) => this.onChnageConfirm('bank_name', e.target.value)}
                           />*/}
                        </FormGroup>
                     </div>
                     <div className="col-md-4">
                        <FormGroup>
                           <Label for="paid_date">Paid Date<span className="required-field">*</span></Label><br />
                           <DatePicker
                              dateFormat="MM/dd/yyyy"
                              name="paid_date"
                              id="paid_date"
                              placeholderText="MM/DD/YYYY"
                              minDate={new Date()}
                              autocomplete={false}
                              selected={this.state.paidDate}
                              onChange={(e) => this.onChnageConfirm('paid_date', e)}
                           />
                           {(this.state.errorConfirm.paid_date) ? <FormHelperText>{this.state.errorConfirm.paid_date}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                     <div className="col-md-12">
                        <FormGroup>
                           <Label for="comment">Invoice Note<span className="required-field">*</span></Label><br />
                           <Input
                              type="textarea"
                              name="comment"
                              id="comment"
                              variant="outlined"
                              value={this.state.paymentConfirm.comment}
                              placeholder="Invoice Note"
                              onChange={(e) => this.onChnageConfirm('comment', e.target.value)}
                           >
                           </Input>
                           {(this.state.errorConfirm.comment) ? <FormHelperText>{this.state.errorConfirm.comment}</FormHelperText> : ''}
                        </FormGroup>
                     </div>
                  </div>
               </ModalBody>
               <ModalFooter>
                  <Button className="mr-5" color="primary" size="sm" onClick={() => this.viewInvoiceClose()}>Cancel</Button>
                  <Button className="mr-5" color="primary" size="sm" onClick={this.confirmInvoiceSubmit.bind(this)} disabled={!this.validateSubmit()}>Submit</Button>
               </ModalFooter>
            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, adminInvoice }) => {
   const { user } = authUser;
   const { loading, openInvoiceList, openInvoiceView, listInvoiceView, currentInvoiceID, providerBankDetails, iouDetails } = adminInvoice;
   return { loading, openInvoiceList, openInvoiceView, listInvoiceView, currentInvoiceID, user, providerBankDetails, iouDetails }

}

export default connect(mapStateToProps, {
   invoiceAdminOpenList, viewInvoiceDetails, deleteInvoiceApplication, listInvoiceApplication, addInvoiceApplication, cancelInvoiceApplication, confirmInvoicesSubmit
})(openInvoiceList);