/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card
import { RctCard } from 'Components/RctCard/index';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import FormHelperText from '@material-ui/core/FormHelperText';
import moment from 'moment';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Button from '@material-ui/core/Button';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { isEmpty } from '../../validator/Validator';
import AppConfig from 'Constants/AppConfig';
import {
   providerInvoiceView, deleteInvoiceApplication, viewInvoice, approveInvoice, cancelInvoiceApplication
} from 'Actions';

class pendingInvoiceList extends Component {

   state = {
      all: false,
      redirectURL: false,
      deleteInvoice: false,
      comment: '',
      planID: '',
      providerID: '',
      invoiceID: '',
      cancelInvoice: false,
      cancelInvoiceID: null,
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.props.providerInvoiceView(this.props.match.params.id);
   }

   onChnage(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'comment':
            if (isEmpty(value)) {
               add_err = "Comment can't be blank";
            } else {
               add_err = '';
            }
            break;
      }
      this.setState({ comment: value });
      this.setState({ add_err: add_err });
   }

   validateSubmit() {
      return (this.state.add_err === '');
   }
   componentWillReceiveProps(nextProps) {

      (nextProps.approveRedirect) ? this.setState({ redirectURL: true }) : ''

   }
   downloadPDF(provider_id) {
      //this.props.invoiceProviderreviewPDF(this.props.previewInvoice)
      this.props.viewInvoice(this.props.match.params.id, provider_id)
   }
   goBack() {
      this.setState({ redirectURL: true })
   }
   deleteInvoiceApp(pp_id, invoice_id, provider_id, e) {
      this.setState({ deleteInvoice: true, planID: pp_id, providerID: provider_id, invoiceID: invoice_id })
      //this.props.deleteInvoiceApplication(pp_id, invoice_id, provider_id);
   }
   confirmDelete() {
      this.props.deleteInvoiceApplication(this.state.planID, this.state.invoiceID, this.state.providerID, this.state.comment);
      this.setState({ deleteInvoice: false, planID: '', providerID: '', invoiceID: '', comment: '', add_err: undefined })
   }
   deleteInvoiceClose() {
      this.setState({ deleteInvoice: false })
   }
   approveInvoice(invoice_id) {
      this.props.approveInvoice(invoice_id);
   }
   cancelInvoice(invoice_id, e) {
      this.setState({ cancelInvoice: true, cancelInvoiceID: invoice_id });
   }
   viewInvoiceClose() {

      this.setState({ cancelInvoice: false, cancelInvoiceID: null });
   }
   cancelInvoiceSubmit() {
      var promise = new Promise(function (resolve, reject) {
         // call resolve if the method succeeds
         this.props.cancelInvoiceApplication(this.state.cancelInvoiceID, this.state.commentNote);
         resolve(true);
      }.bind(this))
      promise.then((bool) => {
         this.setState({ cancelInvoiceID: null, cancelInvoice: false, redirectURL: true });
      })      
   }
   validateInvoiceSubmit() {
      return (this.state.add_err === '');
   }
   render() {
      if (this.state.redirectURL === true) {
         return (<Redirect to={`/admin/accounts/open-invoice`} />);
      }

      let currentDate = new Date();
      currentDate = moment(currentDate).format('DD/MM/YYYY');
      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.previewInvoice" />}
               match={this.props.match}
            />
            <div className="row">
               <div className="col-sm-12 mx-auto">
                  <RctCard>


                     {this.props.invoicePreviewList &&
                        <div className="p-10">
                           <h1 className="text-center mb-20">
                              <img src={AppConfig.appLogo} className="mr-15" alt="Health Partner" />
                           </h1>
                           <div className="d-flex justify-content-between mb-30 add-full-card customer-accnt">

                              <div className="add-card w-30 mr-10">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Provider Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Account No :</strong></td>
                                          <td>{this.props.providerDetails.provider_ac}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Name :</strong> </td>
                                          <td>{this.props.providerDetails.name}</td>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Address :</strong> </td>
                                          <td>
                                             {this.props.providerDetails.address1 + ' ' + this.props.providerDetails.address2 + ' ' + this.props.providerDetails.city + ', ' + this.props.providerDetails.state_name + ' - ' + this.props.providerDetails.zip_code}
                                          </td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Phone :</strong> </td>
                                          <td>{this.props.providerDetails.primary_phone}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </div>

                              <div className="add-card w-30">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2">Invoice Information</th>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Invoice Number :</strong> </td>
                                          <td>{this.props.providerDetails.invoice_number}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Invoice Date :</strong> </td>
                                          <td>{this.props.providerDetails.date_created}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Number of accounts included :</strong> </td>
                                          <td>{this.props.invoicePreviewList.length}</td>
                                       </tr>

                                       <tr>
                                          <td className="text-right"><strong>Invoice Amount :</strong> </td>
                                          <td>${(this.props.totalAmount.total_amount) ? parseFloat(this.props.totalAmount.total_amount).toFixed(2) : '0.00'}</td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>


                              <div className="add-card w-30">

                                 <table>
                                    <tbody>
                                       <tr>
                                          <th colSpan="2"></th>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Discount Amount :</strong> </td>
                                          <td>${(this.props.totalAmount.discount_amount) ? parseFloat(this.props.totalAmount.discount_amount).toFixed(2) : '0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Amount Due :</strong> </td>
                                          <td>${(this.props.totalAmount.total_amount) ? (parseFloat(this.props.totalAmount.total_amount).toFixed(2) - parseFloat(this.props.totalAmount.discount_amount).toFixed(2)).toFixed(2) : '0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>IOU Adjustment :</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)
                                                   :
                                                   (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount)).toFixed(2)
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)
                                                      :
                                                      (parseFloat(this.props.providerDetails.refund_due)).toFixed(2)

                                                   :
                                                   '0.00'}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Total amount due after Adjustment :</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   '0.00'
                                                   :
                                                   (this.props.totalAmount.total_amount !== undefined)?(parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount) - parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount)).toFixed(2):'0.00'
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      '0.00'
                                                      :
                                                      (this.props.totalAmount.total_amount) ?
                                                         (parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount) - parseFloat(this.props.providerDetails.refund_due)).toFixed(2)
                                                         : '0.00'

                                                   :
                                                   parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount).toFixed(2)}</td>
                                       </tr>
                                       <tr>
                                          <td className="text-right"><strong>Remaining IOU Balance Amount:</strong> </td>
                                          <td>${
                                             (this.props.providerDetails.iou_paid_amount != null) ?
                                                (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                   (parseFloat(this.props.providerDetails.refund_due - this.props.providerDetails.iou_paid_amount) - parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)).toFixed(2)
                                                   :
                                                   '0.00'
                                                : (this.props.providerDetails.refund_due != null) ?
                                                   (parseFloat(this.props.providerDetails.refund_due) > parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)) ?
                                                      (parseFloat(this.props.providerDetails.refund_due) - parseFloat(this.props.totalAmount.total_amount - this.props.totalAmount.discount_amount)).toFixed(2)
                                                      :
                                                      '0.00'

                                                   :
                                                   '0.00'}</td>
                                       </tr>
                                    </tbody>
                                 </table>

                              </div>


                           </div>
                           <div className="table-responsive mb-40 pymt-history">
                              <h2 className="text-center mb-10">Invoice Detail</h2>
                              <div className="table-responsive">
                                 <table className="table table-borderless admin-account-view-invoice">
                                    <thead>
                                       <tr>
                                          <th>Plan ID</th>
                                          <th>A/C No</th>
                                          <th>App No</th>
                                          <th>First Name</th>
                                          <th>M/I</th>
                                          <th>Last Name</th>
                                          <th>DoB</th>
                                          <th>Phone</th>
                                          <th>City</th>
                                          <th>State</th>
                                          {/*<th>Zip</th>*/}
                                          <th>Service Date</th>
                                          <th>Loan Amt</th>
                                          <th>Action</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       {this.props.invoicePreviewList.map((invoice, idx) => (
                                          <tr key={idx}>
                                             <td>
                                                {invoice.plan_number}
                                             </td>
                                             <td>
                                                {invoice.patient_ac}
                                             </td>
                                             <td>
                                                {invoice.application_no}
                                             </td>
                                             <td>
                                                {invoice.f_name}
                                             </td>
                                             <td>
                                                {(invoice.m_name) ? invoice.m_name : '-'}
                                             </td>
                                             <td>
                                                {invoice.l_name}
                                             </td>
                                             <td>
                                                {invoice.dob}
                                             </td>
                                             <td>
                                                {invoice.peimary_phone}
                                             </td>
                                             <td>
                                                {invoice.City}
                                             </td>
                                             <td>
                                                {invoice.name}
                                             </td>
                                             {/*<td>
                                                {invoice.zip_code}
                                             </td>*/}
                                             <td>
                                                {invoice.procedure_date}
                                             </td>
                                             <td>
                                                ${parseFloat(invoice.loan_amount).toFixed(2)}
                                             </td>
                                             <td>
                                                <a href="javascript:void(0)" onClick={this.deleteInvoiceApp.bind(this, invoice.pp_id, invoice.provider_invoice_id, invoice.provider_id)} title="Remove Application"><i className="ti-close"></i></a>
                                             </td>
                                          </tr>
                                       ))}
                                    </tbody>
                                 </table>

                              </div>
                           </div>
                        </div>
                     }
                     {this.props.loading &&
                        <RctSectionLoader />
                     }
                     <div className="invoice-head text-right">
                        <ul className="list-inline">
                           <li><a href="javascript:void(0);" onClick={this.goBack.bind(this)}><i className="mr-10 ti-back-left"></i> Cancel</a></li>
                           <li><a href="javascript:void(0)" onClick={this.cancelInvoice.bind(this, this.props.match.params.id)} title="Reject Invoice"><i className="ti-close"></i> Reject Invoice</a></li>
                           <li><a href="javascript:void(0);" onClick={this.approveInvoice.bind(this, this.props.match.params.id)}><img src="https://www.pngrepo.com/download/105607/approve-invoice.png" width="30" /> Approve</a></li>
                           {this.props.invoicePreviewList &&
                              <li><a href="javascript:void(0);" onClick={this.downloadPDF.bind(this, this.props.providerDetails.provider_id)}><i className="mr-10 ti-import"></i> Export PDF</a></li>
                           }
                        </ul>

                     </div>
                  </RctCard>

                  <Modal isOpen={this.state.deleteInvoice} toggle={() => this.deleteInvoiceClose()}>
                     <ModalHeader toggle={() => this.deleteInvoiceClose()}>
                        Are you sure to delete this item?
                     </ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="col-md-12">
                              <FormGroup>
                                 <h3 className="text-left mb-10">Your Comment<span className="required-field">*</span></h3>
                                 <Input
                                    type="textarea"
                                    name="comment"
                                    id="comment"
                                    variant="outlined"
                                    defaultValue={this.state.comment}
                                    placeholder="Enter your comment before remove"
                                    onChange={(e) => this.onChnage('comment', e.target.value)}
                                 >
                                 </Input>
                                 {(this.state.add_err) ? <FormHelperText>{this.state.add_err}</FormHelperText> : ''}
                              </FormGroup>
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <Button variant="contained" color="primary" onClick={() => this.deleteInvoiceClose()}>Cancel</Button>
                        <Button variant="contained" color="primary" onClick={this.confirmDelete.bind(this)} disabled={!this.validateSubmit()}>Remove</Button>
                     </ModalFooter>
                  </Modal>
                  <Modal isOpen={this.state.cancelInvoice} toggle={() => this.viewInvoiceClose()}>
                     <ModalHeader toggle={() => this.viewInvoiceClose()}>
                        Are you sure you want to reject this invoice?
               </ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="col-md-12">
                              <FormGroup>
                                 <h3 className="text-left mb-10">Your Comment<span className="required-field">*</span></h3>
                                 <Input
                                    type="textarea"
                                    name="comment"
                                    id="comment"
                                    variant="outlined"
                                    defaultValue={this.state.commentNote}
                                    placeholder="Enter your comment before cancel invoice"
                                    onChange={(e) => this.onChnage('comment', e.target.value)}
                                 >
                                 </Input>
                                 {(this.state.add_err) ? <FormHelperText>{this.state.add_err}</FormHelperText> : ''}
                              </FormGroup>
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <Button variant="contained" color="primary" onClick={() => this.viewInvoiceClose()}>Cancel</Button>
                        <Button variant="contained" color="primary" onClick={this.cancelInvoiceSubmit.bind(this)} disabled={!this.validateInvoiceSubmit()}>Submit</Button>
                        {/*<Button className="mr-5" color="primary" size="sm" onClick={this.cancelInvoiceSubmit.bind(this)}>Ok</Button>*/}
                     </ModalFooter>
                  </Modal>
               </div>
            </div>

         </div>
      );
   }
}

const mapStateToProps = ({ adminInvoice }) => {
   const { loading, invoicePreviewList, providerDetails, totalAmount, approveRedirect } = adminInvoice;
   return { loading, invoicePreviewList, providerDetails, totalAmount, approveRedirect }

}

export default connect(mapStateToProps, {
   providerInvoiceView, deleteInvoiceApplication, viewInvoice, approveInvoice, cancelInvoiceApplication
})(pendingInvoiceList);