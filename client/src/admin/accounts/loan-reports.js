/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input } from 'reactstrap';

import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CryptoJS from 'crypto-js';
import {
   fullReportList, loanReportFilter, loanReportDownload, loanReportDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class LoanReports extends Component {

   state = {
      reportFilter: {
         customer_name: '',
         loan_start_date: '',
         loan_end_date: '',
         maturity_start_date: '',
         maturity_end_date: '',
         status: '',
         filter_type: '',
         year: '',
         month: '',
         quarter: '',
      },
      loan_start_date: '',
      loan_end_date: '',
      maturity_start_date: '',
      maturity_end_date: '',
      widthTable: '',
      mainHeading: 'Loan Reports'
   }


   componentDidMount() {
      //console.log(ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children)
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //this.props.fullReportList();
      this.props.loanReportFilter(this.state.reportFilter);
   }


   onChangeReportFilter(key, value) {
      switch (key) {
         case 'loan_start_date':
            if (value == null) {
               value = '';
               this.setState({ loan_start_date: '' })
            } else {
               this.setState({ loan_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'loan_end_date':
            if (value == null) {
               value = '';
               this.setState({ loan_end_date: '' })
            } else {
               this.setState({ loan_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'maturity_start_date':
            if (value == null) {
               value = '';
               this.setState({ maturity_start_date: '' })
            } else {
               this.setState({ maturity_start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'maturity_end_date':
            if (value == null) {
               value = '';
               this.setState({ maturity_end_date: '' })
            } else {
               this.setState({ maturity_end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      //console.log(this.state.reportFilter);
      this.props.loanReportDownload(this.state.reportFilter);
   }
   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.loanReportDownloadXLS(this.state.reportFilter);
   }
   fullReportFilter() {
      this.props.loanReportFilter(this.state.reportFilter);
      const { reportFilter } = this.state;
      let fileName = '';
      if (reportFilter.filter_type == 1) {
         moment.updateLocale('en', {
            week: {
               dow: 1,
               doy: 1
            }
         });

         let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
         let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
         fileName = 'Weekly Loan Report (' + startOfWeek + ' - ' + endOfWeek + ')';
      } else if (reportFilter.filter_type == 2) {
         let monthName = moment().month(reportFilter.month-1).format("MMM");
         fileName = 'Monthly Loan Report (' + monthName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 3) {
         let queterName = ''
         if (reportFilter.quarter == 1) {
            queterName = 'Q1';
         } else if (reportFilter.quarter == 2) {
            queterName = 'Q2';
         } else if (reportFilter.quarter == 3) {
            queterName = 'Q3';
         } else if (reportFilter.quarter == 4) {
            queterName = 'Q4';
         }
         fileName = 'Quarterly Loan Report (' + queterName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 4) {
         fileName = 'Yearly Loan Report (' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 5) {
         let currentDate = moment().format('MM/DD/YYYY');
         fileName = 'Year to Date Loan Report (01/01/' + reportFilter.year + '-' + currentDate + ')';
      } else if (reportFilter.filter_type == 6) {
         let loanStart = moment(reportFilter.loan_start_date).format('MM/DD/YYYY');
         let loanEnd = moment(reportFilter.loan_end_date).format('MM/DD/YYYY');
         fileName = 'By Date Loan Report (' + loanStart + '-' + loanEnd + ')';
      }else {
         fileName = this.state.mainHeading;
      }
      this.setState({mainHeading:fileName})
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {
               if (currentValue.loan_amount != '') {
                  accumulator['loan_amount'] = (accumulator['loan_amount'] !== undefined) ? accumulator['loan_amount'] + currentValue.main_amount : currentValue.main_amount;
               }
               if (currentValue.total_late_fee != '') {
                  accumulator['total_late_fee'] = (accumulator['total_late_fee'] !== undefined) ? accumulator['total_late_fee'] + currentValue.total_late_fee : currentValue.total_late_fee;
               }
               if (currentValue.total_finance_charge != '') {
                  accumulator['total_finance_charge'] = (accumulator['total_finance_charge'] !== undefined) ? accumulator['total_finance_charge'] + currentValue.total_finance_charge : currentValue.total_finance_charge;
               }
               if (currentValue.total_payment_amt != '') {
                  accumulator['total_payment_amt'] = (accumulator['total_payment_amt'] !== undefined) ? accumulator['total_payment_amt'] + currentValue.principal_paid + currentValue.total_additional_amt : currentValue.principal_paid + currentValue.total_additional_amt;
               }
               var total_payment_amt = (currentValue.loan_amount != null && currentValue.total_payment_amt != null) ? (currentValue.loan_amount - currentValue.total_payment_amt).toFixed(2) : currentValue.loan_amount;
               if (total_payment_amt != '') {
                  accumulator['total_blc_amt'] = (accumulator['total_blc_amt'] !== undefined) ? parseFloat(accumulator['total_blc_amt']) + currentValue.remaining_amount : currentValue.remaining_amount;
               }

            }

            return accumulator
         }, []);
         //console.log(amount);
         return amount
      }

      //return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
   }
   enc(plainText) {
      var SECRET = 'rmaeshCSS'
      var b64 = CryptoJS.AES.encrypt(plainText, SECRET).toString();
      var e64 = CryptoJS.enc.Base64.parse(b64);
      var eHex = e64.toString(CryptoJS.enc.Hex);
      return eHex;
   }

   componentWillReceiveProps(nextProps) {
      var lateFinArr = [];
      if (nextProps.lateFinCharge) {

         /*nextProps.lateFinCharge.forEach(function(chr) {
            var late_fee = (chr.late_fee_received*chr.late_fee_per)/100
            var fin_fee = (chr.fin_charge_amt*chr.fin_charge_per)/100
            var pp_id = chr.pp_id

            var obj = {
               "late_fee": late_fee,
               "fin_fee": fin_fee,
               "pp_id": pp_id,
            }
            lateFinArr.push(obj)
         })*/
         //console.log('nextProps.lateFinCharge')
         //console.log(nextProps.lateFinCharge)


      }
   }
   validateButton() {

      if (this.state.reportFilter.filter_type == 1 || this.state.reportFilter.filter_type == 5) {
         return true
      } else if (this.state.reportFilter.filter_type == 2) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.month
         )
      } else if (this.state.reportFilter.filter_type == 3) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.quarter
         )
      } else if (this.state.reportFilter.filter_type == 4) {
         return (
            this.state.reportFilter.year
         )
      } else if (this.state.reportFilter.filter_type == 6) {
         return (
            this.state.reportFilter.loan_start_date &&
            this.state.reportFilter.loan_end_date
         )
      } else {
         return false
      }

   }
   render() {

      const admin_report_list = this.props.admin_report_list;
      let totalPrinAmt = 0;
      let matAmt = 0;
      let intAmt = 0;
      let lateAmt = 0;
      let finAmt = 0;
      let prinBalAmt = 0;
      let prinrcvdAmt = 0;
      let intRcvdAmt = 0;
      let totalrcvdAmt = 0;
      /*
            const columns = [
               {
                  name: 'Loan No',
                  field: 'plan_number',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           <Link to={`/admin/credit-application/plan-details/${this.enc(value.application_id.toString())}`}>{value.plan_number}</Link>
                        );
                     }
                  }
               },
               {
                  name: 'Provider Name',
                  field: 'name',
               },
               {
                  name: 'Customer Name',
                  field: 'f_name',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           value.f_name + ' ' + value.m_name + ' ' + value.l_name
                        );
                     }
                  }
               },
               {
                  name: 'Loan Date',
                  field: 'loan_date',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value, tableMeta) => {
                        //console.log(jQuery(this).tableMeta.columnIndex)
                        return (
                           value.loan_date
                        );
                     }
                  }
               },
               {
                  name: 'Prin. Amt',
                  field: 'main_amount',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.main_amount) ? '$' + value.main_amount.toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: 'Maturity Date',
                  field: 'maturity_date',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.maturity_date) ? value.maturity_date : 'NULL'
                        );
                     }
                  }
               },
               {
                  name: 'Matuirty Amt',
                  field: 'maturity_date',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.amount) ? '$' + (value.amount).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: 'Int. Amt',
                  field: 'maturity_date',
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.amount) ? '$' + (value.amount - value.main_amount).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Late Fee",
                  field: "late_fee",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.late_fee) ? '$' + value.late_fee.toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Fin Charges",
                  field: "fin_fee",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.fin_fee) ? '$' + value.fin_fee.toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Prin. Bal.",
                  field: "total_payment_amt",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.remaining_amount) ? '$' + (value.remaining_amount).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Prin. Recvd",
                  field: "principal_paid",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.principal_paid) ? '$' + (value.principal_paid + value.total_additional_amt).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Interest recvd",
                  field: "principal_paid",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        let FinCharge = (value.fin_fee) ? value.fin_fee : 0;
                        let lateFee = (value.late_fee) ? value.late_fee : 0
                        let prinRecd = (value.principal_paid) ? value.principal_paid : 0;
                        let AddAmt = (value.total_additional_amt) ? value.total_additional_amt : 0;
                        let totalAmt = (value.total_payment_amt) ? value.total_payment_amt : 0;
      
                        return (
                           (value.principal_paid) ? '$' + ((totalAmt + AddAmt) - (prinRecd + AddAmt + FinCharge + lateFee)).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Total Amt Recvd",
                  field: "total_payment_amt",
                  options: {
                     filter: true,
                     sort: true,
                     empty: true,
                     customBodyRender: (value) => {
                        return (
                           (value.total_payment_amt) ? '$' + (value.total_payment_amt + value.total_additional_amt).toFixed(2) : '$0.00'
                        );
                     }
                  }
               },
               {
                  name: "Status",
                  field: "p_status",
      
               }
      
            ];
            const footerStyle = {
               display: 'flex',
               justifyContent: 'flex-end',
               padding: '0px 24px 0px 24px'
            };
            const options = {
               //filterType: 'dropdown',
               selectableRows: false,
               filter: false,
               search: false,
               print: false,
               download: false,
               viewColumns: false,
      
               customToolbar: false,
               customFooter: (
                  count,
                  page,
                  rowsPerPage,
                  changeRowsPerPage,
                  changePage,
                  rowsSelected
               ) => {
                  const invoiceSubtotal = this.totalLoanAmount(admin_report_list, page, rowsPerPage);
      
                  return (
      
                     <TableFooter>
                        <TableRow>
                           <TableCell style={footerStyle} ><b>Total Principal Amt Outstanding : ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.loan_amount !== undefined && invoiceSubtotal.loan_amount !== null) ? invoiceSubtotal.loan_amount.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                           <TableCell style={footerStyle} ><b>Total Principal Amt Rcvd : ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.total_payment_amt !== undefined && invoiceSubtotal.total_payment_amt !== null) ? invoiceSubtotal.total_payment_amt.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                           <TableCell style={footerStyle} ><b>Total Principal Amt Balance : ${(invoiceSubtotal !== undefined) ? (invoiceSubtotal.total_blc_amt !== undefined && invoiceSubtotal.total_blc_amt !== null) ? invoiceSubtotal.total_blc_amt.toFixed(2) : '0.00' : '0.00'}</b></TableCell>
                        </TableRow>
      
                        <TableRow>
                           <TablePagination
                              count={count}
                              rowsPerPage={rowsPerPage}
                              page={page}
                              onChangePage={(_, page) => changePage(page)}
                              onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
                              rowsPerPageOptions={[1, 25, 50, 100]}
                           />
                        </TableRow>
                     </TableFooter>
                  );
               }
            };
            const myTheme = createMuiTheme({
               overrides: {
                  MaterialDatatableToolbar: {
                     root: { display: "none" }
                  },
                  MuiTableCell: {
                     footer: { padding: "4px 8px 4px 8px" }
                  },
               }
            });*/
      let currentYear = new Date();
      let currentMonth = currentYear.getMonth() + 1;
      currentYear = currentYear.getFullYear();
      const yearList = [];
      if (this.props.yearStart != '') {
         for (var i = currentYear; i >= this.props.yearStart; i--) {
            yearList.push({ id: i, value: i });
         }
      } else {
         for (var i = currentYear; i >= (currentYear - 5); i--) {
            yearList.push({ id: i, value: i });
         }
      }

      return (
         <div className="admin-reports-loan-report">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={this.state.mainHeading}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="filter_type">Report Period</Label>
                              <Input
                                 type="select"
                                 name="filter_type"
                                 id="filter_type"
                                 onChange={(e) => this.onChangeReportFilter('filter_type', e.target.value)}
                              >
                                 <option value="">Select Report Period</option>
                                 <option value="1">Current Week</option>
                                 <option value="2">Monthly</option>
                                 <option value="3">Quarterly</option>
                                 <option value="4">Yearly</option>
                                 <option value="5">Year to date</option>
                                 <option value="6">By Date</option>
                              </Input>

                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2 || this.state.reportFilter.filter_type == 3 || this.state.reportFilter.filter_type == 4) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="year">Year</Label>
                              <Input
                                 type="select"
                                 name="year"
                                 id="year"
                                 onChange={(e) => this.onChangeReportFilter('year', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {yearList && yearList.map((data, key) => (
                                    <option value={data.id} key={key}>{data.value}</option>
                                 ))}
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="month">Month</Label>
                              <Input
                                 type="select"
                                 name="month"
                                 id="month"
                                 onChange={(e) => this.onChangeReportFilter('month', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1" disabled={(currentMonth < 1 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jan</option>
                                 <option value="2" disabled={(currentMonth < 2 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Feb</option>
                                 <option value="3" disabled={(currentMonth < 3 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Mar</option>
                                 <option value="4" disabled={(currentMonth < 4 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Apr</option>
                                 <option value="5" disabled={(currentMonth < 5 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>May</option>
                                 <option value="6" disabled={(currentMonth < 6 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jun</option>
                                 <option value="7" disabled={(currentMonth < 7 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jul</option>
                                 <option value="8" disabled={(currentMonth < 8 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Aug</option>
                                 <option value="9" disabled={(currentMonth < 9 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Sep</option>
                                 <option value="10" disabled={(currentMonth < 10 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Oct</option>
                                 <option value="11" disabled={(currentMonth < 11 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Nov</option>
                                 <option value="12" disabled={(currentMonth < 12 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Dec</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 3) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="quarter">Quarterly</Label>
                              <Input
                                 type="select"
                                 name="quarter"
                                 id="quarter"
                                 onChange={(e) => this.onChangeReportFilter('quarter', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Q1</option>
                                 <option value="2">Q2</option>
                                 <option value="3">Q3</option>
                                 <option value="4">Q4</option>
                              </Input>
                           </FormGroup>
                        </div>

                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="loan_start_date">Loan date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="loan_start_date"
                                 id="loan_start_date"
                                 selected={this.state.loan_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('loan_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="loan_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="loan_end_date"
                                 id="loan_end_date"
                                 selected={this.state.loan_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('loan_end_date', e)}
                              />
                           </FormGroup>
                        </div>


                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="customer_name">Customer Name</Label>
                              <Input
                                 type="text"
                                 name="customer_name"
                                 id="customer_name"
                                 placeholder="Customer Name"
                                 onChange={(e) => this.onChangeReportFilter('customer_name', e.target.value)}
                              >
                              </Input>
                           </FormGroup>
                        </div>




                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="maturity_start_date">Maturity date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="maturity_start_date"
                                 id="maturity_start_date"
                                 selected={this.state.maturity_start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('maturity_start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="maturity_end_date">&nbsp;</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="maturity_end_date"
                                 id="maturity_end_date"
                                 selected={this.state.maturity_end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('maturity_end_date', e)}
                              />
                           </FormGroup>
                        </div>

                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="status">Status</Label>
                              <Input
                                 type="select"
                                 name="status"
                                 id="status"
                                 placeholder="Financial Chargesstatus"
                                 onChange={(e) => this.onChangeReportFilter('status', e.target.value)}
                              >
                                 <option value="">Status</option>
                                 {this.props.customerPlanStatus && this.props.customerPlanStatus.map((inv_status, key) => (
                                    <option value={inv_status.status_id} key={key}>{inv_status.value}</option>
                                 ))}

                              </Input>

                           </FormGroup>
                        </div>
                        <div className="col-md-2">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="loan_date">&nbsp;</Label>
                                 {this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download"><i className="material-icons mr-10 mt-10 btn_down custom_icon">search</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                                 }
                                 {!this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down_disable custom_icon">search</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf_disable">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls_disable">insert_drive_file</i></a>
                                    </span>
                                 }
                              </FormGroup>

                           </div>
                        </div>


                     </div>
                  </Form>
                  <div className="table-responsive">
                     <table className="table table-bordered table-sm">
                        <thead>
                           <tr>
                              <th>Loan No</th>
                              <th>Provider Name</th>
                              <th>Customer Name</th>
                              <th>Loan Date</th>
                              <th>Prin. Amt</th>
                              <th>Maturity Date</th>
                              <th>Matuirty Amt</th>
                              <th>Int. Amt</th>
                              <th>Late Fee</th>
                              <th>Fin Charges</th>
                              <th>Prin. Bal.</th>
                              <th>Prin. Rcvd</th>
                              <th>Interest Rcvd</th>
                              <th>Total Amt Rcvd</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           {admin_report_list && admin_report_list.map(function (row, idx) {
                              let FinCharge = (row.fin_fee) ? row.fin_fee : 0;
                              let lateFee = (row.late_fee) ? row.late_fee : 0
                              let prinRecd = (row.principal_paid) ? row.principal_paid : 0;
                              let AddAmt = (row.total_additional_amt) ? row.total_additional_amt : 0;
                              let totalAmt = (row.total_payment_amt) ? row.total_payment_amt : 0;
                              let planIntRcvd = ((totalAmt + AddAmt) - (prinRecd + AddAmt + FinCharge + lateFee));
                              totalPrinAmt = totalPrinAmt + row.main_amount;
                              matAmt = matAmt + row.amount;
                              intAmt = intAmt + (row.amount - row.main_amount);
                              lateAmt = lateAmt + row.late_fee;
                              finAmt = finAmt + row.fin_fee;
                              prinBalAmt = prinBalAmt + row.remaining_amount;
                              prinrcvdAmt = prinrcvdAmt + (row.principal_paid + row.total_additional_amt);
                              intRcvdAmt = intRcvdAmt + planIntRcvd;
                              totalrcvdAmt = totalrcvdAmt + (row.total_payment_amt + row.total_additional_amt);
                              return <tr key={idx}>
                                 <td>
                                    <Link to={`/admin/credit-application/plan-details/${this.enc(row.application_id.toString())}`}>{row.plan_number}</Link>
                                 </td>
                                 <td>{row.name}</td>
                                 <td>{row.f_name + ' ' + row.m_name + ' ' + row.l_name}</td>
                                 <td>{row.loan_date}</td>
                                 <td className="text-right">{(row.main_amount) ? '$' + row.main_amount.toFixed(2) : '$0.00'}</td>
                                 <td>{(row.maturity_date) ? row.maturity_date : '-'}</td>
                                 <td className="text-right">{(row.amount) ? '$' + (row.amount).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.amount) ? '$' + (row.amount - row.main_amount).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.late_fee) ? '$' + row.late_fee.toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.fin_fee) ? '$' + row.fin_fee.toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.remaining_amount) ? '$' + (row.remaining_amount).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.principal_paid) ? '$' + (row.principal_paid + row.total_additional_amt).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">${parseFloat(planIntRcvd).toFixed(2)}</td>
                                 <td className="text-right">{(row.total_payment_amt) ? '$' + (row.total_payment_amt + row.total_additional_amt).toFixed(2) : '$0.00'}</td>
                                 <td>{row.p_status}</td>
                              </tr>
                           }.bind(this))
                           }
                           {(admin_report_list === undefined || admin_report_list.length == 0) &&
                              <tr>
                                 <td colSpan="15">Sorry, no matching records found</td>
                              </tr>
                           }
                        </tbody>
                        <tfoot>
                           <tr>
                              <th colSpan="4">Total</th>
                              <th className="text-right">${parseFloat(totalPrinAmt).toFixed(2)}</th>
                              <th></th>

                              <th className="text-right">${parseFloat(matAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(intAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(lateAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(finAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(prinBalAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(prinrcvdAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(intRcvdAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(totalrcvdAmt).toFixed(2)}</th>
                              <th></th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               {/* <MuiThemeProvider theme={myTheme}>
                  <MaterialDatatable
                     ref="myTableData"
                     data={admin_report_list}
                     columns={columns}
                     options={options}


                  />
                        </MuiThemeProvider> */}

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   //console.log(AdminReportReducer)

   const { loading, admin_report_list, customerPlanStatus, lateFinCharge, yearStart } = AdminReportReducer;

   const user = authUser.user;
   return { loading, user, admin_report_list, customerPlanStatus, lateFinCharge, yearStart }

}

export default connect(mapStateToProps, {
   fullReportList, loanReportFilter, loanReportDownload, loanReportDownloadXLS
})(LoanReports);