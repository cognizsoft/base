/**
 * User Management Page
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from "react-helmet";

import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import moment from 'moment';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isEmpty, formatPhoneNumber, isPhone } from '../../validator/Validator';
import {
   customerCreditChargeFilter, customerCreditChargeDownload, customerCreditChargeDownloadXLS
} from 'Actions';

//import AddNewButton from './AddNewButton';

class CreditChargesReports extends Component {

   state = {
      reportFilter: {
         filter_type: '',
         start_date: '',
         end_date: '',
         year: '',
         month: '',
         quarter: '',
      },
      start_date: '',
      end_date: '',
      widthTable: '',
      mainHeading: 'Credit Charges'
   }


   componentDidMount() {
      //this.setState({ widthTable: ReactDOM.findDOMNode(this.refs.myTableData).children[2].children[0].children[1].children[0].children });
      //console.log(this.props.match.params.status_id)

      //this.props.customerCreditChargeFilter(this.state.reportFilter);
      this.props.customerCreditChargeFilter(this.state.reportFilter);

   }


   onChangeReportFilter(key, value) {

      switch (key) {
         case 'start_date':
            if (value == null) {
               value = '';
               this.setState({ start_date: '' })
            } else {
               this.setState({ start_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'end_date':
            if (value == null) {
               value = '';
               this.setState({ end_date: '' })
            } else {
               this.setState({ end_date: value })
               value = moment(value).format('YYYY-MM-DD');
            }
            break;
         case 'provider_phone':
            if (isEmpty(value)) {
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
            } else {
               value = formatPhoneNumber(value)
            }
            break;
      }
      this.setState({
         reportFilter: {
            ...this.state.reportFilter,
            [key]: value
         }
      });
   }


   fullReportDownload() {
      //console.log(this.state.reportFilter);
      this.props.customerCreditChargeDownload(this.state.reportFilter);
   }

   fullReportFilter() {
      //console.log(this.state.reportFilter);
      this.props.customerCreditChargeFilter(this.state.reportFilter);
      const { reportFilter } = this.state;
      let fileName = '';
      if (reportFilter.filter_type == 1) {
         moment.updateLocale('en', {
            week: {
               dow: 1,
               doy: 1
            }
         });

         let startOfWeek = moment().startOf('week').format('MM/DD/YYYY');
         let endOfWeek = moment().endOf('week').format('MM/DD/YYYY');
         fileName = 'Weelky Credit Charges Report (' + startOfWeek + ' - ' + endOfWeek + ')';
      } else if (reportFilter.filter_type == 2) {
         let monthName = moment().month(reportFilter.month - 1).format("MMM");
         fileName = 'Monthly Credit Charges Report (' + monthName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 3) {
         let queterName = ''
         if (reportFilter.quarter == 1) {
            queterName = 'Q1';
         } else if (reportFilter.quarter == 2) {
            queterName = 'Q2';
         } else if (reportFilter.quarter == 3) {
            queterName = 'Q3';
         } else if (reportFilter.quarter == 4) {
            queterName = 'Q4';
         }
         fileName = 'Quarterly Credit Charges Report (' + queterName + '-' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 4) {
         fileName = 'Yearly Credit Charges Report (' + reportFilter.year + ')';
      } else if (reportFilter.filter_type == 5) {
         let currentDate = moment().format('MM/DD/YYYY');
         fileName = 'Year to Date Credit Charges Report (01/01/' + reportFilter.year + '-' + currentDate + ')';
      } else if (reportFilter.filter_type == 6) {
         let loanStart = moment(reportFilter.loan_start_date).format('MM/DD/YYYY');
         let loanEnd = moment(reportFilter.loan_end_date).format('MM/DD/YYYY');
         fileName = 'By Date Credit Charges Report (' + loanStart + '-' + loanEnd + ')';
      } else {
         fileName = this.state.mainHeading;
      }
      this.setState({ mainHeading: fileName })
   }

   fullReportDownloadXLS() {
      //console.log(this.state.reportFilter);
      this.props.customerCreditChargeDownloadXLS(this.state.reportFilter);
   }
   goBack() {
      this.props.history.goBack(-1)
   }
   totalLoanAmount(items, page, rowsPerPage) {
      if (items !== undefined && items.length > 0) {
         var start = 0;
         var end = rowsPerPage;
         if (page != 0) {
            start = page * rowsPerPage;
            end = start + rowsPerPage;
         }
         var amount = items.reduce(function (accumulator, currentValue, currentindex) {
            if (currentindex >= start && currentindex < end) {

               accumulator['totalCC'] = (accumulator['totalCC'] !== undefined) ? accumulator['totalCC'] + currentValue.credit_charge_amt : currentValue.credit_charge_amt;


            }

            return accumulator
         }, []);
         return amount
      }
   }
   validateButton() {

      if (this.state.reportFilter.filter_type == 1 || this.state.reportFilter.filter_type == 5) {
         return true
      } else if (this.state.reportFilter.filter_type == 2) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.month
         )
      } else if (this.state.reportFilter.filter_type == 3) {
         return (
            this.state.reportFilter.year &&
            this.state.reportFilter.quarter
         )
      } else if (this.state.reportFilter.filter_type == 4) {
         return (
            this.state.reportFilter.year
         )
      } else if (this.state.reportFilter.filter_type == 6) {
         return (
            this.state.reportFilter.start_date &&
            this.state.reportFilter.end_date
         )
      } else {
         return false
      }

   }
   render() {

      let currentYear = new Date();
      let currentMonth = currentYear.getMonth() + 1;
      currentYear = currentYear.getFullYear();
      const yearList = [];
      if (this.props.yearStart != '') {
         for (var i = currentYear; i >= this.props.yearStart; i--) {
            yearList.push({ id: i, value: i });
         }
      } else {
         for (var i = currentYear; i >= (currentYear - 5); i--) {
            yearList.push({ id: i, value: i });
         }
      }
      let totalInvAmt = 0;
      let totalCrdAmt = 0;
      let totalPaidAmt = 0;
      return (
         <div className="admin-reports-pro-invoice-report">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={this.state.mainHeading}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <div className="modal-body page-form-outer text-left">
                  <Form>
                     <div className="row">
                        <div className="col-md-2">
                           <FormGroup>
                              <Label for="filter_type">Report Period</Label>
                              <Input
                                 type="select"
                                 name="filter_type"
                                 id="filter_type"
                                 onChange={(e) => this.onChangeReportFilter('filter_type', e.target.value)}
                              >
                                 <option value="">Select Report Period</option>
                                 <option value="1">Current Week</option>
                                 <option value="2">Monthly</option>
                                 <option value="3">Quarterly</option>
                                 <option value="4">Yearly</option>
                                 <option value="5">Year to date</option>
                                 <option value="6">By Date</option>
                              </Input>

                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2 || this.state.reportFilter.filter_type == 3 || this.state.reportFilter.filter_type == 4) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="year">Year</Label>
                              <Input
                                 type="select"
                                 name="year"
                                 id="year"
                                 onChange={(e) => this.onChangeReportFilter('year', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 {yearList && yearList.map((data, key) => (
                                    <option value={data.id} key={key}>{data.value}</option>
                                 ))}
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 2) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="month">Month</Label>
                              <Input
                                 type="select"
                                 name="month"
                                 id="month"
                                 onChange={(e) => this.onChangeReportFilter('month', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1" disabled={(currentMonth < 1 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jan</option>
                                 <option value="2" disabled={(currentMonth < 2 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Feb</option>
                                 <option value="3" disabled={(currentMonth < 3 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Mar</option>
                                 <option value="4" disabled={(currentMonth < 4 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Apr</option>
                                 <option value="5" disabled={(currentMonth < 5 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>May</option>
                                 <option value="6" disabled={(currentMonth < 6 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jun</option>
                                 <option value="7" disabled={(currentMonth < 7 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Jul</option>
                                 <option value="8" disabled={(currentMonth < 8 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Aug</option>
                                 <option value="9" disabled={(currentMonth < 9 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Sep</option>
                                 <option value="10" disabled={(currentMonth < 10 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Oct</option>
                                 <option value="11" disabled={(currentMonth < 11 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Nov</option>
                                 <option value="12" disabled={(currentMonth < 12 && currentYear == this.state.reportFilter.year) ? 'disabled' : ''}>Dec</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 3) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="quarter">Quarterly</Label>
                              <Input
                                 type="select"
                                 name="quarter"
                                 id="quarter"
                                 onChange={(e) => this.onChangeReportFilter('quarter', e.target.value)}
                              >
                                 <option value="">Select</option>
                                 <option value="1">Q1</option>
                                 <option value="2">Q2</option>
                                 <option value="3">Q3</option>
                                 <option value="4">Q4</option>
                              </Input>
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="start_date">Start Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="start_date"
                                 id="start_date"
                                 selected={this.state.start_date}
                                 placeholderText="From"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('start_date', e)}
                              />
                           </FormGroup>
                        </div>
                        <div className={(this.state.reportFilter.filter_type == 6) ? "col-md-2" : "d-none col-md-2"}>
                           <FormGroup>
                              <Label for="end_date">End Date</Label>
                              <DatePicker
                                 dateFormat="MM/dd/yyyy"
                                 name="end_date"
                                 id="end_date"
                                 selected={this.state.end_date}
                                 placeholderText="To"
                                 autocomplete={false}
                                 onChange={(e) => this.onChangeReportFilter('end_date', e)}
                              />
                           </FormGroup>
                        </div>


                        <div className="col-md-2">
                           <div className="list-action search_down_btn">
                              <FormGroup>
                                 <Label for="loan_end_date">&nbsp;</Label>
                                 {this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" onClick={this.fullReportFilter.bind(this)} className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down custom_icon"></i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownload.bind(this)} className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" onClick={this.fullReportDownloadXLS.bind(this)} className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls">insert_drive_file</i></a>
                                    </span>
                                 }
                                 {!this.validateButton() &&
                                    <span>
                                       <a href="javascript:void(0)" className="report-download" title="Search"><i className="material-icons mr-10 mt-10 btn_down_disable custom_icon"></i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download PDF"><i className="material-icons mr-10 mt-10 btn_pdf_disable">picture_as_pdf</i></a>
                                       <a href="javascript:void(0)" className="report-download" title="Download XLXS"><i className="material-icons mr-10 mt-10 btn_xls_disable">insert_drive_file</i></a>
                                    </span>
                                 }
                              </FormGroup>
                           </div>

                        </div>


                     </div>
                  </Form>
                  <div className="table-responsive">
                     <table className="table table-bordered table-sm">
                        <thead>
                           <tr>
                              <th>Invoice No</th>
                              <th>Inv Due Date</th>
                              <th>Customer Name</th>
                              <th className="text-right">Invoice Amt</th>
                              <th className="text-right">CC Charge Amt</th>
                              <th className="text-right">Total Amt Rcvd</th>
                           </tr>
                        </thead>
                        <tbody>
                           {this.props.creditCharesList && this.props.creditCharesList.map(function (row, idx) {
                              totalInvAmt = totalInvAmt + row.invoice_amt;
                              totalCrdAmt = totalCrdAmt + row.credit_charge_amt;
                              let paidAmt = (row.total_paid != null) ? row.total_paid : 0;
                              totalPaidAmt = totalPaidAmt + paidAmt;
                              return <tr key={idx}>
                                 <td>
                                    {row.invoice_number}
                                 </td>
                                 <td>{row.due_date}</td>
                                 <td>{row.f_name + ' ' + row.m_name + ' ' + row.l_name}</td>
                                 <td className="text-right">{(row.invoice_amt) ? '$' + row.invoice_amt.toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">{(row.credit_charge_amt) ? '$' + (row.credit_charge_amt).toFixed(2) : '$0.00'}</td>
                                 <td className="text-right">${parseFloat(paidAmt).toFixed(2)}</td>
                              </tr>
                           }.bind(this))
                           }
                           {(this.props.creditCharesList === undefined || this.props.creditCharesList.length == 0) &&
                              <tr>
                                 <td colSpan="15">Sorry, no matching records found</td>
                              </tr>
                           }
                        </tbody>
                        <tfoot>
                           <tr>
                              <th colSpan="3">Total</th>
                              <th className="text-right">${parseFloat(totalInvAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(totalCrdAmt).toFixed(2)}</th>
                              <th className="text-right">${parseFloat(totalPaidAmt).toFixed(2)}</th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>

               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>





         </div>
      );
   }
}
// map state to props
const mapStateToProps = ({ AdminReportReducer, authUser }) => {
   //console.log(AdminReportReducer)

   const { loading, creditCharesList, statusList, providerList, yearStart } = AdminReportReducer;
   const user = authUser.user;
   return { loading, user, creditCharesList, statusList, providerList, yearStart }

}

export default connect(mapStateToProps, {
   customerCreditChargeFilter, customerCreditChargeDownload, customerCreditChargeDownloadXLS
})(CreditChargesReports); 