/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import TableCell from "@material-ui/core/TableCell";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   FormGroup,
   Label,
   Input
} from 'reactstrap';
import FormHelperText from '@material-ui/core/FormHelperText';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';


import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import TextField from '@material-ui/core/TextField';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isEmpty, isDecimals, pointDecimals } from '../../validator/Validator';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   refundAdminInvoiceList, viewInvoice, adminRefundAmount,
} from 'Actions';

class submitInvoiceList extends Component {

   state = {
      currentModule: 20,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      invoiceList: null, // initial user data
      viewDetails: false,
      singleDetails: null,
      viewPayment: false,

      data: {
         note: '',
      },
      dataError: {},
      check_date: '',
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.refundAdminInvoiceList(this.props.match.params.id);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   getMuiTheme = () => createMuiTheme({
      overrides: {
         MaterialDatatableToolbar: {
            left: {
               flex: "0 0 35%"
            },
            actions: {
               flex: "0 0 65%"
            },

         },
         MuiFormLabel: {
            root: {
               'font-size': '14px',
               "font-weight": 600,
               color: '#000000',
               "width": 'unset'
            }
         },
         MuiFormControlLabel: {
            root: {
               "width": 'unset !important'
            }
         }
      }
   })

   componentWillReceiveProps(nextProps) {

      (nextProps.invoiceList) ? this.setState({ invoiceList: nextProps.invoiceList }) : '';
   }

   viewInvoice(invoice_id, provider_id, e) {
      this.props.viewInvoice(invoice_id, provider_id)
   }

   viewDetaisls(details) {
      let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.filter((x) => x.refund_id == details.refund_id);
      details.currentPay = currentPay;
      this.setState({ viewDetails: true, singleDetails: details })
   }
   viewDetaislsClose() {
      this.setState({ viewDetails: false, singleDetails: null, viewPayment: false })
   }
   viewPayment(details) {
      let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.filter((x) => x.refund_id == details.refund_id);
      //console.log(currentPay)
      details.currentPay = currentPay;
      this.setState({ viewPayment: true, singleDetails: details })

   }
   onChnagePlanAction(key, value) {
      let { dataError, data } = this.state;
      switch (key) {
         case 'note':
            if (isEmpty(value)) {
               dataError[key] = "Note can't be empty";
            } else {
               dataError[key] = "";
            }
            break;
      }
      this.setState({
         data: {
            ...this.state.data,
            [key]: value
         }
      });
      this.setState({ dataError: dataError });
   }

   validateAction() {
      return (
         this.state.dataError.note === ''
      )
   }
   paymentConfirm() {
      const { data } = this.state;
      data.refund_invoice_id = this.state.singleDetails.refund_id
      this.props.adminRefundAmount(this.state.data);
      let datas = {
         note: '',
      }
      let dataError = {}
      this.setState({ viewDetails: false, singleDetails: null, viewPayment: false, check_date: '', dataError: dataError, data: datas })
   }

   render() {
      const { invoiceList, loading, selectedUser, editUser, allSelected, selectedInvoices,singleDetails } = this.state;


      this.state.singleDetails && this.state.singleDetails.currentPay.map(function (data, idx) {
         <React.Fragment>
            <tr>
               <td>Paymment Method</td>
               <td colSpan="3">{data.value}</td>
            </tr>
         </React.Fragment>
      })
      var paidAmount = 0;
      if(singleDetails !== null){
         let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.map(function (data, idx) {
            if (data.refund_id == singleDetails.refund_id && data.status == 4) {
               paidAmount += data.paid_amount;
            }
         });
      }
      //const invoiceList = this.props.invoiceList;
      const columns = [
         {
            name: 'Invoice No',
            field: 'invoice_number',
         },
         {
            name: 'Plan No',
            field: 'plan_number',
         },
         {
            name: 'Provider Name',
            field: 'name',
         },
         {
            name: 'Customer Name',
            field: 'total_application',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     (value.f_name + ' ' + value.m_name + ' ' + value.l_name)
                  )
               },
            }
         },
         {
            name: 'Customer Phone',
            field: 'peimary_phone',
         },
         {
            name: 'Refund Amt',
            field: 'refund_due',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     '$' + parseFloat(value.refund_due).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Adjusted Amt',
            field: 'refund_due',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  var paidAmount = 0;
                  let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.map(function (data, idx) {
                     if (data.refund_id == value.refund_id && data.status == 4) {
                        paidAmount += data.paid_amount;
                     }
                  });
                  return (
                     '$' + parseFloat(paidAmount).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Balance Amt',
            field: 'refund_due',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  var paidAmount = 0;
                  let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.map(function (data, idx) {
                     if (data.refund_id == value.refund_id && data.status == 4) {
                        paidAmount += data.paid_amount;
                     }
                  });
                  return (
                     '$' + (parseFloat(value.refund_due)-parseFloat(paidAmount)).toFixed(2)
                  )
               },
            }
         },
         {
            name: 'Type',
            field: 'value',
            options: {
               noHeaderWrap: true,
               filter: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (value.iou_flag == 1) ? 'IOU' : 'Issue Refund'
               }
            }
         },
         {
            name: 'Status',
            field: 'value',
            options: {
               noHeaderWrap: true,
               filter: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return value.value
               }
            }
         },
         {
            name: 'Action',
            field: 'invoice_status',
            options: {
               noHeaderWrap: true,
               filter: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  /*var paidAmount = 0;
                  let currentPay = this.props.invoiceDetails && this.props.invoiceDetails.map(function (data, idx) {
                     if (data.refund_id == value.refund_id && data.status == 4) {
                        paidAmount += data.paid_amount;
                     }
                  });*/
                  if (value.iou_flag == 1) {
                     return (
                        <span className="list-action">
                           <a href="javascript:void(0)" onClick={() => this.viewDetaisls(value)} color="primary" title="View Details">
                              <i className="ti-eye"></i>
                           </a>
                        </span>
                     )
                  } else {
                     return (
                        <span className="list-action">
                           <a href="javascript:void(0)" onClick={() => this.viewDetaisls(value)} color="primary" title="View Details">
                              <i className="ti-eye"></i>
                           </a>
                           {(value.status == 3) ? <a href="javascript:void(0)" onClick={() => this.viewPayment(value)} color="primary" title="View Details">
                              <i className="ti-money"></i>
                           </a> : ''}
   
                        </span>
                     )
                  }
               },
            }

         },
         
      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],
         pagination: true,
         responsive: 'scroll',
         downloadOptions: { filename: 'submittedInvoice.csv' },
      };
      /*<Link to="/provider/invoice/preview-invoice" color="primary" className="caret btn-sm mr-10-custome">Create and Preview</Link>*/
      //console.log(this.props.invoiceDetails)

      return (
         <div className="credit-application application-list">
            <Helmet>
               <title>Health Partner | Payments | Invoice List</title>
               <meta name="description" content="Invoice List" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.refundInvoices" />}
               match={this.props.match}
            />

            <RctCollapsibleCard fullBlock>
               
               {this.props.refundInvoices &&
                  <MuiThemeProvider theme={this.getMuiTheme()}>
                     <MaterialDatatable
                        data={this.props.refundInvoices}
                        columns={columns}
                        options={options}
                     />
                  </MuiThemeProvider>
               }
               
            </RctCollapsibleCard >
            {this.props.loading &&
               <RctSectionLoader />
            }
            <Modal isOpen={this.state.viewDetails} toggle={() => this.viewDetaislsClose()}>
               <ModalHeader toggle={() => this.viewDetaislsClose()}>
                  Refund Invoice Details
                  </ModalHeader>
               <ModalBody>
                  {this.state.singleDetails &&
                     <div className="row">
                        <table className="table table-striped table-hover table-sm table-bordered">
                           <tbody>
                              <tr>
                                 <td>Invoice Number</td>
                                 <td>{this.state.singleDetails.invoice_number}</td>
                                 <td>Plan No</td>
                                 <td>{this.state.singleDetails.plan_number}</td>
                              </tr>
                              <tr>
                                 <td>Refund Amount</td>
                                 <td>{'$' + parseFloat(this.state.singleDetails.refund_due).toFixed(2)}</td>
                                 <td>Status</td>
                                 <td>{this.state.singleDetails.value}</td>
                              </tr>
                              <tr>
                                 <td>Balance Amount</td>
                                 <td>
                                    {
                                    '$' + (parseFloat(this.state.singleDetails.refund_due)-parseFloat(paidAmount)).toFixed(2)
                                    }
                                 </td>
                                 <td>Customer Name</td>
                                 <td>{this.state.singleDetails.f_name + ' ' + this.state.singleDetails.m_name + ' ' + this.state.singleDetails.l_name}</td>
                              </tr>
                              <tr>
                                 <td>Customer Phone</td>
                                 <td colSpan="3">{this.state.singleDetails.peimary_phone}</td>
                              </tr>
                              <tr>
                                 <td>Note</td>
                                 <td colSpan="3">{this.state.singleDetails.comments}</td>
                              </tr>
                           </tbody>
                        </table>
                        {this.state.singleDetails && this.state.singleDetails.currentPay.map(function (data, idx) {
                           return (this.state.singleDetails.iou_flag == 1) ?
                              <React.Fragment key={idx}>
                                 <div className="w-100 text-center"><h4 className="font-weight-bold">Adjustment Details {idx + 1}</h4></div>
                                 <table className="table table-striped table-hover table-sm table-bordered">
                                    <tbody>
                                       <tr >
                                          <td>Invoice Number</td>
                                          <td>{(data.invoice_number) ? data.invoice_number : '-'}</td>
                                          <td>Paid Amount</td>
                                          <td>{(data.paid_amount) ? '$'+parseFloat(data.paid_amount).toFixed(2) : '$0.00'}</td>
                                          <td>Paid Date</td>
                                          <td>{(data.date_paid) ? data.date_paid : '-'}</td>
                                       </tr>

                                    </tbody>
                                 </table>

                              </React.Fragment>
                              :
                              <React.Fragment key={idx}>
                                 <div className="w-100 text-center"><h4 className="font-weight-bold">Payment Details {idx + 1}</h4></div>
                                 <table className="table table-striped table-hover table-sm table-bordered">
                                    <tbody>
                                       <tr >
                                          <td>Paymment Method</td>
                                          <td>{data.value}</td>
                                          <td>Paid Amount</td>
                                          <td>{(data.paid_amount) ? '$' + parseFloat(data.paid_amount).toFixed(2) : '$0.00'}</td>
                                       </tr>
                                       {
                                          (data.payment_method == 3) ?
                                             <tr>
                                                <td>Check No</td>
                                                <td>{data.check_no}</td>
                                                <td>Check Date</td>
                                                <td>{data.check_date}</td>
                                             </tr>
                                             : ''
                                       }
                                       {
                                          (data.payment_method == 1) ?
                                             <React.Fragment>
                                                <tr>
                                                   <td>Bank Name</td>
                                                   <td>{data.ach_bank_name}</td>
                                                   <td>Routing No</td>
                                                   <td>{data.ach_routing_no}</td>
                                                </tr>
                                                <tr>
                                                   <td>Account No</td>
                                                   <td colSpan="3">{data.ach_account_no}</td>
                                                </tr>
                                             </React.Fragment>
                                             : ''

                                       }
                                    </tbody>
                                 </table>
                              </React.Fragment>
                        }.bind(this))
                        }

                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.viewDetaislsClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            <Modal isOpen={this.state.viewPayment} toggle={() => this.viewDetaislsClose()}>
               <ModalHeader toggle={() => this.viewDetaislsClose()}>
                  Payment Confirmation
                  </ModalHeader>
               <ModalBody>
                  {this.state.singleDetails &&
                     <div className="row">
                        <table className="table table-striped table-hover table-sm table-bordered">
                           <tbody>
                              <tr>
                                 <td>Invoice Number</td>
                                 <td>{this.state.singleDetails.invoice_number}</td>
                                 <td>Plan No</td>
                                 <td>{this.state.singleDetails.application_no}</td>
                              </tr>
                              <tr>
                                 <td>Refund Amount</td>
                                 <td>{(this.state.singleDetails.refund_due) ? '$' + parseFloat(this.state.singleDetails.refund_due).toFixed(2) : '$0.00'}</td>
                                 <td>Status</td>
                                 <td>{this.state.singleDetails.value}</td>
                              </tr>
                           </tbody>
                        </table>
                        {this.state.singleDetails && this.state.singleDetails.currentPay.map(function (data, idx) {
                           return <React.Fragment key={idx}>
                              <div className="w-100 text-center"><h4 className="font-weight-bold">Payment Details {idx + 1}</h4></div>
                              <table className="table table-striped table-hover table-sm table-bordered">
                                 <tbody>
                                    <tr >
                                       <td>Paymment Method</td>
                                       <td>{data.value}</td>
                                       <td>Paid Amount</td>
                                       <td>{(data.paid_amount) ? '$' + parseFloat(data.paid_amount).toFixed(2) : '$0.00'}</td>
                                    </tr>
                                    {
                                       (data.payment_method == 3) ?
                                          <tr>
                                             <td>Check No</td>
                                             <td>{data.check_no}</td>
                                             <td>Check Date</td>
                                             <td>{data.check_date}</td>
                                          </tr>
                                          : ''
                                    }
                                    {
                                       (data.payment_method == 1) ?
                                          <React.Fragment>
                                             <tr>
                                                <td>Bank Name</td>
                                                <td>{data.ach_bank_name}</td>
                                                <td>Routing No</td>
                                                <td>{data.ach_routing_no}</td>
                                             </tr>
                                             <tr>
                                                <td>Account No</td>
                                                <td colSpan="3">{data.ach_account_no}</td>
                                             </tr>
                                          </React.Fragment>
                                          : ''

                                    }
                                 </tbody>
                              </table>
                           </React.Fragment>
                        })
                        }

                        <div className="col-md-12">
                           <FormGroup>
                              <Label for="note">Note<span className="required-field">*</span></Label><br />
                              <Input
                                 type="textarea"
                                 name="note"
                                 id="note"
                                 variant="outlined"
                                 value={this.state.data.note}
                                 placeholder="Note"
                                 onChange={(e) => this.onChnagePlanAction('note', e.target.value)}
                              >
                              </Input>
                              {(this.state.dataError.note) ? <FormHelperText>{this.state.dataError.note}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  <Button onClick={() => this.paymentConfirm()} disabled={!this.validateAction()} variant="contained" color="primary" className="text-white mr-10 mb-5" >
                     Confirm
                              </Button>
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.viewDetaislsClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

            


         </div >
      );
   }
}

const mapStateToProps = ({ authUser, providerInvoice }) => {
   const { user } = authUser;

   const { loading, refundInvoices, paymentMethod, invoiceDetails } = providerInvoice;
   return { loading, refundInvoices, paymentMethod, invoiceDetails, user }

}

export default connect(mapStateToProps, {
   refundAdminInvoiceList, viewInvoice, adminRefundAmount
})(submitInvoiceList);