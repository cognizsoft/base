/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';

import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isContainWhiteSpace } from '../../../validator/Validator';

import {
   userTypeList, updateUserType, deleteUserType, insertUserType, lastInsertIdUserType, checkUserTypeExist
} from 'Actions';

import AddNewButton from './AddNewButton';

class UserTypes extends Component {

   state = {
      currentModule: 2,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addNewUserDetail: {
         value: '',
         description: '',
         status: 1,
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         value: ''
      },
      add_err: {

      }

   }

   validateAddSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.value === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.value === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateUserDetails(name, value)
      });
   }

   componentDidMount() {

      this.permissionFilter(this.state.currentModule);
      this.props.userTypeList();

   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);
      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

	/**
	 * On Delete
	 */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }

	/**
	 * Delete User Permanently
	 */
   deleteUserPermanently() {
      const { selectedUser } = this.state;
      let users = this.props.user_type;

      let indexOfDeleteUser = users.indexOf(selectedUser);
      //console.log(indexOfDeleteUser)
      users.splice(indexOfDeleteUser, 1);
      this.refs.deleteConfirmationDialog.close();

      this.props.deleteUserType(selectedUser);

      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false, users, selectedUser: null });
         NotificationManager.success('User Type Deleted!');
      }, 2000);
   }

	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/**
	 * On Reload
	 */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

	/**
	 * On Select User
	 */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }

	/**
	 * On Change Add New User Details
	 */
   onChangeAddNewUserDetails(key, value) {
      //console.log(this.state.addNewUserDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'value':
            if (isEmpty(value)) {
               add_err[key] = "User type can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid User type. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewUserDetail: {
            ...this.state.addNewUserDetail,
            [key]: value
         }
      });
   }

	/**
	 * Add New User
	 */
   addNewUser() {
      const { value, description, status } = this.state.addNewUserDetail;
      if (value !== '' && status !== '') {
         let users = this.props.user_type;


         this.props.insertUserType(this.state.addNewUserDetail);


         this.setState({ addNewUserModal: false, loading: true });
         let self = this;
         let user = {
            value: '',
            description: '',
            status: 1,
            checked: false
         }
         setTimeout(() => {
            //this.setState({addNewUserDetail:''})
            self.setState({ loading: false, addNewUserDetail: user });
            NotificationManager.success('User Type Created!');
         }, 2000);
      }
   }

	/**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
      this.setState({ openViewUserDialog: true, selectedUser: data });
   }

   /**
    * On View User Modal Close
    */
   onViewUserModalClose() {
      this.setState({ openViewUserDialog: false, selectedUser: null })
   }

	/**
	 * On Edit User
	 */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose = () => {
      let upR = { value: '' }
      let addR = {}
      let addNewUserDetail = {
         value: '',
         description: '',
         status: 1,
         checked: false
      };
      this.setState({ addNewUserModal: false, editUser: null, err: upR, add_err: addR, addNewUserDetail:addNewUserDetail })
   }



	/**
	 * On Update User Details
	 */
   onUpdateUserDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'value':
            if (isEmpty(value)) {
               err[fieldName] = "User type can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               err[fieldName] = "Please enter a valid User type. only allow a-z A-Z 0-9 _ -";
            } */else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editUser: {
            ...this.state.editUser,
            [fieldName]: value
         }
      });
   }

	/**
	 * Update User
	 */
   updateUser() {
      const { editUser } = this.state;
      //console.log(editUser)
      let indexOfUpdateUser = '';
      let users = this.props.user_type;
      //console.log(users)
      for (let i = 0; i < users.length; i++) {
         const user = users[i];

         if (user.mdv_id === editUser.mdv_id) {
            indexOfUpdateUser = i
         }
      }

      this.props.updateUserType(editUser);


      users[indexOfUpdateUser] = editUser;
      this.setState({ loading: true, editUser: null, addNewUserModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ users, loading: false });
         NotificationManager.success('User Type Updated!');
      }, 2000);
   }

   //Select All user
   onSelectAllUser(e) {
      const { selectedUsers, users } = this.state;
      let selectAll = selectedUsers < users.length;
      if (selectAll) {
         let selectAllUsers = users.map(user => {
            user.checked = true
            return user
         });
         this.setState({ users: selectAllUsers, selectedUsers: selectAllUsers.length })
      } else {
         let unselectedUsers = users.map(user => {
            user.checked = false
            return user;
         });
         this.setState({ selectedUsers: 0, users: unselectedUsers });
      }
   }

   /*
   * Title :- checkNameExist
   * Descrpation :- This function use for check name exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- May 5,2019
   */
   checkNameExist(value, md_id) {
      this.props.checkUserTypeExist(value, md_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { err } = this.state;
      (nextProps.typeExist && nextProps.isEdit == 0) ? add_err['value'] = "User type already exists" : '';
      (nextProps.typeExist && nextProps.isEdit == 1) ? err['value'] = "User type already exists" : '';
      this.setState({ add_err: add_err });
      this.setState({ err: err });

   }
   render() {
      const { add_err, err, users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      //console.log('render')
      //console.log(this.props.user_type_last_insert_id)
      const userType = this.props.user_type;

      const columns = [

         {
            name: 'ID',
            field: 'mdv_id'
         },
         {
            name: 'User Type',
            field: 'value',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: true,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  //console.log(value)
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewUserDetail(value)} title="View User Type"><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditUser(value)} title="Edit User Type"><i className="ti-pencil"></i></a> : ''}
                     </div>
                  );
               }
            }
         }

      ];

      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               (this.state.currentPermision.add) ? <AddNewButton opnAddNewUserModal={this.opnAddNewUserModal.bind(this)} /> : ''
            );
         }
      };
      return (

         <div className="user-management">
            <Helmet>
               <title>Health Partner | Users | User Types</title>
               <meta name="description" content="Health Partner User Type" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.usersType" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  title={""}
                  data={userType}
                  columns={columns}
                  options={options}
               />
            </RctCollapsibleCard>

            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete user permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  {editUser === null ?
                     'Add User Type' : 'Update User Type'
                  }
               </ModalHeader>
               <ModalBody>
                  {editUser === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewUserDetails={this.state.addNewUserDetail}
                        onChangeAddNewUserDetails={this.onChangeAddNewUserDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                     : <UpdateUserForm updateErr={err} user={editUser} onUpdateUserDetail={this.onUpdateUserDetails.bind(this)} checkNameExist={this.checkNameExist.bind(this)} />
                  }
               </ModalBody>
               <ModalFooter>
                  {editUser === null ?
                     <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.addNewUser()}
                        disabled={!this.validateAddSubmit()}>Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        disabled={!this.validateUpdateSubmit()}
                        onClick={() => this.updateUser()}>Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'User Type View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">
                                 <div className="colmn-row"><span className="fw-bold first-colmn">User Type:</span> <span className="second-colmn">{selectedUser.value}</span></div>

                                 <div className="colmn-row"><span className="fw-bold first-colmn">Status:</span> <span className="second-colmn">{(selectedUser.status == 1) ? 'Active' : 'Inactive'}</span></div>

                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ userType, authUser }) => {
   //console.log('index----')

   const { user_type, loading, typeExist, isEdit } = userType;
   const user = authUser.user;
   return { user_type, loading, user, typeExist, isEdit }

}

export default connect(mapStateToProps, {
   userTypeList, updateUserType, deleteUserType, insertUserType, lastInsertIdUserType, checkUserTypeExist
})(UserTypes);