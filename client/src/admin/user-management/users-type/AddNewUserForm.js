/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewUserDetails, onChangeAddNewUserDetails, checkNameExist }) => (
    <Form>
       
  
        <FormGroup>
            <Label for="type">User Type<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="value"
                id="user_type"
                fullWidth
                variant="outlined"
                placeholder="Enter User Type"
                value={addNewUserDetails.value}
                error={(addErr.value)?true:false}
                helperText={addErr.value}
                onChange={(e) => onChangeAddNewUserDetails('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="user_type_desc">Description</Label>
                <Input
                    type="textarea"
                    name="user_type_desc"
                    id="user_type_desc"
                    placeholder="Enter User Type Desc"
                    value={addNewUserDetails.user_type_desc}
                    onChange={(e) => onChangeAddNewUserDetails('user_type_desc', e.target.value)}
                />
        </FormGroup>
        <Label for="userEmail">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(addNewUserDetails.status == 1) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(addNewUserDetails.status == 0) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
    </Form>
);

export default AddNewUserForm;
