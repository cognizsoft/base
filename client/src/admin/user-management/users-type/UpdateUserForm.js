/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ updateErr, user, onUpdateUserDetail, checkNameExist }) => (

    <Form>
        <FormGroup>
            <Label for="userEmail">User Type<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="value"
                id="user_type"
                placeholder="User Type"
                fullWidth
                variant="outlined"
                value={user.value}
                error={(updateErr.value)?true:false}
                helperText={updateErr.value}
                onChange={(e) => onUpdateUserDetail('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,user.mdv_id)}
            />
        </FormGroup>
       <FormGroup>
            <Label for="user_type_desc">Description</Label>
                <Input
                    type="textarea"
                    name="description"
                    id="user_type_desc"
                    placeholder="Enter User Type Desc"
                    value={user.description}
                    onChange={(e) => onUpdateUserDetail('description', e.target.value)}
                />
        </FormGroup>
        <Label for="userEmail">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(user.status == 1) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(user.status == 0) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
        
        
    </Form>
);

export default UpdateUserForm;
