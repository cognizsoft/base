/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UnlockUserForm = ({ addErr, questionList, onChangeUnlockUser, check_fullname, check_dob, check_ssn, check_question, check_answer, lockUserDetail, selectedQuestion }) => (
    <Form>
        <div className="row">
            <div className="col-md-12">
               <FormGroup tag="fieldset" className="check_fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_fullname != '') ? check_fullname : ''}
                     name="check_fullname" 
                     onChange={(e) => onChangeUnlockUser('check_fullname', e.target.value)}
                    />{' '} <span className="checkLabel">Full Name</span> <span className="checkName">{(lockUserDetail) ? lockUserDetail[0].f_name+ ' '+lockUserDetail[0].m_name+' '+lockUserDetail[0].l_name : ''}</span>
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
               <FormGroup tag="fieldset" className="check_fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_dob != '') ? check_dob : ''}
                     name="check_dob" 
                     onChange={(e) => onChangeUnlockUser('check_dob', e.target.value)}
                    />{' '} <span className="checkLabel">DOB</span> <span className="checkDob">{(lockUserDetail && lockUserDetail[0].dob) ? lockUserDetail[0].dob.toString() : 'N/A'}</span>
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
               <FormGroup tag="fieldset" className="check_fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_ssn != '') ? check_ssn : ''}
                     name="check_ssn" 
                     onChange={(e) => onChangeUnlockUser('check_ssn', e.target.value)}
                    />{' '} <span className="checkLabel">SSN</span> <span className="checkSsn">{(lockUserDetail && lockUserDetail[0].ssn) ? lockUserDetail[0].ssn.replace(/.(?=.{4})/g, 'x') : 'N/A'}</span>
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>

            <div className="col-md-12">
               <FormGroup tag="fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_question != '') ? check_question : ''}
                     name="check_question" 
                     onChange={(e) => onChangeUnlockUser('check_question', e.target.value)}
                    />{' '} Select Questions
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
                <FormGroup>
                    
                    <Input
                        type="select"
                        name="questions"
                        id="questions"
                        value={(selectedQuestion.questions) ? selectedQuestion.questions : ''}
                        onChange={(e) => onChangeUnlockUser('questions', e.target.value)}

                    >

                        <option value="">Select</option>
                        {questionList && questionList.map((que, key) => (
                            <option value={que.security_questions_id} key={key}>{que.name}</option>
                        ))}
                    </Input>
                    {(addErr.questions) ? <FormHelperText>{addErr.questions}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
               <FormGroup tag="fieldset">
                  <FormGroup check>
                    <Label check className="font-weight-bold">
                    <Input 
                     type="checkbox" 
                     className="agreement_check"
                     value={(check_answer != '') ? check_answer : ''}
                     name="check_answer" 
                     onChange={(e) => onChangeUnlockUser('check_answer', e.target.value)}
                    />{' '} Answer
                    </Label>
                  </FormGroup>
               </FormGroup>
            </div>
            <div className="col-md-12">
                <FormGroup>
                    
                    <TextField
                        type="text"
                        name="answer"
                        id="answer"
                        fullWidth
                        inputProps={{
                            readOnly: true
                        }}
                        value={(selectedQuestion.answer) ? selectedQuestion.answer : ''}
                        variant="outlined"
                        placeholder="Answer"
                        error={(addErr.answer) ? true : false}
                        helperText={addErr.answer}
                        onChange={(e) => onChangeUnlockUser('answer', e.target.value)}
                    />
                </FormGroup>
            </div>
                        
        </div>
    </Form>
);

export default UnlockUserForm;
