/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const UpdateUserForm = ({ updateErr, user, onUpdateUserDetail,userType,checkNameExist }) => (

    <Form>
        <FormGroup>
            <Label for="userType">Select User Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="type_id"
                id="type_id"
                placeholder="Enter Type"
                defaultValue={user.type_id}
                onChange={(e) => onUpdateUserDetail('type_id', e.target.value)}
            >

                <option value="">Select</option>
                {userType && userType.map((type, key) => (
                    <option value={type.mdv_id} key={key}>{type.value}</option>
                ))}
            </Input>
            {(updateErr.type_id) ? <FormHelperText>{updateErr.type_id}</FormHelperText> : ''}
        </FormGroup>
        <FormGroup>
            <Label for="user_role">User Role<span className="required-field">*</span></Label><br />
            <TextField
                type="text"
                name="value"
                id="user_role"
                fullWidth
                variant="outlined"
                placeholder="User Role"
                value={user.value}
                error={(updateErr.value) ? true : false}
                helperText={updateErr.value}
                onChange={(e) => onUpdateUserDetail('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,user.user_type_role_id)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="user_role_desc">Description</Label>
            <Input
                type="textarea"
                name="description"
                id="user_role_desc"
                placeholder="Enter User Role Desc"
                value={user.description}
                onChange={(e) => onUpdateUserDetail('description', e.target.value)}
            />
        </FormGroup>
        <Label for="status">Status</Label><br />
        <FormGroup check>
            <Label check>
                <Input
                    type="radio"
                    name="status"
                    value={1}
                    checked={(user.status == 1) ? true : false}
                    onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                    type="radio"
                    name="status"
                    value={0}
                    checked={(user.status == 0) ? true : false}
                    onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>


    </Form>
);

export default UpdateUserForm;
