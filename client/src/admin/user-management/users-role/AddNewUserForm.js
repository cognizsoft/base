/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const AddNewUserForm = ({ addErr, addNewUserDetails, onChangeAddNewUserDetails, userType, checkNameExist }) => (
    <Form>


        <FormGroup>
            <Label for="userType">Select User Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="mdv_type_id"
                id="mdv_type_id"
                placeholder="Enter Type"
                onChange={(e) => onChangeAddNewUserDetails('mdv_type_id', e.target.value)}
            >

                <option value="">Select</option>
                {userType && userType.map((type, key) => (
                    <option value={type.mdv_id} key={key}>{type.value}</option>
                ))}
            </Input>
            {(addErr.mdv_type_id) ? <FormHelperText>{addErr.mdv_type_id}</FormHelperText> : ''}
        </FormGroup>
        <FormGroup>
            <Label for="user_type">User Role<span className="required-field">*</span></Label><br />
            <TextField
                type="text"
                name="value"
                id="user_role"
                fullWidth
                variant="outlined"
                placeholder="Enter User Role"
                value={addNewUserDetails.value}
                error={(addErr.value) ? true : false}
                helperText={addErr.value}
                onChange={(e) => onChangeAddNewUserDetails('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="user_role_desc">Description</Label>
            <Input
                type="textarea"
                name="user_role_desc"
                id="user_role_desc"
                placeholder="Enter User Role Desc"
                value={addNewUserDetails.user_role_desc}
                onChange={(e) => onChangeAddNewUserDetails('user_role_desc', e.target.value)}
            />
        </FormGroup>
        <Label for="status">Status</Label><br />
        <FormGroup check>
            <Label check>
                <Input
                    type="radio"
                    name="status"
                    value={1}
                    checked={(addNewUserDetails.status == 1) ? true : false}
                    onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                    type="radio"
                    name="status"
                    value={0}
                    checked={(addNewUserDetails.status == 0) ? true : false}
                    onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
    </Form>
);

export default AddNewUserForm;
