/**
 * Users Routes
 */
/* eslint-disable */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// async components
import {
    AsyncUsersTypeComponent,
    AsyncUsersRoleComponent,
    AsyncUserManagementComponent
} from 'Components/AsyncComponent/AsyncComponent';

const Forms = ({ match }) => (
    <div className="content-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/users`} />
            <Route path={`${match.url}/users`} component={AsyncUserManagementComponent} />
            <Route path={`${match.url}/users-type`} component={AsyncUsersTypeComponent} />
            <Route path={`${match.url}/users-role`} component={AsyncUsersRoleComponent} />            
        </Switch>
    </div>
);

export default Forms;
