/**
 * Support Request
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';

// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import ApplicationChart from 'Components/Charts/ApplicationChart';
import { Redirect,Link } from 'react-router-dom';
// intl messagess
import IntlMessages from 'Util/IntlMessages';
import { connect } from 'react-redux';
import {
   hpsDashboardApplication
} from 'Actions';
class ApplicationDashboard extends Component {
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- June 24,2019
   */
   componentDidMount() {
      this.props.hpsDashboardApplication();
   }
   render() {
      
      return (
         <div className="support-widget-wrap">
            <div className="text-center py-10">
               <ApplicationChart />
            </div>
            <List className="list-unstyled p-0">
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsAccepted" /></p>
                  <Badge color="primary" className="px-4">{(this.props.applicationDetails)?this.props.applicationDetails['approved']:0}</Badge>
                  <Link to={`/admin/credit-applications/1`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsDeclined" /></p>
                  <Badge color="warning" className="px-4">{(this.props.applicationDetails)?this.props.applicationDetails['rejected']:0}</Badge>
                  <Link to={`/admin/credit-applications/0`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsPending" /></p>
                  <Badge color="info" className="px-4">{(this.props.applicationDetails)?this.props.applicationDetails['pending']:0}</Badge>
                  <Link to={`/admin/credit-applications/3`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title"><IntlMessages id="widgets.dashboardApplicationsManual" /></p>
                  <Badge color="dark" className="px-4">{(this.props.applicationDetails)?this.props.applicationDetails['manual']:0}</Badge>
                  <Link to={`/admin/credit-applications/2`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
            </List>

         </div>
      );
   }
}

const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, applicationDetails } = DashboardDetails;
   return { loading, applicationDetails }

}

export default connect(mapStateToProps, {
   hpsDashboardApplication
})(ApplicationDashboard);
