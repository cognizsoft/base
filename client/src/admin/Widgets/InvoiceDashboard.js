/**
 * Support Request
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import CustomerInvoiceChart from 'Components/Charts/CustomerInvoiceChart';
import { Redirect, Link } from 'react-router-dom';
// intl messagess
import IntlMessages from 'Util/IntlMessages';

class InvoiceDashboard extends Component {
   
   render() {
      
      return (
         <div className="support-widget-wrap">
            <div className="text-center py-10">
               <CustomerInvoiceChart />
            </div>
            <List className="list-unstyled p-0">
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoicePaid" /></p>
                  <Badge color="primary" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['paid'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/1`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoiceProgress" /></p>
                  <Badge color="warning" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['progress'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/2`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoicePartial" /></p>
                  <Badge color="success" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['partial'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/4`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoiceDue" /></p>
                  <Badge color="purple" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['due'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/3`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem> 
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoiceDeferred" /></p>
                  <Badge color="danger" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['deferred'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/5`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoiceCancelled" /></p>
                  <Badge color="dark" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['cancelled'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/0`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
               <ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
                  <p className="mb-0 content-title w-25"><IntlMessages id="widgets.dashboardCustomerInvoiceRefunded" /></p>
                  <Badge color="info" className="px-4">{(this.props.customerStatus) ? this.props.customerStatus['refunded'] : 0}</Badge>
                  <Link to={`/admin/accounts/week-month-reports/6`}>
                     <IconButton color="default">
                        <i className="ti-eye"></i>
                     </IconButton>
                  </Link>
               </ListItem>
            </List>

         </div>
      );
   }
}


const mapStateToProps = ({ DashboardDetails }) => {
   const { loading, customerStatus } = DashboardDetails;
   return { loading, customerStatus }
}

export default connect(mapStateToProps, {

})(InvoiceDashboard);