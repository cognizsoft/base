/**
 * Recent Orders
 */
import React, { Component } from 'react';
import { Badge } from 'reactstrap';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';

// card component
import { RctCardFooter } from 'Components/RctCard';

//chart component
import SupportChart from 'Components/Charts/SupportChart';
import { Redirect,Link } from 'react-router-dom';
// intl messagess
import IntlMessages from 'Util/IntlMessages';
import { connect } from 'react-redux';

class SupportChartDashboard extends Component {

	state = {
		recentOrders: null
	}

	render() {
		return (
			<div className="support-widget-wrap">
				<div className="text-center py-10">
					<SupportChart />
				</div>
				<List className="list-unstyled p-0">
					<ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
						<p className="mb-0 content-title"><IntlMessages id="widgets.supportHold" /></p>
						<Badge color="primary" className="px-4">{(this.props.supportStatus) ? this.props.supportStatus['Hold'] : 0}</Badge>
						<Link to={`/admin/customer-support/all/2`}>
							<IconButton color="default">
								<i className="ti-eye"></i>
							</IconButton>
						</Link>
					</ListItem>
					<ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
						<p className="mb-0 content-title"><IntlMessages id="widgets.supportClosed" /></p>
						<Badge color="warning" className="px-4">{(this.props.supportStatus) ? this.props.supportStatus['Closed'] : 0}</Badge>
						<Link to={`/admin/customer-support/all/0`}>
							<IconButton color="default">
								<i className="ti-eye"></i>
							</IconButton>
						</Link>
					</ListItem>
					<ListItem className="bg-light px-15 py-0 d-flex justify-content-between align-content-center">
						<p className="mb-0 content-title"><IntlMessages id="widgets.supportOpen" /></p>
						<Badge color="info" className="px-4">{(this.props.supportStatus) ? this.props.supportStatus['open'] : 0}</Badge>
						<Link to={`/admin/customer-support/all/1`}>
							<IconButton color="default">
								<i className="ti-eye"></i>
							</IconButton>
						</Link>
					</ListItem>
					<ListItem className="px-15 py-0 d-flex justify-content-between align-content-center">
						<p className="mb-0 content-title"><IntlMessages id="widgets.supportFollowUp" /></p>
						<Badge color="dark" className="px-4">{(this.props.supportStatus) ? this.props.supportStatus['FollowUp'] : 0}</Badge>
						<Link to={`/admin/customer-support/all/3`}>
							<IconButton color="default">
								<i className="ti-eye"></i>
							</IconButton>
						</Link>
					</ListItem>
				</List>

			</div>
		);
	}
}

const mapStateToProps = ({ DashboardDetails }) => {
	const { loading, customerSupport, supportStatus } = DashboardDetails;
	
	return { loading, customerSupport, supportStatus }

}
//export default SupportDashboard;
export default connect(mapStateToProps, {
})(SupportChartDashboard);
