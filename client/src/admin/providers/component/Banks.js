/**
 * Messages Page
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Input,
   InputGroup,
   InputGroupAddon,
   FormGroup,
   Label
} from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

const Banks = ({ providerBank }) => (
         <div className="messages-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">
                <h3 className="p-view-title">Bank Details</h3>

                 {providerBank && providerBank.map((bank, key) => (
                    <div className="p-view-bank mb-15" key={key}>
                      <div className="view-box">
                        <div className="width-50">
                          <table>
                              <tbody>
                              <tr>
                                <td className="fw-bold">Location Name:</td>
                                <td>{bank.location_name}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Bank Name: </td>
                                <td>{bank.bank_name}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Bank Address: </td>
                                <td>{bank.bank_address}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Routing Number:</td>
                                <td>{bank.rounting_no}</td>
                              </tr>
                              </tbody>
                          </table>
                        </div>
                        <div className="width-50">
                          <table>
                              <tbody>
                              <tr>
                                <td className="fw-bold">Bank A/C#:</td>
                                <td>{bank.bank_accout}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Name on Account:</td>
                                <td>{bank.name_on_account}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold">Bank A/C Type:</td>
                                <td>{bank.bank_account_type}</td>
                              </tr>
                              <tr>
                                <td className="fw-bold"></td>
                                <td></td>
                              </tr>
                              
                              </tbody>
                          </table>
                        </div>

                      </div>
                     </div>
                ))}

               </div>

            </div>
         </div>
      );
export default Banks;