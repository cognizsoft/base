/**
 * Messages Page
 */
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Avatar from '@material-ui/core/Avatar';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { URL } from '../../../apifile/URL';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Input,
   InputGroup,
   InputGroupAddon,
   FormGroup,
   Label
} from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

const DocumentsDetails = ({ providerDocument, opnDocFileModal, providerAgreement, opnDocAgreeFileModal }) => (
         <div className="messages-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
                 
               <div className="view-section-inner">
                  <h3 className="p-view-title">Documents</h3>

                <div className="view-box">
                  <div className="width-100">
                    <table>
                        <thead>
                          <tr>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Documents</th>
                          </tr>
                        </thead>
                        <tbody>
                         {providerDocument && providerDocument.map((document, key) => (
                                    
                            <tr key={key}>
                              <td>{document.file_type}</td>
                              <td>{document.document_name}</td>
                              <td className="p-10">
                                {(document !== undefined && document.document_upload) ? <img src={document.document_upload} width="100" onClick={()=>opnDocFileModal(document.document_upload, document.document_name)}/> : '-'}
                              </td>
                            </tr>
                            
                          ))}

                            {providerAgreement &&
                              <tr>
                                <td>{providerAgreement[0].document_name}</td>
                                <td>{providerAgreement[0].document_name}</td>
                                <td className="p-10">
                                  <img src={providerAgreement[0].document_upload} width="100" onClick={()=> opnDocAgreeFileModal(providerAgreement[0].item_id, providerAgreement[0].document_name)}/>
                                </td>
                              </tr>
                            }

                        </tbody>
                    </table>
                  </div>
                   
                </div>

               </div>

            </div>
         </div>
      );
export default DocumentsDetails;