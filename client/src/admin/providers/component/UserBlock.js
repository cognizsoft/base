/**
 * User Block
 */
import React, { Component } from 'react';

const UserBlock = ({ customerDetails }) => (
            <div className="profile-top mb-20">
                <img src={require('Assets/img/profile-bg.jpg')} alt="profile banner" className="img-fluid" width="1920" height="345" />
                <div className="profile-content">
                    <div className="media">
                        <img src={require('Assets/avatars/avatar-default.png')} alt="user profile" className="rounded-circle mr-30 bordered" width="140" height="140" />
                        <div className="media-body pt-50">
                            <div className="mb-20">
                                <h2 className="user-full-name">{customerDetails.f_name+' '+customerDetails.l_name}</h2>
                                <p>{customerDetails.email}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

export default UserBlock;
