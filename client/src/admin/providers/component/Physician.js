/**
 * Address Page
 */
import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import {
   FormGroup,
   Form,
   Label,
   Input,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
import { Collapse } from 'reactstrap';
import classnames from 'classnames';

import CircularProgress from '@material-ui/core/CircularProgress';
import { NotificationManager } from 'react-notifications';


// intl messages
import IntlMessages from 'Util/IntlMessages';

import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

const Physician = ({ providerPhysician }) => (
         <div className="address-wrapper d-flex">
            <div className="modal-body page-form-outer view-section">
               
                  <div className="view-section-inner">

                    <div className="view-box">

                      <div className="fee-box mb-15">
                        <h3 className="p-view-title">Physicians Speciality</h3>
                        <div className="width-100">
                        
                        <table>
                            <thead>
                              <tr>
                                <th>Location Name</th>
                                <th>Physician Name</th>
                                <th>Speciality</th>
                                <th>Procedure</th>
                              </tr>
                            </thead>
                            <tbody>
                        {providerPhysician && providerPhysician.map((physician, key) => (
                          
                          <tr key={key}>
                            <td>{physician.spl_location_name}</td>
                            <td>{physician.doctor_f_name+' '+physician.doctor_l_name}</td>
                            <td>{physician.speciality}</td>
                            <td>{physician.procedure_name}</td>
                          </tr>
                            
                            
                        ))}
                            </tbody>
                        </table>
                        </div>
                      </div>

                    </div>

                  </div>
              
            </div>
         </div>
      );
export default Physician;
