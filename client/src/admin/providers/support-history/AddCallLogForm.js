/**
 * Add New User Form
 */
import React from 'react';
import { Form, Label, FormGroup, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddCallLogForm = ({ 
    addErr, 
    addCallLogDetails, 
    onChangeAddCallLogDetails, 
    DatePicker, 
    dateTimeStartDate, 
    followUpDateTimeStartDate, 
    masterTicketSource, 
    providerApplication, 
    providerInvoice,
    providerDetails,
}) => (
    <div>
        <div className="row">
            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label className="d-inline w-auto label-auto">Provider Name: </Label>
                    {providerDetails[0].provider_name}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label className="d-inline w-auto label-auto">Provider A/C: </Label>
                    {providerDetails[0].provider_ac}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label className="d-inline w-auto label-auto">Provider Phone: </Label>
                    {providerDetails[0].provider_phone_no}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label>Ticket Source: </Label>
                    {masterTicketSource && masterTicketSource.map((source, key) => (
                        <FormGroup check key={key}>
                            <Label check>
                                <Input
                                    type="radio"
                                    name="ticket_source"
                                    value={source.status_id}
                                    checked={(addCallLogDetails.ticket_source == source.status_id) ? true : false}
                                    onChange={(e) => onChangeAddCallLogDetails('ticket_source', e.target.value)}
                                />{' '}
                                {source.value}
                            </Label>
                        </FormGroup>
                    ))}

                </FormGroup>
            </div>

            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label>Incoming Call Flag?: </Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="call_type"
                                value="Yes"
                                checked={(addCallLogDetails.call_type == "Yes") ? true : false}
                                onChange={(e) => onChangeAddCallLogDetails('call_type', e.target.value)}
                            />{' '}
                            Yes
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="call_type"
                                value="No"
                                checked={(addCallLogDetails.call_type == "No") ? true : false}
                                onChange={(e) => onChangeAddCallLogDetails('call_type', e.target.value)}
                            />{' '}
                            No
                        </Label>
                    </FormGroup>
                </FormGroup>
            </div>

            <div className="col-md-6">
                <FormGroup tag="fieldset" className="ticket-related">
                    <Label>Ticket related to: </Label>
                    <FormGroup check className="mr-20">
                        <Label check>
                            <Input
                                type="checkbox"
                                id="check-ticket-related"
                                checked={(addCallLogDetails.ticket_related_ac == 1) ? true : false}
                                value={(addCallLogDetails.ticket_related_ac != '') ? addCallLogDetails.ticket_related_ac : ''}
                                name="ticket_related_ac"
                                onChange={(e) => onChangeAddCallLogDetails('ticket_related_ac', e.target.value)}
                            />{' '}
                         Profile
                        </Label>
                        {(addErr.ticket_related_ac != '') ?
                            <FormHelperText>{addErr.ticket_related_ac}</FormHelperText>
                            : ''}
                    </FormGroup>

                    <FormGroup check className="mr-20">
                        <Label check>
                            <Input
                                type="checkbox"
                                id="check-ticket-related"
                                disabled={(!providerApplication.length > 0) ? true : false}
                                checked={(addCallLogDetails.ticket_related_app == 1) ? true : false}
                                value={(addCallLogDetails.ticket_related_app != '') ? addCallLogDetails.ticket_related_app : ''}
                                name="ticket_related_app"
                                onChange={(e) => onChangeAddCallLogDetails('ticket_related_app', e.target.value)}
                            />{' '}
                         Application
                        </Label>
                        {(addErr.ticket_related_app != '') ?
                            <FormHelperText>{addErr.ticket_related_app}</FormHelperText>
                            : ''}
                    </FormGroup>

                    <FormGroup check className="mr-20">
                        <Label check>
                            <Input
                                type="checkbox"
                                id="check-ticket-related"
                                disabled={(!providerInvoice.length > 0) ? true : false}
                                checked={(addCallLogDetails.ticket_related_invoice == 1) ? true : false}
                                value={(addCallLogDetails.ticket_related_invoice != '') ? addCallLogDetails.ticket_related_invoice : ''}
                                name="ticket_related_invoice"
                                onChange={(e) => onChangeAddCallLogDetails('ticket_related_invoice', e.target.value)}
                            />{' '}
                         Invoice
                        </Label>
                        {(addErr.ticket_related_invoice != '') ?
                            <FormHelperText>{addErr.ticket_related_invoice}</FormHelperText>
                            : ''}
                    </FormGroup>
                </FormGroup>
            </div>

            {addCallLogDetails.ticket_related_app == 1 &&
                <div className="col-md-3">
                    <FormGroup>
                        <Label for="application_no">Application No:</Label>
                        <Input
                            type="select"
                            name="application_no"
                            id="application_no"
                            placeholder=""
                            value={addCallLogDetails.application_no}
                            onChange={(e) => onChangeAddCallLogDetails('application_no', e.target.value)}
                        >
                            <option value="">Select Application</option>
                            {providerApplication && providerApplication.map((app, key) => (
                                <option value={app.application_id} key={key}>
                                    {app.application_no}
                                </option>
                            ))}

                        </Input>
                        {(addErr.application_no) ? <FormHelperText>{addErr.application_no}</FormHelperText> : ''}
                    </FormGroup>
                </div>
            }

            {addCallLogDetails.ticket_related_invoice == 1 &&
                <div className="col-md-3">
                    <FormGroup>
                        <Label for="invoice_no">Invoice No:</Label>
                        <Input
                            type="select"
                            name="invoice_no"
                            id="invoice_no"
                            placeholder=""
                            value={addCallLogDetails.invoice_no}
                            onChange={(e) => onChangeAddCallLogDetails('invoice_no', e.target.value)}
                        >
                            <option value="">Select Invoice</option>
                            {providerInvoice && providerInvoice.map((invoice, key) => (
                                <option value={invoice.provider_invoice_id} key={key}>
                                    {invoice.invoice_number}
                                </option>
                            ))}

                        </Input>
                        {(addErr.invoice_no) ? <FormHelperText>{addErr.invoice_no}</FormHelperText> : ''}
                    </FormGroup>
                </div>
            }
        </div>

        <div className="row">
            <div className="col-md-6">
                <FormGroup>
                    <Label for="caller_name">Caller Name:</Label>
                    <TextField
                        type="text"
                        name="caller_name"
                        id="caller_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Caller Name"
                        value={addCallLogDetails.caller_name}
                        error={(addErr.caller_name) ? true : false}
                        helperText={addErr.caller_name}
                        onChange={(e) => onChangeAddCallLogDetails('caller_name', e.target.value)}
                    />

                </FormGroup>
            </div>

            <div className="col-md-6">
                <FormGroup>
                    <Label for="call_date_time">Call Date & Time: </Label>
                    <DatePicker
                        name="call_date_time"
                        id="call_date_time"
                        selected={dateTimeStartDate}
                        placeholderText="DD/MM/YYYY HH:MM:SS"
                        autocomplete={false}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        timeCaption="Time"
                        dateFormat="dd/MM/yyyy h:mm:ss"
                        onChange={(e) => onChangeAddCallLogDetails('call_date_time', e)}
                    />

                    {(addErr.call_date_time) ? <FormHelperText>{addErr.call_date_time}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-6">
                <FormGroup>
                    <Label for="subject">Subject:</Label>
                    <TextField
                        type="text"
                        name="subject"
                        id="subject"
                        fullWidth
                        variant="outlined"
                        placeholder="Subject"
                        value={addCallLogDetails.subject}
                        error={(addErr.subject) ? true : false}
                        helperText={addErr.subject}
                        onChange={(e) => onChangeAddCallLogDetails('subject', e.target.value)}
                    />

                </FormGroup>
            </div>

            <div className="col-md-6">
                <FormGroup>
                    <Label for="hps_agent_name">HPS Agent Name:</Label>
                    <TextField
                        type="text"
                        name="hps_agent_name"
                        id="hps_agent_name"
                        fullWidth
                        variant="outlined"
                        placeholder="Hps Agent Name"
                        value={addCallLogDetails.hps_agent_name}
                        error={(addErr.hps_agent_name) ? true : false}
                        helperText={addErr.hps_agent_name}
                        onChange={(e) => onChangeAddCallLogDetails('hps_agent_name', e.target.value)}
                    />

                </FormGroup>
            </div>
        </div>

        <div className="row">
            <div className="col-md-12">
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input
                        type="textarea"
                        name="description"
                        id="description"
                        placeholder="Enter Description"
                        value={addCallLogDetails.description}
                        onChange={(e) => onChangeAddCallLogDetails('description', e.target.value)}
                    />
                </FormGroup>
            </div>
        </div>

        <div className="row">
            <div className="col-md-4">
                <FormGroup tag="fieldset" className="follow-up-needed">
                    <Label for="follow_up"></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="checkbox"
                                id="follow-up-needed"
                                checked={(addCallLogDetails.follow_up == 1) ? true : false}
                                value={(addCallLogDetails.follow_up != '') ? addCallLogDetails.follow_up : ''}
                                name="follow_up"
                                onChange={(e) => onChangeAddCallLogDetails('follow_up', e.target.value)}
                            />{' '}
                         Follow up needed?
                        </Label>
                        {(addErr.follow_up != '') ?
                            <FormHelperText>{addErr.follow_up}</FormHelperText>
                            : ''}
                    </FormGroup>
                </FormGroup>
            </div>
            <div className="col-md-4">
                <FormGroup tag="fieldset" className="follow-up-needed">
                    <Label for="follow_up"></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="checkbox"
                                id="follow-up-needed"
                                checked={(addCallLogDetails.note == 1) ? true : false}
                                value={(addCallLogDetails.note != '') ? addCallLogDetails.note : ''}
                                name="follow_up"
                                onChange={(e) => onChangeAddCallLogDetails('note', e.target.value)}
                            />{' '}
                         Note
                        </Label>
                        {(addErr.note != '') ?
                            <FormHelperText>{addErr.note}</FormHelperText>
                            : ''}
                    </FormGroup>
                </FormGroup>
            </div>

            {addCallLogDetails.follow_up == 1 &&
                <div className="col-md-4">
                    <FormGroup>
                        <Label for="follow_up_date_time">Follow up Date & Time: </Label>
                        <DatePicker
                            name="follow_up_date_time"
                            id="follow_up_date_time"
                            selected={followUpDateTimeStartDate}
                            minDate={new Date()}
                            placeholderText="DD/MM/YYYY HH:MM:SS"
                            autocomplete={false}
                            showTimeSelect
                            timeFormat="HH:mm"
                            timeIntervals={15}
                            timeCaption="Time"
                            dateFormat="dd/MM/yyyy h:mm:ss"
                            onChange={(e) => onChangeAddCallLogDetails('follow_up_date_time', e)}
                        />

                        {(addErr.follow_up_date_time) ? <FormHelperText>{addErr.follow_up_date_time}</FormHelperText> : ''}
                    </FormGroup>
                </div>
            }
        </div>
    </div>
);

export default AddCallLogForm;
