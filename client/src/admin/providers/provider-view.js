/**
 * User Profile Page
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';

import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
// Components
import Provider from './component/Provider';
import Locations from './component/Locations';
import Banks from './component/Banks';
import AdditionalInfo from './component/AdditionalInfo';
import Physician from './component/Physician';
import UserBlock from './component/UserBlock';
import Documents from './component/Documents';
import Users from './component/Users';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// rct card box
import { RctCard } from 'Components/RctCard';
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter
} from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import {
   ProviderDetails, downloadOneDrive, createSupportTicket, getAllTickets
} from 'Actions';
import { patientID } from '../../apifile';
import AddCallLogButton from './support-history/AddCallLogButton';
import AddCallLogForm from './support-history/AddCallLogForm';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MaterialDatatable from "material-datatable";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { isEmpty, isMaster, isNumeric, isObjectEmpty } from '../../validator/Validator';
import { NotificationManager } from 'react-notifications';
import { Link } from 'react-router-dom';

function getSteps() {
   return ['Provider', 'Locations', 'Bank Details', 'Additional Info', 'Physicians', 'Documents', 'Users'];
}

class ProviderView extends Component {

   state = {
      showSsn: false,
      opnDocFileModal: false,
      imgPath: '',
      imgName: '',
      activeStep: 0,
      completed: {},

      ///Support History///
      addCallLogModal: false,
      addCallLogDetail: {
         ticket_source: 1,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_app: 0,
         ticket_related_plan: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         application_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         note:0,
         follow_up_date_time: '',
      },
      view_ticket: false,
      ticket_number: '',
      application_id: '',
      add_err: {},
      dateTimeStartDate: '',
      followUpDateTimeStartDate: ''
   }

   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }

   handleNext = () => {
      let activeStep;

      if (this.isLastStep() && !this.allStepsCompleted()) {
         // It's the last step, but not all steps have been completed,
         // find the first step that has been completed
         const steps = getSteps();
         activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
      } else {
         activeStep = this.state.activeStep + 1;
      }
      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleStep = step => () => {
      this.setState({
         activeStep: step,
      });
   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };

   componentDidMount() {
      var data = {
         provider_id: this.props.match.params.id,
         filter_by: 2 // for filter in node file
      }
      this.props.ProviderDetails(this.props.match.params.id);
      this.props.getAllTickets(data);
   }

   handleChange = (event, value) => {
      this.setState({ activeTab: value });
   }

   opnDocFileModal(path, name) {
      this.setState({ opnDocFileModal: true, imgPath: path, imgName: name })
   }

   opnDocAgreeFileModal(item_id, name) {
      this.props.downloadOneDrive(item_id)
      this.setState({ opnDocFileModal: true, imgName: name })
      console.log(this.state)
   }

   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }

   componentWillReceiveProps(nextProps) {
      (nextProps.oneDrivePreview) ? this.setState({ imgPath: nextProps.oneDrivePreview }) : '';
   }

   handleClickShowSsn = () => {
      this.setState({ showSsn: !this.state.showSsn });
   };

   ////Support History////
   opnAddCallLogModal() {
      this.setState({ addCallLogModal: true });
   }
   onAddUpdateCallLogModalClose = () => {
      let addR = {}
      let addCallLogDetail = {
         ticket_source: 1,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_plan: 0,
         ticket_related_app: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         application_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         note:0,
         follow_up_date_time: ''
      };
      this.setState({ addCallLogModal: false, add_err: addR, addCallLogDetail: addCallLogDetail, view_ticket: false, ticket_number: '' })
   }
   addCallLogView() {
      const min = 1;
      const max = 100000000;
      const rand = Math.floor(Math.random() * (max - min + 1) + min)

      this.setState({ view_ticket: true, ticket_number: rand })
   }
   addCallLogViewBack() {
      this.setState({ view_ticket: false })
   }
   addCallLog() {
      this.state.addCallLogDetail.ticket_number = this.state.ticket_number
      this.state.addCallLogDetail.provider_id = this.props.match.params.id
      this.state.addCallLogDetail.commented_by = 1 // by admin
      //console.log(this.state.addCallLogDetail)
      //return false;
      //insertTermMonth
      this.props.createSupportTicket(this.state.addCallLogDetail);


      this.setState({ addCallLogModal: false, loading: true });
      let self = this;
      let log = {
         ticket_source: 1,
         call_type: 'Yes',
         ticket_related_ac: 0,
         ticket_related_plan: 0,
         ticket_related_app: 0,
         ticket_related_invoice: 0,
         account_no: '',
         plan_no: '',
         application_no: '',
         invoice_no: '',
         caller_name: '',
         call_date_time: '',
         subject: '',
         hps_agent_name: '',
         description: '',
         follow_up: 0,
         note:0,
         follow_up_date_time: '',
         view_ticket: false,
         ticket_number: ''
      }
      setTimeout(() => {
         self.setState({ loading: false, addCallLogDetail: log, view_ticket: false, ticket_number: '', dateTimeStartDate: '', followUpDateTimeStartDate: '' });
         NotificationManager.success('Ticket Created!');
      }, 2000);

   }
   onChangeAddCallLogDetails(key, value) {
      let { add_err, addCallLogDetail } = this.state;
      //console.log(value)
      switch (key) {
         case 'ticket_source':

            break;
         case 'call_type':

            break;
         case 'ticket_related_ac':
            value = (this.state.addCallLogDetail.ticket_related_ac) ? 0 : 1;
            //if(value == 0) {
            addCallLogDetail['application_no'] = ''
            addCallLogDetail['invoice_no'] = ''
            //}
            addCallLogDetail['ticket_related_app'] = 0
            addCallLogDetail['ticket_related_invoice'] = 0
            break;
         case 'ticket_related_plan':
            value = (this.state.addCallLogDetail.ticket_related_plan) ? 0 : 1;
            if (value == 0) {
               addCallLogDetail['plan_no'] = ''
            }
            break;
         case 'ticket_related_app':
            value = (this.state.addCallLogDetail.ticket_related_app) ? 0 : 1;
            //if(value == 0) {
            addCallLogDetail['application_no'] = ''
            addCallLogDetail['invoice_no'] = ''
            //}
            addCallLogDetail['ticket_related_invoice'] = 0
            addCallLogDetail['ticket_related_ac'] = 0

            add_err['application_no'] = 'Select Application No.'
            break;
         case 'ticket_related_invoice':
            value = (this.state.addCallLogDetail.ticket_related_invoice) ? 0 : 1;
            //if(value == 0) {
            addCallLogDetail['invoice_no'] = ''
            addCallLogDetail['application_no'] = ''
            //}
            addCallLogDetail['ticket_related_app'] = 0
            addCallLogDetail['ticket_related_ac'] = 0

            add_err['invoice_no'] = 'Select Invoice No.'
            break;
         case 'plan_no':
            if (isEmpty(value)) {
               add_err[key] = "Select Plan No.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'application_no':
            if (isEmpty(value)) {
               add_err[key] = "Select Application No.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'invoice_no':
            if (isEmpty(value)) {
               add_err[key] = "Select Invoice No.";
            } else {
               add_err[key] = '';
            }
            break;
         case 'caller_name':
            if (isEmpty(value)) {
               add_err[key] = "Caller Name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'call_date_time':
            if (value == '') {
               add_err[key] = "Select Date";
            } else {
               this.setState({ dateTimeStartDate: value })
               value = moment(value).format('YYYY-MM-DD h:mm:ss');
               add_err[key] = '';
            }
            break;
         case 'subject':
            if (isEmpty(value)) {
               add_err[key] = "Subject can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'hps_agent_name':
            if (isEmpty(value)) {
               add_err[key] = "Agent Name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'description':
            if (isEmpty(value)) {
               add_err[key] = "Description can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'follow_up':
            value = (this.state.addCallLogDetail.follow_up) ? 0 : 1;
            if (value == 0) {
               this.setState({ followUpDateTimeStartDate: '' })
            }
            add_err['follow_up_date_time'] = 'Select date'
            break;
         case 'note':
            value = (this.state.addCallLogDetail.note) ? 0 : 1;
            
            break;
         case 'follow_up_date_time':
            if (value == '') {
               add_err[key] = "Select Date";
            } else {
               this.setState({ followUpDateTimeStartDate: value })
               value = moment(value).format('YYYY-MM-DD h:mm:ss');
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });
      this.setState({
         addCallLogDetail: {
            ...this.state.addCallLogDetail,
            [key]: value
         }
      });
      //console.log(this.state.addCallLogDetail)
   }
   validateAddCallLogSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      let { addCallLogDetail } = this.state
      var common = true;

      common = (
         this.state.add_err.caller_name === '' &&
         this.state.add_err.call_date_time === '' &&
         this.state.add_err.subject === '' &&
         this.state.add_err.hps_agent_name === '' &&
         this.state.add_err.description === ''
      ) ? true : false

      var count = 0;
      if (addCallLogDetail.ticket_related_ac == 1) {
         count++
      }
      if (addCallLogDetail.ticket_related_app == 1) {
         var plan_select = (this.state.add_err.application_no === '') ? true : false
         count++
      } else {
         var plan_select = true
      }
      if (addCallLogDetail.ticket_related_invoice == 1) {
         var invoice_select = (this.state.add_err.invoice_no === '') ? true : false
         count++
      } else {
         var invoice_select = true
      }
      if (addCallLogDetail.follow_up == 1) {
         var follow_up_select = (this.state.add_err.follow_up_date_time === '') ? true : false
      } else {
         var follow_up_select = true
      }

      return (count >= 1 && common == true && plan_select == true && invoice_select == true && follow_up_select == true) ? true : false;




      return false

      return (/*
         this.state.add_err.ticket_source === '' &&
         this.state.add_err.call_type === '' &&*/
         this.state.add_err.caller_name === '' &&
         this.state.add_err.call_date_time === '' &&
         this.state.add_err.subject === '' &&
         this.state.add_err.hps_agent_name === '' &&
         this.state.add_err.description === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }
   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateUserDetails(name, value)
      });
   }

   render() {
      console.log('this.props.tickets')
      console.log(this.props.tickets)
      const steps = getSteps();
      const { activeStep } = this.state;
      const { loading } = this.props;

      const path = require('path');
      const providerDetails = this.props.provider_details;
      const providerBank = this.props.provider_bank;
      const providerLocation = this.props.provider_location;
      const providerUser = this.props.provider_user;
      const providerAdditionalDiscount = this.props.provider_additional_discount;
      const providerAdditionalFee = this.props.provider_additional_fee;
      const providerDocument = this.props.provider_document;
      const providerAgreement = this.props.provider_agreement;
      const providerSpecility = this.props.provider_specility;
      const providerSpecilityDoctors = this.props.provider_specility_doctors;

      if (providerSpecility) {
         var checkSpc = 0;
         providerSpecility.forEach(function (item, idx) {
            //console.log('mergedList')
            //console.log(idx)
            //console.log(item)
            if (checkSpc == idx) {
               var splc = providerSpecilityDoctors.filter((s, idx) => s.location_id == item.provider_location_provider_location_id && item.specialization == s.mdv_speciality_id);
               splc.forEach(function (doc, id) {
                  providerSpecility[checkSpc].doctor_f_name = doc.f_name;
                  providerSpecility[checkSpc].doctor_l_name = doc.l_name;
                  providerSpecility[checkSpc].procedure_name = doc.procedure_name;
                  providerSpecility[checkSpc].speciality = doc.speciality;

                  checkSpc++;
               })

            }

            //console.log(splc)
         })
      }

      /////Support History/////
      const supportColumns = [
         {
            name: 'Ticket ID',
            field: 'ticket_id'
         },
         {
            name: 'Call Date/Time',
            field: 'call_date_time'
         },
         {
            name: 'Type(email/ph)',
            field: 'ticket_source',
         },
         {
            name: 'App No.',
            field: 'application_no',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.application_no) ? value.application_no : '-'
                  );
               }
            }
         },
         {
            name: 'Invoice No.',
            field: 'invoice_number',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.invoice_number) ? value.invoice_number : '-'
                  );
               }
            }
         },
         {
            name: 'Caller Name',
            field: 'caller_name',
         },
         {
            name: 'Subject',
            field: 'subject',
         },
         {
            name: 'Status',
            field: 'status',
         },
         {
            name: 'Action',
            field: 'ticket_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {

                  return (
                     <React.Fragment>
                        <span className="list-action">
                           <Link to={`/admin/customer-support/view-ticket/${value.ticket_id}`} title="View Ticket"><i className="ti-eye"></i></Link>
                        </span>
                     </React.Fragment>
                  )
               },

            }
         }

      ];

      const supportOptions = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
            );
         }
      };
      const myTheme = createMuiTheme({
         overrides: {
            MaterialDatatableToolbar: {
               root: { display: "none" }
            },
            MuiTableCell: {
               footer: { padding: "4px 8px 4px 8px" }
            },
            MuiPaper: {
               root: { boxShadow: "none !important" }
            }
         }
      });
      return (
         <div className="providerView-wrapper">
            <Helmet>
               <title>Provider View</title>
               <meta name="description" content="User Profile" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.providerView" />} match={this.props.match} />
            <RctCard>

               <Stepper nonLinear activeStep={activeStep}>
                  {steps.map((label, index) => {
                     return (
                        <Step key={label}>
                           <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                              {label}
                           </StepButton>
                        </Step>
                     );
                  })}
               </Stepper>
               <div>
                  {this.allStepsCompleted() ? (
                     <div className="pl-0">
                        <p>All steps completed - you&quot;re finished</p>
                        <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                     </div>
                  ) : (
                        <div className="pl-0">
                           {(() => {
                              switch (activeStep) {
                                 case 0:
                                    return <Provider
                                       providerDetails={providerDetails}
                                       handleClickShowSsn={this.handleClickShowSsn.bind(this)}
                                       showSsn={this.state.showSsn}
                                    />
                                    brea
                                 case 1:
                                    return <Locations
                                       providerLocation={providerLocation}
                                    />
                                    break;
                                 case 2:
                                    return <Banks
                                       providerBank={providerBank}
                                    />
                                    break;
                                 case 3:
                                    return <AdditionalInfo
                                       providerAdditionalDiscount={providerAdditionalDiscount}
                                       providerAdditionalFee={providerAdditionalFee}
                                    />
                                    break;
                                 case 4:
                                    return <Physician
                                       providerPhysician={providerSpecility}
                                    />
                                    break;
                                 case 5:
                                    return <Documents
                                       providerDocument={providerDocument}
                                       opnDocFileModal={this.opnDocFileModal.bind(this)}
                                       providerAgreement={providerAgreement}
                                       opnDocAgreeFileModal={this.opnDocAgreeFileModal.bind(this)}
                                    />
                                    break;
                                 case 6:
                                    return <Users
                                       providerUser={providerUser}
                                    />
                                 default:
                                    return (<div></div>)
                              }
                           })()}

                        </div>
                     )}
               </div>

               <div className="admin-application-list support-history-container">
                  <RctCollapsibleCard
                     colClasses="col-md-12 w-xs-full support-history"
                     heading="Support History"
                     collapsible
                     fullBlock
                     customClasses="overflow-hidden d-inline-block w-100"
                  >
                     <AddCallLogButton opnAddCallLogModal={this.opnAddCallLogModal.bind(this)} />
                     <MuiThemeProvider theme={myTheme}>
                        <MaterialDatatable
                           data={(this.props.tickets) ? this.props.tickets : ''}
                           columns={supportColumns}
                           options={supportOptions}
                        />
                     </MuiThemeProvider>
                  </RctCollapsibleCard>
               </div>

               {this.props.loading &&
                  <RctSectionLoader />
               }

            </RctCard>

            <Modal isOpen={this.state.addCallLogModal} toggle={() => this.onAddUpdateCallLogModalClose()} className="support-history-modal custom-support-width">
               <ModalHeader toggle={() => this.onAddUpdateCallLogModalClose()}>
                  Add Customer Support Ticket
               </ModalHeader>
               <ModalBody>
                  <AddCallLogForm
                     addErr={this.state.add_err}
                     addCallLogDetails={this.state.addCallLogDetail}
                     onChangeAddCallLogDetails={this.onChangeAddCallLogDetails.bind(this)}
                     DatePicker={DatePicker}
                     dateTimeStartDate={this.state.dateTimeStartDate}
                     followUpDateTimeStartDate={this.state.followUpDateTimeStartDate}
                     masterTicketSource={this.props.masterTicketSource}
                     providerApplication={this.props.providerApplication}
                     providerInvoice={this.props.providerInvoice}
                     providerDetails={this.props.provider_details}
                  />
               </ModalBody>
               <ModalFooter>

                  <Button
                     variant="contained"
                     color="primary"
                     className="text-white"
                     onClick={() => this.addCallLog()}
                     disabled={!this.validateAddCallLogSubmit()}
                  >
                     Submit
                  </Button>

                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCallLogModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

            <Modal className="" isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

               <ModalHeader toggle={() => this.opnViewDocFileModalClose()} className="p-view-popupImg">
                  <span className="float-left>">Name: {this.state.imgName}</span>
                  <span className="float-right"><a href={this.state.imgPath} className="p-view-img-download-link" download><i className="mr-10 ti-import"></i></a></span>
               </ModalHeader>

               <ModalBody>
                  {this.state.imgPath &&
                     <embed src={this.state.imgPath} width="100%" height="350" download />

                  }

               </ModalBody>

            </Modal>

         </div>
      );
   }
}
const mapStateToProps = ({ Provider, creditApplication, CustomerSupportReducer }) => {
   const { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_agreement, provider_specility, provider_specility_doctors } = Provider;

   const { oneDrivePreview } = creditApplication
   const { tickets, masterTicketSource, providerApplication, providerInvoice } = CustomerSupportReducer
   return { loading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_agreement, provider_specility, provider_specility_doctors, oneDrivePreview, tickets, masterTicketSource, providerApplication, providerInvoice }
}
export default connect(mapStateToProps, {
   ProviderDetails, downloadOneDrive, createSupportTicket, getAllTickets
})(ProviderView);