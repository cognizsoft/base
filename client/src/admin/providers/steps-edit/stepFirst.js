/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import { InputAdornment, withStyles } from '@material-ui/core';

const StepFirst = ({ addErr, addData, onChnagerovider, masterProviderType, checkSsnExist }) => (
    <div className="table-responsive">
      <div className="modal-body page-form-outer text-left first-tab-container">
            <Form>
               <div className="row">
                  <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_name">Provider Name<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_name"
                                 id="provider_name"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Provider Name"
                                 value={(addData.provider_name != '') ? addData.provider_name : ''}
                                 error={(addErr.provider_name) ? true : false}
                                 helperText={(addErr.provider_name != '') ? addErr.provider_name : ''}
                                 onChange={(e) => onChnagerovider('provider_name', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup className="provider_type_grp">
                              <Label for="provider_type">Provider Type<span className="required-field">*</span></Label>
                              <Input
                                  type="select"
                                  name="provider_type"
                                  id="provider_type"
                                  placeholder=""
                                  value={addData.provider_type}
                                  onChange={(e) => onChnagerovider('provider_type', e.target.value)}
                                >
                                  <option value="">Select</option>
                                  {masterProviderType && masterProviderType.map((opt, key) => (
                                      <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                  ))}
                                  
                              </Input>
                                {(addErr.provider_type != '') ? <FormHelperText>{addErr.provider_type}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                         <div className="col-md-4">
                           <FormGroup className="ssn-mask">
                              <Label for="provider_taxid_ssn">Tax ID/SSN<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_taxid_ssn"
                                 id="provider_taxid_ssn"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Tax ID/SSN"
                                 inputProps={{ maxLength: 11 }}
                                 value={(addData.provider_taxid_ssn != '') ? addData.provider_taxid_ssn : ''}
                                 error={(addErr.provider_taxid_ssn) ? true : false}
                                 helperText={(addErr.provider_taxid_ssn != '') ? addErr.provider_taxid_ssn : ''}
                                 onChange={(e) => onChnagerovider('provider_taxid_ssn', e.target.value)}
                                 onKeyUp={(e) => checkSsnExist(e.target.value, addData.provider_id)}
                                 />
                           </FormGroup>
                        </div>


                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_phone_no">Phone No<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_phone_no"
                                 id="provider_phone_no"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Phone No"
                                 inputProps={{ maxLength: 14 }}
                                 value={(addData.provider_phone_no != '') ? addData.provider_phone_no : ''}
                                 error={(addErr.provider_phone_no) ? true : false}
                                 helperText={(addErr.provider_phone_no != '') ? addErr.provider_phone_no : ''}
                                 onChange={(e) => onChnagerovider('provider_phone_no', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_email">Email Address<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_email"
                                 id="provider_email"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Email Address"
                                 value={(addData.provider_email != '') ? addData.provider_email : ''}
                                 error={(addErr.provider_email) ? true : false}
                                 helperText={(addErr.provider_email != '') ? addErr.provider_email : ''}
                                 onChange={(e) => onChnagerovider('provider_email', e.target.value)}
                                 />
                           </FormGroup>
                        </div> 

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_fax_no">Fax No</Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_fax_no"
                                 id="provider_fax_no"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Fax No"
                                 inputProps={{ maxLength: 14 }}
                                 value={(addData.provider_fax_no != '') ? addData.provider_fax_no : ''}
                                 error={(addErr.provider_fax_no) ? true : false}
                                 helperText={(addErr.provider_fax_no != '') ? addErr.provider_fax_no : ''}
                                 onChange={(e) => onChnagerovider('provider_fax_no', e.target.value)}
                                 />
                           </FormGroup>
                        </div> 

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_website">Website</Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_website"
                                 id="provider_website"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="www."
                                 value={(addData.provider_website != '') ? addData.provider_website : ''}
                                 error={(addErr.provider_website) ? true : false}
                                 helperText={(addErr.provider_website != '') ? addErr.provider_website : ''}
                                 onChange={(e) => onChnagerovider('provider_website', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_primary_contact">Primary Contact<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_primary_contact"
                                 id="provider_primary_contact"
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Primary Contact"
                                 value={(addData.provider_primary_contact != '') ? addData.provider_primary_contact : ''}
                                 error={(addErr.provider_primary_contact) ? true : false}
                                 helperText={(addErr.provider_primary_contact != '') ? addErr.provider_primary_contact : ''}
                                 onChange={(e) => onChnagerovider('provider_primary_contact', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                           <FormGroup>
                              <Label for="provider_primary_contact_phone">Primary Contact Phone<span className="required-field">*</span></Label><br/>
                                 <TextField
                                 type="text"
                                 name="provider_primary_contact_phone"
                                 id="provider_primary_contact_phone"
                                 fullWidth
                                 variant="outlined"
                                 inputProps={{ maxLength: 14 }}
                                 placeholder="Primary Contact Phone"
                                 value={(addData.provider_primary_contact_phone != '') ? addData.provider_primary_contact_phone : ''}
                                 error={(addErr.provider_primary_contact_phone) ? true : false}
                                 helperText={(addErr.provider_primary_contact_phone != '') ? addErr.provider_primary_contact_phone : ''}
                                 onChange={(e) => onChnagerovider('provider_primary_contact_phone', e.target.value)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-4">
                          
                           <FormGroup tag="fieldset">
                              <Label>In Network</Label>
                              <FormGroup check>
                                  <Label check>
                                      <Input
                                          type="radio"
                                          name="provider_network  "
                                          value={1}
                                          checked={(addData.provider_network == 1) ? true : false}
                                          onChange={(e) => onChnagerovider('provider_network', e.target.value)}
                                          />{' '}
                                      Yes
                                  </Label>
                              </FormGroup>
                              <FormGroup check>
                                  <Label check>
                                      <Input
                                          type="radio"
                                          name="provider_network" 
                                          value={0}
                                          checked={(addData.provider_network == 0) ? true : false}
                                          onChange={(e) => onChnagerovider('provider_network', e.target.value)}
                                          />{' '}
                                      No
                                  </Label>
                              </FormGroup>
                          </FormGroup> 

                         </div>

                         <div className="col-md-4">
                            <FormGroup tag="fieldset">
                              <Label>Status</Label>
                              <FormGroup check>
                                  <Label check>
                                      <Input
                                          type="radio"
                                          name="provider_status"
                                          value={1}
                                          checked={(addData.provider_status == 1) ? true : false}
                                          onChange={(e) => onChnagerovider('provider_status', e.target.value)}
                                          />{' '}
                                      Active
                                  </Label>
                              </FormGroup>
                          </FormGroup>    
                         </div>
                    </div>
            </Form>        
         </div>
    </div>
);

export default StepFirst;