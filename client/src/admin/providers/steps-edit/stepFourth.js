/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import moment from 'moment';
import { URL } from '../../../apifile/URL';
const StepFourth = ({ addErr, addData, provider_document, onChnagerovider, handleFourthTabDiscountNameChange, handleRemoveFourthTabDiscount, handleAddFourthTabDiscount, fourthTabDiscount, handleFourthTabFeeNameChange, handleRemoveFourthTabFee, handleAddFourthTabFee, fourthTabFee, handleFourthTabDocNameChange, handleRemoveFourthTabDoc, handleAddFourthTabDoc, fourthTabDoc, DatePicker, provider_option_discount_type, provider_option_fee_type, provider_option_document_type, opnDocFileModal, provider_payment_term, provider_loan_term_months, provider_option_spl, provider_spl_procedure, handleRemoveThirdTabSpl, handleAddThirdTabSpl, thirdTabSpl, discount_msg }) => (
     <div className="table-responsive">
         <div className="modal-body page-form-outer text-left fourth-tab-container">

            <div className="fourth-tab-discount-rate-container">
              <h3 className="m-0">Discount Rate </h3>
              
              {addData.discount_rate.map((fourthtab_discount, idx) => (
                 <div className="row" key={idx}>

                        {(() => {
                            if (idx != 0) {
                                return (
                                    <React.Fragment>
                                        <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                                        <div className="col-md-3 text-right">
                                            <a href="#" onClick={(e) => handleRemoveFourthTabDiscount(idx)}>Remove Discount Rate (-)</a>
                                        </div>
                                    </React.Fragment>
                                )
                            }

                        })()}

                        <div className='col-md-2'>
                             <FormGroup className="dis_type_grp">
                                <Label for="discount_type">Discount Type<span className="required-field">*</span></Label>
                                <Input
                                    type="select"
                                    name="discount_type"
                                    id="discount_type"
                                    placeholder=""
                                    value={(addData.discount_rate[idx].discount_type_id != '') ? addData.discount_rate[idx].discount_type_id : ''}
                                    onChange={(e) => onChnagerovider('discount_type', e, idx)}
                                  >
                                    <option value="">Select</option>
                                    {provider_option_discount_type && provider_option_discount_type.map((opt, key) => (
                                        <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                    ))}
                                    
                                </Input>
                                {(addErr.discount_rate[idx].discount_type != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_type}</FormHelperText> : ''}
                             </FormGroup>
                          </div>

                          <div className='col-md-2'>
                             <FormGroup className="loan_trm_grp">
                                <Label for="loan_term_month">Loan Term(months)</Label>
                                <Input
                                    type="select"
                                    name="loan_term_month"
                                    id="loan_term_month"
                                    placeholder=""
                                    value={(addData.discount_rate[idx].loan_term_month != '') ? addData.discount_rate[idx].loan_term_month : ''}
                                    onChange={(e) => onChnagerovider('loan_term_month', e.target.value, idx)}
                                >
                                    <option value="">Select</option>
                                    {provider_loan_term_months && provider_loan_term_months.map((opt, key) => (
                                        <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                    ))}
                                    
                                </Input>
                                {(addErr.discount_rate[idx].loan_term_month != '') ? <FormHelperText>{addErr.discount_rate[idx].loan_term_month}</FormHelperText> : ''}
                             </FormGroup>
                        </div>

                          <div className='col-md-2'>
                             <FormGroup>
                                <Label for="discount_rate">Discount Rate<span className="required-field">*</span> (%)</Label><br/>
                                   <TextField
                                   type="text"
                                   name="discount_rate"
                                   id="discount_rate"
                                   fullWidth
                                   variant="outlined"
                                   placeholder="Discount Rate(%)"
                                   value={(addData.discount_rate[idx].discount_rate >= 0) ? addData.discount_rate[idx].discount_rate : ''}
                                   error={(addErr.discount_rate[idx].discount_rate) ? true : false}
                                   helperText={(addErr.discount_rate[idx].discount_rate != '') ? addErr.discount_rate[idx].discount_rate : ''}
                                   onChange={(e) => onChnagerovider('discount_rate', e.target.value, idx)}
                                   />
                             </FormGroup>
                          </div>

                          <div className='col-md-2'>
                             <FormGroup>
                                <Label for="redu_discount_rate">Reduced Interest PCT (%)</Label><br/>
                                   <TextField
                                   type="text"
                                   name="redu_discount_rate"
                                   id="redu_discount_rate"
                                   fullWidth
                                   variant="outlined"
                                   placeholder="Reduced Interest(%)"
                                   value={(addData.discount_rate[idx].redu_discount_rate >= 0) ? addData.discount_rate[idx].redu_discount_rate : ''}
                                   error={(addErr.discount_rate[idx].redu_discount_rate) ? true : false}
                                   helperText={(addErr.discount_rate[idx].redu_discount_rate != '') ? addErr.discount_rate[idx].redu_discount_rate : ''}
                                   onChange={(e) => onChnagerovider('redu_discount_rate', e.target.value, idx)}
                                   />
                             </FormGroup>
                          </div>

                          
                          <div className="col-md-2">
                              <FormGroup className="dis_from_date_grp">
                                  <Label for="discount_from_date">From Date<span className="required-field">*</span></Label>
                                  <DatePicker
                                    name="discount_from_date"
                                    id="discount_from_date"
                                    selected={(addData.discount_rate[idx].discount_from_date && addData.discount_rate[idx].discount_from_date != '00/00/0000') ? new Date(addData.discount_rate[idx].discount_from_date) : '' }
                                    placeholderText="MM/DD/YYYY"
                                    maxDate={(addData.discount_rate[idx].discount_to_date) ? new Date(addData.discount_rate[idx].discount_to_date) : ''}
                                    autocomplete={false}
                                    popperPlacement="right-start"
                                    onChange={(e) => onChnagerovider('discount_from_date', e, idx)}
                                    />
                                  {(addErr.discount_rate[idx].discount_from_date != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_from_date}</FormHelperText> : ''}
                              </FormGroup>
                          </div>
                          
                          
                          <div className="col-md-2">
                              <FormGroup className="dis_to_date_grp">
                                  <Label for="discount_to_date">Expire Date</Label>
                                  <DatePicker
                                    name="discount_to_date"
                                    id="discount_to_date"
                                    selected={(addData.discount_rate[idx].discount_to_date && addData.discount_rate[idx].discount_to_date != '00/00/0000') ? new Date(addData.discount_rate[idx].discount_to_date) : ''}
                                    placeholderText="MM/DD/YYYY"
                                    minDate={(addData.discount_rate[idx].discount_from_date) ? new Date(addData.discount_rate[idx].discount_from_date) : ''}
                                    autocomplete={false}
                                    popperPlacement="right-start"
                                    onChange={(e) => onChnagerovider('discount_to_date', e, idx)}
                                    />
                                  {(addErr.discount_rate[idx].discount_to_date != '') ? <FormHelperText>{addErr.discount_rate[idx].discount_to_date}</FormHelperText> : ''}
                              </FormGroup>
                          </div>
                          
                          
                 </div>
              ))}
                <div className="row">
                  <div className="col-md-3 text-left">
                    <a href="#" onClick={(e) => handleAddFourthTabDiscount()}>Add More Discount Rate (+)</a>
                 </div>
                 <div className="col-md-9 text-left dis_all_req_grp">{(discount_msg) ? discount_msg : ''}</div>
               </div>
            </div>

            <br/>

            <div className="fourth-tab-fee-rate-container">
              <h3>Additional Fee</h3>

              {addData.additional_fee.map((fourthtab_fee, idx) => (
                <div className="row" key={idx}>
                      
                      {(() => {
                            if (idx != 0) {
                                return (
                                    <React.Fragment>
                                        <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                                        <div className="col-md-3 text-right">
                                            <a href="#" onClick={(e) => handleRemoveFourthTabFee(idx)}>Remove Additional Fee (-)</a>
                                        </div>
                                    </React.Fragment>
                                )
                            }

                        })()}

                      <div className="col-md-3">
                           <FormGroup>
                              <Label for="additional_fee_type">Fee Type</Label>
                              <Input
                                  type="select"
                                  name="additional_fee_type"
                                  id="additional_fee_type"
                                  placeholder=""
                                  value={(addData.additional_fee[idx].additional_fee_type_id != '') ? addData.additional_fee[idx].additional_fee_type_id : ''}
                                  onChange={(e) => onChnagerovider('additional_fee_type', e, idx)}
                              >
                                  <option value="">Select</option>
                                  {provider_option_fee_type && provider_option_fee_type.map((opt, key) => (
                                        <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                    ))}
                                  
                              </Input>
                               
                           </FormGroup>
                        </div>

                        <div className="col-md-3">
                           <FormGroup>
                              <Label for="additional_conv_fee">Convenience Fee ($)</Label><br/>
                                 <TextField
                                 type="text"
                                 name="additional_conv_fee"
                                 id="additional_conv_fee"
                                 className={(addData.additional_fee[idx].additional_fee_field) ? 'additional_conv_fee' : ''}
                                 disabled={(addData.additional_fee[idx].additional_fee_field) ? true : false }
                                 fullWidth
                                 variant="outlined"
                                 placeholder="Convenience Fee"
                                 value={(addData.additional_fee[idx].additional_conv_fee != '') ? addData.additional_fee[idx].additional_conv_fee : ''}
                                 error={(addErr.additional_fee[idx].additional_conv_fee) ? true : false}
                                 helperText={(addErr.additional_fee[idx].additional_conv_fee != '') ? addErr.additional_fee[idx].additional_conv_fee : ''}
                                 onChange={(e) => onChnagerovider('additional_conv_fee', e.target.value, idx)}
                                 />
                           </FormGroup>
                        </div>

                        <div className="col-md-3">
                            <FormGroup>
                                <Label for="additional_from_date">From Date</Label>
                                <DatePicker
                                  name="additional_from_date"
                                  id="additional_from_date"
                                  className={(addData.additional_fee[idx].additional_fee_field) ? 'additional_from_date' : ''}
                                  disabled={(addData.additional_fee[idx].additional_fee_field) ? true : false }
                                  selected={(addData.additional_fee[idx].additional_from_date) ? new Date(addData.additional_fee[idx].additional_from_date) : '' }
                                  placeholderText="MM/DD/YYYY"
                                  autocomplete={false}
                                  onChange={(e) => onChnagerovider('additional_from_date', e, idx)}
                                  />
                                  {(addErr.additional_fee[idx].additional_from_date != '') ? <FormHelperText>{addErr.additional_fee[idx].additional_from_date}</FormHelperText> : ''}
                            </FormGroup>
                        </div>

                        <div className="col-md-3">
                            <FormGroup>
                                <Label for="additional_to_date">To Date</Label>
                                <DatePicker
                                  name="additional_to_date"
                                  id="additional_to_date"
                                  className={(addData.additional_fee[idx].additional_fee_field) ? 'additional_to_date' : ''}
                                  disabled={(addData.additional_fee[idx].additional_fee_field) ? true : false }
                                  selected={(addData.additional_fee[idx].additional_to_date) ? new Date(addData.additional_fee[idx].additional_to_date) : '' }
                                  placeholderText="MM/DD/YYYY"
                                  autocomplete={false}
                                  onChange={(e) => onChnagerovider('additional_to_date', e, idx)}
                                  />
                                  {(addErr.additional_fee[idx].additional_to_date != '') ? <FormHelperText>{addErr.additional_fee[idx].additional_to_date}</FormHelperText> : ''}
                            </FormGroup>
                        </div>
                        
                </div>
              ))}
                <div className="row">
                  <div className="col-md-12 text-left">
                    <a href="#" onClick={(e) => handleAddFourthTabFee()}>Add More Additional Fee (+)</a>
                 </div>
               </div>
            </div> 

            <br/>

            <div className="fourth-tab-upload-document-container">
              <h3>Documents</h3>
              {addData.documents.map((fourthtab_doc, idx) => (
                <div className="row" key={idx}>
                  {(() => {
                      if (idx != 0) {
                          return (
                              <React.Fragment>
                                  <div className="col-md-9"><span className="border-top my-3 d-block"></span></div>
                                  <div className="col-md-3 text-right">
                                      <a href="#" onClick={(e) => handleRemoveFourthTabDoc(idx)}>Remove Document (-)</a>
                                  </div>
                              </React.Fragment>
                          )
                      }

                  })()}
                  <div className="col-md-3">
                     <FormGroup>
                        <Label for="document_type">Document Type</Label>
                        <Input
                          type="select"
                          name="document_type"
                          id="document_type"
                          placeholder=""
                          value={(addData.documents[idx].document_type != '') ? addData.documents[idx].document_type : ''}
                          onChange={(e) => onChnagerovider('document_type', e.target.value, idx)}
                          >
                          <option value="">Select</option>
                          {provider_option_document_type && provider_option_document_type.map((opt, key) => (
                                <option value={opt.mdv_id} key={key}>{opt.value}</option>
                            ))}
                            
                        </Input>
                        {(addErr.documents[idx].document_type != '') ? <FormHelperText>{addErr.documents[idx].document_type}</FormHelperText> : ''}
                     </FormGroup>
                  </div>

                  <div className="col-md-3">
                     <FormGroup>
                        <Label for="document_name">Document Name</Label><br/>
                           <TextField
                           type="text"
                           name="document_name"
                           id="document_name"
                           fullWidth
                           variant="outlined"
                           placeholder="Document Name"
                           value={(addData.documents[idx].document_name != '') ? addData.documents[idx].document_name : ''}
                           error={(addErr.documents[idx].document_name) ? true : false}
                           helperText={(addErr.documents[idx].document_name != '') ? addErr.documents[idx].document_name : ''}
                           onChange={(e) => onChnagerovider('document_name', e.target.value, idx)}
                           />
                     </FormGroup>
                  </div>

                  <div className="col-md-3 upload-doc-input">
                    <FormGroup>
                        <Label for="document_upload">Upload Document</Label><br/>
                        <Input
                            type="file"
                            name="document_upload"
                            id="document_upload"
                            placeholder="" 
                            accept="image/gif, image/jpeg, image/png, application/pdf"
                            onChange={(e) => onChnagerovider('document_upload', e, idx)}
                          >
                    
                        </Input>
                    </FormGroup>
                    {addData.documents[idx].document_upload && 
                      <img src={addData.documents[idx].document_upload} width="100" onClick={()=>opnDocFileModal(addData.documents[idx].document_upload)}/>
                    }
                  </div>

                  
                </div>
              ))}
              <div className="row">
                <div className="col-md-12 text-left">
                  <a href="#" onClick={(e) => handleAddFourthTabDoc()}>Add More Documents (+)</a>
                </div>
              </div>
            </div>

            


         </div>
      </div>
);

export default StepFourth;