/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import moment from 'moment';
import { URL } from '../../../apifile/URL';
const StepFourth = ({ addErr, addData, provider_document, onChnagerovider, handleFourthTabDiscountNameChange, handleRemoveFourthTabDiscount, handleAddFourthTabDiscount, fourthTabDiscount, handleFourthTabFeeNameChange, handleRemoveFourthTabFee, handleAddFourthTabFee, fourthTabFee, handleFourthTabDocNameChange, handleRemoveFourthTabDoc, handleAddFourthTabDoc, fourthTabDoc, DatePicker, provider_option_discount_type, provider_option_fee_type, provider_option_document_type, opnDocFileModal, provider_payment_term, provider_loan_term_months, provider_option_spl, provider_spl_procedure, handleRemoveThirdTabSpl, handleAddThirdTabSpl, thirdTabSpl }) => (
     <div className="table-responsive">
         <div className="modal-body page-form-outer text-left fourth-tab-container">

            <div className="fourth-tab-spl-container">
            <h3>Physicians Speciality</h3>
                  {addData.speciality.map((thirdtab_spl, idx) => (

                    <div className="row" key={idx}>
                        
                          {(() => {
                              if (idx != 0) {
                                  return (
                                      <React.Fragment>
                                          <div className="col-md-10"><span className="border-top my-3 d-block"></span></div>
                                          <div className="col-md-2">
                                              <a href="#" onClick={(e) => handleRemoveThirdTabSpl(idx)}>Remove Speciality (-)</a>
                                          </div>
                                      </React.Fragment>
                                  )
                              }

                          })()}

                        

                        <div className="col-md-4">
                             <FormGroup className="spl_loc_grp">
                                <Label for="spl_location_name">Location Name<span className="required-field">*</span></Label>
                                <Input
                                    type="select"
                                    name="spl_location_name"
                                    id="spl_location_name"
                                    placeholder=""
                                    value={addData.speciality[idx].spl_location_name}
                                    onChange={(e) => onChnagerovider('spl_location_name', e.target.value, idx)}
                                >
                                    <option value="">Select</option>
                                    {addData.location && addData.location.map((opt, key) => (
                                        <option value={opt.physical_location_name} key={key}>{opt.physical_location_name}</option>
                                    ))}
                                    
                                </Input>
                                {(addErr.speciality[idx].spl_location_name != '') ? <FormHelperText>{addErr.speciality[idx].spl_location_name}</FormHelperText> : ''}
                             </FormGroup>
                        </div>

                        <div className="col-md-4">
                          <FormGroup>
                            <Label for="doctor_f_name">Physician First Name<span className="required-field">*</span></Label><br />
                            <TextField
                              type="text"
                              name="doctor_f_name"
                              id="doctor_f_name"
                              fullWidth
                              variant="outlined"
                              placeholder="Doctor first name"
                              value={(addData.speciality[idx].doctor_f_name != '') ? addData.speciality[idx].doctor_f_name : ''}
                              error={(addErr.speciality[idx].doctor_f_name) ? true : false}
                              helperText={(addErr.speciality[idx].doctor_f_name != '') ? addErr.speciality[idx].doctor_f_name : ''}
                              onChange={(e) => onChnagerovider('doctor_f_name', e.target.value, idx)}
                            />
                          </FormGroup>
                        </div>

                        <div className="col-md-4">
                          <FormGroup>
                            <Label for="doctor_l_name">Physician Last Name<span className="required-field">*</span></Label><br />
                            <TextField
                              type="text"
                              name="doctor_l_name"
                              id="doctor_l_name"
                              fullWidth
                              variant="outlined"
                              placeholder="Doctor last name"
                              value={(addData.speciality[idx].doctor_l_name != '') ? addData.speciality[idx].doctor_l_name : ''}
                              error={(addErr.speciality[idx].doctor_l_name) ? true : false}
                              helperText={(addErr.speciality[idx].doctor_l_name != '') ? addErr.speciality[idx].doctor_l_name : ''}
                              onChange={(e) => onChnagerovider('doctor_l_name', e.target.value, idx)}
                            />
                          </FormGroup>
                        </div>

                        <div className="col-md-6">
                           <FormGroup className="spl_name_grp">
                              <Label for="specialization">Speciality Name<span className="required-field">*</span></Label>
                              <Input
                                  type="select"
                                  name="specialization"
                                  id="specialization"
                                  placeholder=""
                                  value={addData.speciality[idx].specialization}
                                  onChange={(e) => onChnagerovider('specialization', e.target.value, idx)}
                              >
                                  <option value="">Select</option>
                                    {provider_option_spl && provider_option_spl.map((opt, key) => (
                                        <option value={opt.mdv_id} key={key}>{opt.value}</option>
                                    ))}
                              </Input>
                              {(addErr.speciality[idx].specialization != '') ? <FormHelperText>{addErr.speciality[idx].specialization}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                        <div className="col-md-6">
                           <FormGroup>
                              <Label for="procedure_id">Procedure</Label>
                              <Input
                                  type="select"
                                  name="procedure_id"
                                  id="procedure_id"
                                  placeholder=""
                                  value={addData.speciality[idx].procedure_id}
                                  onChange={(e) => onChnagerovider('procedure_id', e.target.value, idx)}
                              >
                                  <option value="">Select</option>
                                    {provider_spl_procedure[idx] && provider_spl_procedure[idx].map((opt, key) => (
                                        <option value={opt.procedure_id} key={key}>{opt.procedure_name}</option>
                                    ))}
                              </Input>
                              {(addErr.speciality[idx].procedure_id != '') ? <FormHelperText>{addErr.speciality[idx].procedure_id}</FormHelperText> : ''}
                           </FormGroup>
                        </div>

                    </div>

                  ))}

                    
                  <div className="row">
                     <div className="col-md-12">
                        <a href="#" onClick={(e) => handleAddThirdTabSpl()}>Add More Speciality (+)</a>
                     </div>
                 </div>
            </div>

          </div>
      </div>
);

export default StepFourth;