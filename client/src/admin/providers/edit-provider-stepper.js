/*===== Horizonatl Non Linear Stepper =====*/
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isMaster, isLength, isDecimals, isNumberDecimal, isNumeric, isAlphaNumeric, isZip, isEmail, isPhone, formatPhoneNumber, isContainWhiteSpace, isPassword, isSSN, isAlpha, formatSSNNumber } from '../../validator/Validator';
function getSteps() {
   return ['Providers', 'Locations', 'Bank Details', 'Additional Information', 'Physicians', 'Users'];
}
import StepFirst from './steps-edit/stepFirst';
import StepSecond from './steps-edit/stepSecond';
import StepThird from './steps-edit/stepThird';
import StepFourth from './steps-edit/stepFourth';
import StepFourthPhy from './steps-edit/stepFourthPhy';
import StepFifth from './steps-edit/stepFifth';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
   checkSsnExist, providerTypeMasterDataValueList, ProviderDetailsEdit, updateProvider, providerCountry, getProviderStates, getProviderRegion, getProviderOption, removeAddMoreProvider, getSpclProcedure
} from 'Actions';

class EditProvider extends Component {
   state = {
      activeStep: 0,
      cancelState: 0,
      completed: {},

      opnDocFileModal: false,
      imgPath: null,

      shareholders: [{ name: "" }],
      thirdTabBank: [{ name: "" }],
      thirdTabSpl: [{ name: "" }],
      fourthTabDiscount: [{ name: "" }],
      fourthTabFee: [{ name: "" }],
      fourthTabDoc: [{ name: "" }],
      testLoc: "",
      willUpdate: false,
      blank_to_date: "",

      discount_msg: "",
      addData: {
         provider_id: "",
         provider_name: "",
         provider_type: "",
         provider_taxid_ssn: "",
         provider_phone_no: "",
         provider_email: "",
         provider_fax_no: "",
         provider_website: "",
         provider_primary_contact: "",
         provider_primary_contact_phone: "",
         provider_network: 1,
         provider_status: 1,

         user_first_name: "",
         user_middle_name: "",
         user_last_name: "",
         user_email: "",
         user_role: "",
         user_location: "",
         user_phone_no: "",
         username: "",
         user_password: "",
         user_confirm_password: "",
         user_primary_contact_flag: "",

         document_type: "",
         document_name: "",
         document_upload: "",

         payment_term: "",
         //billing_address_flag: false,
         changeURL: 0,

         location: [{
            physical_location_name: "",
            physical_address1: "",
            physical_address2: "",
            physical_country: "",
            physical_state: "",
            physical_region: "",
            physical_county: "",
            physical_city: "",
            physical_zip_code: "",
            physical_phone_no: "",
            primary_address_flag: 1,

            billing_location_name: "",
            billing_address1: "",
            billing_address2: "",
            billing_city: "",
            billing_zip_code: "",
            billing_phone_no: "",
            billing_state_id: "",
            billing_county: "",
            billing_country: "",
            billing_region: "",
            billing_address_flag: 0,
         }],

         bank_details: [{
            location_name: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_accout: "",
            name_on_account: "",
            bank_account_type: "",
         }],
         speciality: [{
            spl_location_name: "",
            specialization: "",
            procedure_id: "",
            doctor_f_name: "",
            doctor_l_name: "",
         }],
         discount_rate: [{
            discount_type: "",
            discount_rate: "",
            redu_discount_rate: "",
            loan_term_month: "",
            discount_from_date: "",
            discount_to_date: "",
            discount_type_filter: "",
         }],
         additional_fee: [{
            additional_fee_field: 1,
            additional_fee_type: "",
            additional_conv_fee: "",
            additional_from_date: "",
            additional_to_date: "",
         }],
         documents: [{
            document_type: "",
            document_name: "",
            document_upload: "",
         }],
      },
      add_err: {
         provider_name: "",
         provider_type: "",
         provider_taxid_ssn: "",
         provider_phone_no: "",
         provider_email: "",
         provider_fax_no: "",
         provider_website: "",
         provider_primary_contact: "",
         provider_primary_contact_phone: "",
         provider_network: 0,
         provider_status: 1,

         user_first_name: "",
         user_middle_name: "",
         user_last_name: "",
         user_eamil: "",
         user_type: "",
         user_role: "",
         user_location: "",
         user_phone_no: "",
         username: "",
         user_password: "",
         user_confirm_password: "",

         document_type: "",
         document_name: "",
         document_upload: "",

         primary_flag: "",
         location: [{
            /*physical_location_name: "",
            physical_address1: "",
            physical_address2: "",
            physical_country: "",
            physical_state: "",
            physical_region: "",
            physical_county: "",
            physical_city: "",
            physical_zip_code: "",
            physical_phone_no: "",

            billing_location_name: "",
            billing_address1: "",
            billing_address2: "",
            billing_city: "",
            billing_zip_code: "",
            billing_phone_no: "",
            billing_state_id: "",
            billing_county:"",
            billing_country:"",
            billing_region: "", */
         }],

         bank_details: [{
            /*location_name: "",
            bank_name: "",
            bank_address: "",
            rounting_no: "",
            bank_accout: "",
            name_on_account: "",
            bank_account_type: "",*/
         }],
         speciality: [{
            /*spl_location_name: "",
            specialization: "",*/
         }],
         discount_rate: [{
            /*discount_type: "",
            discount_rate: "",
            discount_from_date: "",
            discount_to_date: "",*/
         }],
         additional_fee: [{
            /*additional_fee_type: "",
            additional_conv_fee: "",
            additional_from_date: "",
            additional_to_date: "",*/
         }],
         documents: [{
            /*document_type: "",
            document_name: "",
            document_upload: "",*/
         }],
      },
   };


   opnDocFileModal(path) {
      this.setState({ opnDocFileModal: true, imgPath: path })
   }

   opnViewDocFileModalClose = () => {
      this.setState({ opnDocFileModal: false })
   }

   onCheckedClick(key, value, idx) {

      let { add_err } = this.state;
      let newShareholders;

      switch (key) {
         case 'primary_billing_address_flag':

            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_billing_address_flag': (value == true) ? 1 : 0 };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });

            break;

         case 'primary_address_flag':
            console.log('onchecked')
            console.log(value)
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_address_flag': (value == true) ? 1 : 0 };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });

            break;
         default:
            break;
      }

   }

   onLoadProvider(key, value, idx) {

      let { add_err } = this.state;
      let newShareholders;

   }

   onChnagerovider(key, value, idx) {
      let { add_err, addData } = this.state;
      let newShareholders, newThirdTabBank, newThirdTabSpl, newFourthTabDiscount, newFourthTabFee, newFourthTabDoc;

      switch (key) {

         /** 1st step **/
         case 'provider_name':
            if (isEmpty(value)) {
               add_err[key] = "Provider Name can't be blank";
            } else {
               add_err[key] = '';
            }

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Provider Type";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_taxid_ssn':
            if (isEmpty(value)) {
               add_err[key] = "Tax ID/SSN can't be blank";
            } else if (isSSN(value)) {
               value = formatSSNNumber(value)
               add_err[key] = "Please enter 9 digit valid Tax ID/SSN";
            } else {
               value = formatSSNNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_phone_no':
            if (isEmpty(value)) {
               add_err[key] = "Provider Phone No can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = 'Please provide valid Phone No.';
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_email':
            if (isEmpty(value)) {
               add_err[key] = "Email can't be blank";
            } else if (!isEmail(value)) {
               add_err[key] = 'Please provide valid email';
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_fax_no':
            if (isEmpty(value)) {
               add_err[key] = "Fax No. can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = 'Please provide valid Fax No. of 10 digit';
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_website':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_primary_contact':
            if (isEmpty(value)) {
               add_err[key] = "Primary Contact can't be blank";
            } else if (!isAlpha(value)) {
               add_err[key] = "Numeric Not Allowed";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_primary_contact_phone':
            if (isEmpty(value)) {
               add_err[key] = "Primary Contact Phone can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = 'Please provide valid Phone No.';
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_network':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'provider_status':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         /** 2nd step**/

         //physical

         case 'physical_location_name':
            
            var uniLoc = this.state.addData.location.filter((s, sidx) => idx !== sidx && value == s.physical_location_name)

            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Location Name can't be blank";
            } else if(uniLoc.length > 0) {
               add_err['location'][idx][key] = "Location Name should be unique";
            } else {
               add_err['location'][idx][key] = '';
            }
            
            this.state.addData.bank_details.map((data, sidx) => {
               if(this.state.addData.location[idx].physical_location_name == data.location_name) {
                  add_err['bank_details'][sidx]['location_name'] = "Please Select Location";
               }
            })

            this.state.addData.speciality.map((data, sidx) => {
               if(this.state.addData.location[idx].physical_location_name == data.spl_location_name) {
                  add_err['speciality'][sidx]['spl_location_name'] = "Please Select Location";
               }
            })

            //console.log(loc_err)
            //add_err['bank_details'][idx]['location_name'] = "Please Select Location";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_location_name': value, };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_address1':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Address can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_address1': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_address2':
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_address2': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_country':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Please Select Country";
            } else {
               add_err['location'][idx][key] = '';
               this.props.getProviderStates(value, idx, 0);
            }
            add_err['location'][idx]['physical_state'] = "State can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_country_id': value, 'physical_state_id': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_state':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Please Select State";
            } else {
               add_err['location'][idx][key] = '';
               //this.props.getProviderRegion(value,idx,0);
            }
            add_err['location'][idx]['physical_region'] = "Region can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_state_id': value, 'physical_region_id': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_region':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Region can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_region_id': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_county':
            /*if (isEmpty(value)) {
               add_err['location'][idx][key] = "County can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }*/
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_county': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_city':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "City can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_city': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break; isAlphaNumeric
         case 'physical_zip_code':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Zip Code can't be blank";
            } else if (!isZip(value)) {
               add_err['location'][idx][key] = "Please provide valid Zip Code";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_zip_code': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'physical_phone_no':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Phone No. can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = "Please provide valid Phone No.";
            } else {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'physical_phone_no': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'primary_address_flag':
            console.log('onchange')
            console.log(value)
            let addData = this.state.addData.location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.primary_address_flag = 0
                  
               }
               return item
            })
            this.setState({ addData: addData })
            value = (this.state.addData.location[idx].primary_address_flag) ? 0 : 1;
            if(value == 0) {
               add_err['location'][idx][key] = "Atleast should be one primary address.";
               add_err.primary_flag = "Atleast should be one primary address.";
            } else {
               add_err['location'][idx][key] = "";
               add_err.primary_flag = "";
            }

            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_address_flag': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;

         case 'primary_billing_address_flag':

            let addDataa = this.state.addData.location.map(function (item, idsx) {
               if (idx != idsx) {
                  item.primary_billing_address_flag = 0
               }
               return item
            })
            this.setState({ addData: addDataa })
            value = (this.state.addData.location[idx].primary_billing_address_flag) ? 0 : 1;
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'primary_billing_address_flag': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;

         case 'billing_address_flag':

            var val = (value == true) ? 1 : 0;
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;

               return { ...data, 'billing_address_flag': val };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;


         //billing

         case 'billing_location_name':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Location Name can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_location_name': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_address1':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Address can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_address1': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_address2':
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_address2': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_country':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Please Select Country";
            } else {
               add_err['location'][idx][key] = '';
               this.props.getProviderStates(value, idx, 1);
            }
            add_err['location'][idx]['billing_state'] = "State can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_country_id': value, 'billing_state_id': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_state':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Please Select State";
            } else {
               add_err['location'][idx][key] = '';
               //this.props.getProviderRegion(value,idx,1);
            }
            add_err['location'][idx]['billing_region'] = "Region can't be blank";
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_state_id': value, 'billing_region_id': '' };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_region':

            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Region can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_region_id': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_county':
            /*if (isEmpty(value)) {
               add_err['location'][idx][key] = "County can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }*/
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_county': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_city':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "City can't be blank";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_city': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_zip_code':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Zip Code can't be blank";
            } else if (!isZip(value)) {
               add_err['location'][idx][key] = "Please provide valid Zip Code";
            } else {
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_zip_code': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;
         case 'billing_phone_no':
            if (isEmpty(value)) {
               add_err['location'][idx][key] = "Phone No. can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = "Please provide valid Phone No.";
            } else {
               value = formatPhoneNumber(value)
               add_err['location'][idx][key] = '';
            }
            newShareholders = this.state.addData.location.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'billing_phone_no': value };
            });
            key = 'location';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newShareholders
               }
            });
            break;

         /** 3rd step **/
         case 'spl_location_name':
            if (isEmpty(value)) {
               add_err['speciality'][idx][key] = "Please Select Location";
            } else {
               add_err['speciality'][idx][key] = '';
            }
            newThirdTabSpl = this.state.addData.speciality.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'spl_location_name': value };
            });
            key = 'speciality';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabSpl
               }
            });
            break;
         case 'specialization':
            if (isEmpty(value)) {
               add_err['speciality'][idx][key] = "Please Select Speciality";
            } else {
               add_err['speciality'][idx][key] = '';
               this.props.getSpclProcedure(value, idx);
            }
            newThirdTabSpl = this.state.addData.speciality.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'specialization': value, procedure_id: '' };
            });
            key = 'speciality';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabSpl
               }
            });
            break;
         case 'procedure_id':
            if (isEmpty(value)) {
               add_err['speciality'][idx][key] = "Please Select Procedure";
            } else {
               add_err['speciality'][idx][key] = '';

            }
            newThirdTabSpl = this.state.addData.speciality.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'procedure_id': value };
            });
            key = 'speciality';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabSpl
               }
            });
            break;
         case 'doctor_f_name':
            if (isEmpty(value)) {
               add_err['speciality'][idx][key] = "Doctor last name can't be blank";
            } else {
               add_err['speciality'][idx][key] = '';
            }
            newThirdTabSpl = this.state.addData.speciality.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'doctor_f_name': value };
            });
            key = 'speciality';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabSpl
               }
            });
            break;
         case 'doctor_l_name':
            if (isEmpty(value)) {
               add_err['speciality'][idx][key] = "Doctor first name can't be blank";
            } else {
               add_err['speciality'][idx][key] = '';
            }
            newThirdTabSpl = this.state.addData.speciality.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'doctor_l_name': value };
            });
            key = 'speciality';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabSpl
               }
            });
            break;

         ////////

         case 'location_name':
            if (isEmpty(value)) {
               add_err['bank_details'][idx][key] = "Please Select Location";
            } else {
               add_err['bank_details'][idx][key] = '';
            }
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'location_name': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'bank_name':
            if (isEmpty(value)) {
               add_err['bank_details'][idx][key] = "Bank Name can't be blank";
            } else {
               add_err['bank_details'][idx][key] = '';
            }
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_name': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'bank_address':
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_address': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'rounting_no':
            if (isEmpty(value)) {
               add_err['bank_details'][idx][key] = "Routing No. can't be blank";
            } else if (isNumeric(value)) {
               add_err['bank_details'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 9, trim: true })) {
               add_err['bank_details'][idx][key] = "Please enter 9 digit valid routing number";
            } else {
               add_err['bank_details'][idx][key] = '';
            }
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'rounting_no': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'bank_accout':
            if (isEmpty(value)) {
               add_err['bank_details'][idx][key] = "Bank A/C# can't be blank";
            } else if (isNumeric(value)) {
               add_err['bank_details'][idx][key] = "Allow only Numeric";
            } else if (isLength(value, { lt: 8, trim: true })) {
               add_err['bank_details'][idx][key] = "Please enter correct bank account number";
            } else {
               add_err['bank_details'][idx][key] = '';
            }
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_accout': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'name_on_account':
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'name_on_account': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;
         case 'bank_account_type':
            if (isEmpty(value)) {
               add_err['bank_details'][idx][key] = "Select Bank A/C Type";
            } else {
               add_err['bank_details'][idx][key] = '';
            }
            newThirdTabBank = this.state.addData.bank_details.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'bank_account_type_id': value };
            });
            key = 'bank_details';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newThirdTabBank
               }
            });
            break;

         /** 4th step **/
         case 'discount_type':
            var text = value.target.options[value.target.selectedIndex].text
            value = value.target.value;

            this.state.addData.discount_rate.forEach(function(s, sidx) {

               add_err['discount_rate'][sidx]['discount_to_date'] = "";

            })
            
            if (isEmpty(value)) {
               add_err['discount_rate'][idx][key] = "Please Select Discount Type";
            } else if (text == 'Promotional') {
               var filter_val = true;
               add_err['discount_rate'][idx]['discount_from_date'] = "Please Select Date";
               add_err['discount_rate'][idx]['discount_to_date'] = "Please Select Date";
            } else {
               add_err['discount_rate'][idx][key] = '';
               var filter_val = false;
               add_err['discount_rate'][idx]['discount_from_date'] = "Please Select Date";
               add_err['discount_rate'][idx]['discount_to_date'] = "";
            }
            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'discount_type_id': value, 'discount_type_filter': filter_val, 'discount_from_date': '', 'discount_to_date': '', discount_type: text };
            });
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;
         case 'loan_term_month':
            if (isEmpty(value)) {
               add_err['discount_rate'][idx][key] = "Please Select Loan Term";
            } else {
               add_err['discount_rate'][idx][key] = '';
            }

            //console.log(this.state.addData.discount_rate[idx].discount_type)

            /*this.state.addData.discount_rate.forEach(function(s, sidx) {

               add_err['discount_rate'][sidx]['discount_to_date'] = "";
               add_err['discount_rate'][sidx]['discount_from_date'] = "";

            })*/

            if(this.state.addData.discount_rate[idx].discount_type == "Promotional") {

               this.state.addData.discount_rate.forEach(function(s, sidx) {

                  add_err['discount_rate'][sidx]['discount_to_date'] = "";
                  add_err['discount_rate'][sidx]['discount_from_date'] = "";

               })

               add_err['discount_rate'][idx]['discount_from_date'] = "Please Select Date";
               add_err['discount_rate'][idx]['discount_to_date'] = "Please Select Date";

            } else {

               this.state.addData.discount_rate.forEach(function(s, sidx) {

                  add_err['discount_rate'][sidx]['discount_from_date'] = "";

               })
               add_err['discount_rate'][idx]['discount_from_date'] = "Please Select Date";

            }
            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'loan_term_month': value, 'discount_from_date': '', 'discount_to_date': '' };
            });
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;
         case 'discount_rate':
            if (isEmpty(value)) {
               add_err['discount_rate'][idx][key] = "Discount Rate can't be blank";
            } else if (!isDecimals(value)) {
               add_err['discount_rate'][idx][key] = "Allow only numeric and two decimal digit";
            } else {
               add_err['discount_rate'][idx][key] = '';
            }
            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'discount_rate': value };
            });
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;
         case 'redu_discount_rate':
            if (isEmpty(value)) {
               add_err['discount_rate'][idx][key] = "Reduced Discount Rate can't be blank";
            } else if (!isDecimals(value)) {
               add_err['discount_rate'][idx][key] = "Allow only numeric and two decimal digit";
            } else if (value > 100) {
               add_err['discount_rate'][idx][key] = "Value should not be greated than 100"
            } else {
               add_err['discount_rate'][idx][key] = '';
            }
            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'redu_discount_rate': value };
            });
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;
         case 'discount_from_date':
         //console.log(value)
         //console.log('this.state.addData.discount_rate')
         //console.log(this.state.addData.discount_rate)
         //console.log(value)
            if (value == null) {
               //value = moment(value).format('MM/DD/YYYY');
               add_err['discount_rate'][idx][key] = "Please select from date";
            } else if(moment(moment(this.state.addData.discount_rate[idx]['discount_to_date'], 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment(value).format('YYYY-MM-DD'))) {
               //value = moment(value).format('MM/DD/YYYY');
               add_err['discount_rate'][idx][key] = "Date should not be greater than expiry date";
            } else {
               value = moment(value).format('MM/DD/YYYY');

               var dateTo = this.state.addData.discount_rate.filter((s, sidx) => idx === sidx)
               var toDate = dateTo[0].discount_to_date;
               var trm = this.state.addData.discount_rate[idx].loan_term_month;
               var dtype = this.state.addData.discount_rate[idx].discount_type;
               //console.log(trm)
               //console.log(dtype)
               if (dtype == 'Promotional') {
                  var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (new Date(value) >= new Date(s.discount_from_date) && new Date(value) <= new Date(s.discount_to_date) && trm == s.loan_term_month && dtype == s.discount_type))
                  var dateFilterStd = [];
                  var dateFilterTest = [];
               } else {
                  var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (new Date(value) >= new Date(s.discount_from_date) && new Date(value) <= new Date(s.discount_to_date) && s.discount_type !== 'Promotional' && trm == s.loan_term_month))
                  
                  var dateFilterStd = this.state.addData.discount_rate.filter((s, sidx) => (s.discount_to_date == null || s.discount_to_date == '') && s.discount_type != 'Promotional' && trm == s.loan_term_month && idx != sidx)

                  /*var dateFilterStd2 = this.state.addData.discount_rate.filter((s, sidx) => {
                     if(s.discount_to_date == null && s.discount_type !== 'Promotional' && trm == s.loan_term_month && idx != sidx) {
                        console.log('sfsdfsfsf---')
                     }
                     console.log(idx)
                     console.log(sidx)
                     console.log(s.loan_term_month)
                     console.log(s.discount_type)
                     console.log(dtype)
                  })*/
                  /*console.log('----------')
                  console.log(dateFilterStd)
                  console.log(this.state.addData)*/

                  var dateFilterTest = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && sidx <= idx && value <= s.discount_from_date && s.discount_type !== 'Promotional' && trm == s.loan_term_month)

                  

                  var erIdx = idx;
                  var btd = ''
                  this.state.addData.discount_rate.forEach(function(s, sidx) {
                     //console.log(s)
                     if((s.discount_to_date == null || s.discount_to_date == '') && s.discount_type != 'Promotional' && trm == s.loan_term_month && idx != sidx) {
                        erIdx = sidx
                        btd = 1;
                     }

                  })

               }
               this.setState({ blank_to_date: btd })
               //console.log(erIdx)
               //console.log(idx)
               if (dateFilter.length > 0) {
                  add_err['discount_rate'][idx][key] = 'Date should not be overlap with other discount.';
               } else if(dateFilterStd.length > 0 && erIdx < idx) {
                  add_err['discount_rate'][erIdx]['discount_to_date'] = 'Date must be filled, if you want add more discount.';
               } else if(dateFilterTest.length > 0) {
                  add_err['discount_rate'][idx][key] = 'Date should be greater than previous date';
               } else {
                  console.log('elseeef')
                  /*console.log(erIdx)
                  console.log(idx)*/
                  var erIdx = idx;
                  this.state.addData.discount_rate.forEach(function(s, sidx) {
                     //console.log(s)
                     if((s.discount_to_date !== null) && s.discount_type != 'Promotional' && trm == s.loan_term_month && idx != sidx) {
                        erIdx = sidx
                        btd = 1;
                        add_err['discount_rate'][erIdx]['discount_to_date'] = '';
                     }

                  })
                  add_err['discount_rate'][idx][key] = '';
                  
               }

               if (value == dateTo[0].discount_to_date) {
                  add_err['discount_rate'][idx]['discount_to_date'] = '';
               }
               
            }

            this.state.addData.discount_rate.map((data, sidx) => {
               if(idx == sidx) {
                  if(data.discount_to_date != '') {
                     add_err['discount_rate'][idx]['discount_to_date'] = "";
                  }
               }
            })

            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'discount_from_date': value, 'discount_to_date': '' };
            });
            console.log(this.state)
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;
         case 'discount_to_date':
            //console.log(value)
            if (value == null) {
               //value = moment(value).format('MM/DD/YYYY');
               add_err['discount_rate'][idx][key] = "";

            } else if(moment(moment(value, 'MM/DD/YYYY', true).format('YYYY-MM-DD')).isBefore(moment(this.state.addData.discount_rate[idx]['discount_from_date']).format('YYYY-MM-DD'))) {
               add_err['discount_rate'][idx][key] = "Date should not be less than from date";
            } else {
               value = moment(value).format('MM/DD/YYYY');
               
               if(this.state.blank_to_date == 1) {
                  this.setState({blank_to_date: ''})
               }

               var dateFrom = this.state.addData.discount_rate.filter((s, sidx) => idx === sidx);
               var fromDate = dateFrom[0].discount_from_date;
               var trm = this.state.addData.discount_rate[idx].loan_term_month;
               var dtype = this.state.addData.discount_rate[idx].discount_type;


               if (dtype == 'Promotional') {
                  var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (new Date(fromDate) <= new Date(s.discount_from_date) && new Date(value) >= new Date(s.discount_from_date) && trm == s.loan_term_month && dtype == s.discount_type))
                  var dateFilterStd = [];
                  
               } else {
                  var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (new Date(fromDate) <= new Date(s.discount_from_date) && new Date(value) >= new Date(s.discount_from_date) && s.discount_type !== 'Promotional' && trm == s.loan_term_month))

                  var dateFilterStd = this.state.addData.discount_rate.filter((s, sidx) => (s.discount_to_date == null || s.discount_to_date == '') && s.discount_type != 'Promotional' && trm == s.loan_term_month && idx != sidx)


                  var erIdx = idx;
                  var btd = ''
                  this.state.addData.discount_rate.forEach(function(s, sidx) {
                     //console.log(s)
                     if((s.discount_to_date == null || s.discount_to_date == '') && s.discount_type != 'Promotional' && trm == s.loan_term_month && idx != sidx) {
                        erIdx = sidx
                        btd = 1;
                     }

                  })

               }
               
               //console.log('dateFilterStd to date')
               //console.log(dateFilterStd)
               //var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (value >= s.discount_from_date && value <= s.discount_to_date && s.discount_type !== 'Promotional' && trm == s.loan_term_month) )

               //var dateFilter = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx && (fromDate <= s.discount_from_date && value >= s.discount_from_date))
               

               if (dateFilter.length > 0) {
                  add_err['discount_rate'][idx][key] = 'Date should not be overlap with other discount';
               } else if(dateFilterStd.length > 0 && erIdx > idx) {
                  add_err['discount_rate'][erIdx]['discount_from_date'] = '';
                  add_err['discount_rate'][idx][key] = '';
               } else {
                  add_err['discount_rate'][idx][key] = '';
               }
            }

            


            newFourthTabDiscount = this.state.addData.discount_rate.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'discount_to_date': value };
            });
            key = 'discount_rate';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDiscount
               }
            });
            break;

         case 'additional_fee_type':
            var text = value.target.options[value.target.selectedIndex].text
            value = value.target.value;

            var add_field;
            if(value == '' ) {
               add_field = 1;
            } else {
               add_field = ""
               /*add_err['additional_fee'][idx]['additional_conv_fee'] = "Conv fee can't blank";
               add_err['additional_fee'][idx]['additional_from_date'] = "Select from date";
               add_err['additional_fee'][idx]['additional_to_date'] = "Select to date";*/
            }
            /*if (isEmpty(value)) {
               add_err['additional_fee'][idx][key] = "Please Select Fee Type";
            } else {
               add_err['additional_fee'][idx][key] = '';
            }*/
            newFourthTabFee = this.state.addData.additional_fee.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'additional_fee_type_id': value, 'additional_fee_field': add_field, 'additional_conv_fee': '', 'additional_from_date': '', 'additional_to_date': '' };
            });
            key = 'additional_fee';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabFee
               }
            });
            break;
         case 'additional_conv_fee':
            /*if (isEmpty(value)) {
               add_err['additional_fee'][idx][key] = "Fee Type can't be blank";
            } else */if (isNumeric(value)) {
               add_err['additional_fee'][idx][key] = "Allow only Numeric";
            } else {
               add_err['additional_fee'][idx][key] = '';
            }
            newFourthTabFee = this.state.addData.additional_fee.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'additional_conv_fee': value };
            });
            key = 'additional_fee';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabFee
               }
            });
            break;
         case 'additional_from_date':
            if (value == null) {
               add_err['additional_fee'][idx][key] = "";
            } else {
               value = moment(value).format('MM/DD/YYYY');
               add_err['additional_fee'][idx][key] = '';
            }
            newFourthTabFee = this.state.addData.additional_fee.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'additional_from_date': value };
            });
            key = 'additional_fee';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabFee
               }
            });
            break;
         case 'additional_to_date':

            if (value == null) {
               add_err['additional_fee'][idx][key] = "";
            } else {
               value = moment(value).format('MM/DD/YYYY');
               add_err['additional_fee'][idx][key] = '';
            }
            newFourthTabFee = this.state.addData.additional_fee.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'additional_to_date': value };
            });
            key = 'additional_fee';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabFee
               }
            });
            break;
         case 'document_type':
            if (isEmpty(value)) {
               add_err['documents'][idx][key] = "";
            } else {
               add_err['documents'][idx][key] = '';
            }
            newFourthTabDoc = this.state.addData.documents.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'document_type': value };
            });
            key = 'documents';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDoc
               }
            });

            /*if (isEmpty(value)) {
               add_err[key] = "Select Document Type";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });*/
            break;
         case 'document_name':
            if (isEmpty(value)) {
               add_err['documents'][idx][key] = "";
            } else {
               add_err['documents'][idx][key] = '';
            }
            newFourthTabDoc = this.state.addData.documents.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'document_name': value };
            });
            key = 'documents';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDoc
               }
            });

            /*if (isEmpty(value)) {
               add_err[key] = "Document Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });*/
            break;
         case 'document_upload':
            value = value.target.files[0];
            //console.log(value)
            if (isObjectEmpty(value)) {
               add_err['documents'][idx][key] = "";
            } else {
               add_err['documents'][idx][key] = '';
            }
            newFourthTabDoc = this.state.addData.documents.map((data, sidx) => {
               if (idx !== sidx) return data;
               return { ...data, 'document_upload': value };
            });
            key = 'documents';
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: newFourthTabDoc
               }
            });

            /*if (isObjectEmpty(value)) {
               add_err[key] = "Please upload document";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });*/
            break;
         case 'payment_term':

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         /** 5th step **/
         case 'user_first_name':
            if (isEmpty(value)) {
               add_err[key] = "First Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_middle_name':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_last_name':
            if (isEmpty(value)) {
               add_err[key] = "First Name can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_email':
            if (isEmpty(value)) {
               add_err[key] = "Email address can't be blank";
            } if (!isEmail(value)) {
               add_err[key] = "Please provide corect email";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         case 'user_role':
            if (isEmpty(value)) {
               add_err[key] = "Select User Role";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_location':

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_phone_no':
            if (isEmpty(value)) {
               add_err[key] = "Phone No can't be blank";
            } else if (isPhone(value)) {
               value = formatPhoneNumber(value)
               add_err[key] = 'Please provide valid Phone No.';
            } else {
               value = formatPhoneNumber(value)
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_primary_contact_flag':
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: (value == true) ? 1 : 0
               }
            });
            break;
         case 'username':
            if (isEmpty(value)) {
               add_err[key] = "Username can't be blank";
            } else {
               add_err[key] = '';
            }
            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_password':
            if (isEmpty(value)) {
               add_err[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (this.state.addData.user_confirm_password !== '' && this.state.addData.user_confirm_password !== value) {
               add_err[key] = "Password and confirm password not match";
            } else if (this.state.addData.user_confirm_password !== '' && this.state.addData.user_confirm_password === value) {
               add_err[key] = "";
               add_err['user_confirm_password'] = "";
            } else {
               add_err[key] = '';
            }

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;
         case 'user_confirm_password':
            if (isEmpty(value)) {
               add_err[key] = "Password can't be blank";
            } else if (isContainWhiteSpace(value)) {
               add_err[key] = "Password should not contain white spaces";
            } else if (!isPassword(value, { min: 8, trim: true })) {
               add_err[key] = "Password should be minimum of 8 characters with at least 1 capital character & 1 digit";
            } else if (value !== this.state.addData.user_password) {
               add_err[key] = "Password and confirm password not match";
            } else if (value === this.state.addData.user_password) {
               add_err[key] = "";
               add_err['user_password'] = "";
            } else {
               add_err[key] = '';
            }

            this.setState({
               addData: {
                  ...this.state.addData,
                  [key]: value
               }
            });
            break;

         default:
            break;




      }


      this.setState({ add_err: add_err });


   }
   completedSteps() {
      return Object.keys(this.state.completed).length;
   }

   totalSteps = () => {
      return getSteps().length;
   };

   isLastStep() {
      return this.state.activeStep === this.totalSteps() - 1;
   }

   allStepsCompleted() {
      return this.completedSteps() === this.totalSteps();
   }
   submitData() {

   }
   handleNext = () => {
      let activeStep;
      //console.log(this.state.activeStep)
      

      if (this.isLastStep()) {

         activeStep = 5;
         this.submitData();

         /*console.log('this.state.addData')
         console.log(this.state.addData)*/
         //return false;

         let datafrom = new FormData();
         var fileArr = [];
         for (const file of this.state.addData.documents) {
            datafrom.append('imgename', 'date-' + Date.now());
            if (typeof file.document_upload === 'object') {

               datafrom.append('files_id', file.document_name);
            }
            datafrom.append('files', file.document_upload);


         }
         //datafrom.append('imgename', 'date-'+ Date.now());
         //datafrom.append('imgename2', 'date-aman');
         //datafrom.append('imgedata', this.state.addData.document_upload);
         datafrom.append('addData', JSON.stringify(this.state.addData));

         var img = this.state.addData.document_upload;


         this.props.updateProvider(datafrom);
         //this.setState({addData:null});

      } else {
         activeStep = this.state.activeStep + 1;
      }

      if (activeStep == 4) {
         
         var chkVal = [89, 90, 91, 92];
         var existTerm = [];
         this.state.addData.discount_rate.map((rate) => {
            existTerm.push(parseInt(rate.loan_term_month))
         })
         existTerm = existTerm.filter((x, i, a) => a.indexOf(x) == i)
         //console.log(existTerm)

         var final = chkVal.filter(function(item) {
           return !existTerm.includes(item);
         })

         
         //var msg = '';
         if(final.length > 0) {
            /*final.map((dis) => {
               console.log(dis)
               msg = msg +', '+ dis
            })*/
            this.setState({ discount_msg: "Please add discount for each term" })
            return false;
         }

      }

      this.setState({
         activeStep,
      });
   };

   handleBack = () => {
      const { activeStep } = this.state;
      this.setState({
         activeStep: activeStep - 1,
      });
   };

   handleCancel = () => {
      this.setState({
         cancelState: 1,
         addData: ''
      });
   };

   handleStep = step => () => {

      if (step == 4) {
         
         var chkVal = [89, 90, 91, 92];
         var existTerm = [];
         this.state.addData.discount_rate.map((rate) => {
            existTerm.push(parseInt(rate.loan_term_month))
         })
         existTerm = existTerm.filter((x, i, a) => a.indexOf(x) == i)
         //console.log(existTerm)

         var final = chkVal.filter(function(item) {
           return !existTerm.includes(item);
         })

         
         //var msg = '';
         if(final.length > 0) {
            /*final.map((dis) => {
               console.log(dis)
               msg = msg +', '+ dis
            })*/
            this.setState({ discount_msg: "Please add discount for each term" })
            return false;
         }

      }
      
      //console.log(step)
      if (this.validateSubmit()) {
         this.setState({
            activeStep: step,
         });
      }

   };

   handleComplete = () => {
      const { completed } = this.state;
      completed[this.state.activeStep] = true;
      this.setState({
         completed,
      });
      this.handleNext();
   };

   handleReset = () => {
      this.setState({
         activeStep: 0,
         completed: {},
      });
   };
   /*
      * Title :- validateAddSubmit
      * Descrpation :- This function use for enable or disable submit button in add case
      * Author : Cognizsoft and Ramesh Kumar
      * Date :- April 16,2019
      */

   validateDiscount() {
      const { addData } = this.state;
      if (this.state.activeStep === 3) {

         var dis = true;

         var dd = this.state.blank_to_date
         //console.log('dd-------')
         //console.log(dd)
         this.state.add_err.discount_rate.map(function (item, idx) {
            if (dis == true) {
               if (addData.discount_rate[idx].discount_type_filter == false || addData.discount_rate[idx].discount_type_filter === '') {
                  dis = (

                     (item.discount_type === undefined || item.discount_type === '') &&
                     item.loan_term_month === '' &&
                     item.discount_rate === '' &&
                     item.redu_discount_rate === '' &&
                     item.discount_from_date === '' &&
                     //item.discount_to_date === ''
                     (dd === undefined || dd === '')

                  ) ? true : false;

               } else {

                  dis = (

                     (item.discount_type === undefined || item.discount_type === '') &&
                     item.loan_term_month === '' &&
                     item.discount_rate === '' &&
                     item.redu_discount_rate === '' &&
                     item.discount_from_date === '' &&
                     item.discount_to_date === ''

                  ) ? true : false;

               }
            }
         });

         
         return (dis == true) ? true : false;

      }
   }


   validateSubmit() {
      const { addData } = this.state;
      if (this.state.activeStep === 0) {
         return (
            this.state.add_err.provider_name === '' &&
            this.state.add_err.provider_type === '' &&
            this.state.add_err.provider_taxid_ssn === '' &&
            this.state.add_err.provider_phone_no === '' &&
            this.state.add_err.provider_email === '' &&
            this.state.add_err.provider_fax_no === '' &&
            this.state.add_err.provider_primary_contact === '' &&
            this.state.add_err.provider_primary_contact_phone === ''
         );
         //return true;
      } else if (this.state.activeStep === 1) {
         var loc = true;

         var prm_fl = this.state.add_err.primary_flag;
         
         this.state.add_err.location.map(function (item, idx) {
            if (loc == true) {
               if (addData.location[idx].billing_address_flag == 0) {
                  loc = (

                     item.physical_location_name === '' &&
                     item.physical_address1 === '' &&
                     //item.physical_country === '' && 
                     item.physical_state === '' &&
                     //item.physical_region === '' && 
                     item.physical_city === '' &&
                     item.physical_zip_code === '' &&
                     item.physical_phone_no === '' &&
                     item.billing_location_name === '' &&
                     item.billing_address1 === '' &&
                     //item.billing_country === '' && 
                     item.billing_state === '' &&
                     //item.billing_region === '' && 
                     item.billing_city === '' &&
                     item.billing_zip_code === '' &&
                     item.billing_phone_no === '' &&
                     prm_fl === ''

                  ) ? true : false;
               } else {
                  loc = (

                     item.physical_location_name === '' &&
                     item.physical_address1 === '' &&
                     //item.physical_country === '' && 
                     item.physical_state === '' &&
                     //item.physical_region === '' && 
                     item.physical_city === '' &&
                     item.physical_zip_code === '' &&
                     item.physical_phone_no === '' &&
                     prm_fl === ''

                  ) ? true : false;
               }
            }
         });



         return loc;
         //return true;
      } else if (this.state.activeStep === 2) {
         //var mrg = this.state.add_err.bank_details.concat(this.state.add_err.speciality);
         var bank = true;
         this.state.add_err.bank_details.map(function (item, idx) {
            if (bank == true) {
               bank = (

                  item.location_name === '' &&
                  item.bank_name === '' &&
                  item.rounting_no === '' &&
                  item.bank_accout === '' &&
                  item.bank_account_type === ''

               ) ? true : false;
            }
         });

         //////////////////

         /*var spl = true;
         this.state.add_err.speciality.map(function(item, idx) {
            if(spl == true) {
               spl = (

                  item.spl_location_name === '' && 
                  item.specialization === '' 

               ) ? true : false;
            }
         });*/

         return (bank == true) ? true : false;

      } else if (this.state.activeStep === 3) {

         var dis = true;

         /*this.state.addData.discount_rate.forEach(function(s, sidx) {
         
            add_err['discount_rate'][sidx]['discount_to_date'] = "";         

         })*/

         var dd = this.state.blank_to_date
         this.state.add_err.discount_rate.map(function (item, idx) {
            if (dis == true) {
               if (addData.discount_rate[idx].discount_type_filter == false || addData.discount_rate[idx].discount_type_filter === '') {
                  dis = (

                     (item.discount_type === undefined || item.discount_type === '') &&
                     item.loan_term_month === '' &&
                     item.discount_rate === '' &&
                     item.redu_discount_rate === '' &&
                     item.discount_from_date === '' &&
                     //item.discount_to_date === ''
                     (dd === undefined || dd === '')

                  ) ? true : false;

               } else {

                  dis = (

                     (item.discount_type === undefined || item.discount_type === '') &&
                     item.loan_term_month === '' &&
                     item.discount_rate === '' &&
                     item.redu_discount_rate === '' &&
                     item.discount_from_date === '' &&
                     item.discount_to_date === ''

                  ) ? true : false;

               }
            }
         });

         /////////////





         return (dis == true) ? true : false;

      } else if (this.state.activeStep === 4) {
         var spl = true;
         this.state.add_err.speciality.map(function (item, idx) {
            if (spl == true) {
               spl = (

                  item.spl_location_name === '' &&
                  item.specialization === '' &&
                  item.doctor_f_name === '' &&
                  item.doctor_l_name === ''

               ) ? true : false;
            }
         });
         return spl;
      } else if (this.state.activeStep === 5) {
         return (
            this.state.add_err.user_first_name === '' &&
            this.state.add_err.user_last_name === '' &&
            //this.state.add_err.user_email === '' &&
            this.state.add_err.user_role === '' &&
            this.state.add_err.user_phone_no === ''
         );
         //return true;
      }
   }
   handleNameChange = evt => {
      this.setState({ name: evt.target.value });
   };

   handleShareholderNameChange(idx, value) {
      const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
         if (idx !== sidx) return shareholder;
         return { ...shareholder, name: value };
      });
      this.setState({ shareholders: newShareholders });
   };

   handleSubmit = evt => {
      const { name, shareholders } = this.state;
      alert(`Incorporated: ${name} with ${shareholders.length} shareholders`);
   };

   handleAddShareholder() {
      const { addData, add_err } = this.state;
      addData.location = this.state.addData.location.concat([{
         physical_location_name: "",
         physical_address1: "",
         physical_address2: "",
         physical_country: "",
         physical_state: "",
         physical_region: "",
         physical_county: "",
         physical_city: "",
         physical_zip_code: "",
         physical_phone_no: "",

         billing_location_name: "",
         billing_address1: "",
         billing_address2: "",
         billing_city: "",
         billing_zip_code: "",
         billing_phone_no: "",
         billing_state: "",
         billing_country: "",
         billing_county: "",
         billing_region: "",
         billing_address_flag: 0,
      }]);
      this.props.getProviderStates(this.props.provider_country[0].id, addData.location.length - 1, 1, 1);
      add_err.location = this.state.add_err.location.concat([{
         /*physical_location_name: "",
         physical_address1: "",
         physical_address2: "",
         physical_country: "",
         physical_state: "",
         physical_region: "",
         physical_county: "",
         physical_city: "",
         physical_zip_code: "",
         physical_phone_no: "",

         billing_location_name: "",
         billing_address1: "",
         billing_address2: "",
         billing_city: "",
         billing_zip_code: "",
         billing_phone_no: "",
         billing_state: "",
         billing_county:"",
         billing_country:"",
         billing_region:"",*/
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };


   handleRemoveShareholder(idx) {
      const { addData, add_err } = this.state;
      this.props.removeAddMoreProvider(idx);
      if (this.state.addData.location[idx].primary_address_flag == 1) {
         add_err['primary_flag'] = "Atleast should be one primary address.";
      }
      addData.location = this.state.addData.location.filter((s, sidx) => idx !== sidx)
      add_err.location = this.state.add_err.location.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   //THIRD TAB BANK

   handleThirdTabBankNameChange(idx, value) {
      const newThirdTabBank = this.state.thirdTabBank.map((thirdtab_bank, sidx) => {
         if (idx !== sidx) return thirdtab_bank;
         return { ...thirdtab_bank, name: value };
      });
      this.setState({ thirdTabBank: newThirdTabBank });
   };

   handleAddThirdTabBank() {
      const { addData, add_err } = this.state;
      addData.bank_details = this.state.addData.bank_details.concat([{
         location_name: "",
         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_accout: "",
         name_on_account: "",
         bank_account_type: "",
      }]);
      add_err.bank_details = this.state.add_err.bank_details.concat([{
         /*location_name: "",
         bank_name: "",
         bank_address: "",
         rounting_no: "",
         bank_accout: "",
         name_on_account: "",
         bank_account_type: "",*/
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   handleRemoveThirdTabBank(idx) {
      this.props.removeAddMoreProvider(idx);
      const { addData, add_err } = this.state;
      addData.bank_details = this.state.addData.bank_details.filter((s, sidx) => idx !== sidx)
      add_err.bank_details = this.state.add_err.bank_details.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   //THIRD TAB SPECIALITY

   handleThirdTabSplNameChange(idx, value) {
      const newThirdTabSpl = this.state.thirdTabSpl.map((thirdtab_spl, sidx) => {
         if (idx !== sidx) return thirdtab_spl;
         return { ...thirdtab_spl, name: value };
      });
      this.setState({ thirdTabSpl: newThirdTabSpl });
   };

   handleAddThirdTabSpl() {
      const { addData, add_err } = this.state;
      addData.speciality = this.state.addData.speciality.concat([{
         spl_location_name: "",
         specialization: "",
         doctor_f_name: "",
         doctor_l_name: "",
      }]);
      add_err.speciality = this.state.add_err.speciality.concat([{
         /*spl_location_name: "",
         specialization: "",*/
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   handleRemoveThirdTabSpl(idx) {
      this.props.removeAddMoreProvider(idx);
      const { addData, add_err } = this.state;
      addData.speciality = this.state.addData.speciality.filter((s, sidx) => idx !== sidx)
      add_err.speciality = this.state.add_err.speciality.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   //FOURTH TAB DISCOUNT

   handleFourthTabDiscountNameChange(idx, value) {
      const newFourthTabDiscount = this.state.fourthTabDiscount.map((fourthtab_discount, sidx) => {
         if (idx !== sidx) return fourthtab_discount;
         return { ...fourthtab_discount, name: value };
      });
      this.setState({ fourthTabDiscount: newFourthTabDiscount });
   };

   handleAddFourthTabDiscount() {
      const { addData, add_err } = this.state;

      

      /*var chkVal = ["6", "12", "18", "24"];
      var existTerm = [];
      addData.discount_rate.map((rate) => {
         existTerm.push(rate.loan_term_month_value)
      })
      existTerm = existTerm.filter((x, i, a) => a.indexOf(x) == i)
      //console.log(existTerm)

      var final = chkVal.filter(function(item) {
        return !existTerm.includes(item);
      })

      //console.log(final)
      var msg = '';
      if(final.length > 0) {
         final.map((dis) => {
            console.log(dis)
            msg = msg +', '+ dis
         })
         this.setState({ discount_msg: "Please add discount for"+msg+" term(s)" })
         return false;
      }*/

      if(!this.validateDiscount()) {
         this.setState({ discount_msg: "Please fill all required fields" })
         //console.log(this.state.add_err)
         return false;
      }
      this.setState({ discount_msg: "" })
      addData.discount_rate = this.state.addData.discount_rate.concat([{
         discount_type: "",
         discount_rate: "",
         redu_discount_rate: "",
         loan_term_month: "",
         discount_from_date: "",
         discount_to_date: "",
         discount_type_filter: "",
      }]);
      add_err.discount_rate = this.state.add_err.discount_rate.concat([{
         /*discount_type: "",
         discount_rate: "",
         discount_from_date: "",
         discount_to_date: "",*/
      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   handleRemoveFourthTabDiscount(idx) {
      this.props.removeAddMoreProvider(idx);
      const { addData, add_err } = this.state;

      this.state.addData.discount_rate.forEach(function(s, sidx) {
         
         add_err['discount_rate'][sidx]['discount_to_date'] = "";         

      })
      
      
      //add_err['discount_rate'][idx][key] = "Please select to date";
      addData.discount_rate = this.state.addData.discount_rate.filter((s, sidx) => idx !== sidx)
      add_err.discount_rate = this.state.add_err.discount_rate.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err, blank_to_date: ''
      });
   };

   //FOURTH TAB FEE

   handleFourthTabFeeNameChange(idx, value) {
      const newFourthTabFee = this.state.fourthTabFee.map((fourthtab_fee, sidx) => {
         if (idx !== sidx) return fourthtab_fee;
         return { ...fourthtab_fee, name: value };
      });
      this.setState({ fourthTabFee: newFourthTabFee });
   };

   handleAddFourthTabFee() {
      const { addData, add_err } = this.state;
      addData.additional_fee = this.state.addData.additional_fee.concat([{
         additional_fee_field: 1,
         additional_fee_type: "",
         additional_conv_fee: "",
         additional_from_date: "",
         additional_to_date: "",
      }]);
      add_err.additional_fee = this.state.add_err.additional_fee.concat([{

      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   handleRemoveFourthTabFee(idx) {
      this.props.removeAddMoreProvider(idx);
      const { addData, add_err } = this.state;
      addData.additional_fee = this.state.addData.additional_fee.filter((s, sidx) => idx !== sidx)
      add_err.additional_fee = this.state.add_err.additional_fee.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   //FOURTH TAB DOCUMENT

   handleFourthTabDocNameChange(idx, value) {
      const newFourthTabDoc = this.state.fourthTabDoc.map((fourthtab_doc, sidx) => {
         if (idx !== sidx) return fourthtab_doc;
         return { ...fourthtab_doc, name: value };
      });
      this.setState({ fourthTabDoc: newFourthTabDoc });
   };

   handleAddFourthTabDoc() {
      const { addData, add_err } = this.state;
      addData.documents = this.state.addData.documents.concat([{
         document_type: "",
         document_name: "",
         document_upload: "",
      }]);
      add_err.documents = this.state.add_err.documents.concat([{

      }]);
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   handleRemoveFourthTabDoc(idx) {
      this.props.removeAddMoreProvider(idx);
      const { addData, add_err } = this.state;
      addData.documents = this.state.addData.documents.filter((s, sidx) => idx !== sidx)
      add_err.documents = this.state.add_err.documents.filter((s, sidx) => idx !== sidx)
      this.setState({
         addData: addData, add_err: add_err
      });
   };

   ////

   checkSsnExist(value, md_id) {
      this.props.checkSsnExist(value, md_id);
   }

   componentWillReceiveProps(nextProps) {
      let { addData, add_err } = this.state;

      (nextProps.redirectURL != '') ? this.setState({ changeURL: nextProps.redirectURL, willUpdate: false }) : '';

      (nextProps.nameExist && nextProps.isEdit == 0) ? add_err['provider_taxid_ssn'] = "Tax ID/SSN already exists" : '';
      if (nextProps.provider_details != '' && nextProps.provider_details !== undefined && this.state.willUpdate == false) {

         addData.provider_id = nextProps.provider_details[0].provider_id;
         addData.provider_name = nextProps.provider_details[0].provider_name;
         addData.provider_type = nextProps.provider_details[0].provider_type_id;
         addData.provider_taxid_ssn = nextProps.provider_details[0].provider_taxid_ssn;
         addData.provider_phone_no = nextProps.provider_details[0].provider_phone_no;
         addData.provider_email = nextProps.provider_details[0].provider_email;
         addData.provider_fax_no = nextProps.provider_details[0].provider_fax_no;
         addData.provider_website = nextProps.provider_details[0].provider_website;
         addData.provider_primary_contact = nextProps.provider_details[0].provider_primary_contact;
         addData.provider_primary_contact_phone = nextProps.provider_details[0].provider_primary_contact_phone;
         addData.provider_network = nextProps.provider_details[0].provider_network;
         addData.provider_status = nextProps.provider_details[0].provider_status;

         addData.user_first_name = nextProps.provider_user[0].user_first_name;
         addData.user_middle_name = nextProps.provider_user[0].user_middle_name;
         addData.user_last_name = nextProps.provider_user[0].user_last_name;
         addData.user_email = nextProps.provider_user[0].user_email;
         addData.user_role = nextProps.provider_user[0].user_role_id;
         addData.user_location = nextProps.provider_user[0].user_location;
         addData.user_phone_no = nextProps.provider_user[0].user_phone_no;
         addData.user_primary_contact_flag = nextProps.provider_user[0].user_primary_contact_flag;

         addData.location = nextProps.provider_location;
         add_err.location = nextProps.provider_details_loc_add_err;

         addData.bank_details = nextProps.provider_bank;
         add_err.bank_details = nextProps.provider_bank_add_err;

         addData.speciality = nextProps.provider_specility;
         add_err.speciality = nextProps.provider_spl_add_err;


         if (nextProps.provider_specility !== undefined) {

            var checkSpc = 0;
            nextProps.provider_specility.forEach(function (item, idx) {

               if (checkSpc == idx) {
                  var splc = nextProps.provider_specility_doctors.filter((s, idx) => s.location_id == item.provider_location_provider_location_id && item.specialization == s.mdv_speciality_id);

                  splc.forEach(function (doc, id) {
                     addData.speciality[checkSpc].doctor_f_name = doc.f_name;
                     addData.speciality[checkSpc].doctor_l_name = doc.l_name;


                     add_err.speciality[checkSpc].doctor_f_name = '';
                     add_err.speciality[checkSpc].doctor_l_name = '';
                     checkSpc++;
                  })

               }

               //console.log(splc)
            })
         }


         //addData.speciality = nextProps.provider_specility_doctors;

         addData.discount_rate = nextProps.provider_additional_discount;
         add_err.discount_rate = nextProps.provider_addi_dis_add_err;

         var provider_additional_fee = '';
         var provider_additional_fee_err = '';

         if (nextProps.provider_additional_fee.length !== 0) {
            provider_additional_fee = nextProps.provider_additional_fee;
            
            provider_additional_fee_err = nextProps.provider_addi_fee_add_err;
         } else {

            var add_fee = [{
               additional_fee_field: 1,
               additional_fee_type_id: "",
               additional_conv_fee: "",
               additional_from_date: "",
               additional_to_date: "",
            }]

            provider_additional_fee = add_fee;

            var add_fee_err = [{

            }]

            provider_additional_fee_err = add_fee_err;
         }

         addData.additional_fee = provider_additional_fee;
         add_err.additional_fee = provider_additional_fee_err;

         var provider_document = '';
         var provider_document_err = '';

         if (nextProps.provider_document && nextProps.provider_document.length !== 0) {
            provider_document = nextProps.provider_document;
            provider_document_err = nextProps.provider_doc_add_err;
         } else {
            var doc = [{
               document_type: "",
               document_name: "",
               document_upload: "",
            }]

            provider_document = doc;

            var doc_err = [{

            }]

            provider_document_err = doc_err;
         }

         addData.documents = provider_document;
         add_err.documents = provider_document_err;

         this.setState({ addData: addData })
         this.setState({ add_err: add_err })
         this.setState({ willUpdate: true })

      }

   }

   componentDidMount() {
      this.props.getProviderOption();
      this.props.providerTypeMasterDataValueList();
      this.props.providerCountry();
      this.props.ProviderDetailsEdit(this.props.provider_edit_id);

   }

   render() {

      //console.log('this.state')
      //console.log(this.state)
      if (this.state.changeURL == 1) {
         return (<Redirect to={"/admin/providers/provider-list"} />);
      }
      if (this.state.cancelState == 1) {
         return (<Redirect to={"/admin/providers/provider-list"} />);
      }

      const steps = getSteps();
      const master_provider_type = this.props.provider_type_master_data_value;
      const provider_country = this.props.provider_country;
      const provider_option_bank = this.props.provider_option_bank;
      const provider_option_spl = this.props.provider_option_spl;
      const provider_option_discount_type = this.props.provider_option_discount_type;
      const provider_option_fee_type = this.props.provider_option_fee_type;
      const provider_option_document_type = this.props.provider_option_document_type;
      const provider_option_user_role = this.props.provider_option_user_role;
      const provider_spl_procedure = this.props.splProcedure;



      const aloading = this.props.aloading;
      const provider_details = this.props.provider_details;
      const provider_bank = this.props.provider_bank;
      const provider_location = this.props.provider_location;
      const provider_user = this.props.provider_user;
      const provider_additional = this.props.provider_additional;
      const provider_document = this.props.provider_document;
      const provider_payment_term = this.props.provider_payment_term;
      const provider_loan_term_months = this.props.provider_loan_term_months;


      const { activeStep } = this.state;
      return (
         <div className="stepper-outer">

            {aloading !== false &&
               <div className="editprovider_loader w-100 p-70">
                  <RctSectionLoader />



               </div>
            }
            {provider_details &&

               <div className="aaas">

                  <Stepper nonLinear activeStep={activeStep}>
                     {steps.map((label, index) => {
                        return (
                           <Step key={label} >
                              <StepButton onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                                 {label}
                              </StepButton>
                           </Step>
                        );
                     })}
                  </Stepper>
                  <div>
                     {this.allStepsCompleted() ? (
                        <div className="pl-40">
                           <p>All steps completed - you&quot;re finished</p>
                           <Button variant="contained" className="btn-success text-white" onClick={this.handleReset}>Reset</Button>
                        </div>
                     ) : (
                           <div className="text-right">
                              <p></p>
                              {(() => {

                                 switch (activeStep) {
                                    case 0:
                                       return <StepFirst
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          masterProviderType={master_provider_type}
                                          checkSsnExist={this.checkSsnExist.bind(this)}
                                       />
                                       break;
                                    case 1:
                                       return <StepSecond
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          handleShareholderNameChange={this.handleShareholderNameChange.bind(this)}
                                          handleRemoveShareholder={this.handleRemoveShareholder.bind(this)}
                                          handleAddShareholder={this.handleAddShareholder.bind(this)}
                                          shareholders={this.state.shareholders}
                                          providerCountry={provider_country}
                                          stateType={this.props.stateType}
                                          regionType={this.props.regionType}
                                          stateType2={this.props.stateType2}
                                          regionType2={this.props.regionType2}
                                          onCheckedClick={this.onCheckedClick.bind(this)}
                                       />
                                       break;
                                    case 2:
                                       return <StepThird
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          handleThirdTabBankNameChange={this.handleThirdTabBankNameChange.bind(this)}
                                          handleRemoveThirdTabBank={this.handleRemoveThirdTabBank.bind(this)}
                                          handleAddThirdTabBank={this.handleAddThirdTabBank.bind(this)}
                                          thirdTabBank={this.state.thirdTabBank}

                                          handleThirdTabSplNameChange={this.handleThirdTabSplNameChange.bind(this)}

                                          provider_option_bank={provider_option_bank}
                                       />
                                       break;
                                    case 3:
                                       return <StepFourth
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          provider_document={provider_document}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          handleFourthTabDiscountNameChange={this.handleFourthTabDiscountNameChange.bind(this)}
                                          handleRemoveFourthTabDiscount={this.handleRemoveFourthTabDiscount.bind(this)}
                                          handleAddFourthTabDiscount={this.handleAddFourthTabDiscount.bind(this)}
                                          fourthTabFee={this.state.fourthTabFee}
                                          handleFourthTabFeeNameChange={this.handleFourthTabFeeNameChange.bind(this)}
                                          handleRemoveFourthTabFee={this.handleRemoveFourthTabFee.bind(this)}
                                          handleAddFourthTabFee={this.handleAddFourthTabFee.bind(this)}
                                          fourthTabFee={this.state.fourthTabFee}
                                          handleFourthTabDocNameChange={this.handleFourthTabDocNameChange.bind(this)}
                                          handleRemoveFourthTabDoc={this.handleRemoveFourthTabDoc.bind(this)}
                                          handleAddFourthTabDoc={this.handleAddFourthTabDoc.bind(this)}
                                          fourthTabDoc={this.state.fourthTabDoc}
                                          DatePicker={DatePicker}
                                          provider_option_discount_type={provider_option_discount_type}
                                          provider_option_fee_type={provider_option_fee_type}
                                          provider_option_document_type={provider_option_document_type}
                                          opnDocFileModal={this.opnDocFileModal.bind(this)}
                                          provider_payment_term={provider_payment_term}
                                          provider_loan_term_months={provider_loan_term_months}
                                          provider_option_spl={provider_option_spl}
                                          provider_spl_procedure={provider_spl_procedure}
                                          handleRemoveThirdTabSpl={this.handleRemoveThirdTabSpl.bind(this)}
                                          handleAddThirdTabSpl={this.handleAddThirdTabSpl.bind(this)}
                                          thirdTabSpl={this.state.thirdTabSpl}
                                          discount_msg={this.state.discount_msg}
                                       />
                                       break;
                                    case 4:
                                       return <StepFourthPhy
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          provider_document={provider_document}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          handleFourthTabDiscountNameChange={this.handleFourthTabDiscountNameChange.bind(this)}
                                          handleRemoveFourthTabDiscount={this.handleRemoveFourthTabDiscount.bind(this)}
                                          handleAddFourthTabDiscount={this.handleAddFourthTabDiscount.bind(this)}
                                          fourthTabFee={this.state.fourthTabFee}
                                          handleFourthTabFeeNameChange={this.handleFourthTabFeeNameChange.bind(this)}
                                          handleRemoveFourthTabFee={this.handleRemoveFourthTabFee.bind(this)}
                                          handleAddFourthTabFee={this.handleAddFourthTabFee.bind(this)}
                                          fourthTabFee={this.state.fourthTabFee}
                                          handleFourthTabDocNameChange={this.handleFourthTabDocNameChange.bind(this)}
                                          handleRemoveFourthTabDoc={this.handleRemoveFourthTabDoc.bind(this)}
                                          handleAddFourthTabDoc={this.handleAddFourthTabDoc.bind(this)}
                                          fourthTabDoc={this.state.fourthTabDoc}
                                          DatePicker={DatePicker}
                                          provider_option_discount_type={provider_option_discount_type}
                                          provider_option_fee_type={provider_option_fee_type}
                                          provider_option_document_type={provider_option_document_type}
                                          opnDocFileModal={this.opnDocFileModal.bind(this)}
                                          provider_payment_term={provider_payment_term}
                                          provider_loan_term_months={provider_loan_term_months}
                                          provider_option_spl={provider_option_spl}
                                          provider_spl_procedure={provider_spl_procedure}
                                          handleRemoveThirdTabSpl={this.handleRemoveThirdTabSpl.bind(this)}
                                          handleAddThirdTabSpl={this.handleAddThirdTabSpl.bind(this)}
                                          thirdTabSpl={this.state.thirdTabSpl}
                                       />
                                       break;
                                    case 5:
                                       return <StepFifth
                                          addErr={this.state.add_err}
                                          addData={this.state.addData}
                                          provider_user={provider_user}
                                          onChnagerovider={this.onChnagerovider.bind(this)}
                                          provider_option_user_role={provider_option_user_role}
                                       />
                                       break;
                                    default:
                                       return (<div>out</div>)
                                 }
                              })()}
                              <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.handleCancel}>
                                 Cancel
                           </Button>
                              <Button variant="contained" color="primary" className="text-white mr-10 mb-10" disabled={activeStep === 0} onClick={this.handleBack}>
                                 Back
                           </Button>
                              <Button variant="contained" color="primary" className="text-white mr-10 mb-10" onClick={this.handleNext} disabled={!this.validateSubmit()} >
                                 {(activeStep === 5) ? 'Update' : 'Next'}
                              </Button>

                           </div>
                        )}
                  </div>



                  <Modal isOpen={this.state.opnDocFileModal} toggle={() => this.opnViewDocFileModalClose()}>

                     <ModalHeader toggle={() => this.opnViewDocFileModalClose()}>
                        Document File
                  </ModalHeader>

                     <ModalBody>
                        {this.state.imgPath &&
                           <embed src={this.state.imgPath} width="100%" height="350" />

                        }
                     </ModalBody>

                  </Modal>


               </div>
            }
         </div>
      );
   }
}

// map state to props
const mapStateToProps = ({ Provider, authUser }) => {

   const { nameExist, isEdit } = authUser;
   const { loading, aloading, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_term, provider_type_master_data_value, provider_country, stateType, regionType, stateType2, regionType2, provider_option_bank, provider_payment_term, provider_loan_term_months, provider_option_spl, provider_specility, provider_specility_doctors, provider_option_fee_type, provider_option_discount_type, provider_option_document_type, provider_option_user_role, provider_details_loc_add_err, provider_bank_add_err, provider_spl_add_err, provider_addi_dis_add_err, provider_addi_fee_add_err, provider_doc_add_err, splProcedure, redirectURL } = Provider;

   const user = authUser.user;


   return { nameExist, isEdit, loading, aloading, user, provider_details, provider_location, provider_bank, provider_user, provider_additional_discount, provider_additional_fee, provider_document, provider_term, provider_type_master_data_value, provider_country, stateType, regionType, stateType2, regionType2, provider_option_bank, provider_payment_term, provider_loan_term_months, provider_option_spl, provider_specility, provider_specility_doctors, provider_option_fee_type, provider_option_discount_type, provider_option_document_type, provider_option_user_role, provider_details_loc_add_err, provider_bank_add_err, provider_spl_add_err, provider_addi_dis_add_err, provider_addi_fee_add_err, provider_doc_add_err, splProcedure, redirectURL }

}

export default connect(mapStateToProps, {
   checkSsnExist, providerTypeMasterDataValueList, ProviderDetailsEdit, updateProvider, providerCountry, getProviderStates, getProviderRegion, getProviderOption, removeAddMoreProvider, getSpclProcedure
}, null, { pure: false })(EditProvider);