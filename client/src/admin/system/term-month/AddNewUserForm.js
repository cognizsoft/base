/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewUserDetails, onChangeAddNewUserDetails, checkNameExist }) => (
    <Form>
       
  
        <FormGroup>
            <Label for="term_month">Term Month<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="term_month"
                id="term_month"
                fullWidth
                variant="outlined"
                placeholder="Enter Term Month"
                value={addNewUserDetails.term_month}
                error={(addErr.term_month)?true:false}
                helperText={addErr.term_month}
                onChange={(e) => onChangeAddNewUserDetails('term_month', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="risk_factor">Risk Factor<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="risk_factor"
                id="risk_factor"
                fullWidth
                variant="outlined"
                placeholder="Enter Risk Factor"
                value={addNewUserDetails.risk_factor}
                error={(addErr.risk_factor)?true:false}
                helperText={addErr.risk_factor}
                onChange={(e) => onChangeAddNewUserDetails('risk_factor', e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="user_type_desc">Description</Label>
                <Input
                    type="textarea"
                    name="term_month_desc"
                    id="term_month_desc"
                    placeholder="Enter Term Month Desc"
                    value={addNewUserDetails.term_month_desc}
                    onChange={(e) => onChangeAddNewUserDetails('term_month_desc', e.target.value)}
                />
        </FormGroup>
        <Label for="status">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(addNewUserDetails.status == 1) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(addNewUserDetails.status == 0) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
    </Form>
);

export default AddNewUserForm;
