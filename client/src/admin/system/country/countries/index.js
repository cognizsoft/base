/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isAlphaDigitUnderscoreDash, isEmpty, isMaster } from '../../../../validator/Validator';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
  checkCAbbreviationExist, checkSANameExist, getAllCountry, insertCountry, updateCountry
} from 'Actions';

class Country extends Component {

   state = {
      currentModule: 18,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedCountry: null, // selected user to perform operations
      loading: false, // loading activity
      opnAddNewCountryModal: false, // add new user form modal
      addViewCountryModal: false, // add view user form modal
      addNewCountryDetail: {
         name: '',
         abbreviation: '',
         status: 1,
      },
      openViewCountryDialog: false, // view user dialog box
      editCountry: null,
      allSelected: false,
      selectedCountry: 0,
      add_err: {

      },
      update_err: {
         name: '',
         abbreviation: '',
      },
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllCountry();
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
	/**
	 * Open Add New Country Modal
	 */
   opnAddNewCountryModal() {
      this.setState({ opnAddNewCountryModal: true });
   }



   /**
	 * View Country Detail Hanlder
	 */
   viewCountryDetail(Country) {
      this.setState({ openViewCountryDialog: true, selectedCountry: Country });
   }

	/**
	 * On Edit Country
	 */
   onEditCountry(Country) {
      console.log(Country)
      this.setState({ opnAddNewCountryModal: true, editCountry: Country });
   }

	/**
	 * On Add & Update Country Modal Close
	 */
   onAddUpdateCountryModalClose() {
      let emptyError = {
      }
      let addNewCountryDetail = {
         name: '',
         abbreviation: '',
         status: 1,
      }
      let emptyUpdateError = {
         name: '',
         abbreviation: '',
      }
      this.setState({ opnAddNewCountryModal: false, editCountry: null, add_err: emptyError, addNewCountryDetail: addNewCountryDetail, update_err: emptyUpdateError })
   }

   /**
   * On View User Modal Close
   */
   onViewCountryModalClose = () => {
      this.setState({ openViewCountryDialog: false, selectedCountry: null })
   }
   /*
   * Title :- opnAddNewCountryModal
   * Descrpation :- This function use for open add user popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   opnAddNewCountryModal() {
      this.setState({ opnAddNewCountryModal: true });
   }
   /*
   * Title :- onChangeAddNewCountryDetails
   * Descrpation :- This function use for check field validation on add new user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onChangeAddNewCountryDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Country name can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
            break;
         case 'abbreviation':
            if (isEmpty(value)) {
               add_err[key] = "Abbreviation can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid abbreviation.";
            }*/else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewCountryDetail: {
            ...this.state.addNewCountryDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- onUpdateCountryDetails
   * Descrpation :- This function use for check filed validation on edit user case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onUpdateCountryDetails(key, value) {

      let { update_err } = this.state;
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               update_err[key] = "Country name can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               update_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               update_err[key] = '';
            }
            break;
         case 'abbreviation':
            if (isEmpty(value)) {
               update_err[key] = "Abbreviation can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               update_err[key] = "Please enter a valid Abbreviation.";
            } */else {
               update_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ update_err: update_err });

      this.setState({
         editCountry: {
            ...this.state.editCountry,
            [key]: value
         }
      });
   }
   /*
      * Title :- validateAddSubmit
      * Descrpation :- This function use for enable or disable submit button in add case
      * Author : Cognizsoft and Ramesh Kumar
      * Date :- April 16,2019
      */
   validateAddSubmit() {
      return (
         this.state.add_err.name === '' &&
         this.state.add_err.abbreviation === ''
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.update_err.name === '' &&
         this.state.update_err.abbreviation === ''
      );
   }
   /*
   * Title :- addNewUser
   * Descrpation :- This function use for add new user action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   addNewCountry() {

      this.props.insertCountry(this.state.addNewCountryDetail);
      this.setState({ opnAddNewCountryModal: false });
      let self = this;
      let addNewCountryDetail = {
         name: '',
         abbreviation: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewCountryDetail: addNewCountryDetail });
      }, 2000);
   }
   /*
   * Title :- checkUsernameExist
   * Descrpation :- This function use for check username exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkNameExist(value, id) {
      console.log(this.state)
      this.props.checkSANameExist(value, id);
   }
   /*
   * Title :- checkEmailExist
   * Descrpation :- This function use for check eamil exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkAbbreviationExist(value, md_id) {
      this.props.checkCAbbreviationExist(value, md_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { add_err } = this.state;

      let { update_err } = this.state;
      (nextProps.nameExist && nextProps.isEdit == 0) ? add_err['name'] = "Name already exists" : "";
      (nextProps.nameExist && nextProps.isEdit == 1) ? update_err['name'] = "Name already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ update_err: update_err });


   }
   /*
   * Title :- updateCountry
   * Descrpation :- This function use call user update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   updateCountry() {
      const { editCountry } = this.state;

      let indexOfUpdateCountry = '';
      let countryCurrent = this.props.countrylist;
      for (let i = 0; i < countryCurrent.length; i++) {
         const current = countryCurrent[i];
         if (current.id === editCountry.id) {
            indexOfUpdateCountry = i
         }
      }
      this.props.updateCountry(editCountry);

      countryCurrent[indexOfUpdateCountry] = editCountry;

      this.setState({ editCountry: null, opnAddNewCountryModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ countryCurrent });
      //}, 2000);
   }
   render() {
      const { selectedCountry, editCountry, add_err, update_err } = this.state;
      const countrylist = this.props.countrylist;
      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Country Name',
            field: 'name',
         },
         {
            name: 'Abbreviation',
            field: 'abbreviation',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewCountryDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditCountry(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Country.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewCountryModal={this.opnAddNewCountryModal.bind(this)} />:''
            );
         },
      };
      return (
         <div className="country countries">
            <Helmet>
               <title>Health Partner | System | Country | Countries</title>
               <meta name="description" content="Countries" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.systemCountries" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={countrylist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.opnAddNewCountryModal} toggle={() => this.onAddUpdateCountryModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateCountryModalClose()}>
                  {editCountry === null ?
                     'Add Country' : 'Update Country'
                  }
               </ModalHeader>
               <ModalBody>
                  {editCountry === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewCountryDetails={this.state.addNewCountryDetail}
                        onChangeAddNewCountryDetails={this.onChangeAddNewCountryDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                        checkAbbreviationExist={this.checkAbbreviationExist.bind(this)}
                     />
                     : <UpdateUserForm
                        editCountry={editCountry}
                        updateErr={update_err}
                        addNewCountryDetails={this.state.addNewCountryDetails}
                        onUpdateCountryDetails={this.onUpdateCountryDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                        checkAbbreviationExist={this.checkAbbreviationExist.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editCountry === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewCountry()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateCountry()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCountryModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewCountryDialog} toggle={() => this.onViewCountryModalClose()}>
               <ModalHeader toggle={() => this.onViewCountryModalClose()}>
                  {selectedCountry !== null ? 'Country View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedCountry !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Country Name:</span> <span className="second-colmn">{selectedCountry.name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Abbreviation:</span> <span className="second-colmn">{selectedCountry.abbreviation}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedCountry.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, countryList }) => {
  
   const { user } = authUser;
   const { loading, countrylist, nameExist, isEdit, abbreviationExist } = countryList;
   return { loading, countrylist, user, nameExist, isEdit, abbreviationExist }

}

export default connect(mapStateToProps, {
   checkCAbbreviationExist, checkSANameExist, getAllCountry, insertCountry, updateCountry
})(Country);