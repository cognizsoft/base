/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ editCountry, updateErr, onUpdateCountryDetails, checkNameExist, checkAbbreviationExist }) => (
    <Form>
        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Country Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter Country Name"
                        fullWidth
                        variant="outlined"
                        value={editCountry.name}
                        error={(updateErr.name) ? true : false}
                        helperText={updateErr.name}
                        onChange={(e) => onUpdateCountryDetails('name', e.target.value)}
                        onKeyUp={(e) => checkNameExist(e.target.value, editCountry.id)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="abbreviation">Abbreviation<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="abbreviation"
                        id="abbreviation"
                        placeholder="Enter Abbreviation"
                        fullWidth
                        variant="outlined"
                        inputProps={{maxLength:3}}
                        value={editCountry.abbreviation}
                        error={(updateErr.abbreviation) ? true : false}
                        helperText={updateErr.abbreviation}
                        onChange={(e) => onUpdateCountryDetails('abbreviation', e.target.value)}
                        //onBlur={(e) => checkAbbreviationExist(e.target.value)}
                    />
                </FormGroup>
            </div>

           
            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(editCountry.status == 1) ? true : false}
                                onChange={(e) => onUpdateCountryDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(editCountry.status == 0) ? true : false}
                                onChange={(e) => onUpdateCountryDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

        </div>
    </Form>
);

export default UpdateUserForm;
