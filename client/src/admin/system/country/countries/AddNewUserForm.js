/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewCountryDetails, onChangeAddNewCountryDetails, checkNameExist, checkAbbreviationExist }) => (
    <Form>
        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Country Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter State Name"
                        fullWidth
                        variant="outlined"
                        value={addNewCountryDetails.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onChangeAddNewCountryDetails('name', e.target.value)}
                        onKeyUp={(e) => checkNameExist(e.target.value)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Abbreviation<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="abbreviation"
                        id="abbreviation"
                        placeholder="Enter Abbreviation"
                        fullWidth
                        variant="outlined"
                        inputProps={{maxLength:3}}
                        value={addNewCountryDetails.abbreviation}
                        error={(addErr.abbreviation) ? true : false}
                        helperText={addErr.abbreviation}
                        onChange={(e) => onChangeAddNewCountryDetails('abbreviation', e.target.value)}
                        //onBlur={(e) => checkAbbreviationExist(e.target.value)}
                    />
                </FormGroup>
            </div>

           

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewCountryDetails.status == 1) ? true : false}
                                onChange={(e) => onChangeAddNewCountryDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewCountryDetails.status == 0) ? true : false}
                                onChange={(e) => onChangeAddNewCountryDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

        </div>
    </Form>
);

export default AddNewUserForm;
