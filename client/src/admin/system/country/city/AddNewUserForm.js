/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const AddNewUserForm = ({ addErr, addNewCityDetails, onChangeAddNewCityDetails, stateList, countryList, checkNameExist }) => (
    <Form>
        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="country_id">Country Name<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="country_id"
                        id="country_id"
                        placeholder="Enter Type"
                        onChange={(e) => onChangeAddNewCityDetails('country_id', e.target.value)}
                    >

                        <option value="">Select</option>
                        {countryList && countryList.map((country, key) => (
                            <option value={country.id} key={key}>{country.name}</option>
                        ))}
                    </Input>
                    {(addErr.country_id) ? <FormHelperText>{addErr.country_id}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="userType">State Name<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="state_id"
                        id="state_id"
                        placeholder="Enter Type"
                        onChange={(e) => onChangeAddNewCityDetails('state_id', e.target.value)}
                    >

                        <option value="">Select</option>
                        {stateList && stateList.map((state, key) => (
                            <option value={state.state_id} key={key}>{state.name}</option>
                        ))}
                    </Input>
                    {(addErr.state_id) ? <FormHelperText>{addErr.state_id}</FormHelperText> : ''}
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">City Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter City Name"
                        fullWidth
                        variant="outlined"
                        value={addNewCityDetails.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onChangeAddNewCityDetails('name', e.target.value)}
                        onKeyUp={(e) => checkNameExist(e.target.value)}
                    />
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Currently Serving<span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="serving"
                                value={1}
                                checked={(addNewCityDetails.serving == 1) ? true : false}
                                onChange={(e) => onChangeAddNewCityDetails('serving', e.target.value)}
                            />
                            Yes
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="serving"
                                value={0}
                                checked={(addNewCityDetails.serving == 0) ? true : false}
                                onChange={(e) => onChangeAddNewCityDetails('serving', e.target.value)}
                            />
                            No
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status<span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewCityDetails.status == 1) ? true : false}
                                onChange={(e) => onChangeAddNewCityDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewCityDetails.status == 0) ? true : false}
                                onChange={(e) => onChangeAddNewCityDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

        </div>
    </Form>
);

export default AddNewUserForm;
