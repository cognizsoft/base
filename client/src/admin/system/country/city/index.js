/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';


// delete confirmation dialog

import { isEmpty, isMaster } from '../../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllCity, insertCity, updateCity, checkCityExist, selectedCountryState
} from 'Actions';

class City extends Component {

   state = {
      currentModule: 5,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewCityModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewCityDetail: {
         country_id: '',
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      },
      state_list: null,
      openViewCityDialog: false, // view user dialog box
      editCity: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         country_id: '',
         state_id: '',
         name: '',
      },

   }


   checkNameExist(value, md_id) {
      this.props.checkCityExist(value, md_id);
   }

   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.cityExist && nextProps.isEdit == 0) ? add_err['name'] = "City name already exists" : '';
      (nextProps.cityExist && nextProps.isEdit == 1) ? udpate_err['name'] = "City name already exists" : '';

      /*if(nextProps.country_states) {
         let { state_list } = this.state;

         state_list = nextProps.country_states

         this.setState({ state_list: state_list });
      }*/

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err }); 

   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllCity();
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
  permissionFilter = (name) => {
   let per = JSON.parse(this.props.user);

   let newUser = per.user_permission.filter(
      function (per) { return per.description == name }
   );

   this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
}

	 /*
   * Title :- opnAddNewCityModal
   * Descrpation :- This function call user for open popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   opnAddNewCityModal() {
      this.setState({ addNewCityModal: true });
   }

    /*
   * Title :- viewCityDetail
   * Descrpation :- This function use for view pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   viewCityDetail(data) {
      this.setState({ openViewCityDialog: true, selectedUser: data });
   }

	 /*
   * Title :- onEditCity
   * Descrpation :- This function edit pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onEditCity(city) {
      //console.log('city')
      //console.log(city)
      //const filtercountry = this.props.countrylist.filter(x => x.id == city.country_id);

      this.props.selectedCountryState(city.country_id);

      /*var cou_st = (this.props.country_states) ? this.props.country_states.filter(x => x.state_id == city.state_id) : 0;
      console.log(cou_st)
      city.state_id = cou_st[0].state_id;*/

      this.setState({ addNewCityModal: true, editCity: city });

      console.log('editCity')
      console.log(this.state.editCity)
   }

	 /*
   * Title :- onAddUpdateCityModalClose
   * Descrpation :- This function close pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onAddUpdateCityModalClose() {
      let emptyError = {
      }
      let addNewCityDetail = {
         country_id: '',
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      }
      let emptyUpdateError = {
         country_id: '',
         state_id: '',
         name: '',
      }
      this.setState({ addNewCityModal: false, editCity: null, addNewCityDetail: addNewCityDetail, add_err:emptyError, udpate_err: emptyUpdateError })
   }

    /*
   * Title :- onViewUserModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onViewUserModalClose = () => {
      this.setState({ openViewCityDialog: false, selectedUser: null })
   }

   /*
   * Title :- onChangeAddNewUserDetails
   * Descrpation :- This function use for check field validation on add new City
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onChangeAddNewCityDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'country_id':
            if (isEmpty(value)) {
               add_err[key] = "Country name can't be blank";
            } else {
               add_err[key] = '';
               this.props.selectedCountryState(value);
            }
            break;
         case 'state_id':
            if (isEmpty(value)) {
               add_err[key] = "State name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "City name can't be blank";
            } /*else if (!isMaster(value)) {
               add_err[key] = "Please enter a valid City name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewCityDetail: {
            ...this.state.addNewCityDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- onUpdateUserDetails
   * Descrpation :- This function use for check filed validation on edit City case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onUpdateCityDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'country_id':
            if (isEmpty(value)) {
               udpate_err[key] = "Country can't be blank";
            } else {
               udpate_err[key] = '';
               this.props.selectedCountryState(value);
            }
            break;
         case 'state_id':
            if (isEmpty(value)) {
               udpate_err[key] = "State can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               udpate_err[key] = "City name can't be blank";
            } /*else if (!isMaster(value)) {
               udpate_err[key] = "Please enter a valid City name. only allow a-z A-Z 0-9 _ -";
            } */else {
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editCity: {
            ...this.state.editCity,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.country_id === '' &&
         this.state.add_err.state_id === '' &&
         this.state.add_err.name === ''
      );
   }
   /*
   * Title :- addNewcity
   * Descrpation :- This function use for add new city action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   addNewCity() {
      console.log(this.state.addNewCityDetail)
      this.props.insertCity(this.state.addNewCityDetail);
      this.setState({ addNewCityModal: false });
      let self = this;
      let emptyState = {
         country_id: '',
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      }
      setTimeout(() => {
         self.setState({ addNewCityDetail: emptyState });
      }, 2000);
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.udpate_err.country_id === '' &&
         this.state.udpate_err.state_id === '' &&
         this.state.udpate_err.name === ''
      );
   }
   /*
   * Title :- updatecity
   * Descrpation :- This function use call user update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
  updateCity() {
      const { editCity } = this.state;
      //console.log('editCity')
      //console.log(editCity)

      const filtercity = this.props.statelist.filter(x => x.state_id == editCity.state_id);
      const filtercountry = this.props.countrylist.filter(x => x.id == editCity.country_id);

      //console.log('filtercountry')
      //console.log(filtercountry)
      editCity.state_name = filtercity[0].name;
      editCity.country_name = filtercountry[0].name;
      
      let indexOfUpdateCity = '';
      let cities = this.props.citylist;
      //console.log('cities')
      //console.log(cities)
      for (let i = 0; i < cities.length; i++) {
         const city = cities[i];
         if (city.city_id === editCity.city_id) {
            indexOfUpdateCity = i
         }
      }
      //console.log(editCity)
      this.props.updateCity(editCity);

      cities[indexOfUpdateCity] = editCity;

      this.setState({ editCity: null, addNewCityModal: false });
      let self = this;
      self.setState({ cities });
   }
   render() {
      const { add_err, selectedUser, editCity, udpate_err } = this.state;
      //console.log('editCity')
      //console.log(editCity)
      const citylist = this.props.citylist
      const columns = [

         {
            name: 'ID',
            field: 'city_id'
         },
         {
            name: 'City Name',
            field: 'name',
         },
         {
            name: 'State Name',
            field: 'state_name',
         },
         {
            name: 'Country Name',
            field: 'country_name',
         },         
         /*{
            name: 'Currently Serving',
            field: 'serving',
            options: {
               customBodyRender: (value) => {
                  return (
                     value.serving == 1 ? 'Yes' : 'No'
                  );
               }
            }
         },*/
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewCityDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditCity(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Cities.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewCityModal={this.opnAddNewCityModal.bind(this)} />:''
            );
         },

      };
      return (
         <div className="country Cities">
            <Helmet>
               <title>Health Partner | System | Country | Cities</title>
               <meta name="description" content="Cities" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.city" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={citylist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.addNewCityModal} toggle={() => this.onAddUpdateCityModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateCityModalClose()}>
                  {editCity === null ?
                     'Add City' : 'Update City'
                  }
               </ModalHeader>
               <ModalBody>
                  {editCity === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewCityDetails={this.state.addNewCityDetail}
                        onChangeAddNewCityDetails={this.onChangeAddNewCityDetails.bind(this)}
                        stateList={this.props.statelist}
                        countryList={this.props.countrylist}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                     : <UpdateUserForm
                        editCity={editCity}
                        addErr={udpate_err}
                        onUpdateCityDetail={this.onUpdateCityDetails.bind(this)}
                        stateList={this.props.statelist}
                        countryList={this.props.countrylist}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editCity === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewCity()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateCity()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateCityModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewCityDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'City View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                          <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Country Name:</span> <span className="second-colmn">{selectedUser.country_name}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">State Name:</span> <span className="second-colmn">{selectedUser.state_name}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">City Name:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Currently Serving:</span> <span className="second-colmn">{(selectedUser.serving == 1)?'Yes':'NO'}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedUser.status == 1)?'Active':'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}


const mapStateToProps = ({ authUser, regionDetails }) => {
   //console.log('regionDetails')
   //console.log(regionDetails)
   const { user } = authUser;
   const { citylist, loading, statelist, countrylist, cityExist, isEdit } = regionDetails;
   return { citylist, loading, statelist, countrylist, user, cityExist, isEdit }
   //return {}
}

export default connect(mapStateToProps, {
   getAllCity, insertCity, updateCity, checkCityExist, selectedCountryState
})(City);