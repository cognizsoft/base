/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';


// delete confirmation dialog

import { isEmpty, isMaster } from '../../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllRegions, insertRegion, updateRegion, checkRegionExist
} from 'Actions';

class Regions extends Component {

   state = {
      currentModule: 5,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewRegionModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewRegionDetail: {
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      },
      openViewRegionDialog: false, // view user dialog box
      editRegion: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         state_id: '',
         name: '',
      },

   }


   checkNameExist(value, md_id) {
      this.props.checkRegionExist(value, md_id);
   }

   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.regionExist && nextProps.isEdit == 0) ? add_err['name'] = "Region name already exists" : '';
      (nextProps.regionExist && nextProps.isEdit == 1) ? udpate_err['name'] = "Region name already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err }); 

   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllRegions();
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
  permissionFilter = (name) => {
   let per = JSON.parse(this.props.user);

   let newUser = per.user_permission.filter(
      function (per) { return per.description == name }
   );

   this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
}

	 /*
   * Title :- opnAddNewRegionModal
   * Descrpation :- This function call user for open popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   opnAddNewRegionModal() {
      this.setState({ addNewRegionModal: true });
   }

    /*
   * Title :- viewRegionDetail
   * Descrpation :- This function use for view pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   viewRegionDetail(data) {
      this.setState({ openViewRegionDialog: true, selectedUser: data });
   }

	 /*
   * Title :- onEditRegion
   * Descrpation :- This function edit pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onEditRegion(region) {
      console.log('region')
      console.log(region)
      this.setState({ addNewRegionModal: true, editRegion: region });
   }

	 /*
   * Title :- onAddUpdateRegionModalClose
   * Descrpation :- This function close pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onAddUpdateRegionModalClose() {
      let emptyError = {
      }
      let addNewRegionDetail = {
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      }
      let emptyUpdateError = {
         state_id: '',
         name: '',
      }
      this.setState({ addNewRegionModal: false, editRegion: null, addNewRegionDetail: addNewRegionDetail, add_err:emptyError, udpate_err: emptyUpdateError })
   }

    /*
   * Title :- onViewUserModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onViewUserModalClose = () => {
      this.setState({ openViewRegionDialog: false, selectedUser: null })
   }

   /*
   * Title :- onChangeAddNewUserDetails
   * Descrpation :- This function use for check field validation on add new region
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onChangeAddNewRegionDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'state_id':
            if (isEmpty(value)) {
               add_err[key] = "State name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Region name can't be blank";
            } /*else if (!isMaster(value)) {
               add_err[key] = "Please enter a valid region name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewRegionDetail: {
            ...this.state.addNewRegionDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- onUpdateUserDetails
   * Descrpation :- This function use for check filed validation on edit region case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onUpdateRegionDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'state_id':
            if (isEmpty(value)) {
               udpate_err[key] = "State can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               udpate_err[key] = "Region name can't be blank";
            } /*else if (!isMaster(value)) {
               udpate_err[key] = "Please enter a valid region name. only allow a-z A-Z 0-9 _ -";
            } */else {
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editRegion: {
            ...this.state.editRegion,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.state_id === '' &&
         this.state.add_err.name === ''
      );
   }
   /*
   * Title :- addNewRegion
   * Descrpation :- This function use for add new region action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   addNewRegion() {

      this.props.insertRegion(this.state.addNewRegionDetail);
      this.setState({ addNewRegionModal: false });
      let self = this;
      let emptyState = {
         state_id: '',
         name: '',
         serving: 1,
         status: 1
      }
      setTimeout(() => {
         self.setState({ addNewRegionDetail: emptyState });
      }, 2000);
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.udpate_err.state_id === '' &&
         this.state.udpate_err.name === ''
      );
   }
   /*
   * Title :- updateRegion
   * Descrpation :- This function use call user update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
  updateRegion() {
      const { editRegion } = this.state;
      const filterregion = this.props.statelist.filter(x => x.state_id == editRegion.state_id);
      editRegion.state_name = filterregion[0].name;

      let indexOfUpdateRegion = '';
      let regions = this.props.regionlist;
      for (let i = 0; i < regions.length; i++) {
         const region = regions[i];
         if (region.user_id === editRegion.user_id) {
            indexOfUpdateRegion = i
         }
      }
      console.log(editRegion)
      this.props.updateRegion(editRegion);

      regions[indexOfUpdateRegion] = editRegion;

      this.setState({ editRegion: null, addNewRegionModal: false });
      let self = this;
      self.setState({ regions });
   }
   render() {
      const { add_err, selectedUser, editRegion, udpate_err } = this.state;
      const regionlist = this.props.regionlist
      const columns = [

         {
            name: 'ID',
            field: 'region_id'
         },
         {
            name: 'Region Name',
            field: 'name',
         },
         {
            name: 'State Name',
            field: 'state_name',
         },         
         {
            name: 'Currently Serving',
            field: 'serving',
            options: {
               customBodyRender: (value) => {
                  return (
                     value.serving == 1 ? 'Yes' : 'No'
                  );
               }
            }
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewRegionDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditRegion(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Regions.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewRegionModal={this.opnAddNewRegionModal.bind(this)} />:''
            );
         },

      };
      return (
         <div className="country regions">
            <Helmet>
               <title>Health Partner | System | Country | Regions</title>
               <meta name="description" content="Regions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.regions" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={regionlist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.addNewRegionModal} toggle={() => this.onAddUpdateRegionModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateRegionModalClose()}>
                  {editRegion === null ?
                     'Add Region' : 'Update Region'
                  }
               </ModalHeader>
               <ModalBody>
                  {editRegion === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewRegionDetails={this.state.addNewRegionDetail}
                        onChangeAddNewRegionDetails={this.onChangeAddNewRegionDetails.bind(this)}
                        stateList={this.props.statelist}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                     : <UpdateUserForm
                        editRegion={editRegion}
                        addErr={udpate_err}
                        onUpdateRegionDetail={this.onUpdateRegionDetails.bind(this)}
                        stateList={this.props.statelist}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editRegion === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewRegion()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateRegion()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateRegionModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewRegionDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'Region View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                          <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                  <div className="colmn-row"><span className="first-colmn fw-bold">State Name:</span> <span className="second-colmn">{selectedUser.state_name}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Region Name:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Currently Serving:</span> <span className="second-colmn">{(selectedUser.serving == 1)?'Yes':'NO'}</span></div>
                                  <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedUser.status == 1)?'Active':'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}


const mapStateToProps = ({ authUser, regionDetails }) => {
   const { user } = authUser;
   const { regionlist, loading, statelist, regionExist, isEdit } = regionDetails;
   return { regionlist, loading, statelist, user, regionExist, isEdit }
   //return {}
}

export default connect(mapStateToProps, {
   getAllRegions, insertRegion, updateRegion, checkRegionExist
})(Regions);