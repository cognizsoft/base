/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isAlphaDigitUnderscoreDash, isEmpty, isMaster } from '../../../../validator/Validator';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllCountries, getAllstates, insertState, checkSNameExist, checkAbbreviationExist, updateState
} from 'Actions';

class States extends Component {

   state = {
      currentModule: 4,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      opnAddNewStatesModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewStatesDetail: {
         country: '',
         name: '',
         abbreviation: '',
         serving: 1,
         status: 1,
      },
      openViewUserDialog: false, // view user dialog box
      editState: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {

      },
      udpate_err: {
         country_id: '',
         name: '',
         abbreviation: '',
         serving: ''
      },
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllstates();
      this.props.getAllCountries();
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 22,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ opnAddNewStatesModal: true });
   }



   /**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
      this.setState({ openViewUserDialog: true, selectedUser: data });
   }

	/**
	 * On Edit User
	 */
   onEditState(user) {
      console.log(this.state.update_err)
      this.setState({ opnAddNewStatesModal: true, editState: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose() {
      let emptyError = {
      }
      let addNewStatesDetail = {
         country: '',
         name: '',
         abbreviation: '',
         serving: '',
         status: 1,
      }
      let emptyUpdateError = {
         country_id: '',
         name: '',
         abbreviation: '',
         serving: '',
      }
      this.setState({ opnAddNewStatesModal: false, editState: null, add_err: emptyError, addNewStatesDetail: addNewStatesDetail, udpate_err: emptyUpdateError })
   }

   /**
   * On View User Modal Close
   */
   onViewUserModalClose = () => {
      this.setState({ openViewUserDialog: false, selectedUser: null })
   }
   /*
   * Title :- opnAddNewStatesModal
   * Descrpation :- This function use for open add user popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   opnAddNewStatesModal() {
      this.setState({ opnAddNewStatesModal: true });
   }
   /*
   * Title :- onChangeAddNewUserDetails
   * Descrpation :- This function use for check field validation on add new user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onChangeAddNewStatesDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'country':
            if (isEmpty(value)) {
               add_err[key] = "Select Country";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "State name can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               add_err[key] = '';
            }
            break;
         case 'abbreviation':
            if (isEmpty(value)) {
               add_err[key] = "Abbreviation can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               add_err[key] = "Please enter a valid abbreviation.";
            }*/else {
               add_err[key] = '';
            }
            break;
         case 'serving':
            if (isEmpty(value)) {
               add_err[key] = "Serving can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewStatesDetail: {
            ...this.state.addNewStatesDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- onUpdateUserDetails
   * Descrpation :- This function use for check filed validation on edit user case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   onUpdateStateDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'country_id':
            if (isEmpty(value)) {
               udpate_err[key] = "Select Country";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               udpate_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               udpate_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               udpate_err[key] = "State name can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               udpate_err[key] = "Please enter a valid state name. only allow a-z A-Z 0-9 _ -";
            } */else {
               udpate_err[key] = '';
            }
            break;
         case 'abbreviation':
            if (isEmpty(value)) {
               udpate_err[key] = "Abbreviation can't be blank";
            } /*else if (!isAlphaDigitUnderscoreDash(value)) {
               udpate_err[key] = "Please enter a valid Abbreviation.";
            } */else {
               udpate_err[key] = '';
            }
            break;
         case 'serving':
            if (isEmpty(value)) {
               udpate_err[key] = "Serving can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editState: {
            ...this.state.editState,
            [key]: value
         }
      });
   }
   /*
      * Title :- validateAddSubmit
      * Descrpation :- This function use for enable or disable submit button in add case
      * Author : Cognizsoft and Ramesh Kumar
      * Date :- April 16,2019
      */
   validateAddSubmit() {
      return (
         this.state.add_err.country === '' &&
         this.state.add_err.name === '' &&
         this.state.add_err.abbreviation === '' &&
         this.state.add_err.serving === ''
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   validateUpdateSubmit() {
      console.log(this.state.udpate_err)
      return (
         this.state.udpate_err.country_id === '' &&
         this.state.udpate_err.name === '' &&
         this.state.udpate_err.abbreviation === '' &&
         this.state.udpate_err.serving === ''
      );
   }
   /*
   * Title :- addNewUser
   * Descrpation :- This function use for add new user action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   addNewStates() {

      this.props.insertState(this.state.addNewStatesDetail);
      this.setState({ opnAddNewStatesModal: false });
      let self = this;
      let addNewStatesDetail = {
         country: '',
         name: '',
         abbreviation: '',
         serving: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewStatesDetail: addNewStatesDetail });
      }, 2000);
   }
   /*
   * Title :- checkUsernameExist
   * Descrpation :- This function use for check username exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkNameExist(value, md_id) {
      this.props.checkSNameExist(value, md_id);
   }
   /*
   * Title :- checkEmailExist
   * Descrpation :- This function use for check eamil exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   checkAbbreviationExist(value, md_id) {
      this.props.checkAbbreviationExist(value, md_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.stateExist && nextProps.isEditS == 0) ? add_err['name'] = "State name already exists" : '';
      (nextProps.stateExist && nextProps.isEditS == 1) ? udpate_err['name'] = "State name already exists" : '';

      (nextProps.abbreviationExist && nextProps.isEdit == 0) ? add_err['abbreviation'] = "Abbreviation already exists" : '';
      (nextProps.abbreviationExist && nextProps.isEdit == 1) ? udpate_err['abbreviation'] = "Abbreviation already exists" : '';


      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err }); 

   }
   /*
   * Title :- updateUser
   * Descrpation :- This function use call user update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   updateState() {
      const { editState } = this.state;

      const filterType = this.props.countrylist.filter(x => x.id == editState.country_id);
      console.log(editState)
      console.log(this.props.countrylist)
      editState.country = filterType[0].name;
      editState.country_id = filterType[0].id;

      let indexOfUpdateUser = '';
      let stateCurrent = this.props.statelist;
      for (let i = 0; i < stateCurrent.length; i++) {
         const current = stateCurrent[i];
         if (current.state_id === editState.state_id) {
            indexOfUpdateUser = i
         }
      }
      this.props.updateState(editState);

      stateCurrent[indexOfUpdateUser] = editState;

      this.setState({ editState: null, opnAddNewStatesModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ stateCurrent });
      //}, 2000);
   }
   render() {
      const { selectedUser, editState, add_err, udpate_err } = this.state;
      const statelist = this.props.statelist;
      //console.log('statelist')
      console.log(statelist)
      const Countrylist = this.props.countrylist;
      const columns = [

         {
            name: 'ID',
            field: 'state_id'
         },
         {
            name: 'State Name',
            field: 'name',
         },
         {
            name: 'Abbreviation',
            field: 'abbreviation',
         },
         {
            name: 'Currently Serving',
            field: 'serving',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.serving == 1 ? 'Yes' : 'No'
                  );
               }
            }
         },
         {
            name: 'Country',
            field: 'country',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewUserDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditState(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'States.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewStatesModal={this.opnAddNewStatesModal.bind(this)} />:''
            );
         },
      };
      return (
         <div className="country states">
            <Helmet>
               <title>Health Partner | System | Country | States</title>
               <meta name="description" content="States" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.states" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={statelist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.opnAddNewStatesModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  {editState === null ?
                     'Add State' : 'Update State'
                  }
               </ModalHeader>
               <ModalBody>
                  {editState === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewStatesDetails={this.state.addNewStatesDetail}
                        onChangeAddNewStatesDetails={this.onChangeAddNewStatesDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                        checkAbbreviationExist={this.checkAbbreviationExist.bind(this)}
                        countryData={Countrylist}
                     />
                     : <UpdateUserForm
                        editState={editState}
                        addErr={udpate_err}
                        addNewStatesDetails={this.state.addNewStatesDetails}
                        onUpdateStateDetails={this.onUpdateStateDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                        checkAbbreviationExist={this.checkAbbreviationExist.bind(this)}
                        countryData={Countrylist}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editState === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewStates()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateState()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'State View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">State Name:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Abbreviation:</span> <span className="second-colmn">{selectedUser.abbreviation}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Currently Serving:</span> <span className="second-colmn">{(selectedUser.serving == 1) ? 'Yes' : 'No'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedUser.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, statesList }) => {
   const { user } = authUser;
   const { loading, statelist, stateExist, abbreviationExist, countrylist, isEdit, isEditS } = statesList;
   return { loading, statelist, user, stateExist, abbreviationExist, countrylist, isEdit, isEditS }

}

export default connect(mapStateToProps, {
   getAllCountries, getAllstates, insertState, checkSNameExist, checkAbbreviationExist, updateState
})(States);