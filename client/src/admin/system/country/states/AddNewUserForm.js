/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ addErr, addNewStatesDetails, onChangeAddNewStatesDetails, checkNameExist, checkAbbreviationExist, countryData }) => (
    <Form>
        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="country">Country<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="country"
                        id="country"
                        placeholder="Country"
                        onChange={(e) => onChangeAddNewStatesDetails('country', e.target.value)}
                    >
                        <option value="">Select</option>
                        {countryData && countryData.map((opt, key) => (
                            <option value={opt.id} key={key}>{opt.name}</option>
                        ))}
                    </Input>
                    {(addErr.country) ? <FormHelperText>{addErr.country}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">State Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter State Name"
                        fullWidth
                        variant="outlined"
                        value={addNewStatesDetails.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onChangeAddNewStatesDetails('name', e.target.value)}
                        onKeyUp={(e) => checkNameExist(e.target.value)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="abbreviation">Abbreviation<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="abbreviation"
                        id="abbreviation"
                        placeholder="Enter Abbreviation"
                        fullWidth
                        variant="outlined"
                        inputProps={{maxLength:2}}
                        value={addNewStatesDetails.abbreviation}
                        error={(addErr.abbreviation) ? true : false}
                        helperText={addErr.abbreviation}
                        onChange={(e) => onChangeAddNewStatesDetails('abbreviation', e.target.value)}
                        onKeyUp={(e) => checkAbbreviationExist(e.target.value)}
                    />
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Currently Serving<span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="serving"
                                value="1"
                                checked={(addNewStatesDetails.serving == 1) ? true : false}
                                onChange={(e) => onChangeAddNewStatesDetails('serving', e.target.value)}
                            />
                            Yes
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="serving"
                                value="0"
                                checked={(addNewStatesDetails.serving == 0) ? true : false}
                                onChange={(e) => onChangeAddNewStatesDetails('serving', e.target.value)}
                            />
                            No
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>



            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewStatesDetails.status == 1) ? true : false}
                                onChange={(e) => onChangeAddNewStatesDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewStatesDetails.status == 0) ? true : false}
                                onChange={(e) => onChangeAddNewStatesDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

        </div>
    </Form>
);

export default AddNewUserForm;
