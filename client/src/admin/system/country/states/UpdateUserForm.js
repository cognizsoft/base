/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ editState, addErr, onUpdateStateDetails, checkNameExist, checkAbbreviationExist, countryData }) => (
    <Form>
        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="country_id">Country<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="country_id"
                        id="country_id"
                        placeholder="Country"
                        defaultValue={editState.country_id}
                        onChange={(e) => onUpdateStateDetails('country_id', e.target.value)}
                    >
                        <option value="">Select</option>
                        {countryData && countryData.map((opt, key) => (
                            <option value={opt.id} key={key}>{opt.name}</option>
                        ))}
                    </Input>
                    {(addErr.country_id) ? <FormHelperText>{addErr.country_id}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">State Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Enter State Name"
                        fullWidth
                        variant="outlined"
                        value={editState.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onUpdateStateDetails('name', e.target.value)}
                        onKeyUp={(e) => checkNameExist(e.target.value, editState.state_id)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Abbreviation<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="editState"
                        id="editState"
                        placeholder="Enter Abbreviation"
                        fullWidth
                        variant="outlined"
                        inputProps={{maxLength:2}}
                        value={editState.abbreviation}
                        error={(addErr.abbreviation) ? true : false}
                        helperText={addErr.abbreviation}
                        onChange={(e) => onUpdateStateDetails('abbreviation', e.target.value)}
                        onKeyUp={(e) => checkAbbreviationExist(e.target.value, editState.state_id)}
                    />
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Currently Serving<span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="serving"
                                value={1}
                                checked={(editState.serving == 1) ? true : false}
                                onChange={(e) => onUpdateStateDetails('serving', e.target.value)}
                            />
                            Yes
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="serving"
                                value={0}
                                checked={(editState.serving == 0) ? true : false}
                                onChange={(e) => onUpdateStateDetails('serving', e.target.value)}
                            />
                            No
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>



            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(editState.status == 1) ? true : false}
                                onChange={(e) => onUpdateStateDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(editState.status == 0) ? true : false}
                                onChange={(e) => onUpdateStateDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>

        </div>
    </Form>
);

export default UpdateUserForm;
