/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ MFSScoreList, interestRateMList, loanAmountList, paymentTermMonthList, updateErr, updateInterestRateDetails, onUpdateInterestRateDetail,DatePicker,startDate }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-6">

	        <FormGroup>
            <Label for="score_range_id">MFS Score<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="score_range_id"
                id="score_range_id"
                placeholder=""
                defaultValue={updateInterestRateDetails.score_range_id}
                onChange={(e) => onUpdateInterestRateDetail('score_range_id', e.target.value)}
            >
                <option value="">Select</option>
                {MFSScoreList && MFSScoreList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(updateErr.score_range_id) ? <FormHelperText>{updateErr.score_range_id}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="interest_rate_id">Interest Rate<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="interest_rate_id"
                id="interest_rate_id"
                placeholder=""
                defaultValue={updateInterestRateDetails.interest_rate_id}
                onChange={(e) => onUpdateInterestRateDetail('interest_rate_id', e.target.value)}
            >
                <option value="">Select</option>
                {interestRateMList && interestRateMList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(updateErr.interest_rate_id) ? <FormHelperText>{updateErr.interest_rate_id}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="loan_amount_id">Loan Amount<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="loan_amount_id"
                id="loan_amount_id"
                placeholder=""
                defaultValue={updateInterestRateDetails.loan_amount_id}
                onChange={(e) => onUpdateInterestRateDetail('loan_amount_id', e.target.value)}
            >
                <option value="">Select</option>
                {loanAmountList && loanAmountList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(updateErr.loan_amount_id) ? <FormHelperText>{updateErr.loan_amount_id}</FormHelperText> : ''}
        	</FormGroup>

        </div>
        <div className="col-md-6">

	        <FormGroup>
            <Label for="payment_term_month_id">Payment Term Month<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="payment_term_month_id"
                id="payment_term_month_id"
                placeholder=""
                defaultValue={updateInterestRateDetails.payment_term_month_id}
                onChange={(e) => onUpdateInterestRateDetail('payment_term_month_id', e.target.value)}
            >
                <option value="">Select</option>
                {paymentTermMonthList && paymentTermMonthList.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(updateErr.payment_term_month_id) ? <FormHelperText>{updateErr.payment_term_month_id}</FormHelperText> : ''}
        	</FormGroup>

        </div>

        <div className="col-md-6">
	        <FormGroup>
			<Label for="effective_date">Effective Date<span className="required-field">*</span></Label>
			
                <DatePicker
                dateFormat="MM/dd/yyyy"
                name="effective_date"
                id="effective_date"
                selected={startDate}
                placeholderText="MM/DD/YYYY"
                autocomplete={false}
                onChange={(e) => onUpdateInterestRateDetail('effective_date', e)}
                />
                {(updateErr.effective_date) ? <FormHelperText>{updateErr.effective_date}</FormHelperText> : ''}
            
		</FormGroup>
        </div>

        <div className="col-md-6">
	        <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(updateInterestRateDetails.status == 1) ? true : false}
                            onChange={(e) => onUpdateInterestRateDetail('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(updateInterestRateDetails.status == 0) ? true : false}
                            onChange={(e) => onUpdateInterestRateDetail('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>     
        </div>
        </div>
    </Form>
);

export default UpdateUserForm;
