/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewInterestRateForm from './AddNewUserForm';

// update user form
import UpdateInterestRateForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isObjectEmpty, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import {
   interestRateMasterDataValueList, interestRateList, updateInterestRate, insertInterestRate
} from 'Actions';

import AddNewButton from './AddNewButton';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
class InterestRate extends Component {

   state = {
      currentModule: 8,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      InterestRate: null, // initial user data
      selectedInterestRate: null, // selected user to perform operations
      loading: false, // loading activity
      addNewInterestRateModal: false, // add new user form modal
      addNewInterestRateDetail: {
         score_range: '',
         interest_rate: '',
         effective_date: '',
         status: 1,
         checked: false
      },
      openViewInterestRateDialog: false, // view user dialog box
      editInterestRate: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         score_range_id: '',
         interest_rate: '',
         effective_date: ''
      },
      add_err: {
      },
      //startDate: new Date()
      startDate: '',
      ruleExist: false
   }

   validateAddSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.score_range === '' &&
         this.state.add_err.interest_rate === '' &&
         this.state.add_err.effective_date === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.score_range_id === '' &&
         this.state.err.interest_rate === '' &&
         this.state.err.effective_date === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateInterestRateDetails(name, value)
      });
   }

   componentDidMount() {

      this.permissionFilter(this.state.currentModule);
      this.props.interestRateList();
      this.props.interestRateMasterDataValueList();

   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   /**
    * Open Add New Master Value Modal
    */
   opnaddNewInterestRateModal() {
      this.setState({ addNewInterestRateModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedInterestRate = 0;
      let InterestRate = this.state.InterestRate.map(userData => {
         if (userData.checked) {
            selectedInterestRate++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedInterestRate++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ InterestRate, selectedInterestRate });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewInterestRateDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'score_range':
            if (isEmpty(value)) {
               add_err[key] = "Select Score Range field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'interest_rate':
            if (isEmpty(value)) {
               add_err[key] = "Interest Rate field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'loan_amount':
            if (isEmpty(value)) {
               add_err[key] = "Loan amount field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'payment_term_month':
            if (isEmpty(value)) {
               add_err[key] = "Payment term month field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'effective_date':
            if (value == '') {
               add_err[key] = "Select Effective Date";
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            break;


         default:
            break;
      }

      this.setState({ add_err: add_err });
      this.setState({
         addNewInterestRateDetail: {
            ...this.state.addNewInterestRateDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewInterestRate() {
      const { score_range, interest_rate, loan_amount, payment_term_month, effective_date, status } = this.state.addNewInterestRateDetail;
      //console.log(editInterestRate.id)
      
      
      //const filterType = this.props.interest_rate.filter(x => x.score_range_id == score_range && x.interest_rate_id == interest_rate && x.loan_amount_id == loan_amount && x.payment_term_month_id == payment_term_month && x.effective_date == effective_date);
      const filterType = this.props.interest_rate.filter(
         x => x.score_range_id == score_range &&
         x.loan_amount_id == loan_amount && 
         x.payment_term_month_id == payment_term_month && 
         x.effective_date == effective_date);
      
      
      //console.log(this.state.addNewInterestRateDetail)
      //return false;
      if (isObjectEmpty(filterType)) {
         this.setState({ ruleExist: true })
         return false;
      }

      this.props.insertInterestRate(this.state.addNewInterestRateDetail);


      this.setState({ addNewInterestRateModal: false, loading: true, startDate: '' });
      let self = this;
      let user = {
         score_range: '',
         interest_rate: '',
         effective_date: '',
         status: 1,
         checked: false
      }
      setTimeout(() => {
         self.setState({ loading: false, addNewInterestRateDetail: user });
      }, 2000);

   }

   /**
    * View Master Value Detail Hanlder
    */
   viewInterestRateDetail(data) {
      var chngFrmt = data.effective_date;
      var date_explode = chngFrmt.split('-');
      var new_effective_date = date_explode[1] + '/' + date_explode[2] + '/' + date_explode[0];
      data.new_effective_date = new_effective_date;
      this.setState({ openViewInterestRateDialog: true, selectedInterestRate: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewInterestRateModalClose() {
      this.setState({ openViewInterestRateDialog: false, selectedInterestRate: null })
   }

   /**
    * On Edit Master Value
    */
   onEditInterestRate(editRfactor) {
      //console.log(editRfactor)
      this.setState({ addNewInterestRateModal: true, editInterestRate: editRfactor, startDate: new Date(editRfactor.effective_date) });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateInterestRateModalClose = () => {
      let upR = {
         score_range_id: '',
         interest_rate: '',
         effective_date: ''
      }
      let addR = {}
      this.setState({ addNewInterestRateModal: false, editInterestRate: null, err: upR, add_err: addR, startDate: '', ruleExist: false })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateInterestRateDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'score_range_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Range Score Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'interest_rate_id':
            if (isEmpty(value)) {
               err[fieldName] = "Interest Rate field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'loan_amount_id':
            if (isEmpty(value)) {
               err[fieldName] = "Loan amount field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'payment_term_month_id':
            if (isEmpty(value)) {
               err[fieldName] = "Payment Term Month field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'effective_date':
            if (value == null) {
               err[fieldName] = "Select Effective Date";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editInterestRate: {
            ...this.state.editInterestRate,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateInterestRate() {
      const { editInterestRate } = this.state;
      
      
      const filterType_rule = this.props.interest_rate.filter(
         x => x.score_range_id == editInterestRate.score_range_id && 
         x.loan_amount_id == editInterestRate.loan_amount_id && 
         x.payment_term_month_id == editInterestRate.payment_term_month_id && 
         x.effective_date == editInterestRate.effective_date && 
         x.id != editInterestRate.id);

      //console.log(this.props.interest_rate)
      //onsole.log(filterType)
      //console.log(this.state.editInterestRate)
      //return false;
      //if (!isObjectEmpty(filterType_rule)) {
         if (filterType_rule.length>0) {
         this.setState({ ruleExist: true })
         return false;
      }
      //const filterType = this.props.MFSScore.filter(x => x.mdv_id == editInterestRate.score_range_id);
      const scroeType = this.props.MFSScoreList.filter(x => x.mdv_id == editInterestRate.score_range_id);
      const rateType = this.props.interestRateMList.filter(x => x.mdv_id == editInterestRate.interest_rate_id);
      const loanType = this.props.loanAmountList.filter(x => x.mdv_id == editInterestRate.loan_amount_id);
      const monthType = this.props.paymentTermMonthList.filter(x => x.mdv_id == editInterestRate.payment_term_month_id);
      //console.log(editRiskFactor)
      //console.log(filterType)
      editInterestRate.score_range = scroeType[0].value;
      editInterestRate.score_range_id = scroeType[0].mdv_id;
      editInterestRate.rate_range = rateType[0].value;
      editInterestRate.interest_rate_id = rateType[0].mdv_id;
      editInterestRate.loan_range = loanType[0].value;
      editInterestRate.loan_amount_id = loanType[0].mdv_id;
      editInterestRate.term_range = monthType[0].value;
      editInterestRate.payment_term_month_id = monthType[0].mdv_id;
      
      let indexOfUpdateInterestRate = '';
      let InterestRate = this.props.interest_rate;
      //console.log(InterestRate)
      for (let i = 0; i < InterestRate.length; i++) {
         const user = InterestRate[i];

         if (user.id === editInterestRate.id) {
            indexOfUpdateInterestRate = i
         }
      }
      //console.log(editRiskFactor)
      this.props.updateInterestRate(editInterestRate);


      InterestRate[indexOfUpdateInterestRate] = editInterestRate;
      this.setState({ loading: true, editInterestRate: null, addNewInterestRateModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ InterestRate, loading: false });
         NotificationManager.success('Interest Rate Updated!');
      }, 2000);
   }


   render() {
      const { add_err, err, InterestRate, loading, editInterestRate, allSelected, selectedInterestRate } = this.state;
      const MFSScore = this.props.MFSScore
      const interestRate = this.props.interest_rate;



      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Interest Rate',
            field: 'rate_range',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.rate_range + '%'
                  );
               }
            }
         },
         {
            name: 'MFS Score',
            field: 'score_range',
         },
         {
            name: 'Loan Amount',
            field: 'loan_range',
         },
         {
            name: 'Payment Term Month',
            field: 'term_range',
         },
         {
            name: 'Effective Date',
            field: 'effective_date',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  var chngFrmt = value.effective_date;
                  var date_explode = chngFrmt.split('-');
                  var newDate = date_explode[1] + '/' + date_explode[2] + '/' + date_explode[0];
                  return (
                     newDate
                  );
               }
            }
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: true,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  //console.log(value)
                  return (
                     <div className="list-action">

                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewInterestRateDetail(value)}><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditInterestRate(value)}><i className="ti-pencil"></i></a> : ''}

                     </div>
                  );
               }
            }
         }

      ];

      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               (this.state.currentPermision.add) ? <AddNewButton opnaddNewInterestRateModal={this.opnaddNewInterestRateModal.bind(this)} /> : ''
            );
         }
      };
      return (
         <div className="others interest-rate">
            <Helmet>
               <title>Health Partner | Others | Interest Rate</title>
               <meta name="description" content="Interest Rate" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.interestRate" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable

                  data={interestRate}
                  columns={columns}
                  options={options}


               />
            </RctCollapsibleCard>



            <Modal isOpen={this.state.addNewInterestRateModal} toggle={() => this.onAddUpdateInterestRateModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateInterestRateModalClose()}>
                  {editInterestRate === null ?
                     'Add Interest Rate' : 'Update Interest Rate'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist &&
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editInterestRate === null ?
                     <AddNewInterestRateForm
                        MFSScoreList={this.props.MFSScoreList}
                        interestRateMList={this.props.interestRateMList}
                        loanAmountList={this.props.loanAmountList}
                        paymentTermMonthList={this.props.paymentTermMonthList}
                        addErr={add_err}
                        addNewInterestRateDetails={this.state.addNewInterestRateDetail}
                        onChangeaddNewInterestRateDetail={this.onChangeaddNewInterestRateDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                     : <UpdateInterestRateForm
                        MFSScoreList={this.props.MFSScoreList}
                        interestRateMList={this.props.interestRateMList}
                        loanAmountList={this.props.loanAmountList}
                        paymentTermMonthList={this.props.paymentTermMonthList}
                        updateErr={err}
                        updateInterestRateDetails={editInterestRate}
                        onUpdateInterestRateDetail={this.onUpdateInterestRateDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate} />
                  }
               </ModalBody>
               <ModalFooter>
                  {editInterestRate === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewInterestRate()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.updateInterestRate()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateInterestRateModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewInterestRateDialog} toggle={() => this.onViewInterestRateModalClose()}>
               <ModalHeader toggle={() => this.onViewInterestRateModalClose()}>
                  {selectedInterestRate !== null ? 'Interest Rate View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedInterestRate !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">MFS Score:</span> <span className="second-colmn ">{selectedInterestRate.score_range}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Interest Rate:</span> <span className="second-colmn">{selectedInterestRate.rate_range + '%'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Effective Date:</span> <span className="second-colmn">{selectedInterestRate.new_effective_date}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedInterestRate.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>

         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ interestRate, authUser }) => {
   const { interest_rate, loading, MFSScoreList, interestRateMList, loanAmountList, paymentTermMonthList } = interestRate;
   const user = authUser.user;
   return { interest_rate, loading, user, MFSScoreList, interestRateMList, loanAmountList, paymentTermMonthList }

}

export default connect(mapStateToProps, {
   interestRateMasterDataValueList, interestRateList, updateInterestRate, insertInterestRate
})(InterestRate);