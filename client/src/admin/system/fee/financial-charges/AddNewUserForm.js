/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ masterFinancialCharges, addErr, addNewFinancialChargesDetails, onChangeaddNewFinancialChargesDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="financial_charges_type">Financial Charge Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="financial_charges_type"
                id="financial_charges_type"
                placeholder="Financial Charges"
                onChange={(e) => onChangeaddNewFinancialChargesDetail('financial_charges_type', e.target.value)}
            >
                <option value="">Select</option>
                {masterFinancialCharges && masterFinancialCharges.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(addErr.financial_charges_type) ? <FormHelperText>{addErr.financial_charges_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="charge">Charge<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="charge"
                id="charge"
                fullWidth
                variant="outlined"
                placeholder="Rate"
                value={addNewFinancialChargesDetails.charge}
                error={(addErr.charge)?true:false}
                helperText={addErr.charge}
                onChange={(e) => onChangeaddNewFinancialChargesDetail('charge', e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewFinancialChargesDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewFinancialChargesDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewFinancialChargesDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewFinancialChargesDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
