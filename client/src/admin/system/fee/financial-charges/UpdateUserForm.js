/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ masterFinancialCharges, updateErr, updateFinancialChargesDetails, onUpdateFinancialChargesDetail }) => (
    <Form>
    <div className="row">

        
        <div className="col-md-12">
        <FormGroup>
            <Label for="financial_charges_type_id">Financial Charge Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="financial_charges_type_id"
                id="financial_charges_type_id"
                placeholder="Financial Charges Type"
                defaultValue={updateFinancialChargesDetails.financial_charges_type_id}
                onChange={(e) => onUpdateFinancialChargesDetail('financial_charges_type_id', e.target.value)}
            >
                <option value="">Select</option>
                {masterFinancialCharges && masterFinancialCharges.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(updateErr.financial_charges_type_id) ? <FormHelperText>{updateErr.financial_charges_type_id}</FormHelperText> : ''}
        </FormGroup>
        </div>
 

      <div className="col-md-12">
        <FormGroup>
            <Label for="charge">Charge<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="charge"
                id="charge"
                fullWidth
                variant="outlined"
                placeholder="Charge"
                value={updateFinancialChargesDetails.charge}
                error={(updateErr.charge)?true:false}
                helperText={updateErr.charge}
                onChange={(e) => onUpdateFinancialChargesDetail('charge', e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(updateFinancialChargesDetails.status == 1) ? true : false}
                        onChange={(e) => onUpdateFinancialChargesDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(updateFinancialChargesDetails.status == 0) ? true : false}
                        onChange={(e) => onUpdateFinancialChargesDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>   
        </div>
    </div>
    </Form>
);

export default UpdateUserForm;
