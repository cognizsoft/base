/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewFinancialChargesForm from './AddNewUserForm';

// update user form
import UpdateFinancialChargesForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isObjectEmpty, isEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import {
   FinancialChargesMasterDataValueList, FinancialChargesList, updateFinancialCharges, insertFinancialCharges
} from 'Actions'; 

import AddNewButton from './AddNewButton';

class FinancialCharges extends Component {

   state = {
      currentModule: 10,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      FinancialCharges: null, // initial user data
      selectedFinancialCharges: null, // selected user to perform operations
      loading: false, // loading activity
      addNewFinancialChargesModal: false, // add new user form modal
      addNewFinancialChargesDetail: {
         financial_charges_type: '',
         charge: '',
         status: 1,
         checked: false 
      },
      openViewFinancialChargesDialog: false, // view user dialog box
      editFinancialCharges: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         financial_charges_type_id:'',
         charge:''
      },
      add_err: {
         
      },
      ruleExist:false,
      
   }

   validateAddSubmit() {
      return (
         this.state.add_err.financial_charges_type === '' &&
         this.state.add_err.charge === ''
      );
   }

   validateUpdateSubmit() {
      return (
         this.state.err.financial_charges_type_id === '' &&
         this.state.err.charge === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateFinancialChargesDetails(name, value)
      });
   }

   componentDidMount() {
    //console.log('rerer')
      this.permissionFilter(this.state.currentModule);
      this.props.FinancialChargesList();
      this.props.FinancialChargesMasterDataValueList();
      
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewFinancialChargesModal() {
      this.setState({ addNewFinancialChargesModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   

   /**
    * On Change Add New User Details
    */
   onChangeaddNewFinancialChargesDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'financial_charges_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Financial Charges Type field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'charge':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Charge field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewFinancialChargesDetail: {
            ...this.state.addNewFinancialChargesDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewFinancialCharges() {
      const { financial_charges_type, charge, status } = this.state.addNewFinancialChargesDetail;
     
      const filterType = this.props.financial_charges.filter(x => x.financial_charges_type_id ==  financial_charges_type && x.charge == charge);
          //console.log(this.props.financial_charges)
            //console.log(filterType)
            //console.log(this.state.addNewFinancialChargesDetail)
      if (isObjectEmpty(filterType)) {                  
         this.setState({ruleExist : true})
         return false;
      }  
           
      this.props.insertFinancialCharges(this.state.addNewFinancialChargesDetail);

    
       this.setState({ addNewFinancialChargesModal: false, loading: true });
       let self = this;
       let user = {
          financial_charges_type: '',
          charge: '',
          status: 1,
          checked:false
       }
       setTimeout(() => {
          self.setState({ loading: false, addNewFinancialChargesDetail:user});
       }, 2000);
      
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewFinancialChargesDetail(data) {
      this.setState({ openViewFinancialChargesDialog: true, selectedFinancialCharges: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewFinancialChargesModalClose() {
      this.setState({ openViewFinancialChargesDialog: false, selectedFinancialCharges: null })
   }

   /**
    * On Edit Master Value
    */
   onEditFinancialCharges(editFinancialCharges) {
   	  //console.log(editFinancialCharges)
      this.setState({ addNewFinancialChargesModal: true, editFinancialCharges: editFinancialCharges });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateFinancialChargesModalClose = () => {
      let upR = {
      	 financial_charges_type_id:'',
         charge:''}
      let addR = {}
      this.setState({ addNewFinancialChargesModal: false, editFinancialCharges: null, err: upR, add_err: addR, ruleExist: false })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateFinancialChargesDetails(fieldName, value) {

      let { err } = this.state;
      console.log(err)
      switch (fieldName) {
         case 'financial_charges_type_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Financial Charges Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'charge':
            if (isEmpty(value)) {
               err[fieldName] = "Charge field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editFinancialCharges: {
            ...this.state.editFinancialCharges,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateFinancialCharges() {
      const { editFinancialCharges } = this.state;
      //console.log(editFinancialCharges)

      const filterType_rule = this.props.financial_charges.filter(x => x.financial_charges_type ==  editFinancialCharges.financial_charges_type && x.charge == editFinancialCharges.charge && x.id != editFinancialCharges.id);
          console.log(this.props.financial_charges)
            //console.log(filterType)
            //console.log(this.state.addNewFinancialChargesDetail)
      if (isObjectEmpty(filterType_rule)) {                  
         this.setState({ruleExist : true})
         return false;
      }  

      const filterType = this.props.financial_charges_master_data_value.filter(x => x.mdv_id == editFinancialCharges.financial_charges_type_id);
      //console.log(editRiskFactor)
      //console.log(filterType)
      editFinancialCharges.financial_charges_type = filterType[0].value;
      editFinancialCharges.financial_charges_type_id = filterType[0].mdv_id;

      let indexOfUpdateFinancialCharges = '';
      let FinancialCharges = this.props.financial_charges;
      //console.log(FinancialCharges)
      for (let i = 0; i < FinancialCharges.length; i++) {
         const user = FinancialCharges[i];
         
         if (user.id === editFinancialCharges.id) {
            indexOfUpdateFinancialCharges = i
         }
      }
      //console.log(editRiskFactor)
      this.props.updateFinancialCharges(editFinancialCharges);


      FinancialCharges[indexOfUpdateFinancialCharges] = editFinancialCharges;
      this.setState({ loading: true, editFinancialCharges: null, addNewFinancialChargesModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ FinancialCharges, loading: false });
         NotificationManager.success('Financial Charges Updated!');
      }, 2000);
   }

   

   render() {
      const { add_err, err, FinancialCharges, loading, editFinancialCharges, allSelected, selectedFinancialCharges } = this.state;
      const master_financial_charges = this.props.financial_charges_master_data_value
      const FinancialChargesProp = this.props.financial_charges;

      const columns = [
         
         {
            name: 'ID', 
            field: 'id'
         },
         {   
            name: 'Financial Charges', 
            field: 'financial_charges_type',
         },
         {
            name: 'Charge', 
            field: 'charge',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.charge+'%'
               );
             }
           }
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               //console.log(value)
               return (
                 <div className="list-action">
                  {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewFinancialChargesDetail(value)}><i className="ti-eye"></i></a>:''}
                  {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditFinancialCharges(value)}><i className="ti-pencil"></i></a>:''}
                  {(this.state.currentPermision.delete)?<a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a>:''}
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
             (this.state.currentPermision.add)?<AddNewButton opnaddNewFinancialChargesModal={this.opnaddNewFinancialChargesModal.bind(this)}/>:''
           );
         }
      };
      return (
         <div className="others payback">
            <Helmet>
               <title>Health Partner | Others | Financial Charges</title>
               <meta name="description" content="Financial Charges" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.financialCharges" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   
                   data={FinancialChargesProp}
                   columns={columns}
                   options={options}
                   

               />
            </RctCollapsibleCard>
            
            
            
            <Modal isOpen={this.state.addNewFinancialChargesModal} toggle={() => this.onAddUpdateFinancialChargesModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateFinancialChargesModalClose()}>
                  {editFinancialCharges === null ?
                     'Add Financial Charges' : 'Update Financial Charges'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editFinancialCharges === null ?
                     <AddNewFinancialChargesForm
                        masterFinancialCharges={master_financial_charges}
                        addErr={add_err}
                        addNewFinancialChargesDetails={this.state.addNewFinancialChargesDetail}
                        onChangeaddNewFinancialChargesDetail={this.onChangeaddNewFinancialChargesDetails.bind(this)}
                     />
                     : <UpdateFinancialChargesForm
                           masterFinancialCharges={master_financial_charges} 
                           updateErr={err} 
                           updateFinancialChargesDetails={editFinancialCharges} 
                           onUpdateFinancialChargesDetail={this.onUpdateFinancialChargesDetails.bind(this)}/>
                  }
               </ModalBody>
               <ModalFooter>
                  {editFinancialCharges === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewFinancialCharges()} 
                        disabled={!this.validateAddSubmit()}
                        >Add</Button>
                     : <Button 
                           variant="contained" 
                           color="primary" 
                           className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"} 
                           onClick={() => this.updateFinancialCharges()} 
                           disabled={!this.validateUpdateSubmit()}
                           >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateFinancialChargesModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewFinancialChargesDialog} toggle={() => this.onViewFinancialChargesModalClose()}>
               <ModalHeader toggle={() => this.onViewFinancialChargesModalClose()}>
                  {selectedFinancialCharges !== null ? 'Financial Charges View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedFinancialCharges !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                           <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Financial Charges Type:</span> <span className="second-colmn">{selectedFinancialCharges.financial_charges_type}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Charge:</span> <span className="second-colmn">{selectedFinancialCharges.charge+'%'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedFinancialCharges.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  
               </ModalFooter>
            </Modal> 
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ FinancialCharges, authUser }) => {
   //console.log(FinancialCharges)
   
   const { financial_charges, loading, master_value_insert_id, financial_charges_master_data_value } = FinancialCharges;
   const user = authUser.user;
   return { financial_charges, loading, user, financial_charges_master_data_value }
   
}

export default connect(mapStateToProps, {
   FinancialChargesMasterDataValueList, FinancialChargesList, updateFinancialCharges, insertFinancialCharges
})(FinancialCharges);