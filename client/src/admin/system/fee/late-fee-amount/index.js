/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   getAllUsers
} from 'Actions';

export default class LateFeeAmount extends Component {

   state = {
      currentModule: 10,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewUserModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewUserDetail: {
         id: '',
         name: '',
         avatar: '',
         type: '',
         emailAddress: '',
         status: 'Active',
         lastSeen: '',
         accountType: '',
         badgeClass: 'badge-success',
         dateCreated: 'Just Now',
         checked: false
      },
      openViewUserDialog: false, // view user dialog box
      editUser: null,
      allSelected: false,
      selectedUsers: 0
   }


	
	/**
	 * Open Add New User Modal
	 */
   opnAddNewUserModal() {
      this.setState({ addNewUserModal: true });
   }

	/**
	 * On Reload
	 */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

	/**
	 * On Select User
	 */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedUsers = 0;
      let users = this.state.users.map(userData => {
         if (userData.checked) {
            selectedUsers++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedUsers++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ users, selectedUsers });
   }

	
   /**
	 * View User Detail Hanlder
	 */
   viewUserDetail(data) {
       //console.log(this.state.openViewUserDialog);
      this.setState({ openViewUserDialog: true, selectedUser: data });
     // console.log(this.state.openViewUserDialog);
   }

	/**
	 * On Edit User
	 */
   onEditUser(user) {
      this.setState({ addNewUserModal: true, editUser: user });
   }

	/**
	 * On Add & Update User Modal Close
	 */
   onAddUpdateUserModalClose() {
      // console.log(this.state.addNewUserModal);
      this.setState({ addNewUserModal: false, editUser: null })
      //console.log(this.state.addNewUserModal);
   }

    /**
	 * On View User Modal Close
	 */
   onViewUserModalClose = () => {
       //console.log(this.state.addViewUserModal);
      this.setState({ openViewUserDialog: false, selectedUser: null })
      //console.log(this.state.addViewUserModal);
   }
   
   
	

   render() {
      const { users, loading, selectedUser, editUser, allSelected, selectedUsers } = this.state;
      
      const data = [
         { late_fee_type: "One Week", amount: "$100", effective_date: "10/1/2018", status: "Active"},
         { late_fee_type: "Two Week", amount: "$460", effective_date: "6/1/2018", status: "Inactive"},
      ];
      const options = {
         filterType: 'dropdown',
      };
      return (
         <div className="fee late-fee-amount">
            <Helmet>
               <title>Health Partner | System | Fee | Late Fee Amount</title>
               <meta name="description" content="Late Fee Amount" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.lateFeeAmount" />}
               match={this.props.match}
            />
            
            <RctCollapsibleCard fullBlock>
               <div className="table-responsive">
                  <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                     <div>
                        <a href="javascript:void(0)" onClick={() => this.onReload()} className="btn-outline-default mr-10"><i className="ti-reload"></i></a>
                     </div>
                     <div>
                        <a href="javascript:void(0)" onClick={() => this.opnAddNewUserModal()} color="primary" className="caret btn-sm mr-10">Add Late Fee Amount <i className="zmdi zmdi-plus"></i></a>
                     </div>
                  </div>
                  <table className="table table-middle table-hover mb-0">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Late Fee Type</th>
                           <th>Late Fee Amount</th>
                           <th>Effective Date</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        {data && data.map((user, key) => (
                           <tr key={key}>
                              <td>{key +1}</td>
                              <td>
                                 <div className="media">
                                    <div className="media-body">
                                       <h5 className="mb-5 fw-bold">{user.late_fee_type}</h5>
                                    </div>
                                 </div>
                              </td>
                              <td><span className="">{user.amount}</span></td>
                              <td><span className="">{user.effective_date}</span></td>
                              <td className="d-flex justify-content-start">
                               <div className="status">
                                    <span className="d-block">{user.status}</span>
                                 </div>
                              </td>
                              <td className="list-action">
                                 <a href="javascript:void(0)" onClick={() => this.viewUserDetail(user)}><i className="ti-eye"></i></a>
                                 <a href="javascript:void(0)" onClick={() => this.onEditUser(user)}><i className="ti-pencil"></i></a>
                              </td>
                           </tr>
                        ))}
                     </tbody>
                     <tfoot className="border-top">
                        <tr>
                           <td colSpan="100%">
                              <Pagination className="mb-0 py-10 px-10">
                                 <PaginationItem>
                                    <PaginationLink previous href="#" />
                                 </PaginationItem>
                                 <PaginationItem active>
                                    <PaginationLink href="javascript:void(0)">1</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink href="javascript:void(0)">2</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink href="javascript:void(0)">3</PaginationLink>
                                 </PaginationItem>
                                 <PaginationItem>
                                    <PaginationLink next href="javascript:void(0)" />
                                 </PaginationItem>
                              </Pagination>
                           </td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               {loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            
            <Modal isOpen={this.state.addNewUserModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  {editUser === null ?
                     'Add Late Fee Amount' : 'Update Late Fee Amount'
                  }
               </ModalHeader>
               <ModalBody>
                  {editUser === null ?
                     <AddNewUserForm
                        addNewUserDetails={this.state.addNewUserDetail}
                        onChangeAddNewUserDetails=""
                     />
                     : <UpdateUserForm user={editUser} onUpdateUserDetail=""/>
                  }
               </ModalBody>
               <ModalFooter>
                  {editUser === null ?
                     <Button variant="contained" className="text-white btn-success" onClick={() => this.addNewUser()}>Add</Button>
                     : <Button variant="contained" color="primary" className="text-white" onClick={() => this.updateUser()}>Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewUserDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'Late Fee Amount View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Late Fee Type:</span> <span className="second-colmn ">{selectedUser.late_fee_type}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Late Fee Amount:</span> <span className="second-colmn">{selectedUser.amount}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Effective Date:</span> <span className="second-colmn">{selectedUser.effective_date}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedUser.status}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
              
            </Modal> 
            
         </div>
      );
   }
}
