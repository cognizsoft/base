/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ user, onUpdateUserDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="editable">Late Fee Type</Label>
            <Input
                type="select"
                name="editable"
                id="editable"
                placeholder=""
                onChange={(e) => onChangeAddNewUserDetails('editable', e.target.value)}
            >
                <option value="">Select</option>
                <option value="1">One Week</option>
                <option value="0">Two Week</option>
                
            </Input>
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="name">Late Fee Amount</Label>
            <TextField
                type="text"
                name="value"
                id="user_role"
                placeholder="Amount"
                fullWidth
                variant="outlined"
            />
            
        </FormGroup>
        </div>
        

        <div className="col-md-12">
        <FormGroup>
            <Label for="exampleDate">Effective Date</Label>
            <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
			<Label>Status</Label>
			<FormGroup check>
				<Label check>
					<Input type="radio" name="radio1"  />{' '}
					Active
                </Label>
			</FormGroup>
			<FormGroup check>
				<Label check>
					<Input type="radio" name="radio1" />{' '}
					Inactive
                </Label>
			</FormGroup>
		</FormGroup>    
        </div>
        </div>
    </Form>
);

export default UpdateUserForm;
