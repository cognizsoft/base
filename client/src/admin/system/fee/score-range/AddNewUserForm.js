/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ addErr, addNewScoreRangeDetails, onChangeaddNewScoreRangeDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="amount">Amount<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="amount"
                id="amount"
                fullWidth
                variant="outlined"
                placeholder="Amount"
                value={addNewScoreRangeDetails.amount}
                error={(addErr.amount)?true:false}
                helperText={addErr.amount}
                onChange={(e) => onChangeaddNewScoreRangeDetail('amount', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="min_volume">Min Score<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="min_score"
                id="min_score"
                fullWidth
                variant="outlined"
                placeholder="Min Score"
                value={addNewScoreRangeDetails.min_volume}
                error={(addErr.min_score)?true:false}
                helperText={addErr.min_score}
                onChange={(e) => onChangeaddNewScoreRangeDetail('min_score', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="max_score">Max score<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="max_score"
                id="max_score"
                fullWidth
                variant="outlined"
                placeholder="Max score"
                value={addNewScoreRangeDetails.max_score}
                error={(addErr.max_score)?true:false}
                helperText={addErr.max_score}
                onChange={(e) => onChangeaddNewScoreRangeDetail('max_score', e.target.value)}
            />
            
        </FormGroup>
        </div>

        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewScoreRangeDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewScoreRangeDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewScoreRangeDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewScoreRangeDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
