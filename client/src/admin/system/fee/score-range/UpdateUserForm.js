/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ updateErr, updateScoreRangeDetails, onUpdateScoreRangeDetail }) => (
    <Form>
    <div className="row">

        
        <div className="col-md-12">
        <FormGroup>
            <Label for="amount">Min Score<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="amount"
                id="amount"
                fullWidth
                variant="outlined"
                placeholder="Min Score"
                value={updateScoreRangeDetails.amount}
                error={(updateErr.amount)?true:false}
                helperText={updateErr.amount}
                onChange={(e) => onUpdateScoreRangeDetail('amount', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="min_score">Min Score<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="min_score"
                id="min_score"
                fullWidth
                variant="outlined"
                placeholder="Min Score"
                value={updateScoreRangeDetails.min_score}
                error={(updateErr.min_score)?true:false}
                helperText={updateErr.min_score}
                onChange={(e) => onUpdateScoreRangeDetail('min_score', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="max_score">Max Score<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="max_score"
                id="max_score"
                fullWidth
                variant="outlined"
                placeholder="Max Score"
                value={updateScoreRangeDetails.max_score}
                error={(updateErr.max_score)?true:false}
                helperText={updateErr.max_score}
                onChange={(e) => onUpdateScoreRangeDetail('max_score', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(updateScoreRangeDetails.status == 1) ? true : false}
                        onChange={(e) => onUpdateScoreRangeDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(updateScoreRangeDetails.status == 0) ? true : false}
                        onChange={(e) => onUpdateScoreRangeDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>   
        </div>
    </div>
    </Form>
);

export default UpdateUserForm;
