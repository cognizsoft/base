/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewScoreRangeForm from './AddNewUserForm';

// update user form
import UpdateScoreRangeForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import {
  ScoreRangeList, updateScoreRange, insertScoreRange
} from 'Actions'; 

import AddNewButton from './AddNewButton';

class ScoreRange extends Component {

   state = {
      currentModule: 22,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      ScoreRange: null, // initial user data
      selectedScoreRange: null, // selected user to perform operations
      loading: false, // loading activity
      addNewScoreRangeModal: false, // add new user form modal
      addNewScoreRangeDetail: {
         amount: '',
         min_score: '',
         max_score: '',
         status: 1,
         checked: false 
      },
      openViewScoreRangeDialog: false, // view user dialog box
      editScoreRange: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         amount:'',
         min_score:'',
         max_score:'',
      },
      add_err: {
         
      },
      ruleExist:false,
   }

   validateAddSubmit() {
      return (
         this.state.add_err.amount === '' &&
         this.state.add_err.min_score === '' &&
         this.state.add_err.max_score === ''
      );
   }

   validateUpdateSubmit() {
      return (
         this.state.err.amount === '' &&
         this.state.err.min_score === '' &&
         this.state.err.max_score === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateScoreRangeDetails(name, value)
      });
   }

   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.ScoreRangeList();
      //this.props.PaybackMasterDataValueList();
      if(this.props.user) {
         //console.log(this.props.user)
      }
      
   }

   permissionFilter = (name) => {

      
      let per = JSON.parse(this.props.user);
      //console.log(per)
      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewScoreRangeModal() {
      this.setState({ addNewScoreRangeModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedScoreRange = 0;
      let ScoreRange = this.state.ScoreRange.map(userData => {
         if (userData.checked) {
            selectedScoreRange++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedScoreRange++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ ScoreRange, selectedScoreRange });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewScoreRangeDetails(key, value) {
      
      let { add_err } = this.state;
      switch (key) {
         case 'amount':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Amount field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'min_score':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Min Score field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'max_score':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Max Score field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewScoreRangeDetail: {
            ...this.state.addNewScoreRangeDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewScoreRange() {
        const { amount, min_score, max_score, status } = this.state.addNewScoreRangeDetail;
     
        const filterType = this.props.scorerange.filter(x => ((min_score >= x.min_score && min_score <= x.max_score) || ( max_score >= x.min_score && max_score <= x.max_score )));
         
          if (isObjectEmpty(filterType)) {                  
             this.setState({ruleExist : true})
             return false;
          } 
          
         //console.log(this.state.addNewScoreRangeDetail)
         //return false; 
         this.props.insertScoreRange(this.state.addNewScoreRangeDetail);

      
         this.setState({ addNewScoreRangeModal: false, loading: true });
         let self = this;
         let user = {
            amount: '',
            min_score: '',
            max_score: '',
            status: '',
            checked:false
         }
         setTimeout(() => {
            self.setState({ loading: false, addNewScoreRangeDetail:user});
         }, 2000);
      
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewScoreRangeDetail(data) {
      this.setState({ openViewScoreRangeDialog: true, selectedScoreRange: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewScoreRangeModalClose() {
      this.setState({ openViewScoreRangeDialog: false, selectedScoreRange: null })
   }

   /**
    * On Edit Master Value
    */
   onEditScoreRange(editScoreRange) {
   	  //console.log(editScoreRange)
      this.setState({ addNewScoreRangeModal: true, editScoreRange: editScoreRange });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateScoreRangeModalClose = () => {
      let upR = {
      	 amount:'',
         min_score:'',
         max_score:'',
       }
      let addR = {}
      this.setState({ addNewScoreRangeModal: false, editScoreRange: null, err: upR, add_err: addR, ruleExist: false, addNewScoreRangeDetail:upR })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateScoreRangeDetails(fieldName, value) {

      let { err } = this.state;
      
      switch (fieldName) {
         case 'amount':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Amount field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'min_score':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Min Score field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'max_score':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Max Score field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      

      this.setState({
         editScoreRange: {
            ...this.state.editScoreRange,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateScoreRange() {
      const { editScoreRange } = this.state;
      //console.log(editScoreRange)
      //console.log(this.props.scorerange)
      const filterType_rule = this.props.scorerange.filter(x => ((editScoreRange.min_score >= x.min_score && editScoreRange.min_score <= x.max_score) || ( editScoreRange.max_score >= x.min_score && editScoreRange.max_score <= x.max_score )) && x.id !== editScoreRange.id);
        //console.log(filterType_rule)
           if (filterType_rule.length>0) {                  
              this.setState({ruleExist : true})
              return false;
           } 

        //return false;

      //const filterType = this.props.payback_master_data_value.filter(x => x.mdv_id == editPayback.payback_id);
      
      //editScoreRange.payback_type = filterType[0].value;
      //editScoreRange.payback_id = filterType[0].mdv_id;

      let indexOfUpdateScoreRange = '';
      let ScoreRange = this.props.scorerange;
      
      for (let i = 0; i < ScoreRange.length; i++) {
         const user = ScoreRange[i];
         
         if (user.id === editScoreRange.id) {
            indexOfUpdateScoreRange = i
         }
      }
      //return false;
      this.props.updateScoreRange(editScoreRange);


      ScoreRange[indexOfUpdateScoreRange] = editScoreRange;
      this.setState({ loading: true, ruleExist: false, editScoreRange: null, addNewScoreRangeModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ ScoreRange, loading: false });
         NotificationManager.success('Score Range Updated!');
      }, 2000);
   }

   

   render() {
      const { add_err, err, ScoreRange, loading, editScoreRange, allSelected, selectedScoreRange } = this.state;
      //const master_payback = this.props.payback_master_data_value
      //console.log(this.state.ruleExist)
      const ScoreRangeProp = this.props.scorerange;

      const columns = [
         
         {
            name: 'ID', 
            field: 'id'
         },
         {   
            name: 'Amount', 
            field: 'amount',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  '$'+parseFloat(value.amount).toFixed(2)
               );
             }
            }
         },
         {
            name: 'Score Range', 
            field: 'min_score',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.min_score+' - '+value.max_score
               );
             }
           }
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               
               return (
                 <div className="list-action">
                  {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewScoreRangeDetail(value)}><i className="ti-eye"></i></a> : ''}
                  {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditScoreRange(value)}><i className="ti-pencil"></i></a> : ''}
                  {/*(this.state.currentPermision.delete) ? <a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a> : ''*/}
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
            (this.state.currentPermision.view) ? <AddNewButton opnaddNewScoreRangeModal={this.opnaddNewScoreRangeModal.bind(this)}/> : ''
           );
         }
         
      };
      return (
         <div className="others scorernage">
            <Helmet>
               <title>Health Partner | Others | Score Range</title>
               <meta name="description" content="Score Range" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.scoreRange" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   
                   data={ScoreRangeProp}
                   columns={columns}
                   options={options}
                   

               />
            </RctCollapsibleCard>
            
            
            
            <Modal isOpen={this.state.addNewScoreRangeModal} toggle={() => this.onAddUpdateScoreRangeModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateScoreRangeModalClose()}>
                  {editScoreRange === null ?
                     'Add Line of Credit' : 'Update Line of Credit'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editScoreRange === null ?
                     <AddNewScoreRangeForm
                        addErr={add_err}
                        addNewScoreRangeDetails={this.state.addNewScoreRangeDetail}
                        onChangeaddNewScoreRangeDetail={this.onChangeaddNewScoreRangeDetails.bind(this)}
                     />
                     : <UpdateScoreRangeForm
                           updateErr={err} 
                           updateScoreRangeDetails={editScoreRange} 
                           onUpdateScoreRangeDetail={this.onUpdateScoreRangeDetails.bind(this)}/>
                  }
               </ModalBody>
               <ModalFooter>
                  {editScoreRange === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewScoreRange()} 
                        disabled={!this.validateAddSubmit()}
                        >Add</Button>
                     : <Button 
                           variant="contained" 
                           color="primary" 
                           className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"} 
                           onClick={() => this.updateScoreRange()} 
                           disabled={!this.validateUpdateSubmit()}
                           >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateScoreRangeModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewScoreRangeDialog} toggle={() => this.onViewScoreRangeModalClose()}>
               <ModalHeader toggle={() => this.onViewScoreRangeModalClose()}>
                  {selectedScoreRange !== null ? 'Line of Credit' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedScoreRange !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Amount:</span> <span className="second-colmn">{'$'+parseFloat(selectedScoreRange.amount).toFixed(2)}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Score Range:</span> <span className="second-colmn">{selectedScoreRange.min_score+' - '+selectedScoreRange.max_score}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedScoreRange.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
            </Modal> 
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ ScoreRangeReducer, authUser }) => {
   //console.log(ScoreRangeReducer);
   
   const { scorerange, loading } = ScoreRangeReducer;
   const user = authUser.user;
   return { user, scorerange, loading }
   
}

export default connect(mapStateToProps, {
   ScoreRangeList, updateScoreRange, insertScoreRange
})(ScoreRange);