/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ masterPayback, addErr, addNewPaybackDetails, onChangeaddNewPaybackDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="payback_type">Payback Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="payback_type"
                id="payback_type"
                placeholder="Payback Type"
                onChange={(e) => onChangeaddNewPaybackDetail('payback_type', e.target.value)}
            >
                <option value="">Select</option>
                {masterPayback && masterPayback.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(addErr.payback_type) ? <FormHelperText>{addErr.payback_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="min_volume">Min Volume<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="min_volume"
                id="min_volume"
                fullWidth
                variant="outlined"
                placeholder="Min Volume"
                value={addNewPaybackDetails.min_volume}
                error={(addErr.min_volume)?true:false}
                helperText={addErr.min_volume}
                onChange={(e) => onChangeaddNewPaybackDetail('min_volume', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="max_volume">Max Volume<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="max_volume"
                id="max_volume"
                fullWidth
                variant="outlined"
                placeholder="Max Volume"
                value={addNewPaybackDetails.max_volume}
                error={(addErr.max_volume)?true:false}
                helperText={addErr.max_volume}
                onChange={(e) => onChangeaddNewPaybackDetail('max_volume', e.target.value)}
            />
            
        </FormGroup>
        </div>


        <div className="col-md-12">
        <FormGroup>
            <Label for="rate">Rate<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="rate"
                id="rate"
                fullWidth
                variant="outlined"
                placeholder="Rate"
                value={addNewPaybackDetails.rate}
                error={(addErr.rate)?true:false}
                helperText={addErr.rate}
                onChange={(e) => onChangeaddNewPaybackDetail('rate', e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewPaybackDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewPaybackDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewPaybackDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewPaybackDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
