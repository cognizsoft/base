/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ masterPayback, updateErr, updatePaybackDetails, onUpdatePaybackDetail }) => (
    <Form>
    <div className="row">

        
        <div className="col-md-12">
        <FormGroup>
            <Label for="payback_id">Payback Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="payback_id"
                id="payback_id"
                placeholder="Payback Type"
                defaultValue={updatePaybackDetails.payback_id}
                onChange={(e) => onUpdatePaybackDetail('payback_id', e.target.value)}
            >
                <option value="">Select</option>
                {masterPayback && masterPayback.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(updateErr.payback_id) ? <FormHelperText>{updateErr.payback_id}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="min_volume">Min Volume<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="min_volume"
                id="min_volume"
                fullWidth
                variant="outlined"
                placeholder="Min Volume"
                value={updatePaybackDetails.min_volume}
                error={(updateErr.min_volume)?true:false}
                helperText={updateErr.min_volume}
                onChange={(e) => onUpdatePaybackDetail('min_volume', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="max_volume">Max Volume<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="max_volume"
                id="max_volume"
                fullWidth
                variant="outlined"
                placeholder="Max Volume"
                value={updatePaybackDetails.max_volume}
                error={(updateErr.max_volume)?true:false}
                helperText={updateErr.max_volume}
                onChange={(e) => onUpdatePaybackDetail('max_volume', e.target.value)}
            />
            
        </FormGroup>
        </div>

      <div className="col-md-12">
        <FormGroup>
            <Label for="rate">Rate<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="rate"
                id="rate"
                fullWidth
                variant="outlined"
                placeholder="Rate"
                value={updatePaybackDetails.rate}
                error={(updateErr.rate)?true:false}
                helperText={updateErr.rate}
                onChange={(e) => onUpdatePaybackDetail('rate', e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(updatePaybackDetails.status == 1) ? true : false}
                        onChange={(e) => onUpdatePaybackDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(updatePaybackDetails.status == 0) ? true : false}
                        onChange={(e) => onUpdatePaybackDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>   
        </div>
    </div>
    </Form>
);

export default UpdateUserForm;
