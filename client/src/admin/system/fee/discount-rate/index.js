/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewPaybackForm from './AddNewUserForm';

// update user form
import UpdatePaybackForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import {
   PaybackMasterDataValueList, PaybackList, updatePayback, insertPayback
} from 'Actions'; 

import AddNewButton from './AddNewButton';

class Payback extends Component {

   state = {
      currentModule: 7,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      Payback: null, // initial user data
      selectedPayback: null, // selected user to perform operations
      loading: false, // loading activity
      addNewPaybackModal: false, // add new user form modal
      addNewPaybackDetail: {
         payback_type: '',
         min_volume: '',
         max_volume: '',
         rate: '',
         status: 1,
         checked: false 
      },
      openViewPaybackDialog: false, // view user dialog box
      editPayback: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         payback_id:'',
         min_volume:'',
         max_volume:'',
         rate:''
      },
      add_err: {
         
      },
      ruleExist:false,
   }

   validateAddSubmit() {
      return (
         this.state.add_err.payback_type === '' &&
         this.state.add_err.min_volume === '' &&
         this.state.add_err.max_volume === '' &&
         this.state.add_err.rate === ''
      );
   }

   validateUpdateSubmit() {
      return (
         this.state.err.payback_id === '' &&
         this.state.err.min_volume === '' &&
         this.state.err.max_volume === '' &&
         this.state.err.rate === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdatePaybackDetails(name, value)
      });
   }

   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.PaybackList();
      this.props.PaybackMasterDataValueList();
      
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewPaybackModal() {
      this.setState({ addNewPaybackModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedPayback = 0;
      let Payback = this.state.Payback.map(userData => {
         if (userData.checked) {
            selectedPayback++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedPayback++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ Payback, selectedPayback });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewPaybackDetails(key, value) {
      
      let { add_err } = this.state;
      switch (key) {
         case 'payback_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Payback Type field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'min_volume':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Min Volume field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'max_volume':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Max Volume field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'rate':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Rate field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewPaybackDetail: {
            ...this.state.addNewPaybackDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewPayback() {
      const { payback_type, min_volume, max_volume, rate, status } = this.state.addNewPaybackDetail;
     
        const filterType = this.props.payback.filter(x => x.payback_id ==  payback_type && ((min_volume >= x.min_volume && min_volume <= x.max_volume) || ( max_volume >= x.min_volume && max_volume <= x.max_volume )));
         
          if (isObjectEmpty(filterType)) {                  
             this.setState({ruleExist : true})
             return false;
          } 
           
         this.props.insertPayback(this.state.addNewPaybackDetail);

      
         this.setState({ addNewPaybackModal: false, loading: true });
         let self = this;
         let user = {
            payback_type: '',
            min_volume: '',
            max_volume: '',
            rate: '',
            status: '',
            checked:false
         }
         setTimeout(() => {
            self.setState({ loading: false, addNewPaybackDetail:user});
         }, 2000);
      
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewPaybackDetail(data) {
      this.setState({ openViewPaybackDialog: true, selectedPayback: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewPaybackModalClose() {
      this.setState({ openViewPaybackDialog: false, selectedPayback: null })
   }

   /**
    * On Edit Master Value
    */
   onEditPayback(editPayback) {
   	  
      this.setState({ addNewPaybackModal: true, editPayback: editPayback });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdatePaybackModalClose = () => {
      let upR = {
      	 payback_id:'',
         min_volume:'',
         max_volume:'',
         rate:''}
      let addR = {}
      this.setState({ addNewPaybackModal: false, editPayback: null, err: upR, add_err: addR, ruleExist: false, addNewPaybackDetail:upR })
   }

   /**
    * On Update Master Value Details
    */
   onUpdatePaybackDetails(fieldName, value) {

      let { err } = this.state;
      
      switch (fieldName) {
         case 'payback_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Payback Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'min_volume':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Min Volume field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'max_volume':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Max Volume field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'rate':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Rate field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      

      this.setState({
         editPayback: {
            ...this.state.editPayback,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updatePayback() {
      const { editPayback } = this.state;
      const filterType_rule = this.props.payback.filter(x => 
         x.payback_id ==  editPayback.payback_id && 
         ((editPayback.min_volume >= x.min_volume || editPayback.min_volume <= x.max_volume) 
         || ( editPayback.max_volume >= x.min_volume || editPayback.max_volume <= x.max_volume )) && 
         x.rate == editPayback.rate && 
         x.id != editPayback.id);
      
           if (filterType_rule.length>0) {                  
              this.setState({ruleExist : true})
              return false;
           } 

        //return false;

      const filterType = this.props.payback_master_data_value.filter(x => x.mdv_id == editPayback.payback_id);
      
      editPayback.payback_type = filterType[0].value;
      editPayback.payback_id = filterType[0].mdv_id;

      let indexOfUpdatePayback = '';
      let Payback = this.props.payback;
      
      for (let i = 0; i < Payback.length; i++) {
         const user = Payback[i];
         
         if (user.id === editPayback.id) {
            indexOfUpdatePayback = i
         }
      }
      
      this.props.updatePayback(editPayback);


      Payback[indexOfUpdatePayback] = editPayback;
      this.setState({ loading: true, editPayback: null, addNewPaybackModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ Payback, loading: false });
         NotificationManager.success('Payback Updated!');
      }, 2000);
   }

   

   render() {
      const { add_err, err, Payback, loading, editPayback, allSelected, selectedPayback } = this.state;
      const master_payback = this.props.payback_master_data_value
      const PaybackProp = this.props.payback;

      const columns = [
         
         {
            name: 'ID', 
            field: 'id'
         },
         {   
            name: 'Payback Type', 
            field: 'payback_type',
         },
         {
            name: 'Min Volume', 
            field: 'min_volume',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  '$'+value.min_volume
               );
             }
           }
         },
         {
            name: 'Max Volume', 
            field: 'max_volume',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  '$'+value.max_volume
               );
             }
           }
         },
         {
            name: 'Rate', 
            field: 'rate',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.rate+'%'
               );
             }
           }
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               
               return (
                 <div className="list-action">
                  {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewPaybackDetail(value)}><i className="ti-eye"></i></a>:''}
                  {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditPayback(value)}><i className="ti-pencil"></i></a>:''}
                  {(this.state.currentPermision.delete)?<a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a>:''}
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
             (this.state.currentPermision.add)?<AddNewButton opnaddNewPaybackModal={this.opnaddNewPaybackModal.bind(this)}/>:''
           );
         }
         
      };
      return (
         <div className="others payback">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.discountRate" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   
                   data={PaybackProp}
                   columns={columns}
                   options={options}
                   

               />
            </RctCollapsibleCard>
            
            
            
            <Modal isOpen={this.state.addNewPaybackModal} toggle={() => this.onAddUpdatePaybackModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdatePaybackModalClose()}>
                  {editPayback === null ?
                     'Add Payback' : 'Update Payback'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editPayback === null ?
                     <AddNewPaybackForm
                        masterPayback={master_payback}
                        addErr={add_err}
                        addNewPaybackDetails={this.state.addNewPaybackDetail}
                        onChangeaddNewPaybackDetail={this.onChangeaddNewPaybackDetails.bind(this)}
                     />
                     : <UpdatePaybackForm
                           masterPayback={master_payback}
                           updateErr={err} 
                           updatePaybackDetails={editPayback} 
                           onUpdatePaybackDetail={this.onUpdatePaybackDetails.bind(this)}/>
                  }
               </ModalBody>
               <ModalFooter>
                  {editPayback === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewPayback()} 
                        disabled={!this.validateAddSubmit()}
                        >Add</Button>
                     : <Button 
                           variant="contained" 
                           color="primary" 
                           className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"} 
                           onClick={() => this.updatePayback()} 
                           disabled={!this.validateUpdateSubmit()}
                           >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdatePaybackModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewPaybackDialog} toggle={() => this.onViewPaybackModalClose()}>
               <ModalHeader toggle={() => this.onViewPaybackModalClose()}>
                  {selectedPayback !== null ? 'Payback View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedPayback !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Payback Type:</span> <span className="second-colmn">{selectedPayback.payback_type}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Min Volume:</span> <span className="second-colmn">{'$'+selectedPayback.min_volume}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Max Volume:</span> <span className="second-colmn">{'$'+selectedPayback.max_volume}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Rate:</span> <span className="second-colmn">{selectedPayback.rate+'%'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedPayback.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  
               </ModalFooter>
            </Modal> 
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ Payback, authUser }) => {
   
   
   const { payback, loading, master_value_insert_id, payback_master_data_value } = Payback;
   const user = authUser.user;
   return { payback, loading, user, payback_master_data_value }
   
}

export default connect(mapStateToProps, {
   PaybackMasterDataValueList, PaybackList, updatePayback, insertPayback
})(Payback);