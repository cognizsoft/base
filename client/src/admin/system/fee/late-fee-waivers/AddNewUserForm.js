/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ masterLateFeeWaiver, masterLateFeeWaiverReason, addErr, addNewLateFeeWaiverDetails, onChangeaddNewLateFeeWaiverDetail, DatePicker, startDate }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="waiver_type">Waiver Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="waiver_type"
                id="waiver_type"
                placeholder=""
                onChange={(e) => onChangeaddNewLateFeeWaiverDetail('waiver_type', e.target.value)}
            >
                <option value="">Select</option>
                {masterLateFeeWaiver && masterLateFeeWaiver.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.md_id+' - '+opt.value+'%'}</option>
                ))}
                
            </Input>
            {(addErr.waiver_type) ? <FormHelperText>{addErr.waiver_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="reason_type">Reason Description<span className="required-field">*</span></Label>
            <Input
                type="textarea"
                name="reason_type"
                id="reason_type"
                placeholder="Reason Description"
                value={addNewLateFeeWaiverDetails.reason_type}
                
                onChange={(e) => onChangeaddNewLateFeeWaiverDetail('reason_type', e.target.value)}
            />
            {(addErr.reason_type) ? <FormHelperText>{addErr.reason_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        
        <div className="col-md-12">
            <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(addNewLateFeeWaiverDetails.status == 1) ? true : false}
                            onChange={(e) => onChangeaddNewLateFeeWaiverDetail('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(addNewLateFeeWaiverDetails.status == 0) ? true : false}
                            onChange={(e) => onChangeaddNewLateFeeWaiverDetail('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>     
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
