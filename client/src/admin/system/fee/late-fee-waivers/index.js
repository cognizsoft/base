/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewLateFeeWaiverForm from './AddNewUserForm';

// update user form
import UpdateLateFeeWaiverForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
   lateFeeWaiverMasterDataValueList, lateFeeWaiverReasonMasterDataValueList, lateFeeWaiverList, updateLateFeeWaiver, insertLateFeeWaiver
} from 'Actions'; 

import AddNewButton from './AddNewButton';

class LateFeeWaiver extends Component {

   state = {
      currentModule: 9,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      LateFeeWaiver: null, // initial user data
      selectedLateFeeWaiver: null, // selected user to perform operations
      loading: false, // loading activity
      addNewLateFeeWaiverModal: false, // add new user form modal
      addNewLateFeeWaiverDetail: {
         waiver_type: '',
         reason_type: '',
         max_waiver: '',
         effective_date: '',
         status: 1,
         checked: false 
      },
      openViewLateFeeWaiverDialog: false, // view user dialog box
      editLateFeeWaiver: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         waiver_type_id:'',
         reason_type_id:'',
         max_waiver:'',
         effective_date:''
      },
      add_err: {
         
      },
      ruleExist:false,
      startDate: ''
      
   }

   validateAddSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.waiver_type === '' &&
         this.state.add_err.reason_type === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.waiver_type_id === '' &&
         this.state.err.reason_type_id === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateLateFeeWaiverDetails(name, value)
      });
   }

   componentDidMount() {
      
      this.permissionFilter(this.state.currentModule);
      this.props.lateFeeWaiverList();
      this.props.lateFeeWaiverMasterDataValueList();
      this.props.lateFeeWaiverReasonMasterDataValueList();
      
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewLateFeeWaiverModal() {

      this.setState({ addNewLateFeeWaiverModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }


   /**
    * On Change Add New User Details
    */
   onChangeaddNewLateFeeWaiverDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'waiver_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Waiver Type field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'reason_type':
            if (isEmpty(value)) {
               add_err[key] = "Reason description field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewLateFeeWaiverDetail: {
            ...this.state.addNewLateFeeWaiverDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewLateFeeWaiver() {

      const { waiver_type, reason_type, status } = this.state.addNewLateFeeWaiverDetail;
      
         const filterType = this.props.late_fee_waiver.filter(x => x.waiver_type_id ==  waiver_type);
            //console.log(this.props.late_fee_waiver)
            //console.log(filterType)
            //console.log(this.state.addNewLateFeeWaiverDetail)
            //return false;

          /*if (isObjectEmpty(filterType)) {                  
             this.setState({ruleExist : true})
             return false;
          }*/

        
           
         this.props.insertLateFeeWaiver(this.state.addNewLateFeeWaiverDetail);

      
         this.setState({ addNewLateFeeWaiverModal: false, loading: true, startDate: '' });
         let self = this;
         let user = {
            waiver_type: '',
            reason_type: '',
            status: '',
            checked:false
         }
         setTimeout(() => {
            self.setState({ loading: false, addNewLateFeeWaiverDetail:user});
         }, 2000);
      
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewLateFeeWaiverDetail(data) {
      /*var chngFrmt = data.effective_date;
      var date_explode = chngFrmt.split('-');
      var new_effective_date = date_explode[1]+'/'+date_explode[2]+'/'+date_explode[0];
      data.new_effective_date = new_effective_date;*/
      this.setState({ openViewLateFeeWaiverDialog: true, selectedLateFeeWaiver: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewLateFeeWaiverModalClose() {
      this.setState({ openViewLateFeeWaiverDialog: false, selectedLateFeeWaiver: null })
   }

   /**
    * On Edit Master Value
    */
   onEditLateFeeWaiver(edit) {
      
      this.setState({ addNewLateFeeWaiverModal: true, editLateFeeWaiver: edit, startDate: new Date(edit.effective_date) });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateLateFeeWaiverModalClose = () => {
      let upR = {waiver_type_id: '', reason_type_id:'', max_waiver:'', effective_date:''}
      let addR = {}
      this.setState({ addNewLateFeeWaiverModal: false, editLateFeeWaiver: null, err: upR, add_err: addR, ruleExist: false, startDate: '' })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateLateFeeWaiverDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'waiver_type_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Waiver Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'reason_type_id':
            if (isEmpty(value)) {
               err[fieldName] = "Reason description field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editLateFeeWaiver: {
            ...this.state.editLateFeeWaiver,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateLateFeeWaiver() {
      const { editLateFeeWaiver } = this.state;
      //console.log(editLateFeeWaiver)
      //console.log(this.props.late_fee_waiver)

      const filterType_rule = this.props.late_fee_waiver.filter(x => x.waiver_type ==  editLateFeeWaiver.waiver_type && x.id != editLateFeeWaiver.id);
            
      /*if (isObjectEmpty(filterType_rule)) {                  
         this.setState({ruleExist : true})
         return false;
      }*/ 

      //console.log(editRiskFactor)
      const filterType = this.props.late_fee_waiver_master_data_value.filter(x => x.mdv_id == editLateFeeWaiver.waiver_type_id);

      //const filterType1 = this.props.late_fee_reason_master_data_value.filter(x => x.mdv_id == editLateFeeWaiver.reason_type_id);

      editLateFeeWaiver.waiver_type = filterType[0].md_id;
      editLateFeeWaiver.waiver_value = filterType[0].value;
      editLateFeeWaiver.waiver_type_id = filterType[0].mdv_id;

      //editLateFeeWaiver.reason_type = filterType1[0].value;
      //editLateFeeWaiver.reason_type_id = filterType1[0].mdv_id;

      let indexOfUpdateLateFeeWaiver = '';
      let LateFeeWaiver = this.props.late_fee_waiver;
      //console.log(LateFeeWaiver)
      for (let i = 0; i < LateFeeWaiver.length; i++) {
         const user = LateFeeWaiver[i];
         
         if (user.id === editLateFeeWaiver.id) {
            indexOfUpdateLateFeeWaiver = i
         }
      }
      //console.log(editRiskFactor)
      this.props.updateLateFeeWaiver(editLateFeeWaiver);


      LateFeeWaiver[indexOfUpdateLateFeeWaiver] = editLateFeeWaiver;
      this.setState({ loading: true, editLateFeeWaiver: null, addNewLateFeeWaiverModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ LateFeeWaiver, loading: false });
      }, 2000);
   }

  
   

   render() {
      const { add_err, err, LateFeeWaiver, loading, editLateFeeWaiver, allSelected, selectedLateFeeWaiver } = this.state;

      const master_late_fee_waiver = this.props.late_fee_waiver_master_data_value
      const master_late_fee_reason = this.props.late_fee_reason_master_data_value
      const lateFeeWaiver = this.props.late_fee_waiver;
      //console.log(lateFeeWaiver)
      const columns = [
         
         {
            name: 'ID', 
            field: 'id'
         },
         {   
            name: 'Waiver Type', 
            field: 'waiver_type',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  (value.waiver_value) ? value.waiver_type+' - '+value.waiver_value+'%' : value.waiver_type
               );
             }
           }
         },
         {   
            name: 'Reason', 
            field: 'reason_type_id',
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               //console.log(value)
               return (
                 <div className="list-action">
                  {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewLateFeeWaiverDetail(value)}><i className="ti-eye"></i></a>:''}
                  {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditLateFeeWaiver(value)}><i className="ti-pencil"></i></a>:''}
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
            (this.state.currentPermision.add)?<AddNewButton opnaddNewLateFeeWaiverModal={this.opnaddNewLateFeeWaiverModal.bind(this)}/>:''
           );
         }
      };
      return (
         <div className="others late-fee-waiver">
            <Helmet>
               <title>Health Partner | System | Fee | Late Fee Waivers</title>
               <meta name="description" content="Late Fee Waivers" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.lateFee" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   
                   data={lateFeeWaiver}
                   columns={columns}
                   options={options}
                   

               />
            </RctCollapsibleCard>
            
            
            
            <Modal isOpen={this.state.addNewLateFeeWaiverModal} toggle={() => this.onAddUpdateLateFeeWaiverModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateLateFeeWaiverModalClose()}>
                  {editLateFeeWaiver === null ?
                     'Add Late Fee Waiver' : 'Update Late Fee Waiver'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editLateFeeWaiver === null ?
                     <AddNewLateFeeWaiverForm
                        masterLateFeeWaiver={master_late_fee_waiver}
                        masterLateFeeWaiverReason={master_late_fee_reason}
                        addErr={add_err}
                        addNewLateFeeWaiverDetails={this.state.addNewLateFeeWaiverDetail}
                        onChangeaddNewLateFeeWaiverDetail={this.onChangeaddNewLateFeeWaiverDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                     : <UpdateLateFeeWaiverForm
                           masterLateFeeWaiver={master_late_fee_waiver}
                           masterLateFeeWaiverReason={master_late_fee_reason} 
                           updateErr={err} 
                           updateLateFeeWaiverDetails={editLateFeeWaiver} 
                           onUpdateLateFeeWaiverDetail={this.onUpdateLateFeeWaiverDetails.bind(this)}
                           DatePicker={DatePicker}
                           startDate={this.state.startDate}
                           />
                  }
               </ModalBody>
               <ModalFooter>
                  {editLateFeeWaiver === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewLateFeeWaiver()} 
                        disabled={!this.validateAddSubmit()}
                        >Add</Button>
                     : <Button 
                           variant="contained" 
                           color="primary" 
                           className="text-white" 
                           onClick={() => this.updateLateFeeWaiver()} 
                           disabled={!this.validateUpdateSubmit()}
                           >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateLateFeeWaiverModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewLateFeeWaiverDialog} toggle={() => this.onViewLateFeeWaiverModalClose()}>
               <ModalHeader toggle={() => this.onViewLateFeeWaiverModalClose()}>
                  {selectedLateFeeWaiver !== null ? 'Late Fee Waiver View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedLateFeeWaiver !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Waiver Type:</span> <span className="second-colmn">{selectedLateFeeWaiver.waiver_type}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Reason Description:</span> <span className="second-colmn">{selectedLateFeeWaiver.reason_type_id}</span></div>
                                 
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedLateFeeWaiver.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               
            </Modal> 
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ lateFeeWaiver, authUser }) => {
   //console.log(lateFeeWaiver)
   
   const { late_fee_waiver, loading, late_fee_waiver_master_data_value, late_fee_reason_master_data_value } = lateFeeWaiver;
   const user = authUser.user;
   return { late_fee_waiver, loading, user, late_fee_waiver_master_data_value, late_fee_reason_master_data_value }
   
}

export default connect(mapStateToProps, {
   lateFeeWaiverMasterDataValueList, lateFeeWaiverReasonMasterDataValueList, lateFeeWaiverList, updateLateFeeWaiver, insertLateFeeWaiver
})(LateFeeWaiver);