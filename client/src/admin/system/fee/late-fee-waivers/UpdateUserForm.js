/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ masterLateFeeWaiver, masterLateFeeWaiverReason, updateErr, updateLateFeeWaiverDetails, onUpdateLateFeeWaiverDetail, DatePicker, startDate }) => (
    <Form>
    	<div className="row">
        	<div className="col-md-12">
                <FormGroup>
                    <Label for="waiver_type_id">Waiver Type<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="waiver_type_id"
                        id="waiver_type_id"
                        placeholder=""
                        defaultValue={updateLateFeeWaiverDetails.waiver_type_id}
                        onChange={(e) => onUpdateLateFeeWaiverDetail('waiver_type_id', e.target.value)}
                    >
                        <option value="">Select</option>
                        {masterLateFeeWaiver && masterLateFeeWaiver.map((opt, key) => (
                            <option value={opt.mdv_id} key={key}>{opt.md_id+' - '+opt.value+'%'}</option>
                        ))}
                        
                    </Input>
                    {(updateErr.waiver_type_id) ? <FormHelperText>{updateErr.waiver_type_id}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="reason_type_id">Reason Description<span className="required-field">*</span></Label>
                    <Input
                        type="textarea"
                        name="reason_type_id"
                        id="reason_type_id"
                        placeholder="Reason Description"
                        value={updateLateFeeWaiverDetails.reason_type_id}
                        
                        onChange={(e) => onUpdateLateFeeWaiverDetail('reason_type_id', e.target.value)}
                    />
                    {(updateErr.reason_type_id) ? <FormHelperText>{updateErr.reason_type_id}</FormHelperText> : ''}
                </FormGroup>
            </div>

            
        
      
            <div className="col-md-12">
                <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(updateLateFeeWaiverDetails.status == 1) ? true : false}
                            onChange={(e) => onUpdateLateFeeWaiverDetail('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(updateLateFeeWaiverDetails.status == 0) ? true : false}
                            onChange={(e) => onUpdateLateFeeWaiverDetail('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>   
            </div>


    	</div>

    </Form>
);

export default UpdateUserForm;
