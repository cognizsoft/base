/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const AddNewSurchareTypeForm = ({ addErr, addNewSurchargeTypeDetails, onChangeAddNewSurchargeTypeDetails, DatePicker,startDate }) => (
    <Form>
        <div className="row">
        
            <div className="col-md-12">
            <FormGroup>
                <Label for="surcharge_type">Surcharge Type Name<span className="required-field">*</span></Label>
                <TextField
                    type="text"
                    name="surcharge_type"
                    id="surcharge_type"
                    fullWidth
                    variant="outlined"
                    placeholder="Surcharge Type Name"
                    value={addNewSurchargeTypeDetails.surcharge_type}
                    error={(addErr.surcharge_type) ? true : false}
                    helperText={addErr.surcharge_type}
                    onChange={(e) => onChangeAddNewSurchargeTypeDetails('surcharge_type', e.target.value)}
                    
                />
            </FormGroup>
            </div>

            <div className="col-md-12">
            <FormGroup>
                <Label for="surcharge_amount">Surcharge Amount<span className="required-field">*</span></Label>
                <TextField
                    type="text"
                    name="surcharge_amount"
                    id="surcharge_amount"
                    fullWidth
                    variant="outlined"
                    placeholder="Eg. 300, 4000"
                    value={addNewSurchargeTypeDetails.surcharge_amount}
                    error={(addErr.surcharge_amount) ? true : false}
                    helperText={addErr.surcharge_amount}
                    onChange={(e) => onChangeAddNewSurchargeTypeDetails('surcharge_amount', e.target.value)}
                />
                
            </FormGroup>
            </div>
            

            <div className="col-md-12">
            <FormGroup>
                <Label for="date">Effective Date<span className="required-field">*</span></Label>
                <DatePicker
                dateFormat="dd/MM/yyyy"
                name="date"
                id="date"
                selected={startDate}
                placeholderText="DD/MM/YYYY"
                autocomplete={false}
                onChange={(e) => onChangeAddNewSurchargeTypeDetails('date', e)}
                />
                
                {(addErr.date) ? <FormHelperText>{addErr.date}</FormHelperText> : ''}
            </FormGroup>
            </div>
            
            <div className="col-md-12">
            <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(addNewSurchargeTypeDetails.status == 1) ? true : false}
                            onChange={(e) => onChangeAddNewSurchargeTypeDetails('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(addNewSurchargeTypeDetails.status == 0) ? true : false}
                            onChange={(e) => onChangeAddNewSurchargeTypeDetails('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>    
            </div>

        </div>
    </Form>
);

export default AddNewSurchareTypeForm;
