/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';


// delete confirmation dialog

import { isEmpty, isMaster, isNumeric,isObjectEmpty } from '../../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
   checkSurchargeTypeExist, getAllSurchargeType, insertSurchargeType, updateSurchargeType
} from 'Actions';

class SurchargeType extends Component {

   state = {
      currentModule: 11,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewSurchargeTypeModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewSurchargeTypeDetail: {
         surcharge_type: '',
         surcharge_amount: '',
         date: '',
         status: 1
      },
      openViewSurchargeTypeDialog: false, // view user dialog box
      editSurchargeType: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      update_err: {
         name: '',
         amount: '',
         effective_date: '',
      },
      ruleExist:false,
      startDate: ''
   }

   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllSurchargeType();
   }


   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   // componentWillReceiveProps(nextProps) {
   //    //console.log(nextProps)
   //    let { add_err } = this.state;

   //    let { update_err } = this.state;
   //    (nextProps.surchargeExist && nextProps.isEdit == 0) ? add_err['surcharge_type'] = "Surcharge Type already exists" : "";
   //    (nextProps.surchargeExist && nextProps.isEdit == 1) ? update_err['name'] = "Surcharge Type already exists" : update_err['name'] = '';

   //    this.setState({ add_err: add_err });
   //    this.setState({ update_err: update_err });


   // }

   /*
   * Title :- checkSurchargeTypeExist
   * Descrpation :- This function use to check system module exist or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */

   // checkSurchargeTypeExist(name, id) {
   //    //console.log('fun exist')
   //    //this.props.checkSurchargeTypeExist(name, id);
   //    //console.log(this.state.addNewSurchargeTypeDetail)
   // }


   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
  permissionFilter = (name) => {
   let per = JSON.parse(this.props.user);

   let newUser = per.user_permission.filter(
      function (per) { return per.description == name }
   );

   this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
}

	 /*
   * Title :- opnAddNewSurchargeTypeModal
   * Descrpation :- This function call user for open popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   opnAddNewSurchargeTypeModal() {
      this.setState({ addNewSurchargeTypeModal: true });
   }

    /*
   * Title :- viewRegionDetail
   * Descrpation :- This function use for view pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   viewSurchargeTypeDetail(data) {
      var chngFrmt = data.effective_date;
      var date_explode = chngFrmt.split('-');
      var new_effective_date = date_explode[1]+'/'+date_explode[2]+'/'+date_explode[0];
      data.new_effective_date = new_effective_date;
      this.setState({ openViewSurchargeTypeDialog: true, selectedUser: data });
   }

	 /*
   * Title :- onEditSurchargeType
   * Descrpation :- This function edit pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onEditSurchargeType(surcharge) {
      
     
      this.setState({ addNewSurchargeTypeModal: true, editSurchargeType: surcharge, startDate: new Date(surcharge.effective_date) });
   }

	 /*
   * Title :- onAddUpdateRegionModalClose
   * Descrpation :- This function close pop up
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onAddUpdateSurchargeTypeModalClose() {
      let emptyError = {
      }
      let addNewSurchargeTypeDetail = {
         surcharge_type: '',
         surcharge_amount: '',
         date: '',
         status: 1
      }
      let emptyUpdateError = {
         name: '',
         amount: '',
         effective_date: '',
      }
      this.setState({ addNewSurchargeTypeModal: false, editSurchargeType: null, addNewSurchargeTypeDetail: addNewSurchargeTypeDetail, add_err:emptyError, update_err: emptyUpdateError, startDate: '', ruleExist: false })
   }

    /*
   * Title :- onViewUserModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onViewUserModalClose = () => {
      this.setState({ openViewSurchargeTypeDialog: false, selectedUser: null })
   }

   /*
   * Title :- onChangeAddNewUserDetails
   * Descrpation :- This function use for check field validation on add new region
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onChangeAddNewSurchargeTypeDetails(key, value) {
      let { add_err } = this.state;
      //console.log(key)
      switch (key) {
         case 'surcharge_type':
            console.log(this.props.surchargelist)

            if (isEmpty(value)) {
               add_err[key] = "Surcharge Type Name can't be blank";
            } else {
               //var surcharge_amount = document.getElememtByID("surcharge_amount")
               // const filterType = this.props.surchargelist.filter(x => x.name == value && x.amount ==  this.state.addNewSurchargeTypeDetail.surcharge_amount && x.effective_date == this.state.addNewSurchargeTypeDetail.date);
               // if (isObjectEmpty(filterType)) {                  
               //    add_err[key] = "Rule already exists.";
               //    add_err['surcharge_amount'] = "";
               //    add_err['date'] = "";
               // } else {

               //    //const filterType = this.state.addNewSurchargeTypeDetail.surcharge_amount;

                  add_err[key] = '';
               //    //add_err['surcharge_amount'] = "";
               //    //add_err['date'] = "";
               // }
              
           }
            break;
         case 'surcharge_amount':
            if (isEmpty(value)) {
               add_err[key] = "Surcharge Amount can't be blank";
            } else if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else {
               // const filterType = this.props.surchargelist.filter(x => x.amount == value && x.name ==  this.state.addNewSurchargeTypeDetail.surcharge_type && x.effective_date == this.state.addNewSurchargeTypeDetail.date);
               // //console.log(filterType)
               // //console.log(this.state.addNewSurchargeTypeDetail)
               // if (isObjectEmpty(filterType)) {                  
               //    add_err[key] = "Rule already exists.";
               //    add_err['surcharge_type'] = "";
               //    add_err['date'] = "";
                  
               // } else {
                 add_err[key] = '';
               //    //add_err['surcharge_type'] = "";
               //    //add_err['date'] = "";
               // }
            }
            break;
         case 'date':
            if (value == '') {
               add_err[key] = "Select Effective Date";
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';  
            }
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewSurchargeTypeDetail: {
            ...this.state.addNewSurchargeTypeDetail,
            [key]: value
         }
      });
   }
   /*
   * Title :- onUpdateUserDetails
   * Descrpation :- This function use for check filed validation on edit region case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   onUpdateSurchargeTypeDetails(key, value) {
      let { update_err } = this.state;
      //console.log(key +'+'+value )
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               update_err[key] = "Surcharge Type can't be blank";
            } else {
               update_err[key] = '';
            }
            break;
         case 'amount':
            if (isEmpty(value)) {
               update_err[key] = "Surcharge Amount can't be blank";
            } else if (isNumeric(value)) {
               update_err[key] = "Allow only numeric";
            } else {
               update_err[key] = '';
            }
            break;
         case 'effective_date':
         
            if (value == null) {
               update_err[key] = "Select Effective Date";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               update_err[key] = '';
            }
            
         default:
            break;
      }
      this.setState({ update_err: update_err });

      this.setState({
         editSurchargeType: {
            ...this.state.editSurchargeType,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.surcharge_type === '' &&
         this.state.add_err.surcharge_amount === '' &&
         this.state.add_err.date === ''
      );
   }
   /*
   * Title :- addNewRegion
   * Descrpation :- This function use for add new region action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   addNewSurchargeType() {
   const filterType = this.props.surchargelist.filter(x => x.name ==  this.state.addNewSurchargeTypeDetail.surcharge_type && x.effective_date == this.state.addNewSurchargeTypeDetail.date);
            //console.log(this.props.surchargelist)
            //console.log(filterType)
            //console.log(this.state.addNewSurchargeTypeDetail)
      if (isObjectEmpty(filterType)) {                  
         this.setState({ruleExist : true})
         return false;
      } 
            
      this.props.insertSurchargeType(this.state.addNewSurchargeTypeDetail);
      this.setState({ addNewSurchargeTypeModal: false, startDate: '' });
      let self = this;
      let emptyState = {
         surcharge_type: '',
         surcharge_amount: '',
         date: '',
         status: 1
      }
      setTimeout(() => {
         self.setState({ addNewSurchargeTypeDetail: emptyState });
      }, 2000);
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 23,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.update_err.name === '' &&
         this.state.update_err.amount === '' &&
         this.state.update_err.effective_date === ''
      );
   }
   

   updateSurchargeType() {
      const { editSurchargeType } = this.state;
      
      const filterType = this.props.surchargelist.filter(x => x.name ==  editSurchargeType.name && x.effective_date == editSurchargeType.effective_date && x.id != editSurchargeType.id);
            
      if (isObjectEmpty(filterType)) {                  
         this.setState({ruleExist : true})
         return false;
      } 

      //console.log(editSurchargeType)
      let indexOfUpdateSurchargeType = '';
      let SurchargeTypes = this.props.surchargelist;
      //console.log(SurchargeTypes)
      for (let i = 0; i < SurchargeTypes.length; i++) {
         const SurchargeType = SurchargeTypes[i];
         
         if (SurchargeType.id === editSurchargeType.id) {
            indexOfUpdateSurchargeType = i
         }
      }

      this.props.updateSurchargeType(editSurchargeType);


      SurchargeTypes[indexOfUpdateSurchargeType] = editSurchargeType;
      this.setState({ loading: true, editSurchargeType: null, addNewSurchargeTypeModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ SurchargeTypes, loading: false });
      }, 2000);
   }


   render() {
      const { add_err, selectedUser, editSurchargeType, update_err } = this.state;
      const surchargelist = this.props.surchargelist
      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Surcharge Type Name',
            field: 'name',
         },
         {
            name: 'Surcharge Amount',
            field: 'amount',
            options: {
               filter: true,
               sort: true,
               customBodyRender: (value) => {
                  var amnt = parseFloat(value.amount).toFixed(2);
                  return (
                     '$'+amnt
                  );
               }
            }
         },
         {
            name: 'Effective Date',
            field: 'effective_date',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  var chngFrmt = value.effective_date;
                  var date_explode = chngFrmt.split('-');
                  var newDate = date_explode[1]+'/'+date_explode[2]+'/'+date_explode[0];
                  return (
                     newDate
                  );
               }
            }
            
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewSurchargeTypeDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditSurchargeType(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Regions.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewSurchargeTypeModal={this.opnAddNewSurchargeTypeModal.bind(this)} />:''
            );
         },

      };
      return (
         <div className="country regions">
            <Helmet>
               <title>Health Partner | System | Country | Regions</title>
               <meta name="description" content="Regions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.surchargeType" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={surchargelist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>

            <Modal isOpen={this.state.addNewSurchargeTypeModal} toggle={() => this.onAddUpdateSurchargeTypeModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateSurchargeTypeModalClose()}>
                  {editSurchargeType === null ?
                     'Add Surcharge Type' : 'Update Surcharge Type'
                  }
               </ModalHeader>
               <ModalBody>
               
                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }
               
                  
                  {editSurchargeType === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewSurchargeTypeDetails={this.state.addNewSurchargeTypeDetail}
                        onChangeAddNewSurchargeTypeDetails={this.onChangeAddNewSurchargeTypeDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}

                     />
                     : <UpdateUserForm
                        editSurchargeType={editSurchargeType}
                        updateErr={update_err}
                        onUpdateSurchargeTypeDetails={this.onUpdateSurchargeTypeDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editSurchargeType === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewSurchargeType()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateSurchargeType()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateSurchargeTypeModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewSurchargeTypeDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedUser !== null ? 'Surcharge Type View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Surcharge Type Name:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Surcharge Amount:</span> <span className="second-colmn">{'$'+parseFloat(selectedUser.amount).toFixed(2)}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Effective Date:</span> <span className="second-colmn">{selectedUser.new_effective_date}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedUser.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}


const mapStateToProps = ({ authUser, surchargeTypeDetails }) => {
   const { user } = authUser;
   const { loading, surchargelist, surchargeExist, isEdit } = surchargeTypeDetails;
   return { loading, surchargelist, user, surchargeExist, isEdit }
}

export default connect(mapStateToProps, {
   checkSurchargeTypeExist, getAllSurchargeType, insertSurchargeType, updateSurchargeType
})(SurchargeType);