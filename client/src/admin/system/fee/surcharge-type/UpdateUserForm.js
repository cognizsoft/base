/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const UpdateUserForm = ({ editSurchargeType, updateErr, onUpdateSurchargeTypeDetails, DatePicker,startDate }) => (
    <Form>
        <div className="row">
        
            <div className="col-md-12">
            <FormGroup>
                <Label for="surcharge_type">Surcharge Type Name<span className="required-field">*</span></Label>
                <TextField
                    type="text"
                    name="surcharge_type"
                    id="surcharge_type"
                    fullWidth
                    variant="outlined"
                    placeholder="Surcharge Type Name"
                    value={editSurchargeType.name}
                    error={(updateErr.name) ? true : false}
                    helperText={updateErr.name}
                    onChange={(e) => onUpdateSurchargeTypeDetails('name', e.target.value)}
                    
                />
            </FormGroup>
            </div>

            <div className="col-md-12">
            <FormGroup>
                <Label for="surcharge_amount">Surcharge Amount<span className="required-field">*</span></Label>
                <TextField
                    type="text"
                    name="surcharge_amount"
                    id="surcharge_amount"
                    fullWidth
                    variant="outlined"
                    placeholder="Eg. 300, 4000"
                    value={editSurchargeType.amount}
                    error={(updateErr.amount) ? true : false}
                    helperText={updateErr.amount}
                    onChange={(e) => onUpdateSurchargeTypeDetails('amount', e.target.value)}
                />
                
            </FormGroup>
            </div>
            

            <div className="col-md-12">
            <FormGroup>
                <Label for="effective_date">Effective Date<span className="required-field">*</span></Label>
                
                <DatePicker
                name="effective_date"
                id="effective_date"
                selected={startDate}
                placeholderText="MM/DD/YYYY"
                autocomplete={false}
                onChange={(e) => onUpdateSurchargeTypeDetails('effective_date', e)}
                />

                
                {(updateErr.effective_date) ? <FormHelperText>{updateErr.effective_date}</FormHelperText> : ''}
            </FormGroup>
            </div>
            
            <div className="col-md-12">
            <FormGroup tag="fieldset">
                <Label>Status</Label>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={1}
                            checked={(editSurchargeType.status == 1) ? true : false}
                            onChange={(e) => onUpdateSurchargeTypeDetails('status', e.target.value)}
                        />{' '}
                        Active
                    </Label>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input
                            type="radio"
                            name="status"
                            value={0}
                            checked={(editSurchargeType.status == 0) ? true : false}
                            onChange={(e) => onUpdateSurchargeTypeDetails('status', e.target.value)}
                        />{' '}
                        Inactive
                    </Label>
                </FormGroup>
            </FormGroup>    
            </div>

        </div>
    </Form>
);

export default UpdateUserForm;
