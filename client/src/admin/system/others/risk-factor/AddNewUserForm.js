/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';


const AddNewUserForm = ({ masterRiskFactor, addErr, addNewRiskFactorDetails, onChangeaddNewRiskFactorDetail, DatePicker, startDate }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="risk_factor_type">Risk Factors<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="risk_factor_type"
                id="risk_factor_type"
                placeholder=""
                onChange={(e) => onChangeaddNewRiskFactorDetail('risk_factor_type', e.target.value)}
            >
                <option value="">Select</option>
                {masterRiskFactor && masterRiskFactor.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(addErr.risk_factor_type) ? <FormHelperText>{addErr.risk_factor_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="risk_factor_weight">Risk Factor Weight<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="risk_factor_weight"
                id="risk_factor_weight"
                placeholder="Weight"
                fullWidth
                variant="outlined"
                value={addNewRiskFactorDetails.weight}
                error={(addErr.risk_factor_weight)?true:false}
                helperText={addErr.risk_factor_weight}
                onChange={(e) => onChangeaddNewRiskFactorDetail('risk_factor_weight', e.target.value)}
            />
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="risk_factor_weight_type">Weight Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="risk_factor_weight_type"
                id="risk_factor_weight_type"
                placeholder=""
                onChange={(e) => onChangeaddNewRiskFactorDetail('risk_factor_weight_type', e.target.value)}
            >
                <option value="">Select</option>
                <option value="1">+ (Positive)</option>
                <option value="0">- (Negative)</option>
                
            </Input>
            {(addErr.risk_factor_weight_type) ? <FormHelperText>{addErr.risk_factor_weight_type}</FormHelperText> : ''}
        </FormGroup>
        </div>
        

        <div className="col-md-12">
        <FormGroup>
            <Label for="effective_date">Effective Date<span className="required-field">*</span></Label>

            <DatePicker
                dateFormat="dd/MM/yyyy"
                name="effective_date"
                id="effective_date"
                selected={startDate}
                placeholderText="DD/MM/YYYY"
                autocomplete={false}
                onChange={(e) => onChangeaddNewRiskFactorDetail('effective_date', e)}
                />

            
            
                {(addErr.effective_date) ? <FormHelperText>{addErr.effective_date}</FormHelperText> : ''}
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewRiskFactorDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewRiskFactorDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewRiskFactorDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewRiskFactorDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
