/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewRiskFactorForm from './AddNewUserForm';

// update user form
import UpdateRiskFactorForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
   riskFactorMasterDataValueList, riskFactorsList, updateRiskFactors, deleteRiskFactors, insertRiskFactors
} from 'Actions'; 

import AddNewButton from './AddNewButton';

class RiskFactors extends Component {

   state = {
      currentModule: 12,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      RiskFactor: null, // initial user data
      selectedRiskFactor: null, // selected user to perform operations
      loading: false, // loading activity
      addNewRiskFactorModal: false, // add new user form modal
      addNewRiskFactorDetail: {
         risk_factor_type: '',
         risk_factor_weight: '',
         risk_factor_weight_type: '',
         effective_date: '',
         status: 1,
         checked: false 
      },
      openViewRiskFactorDialog: false, // view user dialog box
      editRiskFactor: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         factor_type_id:'',
         weight:'',
         weight_type:'',
         effective_date:''
      },
      add_err: {
         
      },
      ruleExist:false,
      startDate: ''
   }

   validateAddSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.risk_factor_type === '' &&
         this.state.add_err.risk_factor_weight === '' &&
         this.state.add_err.risk_factor_weight_type === '' &&
         this.state.add_err.effective_date === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.factor_type_id === '' &&
         this.state.err.weight === '' &&
         this.state.err.weight_type === '' &&
         this.state.err.effective_date === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateRiskFactorDetails(name, value)
      });
   }

   componentDidMount() {
      
      this.permissionFilter(this.state.currentModule);
      this.props.riskFactorsList();
      this.props.riskFactorMasterDataValueList();
      
   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }

   
   /**
    * Open Add New Master Value Modal
    */
   opnaddNewRiskFactorModal() {
      this.setState({ addNewRiskFactorModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedRiskFactor = 0;
      let RiskFactor = this.state.RiskFactor.map(userData => {
         if (userData.checked) {
            selectedRiskFactor++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedRiskFactor++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ RiskFactor, selectedRiskFactor });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewRiskFactorDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'risk_factor_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Risk Factor Type field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'risk_factor_weight':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "risk factor weight field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'risk_factor_weight_type':
            if (isEmpty(value)) {
               add_err[key] = "select weight type";
            } else {
               add_err[key] = '';
            }
            break;
         case 'effective_date':
            console.log(value)
            if (value == '' || value == null) {
               add_err[key] = "Select Effective Date";
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';  
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewRiskFactorDetail: {
            ...this.state.addNewRiskFactorDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewRiskFactor() {
      const { risk_factor_type, risk_factor_weight, risk_factor_weight_type, effective_date, status } = this.state.addNewRiskFactorDetail;

        const filterType = this.props.risk_factor.filter(x => x.factor_type_id ==  risk_factor_type && x.weight == risk_factor_weight && x.weight_type == risk_factor_weight_type && x.effective_date == effective_date );
         
            //console.log(this.props.risk_factor)
            //console.log(filterType)
            //console.log(this.state.addNewRiskFactorDetail)
            //return false;
          if (isObjectEmpty(filterType)) {                  
             this.setState({ruleExist : true})
             return false;
          } 

     
         let RiskFactor = this.props.risk_factor;
         //console.log('dfdf')
           
         this.props.insertRiskFactors(this.state.addNewRiskFactorDetail);

      
         this.setState({ addNewRiskFactorModal: false, loading: true, startDate: '' });
         let self = this;
         let user = {
            risk_factor_type: '',
            risk_factor_weight: '',
            risk_factor_weight_type: '',
            effective_date: '',
            status: '',
            checked:false
         }
         setTimeout(() => {
            self.setState({ loading: false, addNewRiskFactorDetail:user});
         }, 2000);
      
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewRiskFactorDetail(data) {
      this.setState({ openViewRiskFactorDialog: true, selectedRiskFactor: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewRiskFactorModalClose() {
      this.setState({ openViewRiskFactorDialog: false, selectedRiskFactor: null })
   }

   /**
    * On Edit Master Value
    */
   onEditRiskFactor(editRfactor) {

      this.setState({ addNewRiskFactorModal: true, editRiskFactor: editRfactor, startDate: new Date(editRfactor.effective_date) });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateRiskFactorModalClose = () => {
      let upR = {factor_type_id:'',
         weight:'',
         weight_type:'',
         effective_date:''}
      let addR = {}
      this.setState({ addNewRiskFactorModal: false, editRiskFactor: null, err: upR, add_err: addR, ruleExist: false, startDate: ''  })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateRiskFactorDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'factor_type_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Risk Factor Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'weight':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "risk factor weight field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'weight_type':
            if (isEmpty(value)) {
               err[fieldName] = "select weight type";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'effective_date':
            if (value == null || value == null) {
               err[fieldName] = "Select Effective Date";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editRiskFactor: {
            ...this.state.editRiskFactor,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateRiskFactor() {
      const { editRiskFactor } = this.state;

      const filterType_rule = this.props.risk_factor.filter(x => x.factor_type_id ==  editRiskFactor.factor_type_id && x.weight == editRiskFactor.weight && x.weight_type == editRiskFactor.weight_type && x.effective_date == editRiskFactor.effective_date && x.id != editRiskFactor.id );
         
            //console.log(this.props.risk_factor)
            //console.log(filterType)
            //console.log(editRiskFactor)
            //return false;
          if (isObjectEmpty(filterType_rule)) {                  
             this.setState({ruleExist : true})
             return false;
          } 

      //console.log(editRiskFactor)
      const filterType = this.props.risk_factor_master_data_value.filter(x => x.mdv_id == editRiskFactor.factor_type_id);
      //console.log(editRiskFactor)
      //console.log(filterType)
      editRiskFactor.factor_type = filterType[0].value;
      editRiskFactor.factor_type_id = filterType[0].mdv_id;

      let indexOfUpdateRiskFactor = '';
      let RiskFactor = this.props.risk_factor;
      console.log(RiskFactor)
      for (let i = 0; i < RiskFactor.length; i++) {
         const user = RiskFactor[i];
         
         if (user.id === editRiskFactor.id) {
            indexOfUpdateRiskFactor = i
         }
      }
      //console.log(editRiskFactor)
      this.props.updateRiskFactors(editRiskFactor);


      RiskFactor[indexOfUpdateRiskFactor] = editRiskFactor;
      this.setState({ loading: true, editRiskFactor: null, addNewRiskFactorModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ RiskFactor, loading: false });
         NotificationManager.success('Risk Factor Updated!');
      }, 2000);
   }

   //Select All user
   onSelectAllUser(e) {
      const { selectedMasterValue, MasterVL } = this.state;
      let selectAll = selectedMasterValue < MasterVL.length;
      if (selectAll) {
         let selectAllUsers = MasterVL.map(user => {
            user.checked = true
            return user
         });
         this.setState({ MasterVL: selectAllUsers, selectedMasterValue: selectAllUsers.length })
      } else {
         let unselectedMasterValue = MasterVL.map(user => {
            user.checked = false
            return user;
         });
         this.setState({ selectedMasterValue: 0, MasterVL: unselectedMasterValue });
      }
   }
   
   
   

   render() {
      const { add_err, err, RiskFactor, loading, editRiskFactor, allSelected, selectedRiskFactor } = this.state;
      const master_risk_factor = this.props.risk_factor_master_data_value
      const riskFactor = this.props.risk_factor;

      
      const columns = [
         
         {
            name: 'ID', 
            field: 'id'
         },
         {   
            name: 'Risk Factors', 
            field: 'factor_type',
         },
         {
            name: 'Risk Factor Weight', 
            field: 'weight'
         },
         {
            name: 'Weight Type', 
            field: 'weight_type',
            options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.weight_type == 1 ? '+ (Positive)' : '- (Negative)'
               );
             }
           }
         },
         {
            name: 'Effective Date', 
            field: 'effective_date',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  var chngFrmt = value.effective_date;
                  var date_explode = chngFrmt.split('-');
                  var newDate = date_explode[2]+'/'+date_explode[1]+'/'+date_explode[0];
                  return (
                     newDate
                  );
               }
            }
         },
         {
           name: "Status",
           field: "status",
           options: {
             filter: true,
             sort: true,
             empty: true,
             customBodyRender: (value) => {
               return (
                  value.status == 1 ? 'Active' : 'Inactive'
               );
             }
           }
         },
         {
           name: "Action",
           options: {
             filter: true,
             sort: false,
             empty: true,
             download: false,
             customBodyRender: (value) => {
               //console.log(value)
               return (
                 <div className="list-action">
                  
                  {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewRiskFactorDetail(value)}><i className="ti-eye"></i></a>:''}
                  {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditRiskFactor(value)}><i className="ti-pencil"></i></a>:''}
                  
               </div>
               );
             }
           }
         }
         
      ];
      
      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
           return (
             (this.state.currentPermision.add)?<AddNewButton opnaddNewRiskFactorModal={this.opnaddNewRiskFactorModal.bind(this)}/>:''
           );
         }
      };
      return (
         <div className="others risk-factor">
            <Helmet>
               <title>Health Partner | Others | Risk Factor</title>
               <meta name="description" content="Risk Factor" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.riskFactor" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                   
                   data={riskFactor}
                   columns={columns}
                   options={options}
                   

               />
            </RctCollapsibleCard>
            
            
            
            <Modal isOpen={this.state.addNewRiskFactorModal} toggle={() => this.onAddUpdateRiskFactorModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateRiskFactorModalClose()}>
                  {editRiskFactor === null ?
                     'Add Weighted Risk Factor' : 'Update Weighted Risk Factor'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist && 
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editRiskFactor === null ?
                     <AddNewRiskFactorForm
                        masterRiskFactor={master_risk_factor}
                        addErr={add_err}
                        addNewRiskFactorDetails={this.state.addNewRiskFactorDetail}
                        onChangeaddNewRiskFactorDetail={this.onChangeaddNewRiskFactorDetails.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                     : <UpdateRiskFactorForm
                           masterRiskFactor={master_risk_factor} 
                           updateErr={err} 
                           updateRiskFactorDetails={editRiskFactor} 
                           onUpdateRiskFactorDetail={this.onUpdateRiskFactorDetails.bind(this)}
                           DatePicker={DatePicker}
                           startDate={this.state.startDate}
                           />
                  }
               </ModalBody>
               <ModalFooter>
                  {editRiskFactor === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewRiskFactor()} 
                        disabled={!this.validateAddSubmit()}
                        >Add</Button>
                     : <Button 
                           variant="contained" 
                           color="primary" 
                           className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"} 
                           onClick={() => this.updateRiskFactor()} 
                           disabled={!this.validateUpdateSubmit()}
                           >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateRiskFactorModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>
            
            
            <Modal isOpen={this.state.openViewRiskFactorDialog} toggle={() => this.onViewRiskFactorModalClose()}>
               <ModalHeader toggle={() => this.onViewRiskFactorModalClose()}>
                  {selectedRiskFactor !== null ? 'Weighted Risk Factor View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedRiskFactor !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Risk Factor Type:</span> <span className="second-colmn">{selectedRiskFactor.factor_type}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Risk Factor Weight:</span> <span className="second-colmn">{selectedRiskFactor.weight}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Weight Type:</span> <span className="second-colmn">{selectedRiskFactor.weight_type == 1 ? '+ (Positive)' : '- (Negative)'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Effective Date:</span> <span className="second-colmn">{selectedRiskFactor.effective_date}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedRiskFactor.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  
               </ModalFooter>
            </Modal> 
            
         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ riskFactor, authUser }) => {
   //console.log(riskFactor)
   
   const { risk_factor, loading, master_value_insert_id, risk_factor_master_data_value } = riskFactor;
   const user = authUser.user;
   return { risk_factor, loading, user, risk_factor_master_data_value }
   
}

export default connect(mapStateToProps, {
   riskFactorMasterDataValueList, riskFactorsList, updateRiskFactors, deleteRiskFactors, insertRiskFactors
})(RiskFactors);