/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ masterRiskFactor, updateErr, updateRiskFactorDetails, onUpdateRiskFactorDetail, DatePicker, startDate }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="risk_factor_type">Risk Factors<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="factor_type_id"
                id="factor_type_id"
                placeholder=""
                defaultValue={updateRiskFactorDetails.factor_type_id}
                onChange={(e) => onUpdateRiskFactorDetail('factor_type_id', e.target.value)}
            >
                <option value="">Select</option>
                {masterRiskFactor && masterRiskFactor.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
                
            </Input>
            {(updateErr.factor_type_id) ? <FormHelperText>{updateErr.factor_type_id}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="weight">Risk Factor Weight<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="weight"
                id="weight"
                placeholder="Weight"
                fullWidth
                variant="outlined"
                value={updateRiskFactorDetails.weight}
                error={(updateErr.weight)?true:false}
                helperText={updateErr.weight}
                onChange={(e) => onUpdateRiskFactorDetail('weight', e.target.value)}
            />
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="weight_type">Weight Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="weight_type"
                id="weight_type"
                placeholder=""
                defaultValue={updateRiskFactorDetails.weight_type}
                onChange={(e) => onUpdateRiskFactorDetail('weight_type', e.target.value)}
            >
                <option value="">Select</option>
                <option value="1">+ (Positive)</option>
                <option value="0">- (Negative)</option>
                
            </Input>
            {(updateErr.weight_type) ? <FormHelperText>{updateErr.weight_type}</FormHelperText> : ''}
        </FormGroup>
        </div>
        

        <div className="col-md-12">
        <FormGroup>
            <Label for="effective_date">Effective Date<span className="required-field">*</span></Label>

            <DatePicker
                    name="effective_date"
                    id="effective_date"
                    selected={startDate}
                    placeholderText="MM/DD/YYYY"
                    autocomplete={false}
                    onChange={(e) => onUpdateRiskFactorDetail('effective_date', e)}
                    />

            
                {(updateErr.effective_date) ? <FormHelperText>{updateErr.effective_date}</FormHelperText> : ''}
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(updateRiskFactorDetails.status == 1) ? true : false}
                        onChange={(e) => onUpdateRiskFactorDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(updateRiskFactorDetails.status == 0) ? true : false}
                        onChange={(e) => onUpdateRiskFactorDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default UpdateUserForm;
