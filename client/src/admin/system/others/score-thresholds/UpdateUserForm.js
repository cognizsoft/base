/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
//bUECw7UiPkMD5O%BKTTOnjAu
const UpdateUserForm = ({ updateErr, updateScoreThresholdDetails, onUpdateScoreThresholdDetail, checkScoreThresholdExist, DatePicker, startDate }) => (
    <Form>
        <div className="row">
            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Thresholds Type</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="type"
                                value={1}
                                checked={(updateScoreThresholdDetails.type == 1) ? true : false}
                                onChange={(e) => onUpdateScoreThresholdDetail('type', e.target.value)}
                            />{' '}
                    Auto-Approval
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="type"
                                value={0}
                                checked={(updateScoreThresholdDetails.type == 0) ? true : false}
                                onChange={(e) => onUpdateScoreThresholdDetail('type', e.target.value)}
                            />{' '}
                    Manual Approval
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Thresholds Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Thresholds Name"
                        fullWidth
                        variant="outlined"
                        value={updateScoreThresholdDetails.name}
                        error={(updateErr.name) ? true : false}
                        helperText={updateErr.name}
                        onChange={(e) => onUpdateScoreThresholdDetail('name', e.target.value)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="value">Thresholds Value<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="value"
                        id="value"
                        placeholder="Thresholds Value"
                        fullWidth
                        variant="outlined"
                        value={updateScoreThresholdDetails.value}
                        error={(updateErr.value) ? true : false}
                        helperText={updateErr.value}
                        onChange={(e) => onUpdateScoreThresholdDetail('value', e.target.value)}
                    />
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="Text">Descritpion</Label>
                    <Input
                        type="textarea"
                        name="description"
                        id="description"
                        placeholder="Description"
                        value={updateScoreThresholdDetails.description}
                        onChange={(e) => onUpdateScoreThresholdDetail('description', e.target.value)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="exampleDate">Effective Date<span className="required-field">*</span></Label>
                    <DatePicker
                        name="effective_date"
                        id="effective_date"
                        selected={startDate}
                        placeholderText="DD/MM/YYYY"
                        autocomplete={false}
                        onChange={(e) => onUpdateScoreThresholdDetail('effective_date', e)}
                    />



                    {(updateErr.effective_date) ? <FormHelperText>{updateErr.effective_date}</FormHelperText> : ''}
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(updateScoreThresholdDetails.status == 1) ? true : false}
                                onChange={(e) => onUpdateScoreThresholdDetail('status', e.target.value)}
                            />{' '}
                    Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(updateScoreThresholdDetails.status == 0) ? true : false}
                                onChange={(e) => onUpdateScoreThresholdDetail('status', e.target.value)}
                            />{' '}
                    Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>
    </Form>
);

export default UpdateUserForm;
