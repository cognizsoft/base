/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ addErr, addNewScoreThresholdDetails, onChangeaddNewScoreThresholdDetail, checkScoreThresholdExist, DatePicker, startDate }) => (
    <Form>
        <div className="row">
            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Thresholds Type</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="type"
                                value={1}
                                checked={(addNewScoreThresholdDetails.type == 1) ? true : false}
                                onChange={(e) => onChangeaddNewScoreThresholdDetail('type', e.target.value)}
                            />{' '}
                    Auto-Approval
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="type"
                                value={0}
                                checked={(addNewScoreThresholdDetails.type == 0) ? true : false}
                                onChange={(e) => onChangeaddNewScoreThresholdDetail('type', e.target.value)}
                            />{' '}
                    Manual Approval
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Thresholds Name<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Thresholds Name"
                        fullWidth
                        variant="outlined"
                        value={addNewScoreThresholdDetails.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onChangeaddNewScoreThresholdDetail('name', e.target.value)}
                    />
                </FormGroup>
            </div>



            <div className="col-md-12">
                <FormGroup>
                    <Label for="value">Thresholds Value<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="value"
                        id="value"
                        placeholder="Thresholds Value"
                        fullWidth
                        variant="outlined"
                        value={addNewScoreThresholdDetails.value}
                        error={(addErr.value) ? true : false}
                        helperText={addErr.value}
                        onChange={(e) => onChangeaddNewScoreThresholdDetail('value', e.target.value)}
                    />
                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input
                        type="textarea"
                        name="description"
                        id="description"
                        placeholder="Description"
                        value={addNewScoreThresholdDetails.description}
                        onChange={(e) => onChangeaddNewScoreThresholdDetail('description', e.target.value)}
                    />
                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup>
                    <Label for="effective_date">Effective Date<span className="required-field">*</span></Label>

                    <DatePicker
                        dateFormat="MM/dd/yyyy"
                        name="effective_date"
                        id="effective_date"
                        selected={startDate}
                        placeholderText="MM/DD/YYYY"
                        autocomplete={false}
                        onChange={(e) => onChangeaddNewScoreThresholdDetail('effective_date', e)}
                    />


                    {(addErr.effective_date) ? <FormHelperText>{addErr.effective_date}</FormHelperText> : ''}

                </FormGroup>
            </div>

            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status</Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewScoreThresholdDetails.status == 1) ? true : false}
                                onChange={(e) => onChangeaddNewScoreThresholdDetail('status', e.target.value)}
                            />{' '}
                    Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewScoreThresholdDetails.status == 0) ? true : false}
                                onChange={(e) => onChangeaddNewScoreThresholdDetail('status', e.target.value)}
                            />{' '}
                    Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>
    </Form>
);

export default AddNewUserForm;
