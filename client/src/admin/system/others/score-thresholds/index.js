/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewScoreThresholdForm from './AddNewUserForm';

// update user form
import UpdateScoreThresholdForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isObjectEmpty, isLength, isContainWhiteSpace, isNumeric } from '../../../../validator/Validator';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

import {
   checkScoreThresholdExist, scoreThresholdMasterDataValueList, scoreThresholdList, updateScoreThreshold, insertScoreThreshold
} from 'Actions';

import AddNewButton from './AddNewButton';

class ScoreThreshold extends Component {

   state = {
      currentModule: 13,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      ScoreThreshold: null, // initial user data
      selectedScoreThreshold: null, // selected user to perform operations
      loading: false, // loading activity
      addNewScoreThresholdModal: false, // add new user form modal
      addNewScoreThresholdDetail: {
         type: 1,
         name: '',
         value: '',
         description: '',
         effective_date: '',
         status: 1,
         checked: false
      },
      openViewScoreThresholdDialog: false, // view user dialog box
      editScoreThreshold: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         name: '',
         value: '',
         effective_date: ''
      },
      add_err: {

      },
      startDate: '',
      ruleExist: false,
   }

   validateAddSubmit() {
      //console.log(this.state.add_err)
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.name === '' &&
         this.state.add_err.value === '' &&
         this.state.add_err.effective_date === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.name === '' &&
         this.state.err.value === '' &&
         this.state.err.effective_date === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateScoreThresholdDetails(name, value)
      });
   }



   componentDidMount() {

      this.permissionFilter(this.state.currentModule);
      this.props.scoreThresholdList();
      //this.props.scoreThresholdMasterDataValueList();

   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { add_err } = this.state;

      let { err } = this.state;
      (nextProps.scoreExist && nextProps.isEdit == 0) ? add_err['name'] = "Name already exists" : "";
      (nextProps.scoreExist && nextProps.isEdit == 1) ? err['name'] = "Name already exists" : err['name'] = '';

      this.setState({ add_err: add_err });
      this.setState({ err: err });



   }


   /*
   * Title :- checkSystemModuleExist
   * Descrpation :- This function use to check system module exist or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */

   checkScoreThresholdExist(name, id) {
      this.props.checkScoreThresholdExist(name, id);
      //console.log(this.state)
   }


   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   /**
    * Open Add New Master Value Modal
    */
   opnaddNewScoreThresholdModal() {
      this.setState({ addNewScoreThresholdModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedRiskFactor = 0;
      let RiskFactor = this.state.RiskFactor.map(userData => {
         if (userData.checked) {
            selectedRiskFactor++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedRiskFactor++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ RiskFactor, selectedRiskFactor });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewScoreThresholdDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'type':
            value = (this.state.addNewScoreThresholdDetail.type == 1) ? 0 : 1;
            break;
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Score Thresholds Type field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'value':
            if (isNumeric(value)) {
               add_err[key] = "Allow only numeric";
            } else if (isEmpty(value)) {
               add_err[key] = "Score Thresholds Value field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'effective_date':
            if (value == '') {
               add_err[key] = "Select Effective Date";
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewScoreThresholdDetail: {
            ...this.state.addNewScoreThresholdDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewScoreThreshold() {
      const { name, value, description, effective_date, status } = this.state.addNewScoreThresholdDetail;

      const filterType = this.props.score_threshold.filter(x => x.name == name && x.value == value && x.effective_date == effective_date);

      if (isObjectEmpty(filterType)) {
         this.setState({ ruleExist: true })
         return false;
      }

      this.props.insertScoreThreshold(this.state.addNewScoreThresholdDetail);


      this.setState({ addNewScoreThresholdModal: false, loading: true, startDate: '' });
      let self = this;
      let user = {
         type: 1,
         name: '',
         value: '',
         description: '',
         effective_date: '',
         status: 1,
         checked: false
      }
      setTimeout(() => {
         self.setState({ loading: false, addNewScoreThresholdDetail: user });
      }, 2000);

   }

   /**
    * View Master Value Detail Hanlder
    */
   viewScoreThresholdDetail(data) {
      console.log(data)
      this.setState({ openViewScoreThresholdDialog: true, selectedScoreThreshold: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewScoreThresholdModalClose() {
      this.setState({ openViewScoreThresholdDialog: false, selectedScoreThreshold: null })
   }

   /**
    * On Edit Master Value
    */
   onEditScoreThreshold(editRfactor) {

      this.setState({ addNewScoreThresholdModal: true, editScoreThreshold: editRfactor, startDate: new Date(editRfactor.effective_date) });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateScoreThresholdModalClose = () => {
      let upR = { name: '', value: '', description: '', effective_date: '' }
      let addR = {}
      this.setState({ addNewScoreThresholdModal: false, editScoreThreshold: null, err: upR, add_err: addR, startDate: '', addNewScoreThresholdDetail: upR, ruleExist: false })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateScoreThresholdDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'type':
            value = (this.state.editScoreThreshold.type == 1)?0:1
            break;
         case 'name':
            if (isEmpty(value)) {
               err[fieldName] = "Score Thresholds Type field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'value':
            if (isNumeric(value)) {
               err[fieldName] = "Allow only numeric";
            } else if (isEmpty(value)) {
               err[fieldName] = "Score Thresholds Value field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'effective_date':
            if (value == null) {
               err[fieldName] = "Select Effective Date";
               this.setState({ startDate: '' })
            } else {
               this.setState({ startDate: value })
               value = moment(value).format('YYYY-MM-DD');
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editScoreThreshold: {
            ...this.state.editScoreThreshold,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateScoreThreshold() {
      const { editScoreThreshold } = this.state;
      const filterType = this.props.score_threshold.filter(x => x.name == editScoreThreshold.name && x.value == editScoreThreshold.value && x.effective_date == editScoreThreshold.effective_date && x.id !== editScoreThreshold.id);

      if (filterType.length > 0) {
         this.setState({ ruleExist: true })
         return false;
      }

      let indexOfUpdateScoreThreshold = '';
      let ScoreThreshold = this.props.score_threshold;
      //console.log(ScoreThreshold)
      for (let i = 0; i < ScoreThreshold.length; i++) {
         const user = ScoreThreshold[i];

         if (user.id === editScoreThreshold.id) {
            indexOfUpdateScoreThreshold = i
         }
      }
      
      this.props.updateScoreThreshold(editScoreThreshold);


      ScoreThreshold[indexOfUpdateScoreThreshold] = editScoreThreshold;
      this.setState({ loading: true, editScoreThreshold: null, addNewScoreThresholdModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ ScoreThreshold, loading: false });
         //NotificationManager.success('Score Threshold Updated!');
      }, 2000);
   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for change date format
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- May 14,2019
   */
   changeDate(date) {
      var chngFrmt = date;
      var date_explode = chngFrmt.split('-');
      return date_explode[1] + '/' + date_explode[2] + '/' + date_explode[0];
   }
   render() {
      const { add_err, err, RiskFactor, loading, editScoreThreshold, allSelected, selectedScoreThreshold } = this.state;
      //const master_score_threshold = this.props.score_threshold_master_data_value
      const scoreThreshold = this.props.score_threshold;
      //console.log(scoreThreshold)

      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Score Thresholds Type',
            field: 'type',
            options: {
               customBodyRender: (value) => {
                  return (
                     (value.type == 1)?'Default':'Manual'
                  );
               }
            }
         },
         {
            name: 'Score Thresholds Name',
            field: 'name',
         },
         {
            name: 'Score Thresholds Value',
            field: 'value'
         },
         {
            name: 'Description',
            field: 'description',
         },
         {
            name: 'Effective Date',
            field: 'effective_date',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  var chngFrmt = value.effective_date;
                  var date_explode = chngFrmt.split('-');
                  var newDate = date_explode[1] + '/' + date_explode[2] + '/' + date_explode[0];
                  return (
                     newDate
                  );
               }
            }
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: true,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  //console.log(value)
                  return (
                     <div className="list-action">

                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewScoreThresholdDetail(value)}><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditScoreThreshold(value)}><i className="ti-pencil"></i></a> : ''}

                     </div>
                  );
               }
            }
         }

      ];

      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               (this.state.currentPermision.add) ? <AddNewButton opnaddNewScoreThresholdModal={this.opnaddNewScoreThresholdModal.bind(this)} /> : ''
            );
         }
      };
      return (
         <div className="others score-threshold">
            <Helmet>
               <title>Health Partner | Others | Score Threshold</title>
               <meta name="description" content="Score Threshold" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.systemScoreThresholds" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable

                  data={scoreThreshold}
                  columns={columns}
                  options={options}


               />
            </RctCollapsibleCard>



            <Modal isOpen={this.state.addNewScoreThresholdModal} toggle={() => this.onAddUpdateScoreThresholdModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateScoreThresholdModalClose()}>
                  {editScoreThreshold === null ?
                     'Add App Threshold' : 'Update App Threshold'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist &&
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editScoreThreshold === null ?
                     <AddNewScoreThresholdForm
                        addErr={add_err}
                        addNewScoreThresholdDetails={this.state.addNewScoreThresholdDetail}
                        onChangeaddNewScoreThresholdDetail={this.onChangeaddNewScoreThresholdDetails.bind(this)}
                        checkScoreThresholdExist={this.checkScoreThresholdExist.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                     : <UpdateScoreThresholdForm
                        updateErr={err}
                        updateScoreThresholdDetails={editScoreThreshold}
                        onUpdateScoreThresholdDetail={this.onUpdateScoreThresholdDetails.bind(this)}
                        checkScoreThresholdExist={this.checkScoreThresholdExist.bind(this)}
                        DatePicker={DatePicker}
                        startDate={this.state.startDate}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editScoreThreshold === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewScoreThreshold()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateScoreThreshold()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateScoreThresholdModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewScoreThresholdDialog} toggle={() => this.onViewScoreThresholdModalClose()}>
               <ModalHeader toggle={() => this.onViewScoreThresholdModalClose()}>
                  {selectedScoreThreshold !== null ? 'App Threshold View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedScoreThreshold !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">
                              <div className="colmn-row"><span className="first-colmn fw-bold">Thresholds Type:</span> <span className="second-colmn">{(selectedScoreThreshold.type==1)?'Default':'Manual'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Thresholds Name:</span> <span className="second-colmn">{selectedScoreThreshold.name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Thresholds Value:</span> <span className="second-colmn">{selectedScoreThreshold.value}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Description:</span> <span className="second-colmn">{selectedScoreThreshold.description}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Effective Date:</span> <span className="second-colmn">
                                    {
                                       this.changeDate(selectedScoreThreshold.effective_date)
                                    }
                                 </span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedScoreThreshold.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>

         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ scoreThreshold, authUser }) => {
   //console.log(scoreThreshold)

   const { score_threshold, loading, scoreExist, isEdit } = scoreThreshold;
   const user = authUser.user;
   return { score_threshold, loading, user, scoreExist, isEdit }

}

export default connect(mapStateToProps, {
   checkScoreThresholdExist, scoreThresholdMasterDataValueList, scoreThresholdList, updateScoreThreshold, insertScoreThreshold
})(ScoreThreshold);