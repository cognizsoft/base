import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";

const defaultToolbarStyles = {
  iconButton: {
  },
};

class CustomToolbar extends React.Component {
  

  render() {
    

    return (
      <React.Fragment>
        <Tooltip title={""}>
        <a href="javascript:void(0)" onClick={() => this.props.opnAddNewSystemModuleModal()} color="primary" className="caret btn-sm mr-10">Add System Module <i className="zmdi zmdi-plus"></i></a>
        </Tooltip>
      </React.Fragment>
    );
  }

}

export default withStyles(defaultToolbarStyles, { name: "CustomToolbar" })(CustomToolbar);