/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isEmpty } from '../../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   checkSystemModuleExist, getAllSystemModule, insertSystemModule, updateSystemModule
} from 'Actions';

class SystemModule extends Component {

   state = {
      currentModule: 17,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewSystemModuleModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewSystemModuleDetail: {
         description: '',
         status: 1,
      },
      openViewSystemModuleDialog: false, // view user dialog box
      editSystemModule: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         description: '',
      },
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllSystemModule();
   }


   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.moduleExist && nextProps.isEdit == 0) ? add_err['description'] = "System Module already exists" : '';
      (nextProps.moduleExist && nextProps.isEdit == 1) ? udpate_err['description'] = "System Module already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });

   }

   /*
   * Title :- checkSystemModuleExist
   * Descrpation :- This function use to check system module exist or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */

   checkSystemModuleExist(value, sys_mod_id) {
      this.props.checkSystemModuleExist(value, sys_mod_id);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- viewQuestionDetail
   * Descrpation :- This function use open view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   viewSystemModuleDetail(data) {
      this.setState({ openViewSystemModuleDialog: true, selectedUser: data });
   }

   /*
   * Title :- onEditQuestion
   * Descrpation :- This function use open popup in eidt case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onEditSystemModule(system_module) {
      this.setState({ addNewSystemModuleModal: true, editSystemModule: system_module });
   }

   /*
   * Title :- onAddUpdateQuestionModalClose
   * Descrpation :- This function use close popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onAddUpdateSystemModuleModalClose() {
      let emptyError = {
      }
      let addNewSystemModuleDetail = {
         description: '',
         status: 1,
      }
      let emptyUpdateError = {
         description: '',
      }
      this.setState({ addNewSystemModuleModal: false, editSystemModule: null, add_err: emptyError, addNewSystemModuleDetail: addNewSystemModuleDetail, udpate_err: emptyUpdateError })
   }

   /*
   * Title :- onViewQuestionModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onViewSystemModuleModalClose = () => {
      this.setState({ openViewSystemModuleDialog: false, selectedUser: null })
   }

   /*
   * Title :- opnAddNewQuestionsModal
   * Descrpation :- This function use for open add popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   opnAddNewSystemModuleModal() {
      //console.log('this.state')
      //console.log(this.state)
      this.setState({ addNewSystemModuleModal: true });
   }
   /*
   * Title :- onChangeAddNewQuestionsDetails
   * Descrpation :- This function use for check field validation on add new question
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onChangeAddNewSystemModuleDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'description':
            if (isEmpty(value)) {
               add_err[key] = "System Module name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewSystemModuleDetail: {
            ...this.state.addNewSystemModuleDetail,
            [key]: value
         }
      });
   }

   /*
   * Title :- onUpdateQuestionDetails
   * Descrpation :- This function use for check filed validation on edit question case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onUpdateSystemModuleDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'description':
            if (isEmpty(value)) {
               udpate_err[key] = "System Module name can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;

         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editSystemModule: {
            ...this.state.editSystemModule,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateAddSubmit() {
      //console.log('this.props.moduleExist')
      //console.log(this.state.add_err)
      return (
         
         this.state.add_err.description === ''
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateUpdateSubmit() {
      
      return (
         
         this.state.udpate_err.description === ''
      );
   }
   /*
   * Title :- addNewQuestions
   * Descrpation :- This function use for add new question action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   addNewSystemModule() {

      this.props.insertSystemModule(this.state.addNewSystemModuleDetail);
      this.setState({ addNewSystemModuleModal: false });
      let self = this;
      let addNewSystemModuleDetail = {
         description: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewSystemModuleDetail: addNewSystemModuleDetail });
      }, 2000);
   }
   /*
   * Title :- updateSystemModule
   * Descrpation :- This function use call question update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   updateSystemModule() {
      const { editSystemModule } = this.state;

      let indexOfUpdateSystemModule = '';
      let systemModuleCurrent = this.props.systemmodulelist;
      for (let i = 0; i < systemModuleCurrent.length; i++) {
         const current = systemModuleCurrent[i];
         if (current.sys_mod_id === editSystemModule.sys_mod_id) {
            indexOfUpdateSystemModule = i
         }
      }
      this.props.updateSystemModule(editSystemModule);

      systemModuleCurrent[indexOfUpdateSystemModule] = editSystemModule;

      this.setState({ editSystemModule: null, addNewSystemModuleModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ systemModuleCurrent });
      //}, 2000);
   }
   /*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }
   /*
   * Title :- deleteUserPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   deleteUserPermanently() {
      const { selectedUser } = this.state;
      let questions = this.props.questionlist;
      let indexOfDeleteQuestion = questions.indexOf(selectedUser);
      this.props.deleteQuestion(questions[indexOfDeleteQuestion].id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedUser: null });
   }
   render() {
      const { selectedUser, editSystemModule, add_err, udpate_err } = this.state;
      const systemmodulelist = this.props.systemmodulelist;
      const columns = [

         {
            name: 'ID',
            field: 'sys_mod_id'
         },
         {
            name: 'System Module',
            field: 'description',
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewSystemModuleDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditSystemModule(value)}><i className="ti-pencil"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Question.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewSystemModuleModal={this.opnAddNewSystemModuleModal.bind(this)} />:''
            );
         },
      };
      return (
         <div className="system-settings system-module">
            <Helmet>
               <title>Health Partner | System | System Settings | System Module</title>
               <meta name="description" content="Security Questions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.systemModule" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={systemmodulelist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete question permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            <Modal isOpen={this.state.addNewSystemModuleModal} toggle={() => this.onAddUpdateSystemModuleModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateSystemModuleModalClose()}>
                  {editSystemModule === null ?
                     'Add System Module' : 'Update System Module'
                  }
               </ModalHeader>
               <ModalBody>
                  {editSystemModule === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewSystemModuleDetails={this.state.addNewSystemModuleDetail}
                        onChangeAddNewSystemModuleDetails={this.onChangeAddNewSystemModuleDetails.bind(this)}
                        checkSystemModuleExist={this.checkSystemModuleExist.bind(this)}
                     />
                     :
                     <UpdateUserForm
                        editSystemModule={editSystemModule}
                        addErr={udpate_err}
                        onUpdateSystemModuleDetails={this.onUpdateSystemModuleDetails.bind(this)}
                        checkSystemModuleExist={this.checkSystemModuleExist.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editSystemModule === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewSystemModule()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateSystemModule()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateSystemModuleModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewSystemModuleDialog} toggle={() => this.onViewSystemModuleModalClose()}>
               <ModalHeader toggle={() => this.onViewSystemModuleModalClose()}>
                  {selectedUser !== null ? 'System Module View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                <div className="colmn-row"><span className="first-colmn fw-bold">System Module Name:</span> <span className="second-colmn">{selectedUser.description}</span></div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, systemModuleDetails }) => {
   const { user } = authUser;
   const { loading, systemmodulelist, moduleExist, isEdit } = systemModuleDetails;
   return { loading, systemmodulelist, user, moduleExist, isEdit }
   //return {user }

}

export default connect(mapStateToProps, {
   checkSystemModuleExist, getAllSystemModule, insertSystemModule, updateSystemModule
})(SystemModule);