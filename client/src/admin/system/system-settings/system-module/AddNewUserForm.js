/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewSystemModuleDetails, onChangeAddNewSystemModuleDetails, checkSystemModuleExist }) => (
    <Form>
    <div className="row">
        
       <div className="col-md-12">
        <FormGroup>
            <Label for="description">System Module Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="description"
                id="description"
                placeholder="System Module"
                fullWidth
                variant="outlined"
                value={addNewSystemModuleDetails.description}
                error={(addErr.description) ? true : false}
                helperText={addErr.description}
                onChange={(e) => onChangeAddNewSystemModuleDetails('description', e.target.value)}
                onKeyUp={(e) => checkSystemModuleExist(e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        
            
        </div>
    </Form>
);

export default AddNewUserForm;
