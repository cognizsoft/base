/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ editSystemModule, addErr, onUpdateSystemModuleDetails, checkSystemModuleExist }) => (
    <Form>
    
    <div className="row">
        
       <div className="col-md-12">
        <FormGroup>
            <Label for="description">System Module Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="description"
                id="description"
                placeholder="System Module"
                fullWidth
                variant="outlined"
                value={editSystemModule.description}
                error={(addErr.description) ? true : false}
                helperText={addErr.description}
                onChange={(e) => onUpdateSystemModuleDetails('description', e.target.value)}
                onKeyUp={(e) => checkSystemModuleExist(e.target.value, editSystemModule.sys_mod_id)}
            />
        </FormGroup>
        </div>
    </div>

    </Form>    
);

export default UpdateUserForm;
