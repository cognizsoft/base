/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ addErr, addNewAnsweretails, onChangeAddNewAnswerDetails, questionlists, userslists }) => (
    <Form>

        <div className="row">
            <div className="col-md-6">
                <FormGroup>
                    <Label for="user_id">Select User<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="user_id"
                        id="user_id"
                        onChange={(e) => onChangeAddNewAnswerDetails('user_id', e.target.value, e.target[e.target.selectedIndex].text)}
                    >

                        <option value="">Select</option>
                        {userslists && userslists.map((type, key) => (
                            <option value={type.user_id} key={key}>{type.username+' - '+type.f_name+' '+type.m_name+' '+type.l_name}</option>
                        ))}
                    </Input>
                    {(addErr.user_id) ? <FormHelperText>{addErr.user_id}</FormHelperText> : ''}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="userType">Select Question<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="security_questions_id"
                        id="security_questions_id"
                        onChange={(e) => onChangeAddNewAnswerDetails('security_questions_id', e.target.value, e.target[e.target.selectedIndex].text)}
                    >

                        <option value="">Select</option>
                        {questionlists && questionlists.map((type, key) => (
                            <option value={type.security_questions_id} key={key}>{type.name}</option>
                        ))}
                    </Input>
                    {(addErr.security_questions_id) ? <FormHelperText>{addErr.security_questions_id}</FormHelperText> : ''}
                </FormGroup>
            </div>
            <div className="col-md-6">
                <FormGroup>
                    <Label for="answers">Answer<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="answers"
                        id="answers"
                        placeholder="Answer"
                        fullWidth
                        variant="outlined"
                        value={addNewAnsweretails.answers}
                        error={(addErr.answers) ? true : false}
                        helperText={addErr.answers}
                        onChange={(e) => onChangeAddNewAnswerDetails('answers', e.target.value)}
                    />

                </FormGroup>
            </div>


            <div className="col-md-6">
                <FormGroup tag="fieldset">
                    <Label>Status <span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewAnsweretails.status == 1) ? true : false}
                                onChange={(e) => onChangeAddNewAnswerDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewAnsweretails.status == 0) ? true : false}
                                onChange={(e) => onChangeAddNewAnswerDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>

    </Form>
);

export default AddNewUserForm;
