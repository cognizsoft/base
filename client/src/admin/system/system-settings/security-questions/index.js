/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isEmpty } from '../../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import {
   checkSecurityQuestionExist, getAllQuestion, insertQuestion, updateQuestion, deleteQuestion
} from 'Actions';

class SecurityQuestions extends Component {

   state = {
      currentModule: 6,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewQuestionModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewQuestionDetail: {
         name: '',
         status: 1,
      },
      openViewQuestionDialog: false, // view user dialog box
      editQuestion: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         name: '',
      },
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllQuestion();
   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.questionExist && nextProps.isEdit == 0) ? add_err['name'] = "Security Question already exists" : '';
      (nextProps.questionExist && nextProps.isEdit == 1) ? udpate_err['name'] = "Security Question already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });

   }

   /*
   * Title :- checkSecurityQuestionExist
   * Descrpation :- This function use to check system module exist or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */

   checkSecurityQuestionExist(value, id) {
      this.props.checkSecurityQuestionExist(value, id);
   }

   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- viewQuestionDetail
   * Descrpation :- This function use open view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   viewQuestionDetail(data) {
      this.setState({ openViewQuestionDialog: true, selectedUser: data });
   }

	/*
   * Title :- onEditQuestion
   * Descrpation :- This function use open popup in eidt case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onEditQuestion(question) {
      this.setState({ addNewQuestionModal: true, editQuestion: question });
   }

	/*
   * Title :- onAddUpdateQuestionModalClose
   * Descrpation :- This function use close popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onAddUpdateQuestionModalClose() {
      let emptyError = {
      }
      let addNewQuestionDetail = {
         name: '',
         status: 1,
      }
      let emptyUpdateError = {
         name: '',
      }
      this.setState({ addNewQuestionModal: false, editQuestion: null, add_err: emptyError, addNewQuestionDetail: addNewQuestionDetail, udpate_err: emptyUpdateError })
   }

   /*
   * Title :- onViewQuestionModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onViewQuestionModalClose = () => {
      this.setState({ openViewQuestionDialog: false, selectedUser: null })
   }

   /*
   * Title :- opnAddNewQuestionsModal
   * Descrpation :- This function use for open add popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   opnAddNewQuestionsModal() {
      this.setState({ addNewQuestionModal: true });
   }
   /*
   * Title :- onChangeAddNewQuestionsDetails
   * Descrpation :- This function use for check field validation on add new question
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onChangeAddNewQuestionsDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Question name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewQuestionDetail: {
            ...this.state.addNewQuestionDetail,
            [key]: value
         }
      });
   }

   /*
   * Title :- onUpdateQuestionDetails
   * Descrpation :- This function use for check filed validation on edit question case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onUpdateQuestionDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               udpate_err[key] = "Question name can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;

         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editQuestion: {
            ...this.state.editQuestion,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.name === ''
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.udpate_err.name === ''
      );
   }
   /*
   * Title :- addNewQuestions
   * Descrpation :- This function use for add new question action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   addNewQuestions() {

      this.props.insertQuestion(this.state.addNewQuestionDetail);
      this.setState({ addNewQuestionModal: false });
      let self = this;
      let addNewQuestionDetail = {
         name: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewQuestionDetail: addNewQuestionDetail });
      }, 2000);
   }
   /*
   * Title :- updateQuestion
   * Descrpation :- This function use call question update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   updateQuestion() {
      const { editQuestion } = this.state;

      let indexOfUpdateQuestion = '';
      let questionCurrent = this.props.questionlist;
      for (let i = 0; i < questionCurrent.length; i++) {
         const current = questionCurrent[i];
         if (current.id === editQuestion.id) {
            indexOfUpdateQuestion = i
         }
      }
      this.props.updateQuestion(editQuestion);

      questionCurrent[indexOfUpdateQuestion] = editQuestion;

      this.setState({ editQuestion: null, addNewQuestionModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ questionCurrent });
      //}, 2000);
   }
   /*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }
   /*
   * Title :- deleteUserPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   deleteUserPermanently() {
      const { selectedUser } = this.state;
      let questions = this.props.questionlist;
      let indexOfDeleteQuestion = questions.indexOf(selectedUser);
      this.props.deleteQuestion(questions[indexOfDeleteQuestion].id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedUser: null });
   }
   render() {
      const { selectedUser, editQuestion, add_err, udpate_err } = this.state;
      const questionlist = this.props.questionlist;
      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Questions',
            field: 'name',
         },
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewQuestionDetail(value)} title="View Questions"><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditQuestion(value)} title="Edit Questions"><i className="ti-pencil"></i></a>:''}
                        {(this.state.currentPermision.delete)?<a href="javascript:void(0)" onClick={() => this.onDelete(value)} title="Remove Questions"><i className="ti-close"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'Question.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewQuestionsModal={this.opnAddNewQuestionsModal.bind(this)} />:''
            );
         },
      };
      return (
         <div className="system-settings security-questions">
            <Helmet>
               <title>Health Partner | System | System Settings | Security Questions</title>
               <meta name="description" content="Security Questions" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.securityQuestions" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={questionlist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete question permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            <Modal isOpen={this.state.addNewQuestionModal} toggle={() => this.onAddUpdateQuestionModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateQuestionModalClose()}>
                  {editQuestion === null ?
                     'Add Security Questions' : 'Update Security Questions'
                  }
               </ModalHeader>
               <ModalBody>
                  {editQuestion === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewQuestionDetails={this.state.addNewQuestionDetail}
                        onChangeAddNewQuestionsDetails={this.onChangeAddNewQuestionsDetails.bind(this)}
                        checkSecurityQuestionExist={this.checkSecurityQuestionExist.bind(this)}
                     />
                     :
                     <UpdateUserForm
                        editQuestion={editQuestion}
                        addErr={udpate_err}
                        onUpdateQuestionDetails={this.onUpdateQuestionDetails.bind(this)}
                        checkSecurityQuestionExist={this.checkSecurityQuestionExist.bind(this)}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editQuestion === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewQuestions()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateQuestion()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateQuestionModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewQuestionDialog} toggle={() => this.onViewQuestionModalClose()}>
               <ModalHeader toggle={() => this.onViewQuestionModalClose()}>
                  {selectedUser !== null ? 'Security Questions View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                <div className="colmn-row"><span className="first-colmn fw-bold">Question:</span> <span className="second-colmn">{selectedUser.name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedUser.status}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, questionDetails }) => {
   const { user } = authUser;
   const { loading, questionlist, questionExist, isEdit } = questionDetails;
   return { loading, questionlist, user, questionExist, isEdit }
   //return {user }

}

export default connect(mapStateToProps, {
   checkSecurityQuestionExist, getAllQuestion, insertQuestion, updateQuestion, deleteQuestion
})(SecurityQuestions);