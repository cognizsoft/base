/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewQuestionDetails, onChangeAddNewQuestionsDetails, checkSecurityQuestionExist }) => (
    <Form>

        <div className="row">

            <div className="col-md-12">
                <FormGroup>
                    <Label for="name">Question<span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Question"
                        fullWidth
                        variant="outlined"
                        value={addNewQuestionDetails.name}
                        error={(addErr.name) ? true : false}
                        helperText={addErr.name}
                        onChange={(e) => onChangeAddNewQuestionsDetails('name', e.target.value)}
                        onKeyUp={(e) => checkSecurityQuestionExist(e.target.value)}
                    />

                </FormGroup>
            </div>


            <div className="col-md-12">
                <FormGroup tag="fieldset">
                    <Label>Status <span className="required-field">*</span></Label>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={1}
                                checked={(addNewQuestionDetails.status == 1) ? true : false}
                                onChange={(e) => onChangeAddNewQuestionsDetails('status', e.target.value)}
                            />
                            Active
                </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input
                                type="radio"
                                name="status"
                                value={0}
                                checked={(addNewQuestionDetails.status == 0) ? true : false}
                                onChange={(e) => onChangeAddNewQuestionsDetails('status', e.target.value)}
                            />
                            Inactive
                </Label>
                    </FormGroup>
                </FormGroup>
            </div>
        </div>

    </Form>
);

export default AddNewUserForm;
