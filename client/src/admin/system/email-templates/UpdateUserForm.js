/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import ReactQuill from 'react-quill';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ editEmailTemp, addErr, onUpdateEmailTempDetails, checkEmailTempExist, modules, formats, onChangeAddNewEmailTempEditor, variable, masterTemplateType }) => (
    <Form>

        <div className="row">

            <div className="col-md-4">
                <FormGroup>
                    <Label for="template_type">Template Type<span className="required-field">*</span></Label>
                    <Input
                        type="select"
                        name="template_type"
                        id="template_type"
                        placeholder=""
                        defaultValue={editEmailTemp.template_type}
                        onChange={(e) => onUpdateEmailTempDetails('template_type', e.target.value)}
                    >
                        <option value="">Select</option>
                        {masterTemplateType && masterTemplateType.map((temp, key) => (
                            <option value={temp.status_id} key={key}>{temp.value}</option>
                        ))}
                        
                    </Input>
                    {(addErr.template_type) ? <FormHelperText>{addErr.template_type}</FormHelperText> : ''}
                </FormGroup>
            </div>

           <div className="col-md-4">
                <FormGroup>
                    <Label for="template_name">Template <span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="template_name"
                        id="template_name"
                        placeholder="Template Name"
                        fullWidth
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="outlined"
                        value={editEmailTemp.template_name}
                        error={(addErr.template_name) ? true : false}
                        helperText={addErr.template_name}
                        onChange={(e) => onUpdateEmailTempDetails('template_name', e.target.value)}
                        onKeyUp={(e) => checkEmailTempExist(e.target.value, editEmailTemp.id)}
                    />

                </FormGroup>
            </div>

            <div className="col-md-4">
                <FormGroup>
                    <Label for="template_subject">Subject <span className="required-field">*</span></Label>
                    <TextField
                        type="text"
                        name="template_subject"
                        id="template_subject"
                        placeholder="Template Subject"
                        fullWidth
                        variant="outlined"
                        value={editEmailTemp.template_subject}
                        error={(addErr.template_subject) ? true : false}
                        helperText={addErr.template_subject}
                        onChange={(e) => onUpdateEmailTempDetails('template_subject', e.target.value)}
                    />

                </FormGroup>
            </div>

            {/*<div className="col-md-12">
                            <FormGroup>
                                <Label for="template_content">Content <span className="required-field">*</span></Label>
                                <TextField
                                    type="text"
                                    name="template_content"
                                    id="template_content"
                                    placeholder="Template Content"
                                    fullWidth
                                    variant="outlined"
                                    value={editEmailTemp.template_content}
                                    error={(addErr.template_content) ? true : false}
                                    helperText={addErr.template_content}
                                    onChange={(e) => onUpdateEmailTempDetails('template_content', e.target.value)}
                                />
            
                            </FormGroup>
                        </div>*/}

            <div className="col-md-12">
                <Label for="template_content">Body
                </Label>
                <span className="font-weight-normal mb-5">
                    {variable && variable.map((opt, key) => (
                        <p className="d-inline" key={key}>{opt.variable} </p>
                    ))}
                </span>
                <ReactQuill 

                    modules={modules} 
                    formats={formats}  
                    placeholder="Body" 
                    value={(editEmailTemp.template_content) ? editEmailTemp.template_content : ''}

                    onChange={(text: string, delta: any, source: string, editor: any) => {
                        if (source == 'user') {

                          //console.log(editor.getHTML())
                          onChangeAddNewEmailTempEditor(editor.getHTML())
                          
                        }
                      }}

                    />
                {(addErr.template_content) ? <FormHelperText>{addErr.template_content}</FormHelperText> : ''}
            </div>

        </div>

    </Form>
);

export default UpdateUserForm;
