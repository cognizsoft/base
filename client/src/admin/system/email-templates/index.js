/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import CustomToolbar from "./CustomToolbar";
import {
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';
import { isEmpty } from '../../../validator/Validator';
// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { ReactDOM, render } from 'react-dom'
import {
   checkEmailTempExist, getAllEmailTemp, insertEmailTemp, updateEmailTemp, deleteEmailTemp, getEmailTempVariable
} from 'Actions';

const modules = {
  toolbar: [
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'font': [] }/*, { 'size': [] }*/],
    [{ 'color': [] }, { 'background': [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
    ['link', 'image'],
    ['clean'],
    [{ 'align': [] }]
  ],
};

const formats = [
  'header',
  'font', 'size', 'color', 'background',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image', 'align'
];

class EmailTemplates extends Component {

   state = {
      currentModule: 6,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedUser: null, // selected user to perform operations
      loading: false, // loading activity
      addNewEmailTempModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewEmailTempDetail: {
         template_type: '',
         template_name: '',
         template_subject: '',
         template_content: '',
         status: 1,
      },
      openViewEmailTempDialog: false, // view user dialog box
      editEmailTemp: null,
      allSelected: false,
      selectedUsers: 0,
      add_err: {},
      udpate_err: {
         template_type: '',
         template_name: '',
         template_subject: '',
         template_content: '',
      },
      text: '',
   }

   onChangeAddNewEmailTempEditor = (value) => {

      let { addNewEmailTempDetail, editEmailTemp } = this.state
      //let newValue = replaceTagByText(value);

      addNewEmailTempDetail.template_content = value;
      (editEmailTemp !==null) ? editEmailTemp.template_content = value : ''

      this.setState({ addNewEmailTempDetail: addNewEmailTempDetail, editEmailTemp: editEmailTemp });
   };
   /*
   * Title :- componentDidMount
   * Descrpation :- This function call when component call and call another function or action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.getAllEmailTemp();
   }

   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 16,2019
   */
   componentWillReceiveProps(nextProps) {
      //console.log(nextProps)
      let { add_err } = this.state;
      let { udpate_err } = this.state;
      (nextProps.emailTempExist && nextProps.isEdit == 0) ? add_err['template_name'] = "Template already exists" : '';
      (nextProps.emailTempExist && nextProps.isEdit == 1) ? udpate_err['template_name'] = "Template already exists" : '';

      this.setState({ add_err: add_err });
      this.setState({ udpate_err: udpate_err });

   }

   /*
   * Title :- checkEmailTempExist
   * Descrpation :- This function use to check email template exist or not
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */

   checkEmailTempExist(value, id) {
      this.props.checkEmailTempExist(value, id);
   }

   
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use filter action permission according to current user
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );

      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- viewEmailTempDetail
   * Descrpation :- This function use open view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   viewEmailTempDetail(data) {
      this.setState({ openViewEmailTempDialog: true, selectedUser: data });
   }

	/*
   * Title :- onEditEmailTemp
   * Descrpation :- This function use open popup in eidt case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onEditEmailTemp(emailTemp) {
      this.props.getEmailTempVariable(emailTemp.id)
      //console.log(emailTemp)
      this.setState({ addNewEmailTempModal: true, editEmailTemp: emailTemp });
   }

	/*
   * Title :- onAddUpdateEmailTempModalClose
   * Descrpation :- This function use close popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onAddUpdateEmailTempModalClose() {
      let emptyError = {
      }
      let addNewEmailTempDetail = {
         template_type: '',
         template_name: '',
         template_subject: '',
         template_content: '',
         status: 1,
      }
      let emptyUpdateError = {
         template_type: '',
         template_name: '',
         template_subject: '',
      }
      this.setState({ addNewEmailTempModal: false, editEmailTemp: null, add_err: emptyError, addNewEmailTempDetail: addNewEmailTempDetail, udpate_err: emptyUpdateError })
   }

   /*
   * Title :- onViewQuestionModalClose
   * Descrpation :- This function use close view popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onViewEmailTempModalClose = () => {
      this.setState({ openViewEmailTempDialog: false, selectedUser: null })
   }

   /*
   * Title :- opnAddNewEmailTempModal
   * Descrpation :- This function use for open add popup
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   opnAddNewEmailTempModal() {
      this.setState({ addNewEmailTempModal: true });
   }
   /*
   * Title :- onChangeAddNewQuestionsDetails
   * Descrpation :- This function use for check field validation on add new question
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onChangeAddNewEmailTempDetails(key, value) {
      let { add_err } = this.state;
      switch (key) {
         case 'template_type':
            if (isEmpty(value)) {
               add_err[key] = "Template type can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'template_name':
            if (isEmpty(value)) {
               add_err[key] = "Template name can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'template_subject':
            if (isEmpty(value)) {
               add_err[key] = "Subject can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'template_content':
            if (isEmpty(value)) {
               add_err[key] = "Content can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ add_err: add_err });

      this.setState({
         addNewEmailTempDetail: {
            ...this.state.addNewEmailTempDetail,
            [key]: value
         }
      });
   }

   /*
   * Title :- onUpdateEmailTempDetails
   * Descrpation :- This function use for check filed validation on edit question case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onUpdateEmailTempDetails(key, value) {

      let { udpate_err } = this.state;
      switch (key) {
         case 'template_type':
            if (isEmpty(value)) {
               udpate_err[key] = "Template type can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'template_name':
            if (isEmpty(value)) {
               udpate_err[key] = "Template name can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
            case 'template_subject':
            if (isEmpty(value)) {
               udpate_err[key] = "Subject can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         case 'template_content':
         console.log(value)
            if (isEmpty(value)) {
               udpate_err[key] = "Content can't be blank";
            } else {
               udpate_err[key] = '';
            }
            break;
         default:
            break;
      }
      this.setState({ udpate_err: udpate_err });

      this.setState({
         editEmailTemp: {
            ...this.state.editEmailTemp,
            [key]: value
         }
      });
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for enable or disable submit button in add case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateAddSubmit() {
      return (
         this.state.add_err.template_type === '' &&
         this.state.add_err.template_name === '' &&
         this.state.add_err.template_subject === '' 
      );
   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for enable or disable submit button in edit case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   validateUpdateSubmit() {

      return (
         this.state.udpate_err.template_type === '' &&
         this.state.udpate_err.template_name === '' &&
         this.state.udpate_err.template_subject === ''
      );
   }
   /*
   * Title :- addNewQuestions
   * Descrpation :- This function use for add new question action
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   addNewEmailTemp() {
      //console.log(this.state.addNewEmailTempDetail)
      //return false
      this.props.insertEmailTemp(this.state.addNewEmailTempDetail);
      this.setState({ addNewEmailTempModal: false });
      let self = this;
      let addNewEmailTempDetail = {
         template_type: '',
         template_name: '',
         template_subject: '',
         template_content: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewEmailTempDetail: addNewEmailTempDetail });
      }, 2000);
   }
   /*
   * Title :- updateQuestion
   * Descrpation :- This function use call question update action and update state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   updateEmailTemp() {
      const { editEmailTemp } = this.state;

      let indexOfUpdateEmailTemp = '';
      let emailTempCurrent = this.props.emailTemplist;
      for (let i = 0; i < emailTempCurrent.length; i++) {
         const current = emailTempCurrent[i];
         if (current.id === editEmailTemp.id) {
            indexOfUpdateEmailTemp = i
         }
      }

      //console.log(editEmailTemp)
      //return false;
      this.props.updateEmailTemp(editEmailTemp);

      emailTempCurrent[indexOfUpdateEmailTemp] = editEmailTemp;

      this.setState({ editEmailTemp: null, addNewEmailTempModal: false });
      let self = this;
      //setTimeout(() => {
      self.setState({ emailTempCurrent });
      //}, 2000);
   }
   /*
   * Title :- onDelete
   * Descrpation :- This function call if user want delete any record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   onDelete(data) {
      this.refs.deleteConfirmationDialog.open();
      this.setState({ selectedUser: data });
   }
   /*
   * Title :- deleteUserPermanently
   * Descrpation :- This function delete record if user confirm 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 24,2019
   */
   deleteUserPermanently() {
      const { selectedUser } = this.state;
      let emailTemp = this.props.emailTemplist;
      let indexOfDeleteEmailTemp = emailTemp.indexOf(selectedUser);
      this.props.deleteEmailTemp(emailTemp[indexOfDeleteEmailTemp].id);
      this.refs.deleteConfirmationDialog.close();
      let self = this;
      self.setState({ selectedUser: null });
   }
   render() {
      var parser = new DOMParser();

      const { selectedUser, editEmailTemp, add_err, udpate_err } = this.state;
      const emailTemplist = this.props.emailTemplist;
      const columns = [

         {
            name: 'ID',
            field: 'id'
         },
         {
            name: 'Template Type',
            field: 'template_type',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  
                     if(value.template_type == 1) {
                        return 'Email'
                     } else {
                        return 'Letter'
                     }
                 
               }
            }
         },
         {
            name: 'Template Name',
            field: 'template_name',
         },
         {
            name: 'Subject',
            field: 'template_subject',
         },
         /*{
            name: 'Content',
            field: 'template_content',
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  var regex = /(<([^>]+)>)/ig
                  var body = value.template_content
                  var result = body.replace(regex, "");
                  return (
                     result.substring(0, 30) + '...'
                  );
               }
            }
         },*/
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: false,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewEmailTempDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?<a href="javascript:void(0)" onClick={() => this.onEditEmailTemp(value)}><i className="ti-pencil"></i></a>:''}
                        {(this.state.currentPermision.delete)?<a href="javascript:void(0)" onClick={() => this.onDelete(value)}><i className="ti-close"></i></a>:''}
                     </div>
                  );
               }
            }
         }

      ];
      const options = {
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         rowsPerPageOptions: [10, 20, 50, 100],

         pagination: true,
         downloadOptions: { filename: 'template.csv' },
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewEmailTempModal={this.opnAddNewEmailTempModal.bind(this)} />:''
            );
         },
      };
      return (
         <div className="system-settings security-questions">
            <Helmet>
               <title>Health Partner | System | Email Templates</title>
               <meta name="description" content="Email Templates" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.Templates" />}
               match={this.props.match}
            />
            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={emailTemplist}
                  columns={columns}
                  options={options}
               />
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            <DeleteConfirmationDialog
               ref="deleteConfirmationDialog"
               title="Are You Sure Want To Delete?"
               message="This will delete template permanently."
               onConfirm={() => this.deleteUserPermanently()}
            />
            <Modal className="email_template_modal" isOpen={this.state.addNewEmailTempModal} toggle={() => this.onAddUpdateEmailTempModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateEmailTempModalClose()}>
                  {editEmailTemp === null ?
                     'Add Notification Template' : 'Update Notification Template'
                  }
               </ModalHeader>
               <ModalBody>
                  {editEmailTemp === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewEmailTempDetails={this.state.addNewEmailTempDetail}
                        onChangeAddNewEmailTempDetails={this.onChangeAddNewEmailTempDetails.bind(this)}
                        checkEmailTempExist={this.checkEmailTempExist.bind(this)}
                        modules={modules}
                        formats={formats}
                        onChangeAddNewEmailTempEditor={this.onChangeAddNewEmailTempEditor.bind(this)}
                        masterTemplateType={this.props.masterTempType}
                     />
                     :
                     <UpdateUserForm
                        editEmailTemp={editEmailTemp}
                        addErr={udpate_err}
                        onUpdateEmailTempDetails={this.onUpdateEmailTempDetails.bind(this)}
                        checkEmailTempExist={this.checkEmailTempExist.bind(this)}
                        modules={modules}
                        formats={formats}
                        onChangeAddNewEmailTempEditor={this.onChangeAddNewEmailTempEditor.bind(this)}
                        variable={this.props.emailTempVariable}
                        masterTemplateType={this.props.masterTempType}
                     />
                  }
               </ModalBody>
               <ModalFooter>
                  {editEmailTemp === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewEmailTemp()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateEmailTemp()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateEmailTempModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal className="email_template_modal" isOpen={this.state.openViewEmailTempDialog} toggle={() => this.onViewEmailTempModalClose()}>
               <ModalHeader toggle={() => this.onViewEmailTempModalClose()}>
                  {selectedUser !== null ? 'Notification Template View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedUser !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">

                                 <div className="prew quill-temp" id="quillTemp" dangerouslySetInnerHTML={{ __html: selectedUser.template_content }} />

                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>

            </Modal>

         </div>
      );
   }
}

const mapStateToProps = ({ authUser, EmailTemplateReducer }) => {
   const { user } = authUser;
   const { loading, emailTemplist, emailTempExist, isEdit, emailTempVariable, masterTempType } = EmailTemplateReducer;
   console.log(emailTempVariable)
   return { loading, emailTemplist, user, emailTempExist, isEdit, emailTempVariable, masterTempType }

}

export default connect(mapStateToProps, {
   checkEmailTempExist, getAllEmailTemp, insertEmailTemp, updateEmailTemp, deleteEmailTemp, getEmailTempVariable
})(EmailTemplates);