/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateMasterValueForm = ({ masterNameOption, updateErr, user, onUpdateMasterValueDetail, checkNameExist }) => (
    <Form>
    <div className="row">

        
        <div className="col-md-12">
        <FormGroup>
            <Label for="master_name">Master Name<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="md_id"
                id="master_name"
                placeholder=""
                defaultValue={user.md_id}
                onChange={(e) => onUpdateMasterValueDetail('md_id', e.target.value)}
            >
                <option value="">Select</option>
                {masterNameOption && masterNameOption.map((opt, key) => (
                    <option value={opt.md_name} key={key}>{opt.md_name}</option>
                ))}
                
            </Input>
            {(updateErr.md_id) ? <FormHelperText>{updateErr.md_id}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="master_value">Master Data Value<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="value"
                id="master_value"
                fullWidth
                variant="outlined"
                placeholder="Enter Value"
                value={user.value}
                error={(updateErr.value)?true:false}
                helperText={updateErr.value}
                onChange={(e) => onUpdateMasterValueDetail('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,user.mdv_id)}
            />
        </FormGroup>
        </div>
        <div className="col-md-12">
            <FormGroup>
                <Label for="master_desc">Descritpion</Label>
                <Input
                    type="textarea"
                    name="description"
                    id="master_desc"
                    value={user.description}
                    onChange={(e) => onUpdateMasterValueDetail('description', e.target.value)}
                    />
            </FormGroup>
        </div>
        <div className="col-md-12">
        <FormGroup tag="fieldset">
			<Label>Status</Label>
			<FormGroup check>
				<Label check>
					<Input
                        type="radio"
                        name="status"
                        value={1}
                        onChange={(e) => onUpdateMasterValueDetail('status', e.target.value)}
                        checked={(user.status == 1) ? true : false}
                        />{' '}
					Active
                </Label>
			</FormGroup>
			<FormGroup check>
				<Label check>
					<Input
                        type="radio"
                        name="status"
                        value={0}
                        onChange={(e) => onUpdateMasterValueDetail('status', e.target.value)}
                        checked={(user.status == 0) ? true : false}
                        />{' '}
					Inactive
                </Label>
			</FormGroup>
		</FormGroup>    
        </div>
    </div>
    </Form>
);

export default UpdateMasterValueForm;
