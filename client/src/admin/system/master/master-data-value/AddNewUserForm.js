/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewMasterValueForm = ({ masterNameOption, addErr, addNewMasterValueDetails, onChangeaddNewMasterValueDetail, checkNameExist }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="name">Master Name<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="name"
                id="name"
                placeholder=""
                onChange={(e) => onChangeaddNewMasterValueDetail('name', e.target.value)}
            >
                <option value="">Select</option>
                {masterNameOption && masterNameOption.map((opt, key) => (
                    <option value={opt.md_name} key={key}>{opt.md_name}</option>
                ))}
                
            </Input>
            {(addErr.name) ? <FormHelperText>{addErr.name}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup>
            <Label for="master_value">Master Data Value<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="value"
                id="master_value"
                fullWidth
                variant="outlined"
                placeholder="Enter Value"
                value={addNewMasterValueDetails.value}
                error={(addErr.value)?true:false}
                helperText={addErr.value}
                onChange={(e) => onChangeaddNewMasterValueDetail('value', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
            
        </FormGroup>
        </div>
        

        <div className="col-md-12">
            <FormGroup>
                <Label for="master_desc">Descritpion</Label>
                <Input
                    type="textarea" 
                    name="text" 
                    id="master_desc" 
                    placeholder="Enter Desccription"
                    value={addNewMasterValueDetails.description}
                    onChange={(e) => onChangeaddNewMasterValueDetail('description', e.target.value)}
                    />
            </FormGroup>
        </div>
        <div className="col-md-12">
        <FormGroup tag="fieldset">
			<Label>Status</Label>
			<FormGroup check>
				<Label check>
					<Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewMasterValueDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewMasterValueDetail('status', e.target.value)}
                        />{' '}
					Active
                </Label>
			</FormGroup>
			<FormGroup check>
				<Label check>
					<Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewMasterValueDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewMasterValueDetail('status', e.target.value)}
                        />{' '}
					Inactive
                </Label>
			</FormGroup>
		</FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewMasterValueForm;
