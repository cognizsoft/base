/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewMasterValueForm from './AddNewUserForm';

// update user form
import UpdateMasterValueForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isAlphaDigitUnderscoreDash, isEmpty, isLength, isContainWhiteSpace } from '../../../../validator/Validator';

import {
   masterDataValueList, masterValueList, updateMasterValue, deleteMasterValue, insertMasterValue, checkMasterValueExist
} from 'Actions';

import AddNewButton from './AddNewButton';

class MasterDataValue extends Component {

   state = {
      currentModule: 15,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      MasterVL: null, // initial user data
      selectedMasterVl: null, // selected user to perform operations
      loading: false, // loading activity
      addNewMasterModal: false, // add new user form modal
      addNewMasterValueDetail: {
         name: '',
         value: '',
         description: '',
         status: 1,
         checked: false
      },
      openViewMasterValueDialog: false, // view user dialog box
      editMasterValue: null,
      allSelected: false,
      selectedMasterValue: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         value: '',
         md_id: ''
      },
      add_err: {

      },

   }

   validateAddSubmit() {
      //console.log("adderorB"+this.state.add_err.value)
      return (
         this.state.add_err.value === '' && this.state.add_err.name === ''
      );
      //console.log("adderorB"+this.state.add_err.value)
   }

   validateUpdateSubmit() {
      return (
         this.state.err.value === '' && this.state.err.md_id === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdateMasterValueDetails(name, value)
      });
   }

   componentDidMount() {

      this.permissionFilter(this.state.currentModule);
      this.props.masterValueList();
      this.props.masterDataValueList();

   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   /**
    * Open Add New Master Value Modal
    */
   opnaddNewMasterModal() {
      this.setState({ addNewMasterModal: true });
   }

   /**
    * On Reload
    */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedMasterValue = 0;
      let MasterVL = this.state.MasterVL.map(userData => {
         if (userData.checked) {
            selectedMasterValue++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedMasterValue++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ MasterVL, selectedMasterValue });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewMasterValueDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Select Master Name field";
            } else {
               add_err[key] = '';
               if (this.state.addNewMasterValueDetail.value != '') {
                  this.checkNameExist(this.state.addNewMasterValueDetail.value, '', value)
               }
            }
            break;
         case 'value':
            if (isEmpty(value)) {
               add_err[key] = "Value field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewMasterValueDetail: {
            ...this.state.addNewMasterValueDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewMasterValue() {
      const { name, value, description, status } = this.state.addNewMasterValueDetail;
      if (name !== '' && value !== '') {
         let MasterVL = this.props.master_value;


         this.props.insertMasterValue(this.state.addNewMasterValueDetail);


         this.setState({ addNewMasterModal: false, loading: true });
         let self = this;
         let user = {
            name: '',
            value: '',
            description: '',
            status: 1,
            checked: false
         }
         setTimeout(() => {
            //this.setState({addNewMasterValueDetail:''})
            self.setState({ loading: false, addNewMasterValueDetail: user });
            NotificationManager.success('Record added successfully!');
         }, 2000);
      }
   }

   /**
    * View Master Value Detail Hanlder
    */
   viewMasterValueDetail(data) {
      this.setState({ openViewMasterValueDialog: true, selectedMasterVl: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewMasterValueModalClose() {
      this.setState({ openViewMasterValueDialog: false, selectedMasterVl: null })
   }

   /**
    * On Edit Master Value
    */
   onEditMasterValue(user) {
      this.setState({ addNewMasterModal: true, editMasterValue: user });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdateMasterValueModalClose = () => {
      let upR = { value: '', md_id: '' }
      let addR = {}
      let addNewMasterValueDetail = {
         name: '',
         value: '',
         description: '',
         status: 1,
         checked: false
      };
      this.setState({ addNewMasterModal: false, editMasterValue: null, err: upR, add_err: addR, addNewMasterValueDetail: addNewMasterValueDetail })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateMasterValueDetails(fieldName, value) {

      let { err } = this.state;
      switch (fieldName) {
         case 'value':
            if (isEmpty(value)) {
               err[fieldName] = "Value can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'md_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Master Name";
            } else {
               err[fieldName] = '';
               if (this.state.editMasterValue.value != '') {
                  this.checkNameExist(this.state.editMasterValue.value, this.state.editMasterValue.mdv_id, value)
               }
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editMasterValue: {
            ...this.state.editMasterValue,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateMasterValue() {
      const { editMasterValue } = this.state;
      //console.log(editMasterValue)
      let indexOfUpdateMasterValue = '';
      let MasterVL = this.props.master_value;
      //console.log(MasterVL)
      for (let i = 0; i < MasterVL.length; i++) {
         const user = MasterVL[i];

         if (user.mdv_id === editMasterValue.mdv_id) {
            indexOfUpdateMasterValue = i
         }
      }

      this.props.updateMasterValue(editMasterValue);


      MasterVL[indexOfUpdateMasterValue] = editMasterValue;
      this.setState({ loading: true, editMasterValue: null, addNewMasterModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ MasterVL, loading: false });
         NotificationManager.success('Value updated successfully');
      }, 2000);
   }

   //Select All user
   onSelectAllUser(e) {
      const { selectedMasterValue, MasterVL } = this.state;
      let selectAll = selectedMasterValue < MasterVL.length;
      if (selectAll) {
         let selectAllUsers = MasterVL.map(user => {
            user.checked = true
            return user
         });
         this.setState({ MasterVL: selectAllUsers, selectedMasterValue: selectAllUsers.length })
      } else {
         let unselectedMasterValue = MasterVL.map(user => {
            user.checked = false
            return user;
         });
         this.setState({ selectedMasterValue: 0, MasterVL: unselectedMasterValue });
      }
   }

   /*
   * Title :- checkNameExist
   * Descrpation :- This function use for check name exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   checkNameExist(value, md_id, type) {
      if (value != '') {
         let { add_err } = this.state;
         let { err } = this.state;
         add_err['value'] = '';
         err['value'] = '';
         this.setState({ add_err: add_err });
         this.setState({ err: err });
         type = (type) ? type : this.state.addNewMasterValueDetail.name;
         type = (type) ? type : (this.state.editMasterValue) ? this.state.editMasterValue.md_id : '';

         this.props.checkMasterValueExist(value, md_id, type);
      }

   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { err } = this.state;
      (nextProps.valueExist && nextProps.isEdit == 0) ? add_err['value'] = "Data value already exists" : '';
      (nextProps.valueExist && nextProps.isEdit == 1) ? err['value'] = "Data value already exists" : '';
      this.setState({ add_err: add_err });
      this.setState({ err: err });

   }


   render() {
      const { add_err, err, MasterVL, loading, selectedMasterVl, editMasterValue, allSelected, selectedMasterValue } = this.state;
      const master_name_option = this.props.master_data_value
      const masterValue = this.props.master_value;
      const columns = [

         {
            name: 'ID',
            field: 'mdv_id'
         },
         {
            name: 'Master Data Name',
            field: 'md_id',
         },
         {
            name: 'Master Data Value',
            field: 'value'
         },         
         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: true,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  //console.log(value)
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewMasterValueDetail(value)}><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditMasterValue(value)}><i className="ti-pencil"></i></a> : ''}
                     </div>
                  );
               }
            }
         }

      ];

      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               (this.state.currentPermision.add) ? <AddNewButton opnaddNewMasterModal={this.opnaddNewMasterModal.bind(this)} /> : ''
            );
         }
      }
      return (
         <div className="master master-data-value">
            <Helmet>
               <title>Health Partner | Master | Master Data Value</title>
               <meta name="description" content="Master Data Value" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.systemMasterDataValue" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable

                  data={masterValue}
                  columns={columns}
                  options={options}


               />
            </RctCollapsibleCard>



            <Modal isOpen={this.state.addNewMasterModal} toggle={() => this.onAddUpdateMasterValueModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateMasterValueModalClose()}>
                  {editMasterValue === null ?
                     'Add Master Data Value' : 'Update Master Data Value'
                  }
               </ModalHeader>
               <ModalBody>
                  {editMasterValue === null ?
                     <AddNewMasterValueForm
                        masterNameOption={master_name_option}
                        addErr={add_err}
                        addNewMasterValueDetails={this.state.addNewMasterValueDetail}
                        onChangeaddNewMasterValueDetail={this.onChangeaddNewMasterValueDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                     : <UpdateMasterValueForm masterNameOption={master_name_option} updateErr={err} user={editMasterValue} onUpdateMasterValueDetail={this.onUpdateMasterValueDetails.bind(this)} checkNameExist={this.checkNameExist.bind(this)} />
                  }
               </ModalBody>
               <ModalFooter>
                  {editMasterValue === null ?
                     <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.addNewMasterValue()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.updateMasterValue()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateMasterValueModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewMasterValueDialog} toggle={() => this.onViewMasterValueModalClose()}>
               <ModalHeader toggle={() => this.onViewMasterValueModalClose()}>
                  {selectedMasterVl !== null ? 'Master Data Value View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedMasterVl !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">

                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Master Name:</span> <span className="second-colmn">{selectedMasterVl.md_id}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Master Data Value:</span> <span className="second-colmn">{selectedMasterVl.value}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Description:</span> <span className="second-colmn">{selectedMasterVl.description}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedMasterVl.status == 1) ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>

         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ masterValue, authUser }) => {
   //console.log(masterValue)

   const { master_value, loading, master_data_value, valueExist, isEdit } = masterValue;
   const user = authUser.user;
   return { master_value, loading, user, master_data_value, valueExist, isEdit }

}

export default connect(mapStateToProps, {
   masterDataValueList, masterValueList, updateMasterValue, deleteMasterValue, insertMasterValue, checkMasterValueExist
})(MasterDataValue);