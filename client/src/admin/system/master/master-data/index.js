/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import MaterialDatatable from "material-datatable";
import CustomToolbar from "./CustomToolbar";
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import { NotificationManager } from 'react-notifications';

// add new user form
import AddNewUserForm from './AddNewUserForm';

// update user form
import UpdateUserForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import { isMaster, isEmpty } from '../../../../validator/Validator';
import {
   insertMaster, masterList, updateMaster, checkNameExist, changeMasterPage
} from 'Actions';

class Master extends Component {

   state = {
      currentModule: 14,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      users: null, // initial user data
      selectedMaster: null, // selected user to perform operations
      loading: false, // loading activity
      addNewMasterModal: false, // add new user form modal
      addViewUserModal: false, // add view user form modal
      addNewMasterDetail: {
         md_name: '',
         edit_allowed: 0,
         status: 1,
      },
      add_err: {
      },
      err: {
         md_name: '',
         edit_allowed: ''
      },
      openViewMasterDialog: false, // view user dialog box
      editMaster: null,
      allSelected: false,
      selectedUsers: 0,
      currentPage: 0
   }
   /*
   * Title :- componentDidMount
   * Descrpation :- This function use for call function on component load
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      this.props.masterList();
   }
   /*
   * Title :- permissionFilter
   * Descrpation :- This function use for filter permission according to role
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- May 14,2019
   */
   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }
   /*
   * Title :- updateMaster
   * Descrpation :- This function use update record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   updateMaster() {
      const { editMaster } = this.state;
      let indexOfUpdateUser = '';
      let masters = this.props.master;
      for (let i = 0; i < masters.length; i++) {
         const singleRecord = masters[i];
         if (singleRecord.md_id === editMaster.md_id) {
            indexOfUpdateUser = i
         }
      }
      this.props.updateMaster(editMaster);
      masters[indexOfUpdateUser] = editMaster;
      this.setState({ loading: true, editMaster: null, addNewMasterModal: false });
      let self = this;
      setTimeout(() => {
         self.setState({ masters, loading: false });
         NotificationManager.success('Record updated successfully!');
      }, 2000);
   }
   /*
   * Title :- addNewMaster
   * Descrpation :- This function use for add new record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   addNewMaster() {
      const { md_name, status } = this.state.addNewMasterDetail;
      if (md_name !== '' && status !== '') {
         //let users = this.props.user_type;


         this.props.insertMaster(this.state.addNewMasterDetail);


         this.setState({ addNewMasterModal: false, loading: true });
         let self = this;
         let emptyState = {
            md_name: '',
            edit_allowed: 0,
            status: 1,
         }
         setTimeout(() => {
            //this.setState({addNewUserDetail:''})
            self.setState({ loading: false, addNewMasterDetail: emptyState });
            NotificationManager.success('Record added successfully!');
         }, 2000);

      }
   }
   /*
   * Title :- validateAddSubmit
   * Descrpation :- This function use for check error exits or not on add new record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   validateAddSubmit() {
      
      return (
         //this.state.add_err.md_name === '' && (this.state.add_err.edit_allowed === '' || this.state.add_err.edit_allowed === undefined)
         this.state.add_err.md_name === ''

      );

   }
   /*
   * Title :- validateUpdateSubmit
   * Descrpation :- This function use for check error exits or not on update exist record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   validateUpdateSubmit() {
      return (
         this.state.err.md_name === '' && (this.state.err.edit_allowed === '' || this.state.err.edit_allowed === undefined)
      );
   }
   /*
   * Title :- onUpdateMasterDetails
   * Descrpation :- This function use for check validation on update record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onUpdateMasterDetails(fieldName, value) {
      let { err } = this.state;
      switch (fieldName) {
         case 'md_name':
            if (isEmpty(value)) {
               err[fieldName] = "Name can't be blank";
            } else if (!isMaster(value)) {
               add_err[key] = "Please enter a valid name. only allow a-z A-Z 0-9 _ - and space";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'edit_allowed':
            if (isEmpty(value)) {
               err[fieldName] = "Please select Editable";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });

      this.setState({
         editMaster: {
            ...this.state.editMaster,
            [fieldName]: value
         }
      });
   }
   /*
   * Title :- checkNameExist
   * Descrpation :- This function use for check name exist or not in both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   checkNameExist(value, md_id) {
      this.props.checkNameExist(value, md_id);
   }
   /*
   * Title :- componentWillReceiveProps
   * Descrpation :- This function use for check name exist or not and according to response update error state 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   componentWillReceiveProps(nextProps) {
      let { add_err } = this.state;
      let { err } = this.state;
      (nextProps.nameExist && nextProps.isEdit == 0) ? add_err['md_name'] = "Name already exists" : '';
      (nextProps.nameExist && nextProps.isEdit == 1) ? err['md_name'] = "Name already exists" : '';
      this.setState({ add_err: add_err });
      this.setState({ err: err });

   }
   /*
   * Title :- onChangeAddNewMasterDetails
   * Descrpation :- This function use add new record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onChangeAddNewMasterDetails(key, value) {

      let { add_err } = this.state;
      switch (key) {
         case 'md_name':
            if (isEmpty(value)) {
               add_err[key] = "Name can't be blank";
            } else if (!isMaster(value)) {
               add_err[key] = "Please enter a valid name. only allow a-z A-Z 0-9 _ - and space";
            } else {
               add_err[key] = '';
            }
            break;
         case 'edit_allowed':
            if (isEmpty(value)) {
               add_err[key] = "Please select Editable";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewMasterDetail: {
            ...this.state.addNewMasterDetail,
            [key]: value
         }
      });
   }

	/*
   * Title :- opnAddNewMasterModal
   * Descrpation :- This function use for open popup for add new record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   opnAddNewMasterModal() {
      this.setState({ addNewMasterModal: true });
   }

	/*
   * Title :- onReload
   * Descrpation :- This function use for reload table data
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onReload() {
      this.setState({ loading: true });
      let self = this;
      setTimeout(() => {
         self.setState({ loading: false });
      }, 2000);
   }

   /*
   * Title :- viewMasterDetail
   * Descrpation :- This function use for view particular record
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   viewMasterDetail(data) {
      this.setState({ openViewMasterDialog: true, selectedMaster: data });
   }

	/*
   * Title :- onEditMaster
   * Descrpation :- This function use for set edit record details in state
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onEditMaster(data) {
      this.setState({ addNewMasterModal: true, editMaster: data });
   }

	/*
   * Title :- onAddUpdateUserModalClose
   * Descrpation :- This function use for close popup box on both case
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onAddUpdateUserModalClose() {
      this.setState({ addNewMasterModal: false, editMaster: null })
      let err = {
         md_name: '',
         edit_allowed: ''
      }
      this.setState({ err: err });
      let add_err = {}
      this.setState({ add_err: add_err });
      let addNewMasterDetail = {
         md_name: '',
         edit_allowed: 0,
         status: 1,
      }
      this.setState({ addNewMasterDetail: addNewMasterDetail });
   }

   /*
   * Title :- onViewUserModalClose
   * Descrpation :- This function use for close view single popup box
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   onViewUserModalClose = () => {
      this.setState({ openViewMasterDialog: false, selectedMaster: null })
   }

	/*
   * Title :- render
   * Descrpation :- This function use for return final html 
   * Author : Cognizsoft and Ramesh Kumar
   * Date :- April 11,2019
   */
   render() {
      const { add_err, err, users, loading, selectedMaster, editMaster, allSelected, selectedUsers } = this.state;
      const master = this.props.master;
      const columns = [
         { name: 'ID', field: 'md_id', },
         { name: 'Master Data Name', field: 'md_name', },
         {
            name: 'Status',
            field: 'status',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <React.Fragment>
                        <span className="d-flex justify-content-start">
                           <div className="status">
                              <span className="d-block">{(value.status == '1') ? 'Active' : 'Inactive'}</span>
                           </div>
                        </span>
                     </React.Fragment>
                  )
               },
               customValue: (value, tableMeta, updateValue) => value.status,
               customSortValue: (value, tableMeta, updateValue) => value.status,
            }
         },
         {
            name: 'Editable',
            field: 'edit_allowed',
            options: {
               noHeaderWrap: true,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (value.edit_allowed == '1') ? 'Yes' : 'No'
               },
               customValue: (value, tableMeta, updateValue) => value.edit_allowed,
               customSortValue: (value, tableMeta, updateValue) => value.edit_allowed
            }
         },
         {
            name: 'Action',
            field: 'md_id',
            options: {
               noHeaderWrap: true,
               filter: false,
               sort: false,
               download: false,
               customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                     <React.Fragment>
                        <span className="list-action">
                        {(this.state.currentPermision.view)?<a href="javascript:void(0)" onClick={() => this.viewMasterDetail(value)}><i className="ti-eye"></i></a>:''}
                        {(this.state.currentPermision.edit)?(value.edit_allowed == 1) ? <a href="javascript:void(0)" onClick={() => this.onEditMaster(value)}><i className="ti-pencil"></i></a> : '':''}
                        </span>
                     </React.Fragment>
                  )
               },

            }
         },


      ];

      const options = {
         
         filter: true,
         filterType: 'dropdown',
         selectableRows: false,
         download: false,
         /*count: this.props.total,
         page: this.state.currentPage,*/
         rowsPerPageOptions: [10, 20, 50, 100],
         customToolbar: () => {
            return (
               (this.state.currentPermision.add)?<CustomToolbar opnAddNewMasterModal={this.opnAddNewMasterModal.bind(this)} />:''
            );
         },
         pagination: true,
         downloadOptions: { filename: 'MasterModule.csv' },
         /*serverSide: true,
         onTableChange: (action, tableState) => {

            //console.log(action, tableState);
            // a developer could react to change on an action basis or
            // examine the state as a whole and do whatever they want
            let option = {}
            switch (action) {
               case 'tableState':
                  this.setState({ currentPage: tableState.page })
                  option['page'] = tableState.page;
                  option['searchText'] = tableState.searchText;
                  option['rowsPerPage'] = tableState.rowsPerPage;
                  this.props.changeMasterPage(option);
                  break;
               case 'search':
                  this.setState({ currentPage: 0 })
                  option['page'] = 0;
                  option['searchText'] = tableState.searchText;
                  option['rowsPerPage'] = tableState.rowsPerPage;
                  this.props.changeMasterPage(option);
                  break;
               case 'changeRowsPerPage':
                  this.setState({ currentPage: 0 })
                  option['page'] = 0;
                  option['searchText'] = tableState.searchText;
                  option['rowsPerPage'] = tableState.rowsPerPage;
                  this.props.changeMasterPage(option);
                  break;
               case 'filterChange':
                  console.log(tableState.filterList[1])
                  this.setState({ currentPage: 0 })
                  option['page'] = 0;
                  option['searchText'] = tableState.searchText;
                  option['rowsPerPage'] = tableState.rowsPerPage;
                  option['filterList'] = JSON.stringify(tableState.filterList);
                  this.props.changeMasterPage(option);
                  break;

            }
   }*/
      };
      return (
         <div className="master master-data">
            <Helmet>
               <title>Health Partner | Master | Master Data</title>
               <meta name="description" content="Master Data" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.masterData" />}
               match={this.props.match}
            />
            <RctCollapsibleCard fullBlock>
               <MaterialDatatable
                  data={master}
                  columns={columns}
                  options={options}
               />
               {loading &&
                  <RctSectionLoader />
               }
               {this.props.loading &&
                  <RctSectionLoader />
               }
            </RctCollapsibleCard>
            

            <Modal isOpen={this.state.addNewMasterModal} toggle={() => this.onAddUpdateUserModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdateUserModalClose()}>
                  {editMaster === null ?
                     'Add Master Data' : 'Update Master Data'
                  }
               </ModalHeader>
               <ModalBody>
                  {editMaster === null ?
                     <AddNewUserForm
                        addErr={add_err}
                        addNewMasterDetails={this.state.addNewMasterDetail}
                        onChangeAddNewMasterDetails={this.onChangeAddNewMasterDetails.bind(this)}
                        checkNameExist={this.checkNameExist.bind(this)}
                     />
                     : <UpdateUserForm updateErr={err} masterdata={editMaster} onUpdateMasterDetail={this.onUpdateMasterDetails.bind(this)} checkNameExist={this.checkNameExist.bind(this)} />
                  }
               </ModalBody>
               <ModalFooter>
                  {editMaster === null ?
                     <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        onClick={() => this.addNewMaster()}
                        disabled={!this.validateAddSubmit()}>Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className="text-white"
                        disabled={!this.validateUpdateSubmit()}
                        onClick={() => this.updateMaster()}>Update</Button>
                  }
                  {' '}
                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdateUserModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>


            <Modal isOpen={this.state.openViewMasterDialog} toggle={() => this.onViewUserModalClose()}>
               <ModalHeader toggle={() => this.onViewUserModalClose()}>
                  {selectedMaster !== null ? 'Master Data View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedMaster !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Master Data Name:</span> <span className="second-colmn ">{selectedMaster.md_name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{(selectedMaster.status == 1) ? 'Active' : 'Inactive'}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Editable:</span> <span className="second-colmn">{(selectedMaster.edit_allowed == 1) ? 'Yes' : 'No'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>

               </ModalFooter>
            </Modal>
         </div>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser, masterList }) => {
   const { master, loading, nameExist, isEdit, total } = masterList;
   const user = authUser.user;
   return { master, loading, nameExist, isEdit, total, user }

}

export default connect(mapStateToProps, {
   insertMaster, masterList, updateMaster, checkNameExist, changeMasterPage
})(Master);