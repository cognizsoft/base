/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const UpdateUserForm = ({ updateErr, masterdata, onUpdateMasterDetail, checkNameExist  }) => (
    <Form>
    <div className="row">
        <div className="col-md-12">
        <FormGroup>
            <Label for="name">Master Data Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="md_name"
                id="md_name"
                fullWidth
                variant="outlined"
                placeholder="Enter Name"
                value={masterdata.md_name}
                error={(updateErr.md_name)?true:false}
                helperText={updateErr.md_name}
                onChange={(e) => onUpdateMasterDetail('md_name', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,masterdata.md_id)}
            />
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="editable">Editable</Label>
            <Input
                type="select"
                name="edit_allowed"
                id="edit_allowed"
                defaultValue={masterdata.edit_allowed}
                error={(updateErr.edit_allowed)? updateErr.edit_allowed : ''} 
                onChange={(e) => onUpdateMasterDetail('edit_allowed', e.target.value)}
            >
                <option value="">Select</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
                
            </Input>
            {(updateErr.edit_allowed) ? <FormHelperText>{updateErr.edit_allowed}</FormHelperText> :''}
        </FormGroup>
        </div>
        <div className="col-md-12">
        <FormGroup tag="fieldset">
			<Label>Status</Label>
			<FormGroup check>
				<Label check>
					<Input 
                    type="radio" 
                    name="status" 
                    value={1}
                    onChange={(e) => onUpdateMasterDetail('status', e.target.value)} 
                    checked={(masterdata.status == 1) ? true : false}
                    />{' '}
					Active
                </Label>
			</FormGroup>
			<FormGroup check>
				<Label check>
					<Input 
                    type="radio" 
                    name="status" 
                    value={0}
                    onChange={(e) => onUpdateMasterDetail('status', e.target.value)} 
                    checked={(masterdata.status == 0) ? true : false}
                    />{' '}
					Inactive
                </Label>
			</FormGroup>
		</FormGroup>    
        </div>
    </div>
    </Form>
);

export default UpdateUserForm;
