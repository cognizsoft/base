/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
const AddNewUserForm = ({ addErr, addNewMasterDetails, onChangeAddNewMasterDetails, checkNameExist }) => (
    <Form>
    <div className="row">
        <div className="col-md-12">
        <FormGroup>
            <Label for="name">Master Data Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="md_name"
                id="md_name"
                fullWidth
                variant="outlined"
                placeholder="Enter Name"
                value={addNewMasterDetails.md_name}
                error={(addErr.md_name)?true:false}
                helperText={addErr.md_name}
                onChange={(e) => onChangeAddNewMasterDetails('md_name', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
            
        </FormGroup>
        </div>
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="editable">Editable</Label>
            
            <Input
                type="select"
                name="edit_allowed"
                id="editable"
                placeholder="" 
                onChange={(e) => onChangeAddNewMasterDetails('edit_allowed', e.target.value)}
            >
                <option value="">Select</option>
				<option value="1">Yes</option>
				<option value="0">No</option>
				
			</Input>
            {(addErr.edit_allowed) ? <FormHelperText>{addErr.edit_allowed}</FormHelperText> :''}
        </FormGroup>
        </div>
        <div className="col-md-12">
        <FormGroup tag="fieldset">
			<Label>Status</Label>
			<FormGroup check>
				<Label check>
                    <Input 
                        type="radio" 
                        name="status" 
                        value={1}
                        onChange={(e) => onChangeAddNewMasterDetails('status', e.target.value)} 
                        checked={(addNewMasterDetails.status == 1) ? true : false}
                    />{' '}
					Active
                </Label>
			</FormGroup>
			<FormGroup check>
				<Label check>
                    <Input 
                        type="radio" 
                        name="status" 
                        value={0}
                        onChange={(e) => onChangeAddNewMasterDetails('status', e.target.value)} 
                        checked={(addNewMasterDetails.status == 0) ? true : false}
                    />{' '}
					Inactive
                </Label>
			</FormGroup>
		</FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
