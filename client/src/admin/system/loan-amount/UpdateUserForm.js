/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ updateErr, user, onUpdateUserDetail, checkNameExist }) => (

    <Form>
        <FormGroup>
            <Label for="loan_amount">Loan Amount<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="loan_amount"
                id="loan_amount"
                placeholder="Enter Term Month"
                fullWidth
                variant="outlined"
                value={user.loan_amount}
                error={(updateErr.loan_amount)?true:false}
                helperText={updateErr.loan_amount}
                onChange={(e) => onUpdateUserDetail('loan_amount', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,user.mdv_id)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="risk_factor">Risk Factor<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="risk_factor"
                id="risk_factor"
                placeholder="Enter Risk Factor"
                fullWidth
                variant="outlined"
                value={user.risk_factor}
                error={(updateErr.risk_factor)?true:false}
                helperText={updateErr.risk_factor}
                onChange={(e) => onUpdateUserDetail('risk_factor', e.target.value)}
            />
        </FormGroup>
       <FormGroup>
            <Label for="loan_amount_desc">Description</Label>
                <Input
                    type="textarea"
                    name="loan_amount_desc"
                    id="loan_amount_desc"
                    placeholder="Enter Term Month Desc"
                    value={user.loan_amount_desc}
                    onChange={(e) => onUpdateUserDetail('loan_amount_desc', e.target.value)}
                />
        </FormGroup>
        <Label for="status">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(user.status == 1) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(user.status == 0) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
        
        
    </Form>
);

export default UpdateUserForm;
