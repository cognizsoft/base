import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";

const defaultToolbarStyles = {
  iconButton: {
  },
};

class AddNewButton extends React.Component {
  
  handleClick = () => {
    console.log("clicked on icon!");
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip title="">
          <a href="javascript:void(0)" onClick={() => this.props.opnAddNewUserModal()} color="primary" className="caret btn-sm mr-10">Add Loan Amount <i className="zmdi zmdi-plus"></i></a>
        </Tooltip>
      </React.Fragment>
    );
  }

}

export default withStyles(defaultToolbarStyles, { name: "AddNewButton" })(AddNewButton);