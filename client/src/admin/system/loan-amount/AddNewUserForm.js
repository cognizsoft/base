/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewUserDetails, onChangeAddNewUserDetails, checkNameExist }) => (
    <Form>
       
  
        <FormGroup>
            <Label for="loan_amount">Loan Amount<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="loan_amount"
                id="loan_amount"
                fullWidth
                variant="outlined"
                placeholder="Enter Term Month"
                value={addNewUserDetails.loan_amount}
                error={(addErr.loan_amount)?true:false}
                helperText={addErr.loan_amount}
                onChange={(e) => onChangeAddNewUserDetails('loan_amount', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="risk_factor">Risk Factor<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="risk_factor"
                id="risk_factor"
                fullWidth
                variant="outlined"
                placeholder="Enter Risk Factor"
                value={addNewUserDetails.risk_factor}
                error={(addErr.risk_factor)?true:false}
                helperText={addErr.risk_factor}
                onChange={(e) => onChangeAddNewUserDetails('risk_factor', e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="user_type_desc">Description</Label>
                <Input
                    type="textarea"
                    name="loan_amount_desc"
                    id="loan_amount_desc"
                    placeholder="Enter Term Month Desc"
                    value={addNewUserDetails.loan_amount_desc}
                    onChange={(e) => onChangeAddNewUserDetails('loan_amount_desc', e.target.value)}
                />
        </FormGroup>
        <Label for="status">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(addNewUserDetails.status == 1) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(addNewUserDetails.status == 0) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
    </Form>
);

export default AddNewUserForm;
