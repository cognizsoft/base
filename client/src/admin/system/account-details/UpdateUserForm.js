/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const UpdateUserForm = ({ updateErr, user, onUpdateUserDetail, checkNameExist }) => (

    <Form>
        <FormGroup>
            <Label for="score">Score<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="score"
                id="score"
                placeholder="Enter Score"
                fullWidth
                variant="outlined"
                value={user.score}
                error={(updateErr.score)?true:false}
                helperText={updateErr.score}
                onChange={(e) => onUpdateUserDetail('score', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value,user.id)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="apr">Interest Rate (%)<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="apr"
                id="apr"
                placeholder="Enter Interest Rate (%)"
                fullWidth
                variant="outlined"
                value={user.apr}
                error={(updateErr.apr)?true:false}
                helperText={updateErr.apr}
                onChange={(e) => onUpdateUserDetail('apr', e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="risk_factor">Risk Factor<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="risk_factor"
                id="risk_factor"
                placeholder="Enter Risk Factor"
                fullWidth
                variant="outlined"
                value={user.risk_factor}
                error={(updateErr.risk_factor)?true:false}
                helperText={updateErr.risk_factor}
                onChange={(e) => onUpdateUserDetail('risk_factor', e.target.value)}
            />
        </FormGroup>
        <Label for="status">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(user.status == 1) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(user.status == 0) ? true : false}
                onChange={(e) => onUpdateUserDetail('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
        
        
    </Form>
);

export default UpdateUserForm;
