/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';

const AddNewUserForm = ({ addErr, addNewUserDetails, onChangeAddNewUserDetails, checkNameExist }) => (
    <Form>
       
  
        <FormGroup>
            <Label for="score">Score<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="score"
                id="score"
                fullWidth
                variant="outlined"
                placeholder="Enter Score"
                value={addNewUserDetails.score}
                error={(addErr.score)?true:false}
                helperText={addErr.score}
                onChange={(e) => onChangeAddNewUserDetails('score', e.target.value)}
                onKeyUp={(e) => checkNameExist(e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="apr">Interest Rate (%)<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="apr"
                id="apr"
                fullWidth
                variant="outlined"
                placeholder="Enter Interest Rate (%)"
                value={addNewUserDetails.apr}
                error={(addErr.apr)?true:false}
                helperText={addErr.apr}
                onChange={(e) => onChangeAddNewUserDetails('apr', e.target.value)}
            />
        </FormGroup>
        <FormGroup>
            <Label for="risk_factor">Risk Factor<span className="required-field">*</span></Label><br/>
            <TextField
                type="text"
                name="risk_factor"
                id="risk_factor"
                fullWidth
                variant="outlined"
                placeholder="Enter Risk Factor"
                value={addNewUserDetails.risk_factor}
                error={(addErr.risk_factor)?true:false}
                helperText={addErr.risk_factor}
                onChange={(e) => onChangeAddNewUserDetails('risk_factor', e.target.value)}
            />
        </FormGroup>
        <Label for="status">Status</Label><br/>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={1}
                checked={(addNewUserDetails.status == 1) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Active
            </Label>
        </FormGroup>
        <FormGroup check>
            <Label check>
                <Input
                type="radio"
                name="status"
                value={0}
                checked={(addNewUserDetails.status == 0) ? true : false}
                onChange={(e) => onChangeAddNewUserDetails('status', e.target.value)}
                />
                Inactive
            </Label>
        </FormGroup>
    </Form>
);

export default AddNewUserForm;
