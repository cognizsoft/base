/**
 * User Management Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MaterialDatatable from "material-datatable";
import { connect } from 'react-redux';
import {
   Pagination,
   PaginationItem,
   PaginationLink,
   Modal,
   ModalHeader,
   ModalBody,
   ModalFooter,
   Badge
} from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { NotificationManager } from 'react-notifications';
import Avatar from '@material-ui/core/Avatar';

// api
import api from 'Api';

// delete confirmation dialog
import DeleteConfirmationDialog from 'Components/DeleteConfirmationDialog/DeleteConfirmationDialog';

// add new user form
import AddNewPaybackForm from './AddNewUserForm';

// update user form
import UpdatePaybackForm from './UpdateUserForm';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import { isEmpty, isObjectEmpty, isNumeric } from '../../../validator/Validator';

import {
   proceduresList, insertProcedures, updateProcedures
} from 'Actions';

import AddNewButton from './AddNewButton';

class Payback extends Component {

   state = {
      currentModule: 23,
      currentPermision: {
         add: false,
         edit: false,
         view: false,
         delete: false
      },
      all: false,
      Payback: null, // initial user data
      selectedProcedures: null, // selected user to perform operations

      addNewProceduresModal: false, // add new user form modal
      addNewProceduresDetail: {
         speciality_type: '',
         name: '',
         code: '',
         status: 1,
      },
      openViewProceduresDialog: false, // view user dialog box
      editProcedures: null,
      allSelected: false,
      //selectedRiskFactor: 0,
      username: '',
      password: '',
      updateForm: {},
      err: {
         spec_id: '',
         procedure_name: '',
         procedure_code: ''
      },
      add_err: {

      },
      ruleExist: false,
   }

   validateAddSubmit() {
      return (
         this.state.add_err.speciality_type === '' &&
         this.state.add_err.name === '' &&
         this.state.add_err.code === ''
      );
   }

   validateUpdateSubmit() {
      return (
         this.state.err.spec_id === '' &&
         this.state.err.procedure_name === '' &&
         this.state.err.procedure_code === ''
      );
   }

   handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      let { updateForm } = this.state;
      updateForm[name] = value;

      this.setState({
         updateForm: updateForm
      }, function () {
         this.onUpdatePaybackDetails(name, value)
      });
   }

   componentDidMount() {
      this.permissionFilter(this.state.currentModule);
      //this.props.PaybackList();
      this.props.proceduresList();

   }

   permissionFilter = (name) => {
      let per = JSON.parse(this.props.user);

      let newUser = per.user_permission.filter(
         function (per) { return per.description == name }
      );
      this.setState({ currentPermision: { add: newUser[0].create_flag, edit: newUser[0].edit_flag, view: newUser[0].view_flag, delete: newUser[0].delete_flag } });
   }


   /**
    * Open Add New Master Value Modal
    */
   opnaddNewProceduresModal() {
      this.setState({ addNewProceduresModal: true });
   }



   /**
    * On Select User
    */
   onSelectUser(user) {
      user.checked = !user.checked;
      let selectedPayback = 0;
      let Payback = this.state.Payback.map(userData => {
         if (userData.checked) {
            selectedPayback++;
         }
         if (userData.id === user.id) {
            if (userData.checked) {
               selectedPayback++;
            }
            return user;
         } else {
            return userData;
         }
      });
      this.setState({ Payback, selectedPayback });
   }

   /**
    * On Change Add New User Details
    */
   onChangeaddNewProceduresDetails(key, value) {
      //console.log(this.state.addNewMasterValueDetail)
      let { add_err } = this.state;
      switch (key) {
         case 'speciality_type':
            if (isEmpty(value)) {
               add_err[key] = "Select Payback Type field";
            } else {
               add_err[key] = '';
            }
            break;
         case 'name':
            if (isEmpty(value)) {
               add_err[key] = "Name field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         case 'code':
            if (isEmpty(value)) {
               add_err[key] = "Code field can't be blank";
            } else {
               add_err[key] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ add_err: add_err });

      this.setState({
         addNewProceduresDetail: {
            ...this.state.addNewProceduresDetail,
            [key]: value
         }
      });
   }

   /**
    * Add New Master Value
    */
   addNewProcedures() {
      console.log(this.state.addNewProceduresDetail);

      const { speciality_type, name, code, status } = this.state.addNewProceduresDetail;
      //console.log( this.props.proceduresData)
      const filterType = this.props.proceduresData.filter(x =>
         x.spec_id == speciality_type &&
         x.procedure_name == name &&
         x.procedure_code == code
      );


      if (isObjectEmpty(filterType)) {
         this.setState({ ruleExist: true })
         return false;
      }

      this.props.insertProcedures(this.state.addNewProceduresDetail);


      this.setState({ addNewProceduresModal: false });
      let self = this;
      let procedure = {
         speciality_type: '',
         name: '',
         code: '',
         status: 1,
      }
      setTimeout(() => {
         self.setState({ addNewProceduresDetail: procedure });
      }, 2000);

   }

   /**
    * View Master Value Detail Hanlder
    */
   viewProceduresDetail(data) {
      console.log(data)
      this.setState({ openViewProceduresDialog: true, selectedProcedures: data });
   }

   /**
    * On View Master Value Modal Close
    */
   onViewProceduresModalClose() {
      this.setState({ openViewProceduresDialog: false, selectedProcedures: null })
   }

   /**
    * On Edit Master Value
    */
   onEditProcedures(editProcedures) {
      //console.log(editProcedures)
      this.setState({ addNewProceduresModal: true, editProcedures: editProcedures });
   }

   /**
    * On Add & Update Master Value Modal Close
    */
   onAddUpdatePaybackModalClose = () => {
      let upR = {
         spec_id: '',
         procedure_name: '',
         procedure_code: '',
      }
      let addR = {}
      this.setState({ addNewProceduresModal: false, editProcedures: null, err: upR, add_err: addR, ruleExist: false })
   }

   /**
    * On Update Master Value Details
    */
   onUpdateProceduresDetails(fieldName, value) {

      let { err } = this.state;

      switch (fieldName) {
         case 'spec_id':
            if (isEmpty(value)) {
               err[fieldName] = "Select Payback Type field";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'procedure_name':
            if (isEmpty(value)) {
               err[fieldName] = "Name field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         case 'procedure_code':
            if (isEmpty(value)) {
               err[fieldName] = "Code field can't be blank";
            } else {
               err[fieldName] = '';
            }
            break;
         default:
            break;
      }

      this.setState({ err: err });
      //console.log(this.state.err)

      this.setState({
         editProcedures: {
            ...this.state.editProcedures,
            [fieldName]: value
         }
      });
   }

   /**
    * Update User
    */
   updateProcedures() {
      const { editProcedures } = this.state;
  
      const filterType_rule = this.props.proceduresData.filter(x => 
         x.spec_id == editProcedures.spec_id && 
         x.procedure_name == editProcedures.procedure_name &&
         x.procedure_code == editProcedures.procedure_code &&
         x.procedure_id != editProcedures.procedure_id
         );
         
      //const filterType_rule = this.props.payback.filter(x => x.payback_id ==  editPayback.payback_id && x.min_volume == editPayback.min_volume && x.max_volume == editPayback.max_volume && x.rate == editPayback.rate && x.id != editPayback.id);
      //console.log(this.props.payback)
      //console.log(filterType)
      //console.log(editPayback)
      if (filterType_rule.length>0) {
         this.setState({ ruleExist: true })
         return false;
      }

      

      const filterType = this.props.specialityData.filter(x => x.mdv_id == editProcedures.spec_id);
      //console.log(editRiskFactor)
      //console.log(filterType)
      editProcedures.value = filterType[0].value
      

      let indexOfUpdatePayback = '';
      let procedures = this.props.proceduresData;

      for (let i = 0; i < procedures.length; i++) {
         const user = procedures[i];

         if (user.procedure_id === editProcedures.procedure_id) {
            indexOfUpdatePayback = i
         }
      }
      //return false;
      //console.log(editRiskFactor)
      this.props.updateProcedures(editProcedures);


      procedures[indexOfUpdatePayback] = editProcedures;
      this.setState({ editProcedures: null, addNewProceduresModal: false });
      let self = this;
      //setTimeout(() => {
         self.setState({ procedures });
        // NotificationManager.success('Procedures Updated!');
      //}, 2000);
   }



   render() {
      const { add_err, err, Payback, editProcedures, allSelected, selectedProcedures } = this.state;
      //console.log(editProcedures)

      const columns = [

         {
            name: 'ID',
            field: 'procedure_id'
         },
         {
            name: 'Speciality',
            field: 'value',
         },
         {
            name: 'Procedure Name',
            field: 'procedure_name',

         },
         {
            name: 'Prodcedure Code',
            field: 'procedure_code',

         },

         {
            name: "Status",
            field: "status",
            options: {
               filter: true,
               sort: true,
               empty: true,
               customBodyRender: (value) => {
                  return (
                     value.status == 1 ? 'Active' : 'Inactive'
                  );
               }
            }
         },
         {
            name: "Action",
            options: {
               filter: true,
               sort: false,
               empty: true,
               download: false,
               customBodyRender: (value) => {
                  //console.log(value)
                  return (
                     <div className="list-action">
                        {(this.state.currentPermision.view) ? <a href="javascript:void(0)" onClick={() => this.viewProceduresDetail(value)}><i className="ti-eye"></i></a> : ''}
                        {(this.state.currentPermision.edit) ? <a href="javascript:void(0)" onClick={() => this.onEditProcedures(value)}><i className="ti-pencil"></i></a> : ''}
                     </div>
                  );
               }
            }
         }

      ];

      const options = {
         filterType: 'dropdown',
         selectableRows: false,
         customToolbar: () => {
            return (
               (this.state.currentPermision.view) ? <AddNewButton opnaddNewProceduresModal={this.opnaddNewProceduresModal.bind(this)} /> : ''
            );
         }

      };
      return (
         <div className="others payback">
            <Helmet>
               <title>Health Partner | Others | Payback</title>
               <meta name="description" content="Payback" />
            </Helmet>
            <PageTitleBar
               title={<IntlMessages id="sidebar.procedures" />}
               match={this.props.match}
            />

            <RctCollapsibleCard heading="" fullBlock>
               <MaterialDatatable
                  data={this.props.proceduresData}
                  columns={columns}
                  options={options}
               />
            </RctCollapsibleCard>



            <Modal isOpen={this.state.addNewProceduresModal} toggle={() => this.onAddUpdatePaybackModalClose()}>
               <ModalHeader toggle={() => this.onAddUpdatePaybackModalClose()}>
                  {editProcedures === null ?
                     'Add Procedures' : 'Update Procedures'
                  }
               </ModalHeader>
               <ModalBody>

                  {this.state.ruleExist &&
                     <div className="alert alert-danger"><strong>Rule Already Exist!</strong></div>
                  }

                  {editProcedures === null ?
                     <AddNewPaybackForm
                        speciality={this.props.specialityData}
                        addErr={add_err}
                        addNewProceduresDetails={this.state.addNewProceduresDetail}
                        onChangeaddNewProceduresDetail={this.onChangeaddNewProceduresDetails.bind(this)}
                     />
                     : <UpdatePaybackForm
                        speciality={this.props.specialityData}
                        updateErr={err}
                        updateProceduresDetails={editProcedures}
                        onUpdateProceduresDetail={this.onUpdateProceduresDetails.bind(this)} />
                  }
               </ModalBody>
               <ModalFooter>
                  {editProcedures === null ?
                     <Button
                        variant="contained"
                        className={(this.validateAddSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.addNewProcedures()}
                        disabled={!this.validateAddSubmit()}
                     >Add</Button>
                     : <Button
                        variant="contained"
                        color="primary"
                        className={(this.validateUpdateSubmit()) ? "text-white btn-success" : "text-white btn-error"}
                        onClick={() => this.updateProcedures()}
                        disabled={!this.validateUpdateSubmit()}
                     >Update</Button>
                  }

                  <Button variant="contained" className="text-white btn-danger" onClick={() => this.onAddUpdatePaybackModalClose()}>Cancel</Button>
               </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.openViewProceduresDialog} toggle={() => this.onViewProceduresModalClose()}>
               <ModalHeader toggle={() => this.onViewProceduresModalClose()}>
                  {selectedProcedures !== null ? 'Procedures View' : ''}
               </ModalHeader>
               <ModalBody>
                  {selectedProcedures !== null &&
                     <div>
                        <div className="clearfix d-flex">
                           <div className="media pull-left width-100">
                              
                              <div className="media-body">
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Speciality Type:</span> <span className="second-colmn">{selectedProcedures.value}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Procedure Name:</span> <span className="second-colmn">{selectedProcedures.procedure_name}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Prodcedure Code:</span> <span className="second-colmn">{selectedProcedures.procedure_code}</span></div>
                                 <div className="colmn-row"><span className="first-colmn fw-bold">Status:</span> <span className="second-colmn">{selectedProcedures.status == 1 ? 'Active' : 'Inactive'}</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  }
               </ModalBody>
               <ModalFooter>
                  
               </ModalFooter>
            </Modal> 

         </div>
      );
   }
}


// map state to props
const mapStateToProps = ({ Procedures, authUser }) => {

   const { loading, proceduresData, specialityData } = Procedures;
   const user = authUser.user;
   return { loading, proceduresData, specialityData, user }

}

export default connect(mapStateToProps, {
   proceduresList, insertProcedures, updateProcedures
})(Payback);