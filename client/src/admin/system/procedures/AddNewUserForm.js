/**
 * Add New User Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const AddNewUserForm = ({ speciality, addErr, addNewProceduresDetails, onChangeaddNewProceduresDetail }) => (
    <Form>
    <div className="row">
        
        <div className="col-md-12">
        <FormGroup>
            <Label for="speciality_type">Speciality Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="speciality_type"
                id="speciality_type"
                onChange={(e) => onChangeaddNewProceduresDetail('speciality_type', e.target.value)}
            >
                <option value="">Select</option>
                {speciality && speciality.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(addErr.speciality_type) ? <FormHelperText>{addErr.speciality_type}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="name">Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="name"
                id="name"
                fullWidth
                variant="outlined"
                placeholder="Procedure Name"
                value={addNewProceduresDetails.name}
                error={(addErr.name)?true:false}
                helperText={addErr.name}
                onChange={(e) => onChangeaddNewProceduresDetail('name', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="code">Code<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="code"
                id="code"
                fullWidth
                variant="outlined"
                placeholder="Procedure Code"
                value={addNewProceduresDetails.code}
                error={(addErr.code)?true:false}
                helperText={addErr.code}
                onChange={(e) => onChangeaddNewProceduresDetail('code', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(addNewProceduresDetails.status == 1) ? true : false}
                        onChange={(e) => onChangeaddNewProceduresDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(addNewProceduresDetails.status == 0) ? true : false}
                        onChange={(e) => onChangeaddNewProceduresDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>    
        </div>
        </div>
    </Form>
);

export default AddNewUserForm;
