/**
 * Update User Details Form
 */
import React from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';

const UpdateUserForm = ({ speciality, updateErr, updateProceduresDetails, onUpdateProceduresDetail }) => (
    <Form>
    <div className="row">

        
        <div className="col-md-12">
        <FormGroup>
            <Label for="speciality_type">Speciality Type<span className="required-field">*</span></Label>
            <Input
                type="select"
                name="spec_id"
                id="spec_id"
                defaultValue={updateProceduresDetails.spec_id}
                onChange={(e) => onUpdateProceduresDetail('spec_id', e.target.value)}
            >
                <option value="">Select</option>
                {speciality && speciality.map((opt, key) => (
                    <option value={opt.mdv_id} key={key}>{opt.value}</option>
                ))}
            </Input>
            {(updateErr.spec_id) ? <FormHelperText>{updateErr.spec_id}</FormHelperText> : ''}
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="procedure_name">Name<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="procedure_name"
                id="procedure_name"
                fullWidth
                variant="outlined"
                placeholder="Procedure Name"
                value={updateProceduresDetails.procedure_name}
                error={(updateErr.procedure_name)?true:false}
                helperText={updateErr.procedure_name}
                onChange={(e) => onUpdateProceduresDetail('procedure_name', e.target.value)}
            />
            
        </FormGroup>
        </div>

        <div className="col-md-6">
        <FormGroup>
            <Label for="procedure_code">Code<span className="required-field">*</span></Label>
            <TextField
                type="text"
                name="procedure_code"
                id="procedure_code"
                fullWidth
                variant="outlined"
                placeholder="Procedure Code"
                value={updateProceduresDetails.procedure_code}
                error={(updateErr.procedure_code)?true:false}
                helperText={updateErr.procedure_code}
                onChange={(e) => onUpdateProceduresDetail('procedure_code', e.target.value)}
            />
            
        </FormGroup>
        </div>

        
        <div className="col-md-12">
        <FormGroup tag="fieldset">
            <Label>Status</Label>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status"
                        value={1}
                        checked={(updateProceduresDetails.status == 1) ? true : false}
                        onChange={(e) => onUpdateProceduresDetail('status', e.target.value)}
                        />{' '}
                    Active
                </Label>
            </FormGroup>
            <FormGroup check>
                <Label check>
                    <Input
                        type="radio"
                        name="status" 
                        value={0}
                        checked={(updateProceduresDetails.status == 0) ? true : false}
                        onChange={(e) => onUpdateProceduresDetail('status', e.target.value)}
                        />{' '}
                    Inactive
                </Label>
            </FormGroup>
        </FormGroup>   
        </div>
    </div>
    </Form>
);

export default UpdateUserForm;
