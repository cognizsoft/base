const mysql = require('mysql');
var con = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  database:'alpha_hps',
});

con.connect(function(error){
  if(error) console.log('mysql not found')
  else console.log('Connected')
});

module.exports = con;