<<<<<<< HEAD
/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, con, jwtMW) {
  const md5 = require('md5');
  const jwt = require('jsonwebtoken');
  let date = require('date-and-time');
  app.post('/api/users/login', (req, res) => {

    let username = req.body.username;
    let password = req.body.password;
    password = md5(password);
    ///console.log('select user_id,username from users where delete_flag = 0 AND status=1 AND username="'+username+'" AND password="'+password+'"')
    con.query('select user.user_id,user.username,master_data_values.value as role from user inner join master_data_values on master_data_values.mdv_id=user.mdv_user_type_id where user.delete_flag = 0 AND user.status=1 AND user.username="' + username + '" AND user.password="' + password + '"', function (error, rows, fields) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        if (rows.length > 0) {
          let token = jwt.sign({ username: rows[0].username }, 'ramesh', { expiresIn: 129600 }); // Signing the token
          /*
          * This sql use for get user permission
          */
          con.query('select system_module.description,permission.create_flag,permission.edit_flag,permission.view_flag,permission.delete_flag '
            + 'from permission '
            + 'INNER JOIN user_role ON permission.mdv_role_id=user_role.mdv_role_id '
            + 'INNER JOIN system_module ON system_module.sys_mod_id=permission.sys_mod_id '
            + 'where user_role.user_id=' + rows[0].user_id, function (error, permission, fields) {

              if (error) {
                var obj = {
                  "user": {
                    "status": 1,
                    "username": rows[0].username,
                    "current_user_id": rows[0].user_id,
                    token
                  },
                  "role":rows[0].role,
                  "user_permission": '',
                }
                res.send(obj);
              } else {
                var obj = {
                  "user": {
                    "status": 1,
                    "username": rows[0].username,
                    "current_user_id": rows[0].user_id,
                    token
                  },
                  "role":rows[0].role,
                  "user_permission": permission,
                }
                res.send(obj);
              }
            })
        }
        else {
          //var myData = ['status'=>0, 'message'=>'Username and password not match'];
          var obj = {
            "status": 0,
            "message": "Username or password mismatch."
          }
          res.send(obj);
        }
      }
    })
  })

  app.get('/api/user-list/', jwtMW, (req, res) => {
    let result = {};
    con.query('SELECT user.user_id, user.username, user.email_id,user.mdv_user_type_id,user.f_name,user.m_name,user.l_name, user.status,master_data_values.value as user_type,master_data_values_role.value as user_role,user_role.mdv_role_id, provider.provider_id, provider.name FROM user INNER JOIN master_data_values ON master_data_values.mdv_id=user.mdv_user_type_id INNER JOIN user_role ON user_role.user_id=user.user_id INNER JOIN master_data_values as master_data_values_role ON master_data_values_role.mdv_id=user_role.mdv_role_id LEFT JOIN provider ON provider.provider_id=user.client_id WHERE user.delete_flag = ?',
      [
        0
      ]
      , function (error, rows, fields) {
        if (error) console.log(error)
        else {
          // get user type
          con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
            [
              1, 0, 'User Type'
            ]
            , function (error, type, fields) {
              if (error) console.log(error)
              else {
                con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                  [
                    1, 0, 'User Role'
                  ]
                  , function (error, role, fields) {
                    if (error) console.log(error)
                    else {
                      con.query('SELECT provider_id,name FROM provider WHERE status=? AND deleted_flag = ?',
                        [
                          1, 0
                        ]
                        , function (error, provider, fields) {
                          if (error) console.log(error)
                          else {
                            result.status = 1;
                            result.userRole = role;
                            result.userType = type;
                            result.result = rows;
                            result.provider = provider;
                            res.send(result);
                          }
                        })
                    }
                  })
              }
            })

        }
      })
  })

  app.post('/api/user-username-exist/', jwtMW, (req, res) => {
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=?';
    } else {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=? AND user_id !=?';
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          console.log(err);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-eamil-exist/', jwtMW, (req, res) => {
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=?';
    } else {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=? AND user_id !=?';
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          console.log(err);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-delete/', jwtMW, (req, res) => {
    con.query('UPDATE user SET delete_flag=? WHERE user_id=?',
      [
        1,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 1
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "user_id": req.body.user_id
          }
          res.send(obj);

        }
      })
  })
  //other routes..
  app.post('/api/user-insert/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('INSERT INTO user SET mdv_user_type_id=?,client_id=?,username=?,password=?,email_id=?,f_name=?,m_name=?,l_name=?,status=?,date_created=?,date_modified=?,created_by=?,modified_by=?',
        [
          req.body.mdv_user_type_id,
          req.body.client_id,
          req.body.username,
          password,
          req.body.email_id,
          req.body.f_name,
          req.body.m_name,
          req.body.l_name,
          req.body.status,
          current_date,
          current_date,
          current_user_id,
          current_user_id
        ]
        , function (err, resultmain) {
          if (err) {
            console.log(err)
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('INSERT INTO user_role SET user_id=?, mdv_role_id=?, status=?, date_created=?, date_modified=?, created_by=?, modified_by=?',
            [
              resultmain.insertId,
              req.body.mdv_role_id,
              req.body.status,
              current_date,
              current_date,
              current_user_id,
              current_user_id
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                obj.status = 1;
                obj.user_id = resultmain.insertId;
                obj.mdv_user_type_id = req.body.mdv_user_type_id;
                obj.mdv_role_id = req.body.mdv_role_id;
                obj.username = req.body.username;
                obj.email_id = req.body.email_id;
                obj.userstatus = req.body.status;
                obj.f_name = req.body.f_name;
                obj.m_name = req.body.m_name;
                obj.l_name = req.body.l_name;
                obj.provider_id = req.body.client_id;
                res.send(obj);
              });
            });
        });
    });
    /*let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    con.query('INSERT INTO master_data (md_name, status, edit_allowed, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?)',
      [
        req.body.md_name,
        req.body.status,
        req.body.edit_allowed,
        current_user_id,
        current_user_id,
        current_date,
        current_date

      ]
      , function (err, result) {

        if (err) {
          console.log(err);
        } else {
          var obj = {
            "status": 1,
            "last_type_id": result.insertId,
            "md_name": req.body.md_name,
            "edit_allowed": req.body.edit_allowed,
            "data_status": req.body.status
          }
          console.log(obj)
          res.send(obj);

        }
      })*/
  })

  app.post('/api/user-update/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let client_id = (req.body.client_id) ? req.body.client_id : null;
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('UPDATE user SET mdv_user_type_id=?,client_id=?,username=?,email_id=?,f_name=?,m_name=?,l_name=?,status=?,date_modified=?,modified_by=? WHERE user_id=?',
        [
          req.body.mdv_user_type_id,
          client_id,
          req.body.username,
          req.body.email_id,
          req.body.f_name,
          req.body.m_name,
          req.body.l_name,
          req.body.status,
          current_date,
          current_user_id,
          req.body.user_id,
        ]
        , function (err, resultmain) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('UPDATE user_role SET mdv_role_id=?, status=?, date_modified=?, modified_by=? WHERE user_id=?',
            [
              req.body.mdv_role_id,
              req.body.status,
              current_date,
              current_user_id,
              req.body.user_id,
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                obj.status = 1;
                res.send(obj);
              });
            });
        });
    });
  })

  app.post('/api/user-password/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);
    con.query('UPDATE user SET password=?,date_modified=?,modified_by=? WHERE user_id=?',
      [
        password,
        current_date,
        current_user_id,
        req.body.password_id,
      ]
      , function (err, result) {
        if (err) {
          var obj = {
            "status": 0,
            "message" : "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1
          }
          res.send(obj);

        }
      })
  })
  
}
=======
/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function (app, con, jwtMW) {
  const md5 = require('md5');
  const jwt = require('jsonwebtoken');
  let date = require('date-and-time');
  app.post('/api/users/login', (req, res) => {

    let username = req.body.username;
    let password = req.body.password;
    password = md5(password);
    ///console.log('select user_id,username from users where delete_flag = 0 AND status=1 AND username="'+username+'" AND password="'+password+'"')
    con.query('select user.user_id,user.username,master_data_values.value as role from user inner join master_data_values on master_data_values.mdv_id=user.mdv_user_type_id where user.delete_flag = 0 AND user.status=1 AND user.username="' + username + '" AND user.password="' + password + '"', function (error, rows, fields) {
      if (error) {
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
        }
        res.send(obj);
      } else {
        if (rows.length > 0) {
          let token = jwt.sign({ username: rows[0].username }, 'ramesh', { expiresIn: 129600 }); // Signing the token
          /*
          * This sql use for get user permission
          */
          con.query('select system_module.description,permission.create_flag,permission.edit_flag,permission.view_flag,permission.delete_flag '
            + 'from permission '
            + 'INNER JOIN user_role ON permission.mdv_role_id=user_role.mdv_role_id '
            + 'INNER JOIN system_module ON system_module.sys_mod_id=permission.sys_mod_id '
            + 'where user_role.user_id=' + rows[0].user_id, function (error, permission, fields) {

              if (error) {
                var obj = {
                  "user": {
                    "status": 1,
                    "username": rows[0].username,
                    "current_user_id": rows[0].user_id,
                    token
                  },
                  "role":rows[0].role,
                  "user_permission": '',
                }
                res.send(obj);
              } else {
                var obj = {
                  "user": {
                    "status": 1,
                    "username": rows[0].username,
                    "current_user_id": rows[0].user_id,
                    token
                  },
                  "role":rows[0].role,
                  "user_permission": permission,
                }
                res.send(obj);
              }
            })
        }
        else {
          //var myData = ['status'=>0, 'message'=>'Username and password not match'];
          var obj = {
            "status": 0,
            "message": "Username or password mismatch."
          }
          res.send(obj);
        }
      }
    })
  })

  app.get('/api/user-list/', jwtMW, (req, res) => {
    let result = {};
    con.query('SELECT user.user_id, user.username, user.email_id,user.mdv_user_type_id,user.f_name,user.m_name,user.l_name, user.status,master_data_values.value as user_type,master_data_values_role.value as user_role,user_role.mdv_role_id, provider.provider_id, provider.name FROM user INNER JOIN master_data_values ON master_data_values.mdv_id=user.mdv_user_type_id INNER JOIN user_role ON user_role.user_id=user.user_id INNER JOIN master_data_values as master_data_values_role ON master_data_values_role.mdv_id=user_role.mdv_role_id LEFT JOIN provider ON provider.provider_id=user.client_id WHERE user.delete_flag = ?',
      [
        0
      ]
      , function (error, rows, fields) {
        if (error) console.log(error)
        else {
          // get user type
          con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
            [
              1, 0, 'User Type'
            ]
            , function (error, type, fields) {
              if (error) console.log(error)
              else {
                con.query('SELECT mdv_id,value FROM master_data_values WHERE status=? AND deleted_flag = ? AND md_id=?',
                  [
                    1, 0, 'User Role'
                  ]
                  , function (error, role, fields) {
                    if (error) console.log(error)
                    else {
                      con.query('SELECT provider_id,name FROM provider WHERE status=? AND deleted_flag = ?',
                        [
                          1, 0
                        ]
                        , function (error, provider, fields) {
                          if (error) console.log(error)
                          else {
                            result.status = 1;
                            result.userRole = role;
                            result.userType = type;
                            result.result = rows;
                            result.provider = provider;
                            res.send(result);
                          }
                        })
                    }
                  })
              }
            })

        }
      })
  })

  app.post('/api/user-username-exist/', jwtMW, (req, res) => {
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=?';
    } else {
      sql = 'SELECT user_id FROM user WHERE delete_flag=? AND username=? AND user_id !=?';
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          console.log(err);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-eamil-exist/', jwtMW, (req, res) => {
    let sql = '';
    if (req.body.user_id === undefined) {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=?';
    } else {
      sql = 'SELECT email_id FROM user WHERE delete_flag=? AND email_id=? AND user_id !=?';
    }
    con.query(sql,
      [
        0,
        req.body.username,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          console.log(err);
        } else {

          var obj = {
            "status": 1,
            "exist": (rows.length > 0) ? 1 : 0
          }
          res.send(obj);

        }
      })
  })

  app.post('/api/user-delete/', jwtMW, (req, res) => {
    con.query('UPDATE user SET delete_flag=? WHERE user_id=?',
      [
        1,
        req.body.user_id
      ]
      , function (error, rows, fields) {

        if (error) {
          var obj = {
            "status": 1
          }
          res.send(obj);
        } else {

          var obj = {
            "status": 1,
            "user_id": req.body.user_id
          }
          res.send(obj);

        }
      })
  })
  //other routes..
  app.post('/api/user-insert/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('INSERT INTO user SET mdv_user_type_id=?,client_id=?,username=?,password=?,email_id=?,f_name=?,m_name=?,l_name=?,status=?,date_created=?,date_modified=?,created_by=?,modified_by=?',
        [
          req.body.mdv_user_type_id,
          req.body.client_id,
          req.body.username,
          password,
          req.body.email_id,
          req.body.f_name,
          req.body.m_name,
          req.body.l_name,
          req.body.status,
          current_date,
          current_date,
          current_user_id,
          current_user_id
        ]
        , function (err, resultmain) {
          if (err) {
            console.log(err)
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('INSERT INTO user_role SET user_id=?, mdv_role_id=?, status=?, date_created=?, date_modified=?, created_by=?, modified_by=?',
            [
              resultmain.insertId,
              req.body.mdv_role_id,
              req.body.status,
              current_date,
              current_date,
              current_user_id,
              current_user_id
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                obj.status = 1;
                obj.user_id = resultmain.insertId;
                obj.mdv_user_type_id = req.body.mdv_user_type_id;
                obj.mdv_role_id = req.body.mdv_role_id;
                obj.username = req.body.username;
                obj.email_id = req.body.email_id;
                obj.userstatus = req.body.status;
                obj.f_name = req.body.f_name;
                obj.m_name = req.body.m_name;
                obj.l_name = req.body.l_name;
                obj.provider_id = req.body.client_id;
                res.send(obj);
              });
            });
        });
    });
    /*let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    con.query('INSERT INTO master_data (md_name, status, edit_allowed, created_by, modified_by, created, modified) VALUES(?,?,?,?,?,?,?)',
      [
        req.body.md_name,
        req.body.status,
        req.body.edit_allowed,
        current_user_id,
        current_user_id,
        current_date,
        current_date

      ]
      , function (err, result) {

        if (err) {
          console.log(err);
        } else {
          var obj = {
            "status": 1,
            "last_type_id": result.insertId,
            "md_name": req.body.md_name,
            "edit_allowed": req.body.edit_allowed,
            "data_status": req.body.status
          }
          console.log(obj)
          res.send(obj);

        }
      })*/
  })

  app.post('/api/user-update/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let client_id = (req.body.client_id) ? req.body.client_id : null;
    var obj = {};
    con.beginTransaction(function (err) {
      if (err) {
        obj.status = 0;
        obj.message = "Something wrong please try again.";
      }
      con.query('UPDATE user SET mdv_user_type_id=?,client_id=?,username=?,email_id=?,f_name=?,m_name=?,l_name=?,status=?,date_modified=?,modified_by=? WHERE user_id=?',
        [
          req.body.mdv_user_type_id,
          client_id,
          req.body.username,
          req.body.email_id,
          req.body.f_name,
          req.body.m_name,
          req.body.l_name,
          req.body.status,
          current_date,
          current_user_id,
          req.body.user_id,
        ]
        , function (err, resultmain) {
          if (err) {
            con.rollback(function () {
              obj.status = 0;
              obj.message = "Something wrong please try again.";
            });
          }
          con.query('UPDATE user_role SET mdv_role_id=?, status=?, date_modified=?, modified_by=? WHERE user_id=?',
            [
              req.body.mdv_role_id,
              req.body.status,
              current_date,
              current_user_id,
              req.body.user_id,
            ]
            , function (err, result) {
              if (err) {
                con.rollback(function () {
                  obj.status = 0;
                  obj.message = "Something wrong please try again.";
                });
              }
              con.commit(function (err) {
                if (err) {
                  con.rollback(function () {
                    obj.status = 0;
                    obj.message = "Something wrong please try again.";
                  });
                }
                obj.status = 1;
                res.send(obj);
              });
            });
        });
    });
  })

  app.post('/api/user-password/', jwtMW, (req, res) => {
    let now = new Date();
    let current_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
    let current_user_id = req.body.current_user_id;
    let password = md5(req.body.password);
    con.query('UPDATE user SET password=?,date_modified=?,modified_by=? WHERE user_id=?',
      [
        password,
        current_date,
        current_user_id,
        req.body.password_id,
      ]
      , function (err, result) {
        if (err) {
          var obj = {
            "status": 0,
            "message" : "Something wrong please try again."
          }
          res.send(obj);
        } else {
          var obj = {
            "status": 1
          }
          res.send(obj);

        }
      })
  })
  
}
>>>>>>> e43bd9835fbaf019220f2366c0a66d23641f9acb
