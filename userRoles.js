<<<<<<< HEAD
/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function(app,con,jwtMW){
  /*app.use(jwtMW)
  app.use(function (err, req, res, next) {
    console.log(err)
    if (err.name === 'UnauthorizedError') {
      var obj = {
         "status": 0,
         "message": "Invalid token."
      }
      res.status(401).send(obj);
    } else {
      next(err);
    }
  });*/

  app.get('/api/user-roles/', jwtMW,(req,res) => {

    con.query('select mdv_id, value, description, status from master_data_values where deleted_flag = 0 AND md_id = "User Role"', function (error,rows, fields){
      if(error) console.log(error)
      else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////
  /////UPDATE USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-update/', jwtMW,(req,res) => {
    //console.log(req.body);
    let user_role = req.body.value;
    let user_role_desc = req.body.description;
    let status = req.body.status;
    let mdv_id = req.body.mdv_id;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE master_data_values SET value="'+user_role+'", description="'+user_role_desc+'", status="'+status+'", modified=NOW(), modified_by="'+current_user_id+'" WHERE mdv_id="'+mdv_id+'"', function (err,result){
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })

  //////////////////////////
  /////DELETE USER TYPE/////
  /////////////////////////

 /* app.post('/api/user-roles-delete/', jwtMW,(req,res) => {
    //console.log(req.body);
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
     con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="'+current_user_id+'" WHERE type_id="'+type_id+'"', function (err,result){
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })*/

  //////////////////////////
  /////INSERT USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-insert/', jwtMW,(req,res) => {
    console.log(req.body);
    let current_user_id = req.body.current_user_id;
     con.query('INSERT INTO master_data_values (md_id, value, description, status, created_by, modified_by, created, modified) VALUES("User Role", "'+req.body.value+'", "'+req.body.user_role_desc+'", "'+req.body.status+'", "'+current_user_id+'", "'+current_user_id+'", NOW(), NOW())', function (err,result){
          
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1,
           "last_role_id": result.insertId,
           "user_role": req.body.value,
           "user_role_desc": req.body.user_role_desc,
           "user_role_status": req.body.status
          }
          res.send(obj);
           
          }
     })
  })

  //////////////////////////
  /////LAST INSERT ID USER TYPE/////
  /////////////////////////

 /*
  app.get('/api/user-types-last-insert-id/', jwtMW,(req,res) => {

    con.query('SELECT mdv_id FROM master_data_values WHERE deleted_flag = 0 AND md_id = "User Type" ORDER BY mdv_id DESC LIMIT 1', function (error,rows, fields){
      if(error) console.log(error)
      else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  app.post('/api/usertypes/submit', jwtMW, (req,res) => {
    console.log(req.body)
    let user_type = req.body.usertype;
    let status    = req.body.status;
    // insert new user type according to request
    con.query('INSERT INTO `user_types`(`user_type`, `status`) VALUES ("'+user_type+'",'+status+')', function(error,rows,fields){
    console.log(rows);
    console.log(error)  
      if(error){
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
       }
       res.send(obj);
      }else{
        con.query('select type_id,user_type,status from user_types where delete_flag = 0', function (error,rows, fields){
          if(error) console.log(error)
          else {
            var obj = {
              "status": 1,
              "result": rows,
            }
            res.send(obj);
            
          }
        })
        
      }
    })
    
  })

  app.post('/api/usertypes/getUserType', jwtMW, (req,res) => {
    console.log(req.body)
    let user_type_id = req.body.user_type_id;
    // insert new user type according to request
    con.query('SELECT type_id,user_type,status FROM `user_types` WHERE type_id='+user_type_id, function(error,rows,fields){
    console.log(rows);
    console.log(error)  
      if(error){
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
       }
       res.send(obj);
      }else{
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
        
      }
    })
    
  })
  
*/
    //other routes..
}
=======
/*
* Title: userTypes
* Descrpation :- This module blong to user type all application
* Date :- 25 Feb 2019
* Author :- Ramesh Kumar
*/
module.exports = function(app,con,jwtMW){
  /*app.use(jwtMW)
  app.use(function (err, req, res, next) {
    console.log(err)
    if (err.name === 'UnauthorizedError') {
      var obj = {
         "status": 0,
         "message": "Invalid token."
      }
      res.status(401).send(obj);
    } else {
      next(err);
    }
  });*/

  app.get('/api/user-roles/', jwtMW,(req,res) => {

    con.query('select mdv_id, value, description, status from master_data_values where deleted_flag = 0 AND md_id = "User Role"', function (error,rows, fields){
      if(error) console.log(error)
      else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  //////////////////////////
  /////UPDATE USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-update/', jwtMW,(req,res) => {
    //console.log(req.body);
    let user_role = req.body.value;
    let user_role_desc = req.body.description;
    let status = req.body.status;
    let mdv_id = req.body.mdv_id;
    let current_user_id = req.body.current_user_id;

     con.query('UPDATE master_data_values SET value="'+user_role+'", description="'+user_role_desc+'", status="'+status+'", modified=NOW(), modified_by="'+current_user_id+'" WHERE mdv_id="'+mdv_id+'"', function (err,result){
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })

  //////////////////////////
  /////DELETE USER TYPE/////
  /////////////////////////

 /* app.post('/api/user-roles-delete/', jwtMW,(req,res) => {
    //console.log(req.body);
    let type_id = req.body.type_id;
    let current_user_id = req.body.current_user_id;
     con.query('UPDATE user_types SET delete_flag="1", modified=NOW(), modified_by="'+current_user_id+'" WHERE type_id="'+type_id+'"', function (err,result){
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1
          }
          res.send(obj);
           
          }
     })
  })*/

  //////////////////////////
  /////INSERT USER TYPE/////
  /////////////////////////

  app.post('/api/user-roles-insert/', jwtMW,(req,res) => {
    console.log(req.body);
    let current_user_id = req.body.current_user_id;
     con.query('INSERT INTO master_data_values (md_id, value, description, status, created_by, modified_by, created, modified) VALUES("User Role", "'+req.body.value+'", "'+req.body.user_role_desc+'", "'+req.body.status+'", "'+current_user_id+'", "'+current_user_id+'", NOW(), NOW())', function (err,result){
          
          if(err) {
            console.log(err);
          } else {
            var obj = {
           "status": 1,
           "last_role_id": result.insertId,
           "user_role": req.body.value,
           "user_role_desc": req.body.user_role_desc,
           "user_role_status": req.body.status
          }
          res.send(obj);
           
          }
     })
  })

  //////////////////////////
  /////LAST INSERT ID USER TYPE/////
  /////////////////////////

 /*
  app.get('/api/user-types-last-insert-id/', jwtMW,(req,res) => {

    con.query('SELECT mdv_id FROM master_data_values WHERE deleted_flag = 0 AND md_id = "User Type" ORDER BY mdv_id DESC LIMIT 1', function (error,rows, fields){
      if(error) console.log(error)
      else {
        //console.log(rows)
        var obj = {
           "status": 1,
           "result": rows,
        }
        res.send(obj);
      }
    })
  })

  app.post('/api/usertypes/submit', jwtMW, (req,res) => {
    console.log(req.body)
    let user_type = req.body.usertype;
    let status    = req.body.status;
    // insert new user type according to request
    con.query('INSERT INTO `user_types`(`user_type`, `status`) VALUES ("'+user_type+'",'+status+')', function(error,rows,fields){
    console.log(rows);
    console.log(error)  
      if(error){
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
       }
       res.send(obj);
      }else{
        con.query('select type_id,user_type,status from user_types where delete_flag = 0', function (error,rows, fields){
          if(error) console.log(error)
          else {
            var obj = {
              "status": 1,
              "result": rows,
            }
            res.send(obj);
            
          }
        })
        
      }
    })
    
  })

  app.post('/api/usertypes/getUserType', jwtMW, (req,res) => {
    console.log(req.body)
    let user_type_id = req.body.user_type_id;
    // insert new user type according to request
    con.query('SELECT type_id,user_type,status FROM `user_types` WHERE type_id='+user_type_id, function(error,rows,fields){
    console.log(rows);
    console.log(error)  
      if(error){
        var obj = {
          "status": 0,
          "message": "Something wrong please try again."
       }
       res.send(obj);
      }else{
        var obj = {
          "status": 1,
          "result": rows,
        }
        res.send(obj);
        
      }
    })
    
  })
  
*/
    //other routes..
}
>>>>>>> e43bd9835fbaf019220f2366c0a66d23641f9acb
